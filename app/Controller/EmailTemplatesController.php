<?php
/*
 @controller : EmailTemplates
 @created    : 31 jan 2013
 @author     :
 @use        : Manager email templates
 
*/

class EmailTemplatesController extends AppController {

    var $name = 'EmailTemplates';
    var $uses = array('EmailTemplate');
    
	function beforeFilter()
	{
        parent::beforeFilter();
    }



    function admin_index() 
	{
		$this->layout = 'admin';  
        $this->set('title_for_layout', __('EMail Template Manager', true));
        $this->set('emailTemplate', $this->paginate('EmailTemplate'));

    }



    function admin_add() 
	{
		$this->layout = 'admin';  
        $this->set('title_for_layout', __('Add Email Template', true));
        if (!empty($this->data)) {
            $this->EmailTemplate->create();
            if ($this->EmailTemplate->save($this->data)) {
                $this->Session->setFlash(__('The  Email has been saved', true),'default',array('class'=>'flash_good'));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The  Email could not be saved. Please, try again.', 'admin_flash_bad'));
            }
        }

    }



    function admin_edit($id = null)
	{
		$this->layout = 'admin';  
        $this->set('title_for_layout', __('Edit System Mail', true));
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid  Email', 'admin_flash_bad'));
            $this->redirect(array('action'=>'index'));
        }

        if (!empty($this->data)) {
            if ($this->EmailTemplate->save($this->data)) {
                $this->Session->setFlash(__('The  Mail has been saved', true),'default',array('class'=>'flash_good'));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The  Mail could not be saved. Please, try again.', 'admin_flash_bad'));

            }
        }

        if (empty($this->data)) {
            $this->data = $this->EmailTemplate->read(null, $id);
        }

    }



    function admin_delete($id = null)
	{
		$this->layout = 'admin';  
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for  Mail', 'admin_flash_bad'));
            $this->redirect(array('action'=>'index'));
        }

        if ($this->EmailTemplate->delete($id)) {
            $this->Session->setFlash(__('System Mail deleted', 'admin_flash_bad'));
            $this->redirect(array('action'=>'index'));
        }

    }



}?>