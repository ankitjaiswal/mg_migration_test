<?php
	/**
	* Clients Controller
	*
	* PHP version 5
	*
	* @category Controller
	*/
App::uses('Sanitize', 'Utility');
require('../webroot/stripe-php-1.17.2/lib/Stripe.php');
	class ClientsController extends AppController{
		/**
		* Controller name
		* @var string
		* @access public
		*/
		var $name = 'Clients';
		/**
		* Models used by the Controller
		* @var array
		* @access public
		*/	
		var $uses =array('User','UserReference','Social','Mentorship','Answer','Message',
						'TempRequest','Feedback','MessageLink','Testimonial', 'City','NewsLetter', 'Project', 'PlanUser','ProjectInvoice');
		var $helpers = array('General', 'Form', 'Ajax', 'javascript', 'Paginator');
		var $components = array("Session", "Email", "Auth", "RequestHandler" ,"Upload","General",'Paginator','Security');
		function beforeFilter(){			
			parent::beforeFilter();		
			$this->Auth->allow('convertpdf','activate','resend_email','my_account','edit_account','account_setting','private_log','requestMentor','request_message','index','stripecheckout', 'striperecurr2','striperecurr3', 'striperecurrcancel','cancel_plan','admin_index','admin_add','admin_delete','admin_changepassword',
                        'admin_edit','give_feedback','admin_process','save_profileinfo','save_companydetails','save_companycontact');
		}


		function admin_index(){
    	if($this->Auth->user('id') == 1){

			$this->layout = 'admin';  
			if(!isset($this->request['named']['page'])){
				$this->Session->delete('Menteesearch');
			}
			$filters[] = array('User.role_id'=>3); 
			if( !empty($this->data)){			          
				$this->Session->delete('Menteesearch');
				$keyword = $this->data['UserReference']['first_name'];
				if(!empty($this->data['UserReference']['first_name'])){
					//App::import('Sanitize');
					//$keyword = Sanitize::escape($this->data['UserReference']['first_name']);
                                     $keyword = ($this->data['UserReference']['first_name']);
					$this->Session->write('Menteesearch', $keyword);				
				}
			}
			if($this->Session->check('Menteesearch')){
				$first_name = array('UserReference.first_name LIKE'=>"%".$this->Session->read('Menteesearch')."%");					
				$last_name = array('UserReference.last_name LIKE'=>"%".$this->Session->read('Menteesearch')."%");					
				$filters[] = array('OR'=>array($first_name,$last_name));
			}
			$this->Paginator->settings = array(
					'limit'=>20, 
					'order'=>array('User.id'=>'DESC'),
					'conditions'=>$filters
				);			
			$result = $this->Paginator->paginate('User');			       
			$this->set('data', $result);	 
			$this->set('title_for_layout', 'Client');
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
		}
		function admin_add() {
    	if($this->Auth->user('id') == 1){

			$this->layout = 'admin';  
			$this->set('title_for_layout', 'Add Client');
			if(!empty($this->data)) 
			{
				//prd($this->data);
				$this->request->data['User']['role_id'] 	= 3;
				$this->request->data['User'] 			= $this->General->myclean($this->data['User']);
				$this->request->data['UserReference'] 	= $this->General->myclean($this->data['UserReference']);

				$this->User->set($this->request->data);
				$this->User->setValidation('admin');
				$this->UserReference->set($this->request->data);
				$this->UserReference->setValidation('admin');
				//$this->data['User']['password'] = Security::hash($this->data['User']['password2'], null, true);
				$this->request->data['User']['password'] = 'mentorsguild';//Security::hash('mentorsguild', null, true);
				$this->request->data['User']['password2'] = Security::hash('mentorsguild', null, true);
				
				//$this->data['Availablity']['day_time'] = $this->General->convertAvailablityToDB($this->data['Availablity1']);
							
				//////////////add created by and modified by
				$this->_setCreatedByModifiedBy();
					
				if($this->User->saveAll($this->request->data,array('validate'=>'first'))){			
						
					$id = $this->User->getInsertID();;
					//$this->_insertFile(array('id'=>$id ,'userImage'=>$this->data['UserImage1']));					
					
										
					
					$this->Session->setFlash('Client has been saved', 'admin_flash_good');
					$this->redirect($this->referer());
									
					
				}else{
					$this->Session->setFlash('Please correct the errors listed below.', 'admin_flash_bad');
				}
				
				


			}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }			
			
		}
		function _insertFile($imageDataArr){
				//////uppload profile....
				$this->loadModel('UserImage');
				//prd($imageDataArr['userImage']);
				if(isset($imageDataArr['userImage']) && $imageDataArr['userImage']['image_name']['name'] !=''){
					
					$destination = IMAGES.MENTEES_IMAGE_PATH.DS.$imageDataArr['id'];
					if(!is_dir($destination)){
						mkdir($destination, 0777);
					}
					$destination .=DS;				
					if (!is_dir($destination.'small')) {
						mkdir($destination.'small', 0777);
					}					
					$imageArr = $imageDataArr['userImage']['image_name'];				
					$result = $this->Upload->upload($imageArr,$destination, null, array('type' => 'resizecrop', 'size' => array('100', '100'), 'output' => 'jpg'));
					$result = $this->Upload->upload($imageArr, $destination.'small/', $this->Upload->result, array('type' => 'resizecrop', 'size' => array('30', '30')));
					
					$data = array('image_name'=>$this->Upload->result,'user_id'=>$imageDataArr['id']);
					
					$this->UserImage->save($data);
					$this->Session->write('User.image', $this->Upload->result);
					//prd($data);			
				}

				

			
		
		}


		function _setCreatedByModifiedBy(){
			if(isset($this->data['Availablity'])){
				$this->data['Availablity']['created_by'] 				= 	$this->Auth->user('id');
				$this->data['Availablity']['modified_by'] 				= 	$this->Auth->user('id');				
			}

			$this->data['User']['created_by'] 						= 	$this->Auth->user('id');
			$this->data['User']['modified_by'] 						= 	$this->Auth->user('id');

			$this->data['UserReference']['created_by'] 				= 	$this->Auth->user('id');
			$this->data['UserReference']['modified_by'] 			= 	$this->Auth->user('id');
			if(isset($this->data['Social'])){
				foreach($this->data['Social'] as $key=>$value){
					$this->data['Social'][$key]['created_by'] 			= 	$this->Auth->user('id');
					$this->data['Social'][$key]['modified_by'] 			= 	$this->Auth->user('id');;
				}
			}

			if(isset($this->data['Communication'])){
				foreach($this->data['Communication'] as $key=>$value){
					$this->data['Communication'][$key]['created_by'] 	= 	$this->Auth->user('id');
					$this->data['Communication'][$key]['modified_by'] 	= 	$this->Auth->user('id');;
				}			
			}
		
		}


		function admin_edit($id = null) {
    	            if($this->Auth->user('id') == 1){
			
			$this->layout = 'admin';  
			if(!$id){
				$this->Session->setFlash('Client Id is Invalid', 'admin_flash_bad');
				$this->redirect($this->referer());
			}
			$this->set('title_for_layout', 'Edit Client');
			if(!empty($this->data)) 
			{
				
				$this->data['User']['role_id'] 	= 3;
				$this->data['User'] 			= $this->General->myclean($this->data['User']);
				$this->data['UserReference'] 	= $this->General->myclean($this->data['UserReference']);

				$this->User->set($this->data);
				$this->User->setValidation('admin');
				$this->UserReference->set($this->data);
				$this->UserReference->setValidation('admin');		
				
										
				//////////////add created by and modified by
				$this->_setCreatedByModifiedBy();				
				//////////////////////end here
				
		
				///////////////end here
				if($this->User->saveAll($this->data,array('validate'=>'first'))) 
				{
										
					$this->Session->setFlash('Client has been saved', 'admin_flash_good');
					//$this->redirect($this->referer());
                                        $this->redirect(array('action' => 'admin_index'));
				}else{
					$this->Session->setFlash('Please correct the errors listed below.', 'admin_flash_bad');
				}

			}else{
				$this->data = $this->User->read(Null,$id);
				
			}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
			
		}
		function _getUserImages($user_id = null){
			/* ==========get images for a particular useres=========== */
			if($user_id){
				$this->loadModel('UserImage');
				$this->UserImage->unbindModel(array('belongsTo'=>array('User')),false);
				$data = $this->UserImage->find('all',array('conditions'=>array('UserImage.user_id'=>$user_id)));
				$dataArr = array();
				if(!empty($data)){
					foreach($data as $key=>$value){
						$dataArr['UserImage'][] = $value['UserImage'];
					}
					$this->data['UserImage'] = $dataArr['UserImage'];
				}else{
					$this->data['UserImage'] = array();
				}
				
				//pr($this->data);die;
			}
			/* ==================end here=================== */		
		}		
		function admin_delete($id = null) {
    	if($this->Auth->user('id') == 1){
			
			$this->layout = 'admin';  
			if(!$id) {
				$this->Session->setFlash('Invalid id for NewsLetter');
				$this->redirect($this->referer());
			}
			$admin = $this->User->read(null, $id);
			if(empty($admin)) {
				$this->Session->setFlash('Invalid Client Id', 'admin_flash_bad');
				$this->redirect($this->referer());
			}

			if($this->User->delete($id)) {	 
				$delData = $this->deleteAllFolder($id); 
                $del = $this->deleteTablesData($id);
                
                $this->PlanUser->deleteAll(array('PlanUser.user_id' => $id));
                
				$this->Session->setFlash('Client has been deleted successfully', 'admin_flash_good');
				$this->redirect($this->referer());		
			}
			$this->Session->setFlash('Client has not been deleted', 'admin_flash_bad');
			$this->redirect($this->referer());
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
		}
        
        /* delete all table data related to users */
        function deleteTablesData($id = null)
        {
           $this->MessageLink->deleteAll(array('MessageLink.mentee_id'=>$id));
           $this->Message->deleteAll(array('Message.mentee_id'=>$id));
           $ids = $this->Mentorship->find('all',array('conditions'=>array('Mentorship.mentee_id'=>$id),'fields'=>array('Mentorship.id')));
           if(!empty($ids))
           {
              foreach($ids as $mentId)
              {
                  $mentIds[] = $mentId['Mentorship']['id']; 
                  $this->Feedback->deleteAll(array('Feedback.mentorship_id'=>$mentId['Mentorship']['id']));
                  $this->Mentorship->deleteAll(array('Mentorship.id'=>$mentId['Mentorship']['id']));
                  $this->Answer->deleteAll(array('Answer.mentorship_id'=>$mentId['Mentorship']['id']));
              }
           }           
        }
        function multipleDeleteRecord($ids)
        {
            foreach($ids as $id)
            {
                $this->deleteAllFolder($id);
                $this->deleteTablesData($id);
            }
        }
		function admin_process(){
			$this->layout = 'admin';  	
			if(!empty($this->data)){ 
				App::import('Sanitize');
				$action = Sanitize::escape($_POST['pageAction']);
					  
				foreach ($this->data['User'] AS $value) {	      
					if ($value != 0) {
						$ids[] = $value;				
					}
				}

				if (count($this->data) == 0 || $this->data['User'] == null) {
					$this->Session->setFlash('No items selected.', 'admin_flash_bad');
					$this->redirect(array('controller' => 'clients', 'action' => 'admin_index'));
				}
				if($action == "delete"){
					$this->User->deleteAll(array('User.id'=>$ids));        	
					$this->Session->setFlash('Client have been deleted successfully', 'admin_flash_good');
					$this->redirect(array('controller'=>'clients', 'action'=>'admin_index'));

				}
				if($action == "activate"){
					//echo Configure::read('App.Status.active');
					//prd($ids);
					$this->User->updateAll(array('User.status'=>1),array('User.id'=>$ids));
					$this->Session->setFlash('Client have been activated successfully', 'admin_flash_good');
					$this->redirect(array('controller'=>'clients', 'action'=>'admin_index'));
				}
				if($action == "deactivate"){
					$this->User->updateAll(array('User.status'=>Configure::read('App.Status.inactive')),array('User.id'=>$ids));
					$this->Session->setFlash('Client have been deactivated successfully', 'admin_flash_good');
					$this->redirect(array('controller'=>'clients', 'action'=>'admin_index'));
				}
			}else{
				$this->redirect(array('controller'=>'clients', 'action'=>'admin_index'));
			}
		}
		function admin_changepassword($id = null){
    	if($this->Auth->user('id') == 1){
			$this->layout = 'admin';  
		
			$this->set('title_for_layout', 'Change Password');     
			if (!$id && empty($this->data)) {
				$this->Session->setFlash(__('Invalid User', true));
				$this->redirect($this->referer());
			}
			if(!empty($this->data)){
				$this->User->set($this->data);
				$this->User->setValidation('change_password');
				if($this->User->validates()){
                                   $user = $this->User->find('all',array('conditions'=>array('User.id'=>$id)));
                                   $user['User']['password'] = $this->data['User']['password2'];//Security::hash($this->data['User']['password2'], null, true);
                                   $user['User']['password2'] = Security::hash($this->data['User']['password2'], null, true);
                                   $this->User->save($user,false);
					//$this->User->updateAll(array('password'=>"'".$this->data['User']['password']."'"), array('User.id'=>$id));
					$this->Session->setFlash('Password has been changed successfully', 'admin_flash_good');
					$this->redirect(array('controller'=>'clients', 'action'=>'admin_index'));
				}
			}else{
				$this->data = $this->User->read(null, $id);
			}   
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }       

		}
		function _randomPrefix($length)
		{
			$random= "";
			
			srand((double)microtime()*1000000);

			$data = "AbcDE123IJKLMN67QRSTUVWXYZ";
			$data .= "aBCdefghijklmn123opq45rs67tuv89wxyz";
			$data .= "0FGH45OP89";

			for($i = 0; $i < $length; $i++)
			{
				$random .= substr($data, (rand()%(strlen($data))), 1);
			}

			return $random;

		}
		
		function my_account($id=null)
		{


			$this->layout= 'defaultnew';


                       if($id=="")
			{
                            if($this->Session->read('Auth.User.role_id') == "2" || $this->Session->read('Auth.User.id') ==''){

                              $this->redirect(array('controller' => 'fronts', 'action' => 'index'));

                             }else{
                         
				$id=$this->Session->read('Auth.User.id');

                             }
			}
			
                        

			$this->User->Behaviors->attach('Containable');

                        if(strpos($id, ".")!== false){
                        $user = $this->User->find('first',array('conditions'=>array('User.url_key'=>$id)));
                        $id =  $user['User']['id'];

                        }                     

			
			$this->data = $this->User->find('first',
				array(
					'contain'=>array(
						'UserReference'=>array(
							'fields'=>array('first_name','last_name','background_summary','area_of_expertise','accept_application','desire_mentee_profile','fee_first_hour','fee_regular_session','created','zipcode','headline','phone','client_companyname','client_companydomain','client_companyaddress','client_companycontactname','client_companylogourl','client_companycontact','client_linkedinurl','client_companydetails','client_companydesignation'),
						),
						'UserImage'
						
					),
					'conditions'=>array('User.id'=>$id)		
				)
		
			);
 
                            if($this->data =='' || $this->data['User']['role_id'] !="3"){

                              $this->redirect(array('controller' => 'fronts', 'action' => 'index'));

                              }

			$this->loadModel('City');
		        $dataZip= $this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($this->data['UserReference']['zipcode']))));
		    
			$this->set("city",$dataZip['City']['city_name']);
			$this->set("state",$dataZip['City']['state']);

			$this->set("username",$this->data['User']['username']);
			$this->set("password",$this->data['User']['password2']);

                        $this->set("title_for_layout",$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name']);
			
			//finding number of invoices
			$mentee_id = $this->Session->read('Auth.User.id');
			$invoicedata = $this->ProjectInvoice->find('all', array('conditions' => array('ProjectInvoice.inv_client_id' => $mentee_id),'order'=>'ProjectInvoice.id DESC'));
                        $this->set('invoicedata',$invoicedata);

			
			//findinf number of projects
			$pQ = $this->Project->find('count', array('conditions' => array('Project.user_id' => $mentee_id)));
			if($pQ == 0) {
				$this->set("numberOfProjects", 0);
			} else {
				$this->set("numberOfProjects", 1);
			}
			
			$userTitle = $this->data['UserReference']['first_name'].' '.$this->data['UserReference']['last_name'];
			$this->set("title_for_layout",$userTitle);


                        $mentee_id = $this->Session->read('Auth.User.id');
                        $role_id = $this->Session->read('Auth.User.role_id');




	    	        $this->Paginator->settings = array('conditions'=>array('Project.user_id'=>$mentee_id),'order'=>'Project.id DESC');
	    	
	    	        $projectdata = $this->Paginator->paginate('Project');
	    	        $this->set(compact('projectdata'));

                        $this->set('mentorData',$mentorData);
                        $this->set('mentee_id',$mentee_id);

		}
		
	function edit_account($id= null){
		$this->layout= 'defaultnew';
	  if($this->Session->read('Auth.User.id')=='' || $this->Session->read('Auth.User.role_id')!='3')
            {
                $sending = SITE_URL."fronts/index";
                $this->Session->write('reqReschdule','user');
                echo "<script type='text/javascript'>window.location.href='".$sending."';loginpopup();</script>";
                exit;
            }
			$this->set("title_for_layout","Edit Profile");	
			$this->User->Behaviors->attach('Containable');
			if (empty($this->data)) 
			{	
				$this->User->Behaviors->attach('Containable');
				$this->data = $this->User->find('first',
					array(
						'contain'=>array(
							'UserReference'=>array(
								'fields'=>array('first_name','last_name','background_summary','accept_application','area_of_expertise','desire_mentee_profile','fee_first_hour','fee_regular_session','created','zipcode','application_5_fee','paypal_account','headline'),
							),
							'UserImage',

							'Communication',
							'Availablity',
							'Social',

						),
						'conditions'=>array('User.id'=>$this->Auth->user('id'))		
					)
			
				);


				$this->loadModel('City');
				$dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($this->data['UserReference']['zipcode']))));

				$this->set("city",$dataZip['City']['city_name']);
				$this->set("state",$dataZip['City']['state']);
				$this->set("resourcepath",IMAGES.MENTORS_RESOURCE_PATH.DS.$this->Auth->user('id').DS);
			}
			else
			{

				$user_ref = $this->UserReference->find('first',array('conditions' => array('UserReference.user_id' => $this->Auth->user('id')),'fields' =>array('UserReference.id')));
				$user_ref_id = $user_ref['UserReference']['id'];
				
				
				$user_ref_data	=	array();
				$user_ref_data['UserReference']['first_name']			=	$this->data['UserReference']['first_name'];
				$user_ref_data['UserReference']['last_name']			=	$this->data['UserReference']['last_name'];
				$user_ref_data['UserReference']['zipcode']				=	$this->data['UserReference']['zipcode'];
				$user_ref_data['UserReference']['background_summary']	=	$this->data['UserReference']['background_summary'];
				$user_ref_data['UserReference']['id']					=	$user_ref_id;
				$user_ref_data['UserReference']['headline']					=   $this->data['UserReference']['headline'];
				
				$this->UserReference->set($user_ref_data);
				$this->UserReference->save($user_ref_data);
				
				//// Social link edit START /////////////////
				$this->Social->deleteAll(array('Social.user_id' => $this->Session->read('Auth.User.id')));
				$linkCount=0;
				
				while(count($_REQUEST['link'])>$linkCount)
				{
					
					if(trim($_REQUEST['link'][$linkCount])!='')
					{
						$linkArray=array();
						$linkArray['Social']['user_id']=$this->Auth->user('id');
						$linkArray['Social']['social_name']=$_REQUEST['link'][$linkCount];
						$this->Social->create();
						$this->Social->save($linkArray,false);
					}
					$linkCount++;
				}
                        if(!(false !== stripos($this->data['UserReference']['last_name'],"'")  || false !== stripos($this->data['UserReference']['first_name'],"'")))
                        {
			$this->User->createUrlKey($this->Auth->user('id'));

                        }
                $this->Session->setFlash(__("Profile updated", true), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'my_account'));
			}
    	
  		}

		
		function account_setting()
		{
			$this->layout= 'defaultnew';
			$this->set('title_for_layout','Account Setting');
			$userId = $this->Session->read('Auth.User.id');
			if(empty($userId) || $this->Session->read('Auth.User.role_id')!='3')
				$this->redirect(array('controller'=>'fronts','action'=>'index'));
			if(!empty($this->data))
			{
                                        if($this->data['User']['username']!=''){

                                             $this->User->updateAll(array('User.username'=>"'".$this->data['User']['username']."'"),array('User.id'=>$userId));
                                         }

				$email = $this->Session->read('Auth.User.username');
				$type = $this->data['UserReference']['email_notification'];
				$this->__subscribenewsletter($email,$type);
				if($this->data['User']['oldpass']!='' && $this->data['User']['newpass']!='' && $this->data['User']['confpass']!='')
				{
					$oldPass = $this->data['User']['oldpass'];//Security::hash($this->data['User']['oldpass'], null, true);
					$userData = $this->User->find('first',array('fields'=>array('User.password'),'conditions'=>array('User.id'=>$userId),'recursive'=>'0'));
					if($this->data['User']['newpass']==$this->data['User']['confpass'])
					{
						$this->request->data['user']['id'] = $userId;
						$this->request->data['user']['password'] = $this->data['User']['newpass'];//Security::hash($this->data['User']['newpass'], null, true);
                                                $this->request->data['user']['password2'] = Security::hash($this->data['User']['newpass'], null, true);
						$this->User->save($this->request->data['user'],false);
 
					}
					else
					{
						$this->Session->write('pass_error', 'Password incorrect');
						$this->redirect($this->referer());
					}



				}
				$this->UserReference->save($this->data['UserReference']);



				$this->redirect(array('action'=>'my_account'));
			}
			$userData = $this->User->find('all',array('fields'=>array('UserReference.id,UserReference.user_id,UserReference.email_notification'),'conditions'=>array('User.id'=>$userId),'recursive'=>'0'));
			$this->set('userData',$userData[0]);
		}
		
		/* Activate mentee account */
		
		function activate($email = null, $activation_key = null)
        {      
                             $this->Session->write('popup', 1);
            $userData = $this->User->find('first', array('conditions' => array('User.role_id' => '3','User.activation_key' => $activation_key,'User.access_specifier' => 'draft'),'recursive'=>'0'));
           if (isset($userData) && count($userData['User'])>0){
                $saveUser['User']['id'] = $userData['User']['id'];
                $saveUser['User']['is_approved'] = '1';
                $saveUser['User']['activation_key'] =  substr(md5(uniqid()), 0, 20);    
                $saveUser['User']['access_specifier'] = 'publish';  
                $this->User->save($saveUser,false);
                $this->Session->destroy();

                $this->Session->write('menteeActEmail',$email);
                $this->Session->write('menteeRegister',$email);
                $this->redirect(array('controller'=>'users','action' => 'registration_step2'));             
            }else {
                $this->Session->setFlash(__('Invalid activation link, please contact site administrator.', true), 'flash_bad');
                $this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
            }
        }
		
		function resend_email()
		{
			if(isset($this->data['User']['id']) && $this->data['User']['id']!='')
			{
				$user = $this->User->findById($this->data['User']['id']);
				if(!empty($user) && count($user['User'])>0)
				{
					$act_key = substr(md5(uniqid()), 0, 20);
					
					$linkPassword = $this->General->randamPassword();
                    $this->data['User']['password'] =  Security::hash($linkPassword, null, true);
                    $this->data['User']['password'] = $linkPassword;
                    $savePass = $linkPassword;//Security::hash($linkPassword, null, true);
                    
					$this->User->id = $this->data['User']['id'];
					$this->User->saveField('activation_key', $act_key);
                    $this->User->saveField('password', $savePass);
					
					$this->data['User']['username'] = $user['User']['username'];
					$this->data['User']['activation_key'] = $act_key;
					$this->data['UserReference']['first_name'] = $user['UserReference']['first_name'];
					$this->data['UserReference']['last_name'] =$user['UserReference']['last_name'];
					$this->set('data',$this->data);
					//$this->Email->send();
					



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $user['User']['username'];
                           $subject = "Welcome to GUILD";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_register");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_useremail=".urlencode($user['User']['username']);
                           $data .= "&merge_userkey=".urlencode($act_key);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

					
					$this->set('menteeData',$this->data['User']['id']);
					$this->redirect(array('controller' => 'users', 'action' => 'thanksuser'));
				}
				else
					$this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
			}
		}
		
		
		/*************** Front page********************************** */
		function deleteAllFolder($id)
		{
			$dirnameImage = IMAGES . MENTEES_IMAGE_PATH . DS . $id;
			$dirnameResource = IMAGES . MENTEES_RESOURCE_PATH . DS . $id;
			$dirnameResume = IMAGES . MENTEES_RESUME_PATH . DS . $id;
			if(is_dir($dirnameImage))
			{
				$this->RmDirRecursive($dirnameImage); 
				rmdir($dirnameImage);
			}
			if(is_dir($dirnameResource))
			{
				$this->RmDirRecursive($dirnameResource); 
				rmdir($dirnameResource);
			}
			if(is_dir($dirnameResume))
			{
				$this->RmDirRecursive($dirnameResume); 
				rmdir($dirnameResume);
			}
			return true;
		}
		
		function RmDirRecursive($path) 
		{
			$realPath = realpath($path);
			if (!file_exists($realPath)) 
				return false;
			$DirIter = new RecursiveDirectoryIterator($realPath);	
			$fileObjects = new RecursiveIteratorIterator($DirIter, RecursiveIteratorIterator::CHILD_FIRST);
			foreach ($fileObjects as $name => $fileObj) {
				if ($fileObj->isDir()) 
					rmdir($name);
				else
					unlink($name);
			}
			return true;
		}
		
		function private_log($id=null)
		{
			$roleId = $this->Session->read('Auth.User.role_id');
			$userId = $this->Session->read('Auth.User.id');
			if(!empty($this->data))
			{
				if(isset($this->data['Message']['message']) && trim($this->data['Message']['message'])!='')
				{
					$this->data['Message']['sent_by'] = $this->Session->read('Auth.User.role_id');
					$this->data['Message']['isRead'] = 0; //0 -> UnRead, 1-> Read;
					$this->Message->save($this->data);
				}
                if($this->Session->read('Auth.User.role_id')==2)
                {
                    if(isset($this->data['MessageLink']['link_text']) && trim($this->data['MessageLink']['link_text'])!='')
                    {
                        $this->request->data['MessageLink']['mentor_id'] = $this->Session->read('Auth.User.id');
                        $this->request->data['MessageLink']['mentee_id'] = $this->data['Message']['mentee_id'];
                        $this->MessageLink->save($this->request->data['MessageLink']);
                    }
                    else
                    {
                        $condition = array('MessageLink.mentor_id' => $userId,'MessageLink.mentee_id' => $this->data['Message']['mentee_id']);
                        $this->MessageLink->deleteAll($condition);     
                    }
                }   
				$this->redirect(array('action'=>'private_log/'.$this->data['Message']['log_id']));
			}
			$this->Mentorship->bindModel(
				array(
				'belongsTo' => array(
					'Mentor' => array(
						'className' => 'UserReference',
						'foreignKey' => false,
						'conditions' =>array('Mentorship.mentor_id=Mentor.user_id'),
				 	),
					'Mentee' => array(
						'className' => 'UserReference',
						'foreignKey' => false,
						'conditions' =>array('Mentorship.mentee_id=Mentee.user_id'),
				 	)
					)));
			$this->Mentorship->unbindModel(array('hasMany'=>array('Answer')));
			$mentorship = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.id'=>$id),'fields'=>array('Mentorship.id','Mentorship.mentor_id','Mentorship.mentee_id','Mentor.first_name','Mentor.last_name','Mentee.first_name','Mentee.last_name','Mentorship.mentee_need')));
			$this->set('mentorship',$mentorship);
			if(empty($mentorship['Mentorship']))
			{
				if($roleId=='2')
					$this->redirect(array('controller'=>'users','action'=>'my_account'));
				if($roleId=='3')
					$this->redirect(array('action'=>'my_account'));
			}
			if($mentorship['Mentorship']['mentor_id']==$userId)
				$condition = array('Message.mentee_id'=>$mentorship['Mentorship']['mentee_id'],'Message.mentor_id'=>$userId);
            else if($mentorship['Mentorship']['mentee_id']==$userId)
				$condition = array('Message.mentee_id'=>$userId,'Message.mentor_id'=>$mentorship['Mentorship']['mentor_id']);
            else
                $this->redirect(array('controller'=>'fronts','action'=>'index'));
			
		    $messages = $this->Message->find('all',array('conditions'=>$condition,'order'=>'Message.id desc'));
		    
		    //Mark all messages as Read
		    foreach($messages as $msg)
		    {
		    	if(($msg['Message']['sent_by'] != $roleId) & $msg['Message']['isRead'] == 0)
		    	{
		    		$msg['Message']['isRead'] = 1; //0 -> UnRead, 1-> Read;
		    		$this->Message->save($msg);
		    	}
		    }
		    
            if($mentorship['Mentorship']['mentor_id']==$userId)
                $condLink = array('MessageLink.mentee_id'=>$mentorship['Mentorship']['mentee_id'],'MessageLink.mentor_id'=>$userId);
            if($mentorship['Mentorship']['mentee_id']==$userId)
                $condLink = array('MessageLink.mentee_id'=>$userId,'MessageLink.mentor_id'=>$mentorship['Mentorship']['mentor_id']);
            
            $linkData = $this->MessageLink->find('first',array('conditions'=>$condLink));
			$this->set('messages',$messages);
            $this->set('linkData',$linkData);
       }
			
		/* Request functionality for mentor start */
		function requestMentor()
		{
			$this->layout = '';
			$this->render(false);
			if ($this->RequestHandler->isAjax())
			{
				$tempData = array('mentee_id'=>$this->Auth->user('id'),'mentor_id'=>$_REQUEST['id']);
                $this->TempRequest->save($tempData);
                $menteeData = $this->UserReference->find('first',array('conditions'=>array('UserReference.user_id'=>$this->Session->read('Auth.User.id')),'fields'=>array('UserReference.first_name','UserReference.last_name')));
                $mentorData = $this->UserReference->find('first',array('conditions'=>array('UserReference.user_id'=>$_REQUEST['id']),'fields'=>array('UserReference.first_name','UserReference.last_name')));
                $data = array(
                                'MenteeFName'=>$menteeData['UserReference']['first_name'],
                                'MenteeLName'=>$menteeData['UserReference']['last_name'],
                                'MentorFName'=>$mentorData['UserReference']['first_name'],
                                'MentorLName'=>$mentorData['UserReference']['last_name']
                              );
				/* Request mail to mentee */
				/*$this->Email->to = $this->Session->read('Auth.User.username');
                $this->Email->from = Configure::read('Site.email');
                $this->set('data',$data);
                $this->Email->subject = 'Mentorship request submitted';
                $this->Email->template = 'request_sent';
                $this->Email->sendAs = 'html';
                $this->Email->send();*/
                


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $this->Session->read('Auth.User.username');
                           $subject = "Consultation request submitted";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("request_sent");
                           $data .= "&merge_clientfname=".urlencode($data[MenteeFName]);
                           $data .= "&merge_consultantfname=".urlencode($data[MentorFName]);
                           $data .= "&merge_consultantlname=".urlencode($data[MentorLName]);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

				
				echo json_encode(array('value' => '1'));
				exit();
			}
		}
		
		function request_message()
		{
			$this->layout = 'ajax';
		}
		/* Request functionality for mentor end */
		
		function mentorshipApply()
		{
			$this->layout = '';
			$this->render(false);
			if ($this->RequestHandler->isAjax())
			{
				if($_REQUEST['action']=='accept')
				{
					$this->Mentorship->id = $_REQUEST['id'];
					$this->Mentorship->saveField('applicationType', '3');
					echo json_encode(array('value' => '1'));
					exit();							
				}
				if($_REQUEST['action']=='reject')
				{
					$this->Mentorship->id = $_REQUEST['id'];
					$this->Mentorship->saveField('applicationType', '6');
					echo json_encode(array('value' => '2'));
					exit();
				}
			}
		}
		function give_feedback($mentor_id = null,$mentorship_id=null)
		{
                      $this->layout = 'ajax';
                     $userId = $this->Session->read('Auth.User.id');
			if(!empty($this->data)){
				if($this->Feedback->save($this->data)){

					$mentor_id = $this->data['Feedback']['member_id'];
                                   $mentor = $this->User->find('first', array( 'contain'=>array( 'UserReference'=>array('fields'=>array('first_name','last_name'))),'conditions'=>array('User.id'=>$mentor_id)));
					if($this->Session->read('Auth.User.role_id') == '2'){
					$mentee = $this->User->find('first', array( 'contain'=>array( 'UserReference'=>array('fields'=>array('first_name','last_name','linkedin_headline'))),'conditions'=>array('User.id'=>$userId)));

					$client_name = $mentee['UserReference']['first_name'].' '.$mentee['UserReference']['last_name'];

                                   $testimonial['Testimonial']['user_id'] = $mentor_id;
                                   $testimonial['Testimonial']['testimonial_detail'] = $this->data['Feedback']['description'];
                                   $testimonial['Testimonial']['client_name'] = $client_name;
                                   $testimonial['Testimonial']['client_designation'] = $mentee['UserReference']['linkedin_headline'];
                                   $testimonial['Testimonial']['client_organization'] = "MG-Consultant";
                                   $this->Testimonial->save($testimonial);

					}else{
                                   $mentee = $this->User->find('first', array( 'contain'=>array( 'UserReference'=>array('fields'=>array('first_name','last_name','headline'))),'conditions'=>array('User.id'=>$userId)));


					$client_name = $mentee['UserReference']['first_name'].' '.$mentee['UserReference']['last_name'];

                                   $testimonial['Testimonial']['user_id'] = $mentor_id;
                                   $testimonial['Testimonial']['testimonial_detail'] = $this->data['Feedback']['description'];
                                   $testimonial['Testimonial']['client_name'] = $client_name;
                                   $testimonial['Testimonial']['client_designation'] = $mentee['UserReference']['headline'];
                                   $testimonial['Testimonial']['client_organization'] = "MG-Client";
                                   $this->Testimonial->save($testimonial);
	                         
                                   }

					
					
			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "iqbal@guild.im";
                           $subject = "Feedback received";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_feedback");
                           $data .= "&merge_consultantfname=".urlencode($consultantfname);
                           $data .= "&merge_clientfname=".urlencode($mentee['UserReference']['first_name']);
                           $data .= "&merge_clientlname=".urlencode($mentee['UserReference']['last_name']);
                           $data .= "&merge_consultanturlkey=".urlencode($mentor['User']['url_key']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

					$this->Session->setFlash(__('Your Feedback has been submitted', true), 'default', array('class' => 'success'));
					if($this->Session->read('Auth.User.role_id') == 3)
						$this->redirect(array('controller'=>'clients','action' => 'my_account'));
					else if ($this->Session->read('Auth.User.role_id') == 2)
						$this->redirect(array('controller'=>'users','action' => 'my_account'));
				 
				}else{
					$this->Session->setFlash(__('Error! Please try again', true), 'default', array('class' => 'success'));
					$this->redirect(array('controller'=>'clients','action' => 'my_account'));
				}
			}else{
				$data = $this->Feedback->find('first',array('conditions' => array('Feedback.client_id'=>$this->Session->read('Auth.User.id'), 'Feedback.member_id'=>$mentor_id)));
				$this->set('data',$data);
				$this->set('client_id', $this->Session->read('Auth.User.id'));
				$this->set('member_id', $mentor_id);
			}
		}
    
    function convertpdf()
    {
        
    }
	
	function __subscribenewsletter($email=null,$notify=null)
	{
		if($email!="")
		{
			$newsletterData = $this->NewsLetter->find('first',array('conditions'=>array('NewsLetter.email'=>$email)));
			if(empty($newsletterData))
			{
				$newsData['NewsLetter']['email'] = $email;
				$newsData['NewsLetter']['status'] = '1'; 
				$this->NewsLetter->save($newsData);
			}
			if($notify!="" && $notify=='N')
			{
				$conditions = array('NewsLetter.email'=>$email);
				$this->NewsLetter->deleteAll($conditions);
			}
		}
	} 

             function striperecurrcancel(){
		
		Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");
		
		$user = $this->User->find('first',array('conditions'=>array('User.username'=>$this->Auth->user('username'))));
		
		$customer = Stripe_Customer::retrieve($user['User']['clientPaymentId']);
		
		$subscription = $customer->subscriptions;
		
		$subscription['data'][0]->cancel();
		
		$user['User']['plan_type'] = "Basic";
		$user['User']['clientPaymentId'] = '';
		
		$this->User->save($user);

              //  sending  mail to client after publish profile 
  
             $mailData = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
                        

		
			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $iqbalemail = "iqbal@guild.im";
                           $to = $this->Auth->user('username').";".$iqbalemail;
                           $subject = "Your Business Plan has been canceled";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("cancel_business_plan");
                           $data .= "&merge_userfname=".urlencode($mailData['UserReference']['first_name']);
                           $data .= "&merge_link=".urlencode("pricing");
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

		$this->Session->write('Auth', $user);
		$this->Session->setFlash(__("You are now a Basic Client.", true), 'default', array('class' => 'success'));
		$this->redirect(array('controller'=>'pricing','action'=>'pricing'));
		
	}
	
	function striperecurr2($id = null) {
	      
               if($id == 1){
               
               $this->Session->write('RedirectTo','Client_edit');
               }
               if($id == 2){
               $this->Session->write('RedirectTo','Pricing');
               }

		if(!empty($_POST['stripeToken'])) {
	
			// Set your secret key: remember to change this to your live secret key in production
			// See your keys here https://dashboard.stripe.com/account
			Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");
	
			// Get the credit card details submitted by the form
			$token = $_POST['stripeToken'];
				
			// Create the charge on Stripe's servers - this will charge the user's card
			try {
				$customer = Stripe_Customer::create(array(
					"card" => $token,
					"plan" => "mg_premium_495",
					"email" => $this->Auth->user('username'))
				);
	
				$user = $this->User->find('first',array('conditions'=>array('User.username'=>$this->Auth->user('username'))));
				
				$user['User']['plan_type'] = "Business";
				$user['User']['clientPaymentId'] = $customer['id'];
				
				$this->User->save($user);
                         
                            //  sending  mail to client after publish profile   
                        $mailData = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
                        


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $iqbalemail = "iqbal@guild.im";
                           $to = $this->Auth->user('username').";".$iqbalemail;
                           $subject = "Welcome to our Business Membership!";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("business_plan");
                           $data .= "&merge_userfname=".urlencode($mailData['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

                            $this->Session->write('Auth', $user);

                            if($this->Session->read('RedirectTo') == 'Client_edit'){
                            $this->Session->setFlash(__("You are now enrolled to the Business plan.", true), 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'clients','action'=>'edit_account'));
				

	                     }if($this->Session->read('RedirectTo') == 'Pricing'){
                            $this->Session->setFlash(__("You are now enrolled to the Business plan.", true), 'default', array('class' => 'success'));
                            $this->redirect(array('controller'=>'pricing','action'=>'pricing'));
                            

                            }
                            $this->Session->delete('RedirectTo');
			} catch(Stripe_CardError $e) {
				pr("There is some error");
				prd($e);
				// The card has been declined
			}
		} else {
				
			$this->set("title_for_layout","Upgrade to business plan");
			$this->User->Behaviors->attach('Containable');
			$this->data = $this->User->find('first',array('contain'=>array('UserReference'=>array('fields'=>array('zipcode','first_name','last_name','background_summary')),'UserImage'),'conditions'=>array('role_id'=>3,'User.id'=>$this->Session->read('Auth.User.id'),'status'=>1)));			
		}
	}


	function striperecurr3($id = null) {
	      
               if($id == 1){
               
               $this->Session->write('RedirectTo','Client_edit');
               }
               if($id == 3){
               $this->Session->write('RedirectTo','Pricing');
               }

		if(!empty($_POST['stripeToken'])) {
	
			// Set your secret key: remember to change this to your live secret key in production
			// See your keys here https://dashboard.stripe.com/account
			Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");
	
			// Get the credit card details submitted by the form
			$token = $_POST['stripeToken'];
				
			// Create the charge on Stripe's servers - this will charge the user's card
			try {
				$customer = Stripe_Customer::create(array(
					"card" => $token,
					"plan" => "mg_premium_995",
					"email" => $this->Auth->user('username'))
				);
	
				$user = $this->User->find('first',array('conditions'=>array('User.username'=>$this->Auth->user('username'))));
				
				$user['User']['plan_type'] = "Enterprise";
				$user['User']['clientPaymentId'] = $customer['id'];
				
				$this->User->save($user);
                         
                            //  sending  mail to client after publish profile   
                        $mailData = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
                        


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $iqbalemail = "iqbal@guild.im";
                           $to = $this->Auth->user('username').";".$iqbalemail;
                           $subject = "Welcome to our Enterprise Membership!";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("enterprise_plan");
                           $data .= "&merge_userfname=".urlencode($mailData['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


                            $this->Session->write('Auth', $user);

                            if($this->Session->read('RedirectTo') == 'Client_edit'){
                            $this->Session->setFlash(__("You are now enrolled to the Business plan.", true), 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'clients','action'=>'edit_account'));
				

	                     }if($this->Session->read('RedirectTo') == 'Pricing'){
                            $this->Session->setFlash(__("You are now enrolled to the Enterprise plan.", true), 'default', array('class' => 'success'));
                            $this->redirect(array('controller'=>'pricing','action'=>'pricing'));
                            

                            }
                            $this->Session->delete('RedirectTo');
			} catch(Stripe_CardError $e) {
				pr("There is some error");
				prd($e);
				// The card has been declined
			}
		} else {
				
			$this->set("title_for_layout","Upgrade to business plan");
			$this->User->Behaviors->attach('Containable');
			$this->data = $this->User->find('first',array('contain'=>array('UserReference'=>array('fields'=>array('zipcode','first_name','last_name','background_summary')),'UserImage'),'conditions'=>array('role_id'=>3,'User.id'=>$this->Session->read('Auth.User.id'),'status'=>1)));			
		}
	}
	

	function stripecheckout($fee = null, $message = null) {
		
		if(!empty($_POST['stripeToken'])) {
		
			// Set your secret key: remember to change this to your live secret key in production
			// See your keys here https://dashboard.stripe.com/account
			Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");
		
			// Get the credit card details submitted by the form
			$token = $_POST['stripeToken'];
			$amount = $_POST['FEE'];

			// Create the charge on Stripe's servers - this will charge the user's card
			try {
				$charge = Stripe_Charge::create(array(
						"amount" => $amount, // amount in cents, again
						"currency" => "usd",
						"card" => $token,
						"description" => $this->Auth->user('email'))
				);
				
				//pr("Charged");
				//prd($charge);
				
				$this->redirect(array('controller'=>'pricing','action'=>'actionAfterPaymentSuccess'));
				
			} catch(Stripe_CardError $e) {
				pr("There is some error");
				prd($e);
				// The card has been declined
			}
		} else {
			
			$centFee = $fee * 100;
			
			$this->set('centFee', $centFee);
			$this->set('fee', $fee);
			$this->set('message', $message);
			
		}
		//$this->layout=false;
	}
 
      function cancel_plan(){
				
			$this->set("title_for_layout","Cancel business plan");
			$this->User->Behaviors->attach('Containable');
			$this->data = $this->User->find('first',array('contain'=>array('UserReference'=>array('fields'=>array('zipcode','first_name','last_name','background_summary')),'UserImage'),'conditions'=>array('role_id'=>3,'User.id'=>$this->Session->read('Auth.User.id'),'status'=>1)));			
		     }
	




          function save_profileinfo(){



		if($this->Auth->user('id') != "") {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);
				$fname = ucfirst($this->request->data['fname']);
				$lname = ucfirst($this->request->data['lname']);
				$zipcode = $this->request->data['zipcode'];
                                $profileheadline= $this->request->data['profileheadline'];
                                $phonenumber= $this->request->data['phonenumber'];
                                $linkedinurl= $this->request->data['linkedinurl'];


				
				$mentor_data = $this->User->find('first',
						array(
							'contain'=>array(
								'UserReference'=>array(
									'fields'=>array('first_name','last_name','zipcode','headline','phone','client_linkedinurl','client_companydesignation'),
								),
								
							),
							'conditions'=>array('User.id'=>$this->Auth->user('id'))		
						)
				
					);
				
				$mentor_data['UserReference']['first_name']			=	$fname;
				$mentor_data['UserReference']['last_name']			=	$lname;
				$mentor_data['UserReference']['zipcode']			=	$zipcode;
                                $mentor_data['UserReference']['client_companydesignation']    =   $profileheadline;
                                $mentor_data['UserReference']['phone']    =   $phonenumber;
                                $mentor_data['UserReference']['client_linkedinurl']    =   $linkedinurl;

				$this->UserReference->save($mentor_data['UserReference'], false);
 
			        $this->loadModel('City');
		                $dataZip= $this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($zipcode))));
		                $location = $dataZip['City']['city_name'].", ".$dataZip['City']['state'];


			



	
				$this->layout = '';
				$this->render(false);
			        echo json_encode(array('value' => 1,'location'=>$location));
				exit();
			}
		}

          }

          function save_companydetails(){



		if($this->Auth->user('id') != "") {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);
				$companyname = ucfirst($this->request->data['companyname']);
				$companydomain = $this->request->data['companydomain'];
				$companydetails = $this->request->data['companydetails'];


				
				$mentor_data = $this->User->find('first',
						array(
							'contain'=>array(
								'UserReference'=>array(
									'fields'=>array('client_companyname','client_companydomain','client_companydetails'),
								),
								
							),
							'conditions'=>array('User.id'=>$this->Auth->user('id'))		
						)
				
					);
				
				$mentor_data['UserReference']['client_companyname']			=	$companyname;
				$mentor_data['UserReference']['client_companydomain']			=	$companydomain;
				$mentor_data['UserReference']['client_companydetails']			=	$companydetails;

				$this->UserReference->save($mentor_data['UserReference'], false);
 


			



	
				$this->layout = '';
				$this->render(false);
			        echo json_encode(array('value' => 1));
				exit();
			}
		}

          }


          function save_companycontact(){



		if($this->Auth->user('id') != "") {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);
				$companycontactname = ucfirst($this->request->data['companycontactname']);
				$companycontactnumber = ucfirst($this->request->data['companycontactnumber']);
				$companycontactaddress = $this->request->data['companycontactaddress'];


				
				$mentor_data = $this->User->find('first',
						array(
							'contain'=>array(
								'UserReference'=>array(
									'fields'=>array('client_companycontactname','client_companycontact','client_companyaddress'),
								),
								
							),
							'conditions'=>array('User.id'=>$this->Auth->user('id'))		
						)
				
					);
				
				$mentor_data['UserReference']['client_companycontactname']			=	$companycontactname;
				$mentor_data['UserReference']['client_companycontact']			=	$companycontactnumber;
				$mentor_data['UserReference']['client_companyaddress']			=	$companycontactaddress;

				$this->UserReference->save($mentor_data['UserReference'], false);
 


			



	
				$this->layout = '';
				$this->render(false);
			        echo json_encode(array('value' => 1));
				exit();
			}
		}

          }






       }

	?>