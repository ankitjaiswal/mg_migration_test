<?php

/**
 * Mentors Controller
 *
 * PHP version 5
 *
 * @category Controller
 */
class ReportsController extends AppController {

    /**
     * Controller name
     * @var string
     * @access public
     */
    var $name = 'Reports';
    /**
     * Models used by the Controller
     * @var array
     * @access public
     */
    var $uses = array('User', 'UserReference','Mentorship',
    'Invoice','Message','MessageLink','Communication', 'Keywords', 'Referrals','Topic');
    var $helpers = array('General','Front','Paginator');
    var $components = array('Upload','RequestHandler','Paginator');
    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('admin_userlist','admin_mentorlist','admin_mentor_static','admin_programlist','admin_weeklyprogramlist','admin_invoicelist','admin_userlog','admin_userlog','admin_loghistory','admin_mentorstatics','admin_search','admin_referral','admin_autocomplete','admin_editautocompletetext','admin_deleteautocompletetext');
    }
    
    /* admin functionality related to reporting */
        
    function admin_userlist()
    {
    	if($this->Auth->user('id') == 1){

      $this->layout = 'admin'; 
    	$this->User->unbindModel(array('hasOne'=>array('Availablity'),'hasMany'=>array('UserImage','Resume','Social','Question','Communication')),false);
        $this->Paginator->settings = array(
            'limit' => 20,
            'order' => array('User.id' => 'DESC'),
            'conditions'=>array('User.id!=1'),
        );
        $result = $this->Paginator->paginate('User');        
        $this->set('data', $result);
         }else{
        $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
         }
    }
    
    function admin_mentorlist()
    {
    	if($this->Auth->user('id') == 1){

             $this->layout = 'admin'; 
       $this->User->unbindModel(array('hasOne'=>array('Availablity'),'hasMany'=>array('UserImage','Resume','Social','Question','Communication')),false);
       $this->Paginator->settings = array(
            'limit' => '100',
            'order' => array('User.id' => 'DESC'),
            'conditions'=>array('User.role_id=2'),
            'limit' => Configure::read('App.AdminPageLimit'),
        );
        $result = $this->Paginator->paginate('User');        
        $this->set('data', $result);
         }else{
        $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
         }
    }
    
    
    function admin_mentor_static($mentorId=null)
    {

    	if($this->Auth->user('id') == 1){

              $this->layout = 'admin'; 
        $menteeCount = 0; $SessionCount = 0; $weekAmount = 0; $totAmount = 0; $totCommission = 0;
        $start_date = date('Y-m-d',time()+( 1 - date('w'))*24*3600);
        $end_date = date('Y-m-d');
        $condition[] = array('Invoice.modified >= '=>$start_date.' 00:00:00'); 
        $condition[] = array('Invoice.modified <= '=>$end_date.' 23:59:59');
        
        if(!empty($this->data))
        {
            $st_date = explode("/",$this->data['Mentor']['start']);
            $ed_date = explode("/",$this->data['Mentor']['end']);
            $searchStartDate = $st_date[2].'-'.$st_date[0].'-'.$st_date[1];
            $searchEndDate = $ed_date[2].'-'.$ed_date[0].'-'.$ed_date[1];
            
            $searchCondition[] = array('Mentorship.modified >= '=>$searchStartDate.' 00:00:00'); 
            $searchCondition[] = array('Mentorship.modified <= '=>$searchEndDate.' 23:59:59');
            $searchCondition['Mentorship.mentor_id'] =  $mentorId;
            $searchCondition['Mentorship.is_deleted'] = '0';
            
           // $menteeCount = $this->Mentorship->find('count',array('conditions'=>$searchCondition,'group'=>'Mentorship.mentee_id'));
            $mentee = $this->Mentorship->query('SELECT count(distinct(`Mentorship`.`mentee_id`)) as countMentee 
                        FROM `mentorships` AS `Mentorship` 
                        WHERE `Mentorship`.`mentor_id` ='.$mentorId.' AND modified>=\''.$searchStartDate.'00:00:00'.'\'  AND modified<=\''.$searchEndDate.'23:59:59'.'\' AND `Mentorship`.`is_deleted`!=1');
            $menteeCount = $mentee[0][0]['countMentee'];
            $SessionCount = $this->Mentorship->find('count',array('conditions'=>$searchCondition));
            $ids = $this->Mentorship->find('all',array('conditions'=>$searchCondition,'fields'=>array('Mentorship.id')));
            if(!empty($ids))
             {
                 
                foreach($ids as $mentId)
                {
                    $mentIds[] = $mentId['Mentorship']['id']; 
                }
                $condition['Invoice.mentorship_id'] =  $mentIds;
                
                $totalEarning = $this->Invoice->find('first',array('conditions'=>array('Invoice.mentorship_id'=>$mentIds),'fields'=>array('sum(((Invoice.total) - ((Invoice.total * Invoice.rate)/100))) AS Amount,sum((Invoice.total * Invoice.rate)/100) as commission ')));
                $weekEarning = $this->Invoice->find('first',array('conditions'=>$condition,'fields'=>array('sum(((Invoice.total) - ((Invoice.total * Invoice.rate)/100))) AS amount')));
                
                if($totalEarning[0]['Amount']!='')
                    $totAmount= $totalEarning[0]['Amount'];
                if($totalEarning[0]['commission']!='')
                    $totCommission= $totalEarning[0]['commission'];
                if($weekEarning[0]['amount']!='')
                    $weekAmount = $weekEarning[0]['amount'];
             }
        }
        if(empty($this->data))
        {
            //$menteeCount = $this->Mentorship->find('count',array('conditions'=>array('Mentorship.mentor_id'=>$mentorId,'Mentorship.is_deleted!=1'),'group'=>'Mentorship.mentee_id'));
            $mentee = $this->Mentorship->query("SELECT count(distinct(`Mentorship`.`mentee_id`)) as countMentee FROM `mentorships` AS `Mentorship`   WHERE `Mentorship`.`mentor_id` =".$mentorId." AND `Mentorship`.`is_deleted`!=1");
            $menteeCount = $mentee[0][0]['countMentee'];
            $SessionCount = $this->Mentorship->find('count',array('conditions'=>array('Mentorship.mentor_id'=>$mentorId,'Mentorship.is_deleted!=1')));
            $ids = $this->Mentorship->find('all',array('conditions'=>array('Mentorship.mentor_id'=>$mentorId,'Mentorship.is_deleted!=1'),'fields'=>array('Mentorship.id')));
            if(!empty($ids))
             {
                 
                foreach($ids as $mentId)
                {
                    $mentIds[] = $mentId['Mentorship']['id']; 
                }
                $totalEarning = $this->Invoice->find('first',array('conditions'=>array('Invoice.mentorship_id'=>$mentIds),'fields'=>array('sum(((Invoice.total) - ((Invoice.total * Invoice.rate)/100))) AS Amount,sum((Invoice.total * Invoice.rate)/100) as commission ')));
                $condition['Invoice.mentorship_id'] =  $mentIds;
                
                $weekEarning = $this->Invoice->find('first',array('conditions'=>$condition,'fields'=>array('sum(((Invoice.total) - ((Invoice.total * Invoice.rate)/100))) AS amount')));
                if($totalEarning[0]['Amount']!='')
                    $totAmount= $totalEarning[0]['Amount'];
                if($totalEarning[0]['commission']!='')
                    $totCommission= $totalEarning[0]['commission'];
                if($weekEarning[0]['amount']!='')
                    $weekAmount = $weekEarning[0]['amount'];
             }
        }
        $data['amount'] = $totAmount;
        $data['commission'] = $totCommission;
        $data['weekearning'] = $weekAmount;
        $data['menteeCount'] = $menteeCount;
        $data['sessionCount']= $SessionCount;
        $data['userId']= $mentorId;
        $data['mentorId'] = $mentorId;
        $this->set('data',$data);
         }else{
        $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
         }
    } 

   /* function admin_programlist()
    {
        $searchStartDate = "";
        $searchEndDate   =  "";
        if(!empty($this->data))
        {
            $st_date = explode("/",$this->data['Mentor']['start']);
            $ed_date = explode("/",$this->data['Mentor']['end']);
            $searchStartDate = "'".$st_date[2].'-'.$st_date[0].'-'.$st_date[1]." 00:00:00"."'";
            $searchEndDate = "'".$ed_date[2].'-'.$ed_date[0].'-'.$ed_date[1]." 23:59:59"."'";
            $data = $this->Mentorship->query('(
                                SELECT A.*,invoices.id as inv_id,invoices.inv_create_status as inv_status,invoices.title as inv_title,invoices.inv_date as inv_date,invoices.inv_time as inv_time,invoices.total as inv_total
                                FROM mentorships as A left join invoices on (A.id = invoices.mentorship_id)
                                WHERE NOT EXISTS(SELECT * FROM mentorships as B WHERE B.parent_id = A.id) AND A.parent_id =0 AND A.applicationType!=6 
                                AND A.is_deleted!=1 AND (A.modified>='.$searchStartDate.' AND A.modified<='.$searchEndDate.')
                                )
                                UNION
                                (
                                SELECT B.*,invoices.id as inv_id,invoices.inv_create_status as inv_status,invoices.title as inv_title,invoices.inv_date as inv_date,invoices.inv_time as inv_time,invoices.total as inv_total
                                FROM mentorships AS B left join invoices on (B.id = invoices.mentorship_id)
                                
                                WHERE B.id IN(
                                SELECT max(id)
                                FROM mentorships as A
                                WHERE EXISTS(SELECT * FROM mentorships as B WHERE A.parent_id = B.id) GROUP BY A.parent_id) AND B.applicationType!=6 AND B.is_deleted!=1
                                AND (B.modified>='.$searchStartDate.' AND B.modified<='.$searchEndDate.')
                                )  order by id desc');
        }    
        if(empty($this->data))
        {   
            $data = $this->Mentorship->query('(
                                SELECT A.*,invoices.id as inv_id,invoices.inv_create_status as inv_status,invoices.title as inv_title,invoices.inv_date as inv_date,invoices.inv_time as inv_time,invoices.total as inv_total
                                FROM mentorships as A left join invoices on (A.id = invoices.mentorship_id)
                                WHERE NOT EXISTS(SELECT * FROM mentorships as B WHERE B.parent_id = A.id) AND A.parent_id =0 AND A.applicationType!=6 AND A.is_deleted!=1
                                )
                                UNION
                                (
                                SELECT B.*,invoices.id as inv_id,invoices.inv_create_status as inv_status,invoices.title as inv_title,invoices.inv_date as inv_date,invoices.inv_time as inv_time,invoices.total as inv_total
                                FROM mentorships AS B left join invoices on (B.id = invoices.mentorship_id)
                                
                                WHERE B.id IN(
                                SELECT max(id)
                                FROM mentorships as A
                                WHERE EXISTS(SELECT * FROM mentorships as B WHERE A.parent_id = B.id) GROUP BY A.parent_id) AND B.applicationType!=6 AND B.is_deleted!=1
                                )  order by id desc ');
        }
        $this->set('startDate',$searchStartDate);
        $this->set('endDate',$searchEndDate);
        $this->set('data',$data);
    }*/

    function admin_programlist()
    {
    	if($this->Auth->user('id') == 1){


             $this->layout = 'admin'; 
        $searchStartDate = "";
        $searchEndDate   =  ""; 
        if(!empty($this->data))
        {
            $st_date = explode("/",$this->data['Mentor']['start']);
            $ed_date = explode("/",$this->data['Mentor']['end']);
            $searchStartDate =$st_date[2].'-'.$st_date[0].'-'.$st_date[1]." 00:00:00";
            $searchEndDate = $ed_date[2].'-'.$ed_date[0].'-'.$ed_date[1]." 23:59:59";
            $condition[] = array('Mentorship.modified >='=>$searchStartDate); 
            $condition[] = array('Mentorship.modified <='=>$searchEndDate); 
        }
        else
        {
            $condition[] = array('Mentorship.modified >='=>'1970-01-01 00:00:00'); 
            $condition[] = array('Mentorship.modified <='=>date('Y-m-d').' 23:59:59');
        }
        $mentorIds = $this->Mentorship->find('list',array('conditions'=>$condition,'recursive'=>0,'fields'=>'Mentorship.mentor_id','group'=>'mentor_id'));
        $mainArray = array();
        $data = array();
        foreach($mentorIds as $ids)
        {
             $menteeIds = $this->Mentorship->find('all',array('conditions'=>array('Mentorship.mentor_id'=>$ids,$condition),'recursive'=>'0','fields'=>'Mentorship.mentee_id,Mentorship.id','group'=>'Mentorship.mentee_id','order'=>'Mentorship.id asc'));
             foreach($menteeIds as $mentee)
             {
                $this->Mentorship->bindModel(
                            array(
                            'belongsTo' => array(
                                'Invoice' => array('className' => 'Invoice','foreignKey' => false,'conditions'=>array('Mentorship.id=Invoice.mentorship_id')),
                                )));
                 $menteedata = $this->Mentorship->find('all',array('conditions'=>array('Mentorship.mentee_id'=>$mentee['Mentorship']['mentee_id'],'Mentorship.mentor_id'=>$ids,$condition),'fields'=>array('sum(Mentorship.paypal_fee) as fee,sum(Invoice.total) as total'),'recursive'=>0));
                 $menteedata[0]['mentor_id'] = $ids;
                 $menteedata[0]['mentee_id'] = $mentee['Mentorship']['mentee_id'];
                 $menteedata[0]['id'] = $mentee['Mentorship']['id'];
                 $data[] = $menteedata[0];
             }
        }
        $this->set('data',$data);
         }else{
        $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
         }
    }
    
    
    function admin_weeklyprogramlist()
    {
    	if($this->Auth->user('id') == 1){

              $this->layout = 'admin'; 
        $appSent = 0; $appAccept = 0; $orderConfirm = 0; $intialConultation = 0; $session1 =0 ; $session2 = 0; $otherSession = 0; $disputes = 0;
        $start_date = date('Y-m-d',time()+( 1 - date('w'))*24*3600);
        $end_date = date('Y-m-d');
        $condition[] = array('Mentorship.created >= '=>$start_date.' 00:00:00'); 
        $condition[] = array('Mentorship.created <= '=>$end_date.' 23:59:59');
        
        $conditionInvoice[] = array('Invoice.modified >= '=>$start_date.' 00:00:00'); 
        $conditionInvoice[] = array('Invoice.modified <= '=>$end_date.' 23:59:59');
        
        if(!empty($this->data))
        {
        	$conditionInvoice = '';$condition='';
            $st_date = explode("/",$this->data['Mentor']['start']);
            $ed_date = explode("/",$this->data['Mentor']['end']);
            $searchStartDate = $st_date[2].'-'.$st_date[0].'-'.$st_date[1];
            $searchEndDate = $ed_date[2].'-'.$ed_date[0].'-'.$ed_date[1];
            
            $condition[] = array('Mentorship.created >= '=>$searchStartDate.' 00:00:00'); 
            $condition[] = array('Mentorship.created <= '=>$searchEndDate.' 23:59:59');
            
            $conditionInvoice[] = array('Invoice.modified >= '=>$searchStartDate.' 00:00:00'); 
            $conditionInvoice[] = array('Invoice.modified <= '=>$searchEndDate.' 23:59:59');
            $appSent = $this->Mentorship->find('count',array('conditions'=>array($condition,'Mentorship.parent_id'=>'0')));
            $appAccept = $this->Mentorship->find('count',array('conditions'=>array($condition,'Mentorship.parent_id'=>'0','Mentorship.is_deleted'=>'0','Mentorship.applicationType > '=>'3')));
            $this->Mentorship->bindModel(
                            array(
                            'belongsTo' => array(
                                'Invoice' => array('className' => 'Invoice','foreignKey' => false,'conditions'=>array('Mentorship.id=Invoice.mentorship_id')),
                                )),false);
            $orderConfirm = $this->Invoice->find('count',array('conditions'=>array($conditionInvoice,'Invoice.inv_create_status'=>'1')));
            $intialConultation = $this->Mentorship->find('count',array('conditions'=>array($conditionInvoice,'Invoice.title'=>'Initial consultation','Mentorship.applicationType'=>'5')));
			$session1 = $this->Mentorship->find('count',array('conditions'=>array($conditionInvoice,'Invoice.title'=>'Session 1','Mentorship.applicationType'=>'5')));
            $session2 = $this->Mentorship->find('count',array('conditions'=>array($conditionInvoice,'Invoice.title'=>'Session 2','Mentorship.applicationType'=>'5')));
            $otherSession = $this->Mentorship->find('count',array('conditions'=>array($conditionInvoice,'Mentorship.applicationType'=>'5','Invoice.title NOT'=>array('Initial consultation','Session 1','Session 2'))));
            $disputes = $this->Mentorship->find('count',array('conditions'=>array($condition,'Mentorship.is_deleted'=>'1')));
            $this->set('start',$this->data['Mentor']['start']);
            $this->set('end',$this->data['Mentor']['end']);
        }
        else
        {
	        $this->Mentorship->bindModel(
                            array(
                            'belongsTo' => array(
                                'Invoice' => array('className' => 'Invoice','foreignKey' => false,'conditions'=>array('Mentorship.id=Invoice.mentorship_id')),
                                )),false);
	        $appSent = $this->Mentorship->find('count',array('conditions'=>array($condition,'Mentorship.parent_id'=>'0')));
            $appAccept = $this->Mentorship->find('count',array('conditions'=>array($condition,'Mentorship.parent_id'=>'0','Mentorship.is_deleted'=>'0','Mentorship.applicationType >'=>'3')));
            $orderConfirm = $this->Invoice->find('count',array('conditions'=>array($conditionInvoice,'Invoice.inv_create_status'=>'1')));
            $intialConultation = $this->Mentorship->find('count',array('conditions'=>array($conditionInvoice,'Invoice.title'=>'Initial consultation','Mentorship.applicationType'=>'5')));
			$session1 = $this->Mentorship->find('count',array('conditions'=>array($conditionInvoice,'Invoice.title'=>'Session 1','Mentorship.applicationType'=>'5')));
            $session2 = $this->Mentorship->find('count',array('conditions'=>array($conditionInvoice,'Invoice.title'=>'Session 2','Mentorship.applicationType'=>'5')));
            $otherSession = $this->Mentorship->find('count',array('conditions'=>array($conditionInvoice,'Mentorship.applicationType'=>'5','Invoice.title NOT'=>array('Initial consultation','Session 1','Session 2'))));
            $disputes = $this->Mentorship->find('count',array('conditions'=>array($condition,'Mentorship.is_deleted'=>'1')));
        }
		$this->set('appSent',$appSent);
        $this->set('appAccept',$appAccept);
        $this->set('orderConfirm',$orderConfirm);
        $this->set('intialConultation',$intialConultation);
        $this->set('session1',$session1);
        $this->set('session2',$session2);
        $this->set('otherSession',$otherSession);
        $this->set('disputes',$disputes);
         }else{
        $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
         }
    }

    function admin_invoicelist()
    {
    	if($this->Auth->user('id') == 1){
                   $this->layout = 'admin'; 
        
         $this->Invoice->bindModel(
                        array(
                        'belongsTo' => array(
                            'Mentorship' => array(
                                'className' => 'Mentorship',
                                'foreignKey' => false,
                                'conditions' =>array('Invoice.mentorship_id=Mentorship.id'),
                            ),
                            'Mentee' => array(
                                'className' => 'UserReference',
                                'foreignKey' => false,
                                'conditions' =>array('Mentorship.mentee_id=Mentee.user_id'),
                                'fields' => array('Mentee.first_name','Mentee.last_name'),
                            ),
                            'Mentor' => array(
                                'className' => 'UserReference',
                                'foreignKey' => false,
                                'conditions' =>array('Mentorship.mentor_id=Mentor.user_id'),
                                'fields' => array('Mentor.first_name','Mentor.last_name'),
                            ),
                            'User' => array(
                                'className' => 'User',
                                'foreignKey' => false,
                                'conditions' =>array('Mentorship.mentor_id=User.id'),
                                'fields' => array('User.mentor_type'),
                            ),
                         )),false);
         // $data = $this->Invoice->find('all');
          //$this->set('data',$data);
        
        $fields = array('Invoice.*','Mentorship.*','User.mentor_type','Mentee.first_name','Mentee.last_name','Mentor.first_name','Mentor.last_name');
        $this->Paginator->settings = array('fields' => $fields,'limit'=>20,'recursive'=>'0');
        $data=$this->Paginator->paginate('Invoice');
        $this->set(compact('data'));
         }else{
        $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
         }
    }
    
    /* Message log functions */
    
    function admin_userlog()
        {
    	if($this->Auth->user('id') == 1){
                    $this->layout = 'admin'; 
           $this->Message->bindModel(
                        array(
                        'belongsTo' => array(
                            'Mentor' => array(
                                'className' => 'UserReference',
                                'foreignKey' => false,
                                'conditions' =>array('Message.mentor_id=Mentor.user_id'),
                            ),
                            'Mentee' => array(
                                'className' => 'UserReference',
                                'foreignKey' => false,
                                'conditions' =>array('Message.mentee_id=Mentee.user_id'),
                            ),
                         )));
           $fields = array('Message.*','Mentor.first_name','Mentor.last_name','Mentee.first_name','Mentee.last_name');
           $data = $this->Message->find('all',array('fields'=>$fields,'group'=>'Message.mentee_id,Message.mentor_id'));
           $this->set('data',$data);
         }else{
        $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
         }
        }

        function admin_loghistory($mentorId=null,$menteeId=null)
        {

    	if($this->Auth->user('id') == 1){
                   $this->layout = 'admin'; 
           $this->Message->bindModel(
                        array(
                        'belongsTo' => array(
                            'Mentor' => array(
                                'className' => 'UserReference',
                                'foreignKey' => false,
                                'conditions' =>array('Message.mentor_id=Mentor.user_id'),
                            ),
                            'Mentee' => array(
                                'className' => 'UserReference',
                                'foreignKey' => false,
                                'conditions' =>array('Message.mentee_id=Mentee.user_id'),
                            ),
                         )));
           $fields = array('Message.*','Mentor.first_name','Mentor.last_name','Mentee.first_name','Mentee.last_name');
           $data = $this->Message->find('all',array('fields'=>$fields,'conditions'=>array('Message.mentor_id'=>$mentorId,'Message.mentee_id'=>$menteeId),'order'=>'Message.id desc'));
           $this->set('data',$data);
           $shareLink = $this->MessageLink->find('first',array('conditions'=>array('MessageLink.mentor_id'=>$mentorId,'MessageLink.mentee_id'=>$menteeId)));
            $this->set('shareLink',$shareLink);
         }else{
        $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
         }
        }
       
       function admin_mentorstatics()
        {
    	if($this->Auth->user('id') == 1){
                  $this->layout = 'admin'; 
            $menteeCount = 0; $SessionCount = 0; $weekAmount = 0; $totAmount = 0; $totCommission = 0;
            $start_date = date('m/d/Y',time()+( 1 - date('w'))*24*3600);
            $end_date = date('m/d/Y');
            if(!empty($this->data) || isset($this->params['pass'][0]) || isset($this->params['pass'][1]))
            {
                if(isset($this->data['Mentor']['start']))
                    $start_date = $this->data['Mentor']['start'];
                else
                    $start_date = str_replace("_","/",$this->params['pass'][0]);
                if(isset($this->data['Mentor']['end']))
                    $end_date = $this->data['Mentor']['end'];
                else
                    $end_date = str_replace("_","/",$this->params['pass'][1]);
                
                $st_date = explode("/",$start_date);
                $ed_date = explode("/",$end_date);
                $searchStartDate = $st_date[2].'-'.$st_date[0].'-'.$st_date[1];
                $searchEndDate = $ed_date[2].'-'.$ed_date[0].'-'.$ed_date[1];
                
                $searchCondition[] = array('Mentorship.modified >= '=>$searchStartDate.' 00:00:00'); 
                $searchCondition[] = array('Mentorship.modified <= '=>$searchEndDate.' 23:59:59');
                $fields = array('distinct(Mentorship.mentor_id) as mentor_id');
                $this->Paginator->settings = array('fields' => $fields,'conditions'=>$searchCondition,'limit'=>Configure::read('App.AdminPageLimit'),'recursive'=>'0');
                $data=$this->Paginator->paginate('Mentorship');
                $this->set('firstTime','enter');
            }
            if(empty($this->data) && !isset($this->params['pass'][0]))
            {
                $fields = array('distinct(Mentorship.mentor_id) as mentor_id');
                $this->Paginator->settings = array('fields' => $fields,'limit'=>20,'recursive'=>'0');
                $data=$this->Paginator->paginate('Mentorship');  
            }
            $this->set(compact('data'));
            $this->set('start',$start_date);
            $this->set('end',$end_date);
         }else{
        $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
         }
         }

function admin_search() 
{            
    	if($this->Auth->user('id') == 1){
        $this->layout = 'admin'; 
	$result = $this->Keywords->find('all',array('order'=>'Keywords.id desc'));
	$this->set('data', $result);
         }else{
        $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
         }
}

function admin_referral(){
    	if($this->Auth->user('id') == 1){
      $this->layout = 'admin'; 
	$result = $this->Referrals->find('all',array('order'=>'Referrals.id desc'));
	$this->set('data', $result);
         }else{
        $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
         }
}

function admin_autocomplete(){
    	if($this->Auth->user('id') == 1){
       $this->layout = 'admin'; 
	$result = $this->Topic->find('all',array('order'=>'Topic.id desc'));
	$this->set('data', $result);
         }else{
        $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
         }

}
function admin_editautocompletetext($id = null){
    	if($this->Auth->user('id') == 1){
	      $this->layout = 'admin'; 
	$this->pageTitle = __('Edit Autocomplete Text', true);
	if(!$id && empty($this->data)) {
		$this->Session->setFlash('Invalid Id');
		$this->redirect(array('action' => 'admin_autocomplete'));
	}
	
	$this->set('modelName', 'Topci');
	$this->set('controllerName', 'reports');
	
	$res = $this->Topic->read(null, $id);
	$this->set('res',$res);
	
	if(!empty($this->data))
	{
		$this->Topic->set($this->data);
	
		if ($this->Topic->save(trim($this->data['autocomplete_text']))){
			$this->Session->setFlash('The AutoComplete Text has been saved', 'admin_flash_good');
			$this->redirect(array('action' => 'admin_autocomplete'));
		}else{
			$this->Session->setFlash('Please correct the errors listed below.', 'admin_flash_bad');
		}
	}else{
		$this->data = $this->Topic->read(null, $id);
	}
         }else{
        $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
         }
	
}
function admin_deleteautocompletetext($id = null){
    	if($this->Auth->user('id') == 1){
	      $this->layout = 'admin'; 
	if($this->Topic->delete($id)) {
		$this->Session->setFlash('The AutoComplete Text has been deleted', 'admin_flash_good');
		$this->redirect(array('action' => 'admin_autocomplete'));
	} else {
		$this->Session->setFlash('Autocomplete Text could not be deleted', 'admin_flash_bad');
	}
         }else{
        $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
         }
}
}

?>