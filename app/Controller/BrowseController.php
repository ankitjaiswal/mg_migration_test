<?php
/**
 * Cities Controller
 *
 * PHP version 5
 *
 * @category Controller
 */
 
use Elasticsearch\ClientBuilder;

require 'vendor/autoload.php';
	
App::uses('Sanitize', 'Utility');
//require('../webroot/mandrill/Mandrill.php');
class BrowseController extends AppController{
	/**
     * Controller name
     *
     * @var string
     * @access public
     */
	var $name	=	'Browse';
	/**
     * Models used by the Controller
     *
     * @var array
     * @access public
     */	 
	var $uses =array('User','StaticPage','Mentorship','City','Mentorship','Answer','FeaturedMentor',
			'UserReference','TempMentorship', 'Keyword','Topic','DirectoryUser',
			'Feedback','Roundtable', 'Member','MemberIndustryCategory','IndustryCategory','Synonym');
	var $helpers = array('General','Form','Ajax','Javascript','Html','Image');
	var $components = array("Session", "Email","Auth","RequestHandler","General");
	protected $_searchSession;
   /**
	 * beforeFilter
	 * @return void
	 */	 
	function beforeFilter(){
		parent::beforeFilter();	 
		$this->Auth->allow('search_result','directory','delete_cache','regenerate_search_urls','refresh_cache');
		if (in_array($this->params['action'], array('admin_process'))){
			$this->Security->validatePost = false;						
		}
      $this->searchSession = session_id();
		
	}
	/*
	* Front page 
	*/	
	function refresh_cache(){
		
		/*$unique_urls = $this->Keyword->find('first', array('fields'=>array('id','url'), 'order' => 'modified asc'));
		
		$unique_urls['Keyword']['modified'] = date("Y-m-d H:i:s");
		
		$this->Keyword->save($unique_urls);
		
		set_time_limit(0);
		
	   	if (!empty($unique_urls['Keyword']['url']) && $unique_urls['Keyword']['url'] != '') {

    		$curlOptions = array (
				CURLOPT_URL => $unique_urls['Keyword']['url'].'?isCron=1',
    				CURLOPT_VERBOSE => 1,
    				CURLOPT_SSL_VERIFYPEER => 0,
    				CURLOPT_SSL_VERIFYHOST => 0,
    				CURLOPT_RETURNTRANSFER => 1,
    		);

    		$ch = curl_init();
    		curl_setopt_array($ch,$curlOptions);
    
    		$response = curl_exec($ch);

			curl_close($ch);
			
		}*/
	}

	function regenerate_search_urls(){
		
		/*$unique_urls = $this->Keyword->find('all', array('fields'=>'DISTINCT url'));
		
		set_time_limit(0);
		
		foreach ($unique_urls as $key => $url) {
			
		   	if (!empty($url['Keyword']['url']) && $url['Keyword']['url'] != '') {
    
	    		$curlOptions = array (
	    				CURLOPT_URL => $url['Keyword']['url'].'?isCron=1',
	    				CURLOPT_VERBOSE => 1,
	    				CURLOPT_SSL_VERIFYPEER => 0,
	    				CURLOPT_SSL_VERIFYHOST => 0,
	    				CURLOPT_RETURNTRANSFER => 1,
	    		);
    
	    		$ch = curl_init();
	    		curl_setopt_array($ch,$curlOptions);
	    
	    		$response = curl_exec($ch);
    
    			curl_close($ch);
				
				usleep (15000000);
    		}
    	}*/
	}
	function delete_cache(){
		
		$files = glob('/var/www/html/mentorsguild/app/webroot/cache/*'); // get all file names
		foreach($files as $file){ // iterate files
		  if(is_file($file))
		    unlink($file); // delete file
		}

	}
	
	function search_result($adIndustry = null, $adSubIndustry = null,$adKeyword = null, $adstate = null, $adCity = null){
	
	     if(empty($adCity)){
	     	  $adCity ="new-york";
         }

	       if(false !== stripos($adCity,"'")){
        	$adCity = str_replace("'","",$adCity);
                } 

         if(empty($adstate)){
            $adstate ="Ny";
        }

                if($this->Session->check('mentorPopup')){
                $this->Session->delete('mentorPopup');
                }
                if($this->Session->check('menteePopup')){
                $this->Session->delete('menteePopup');
                }
                if($this->Session->check('projectId')){
                $this->Session->delete('projectId');
                } 
                if($this->Session->check('questionId')){
                $this->Session->delete('questionId');
                } 
                if($this->Session->check('consultantPopup')){
                $this->Session->delete('consultantPopup');
                }
                if($this->Session->check('d_user_id')){
                $this->Session->delete('d_user_id');
                } 
                if($this->Session->check('login_industry')){
                $this->Session->delete('login_industry');
                } 
                if($this->Session->check('mentor')){
                $this->Session->delete('mentor');
                }
                if($this->Session->check('directoryclaim')){
                $this->Session->delete('directoryclaim');
                } 

             $this->set('login_industry',$adIndustry);
	     $this->set('login_subindustry',$adSubIndustry);	 
             $this->set('login_keyword',$adKeyword);
             $this->set('login_city',$adCity);
             $this->set('login_state',$adstate);

             $this->Session->write('login_industry',$adIndustry);
             $this->Session->write('login_subindustry',$adSubIndustry);
             $this->Session->write('login_keyword',$adKeyword);
             $this->Session->write('login_city',$adCity);
             $this->Session->write('login_state',$adstate);

	
                $this->set('cache_industry', $adIndustry);
                $this->set('cache_subindustry', $adSubIndustry);
		$this->set('cache_keyword', $adKeyword);
		$this->set('cache_adCity', $adCity);
		$this->set('cache_adstate', $adstate);

		if(!empty($adCity) && $adCity != '')
		{
			$adCity = str_replace("-"," ",$adCity);
		}
		
		if(!empty($adKeyword) && $adKeyword != '')
		{
			$adKeyword = str_replace("-"," ",$adKeyword);
		}
		if(!empty($adIndustry) && $adIndustry != '')
		{
			$adIndustry = str_replace("-"," ",$adIndustry);
		}

		if(!empty($adSubIndustry) && $adSubIndustry != '')
		{
			$adSubIndustry = str_replace("-"," ",$adSubIndustry);
		}
                
                if(!empty($adIndustry) && $adIndustry == 'non profit' || $adIndustry == 'Non profit')
                {
                   $adIndustry = "Non-profit";
                 }
                if(!empty($adSubIndustry) && $adSubIndustry == 'fund raising' || $adSubIndustry == 'Fund raising')
                {
                   $adSubIndustry = "Fund-Raising";
                 } 
		$this->layout = 'search';
		
	       if(false !== stripos($adKeyword,"_slash"))
        	$adKeyword = str_replace("_slash","/",$adKeyword);
               if(false !== stripos($adKeyword,"_or"))
        	$adKeyword = str_replace("_or","-",$adKeyword);
               if(false !== stripos($adKeyword,"_and"))
         	$adKeyword = str_replace("_and","&",$adKeyword);

                           $keyword = $adKeyword;


                            $topictabledata = $this->Topic->find('first', array('conditions' => array('Topic.autocomplete_text	' => $keyword))); 
                            if($topictabledata['Topic']['isName'] == 1)
                            {
                             $name = "yes";
                            }else{
                             $name = "no";
                            }
                            if($adIndustry == 'all industries' && $adSubIndustry == 'all categories' && $keyword != '' && $keyword == 'lean six sigma' && $name == 'no'){
                            $title = 'Experts in '.ucwords($keyword);
                            }
                            else if($adIndustry == 'all industries' && $adSubIndustry == 'all categories' && $keyword != '' && $keyword != 'browse all' && $name == 'no'){
                            $title = 'Experts in '.ucwords($keyword);
                            }
                            else if($adIndustry == 'all industries' && $adSubIndustry == 'all categories' && $keyword != '' && $keyword != 'browse all' && $name == 'yes'){
                            $title = 'Search results for "'.ucwords($keyword).'"';
                            }
                            else if($adIndustry == 'all industries' && $adSubIndustry == 'all categories' && $keyword == 'browse all'){
                            $title = 'Experts near you';
                            }
                            else if($adIndustry != 'all industries' && $adSubIndustry == 'all categories' && $keyword == 'browse all'){
                            $title =  'Experts in '.ucfirst($adIndustry);
                            }
                            else if($adIndustry != 'all industries' && $adSubIndustry == 'all categories' && $keyword != '' && $keyword != 'browse all' && $name == 'no'){
                            $title =  ucfirst($keyword).' experts in '.ucfirst($adIndustry);
                            }
                            else if($adIndustry != 'all industries' && $adSubIndustry == 'all categories' && $keyword != '' && $keyword != 'browse all' && $name == 'yes'){
                            $title = 'Search results for "'.ucwords($keyword).'"';
                            }
                            else if($adIndustry != 'all industries' && $adSubIndustry != 'all categories' && $keyword == 'browse all'){
                            $title = ucfirst($adSubIndustry).' experts';
                            }
                            else if($adIndustry != 'all industries' && $adSubIndustry != 'all categories' && $keyword != '' && $keyword != 'browse all' && $name == 'no'){
                            $title = ucfirst($keyword).' experts in '.ucfirst($adIndustry);
                            }
                            else if($adIndustry != 'all industries' && $adSubIndustry != 'all categories' && $keyword != '' && $keyword != 'browse all' && $name == 'yes'){
                            $title = 'Search results for "'.ucwords($keyword).'"';
                            }
                            else{
                            $title = 'Experts near you';
                            }


         

				
                $this->set("title_for_layout",$title);
                $mentorsdata = array();

		$location = array('city'=>$adCity,'state'=>$adstate);
		
		$userLocation = $this->_getLatLong($location);
	
		$userLocationString =  $userLocation['City']['latitude'] . ", ". $userLocation['City']['longitude'];
		
		$ind_cat_str = $adIndustry . ": " . $adSubIndustry;
		
		pr("Location : " . $userLocationString);
		pr("Ind-Cat : " . $ind_cat_str);

		$client = ClientBuilder::create()->build();

                $consultant_name = Configure::read('consultant_name');
                $profile_headline = Configure::read('profile_headline');
                $area_of_expertise = Configure::read('area_of_expertise');
                $background_summary = Configure::read('background_summary');
                $topics_weight = Configure::read('topics_weight');
                $topics_weight1 = Configure::read('topics_weight1');
                $location_offset = Configure::read('location_offset');
                $location_scale = Configure::read('location_scale');
                $location_weight = Configure::read('location_weight');
                $quality_weight = Configure::read('quality_weight');
                $quality_weight1 = Configure::read('quality_weight1');
                $quality_weight2 = Configure::read('quality_weight2');
                $quality_weight3 = Configure::read('quality_weight3');
                $industry_weight = Configure::read('industry_weight');
                $industry_weight1 = Configure::read('industry_weight1');
                $category_weight = Configure::read('category_weight');

                $quality1 = "1";
                $quality2 = "2";
                $quality3 = "3";
                 

                $keyword = $adKeyword;
                 
		if( $adIndustry == "all industries" && $adSubIndustry == "all categories" ) {

			if($adKeyword == "browse all"){
				$adKeyword = 'a';
                                $syKeyword = $adKeyword;
			}
                        else{

                            $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$adKeyword )));
                            if($synonym_match['Synonym']['original_word'] != ''){
                            $syKeyword = $synonym_match['Synonym']['synonym_word'];
                            }else{
                            $syKeyword = $adKeyword;
                            }

                          }
			$params = array(
			    'index' => 'guild_members',
			    'type' => 'guildlive_new1',
			    'size' => 7,
			    'body' => array(
			    			'query' => [
								'function_score' => [
									'query' => [
							            'bool' => [
							                'should' => [
							                    [ 'match_phrase' => ['name'=>[ 'query' => $adKeyword , 'boost'=> $consultant_name]]],
							                    [ 'match_phrase' => ['headline'=>[ 'query' => $adKeyword , 'slop'=>1, 'boost'=> $profile_headline]]],
							                    [ 'match_phrase' => ['background_summary'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost'=> $background_summary]]],
							                    [ 'match_phrase' => ['topic1'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['topic2'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['topic3'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['topic4'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['topic5'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['topic_synonyms'=>[ 'query' => $syKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match' => ['quality'=>[ 'query' => $quality1,  'boost' => $quality_weight1]]],
							                    [ 'match' => ['quality'=>[ 'query' => $quality2,  'boost' => $quality_weight2]]],
							                    [ 'match' => ['quality'=>[ 'query' => $quality3,  'boost' => $quality_weight3]]]
							                ]
							               ]
							              ],
									'functions' => [
												[
													'exp' => [
												 		'location' => [
											              'origin' => $userLocationString,
											              'offset' => $location_offset,
											              'scale' =>  $location_scale
											            ]
											          ],
											          'weight' => $location_weight
						        			],

						        		]
						        	]
						        	],
                                                                 'min_score'=> 0.01
			    )
			);
			
		} 
		else if( $adIndustry != "all industries" && $adSubIndustry == "all categories" ) {
			
			if($adKeyword == "browse all"){
				$adKeyword = $adIndustry;
                                $syKeyword = $adKeyword;
			}
                        else{

                            $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$adKeyword )));
                            if($synonym_match['Synonym']['original_word'] != ''){
                            $syKeyword = $synonym_match['Synonym']['synonym_word'];
                            }else{
                            $syKeyword = $adKeyword;
                            }
                          }

			$params = array(
			    'index' => 'guild_members',
			    'type' => 'guildlive_new1',
			    'size' => 7,
			    'body' => array(
			    			'query' => [
								'function_score' => [
									'query' => [
							            'bool' => [
							                'should' => [
							                    [ 'match_phrase' => ['name'=>[ 'query' => $adKeyword , 'slop'=>1, 'boost'=> $consultant_name]]],
							                    [ 'match_phrase' => ['headline'=>[ 'query' => $adKeyword , 'slop'=>1, 'boost'=> $profile_headline]]],
							                    [ 'match_phrase' => ['background_summary'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost'=> $background_summary]]],
							                    [ 'match_phrase' => ['topic1'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['topic2'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['topic3'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['topic4'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['topic5'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['topic_synonyms'=>[ 'query' => $syKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['industry1'=>[ 'query' => $adIndustry, 'slop'=>1, 'boost' => $industry_weight]]],
							                    [ 'match_phrase' => ['industry2'=>[ 'query' => $adIndustry, 'slop'=>1, 'boost' => $industry_weight]]],
							                    [ 'match_phrase' => ['industry3'=>[ 'query' => $adIndustry, 'slop'=>1, 'boost' => $industry_weight]]],
							                    [ 'match' => ['quality'=>[ 'query' => $quality1,  'boost' => $quality_weight1]]],
							                    [ 'match' => ['quality'=>[ 'query' => $quality2,  'boost' => $quality_weight2]]],
							                    [ 'match' => ['quality'=>[ 'query' => $quality3,  'boost' => $quality_weight3]]]
							                ]
							               ]
							              ],
									'functions' => [
												[
													'exp' => [
												 		'location' => [
											              'origin' => $userLocationString,
											              'offset' => $location_offset,
											              'scale' =>  $location_scale
											            ]
											          ],
											          'weight' => $location_weight
						        			],

						        		]
						        	]
						        	]
			    )
			);
			
		} 
		else {
			
				if($adKeyword == "browse all"){
					$adKeyword = $adIndustry;
                                        $syKeyword = $adKeyword;
				}
                              else{

                            $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$adKeyword )));
                            if($synonym_match['Synonym']['original_word'] != ''){
                            $syKeyword = $synonym_match['Synonym']['synonym_word'];
                            }else{
                            $syKeyword = $adKeyword;
                            }
                            }

				$params = array(
			    'index' => 'guild_members',
			    'type' => 'guildlive_new1',
			    'size' => 7,
			    'body' => array(
			    			'query' => [
								'function_score' => [
									'query' => [
							            'bool' => [
							                'should' => [
							                    [ 'match_phrase' => ['name'=>[ 'query' => $adKeyword , 'slop'=>1, 'boost'=> $consultant_name]]],
							                    [ 'match_phrase' => ['headline'=>[ 'query' => $adKeyword , 'slop'=>1, 'boost'=> $profile_headline]]],
							                    [ 'match_phrase' => ['background_summary'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost'=> $background_summary]]],
							                    [ 'match_phrase' => ['topic1'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['topic2'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['topic3'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['topic4'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['topic5'=>[ 'query' => $adKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['topic_synonyms'=>[ 'query' => $syKeyword, 'slop'=>1, 'boost' => $topics_weight1]]],
							                    [ 'match_phrase' => ['industry1'=>[ 'query' => $adIndustry, 'slop'=>1, 'boost' => $industry_weight1]]],
							                    [ 'match_phrase' => ['industry2'=>[ 'query' => $adIndustry, 'slop'=>1, 'boost' => $industry_weight1]]],
							                    [ 'match_phrase' => ['industry3'=>[ 'query' => $adIndustry, 'slop'=>1, 'boost' => $industry_weight1]]],
							                    [ 'match_phrase' => ['category1'=>[ 'query' => $adSubIndustry, 'slop'=>1, 'boost' => $category_weight]]],
							                    [ 'match_phrase' => ['category2'=>[ 'query' => $adSubIndustry, 'slop'=>1, 'boost' => $category_weight]]],
							                    [ 'match_phrase' => ['category3'=>[ 'query' => $adSubIndustry, 'slop'=>1, 'boost' => $category_weight]]],
							                    [ 'match' => ['quality'=>[ 'query' => $quality1,  'boost' => $quality_weight1]]],
							                    [ 'match' => ['quality'=>[ 'query' => $quality2,  'boost' => $quality_weight2]]],
							                    [ 'match' => ['quality'=>[ 'query' => $quality3,  'boost' => $quality_weight3]]]
							                ]
							               ]
							              ],
									'functions' => [
												[
													'exp' => [
												 		'location' => [
											              'origin' => $userLocationString,
											              'offset' => $location_offset,
											              'scale' =>  $location_scale
											            ]
											          ],
											          'weight' => $location_weight
						        			],

						        		]
						        	]
						        	]
			    )
			);
		}


					

		$response = $client->search($params);
                //pr($response);
                //die;

			    $final_mentorsdata = array();
				$inc = 0;
					
				foreach($response['hits']['hits'] as $value)
				{
						$final_mentorsdata[$inc]['User']['id'] = $value['_id'];
						$final_mentorsdata[$inc]['User']['extension'] = $value['_source']['extension'];
						$final_mentorsdata[$inc]['User']['url_key'] = $value['_source']['url_key'];
						$final_mentorsdata[$inc]['UserReference']['first_name'] = $value['_source']['first_name'];
						$final_mentorsdata[$inc]['UserReference']['last_name'] = $value['_source']['last_name'];
						$final_mentorsdata[$inc]['UserReference']['background_summary'] = $value['_source']['background_summary'];
						$final_mentorsdata[$inc]['UserReference']['area_of_expertise'] = $value['_source']['area_of_expertise'];
						$final_mentorsdata[$inc]['UserReference']['City']['city_name'] = $value['_source']['city_name'];
						$final_mentorsdata[$inc]['UserReference']['City']['state'] = $value['_source']['state'];
						$final_mentorsdata[$inc]['UserImage'][0]['image_name']= $value['_source']['user_image'];
                                                $final_mentorsdata[$inc]['UserReference']['linkedin_headline']= $value['_source']['headline'];


                                                if($value['_source']['topic1'] !='' && $value['_source']['topic2'] !='' && $value['_source']['topic3'] !='' && $value['_source']['topic4'] !='' && $value['_source']['topic5'] !=''){
						$final_mentorsdata[$inc]['UserReference']['area_of_expertise'] = $value['_source']['topic1'].' | '.$value['_source']['topic2'].' | '.$value['_source']['topic3'].' | '.$value['_source']['topic4'].' | '.$value['_source']['topic5'];
                                                }
                                                if($value['_source']['topic1'] !='' && $value['_source']['topic2'] !='' && $value['_source']['topic3'] !='' && $value['_source']['topic4'] !='' && $value['_source']['topic5'] ==''){
						$final_mentorsdata[$inc]['UserReference']['area_of_expertise'] = $value['_source']['topic1'].' | '.$value['_source']['topic2'].' | '.$value['_source']['topic3'].' | '.$value['_source']['topic4'];
                                                }
                                                if($value['_source']['topic1'] !='' && $value['_source']['topic2'] !='' && $value['_source']['topic3'] !='' && $value['_source']['topic4'] =='' && $value['_source']['topic5'] ==''){
						$final_mentorsdata[$inc]['UserReference']['area_of_expertise'] = $value['_source']['topic1'].' | '.$value['_source']['topic2'].' | '.$value['_source']['topic3'];
                                                }
                                                if($value['_source']['topic1'] !='' && $value['_source']['topic2'] !='' && $value['_source']['topic3'] =='' && $value['_source']['topic4'] =='' && $value['_source']['topic5'] ==''){
						$final_mentorsdata[$inc]['UserReference']['area_of_expertise'] = $value['_source']['topic1'].' | '.$value['_source']['topic2'];
                                                }
                                                if($value['_source']['topic1'] !='' && $value['_source']['topic2'] =='' && $value['_source']['topic3'] =='' && $value['_source']['topic4'] =='' && $value['_source']['topic5'] ==''){
						$final_mentorsdata[$inc]['UserReference']['area_of_expertise'] = $value['_source']['topic1'];
                                                }

                                                if($value['_source']['industry1'] !='' && $value['_source']['industry2'] !='' && $value['_source']['industry3'] !=''){
                                                $final_mentorsdata[$inc]['UserReference']['ind-cat'] = $value['_source']['industry1'].' - '.$value['_source']['category1'].' | '.$value['_source']['industry2'].' - '.$value['_source']['category2'].' | '.$value['_source']['industry3'].' - '.$value['_source']['category3'];
                                                }
                                                if($value['_source']['industry1'] !='' && $value['_source']['industry2'] !='' && $value['_source']['industry3'] ==''){
                                                $final_mentorsdata[$inc]['UserReference']['ind-cat'] = $value['_source']['industry1'].' - '.$value['_source']['category1'].' | '.$value['_source']['industry2'].' - '.$value['_source']['category2'];
                                                }
                                                if($value['_source']['industry1'] !='' && $value['_source']['industry2'] =='' && $value['_source']['industry3'] ==''){
                                                $final_mentorsdata[$inc]['UserReference']['ind-cat'] = $value['_source']['industry1'].' - '.$value['_source']['category1'];
                                                }

						$inc = $inc + 1;
				 }

				//sorting 
				$data = $final_mentorsdata;
                                
				$this->set('adSubIndustry', $adSubIndustry);
				
				$this->set(compact('data','keyword','adCity','adstate','adIndustry'));
				
				//Finding directory members
				/*$dUsers = $this->DirectoryUser->find('all',array('conditions'=>array('DirectoryUser.hasPublished'=>0), 'order'=>array('DirectoryUser.last_name'=>'asc')));
				$dUsersFinal = array();
				$inc = 0;
				foreach ($dUsers as $key=>$value) 
				{
					$dValue = 0;
					if($keyword != '')
					{
						foreach ($keyArray as $keyw) 
						{
							if (0 == strcasecmp($keyw, $value['DirectoryUser']['first_name']) || 0 == strcasecmp($keyw, $value['DirectoryUser']['first_name'])) 
							{
								$dValue++;
							}
                                                        if (0 == strcasecmp($keyw, $value['DirectoryUser']['last_name']) || 0 == strcasecmp($keyw, $value['DirectoryUser']['last_name'])) 
						        {
								$dValue++;
							}
						 }
	
						 if (false !== stripos($value['DirectoryUser']['area_of_expertise'], $keyword)) 
						 {
							$dValue++;
						 }
						       
						 if($dValue > 0) 
						 {
							$dUsersFinal[$inc++] = $value;
						 }
                         } else 
                        {
							$dUsersFinal[$inc++] = $value;
				        }
			       }*/
				$dUsersFinalSorted = Set::sort($dUsersFinal, '{n}.DirectoryUser.quality', 'asc');
				$this->set('dUsersFinal', $dUsersFinalSorted);
}




         	function directory(){
		
		//Finding directory members
		$dUsers = $this->DirectoryUser->find('all',array('conditions'=>array('DirectoryUser.hasPublished'=>0), 'order'=>array('DirectoryUser.last_name'=>'asc')));
		$dUsersFinal = array();
		$inc = 0;
		foreach ($dUsers as $key=>$value) {
			$dUsersFinal[$inc++] = $value;
		}
			
		$dUsersFinalSorted = Set::sort($dUsersFinal, '{n}.DirectoryUser.quality', 'asc');
		$this->set('dUsersFinal', $dUsersFinalSorted);
		
	}
          
     
	
	
    function _getCityData($zipcode){
		$this->loadModel('City');
		$mentorCityArr = $this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($zipcode))));
		return $mentorCityArr['City'];
	}	
	
	function _getLatLong($cityStateArr){
		//prd($cityStateArr);
		$this->loadModel('City');
		if(isset($cityStateArr['city']) && isset($cityStateArr['state'])){
			$cityArr = $this->City->find('first',array('conditions'=>array('City.city_name'=>trim($cityStateArr['city']),'City.state'=>trim($cityStateArr['state']))));
		}elseif(isset($cityStateArr['city'])){
			$cityArr = $this->City->find('first',array('conditions'=>array('City.city_name'=>trim($cityStateArr['city']))));
		}elseif(isset($cityStateArr['state'])){
			$cityArr = $this->City->find('first',array('conditions'=>array('City.city_name'=>trim($cityStateArr['state']))));
		}else{
			$cityArr = array();
		}
		return $cityArr;
	}
	
	function _calc_distance($coodArr){
		$distance = (3958 * 3.1415926 * sqrt(
				($coodArr['point1']['lat'] - $coodArr['point2']['lat'])
				* ($coodArr['point1']['lat'] - $coodArr['point2']['lat'])
				+ cos($coodArr['point1']['lat'] / 57.29578)
				* cos($coodArr['point2']['lat'] / 57.29578)
				* ($coodArr['point1']['long'] - $coodArr['point2']['long'])
				* ($coodArr['point1']['long'] - $coodArr['point2']['long'])
		) / 180);
	
		return $distance;
	}
	
}

?>