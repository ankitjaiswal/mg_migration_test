<?php

/**
 * Users Controller
 *
 * PHP version 5
 * Project start date 29 Mar 2012
 * Developed by :Sandeep singh
 * Company info:Octalsolution
 * @Users Controller
 */

App::uses('Sanitize', 'Utility');


use Elasticsearch\ClientBuilder;
require 'vendor/autoload.php';
require('../webroot/stripe-php-1.17.2/lib/Stripe.php');
class UsersController extends AppController {

	/**
	 * Controller name
	 *
	 * @var string
	 * @access public
	 */
	var $name = 'Users';
	/**
	 * Models used by the Controller
	 *
	 * @var array
	 * @access public
	 */
	var $uses = array('User','UserImage', 'UserReference','Availablity',
			'Communication','Social','TempRequest','Feedback',
			'City','NewsLetter','Referral','CaseStudy',
			'QnaQuestion', 'Insight','InsightCategory','QnaAnswer','QnaQuestionCategory',
			'QnaIgnoredQuestion', 'Project','DirectoryConsultation',
			'DirectoryUser','Mentorship','Feed','Testimonial','ShowcaseClient','Topic','IndustryCategory',
			'Topic', 'MemberIndustryCategory','Prospect','Proposal', 'City','ProposalTopic','CasestudyTopic','Synonym','ConsultationRequest','MentorApply');
	var $helpers = array('General', 'Form', 'Ajax', 'javascript', 'Paginator');
	var $components = array("Session", "Email", "Auth", "RequestHandler" ,"Upload","General",'Paginator','Security');

	function beforeFilter() {
		//echo Security::hash('admin1467', null, true);die;
		parent::beforeFilter();
              //$this->Auth->authorize = false;
		$this->Auth->allow('register','login','edit_account','registration_step2','logout',
				'checkEmail','checkEmailForgot','thanks','checkEmailPassword','my_account',
				'forgot','forgot_thanks','reset_password','feedback','feedbackdetail',
				'signuplinkedin','checkUser','signuplinkedinMentor',
				'signuplinkedinMentorPopup','signuplinkedinMenteePopup','cancellinkedin',
				'thanksuser','other_testimonials','loginfromregister','users_listasdfrgt12341',
				'linkedinQnAAuth', 'QFromMailAuth','projectAuth','directoryConsultAuth','feedbackAuth',
				'directoryClaimAuth', 'isOpenPopUp','MentorshipRequestAuth','linkedinSearchAuth', 'addnew', 'admin_login', 'admin_dashboard','client_invitation','refer_member','invitation','sessionlocation','profile_edit','update_profile','save_profileinfo','save_industrycategory','save_professionalsummary','save_engagementovierview','save_pastclients','save_availability','save_engfee','save_topics','save_sociallinks','stripecheckout','client_auth_popup');

	}

	function logout_message(){
		$this->layout = 'ajax';
	}
	function admin_login() {

		$this->set('title_for_layout', 'Admin User Login');
		$this->layout = 'admin_login';

		if ($this->Auth->login()) {
			if ($this->Auth->user('id') == '1') {

				$this->Session->write('Admin.email', $this->data['User']['email']);
				$this->Session->write('Admin.password', $_POST['data']['User']['password']);
				$this->layout = 'admin';   

				$this->redirect(array('controller' => 'users', 'action' => 'admin_dashboard'));
			}
		} 
	}

	function admin_dashboard() {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		$this->set('title_for_layout', 'Dashboard');
		$this->redirect( array('controller' => 'members','action' => 'admin_search'));
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	
           }

	function admin_logout() {
		$this->layout = 'admin';  
		$this->Session->setFlash(__('Log out successful.', true));
		$this->redirect($this->Auth->logout());
              }

	function admin_index() {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		if (!isset($this->request['named']['page'])) {
			$this->Session->delete('UserSearch');
		}
		$this->User->bindModel(array('hasOne' => array('UserReference')), false);
		$filters = array('User.role_id=1');
		if (!empty($this->data)) {
			$this->Session->delete('UserSearch');
			if (!empty($this->data['UserReference']['first_name'])) {
				$keyword = Sanitize::escape($this->data['UserReference']['first_name']);
				$this->Session->write('UserSearch', $keyword);
			}
		}

		if ($this->Session->check('UserSearch')) {
			$filters[] = array('UserReference.first_name LIKE' => "%" . $this->Session->read('UserSearch') . "%");
		}
		/* Paginate method is used to find all listing of countries */
		$this->paginate['User'] = array(
				'limit' => Configure::read('App.AdminPageLimit'),
				'order' => array('User.username' => 'ASC'),
				'conditions' => $filters
		);

		$data = $this->paginate('User');
		//prd($data);
		$this->set('data', $data);
		$this->set('title_for_layout', __('User', true));
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}

	function admin_add() {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		$this->set('title_for_layout', 'Add Admin Users');
		if (!empty($this->data)) {
			$this->User->set($this->data);
			$this->User->setValidation('admin');
			$this->UserReference->set($this->data);
			$this->UserReference->setValidation('admin');
			$this->data['User']['password'] = $this->data['User']['password2'];//Security::hash($this->data['User']['password2'], null, true);
			$this->data['User']['password2'] = Security::hash($this->data['User']['password2'], null, true);
			
			if ($this->User->saveAll($this->data, array('validate' => 'first'))) {
				$this->Session->setFlash('The Admin has been saved', 'admin_flash_good');
				$this->redirect(array('admin' => true,'action' => 'index'));
			} else {
				$this->Session->setFlash('Please correct the errors listed below.', 'admin_flash_bad');
			}
		} else {
			unset($this->data['User']['username']);
		}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}

	function admin_edit($id = null) {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		$this->set('title_for_layout', "Edit Admin Users");
		if (!$id && empty($this->data)) {
			$this->Session->setFlash('Invalid user');
			$this->redirect(array('admin' => true,'action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->User->data['User']['role_id'] = 1;
			$this->User->set($this->data);
			$this->User->setValidation('admin');

			$this->UserReference->set($this->data);
			$this->UserReference->setValidation('admin');

			if ($this->User->validates() && $this->UserReference->validates()) {
				if ($this->User->saveAll($this->data)) {

					$this->Session->setFlash('Updated successfully', 'admin_flash_good');
					$this->redirect(array('admin' => true,'action' => 'index'));
				} else {
					$this->Session->setFlash('Please correct the errors listed below.', 'admin_flash_bad');
				}
			} else {
				$this->Session->setFlash('The Admin could not be updated. Please, try again.', 'admin_flash_bad');
			}
		} else {
			$this->data = $this->User->read(null, $id);
		}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}

	function admin_delete($id = null) {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		if (!$id) {
			$this->Session->setFlash('Invalid id for Admin');
			$this->redirect(array('admin' => true,'action' => 'index'));
		}
		$admin = $this->User->read(null, $id);
		if (empty($admin)) {
			$this->Session->setFlash('Invalid User', 'admin_flash_bad');
			$this->redirect(array('action' => 'index'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash('User has been deleted successfully', 'admin_flash_good');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash('User has not deleted', 'admin_flash_bad');
		$this->redirect(array('action' => 'index'));
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}

	function message_delete($flag = null, $id=null) {
		$this->loadModel('MessageCenter');
		if (!$id) {
			$this->Session->setFlash('Invalid id for users');
			$this->redirect(array('action' => 'message_center'));
		}
		$front = $this->MessageCenter->read(null, $id);
		if (empty($front)) {
			$this->Session->setFlash('Invalid Message', 'flash_bad');
			$this->redirect(array('action' => 'message_center'));
		}
		if ($flag == "inbox") {
			$data = array(
					'MessageCenter' => array(
							'id' => $id,
							'receiver_delete' => 1
					)
			);
			$this->MessageCenter->save($data, false, array('receiver_delete'));
		}
		if ($flag == "outbox") {
			$data = array(
					'MessageCenter' => array(
							'id' => $id,
							'sender_delete' => 1
					)
			);
			$this->MessageCenter->save($data, false, array('sender_delete'));
		}
		$this->Session->setFlash('User has been deleted successfully', 'flash_bad');
		$this->redirect(array('action' => 'message_center'));
	}

	function admin_process() {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		if (!empty($this->data)) {
			App::import('Sanitize');
			$action = Sanitize::escape($this->data['User']['pageAction']);
			foreach ($this->data['User'] AS $value) {
				if ($value != 0) {
					$ids[] = $value;
				}
			}

			if (count($this->data) == 0 || $this->data['User'] == null) {
				$this->Session->setFlash('No items selected.', 'admin_flash_bad');
				$this->redirect(array('controller' => 'Users', 'action' => 'index'));
			}
			if ($action == "delete") {
				$this->User->deleteAll(array('User.id' => $ids));
				$this->Session->setFlash('Users have been deleted successfully', 'admin_flash_good');
				$this->redirect(array('controller' => 'Users', 'action' => 'index'));
			}
			if ($action == "activate") {
				$this->User->updateAll(array('User.status' => 1), array('User.id' => $ids));
				$this->Session->setFlash('Users have been activated successfully', 'admin_flash_good');
				$this->redirect(array('controller' => 'Users', 'action' => 'index'));
			}
			if ($action == "deactivate") {
				$this->User->updateAll(array('User.status' => Configure::read('App.Status.inactive')), array('User.id' => $ids));
				$this->Session->setFlash('Users have been deactivated successfully', 'admin_flash_good');
				$this->redirect(array('controller' => 'Users', 'action' => 'index'));
			}
		} else {
			$this->redirect(array('controller' => 'Users', 'action' => 'index'));
		}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}

	function admin_changepassword($id = null) {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		$this->pageTitle = __('Change Password', true);
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid User', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			$this->User->setValidation('change_password');
			$this->User->set($this->data);
			if ($this->User->validates()) {
				$this->data['User']['password'] = $this->data['User']['password2'];//Security::hash($this->data['User']['password2'], null, true);
				$this->User->updateAll(array('password' => "'" . $this->data['User']['password'] . "'"), array('User.id' => $id));
 
                                $password2 = Security::hash($this->data['User']['password2'], null, true);

				$this->User->updateAll(array('User.password2' => "'" . $password2 . "'"), array('User.id' => $id));

				$this->Session->setFlash('Password has been changed successfully', 'admin_flash_good');
				$this->redirect(array('action' => 'index'));
			}
		} else {
			$this->data = $this->User->read(null, $id);
		}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}

	function admin_viewuser($id=null) {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		$this->set('title_for_layout', 'Admin Personal Information');
		if ($id) {
			$user_information = $this->User->find('all', array('conditions' => array('User.id' => $this->Auth->user('id'))));
			$this->set('data', $user_information);
		}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}
	function register(){
		//step1
		$this->layout = 'ajax';
		$this->Session->write('popup', 1);
	}

	function isOpenPopUp(){
		 
		if ($this->RequestHandler->isAjax()) {
			$pop = $this->Session->read('popup');
			$this->layout = '';
			$this->render(false);
			if($this->Session->read('Auth.User.id') == '' && isset($pop) == false){
				echo json_encode(array('value' => '1'));
				$this->Session->write('popup', 1);
			} else {
				echo json_encode(array('value' => '0'));
			}
			exit();
		}
	}

	function addnew(){
		
		if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                return $this->redirect(array('action' => 'index'));
            }
        }
	}
	//step2
	function registration_step2(){

              $this->layout = 'defaultnew';
        	
              $this->Session->write('popup', 1);
		//if(isset($this->data) && count($this->data)>0){
			if(!isset($this->data['UserReference']['country']))
			{
				$this->__subscribenewsletter($this->data['User']['username']);
				$this->request->data['User'] = $this->General->myclean($this->data['User']);
				$this->request->data['UserReference'] = $this->General->myclean($this->data['UserReference']);

				$this->User->create();
				$this->User->set($this->request->data);
				$this->User->setValidation('front');

				$activationKey = substr(md5(uniqid()), 0, 20);
				$this->request->data['User']['password'] = $this->data['User']['password2'];//Security::hash($this->data['User']['password2'], null, true);
				$this->request->data['User']['password2'] = Security::hash($this->data['User']['password2'], null, true);
				$this->request->data['User']['role_id'] = 3;
				$this->request->data['User']['status'] = 1;
				$this->request->data['User']['access_specifier'] = 'draft';
				$this->request->data['User']['activation_key'] = $activationKey;

				if ($this->User->saveAll($this->request->data, array('validate' => 'first'))) {
					$menteeArray['password'] = $this->data['User']['password2'];
					$menteeArray['username'] = $this->data['User']['username'];
					$this->Session->write('menteeTempData',$menteeArray);
					$id = $this->User->getInsertID();
					$user = $this->User->findById($id);
					$this->Session->write('Auth',$user);
				}
			}
			if(isset($this->data['UserReference']['country'])){

				$userId = $this->Session->read('Auth.User.id');
				$refId = $this->Session->read('Auth.UserReference.id');
				$this->request->data['User']['id'] = $userId;
				$this->request->data['User']['email_send'] = '1';
				if($this->User->save($this->request->data)){

					$this->request->data['UserReference']['id'] = $refId ;
					$this->UserReference->save($this->request->data['UserReference']);
					$this->Social->deleteAll(array('Social.user_id'=>$userId));
					if(!empty($this->data['Social']))
					{
						foreach($this->data['Social'] as $social)
						{
							$socialArray = array();
							if(trim($social['social_name'])!='')
							{
								$this->Social->create();
								$socialArray['Social']['social_name'] = $social['social_name'];
								$socialArray['Social']['user_id'] = $userId;
								$this->Social->save($socialArray);
							}
						}
					}
					$this->User->createUrlKey($userId);
					 
					if($this->Session->read('Auth.User.socialId')!='')
					{
						$this->request->data['User']['username'] = $this->Session->read('Auth.User.username');
						$linkPassword = $this->General->randamPassword();
						$this->request->data['User']['password2'] = $linkPassword;
						$savePass = $linkPassword;//Security::hash($linkPassword, null, true);
						$this->User->id = $userId;
						$this->User->saveField('password', $savePass);
						$this->User->saveField('access_specifier', 'publish');
						 


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $this->data['User']['username'];
                           $subject = "Welcome to GUILD";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_register_linkedin");
                           $data .= "&merge_userfname=".urlencode($this->data['UserReference']['first_name']);
                           $data .= "&merge_useremail=".urlencode($this->data['User']['username']);
                           $data .= "&merge_userpassword=".urlencode($this->data['User']['password2']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


					$user = $this->User->findById($userId);
					$this->Session->write('Auth',$user);

					$this->redirect(array('action'=>'thanksuser'));						 
						 
					}
					else
					{
					$user = $this->User->findById($userId);
					$this->Session->write('Auth',$user);
			              $this->redirect(array('controller'=>'clients','action'=>'my_account'));
					}
					$this->request->data['User']['activation_key'] = $this->Session->read('Auth.User.activation_key');



				}else{
					$this->Session->setFlash("Please remove below errors");
				}
			}

		//}else{
		//	//$this->redirect(array('controller'=>'fronts','action'=>'index'));
		//	$this->set('popup','open');
		//}
	}

	function thanksuser($id)
	{
		$userId = $id;
		$MenteeData = $this->User->findById($userId);
   
              $id = $this->User->findById($userId);
		if(count($MenteeData)>0 || $MenteeData['User']['email_send']=='1'){
			$MenteeData['isLinkedin'] = 0;
			if($this->Session->read('Auth.User.socialId')!='') //Linkeding user
				$MenteeData['isLinkedin'] = 1;
			    $this->set('MenteeData',$MenteeData);
                     
                  
		}else{
			$this->redirect(array('controller'=>'fronts','action'=>'index'));
		}
	}



	function thanks(){
		$this->redirect(array('controller'=>'fronts','action'=>'index'));
	}
	function checkEmail(){
		if ($this->RequestHandler->isAjax()) {
			Configure::write('debug', 0);
			$email = $_REQUEST['email'];
			 
			$count = $this->User->find('count', array('conditions' => array('User.status' =>array(0,1),'User.username' =>trim($email))));
			$uData = $this->MentorApply->find('first', array('conditions' => array('MentorApply.email' =>$email)));
			$count1 = $this->MentorApply->find('count', array('conditions' => array('MentorApply.email' =>trim($email))));

			$this->layout = '';
			$this->render(false);
			echo json_encode(array('value' => $count, 'value1'=>$count1, 'status'=>$uData['MentorApply']['isApproved']));
			exit();
		}

	}
	function checkEmailForgot()
	{
		if ($this->RequestHandler->isAjax())
		{
			Configure::write('debug', 0);
			$email = $_REQUEST['email'];
			 
			$count = $this->User->find('count', array('conditions' => array('User.status' =>array(0,1),'User.username' =>trim($email))));
			$data = $this->User->find('first', array('conditions' => array('User.username' =>trim($email))));
			$this->layout = '';
			$this->render(false);
			if($count!=0)
			{
				if($data['User']['status']=='0')
				{
					echo json_encode(array('value' => $count,'status'=>'inactive'));
				}
				if($data['User']['status']=='1')
				{
					echo json_encode(array('value' => $count,'status'=>'active'));
				}
				exit;
			}
			echo json_encode(array('value' => $count));
			exit();
		}
	}
	function checkEmailPassword(){

		if ($this->RequestHandler->isAjax()) {

			Configure::write('debug',0);
			$email = $this->request->data['email'];
			$pass = Security::hash($this->request->data['pass'], null, true);
			$countEmail = $this->User->find('count', array('conditions' => array('User.status' =>1,'User.username' =>trim($email))));
			if($countEmail!=0)
			{
				$countPass = $this->User->find('count', array('conditions' => array('User.status' =>1,'User.username' =>trim($email),'User.password2' =>trim($pass))));
                //$countPass = $this->User->find('count', array('conditions' => array('User.status' =>1,'User.username' =>trim($email))));
			}
			$this->layout = '';
			$this->render(false);

			if(isset($countPass) && $countPass==0)
				echo json_encode(array('value' => $countPass,'value1' => $pass,'value2' => $email,'pass'=>'wrong'));
			else
				echo json_encode(array('value' => $countEmail,'pass'=>'email'));
			exit();
		}
	}

	function loginfromregister() {
		 
		$this->redirect(array('controller'=>'fronts','action'=>'index','openloginpopup:'.true));
	}

	function invitation() {

		if(!empty($this->data)) {

			$this->User->Behaviors->attach('Containable');
			$user = $this->User->findById($this->Auth->user('id'));

			$emailIds = explode(',',$this->data['EmailId']);

			for($i=0;$i<count($emailIds);$i++)
			{

				$details = array();
				$details = array('mentorEmailId'=>$user['User']['username'],'ReferalEmailId'=>$emailIds[$i], 'role'=>'Member');
				$this->Referral->save($details);
				$this->Referral->id = '';
				 


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = $user['User']['username'];
                           $fromName = $user['UserReference']['first_name']." ".$user['UserReference']['last_name'];
                           $websiteurl = SITE_URL;
                           $to = $emailIds[$i];
                           $link = SITE_URL."members/56KOv57lNjSR9yI6q5Ps";
                           $subject = $this->data['EmailSubject'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("send_invitation");
                           $data .= "&merge_emailbody=".urlencode(nl2br($this->data['EmailBody']));
                           $data .= "&merge_link=".urlencode($link);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

			}

			$this->redirect(array('controller'=>'users','action'=>'my_account'));

		} else {

			$this->User->Behaviors->attach('Containable');
			$user = $this->User->findById($this->Auth->user('id'));
			$this->layout = 'ajax';
			$this->set("fname", $user['UserReference']['first_name']);
		}
	}

	function client_invitation() {

		if(!empty($this->data)) {

			$this->User->Behaviors->attach('Containable');
			$user = $this->User->findById($this->Auth->user('id'));

			$emailIds = explode(',',$this->data['EmailId']);

			for($i=0;$i<count($emailIds);$i++)
			{

				$details = array();
				$details = array('mentorEmailId'=>$user['User']['username'],'ReferalEmailId'=>$emailIds[$i], 'role'=>'Client');
				$this->Referral->save($details);
				$this->Referral->id = '';
					



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = $user['User']['username'];
                           $fromName = $user['UserReference']['first_name']." ".$user['UserReference']['last_name'];
                           $websiteurl = SITE_URL;
                           $to = $emailIds[$i];
                           $link = SITE_URL;
                           $subject = $this->data['EmailSubject'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("send_invitation");
                           $data .= "&merge_emailbody=".urlencode(nl2br($this->data['EmailBody']));
                           $data .= "&merge_link=".urlencode($link);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

			}

			$this->redirect(array('controller'=>'clients','action'=>'my_account'));

		} else {

			$this->User->Behaviors->attach('Containable');
			$user = $this->User->findById($this->Auth->user('id'));
			$this->layout = 'ajax';
			$this->set("fname", $user['UserReference']['first_name']);
		}
	}

     
        function refer_member() {

		
		if(!empty($this->data)) {

			$this->User->Behaviors->attach('Containable');
			$user = $this->User->findById($this->Auth->user('id'));
                   
			
                     $refer = $this->Session->read('refers');
                     $emailIds = explode(',',$this->data['EmailId']);
                     $fname =  $this->Session->read('fname');
                     $emails =  $this->Session->read('email');
			for($i=0;$i<count($emailIds);$i++)
			{

				$details = array();
				$details = array('mentorEmailId'=>$user['User']['username'],'ReferalEmailId'=>$emailIds[$i], 'role'=>'Member');
				$this->Referral->save($details);
				$this->Referral->id = '';
					


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = $user['User']['username'];
                           $fromName = $user['UserReference']['first_name']." ".$user['UserReference']['last_name'];
                           $websiteurl = SITE_URL;
                           $to = $emailIds[$i];
                           $link = SITE_URL.$refer;
                           $subject = $this->data['EmailSubject'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("refer_member");
                           $data .= "&merge_emailbody=".urlencode(nl2br($this->data['EmailBody']));
                           $data .= "&merge_link=".urlencode($link);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


			}



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $emails;
                           $link = SITE_URL;
                           $subject = "You have a referral";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("referred_by");
                           $data .= "&merge_referfname=".urlencode($fname); 
                           $data .= "&merge_referralfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_referrallname=".urlencode($user['UserReference']['last_name']);
                           $data .= "&merge_referralemail=".urlencode($this->Auth->user('username'));                                                                                                                           
                           $data .= "&merge_referralurl=".urlencode($this->Auth->user('url_key'));
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


			$this->redirect(SITE_URL."$refer");

		} else {

			$this->User->Behaviors->attach('Containable');
			$user = $this->User->findById($this->Auth->user('id'));
			$this->layout = 'ajax';
			$this->set("fname", $user['UserReference']['first_name']);
		}
	}


	function users_listasdfrgt12341(){
		 
		if($this->Auth->user('role_id') != Configure::read('App.Role.Visitor')) {
			$this->redirect(array('controller'=>'fronts','action'=>'index'));
		} else {
			$this->User->Behaviors->attach('Containable');
			$this->User->unbindModel(array('hasOne'=>array('Availablity'),'hasMany'=>array('UserImage','Social','Communication')),false);
			$userslist = $this->User->find('all');
			$this->set('userslist', $userslist);
		}
	}

	/**function login(){


		$this->layout = 'ajax';
		$this->set('title_for_layout', 'User Login');


		if ($this->Auth->login()) {

                // if($this->Auth->login($this->request->data)){
			$UserData = $this->User->read('password',$this->Auth->user('id'));
			//set image logined user
			if(isset($UserData['UserImage'][0]['image_name']) &&  count($UserData['UserImage'])>0){
				$this->Session->write('User.image',$UserData['UserImage'][0]['image_name']);
			}
			else
			{
				$this->Session->write('User.image','');
			}

			if($this->Auth->user('id') ==1){
				
                         $this->redirect(array('controller'=>'fronts','action'=>'index'));
			}

			if($this->Auth->user('access_specifier') =='draft' && $this->Auth->user('role_id') == 2){
				$this->redirect(array('controller'=>'members','action'=>'member_full_profile'));

			}
                       if($this->Auth->user('role_id') != ''  && $this->Session->check('project_login') && $this->Session->read('project_login') != -1)
                                       {
                          	           $project = $this->Project->find('first',array('conditions'=>array('Project.activation_key'=>$this->Session->read('project_login'))));
                                       $project['Project']['user_id'] = $this->Auth->user('id');
					    $project['Project']['activation_key'] = '-1';
                                       $this->Project->save($project);

                                       $this->Session->delete('project_login');
                                       $this->redirect(array('controller'=>'project','action'=>'index',$project['Project']['project_url']));
                      }
                     if($this->Session->check('project_url') && $this->Session->read('project_url') != -1)
                                       {
                                        $project_url = $this->Session->read('project_url');
                                        $this->Session->delete('project_url'); 
                                        $this->redirect(array('controller'=>'project','action'=>'index',$project_url));
                      }

                      if($this->Session->check('qna_question') && $this->Session->read('qna_question') != -1)
                                       {
                                        $qna_question = $this->Session->read('qna_question');
                                        $this->Session->delete('qna_question'); 
                                        $this->redirect(array('controller'=>'qna','action'=>'question',$qna_question));
                      }  
                     
                      if($this->Session->check('qna_category') && $this->Session->read('qna_category') != -1)
                                       {
                                        $qna_category = $this->Session->read('qna_category');
                                        $this->Session->delete('qna_category'); 
                                        $this->redirect(array('controller'=>'qna','action'=>'category',$qna_category));
                      }                    
                      
                     if($this->Auth->user('role_id') ==2 && $this->Session->check('mediaUrl') && $this->Session->read('mediaUrl') != '')
                                       {
                                        $this->Session->delete('mediaUrl');
                                        $this->redirect(array('controller'=>'members','action'=>'pr'));
                      }
                     if($this->Session->check('member_serviceUrl') && $this->Auth->user('role_id') ==2)
                                       {
                                        $this->Session->delete('member_serviceUrl');
                                        $this->redirect(array('controller'=>'members','action'=>'advantage'));
                      }
                     if($this->Session->check('flevyUrl') && $this->Session->read('flevyUrl') != '')
                                       {
                                        $this->Session->delete('flevyUrl');
                                        $this->redirect(array('controller'=>'members','action'=>'flevy'));
                      }
                     if($this->Session->check('nlensesUrl') && $this->Session->read('nlensesUrl') != '')
                                       {
                                        $this->Session->delete('nlensesUrl');
                                        $this->redirect(array('controller'=>'members','action'=>'ninelenses'));
                      }
                     if($this->Session->check('sservicesUrl') && $this->Session->read('sservicesUrl') != '')
                                       {
                                        $this->Session->delete('sservicesUrl');
                                        $this->redirect(array('controller'=>'members','action'=>'resources'));
                      }
                if($this->Session->check('projectlink') && $this->Session->read('projectlink') != ''){
                                     $projectlink = $this->Session->read('projectlink');
                                     $this->Session->delete('projectlink');
			
			$this->redirect($projectlink);
		}
                if($this->Session->check('invoicepaypage') && $this->Session->read('invoicepaypage') != ''){
                                     $invoicepaypage = $this->Session->read('invoicepaypage');
                                     $this->Session->delete('invoicepaypage');
			
			$this->redirect($invoicepaypage);
		}
                if($this->Session->check('projectId') && $this->Session->read('projectId') != ''){
                                     $projectid = $this->Session->read('projectId');
                                     $this->Session->delete('projectId');
			
			$this->redirect(array('controller'=>'project', 'action' => 'view', $projectid));
		}
                     if($this->Auth->user('role_id') ==3 && $this->Session->check('menteeRegister'))
                                       {
                                        $this->Session->delete('menteeRegister');
                                        $user = $this->User->findById($this->Auth->user('id'));
				            $this->Session->write('Auth',$user);
                                        $this->redirect(array('action'=>'registration_step2'));
                      }

                     if($this->Session->check('login_keyword') && $this->Session->check('login_city') && $this->Session->check('login_state')) {
                                    
                                       $loginIndustry = $this->Session->read('login_industry');
                                       $loginSubindustry = $this->Session->read('login_subindustry');
                                       $loginKeyword = $this->Session->read('login_keyword');
                                       $loginCity = $this->Session->read('login_city');
                                       $loginState = $this->Session->read('login_state');
                                      
                                       $this->Session->delete('login_industry');
                                       $this->Session->delete('login_subindustry');
                                       $this->Session->delete('login_keyword');
                                       $this->Session->delete('login_city');
                                       $this->Session->delete('login_state');
                                       $this->redirect(SITE_URL."browse/search_result/".$loginIndustry.'/'.$loginSubindustry.'/'.$loginKeyword.'/'.$loginState.'/'.$loginCity);
                                      
                        }
			////////////Apply for mentorship section for login controller/////////////////////
			if(isset($this->data['User']['mentor_id']) && $this->data['User']['mentor_id'] !=''){
				if($this->Auth->user('role_id') == 2){
					$this->Session->setFlash('Sorry! You have logined as mentor.');
					$this->redirect(array('controller'=>'static_pages','action'=>'thanks'));
				}else{
					$this->redirect(array('controller'=>'fronts','action'=>'consultation_request',$this->data['User']['mentor_id']));
				}
			}
			/////////////////////////////////
                      /////// logged out consultant check his asked question & consultation request mail view it here link & Initial Consultation request link/////////////       
                      
                      if($this->Auth->user('role_id') != '' && $this->Auth->user('role_id') == 2 && $this->Session->check('myaccount') !=''){
			             //$qnaconsults = $this->Session->read('qnaconsult');
                                  $this->Session->delete('myaccount');
                                  $this->redirect(SITE_URL."users/my_account");
                                   }
                           
                      else if($this->Auth->user('role_id') != '' && $this->Auth->user('role_id') == 2 && $this->Session->check('qnaconsult') !=''){
			             $qnaconsults = $this->Session->read('qnaconsult');
                                  //$this->Session->delete('qnaconsult');
                                  $this->redirect(SITE_URL."qna/question/".$qnaconsults);
                                   }
                      else if($this->Auth->user('role_id') != '' && $this->Auth->user('role_id') == 2){
                                   $this->redirect(array('controller'=>'users','action'=>'my_account'));
                                   }                              
                      else{
                                   $this->redirect(array('controller'=>'fronts','action'=>'index'));
                                   } 
                      
                      ////////////Question URL/////////////////////
			if(isset($this->data['User']['question_url']) && $this->data['User']['question_url'] !=''){
				if($this->Auth->user('role_id') == 2){
					$this->redirect(array('controller'=>'qna','action'=>'question', $this->data['User']['question_url']));
					}
			}
			/////////////////////////////////


			if($this->Auth->user('access_specifier') =='draft' && $this->Auth->user('role_id') == 2){
				$this->redirect(array('controller'=>'members','action'=>'member_full_profile'));

			}else if($this->Auth->user('access_specifier') =='publish' && $this->Auth->user('role_id') == 2 && $UserData['User']['password'] ==''){
				$this->redirect(array('controller'=>'members','action'=>'account_setting'));
			}else if($this->Auth->user('access_specifier') =='draft' && $this->Auth->user('role_id') == 3)
			{
				if($this->Auth->user('email_send') =='0'):
				$user = $this->User->findById($this->Auth->user('id'));
				$this->Session->write('Auth',$user);
				$this->redirect(array('controller'=>'users','action'=>'registration_step2'));
				elseif($this->Auth->user('email_send') =='1' && $this->Auth->user('is_approved') =='0'):
				$id = $this->Auth->user('id');
				//$this->Session->destroy();
				$this->redirect(array('action'=>'thanksuser'));
				else:
                            $user = $this->User->findById($this->Auth->user('id'));
				$this->Session->write('Auth',$user);
				$this->redirect(array('controller'=>'fronts','action'=>'index'));
				//$this->redirect(array('controller'=>'clients','action'=>'my_account'));
				endif;
			}
                       else if($this->Auth->user('role_id')== 3) {
			                 
                                   
                                      if($this->Session->check('login_keyword') && $this->Session->check('login_city') && $this->Session->check('login_state')) {
                                    
                                       $loginIndustry = $this->Session->read('login_industry');
                                       $loginSubindustry = $this->Session->read('login_subindustry');
                                       $loginKeyword = $this->Session->read('login_keyword');
                                       $loginCity = $this->Session->read('login_city');
                                       $loginState = $this->Session->read('login_state');
                                      
                                       $this->Session->delete('login_industry');
                                       $this->Session->delete('login_subindustry');
                                       $this->Session->delete('login_keyword');
                                       $this->Session->delete('login_city');
                                       $this->Session->delete('login_state');
                                       $this->redirect(SITE_URL."browse/search_result/".$loginIndustry.'/'.$loginSubindustry.'/'.$loginKeyword.'/'.$loginState.'/'.$loginCity);
                                      
                                       }
                                       else if ($this->Session->check('browse_referrer')) {
                                       
                                       $browseReferrers = $this->Session->read('browse_referrer');
                                       $this->Session->delete('browse_referrer');
                                       $this->redirect(SITE_URL."browse/category/".$browseReferrers);
                                       }
                                       else if($this->Session->check('qna_category') && $this->Session->read('qna_category') != -1)
                                       {
                                        $qna_category = $this->Session->read('qna_category');
                                        $this->Session->delete('qna_category'); 
                                        $this->redirect(array('controller'=>'qna','action'=>'category',$qna_category));
                                       }
                                      else {
                                       
                                       $this->redirect(array('controller'=>'fronts','action'=>'index'));
                                      }
					//$this->redirect(array('controller'=>'clients','action'=>'my_account'));
				}else if($this->Auth->user('role_id')== 2) {
					$this->redirect(array('controller'=>'users','action'=>'my_account'));
				}else{
					$this->redirect(array('controller'=>'users','action'=>'users_listasdfrgt12341'));
				}
			}
			 
		
	}**/

	function logout(){
		$this->Session->delete();
              $this->Session->delete('login_industry');
              $this->Session->delete('login_subindustry');
	      $this->Session->delete('login_keyword');
              $this->Session->delete('login_city');
              $this->Session->delete('login_state');
              $this->Session->delete('browse_referrer');
              $this->Session->delete('project_url');
              $this->Session->delete('mediaUrl');
              $this->Session->delete('member_serviceUrl');
              $this->Session->delete('flevyUrl');
              $this->Session->delete('nlensesUrl');
              $this->Session->delete('sservicesUrl');
              $this->Session->delete('project_login');
              $this->Session->delete('invited_member');
              $this->Session->delete('qna_category');
              $this->Session->delete('qna_question');
              $this->Session->delete('menteePopup');
              $this->Session->delete('projectId');
              $this->Session->delete('questionId');
              $this->Session->delete('duserid');
              $this->Session->delete('mentor');
              $this->Session->delete('mentorshipId');
              $this->Session->delete('consultantPopup');
              $this->Session->delete('directoryclaim');
              $this->Session->delete('projectindexUrl');
              $this->Session->delete('invoicepaypage');

              $this->redirect($this->Auth->logout());
	}

	function other_testimonials($id=null)
	{
		$userData = $this->UserReference->find('first',array('conditions'=>array('UserReference.user_id'=>$id)));
		$this->layout = 'ajax';
		$this->set("feedback_link",$userData['UserReference']['feedback_link']);
		$this->set("title_for_layout","Other Testimonials");
	}

	function my_account($id= null){

                $this->layout= 'defaultnew';

		if($id == "" && $this->Session->read('Auth.User.id')=='' && ($this->Session->read('Auth.User.role_id')!='2' && $this->Session->read('Auth.User.role_id')!='3'))
		{

			$this->redirect(array('controller'=>'fronts','action'=>'index','openloginpopup:'.true));
		}
		if($id=="")
		{

			$id=$this->Auth->user('id');
		}
		else
		{

			//$newuser=mysql_fetch_array(mysql_query("SELECT * FROM users WHERE url_key='".$id."'"));
                       $newuser = $this->User->find('first',array('conditions'=>array('User.url_key'=>$id)));

			  $id=$newuser['User']['id'];

			if($id=="")
			{
				$this->redirect(array('controller'=>'fronts','action'=>'index'));
				$this->set("ERROR","No Records Exist");
			}
		}
              
              $this->User->Behaviors->attach('Containable');

		$this->data = $this->User->find('first',
				array(
						'contain'=>array(
								'UserReference'=>array(
										'fields'=>array('first_name','last_name','background_summary','area_of_expertise','accept_application','desire_mentee_profile','fee_first_hour','fee_regular_session','regular_session_per','created','zipcode','past_clients','guarantee','feedback_link','feeNSession','linkedin_headline','business_website','intro_link'),
								),
								'UserImage' ,
								
								'Communication',
								'Availablity',
								'Social',
								'CaseStudy',
								'MemberIndustryCategory',
								'Feed',
                                                                'Testimonial',
                                                                'ShowcaseClient',
                                                                'Proposal'
						),
						'conditions'=>array('User.id'=>$id)
				)

		);

          
          
		if($this->data['User']['status'] == 0) {
			$this->redirect(array('controller'=>'fronts','action'=>'index'));
		}
              if($this->data['User']['url_key'] != $this->request['url']['url']){
                    $this->Session->write('refers',$this->data['User']['url_key']);
                    $this->Session->write('fname',$this->data['UserReference']['first_name']);
                    $this->Session->write('lname',$this->data['UserReference']['last_name']);
                    $this->Session->write('email',$this->data['User']['username']);              
              }

              $feeds = $this->Feed->find('all',array('conditions' => array('Feed.user_id' => $id),'order'=>'utc_published DESC'));
              
              $this->set('feeds', $feeds);
		//Get Questions 'from members category which are not answered by him
		$member_category1 = $this->data['MemberIndustryCategory']['category1'];
		$member_category2 = $this->data['MemberIndustryCategory']['category2'];
		$member_category3 = $this->data['MemberIndustryCategory']['category3'];

		$questions = array();
		$qCount = 0;
		 
		$ind_obj1 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $this->data['MemberIndustryCategory']['industry1'])));
		$cat_obj1 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $this->data['MemberIndustryCategory']['category1'])));
		
		$ind_obj2 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $this->data['MemberIndustryCategory']['industry2'])));
		$cat_obj2 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $this->data['MemberIndustryCategory']['category2'])));
		
		$ind_obj3 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $this->data['MemberIndustryCategory']['industry3'])));
		$cat_obj3 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $this->data['MemberIndustryCategory']['category3'])));
		
		$industry_string = '';
		
		if(empty($ind_obj1) == false && empty($cat_obj1) == false) {
			
			if($cat_obj1['IndustryCategory']['category'] == "ALL CATEGORIES")
				$industry_string = $industry_string . $ind_obj1['IndustryCategory']['category'];
			else
				$industry_string = $industry_string . $ind_obj1['IndustryCategory']['category'] . ":  " . $cat_obj1['IndustryCategory']['category'];
			
			$industry_string = $industry_string . "|";
		}

		if(empty($ind_obj2) == false && empty($cat_obj2) == false) {
			
			if($cat_obj2['IndustryCategory']['category'] == "ALL CATEGORIES")
				$industry_string = $industry_string . $ind_obj2['IndustryCategory']['category'];
			else
				$industry_string = $industry_string . $ind_obj2['IndustryCategory']['category'] . ": " . $cat_obj2['IndustryCategory']['category'];
			
			$industry_string = $industry_string . "|";
		}

		if(empty($ind_obj3) == false && empty($cat_obj3) == false) {
			
			if($cat_obj3['IndustryCategory']['category'] == "ALL CATEGORIES")
				$industry_string = $industry_string . $ind_obj3['IndustryCategory']['category'];
			else
				$industry_string = $industry_string . $ind_obj3['IndustryCategory']['category'] . ": " . $cat_obj3['IndustryCategory']['category'];
			
			$industry_string = $industry_string . "|";
		}
		
		$cat_list = $industry_string;
		
		$this->set('cat_list', $cat_list);


		
		$this->QnaQuestion->Behaviors->attach('Containable');
		if($member_category1 != -1){
			$category1_qs = $this->QnaQuestionCategory->find('all', array('conditions' => array('QnaQuestionCategory.topic_id' => $member_category1)));
			 
			foreach ($category1_qs as $value) {

				$question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.id' => $value['QnaQuestionCategory']['qna_question_id'], 'QnaQuestion.isdraft'=>'0')));
				if(isset($question) && $question != '') {
					$answered = $this->QnaAnswer->find('first', array('conditions' => array('QnaAnswer.question_id' => $value['QnaQuestionCategory']['qna_question_id'])));
					$isIgnored = $this->QnaIgnoredQuestion->find('first', array('conditions' => array('QnaIgnoredQuestion.question_id' => $value['QnaQuestionCategory']['qna_question_id'],'QnaIgnoredQuestion.member_id' => $id)));
					if($answered == null && $isIgnored == null)
						$questions[$value['QnaQuestionCategory']['qna_question_id']] = $question;
				}
			}
		}
		if($member_category2 != -1) {
			$category2_qs = $this->QnaQuestionCategory->find('all', array('conditions' => array('QnaQuestionCategory.topic_id' => $member_category2)));
			 
			foreach ($category2_qs as $value) {
				$question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.id' => $value['QnaQuestionCategory']['qna_question_id'], 'QnaQuestion.isdraft'=>'0')));
				if(isset($question) && $question != '') {
					$answered = $this->QnaAnswer->find('first', array('conditions' => array('QnaAnswer.question_id' => $value['QnaQuestionCategory']['qna_question_id'])));
					$isIgnored = $this->QnaIgnoredQuestion->find('first', array('conditions' => array('QnaIgnoredQuestion.question_id' => $value['QnaQuestionCategory']['qna_question_id'],'QnaIgnoredQuestion.member_id' => $id)));
					if($answered == null && $isIgnored == null)
						$questions[$value['QnaQuestionCategory']['qna_question_id']] = $question;
				}
			}
		}
		if($member_category3 != -1) {
			$category3_qs = $this->QnaQuestionCategory->find('all', array('conditions' => array('QnaQuestionCategory.topic_id' => $member_category3)));

			foreach ($category3_qs as $value) {
				$question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.id' => $value['QnaQuestionCategory']['qna_question_id'], 'QnaQuestion.isdraft'=>'0')));
				if(isset($question) && $question != '') {
					$answered = $this->QnaAnswer->find('first', array('conditions' => array('QnaAnswer.question_id' => $value['QnaQuestionCategory']['qna_question_id'])));
					$isIgnored = $this->QnaIgnoredQuestion->find('first', array('conditions' => array('QnaIgnoredQuestion.question_id' => $value['QnaQuestionCategory']['qna_question_id'],'QnaIgnoredQuestion.member_id' => $id)));
					if($answered == null && $isIgnored == null)
						$questions[$value['QnaQuestionCategory']['qna_question_id']] = $question;
				}
			}
		}

		//prd($questions);
		$this->set("questions",$questions);

                $casestudy = $this->CaseStudy->find('all', array('order'=>'CaseStudy.modified desc','conditions' => array('CaseStudy.user_id' => $id)));
		$this->set("casestudy",$casestudy);

		//GetQ categories
		$result = $this->Topic->find('all');
		$qcategories = array();
		foreach($result as $value){
			$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
		}
		$this->set("categories",$qcategories);

		//Get all questions answered by him
		$answers = $this->QnaAnswer->find('all',array('order'=>'QnaAnswer.id desc', 'conditions' => array('QnaAnswer.member_id' => $id)));
		$answeredQs = array();
		$aqCount = 0;
		foreach ($answers as $answer){
			 
			$question = $this->QnaQuestion->find('first',array('conditions' => array('QnaQuestion.id' => $answer['QnaAnswer']['question_id'])));
			$question['answer_text'] = $answer['QnaAnswer']['answer_text'];
			$answeredQs[$answer['QnaAnswer']['question_id']] = $question;
		}

		$this->set("answeredQs",$answeredQs);

              //Get all insights  by data
              $this->Insight->Behaviors->attach('Containable');
		$insight = $this->Insight->find('all',array('order'=>'Insight.id desc','conditions' => array('Insight.user_id' => $id, 'Insight.published' => 1)));
	
           
               $this->set("insightData",$insight);

              //Get all proposal  by data
                $proposalfinal = array();
		$proposalarray = $this->Proposal->find('all',array('order'=>'Proposal.modified desc','conditions' => array('Proposal.user_id' => $id)));
		foreach($proposalarray as $proposal){ 
                        $proposal['Proposal']['proposal_title'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_title']);
                        $proposal['Proposal']['proposal_details'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_details']);
                        $proposal['Proposal']['proposal_approach'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_approach']);
                        $proposal['Proposal']['proposal_success'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_success']);
                        $proposalfinal[$proposal['Proposal']['id']] = $proposal;
               }               

                $this->set("proposalData",$proposalfinal);
                 

		$this->loadModel('City');
		$dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($this->data['UserReference']['zipcode']))));
		$this->set("city",$dataZip['City']['city_name']);
		$this->set("state",$dataZip['City']['state']);
		$this->set("resourcepath","img/".MENTORS_RESOURCE_PATH.DS.$id.DS);

		$userTitle = $this->data['UserReference']['first_name'].' '.$this->data['UserReference']['last_name'];

		$this->set("title_for_layout",$userTitle);


		//finding number of invoices
		$mentor_id = $this->Session->read('Auth.User.id');


		$mentorData = $this->UserReference->find('first',array('conditions'=>array('UserReference.user_id'=>$mentor_id),'fields'=>array('first_name','last_name')));

		 
		//findinf number of projects
		$pQ = $this->Project->find('count', array('conditions' => array('Project.user_id' => $mentor_id)));
		$pQ += $this->Project->find('count', array('conditions' => array('Project.isPublic' => '1', 'Project.status' => '4')));

		if($pQ == 0) {
			$this->set("numberOfProjects", 0);
		} else {
			$this->set("numberOfProjects", 1);
		}

	}



	function autocompleteZipcode(){


		$data = array('AD'=>'aaaa','a'=>'ssss');
		echo json_encode($data);

	}
	function admin_runQuery() {
		$this->layout = 'admin';  
		$this->autoRender = false;
		/*ALTER TABLE `user_references` ADD `area_of_expertise` VARCHAR( 500 ) NOT NULL AFTER `phone` ,
		 ADD `desire_mentee_profile` VARCHAR( 1000 ) NOT NULL AFTER `area_of_expertise` */
		$this->User->query("ALTER TABLE `user_references` DROP `city_id`");
		echo "runed successfully.";
		die;
	}

	function _findexts ($filename)
	{
		$filename = strtolower($filename) ;
		$exts = split("[/\\.]", $filename) ;
		$n = count($exts)-1;
		$exts = $exts[$n];
		return $exts;
	}

	function profile_photo_edit()
	{
		$this->layout = 'ajax';
		$this->set('title_for_layout', 'Photo Edit');
		if (empty($this->data))
		{
			$this->set("id",$this->Auth->user('id'));
		}
		else
		{
			/*prd(phpinfo());
			 die();*/
			$this->_insertProfileImage($_FILES['photoedit'] , $this->Auth->user('id'));
			$this->redirect(array('action' => 'edit_account'));
		}

	}
	function social_link_edit()
	{
		//$this->autoRender=false;
		$this->layout = 'ajax';
		$this->set('title_for_layout', 'Social link Edit');
		if (empty($this->data))
		{
			$this->loadModel('UserImage');
			$this->data = $this->User->find('first',
					array(
							'contain'=>array(
									'Social',
							),
							'conditions'=>array('User.id'=>$this->Auth->user('id'))
					)

			);
			$this->set("id",$this->Auth->user('id'));
		}
		else
		{

			$this->Social->deleteAll(array('Social.user_id' => $this->Auth->user('id')));
			$linkCount=0;
			while(count($_REQUEST['link'])>$linkCount)
			{
				//prd($_FILES['resource_location']);
				if(trim($_REQUEST['link'][$linkCount])!='')
				{
					//prd($_REQUEST['link'][$linkCount]);
					$linkArray=array();
					$linkArray['Social']['user_id']=$this->Auth->user('id');
					$linkArray['Social']['social_name']=$_REQUEST['link'][$linkCount];
					$this->Social->create();
					$this->Social->save($linkArray,false);
				}
				$linkCount++;
			}
			//$this->_insertProfileImage($_FILES['photoedit'] , $this->Auth->user('id'));
			$this->redirect(array('action' => 'edit_account'));
		}
	}

	function _insertProfileImage($imageDataArr,$id) {
		//////uppload profile....

		$ext=$this->_findexts($imageDataArr['name']);
		if($ext=="jpg" || $ext=="jpeg" || $ext=="png" ||  $ext=="gif" )
		{

			if (isset($imageDataArr['name']) && $imageDataArr['name'] != '') {

				$this->loadModel('UserImage');

				$destination = IMAGES . MENTORS_IMAGE_PATH . DS . $id;
				if (!is_dir($destination)) {
					mkdir($destination, 0777);
				}
				$destination .=DS;
				if (!is_dir($destination.'small')) {
					mkdir($destination.'small', 0777);
				}
				$imageArr = $imageDataArr;
				//prd($destination);
				/*  $ad_image = md5(rand() * time()) . ".$ext";
				$originalImage=$destination.$ad_image;
				@copy($imageArr['tmp_name'],$originalImage);
				$actualImageName=md5(rand() * time()) . "11.$ext";
				$this->_smart_resize_image($imageArr['tmp_name'], $width = 150, $height = 150, $proportional = true, $output = $destination.$actualImageName, $delete_original = false, $use_linux_commands = false, $apply_watermark = false, $watermark_image = false );
				$this->_smart_resize_image($imageArr['tmp_name'], $width = 30, $height = 30, $proportional = true, $output = $destination.'small/'.$actualImageName, $delete_original = true, $use_linux_commands = false, $apply_watermark = false, $watermark_image = false );*/

				$result = $this->Upload->upload($imageArr, $destination, null, array('type' => 'resizecrop', 'size' => array('200', '200')));
				$result = $this->Upload->upload($imageArr, $destination.'small/', $this->Upload->result, array('type' => 'resizecrop', 'size' => array('40', '40')));
				//prd($this->Upload->result);
				$this->UserImage->updateAll(array('UserImage.image_name' => "'" . $this->Upload->result . "'"), array('UserImage.user_id' => $id));

			}
		}
	}


	/* Forgot password in front end */

	function forgot(){
		//step1
		$this->layout = 'ajax';
		if(!empty($this->data))
		{
			$email = $this->data['User']['username'];
			$user = $this->User->find('first',array('conditions'=>array('User.username'=>trim($email),'User.role_id <>'=>'1'),'fields'=>array('User.id','User.username','UserReference.first_name','UserReference.last_name')));
			if(!empty($user))
			{
				$actKey =  substr(md5(uniqid()), 0, 20);
				$this->request->data['User']['activation_key'] = $actKey;
				$this->request->data['User']['id'] = $user['User']['id'];
				$user['User']['activation_key'] = $actKey;
				if($this->User->save($this->request->data,false))
				{

					 
			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $this->data['User']['username'];
                           $subject = "Set password";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("forgot_password");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_key=".urlencode($actKey);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
					 
					////////////////
					$this->redirect(SITE_URL.'reset_mail_sent');
				}
				else{
					$this->redirect(array('controller'=>'fronts','action'=>'index'));
				}
			}
		}
	}

	function forgot_thanks()
	{
           $this->layout = 'defaultnew';
	}
	 
	function reset_password()
	{


                
		$error = '';
		if (!isset($this->request['url']['act']) && empty($this->data)) {

			$this->Session->write('forgot_error', 'You have clicked an invalid link');
		}
		if(isset($this->request['url']['act']) && $this->request['url']['act']!=NULL)
		{
			$user = $this->User->find('first',array('conditions'=>array('User.activation_key'=>$this->request['url']['act']),'fields'=>array('User.id'),'recursive'=>'0'));
			if(!empty($user) && count($user)>0)
			{

				$this->set('id',$user['User']['id']);
			}
			else
			{

			
				$this->Session->write('forgot_error', 'You have clicked an invalid link');
			}
		}
		if(!empty($this->data))
		{
			$saveData['User']['id'] = $this->data['User']['id'];
			$saveData['User']['activation_key'] = substr(md5(uniqid()), 0, 20);
			$saveData['User']['password'] = $this->data['User']['newpass'];//Security::hash($this->data['User']['newpass'], null, true);
			$saveData['User']['password2'] = Security::hash($this->data['User']['newpass'], null, true);

			if($this->User->save($saveData,false))
			{
				$this->Session->write('forgot_success', 'success');
			}
		}
	}

	function feedback()
	{
		$this->layout = 'ajax';
		$this->Feedback->bindModel(
				array(
						'belongsTo' => array(
								'Mentee' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('client_id=Mentee.user_id')),
						)));
		$fields = array('Feedback.*','Mentee.first_name','Mentee.last_name'/*,'Mentor.first_name','Mentor.last_name'*/);
		$data = $this->Feedback->find('all',array('conditions' => array('member_id'=>$this->Session->read('Auth.User.id')),'fields'=>$fields,'order'=>'Feedback.modified desc'));
		$this->set('data',$data);
		$mentorData = $this->UserReference->find('first',array('conditions'=>array('UserReference.user_id'=>$this->Session->read('Auth.User.id'))));
		$this->set('mentorData',$mentorData);
	}

	function feedbackdetail()
	{
		$this->layout = 'ajax';
	}


	/* sign up with linkedin social network START */

	function signuplinkedin()
	{




		if($this->Session->read('projectId') != '') {

			
			$this->redirect(array('controller'=>'project', 'action' => 'projectlinkedinAuth', $_REQUEST['oid'], $_REQUEST['email']));
		}
		if($this->Session->read('projectindexUrl') != '') {
			
			$this->redirect(array('controller'=>'project', 'action' => 'projectinterestlinkedinAuth', $_REQUEST['oid'], $_REQUEST['email']));
		}

		if($this->Session->read('menteePopup') != '') {

			$this->redirect(array('controller'=>'pricing', 'action' => 'PricingAuth', $_REQUEST['oid'], $_REQUEST['email']));
		}

		if($this->Session->read('questionId') != '') {
			
			$this->redirect(array( 'action' => 'linkedinQnAAuth', $_REQUEST['oid'], $_REQUEST['email']));
		}

		if($this->Session->read('mentorshipId') != '') {
			
			$this->redirect(array( 'action' => 'MentorshipRequestAuth', $_REQUEST['oid'], $_REQUEST['email']));
		}
		if($this->Session->read('duserid') != '') {
			
			$this->redirect(array( 'action' => 'directoryClaimAuth', $_REQUEST['oid'], $_REQUEST['email']));
		}
		if($this->Session->read('login_industry') != '') {
			
			$this->redirect(array( 'action' => 'linkedinSearchAuth', $_REQUEST['oid'], $_REQUEST['email']));
		}
		if($this->Session->read('consultantPopup') != '') {

			$this->redirect(array( 'action' => 'signuplinkedinMentorPopup', $_REQUEST['oid'], $_REQUEST['email']));
		}
		if($this->Session->read('mentor') != '') {
			
			$this->redirect(array( 'action' => 'signuplinkedinMentor', $_REQUEST['oid'], $_REQUEST['email']));
		}
		if($this->Session->read('directoryclaim') != '') {
			
			$this->redirect(array( 'action' => 'directoryClaimAuth', $_REQUEST['oid'], $_REQUEST['email']));
		}


		if(isset($_REQUEST['oid']) && $_REQUEST['oid']!='')
		{
                       
			$userData = $this->User->find('first',array('conditions'=>array('OR'=>array('User.username'=>$_REQUEST['email'],'User.socialId'=>$_REQUEST['oid'])),'recursive'=>'0'));
                              
			if(empty($userData))
			{

    		             /* Read linkedin data from text file  */
    		             $current = file_get_contents(SITE_URL.$_REQUEST['oid'].".txt");
    		             $linkData = explode("<br/>",$current);
    		             unlink($_REQUEST['oid'].".txt");
    		             $this->__subscribenewsletter($linkData[4]);
                           
		$saveData['User']['status'] = '1';
		$saveData['User']['activation_key'] = substr(md5(uniqid()), 0, 20);

		$this->User->create();
		$saveData['User']['access_specifier'] = 'publish';
		$saveData['User']['is_approved'] = '1';
		$saveData['User']['role_id'] = 3;
		$saveData['User']['username'] = $linkData[4];
		$saveData['User']['socialId'] = $socialId;
		$password = $this->General->randamPassword();
		$saveData['User']['password'] = $password;//Security::hash($password, null, true);
		$saveData['User']['password2'] = Security::hash($password, null, true);
			
		if($this->User->save($saveData,false))
		{
			$id = $this->User->id;

			if($linkData[5]!='' || $linkData[7]!='')
			{
				$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
				if(empty($socialLink))
				{
					$socialData['Social']['user_id'] = $id;
					$socialData['Social']['created_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				else
				{
					$socialData['Social']['id'] = $socialLink['Social']['id'];
					$socialData['Social']['modified_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				$this->Social->save($socialData);
				$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
				if($linkData[7]!='')
				{
					$twittLink = explode(',',$linkData[7]);
					for($i=0;$i<count($twittLink);$i++)
					{
						$this->Social->create();
						$twitData['Social']['user_id'] = $id;
						$twitData['Social']['created_by'] = $id;
						$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
						$this->Social->save($twitData);
					}
				}
			}

			$this->UserReference->create();
			$refData['UserReference']['first_name'] = $linkData[1];
			$refData['UserReference']['last_name'] = $linkData[2];
			$refData['UserReference']['user_id'] = $id;
			$refData['UserReference']['background_summary']  =  $linkData[6];
			$refData['UserReference']['headline']  =  $linkData[8];
			$this->User->createUrlKey($id);
			$this->UserReference->save($refData);

			if(isset($linkData[3]) && $linkData[3]!='')
			{
				$db_imgname = "user_".time().".jpeg";
				$file_name = IMAGES . MENTEES_IMAGE_PATH . DS . $id;

				$destination = IMAGES . MENTEES_IMAGE_PATH . DS . $id;
				$imagePath = $linkData[3];
				$des1 = $file_name.DS.'uploaded';
				$des2 = $file_name.DS.'small/';
				if (!is_dir($destination)) {
					mkdir($destination, 0777);
					mkdir($destination.DS.'small', 0777);
					mkdir($destination.DS.'uploaded', 0777);
				}
				if(!copy($imagePath,$file_name.DS.'uploaded'.DS.$db_imgname))
				{
					$linkData[3]="";
				}
				$cropdata = array();
				$cropdata['x'] = 0; $cropdata['y'] = 0; $cropdata['w'] = 40; $cropdata['h'] = 40; $cropdata['name'] = $db_imgname;
				$rr = $this->imageCrop($des1.DS,$cropdata,$des2,40,40);
				$rr = $this->imageCrop($des1.DS,$cropdata,$destination,200,200);
				$image_data['UserImage']['user_id'] = $id;
				$image_data['UserImage']['image_name'] = $db_imgname;
				$this->UserImage->save($image_data);
			}

			$user = $this->User->findById($id);
			if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
				$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
			else
				$this->Session->write('User.image','');

			$this->Session->write('Auth',$user);



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $linkData[4];
                           $subject = "Welcome to GUILD";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_register_linkedin");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_useremail=".urlencode($linkData[4]);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

			        $this->Session->setFlash(__("Welcome! You are now a GUILD client.", true), 'default', array('class' => 'success'));
				$this->redirect(array('controller' => 'clients', 'action' => 'my_account'));	
			
		      }			
                   }
			else
			{
				if($userData['User']['status']=='0')
				{
					
					if($userData['User']['access_specifier']=='draft' && $userData['User']['role_id']=='2')
					{

    		                           $current = file_get_contents(SITE_URL.$_REQUEST['oid'].".txt");
    		                           $linkData = explode("<br/>",$current);
    		                           unlink($_REQUEST['oid'].".txt");

						if(isset($linkData[3]) && $linkData[3]!='')
						{
                                                        $id = $userData['User']['id'];

                                                        $db_imgname = $linkData[1]."-".$linkData[2]."-Guild.jpeg";
							$file_name = IMAGES . MENTORS_IMAGE_PATH . DS . $id;

							$destination = IMAGES . MENTORS_IMAGE_PATH . DS . $id;
							$imagePath = $linkData[3];
							$des1 = $file_name.DS.'uploaded';
							$des2 = $file_name.DS.'small/';
							if (!is_dir($destination)) 
                                                        {
								mkdir($destination, 0777);
								mkdir($destination.DS.'small', 0777);
								mkdir($destination.DS.'uploaded', 0777);
							}
							if(!copy($imagePath,$file_name.DS.'uploaded'.DS.$db_imgname))
							{
								$linkData[3]="";
							}
							$cropdata = array();
							$cropdata['x'] = 0; $cropdata['y'] = 0; $cropdata['w'] = 40; $cropdata['h'] = 40; $cropdata['name'] = $db_imgname;
							$rr = $this->imageCrop($des1.DS,$cropdata,$des2,40,40);
							$rr = $this->imageCrop($des1.DS,$cropdata,$destination,200,200);
							$image_data['UserImage']['user_id'] = $userData['User']['id'];
							$image_data['UserImage']['image_name'] = $db_imgname;
							$this->UserImage->save($image_data);
						}


                                                $this->User->updateAll(array('User.status' => 1),array('User.id ' => $userData['User']['id']));

                                                $userreference = $this->UserReference->find('first',array('conditions'=>array('UserReference.user_id'=>$userData['User']['id'])));
                                                $userreference['UserReference']['linkedin_headline']  = $linkData[8];

						$this->UserReference->save($userreference);


						$user = $this->User->findById($userData['User']['id']);
						if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
							$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
						else
							$this->Session->write('User.image','');
						$this->Session->write('Auth.User',$user['User']);



						$sending = SITE_URL."members/member_full_profile";

						echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
						die;


					}
					
					$sending = SITE_URL."staticPages/thanks";
					if($userData['User']['access_specifier']=='draft')
					{
						$this->Session->setFlash(__('Your application has been already submitted.', true), 'default', array('class' => 'notclass'));
					}
					if($userData['User']['access_specifier']=='publish')
					{
						$this->Session->setFlash(__('Your profile has been deactivated. Please mail <a href="mailto:help@guild.im" class="delete">help@guild.im</a> to reactivate.', true), 'default', array('class' => 'notclass'));
					}
					echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
					die;
				}
				if($userData['User']['role_id']=='2')
				{
					$user['User']['id'] = $userData['User']['id'];
					$user['User']['socialId'] = $_REQUEST['oid'];
					$this->User->save($user,false);
					$user = $this->User->findById($userData['User']['id']);
					if($_REQUEST['public_url']!='')
					{
						$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$user['User']['id'],'social_name like'=>'%linkedin%')));
						if(empty($socialLink))
						{
							$socialData['Social']['user_id'] = $user['User']['id'];
							$socialData['Social']['created_by'] = $user['User']['id'];
							$socialData['Social']['social_name'] = $_REQUEST['public_url'];
						}
						else
						{
							$socialData['Social']['id'] = $socialLink['Social']['id'];
							$socialData['Social']['modified_by'] = $user['User']['id'];
							$socialData['Social']['social_name'] = $_REQUEST['public_url'];
						}
						$this->Social->save($socialData);
					}
					if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
						$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
					else
						$this->Session->write('User.image','');
					$this->Session->write('Auth.User',$user['User']);
					if($user['User']['access_specifier']=='draft')
						$sending = SITE_URL."members/member_full_profile";
					else if($this->Auth->user('role_id') != '' && $this->Auth->user('role_id') == 2 && $this->Session->check('qnaconsult') !=''){
			                   $qnaconsults = $this->Session->read('qnaconsult');
                                        $this->Session->delete('qnaconsult');
                                        $this->redirect(SITE_URL."qna/question/".$qnaconsults);
                                   }

                                   else if($this->Session->check('PageUrl') && $this->Session->read('PageUrl') != -1)
                                    {
                                        $page_url = $this->Session->read('PageUrl');
                                        $this->Session->delete('PageUrl');
                                        $this->redirect($page_url);
                                    }
                                   else if($this->Auth->user('role_id') != ''  && $this->Session->check('project_login') && $this->Session->read('project_login') != -1)
                                       {
                          	           $project = $this->Project->find('first',array('conditions'=>array('Project.activation_key'=>$this->Session->read('project_login'))));
                                       $project['Project']['user_id'] = $this->Auth->user('id');
					    $project['Project']['activation_key'] = '-1';
                                       $this->Project->save($project);



                                       $this->Session->delete('project_login');
                                       $this->redirect(array('controller'=>'project','action'=>'index',$project['Project']['project_url']));
                                       }
                                   else if($this->Session->check('project_url') && $this->Session->read('project_url') != -1)
                                    {
                                        $project_url = $this->Session->read('project_url');
                                        $this->Session->delete('project_url');
                                        $this->redirect(array('controller'=>'project','action'=>'index',$project_url));
                                    }
 
                                   else if($this->Session->check('qna_category') && $this->Session->read('qna_category') != -1)
                                       {
                                        $qna_category = $this->Session->read('qna_category');
                                        $this->Session->delete('qna_category'); 
                                        $this->redirect(array('controller'=>'qna','action'=>'category',$qna_category));
                                     }
                                   else if($this->Auth->user('role_id') ==2 && $this->Session->check('mediaUrl') && $this->Session->read('mediaUrl') != '')
                                       {
                                       $this->Session->delete('mediaUrl');
                                        $this->redirect(array('controller'=>'members','action'=>'pr'));
                                       }
                                  else if($this->Session->check('member_serviceUrl') && $this->Session->read('member_serviceUrl') != '')
                                       {
                                        $this->Session->delete('member_serviceUrl');
                                        $this->redirect(array('controller'=>'members','action'=>'advantage'));
                                       }
                                  else if($this->Session->check('flevyUrl') && $this->Session->read('flevyUrl') != '')
                                       {
                                        $this->Session->delete('flevyUrl');
                                        $this->redirect(array('controller'=>'members','action'=>'flevy'));
                                       }
                                  else if($this->Session->check('nlensesUrl') && $this->Session->read('nlensesUrl') != '')
                                       {
                                        $this->Session->delete('nlensesUrl');
                                        $this->redirect(array('controller'=>'members','action'=>'ninelenses'));
                                       }

                                  else if($this->Session->check('sservicesUrl') && $this->Session->read('sservicesUrl') != '')
                                       {
                                        $this->Session->delete('sservicesUrl');
                                        $this->redirect(array('controller'=>'members','action'=>'resources'));
                                       }
                                   else
						$sending = SITE_URL."users/my_account";
					echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
					die;
				}
				if($userData['User']['role_id']=='3')
				{
					//$user['User']['id'] = $userData['User']['id'];
					//$user['User']['socialId'] = $_REQUEST['oid'];
					//$this->User->save($user,false);
					$user = $this->User->findById($userData['User']['id']);
					if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
						$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
					else
						$this->Session->write('User.image','');
					$this->Session->write('Auth',$user);
					if($user['User']['access_specifier'] =='draft')
					{
						if($user['User']['email_send'] =='0'):
						$sending = SITE_URL."users/registration_step2";
						elseif($user['User']['email_send'] =='1' && $user['User']['is_approved']=='0'):
						$id = $user['User']['id'];
						//$this->Session->destroy();
						$sending = SITE_URL."users/thanksuser/";
						else:
						$user = $this->User->findById($this->Auth->user('id'));
						$this->Session->write('Auth',$user);
						$sending = SITE_URL."clients/my_account";
						endif;
					}
					else{
					    if ($this->Session->check('login_keyword') && $this->Session->check('login_city') && $this->Session->check('login_state')) {
                                       
                                       $loginIndustry = $this->Session->read('login_industry');
                                       $loginSubindustry = $this->Session->read('login_subindustry');
                                       $loginKeyword = $this->Session->read('login_keyword');
                                       $loginCity = $this->Session->read('login_city');
                                       $loginState = $this->Session->read('login_state');

                                       $this->Session->delete('login_industry');
                                       $this->Session->delete('login_subindustry');
                                       $this->Session->delete('login_keyword');
                                       $this->Session->delete('login_city');
                                       $this->Session->delete('login_state');
                                       $this->redirect(SITE_URL."browse/search_result/".$loginIndustry.'/'.$loginSubindustry.'/'.$loginKeyword.'/'.$loginState.'/'.$loginCity);
                                       }
                                       else if($this->Session->check('browse_referrer')) {
                                       $browseReferrers = $this->Session->read('browse_referrer');
                                       $this->Session->delete('browse_referrer');
                                       $this->redirect(SITE_URL."browse/category/".$browseReferrers);
                                       }
                                      else if($this->Session->check('PageUrl') && $this->Session->read('PageUrl') != -1)
                                       {
                                        $page_url = $this->Session->read('PageUrl');
                                        $this->Session->delete('PageUrl');
                                        $this->redirect($page_url);
                                      } 
                                       else if($this->Session->check('qna_category') && $this->Session->read('qna_category') != -1)
                                       {
                                        $qna_category = $this->Session->read('qna_category');
                                        $this->Session->delete('qna_category'); 
                                        $this->redirect(array('controller'=>'qna','action'=>'category',$qna_category));
                                       }

                                       else if($this->Session->check('invoice_Id'))
                                       {
				
			                  $invoiceId = $this->Session->read('invoice_Id');
                                       $this->Session->delete('invoice_Id');
                                       $this->redirect(SITE_URL."invoice/order_confirm/".$invoiceId); 
                                       }
                                       else if($this->Auth->user('role_id') != ''  && $this->Session->check('project_login') && $this->Session->read('project_login') != -1)
                                       {
                          	           $project = $this->Project->find('first',array('conditions'=>array('Project.activation_key'=>$this->Session->read('project_login'))));
                                       $project['Project']['user_id'] = $this->Auth->user('id');
					    $project['Project']['activation_key'] = '-1';
                                       $this->Project->save($project);

                                       $this->Session->delete('project_login');
                                       $this->redirect(array('controller'=>'project','action'=>'index',$project['Project']['project_url']));
                                       }
                                      else if($this->Session->check('project_url') && $this->Session->read('project_url') != -1)
                                      {
                                        $project_url = $this->Session->read('project_url');
                                        $this->Session->delete('project_url');
                                        $this->redirect(array('controller'=>'project','action'=>'index',$project_url));
                                      }
                                      else if($this->Session->check('flevyUrl') && $this->Session->read('flevyUrl') != -1)
                                      {
                                       
                                        $this->Session->delete('flevyUrl');
                                        $this->redirect(array('controller'=>'members','action'=>'flevy'));
                                      }

                                      else if($this->Session->check('nlensesUrl') && $this->Session->read('nlensesUrl') != -1)
                                      {
                                       
                                        $this->Session->delete('nlensesUrl');
                                        $this->redirect(array('controller'=>'members','action'=>'ninelenses'));
                                      }
                                      else if($this->Session->check('sservicesUrl') && $this->Session->read('sservicesUrl') != -1)
                                      {
                                       
                                        $this->Session->delete('sservicesUrl');
                                        $this->redirect(array('controller'=>'members','action'=>'resources'));
                                      }
                                       else{
                                          $sending = SITE_URL."fronts/index";}
						//$sending = SITE_URL."clients/my_account";
					    }
					echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
					die;
				}
				else
				{
					$sending = SITE_URL."thanks";
					$this->Session->setFlash(__("Sorry! You are unauthorized user.", true), 'default', array('class' => 'notclass'));
					echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
					die;
				}
			}
		}
		else
		{
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("Sorry! You are unauthorized user.", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
			die;
		}
	}

	function signuplinkedinMentor($oid, $email)
	{         
		if(isset($oid) && $oid!='' && $email!='')
		{

               
			$userData = $this->User->find('first',array('conditions'=>array('OR'=>array('User.username'=>$email,'User.socialId'=>$oid)),'recursive'=>'0'));
			if(empty($userData))
			{
				$linkedinArr = array();

				/* read linkedin data from summary text file
				 $cc[0]=>oid,$cc[1]=>first_name,$cc[2]=>last_name,$cc[3]=>imageurl,$cc[4]=>email,$cc[5]=>publicurl,$cc[6]=>summary
				*/
				$current = file_get_contents(SITE_URL.$oid.".txt");
				$linkData = explode("<br/>",$current);
				unlink($oid.".txt");

				$linkedinArr['username']   = $email;
				$linkedinArr['socialId']   = $oid;
				$linkedinArr['first_name'] = $linkData[1];
				$linkedinArr['last_name']  =  $linkData[2];
				$linkedinArr['summary']  =  $linkData[6];
				$linkedinArr['profile_url']  =  $linkData[5];
                            $this->Session->write('linkedinArr',$linkedinArr);

				$sending = SITE_URL."members/apply";
				echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
				die;
			}
			else
			{
				if($userData['User']['status']=='0')
				{
					$sending = SITE_URL."staticPages/thanks";
					if($userData['User']['access_specifier']=='draft')
					{
						$this->Session->setFlash(__("Your application has been already submitted.", true), 'default', array('class' => 'notclass'));
					}
					if($userData['User']['access_specifier']=='publish')
					{
						$this->Session->setFlash(__('Your profile has been deactivated. Please mail <a href="mailto:help@guild.im" class="delete">help@guild.im</a> to reactivate.', true), 'default', array('class' => 'notclass'));
					}
					echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
					die;
				}
				if($userData['User']['role_id']=='2')
				{
					$user['User']['id'] = $userData['User']['id'];
					$user['User']['socialId'] = $oid;
					$this->User->save($user,false);
					/*if($_REQUEST['public_url']!='')
					 {
					$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$user['User']['id'],'social_name like'=>'%linkedin%')));
					if(empty($socialLink))
					{
					$socialData['Social']['user_id'] = $user['User']['id'];
					$socialData['Social']['created_by'] = $user['User']['id'];
					$socialData['Social']['social_name'] = $_REQUEST['public_url'];
					}
					else
					{
					$socialData['Social']['id'] = $socialLink['Social']['id'];
					$socialData['Social']['modified_by'] = $user['User']['id'];
					$socialData['Social']['social_name'] = $_REQUEST['public_url'];
					}
					$this->Social->save($socialData);
					}*/
					$user = $this->User->findById($userData['User']['id']);
					if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
						$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
					else
						$this->Session->write('User.image','');
					$this->Session->write('Auth.User',$user['User']);

					if($user['User']['access_specifier']=='draft')
						$sending = SITE_URL."members/member_full_profile";
					else
						$sending = SITE_URL."users/my_account";
					echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
					die;
				}
				else
				{
					$sending = SITE_URL."thanks";
					$this->Session->setFlash('Existing user account. Please login to your account or contact <a href="mailto:help@guild.im" class="delete">help@guild.im.</a>', 'default', array('class' => 'notclass'));
					echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
					die;
				}
			}
		}
		else
		{
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("Sorry! You are unauthorized user.", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
			die;
		}
	}
	function directoryClaimAuth($oid, $email) {

                 
		if(isset($oid) && $oid!='')
		{


			$current = file_get_contents(SITE_URL.$oid.".txt");
			$linkData = explode("<br/>",$current);
			unlink($oid.".txt");

			if($linkData[4] == '' || $linkData[1] == '') {

				$linkData[0] = $oid;
				$linkData[4] = $email;
			}


	              $d_user_id  =  $this->Session->read('directoryclaim');
                      $this->Session->delete('directoryclaim');

			$public_url = $linkData[5];
			$email = $linkData[4];
                        $fname = $linkData[1];
                        $lname = $linkData[2];

			$dUser = $this->DirectoryUser->find('first',array('conditions'=>array('DirectoryUser.id'=>$d_user_id)));
                        $match = $this->General->addhttp($dUser['DirectoryUser']['linkedin_url']);
			$dfname = $dUser['DirectoryUser']['first_name'];
                        $dlname = $dUser['DirectoryUser']['last_name'];

			if($dUser['DirectoryUser']['username'] == $email ||  $match == $this->General->addhttp($public_url) || (strpos($fname,'$dfname') == false && strpos($lname,'$dlname') == false) ){
				 
				//Search in user -- if not found --> create -- if found --> see hasPublished and redirect
				$existEmail = $this->User->find('first',array('conditions'=>array('User.username'=>$email)));
				 
				$saveData['User']['status'] = '1';
				$saveData['User']['activation_key'] = substr(md5(uniqid()), 0, 20);
				if(empty($existEmail))
				{
					$this->User->create();
					$saveData['User']['access_specifier'] = 'draft';
					$saveData['User']['is_approved'] = '1';
					$saveData['User']['role_id'] = '2';
					$saveData['User']['username'] = $linkData[4];
					$saveData['User']['socialId'] = $oid;
					$password = $this->General->randamPassword();
					$saveData['User']['password'] = $password;//Security::hash($password, null, true);
					$saveData['User']['password2'] = Security::hash($password, null, true);

					if($this->User->save($saveData,false))
					{
						$id = $this->User->id;
						if($linkData[5]!='' || $linkData[7]!='')
						{
							$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
							if(empty($socialLink))
							{
								$socialData['Social']['user_id'] = $id;
								$socialData['Social']['created_by'] = $id;
								$socialData['Social']['social_name'] = $linkData[5];
							}
							else
							{
								$socialData['Social']['id'] = $socialLink['Social']['id'];
								$socialData['Social']['modified_by'] = $id;
								$socialData['Social']['social_name'] = $linkData[5];
							}
							$this->Social->save($socialData);                                                        
							$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
							if($linkData[7]!='')
							{
								$twittLink = explode(',',$linkData[7]);
								for($i=0;$i<count($twittLink);$i++)
								{
									$this->Social->create();
									$twitData['Social']['user_id'] = $id;
									$twitData['Social']['created_by'] = $id;
									$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
									$this->Social->save($twitData);
								}
							}
						}
						 

						$this->UserReference->create();
						$refData['UserReference']['first_name'] = $linkData[1];
						$refData['UserReference']['last_name'] = $linkData[2];
						$refData['UserReference']['user_id'] = $id;
						$refData['UserReference']['background_summary']  =  $linkData[6];
                                                $refData['UserReference']['linkedin_headline']  = $linkData[8];	 
						$this->UserReference->save($refData);
						$this->User->createUrlKey($id);
						if(isset($linkData[3]) && $linkData[3]!='')
						{
							$db_imgname = $linkData[1]."-".$linkData[2]."-Guild.jpeg";
							$file_name = IMAGES . MENTORS_IMAGE_PATH . DS . $id;
							 
							$destination = IMAGES . MENTORS_IMAGE_PATH . DS . $id;
							$imagePath = $linkData[3];
							$des1 = $file_name.DS.'uploaded';
							$des2 = $file_name.DS.'small/';
							if (!is_dir($destination)) {
								mkdir($destination, 0777);
								mkdir($destination.DS.'small', 0777);
								mkdir($destination.DS.'uploaded', 0777);
							}
							if(!copy($imagePath,$file_name.DS.'uploaded'.DS.$db_imgname))
							{
								$linkData[3]="";
							}
							$cropdata = array();
							$cropdata['x'] = 0; $cropdata['y'] = 0; $cropdata['w'] = 40; $cropdata['h'] = 40; $cropdata['name'] = $db_imgname;
							$rr = $this->imageCrop($des1.DS,$cropdata,$des2,40,40);
							$rr = $this->imageCrop($des1.DS,$cropdata,$destination,200,200);
							$image_data['UserImage']['user_id'] = $id;
							$image_data['UserImage']['image_name'] = $db_imgname;
							$this->UserImage->save($image_data);
						}
						 
						$user = $this->User->findById($id);
							
						$dUser = $this->DirectoryUser->findById($d_user_id);
						$dUser['DirectoryUser']['user_id'] = $id;
						$this->DirectoryUser->save($dUser);
						if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
							$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
						else
							$this->Session->write('User.image','');
						$this->Session->write('Auth.User',$user['User']);

                          //EasticEmail Integration - Begin

                           $res = "";
                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $linkData[4];
                           $subject = "Welcome to GUILD";
                           $urlkey = $user['User']['url_key'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("consultant_registration_email");
                           $data .= "&merge_userfname=".urlencode($linkData[1]);
                           $data .= "&merge_userurlkey=".urlencode($urlkey);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);

                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
						 
						$sending = SITE_URL."members/member_full_profile";
						 
						echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
						die;
					}
				} else { //User exists but he has clicked on claim profile again -- i.e he is in draft mode. Login him and redirect to profile creation

					$id = $existEmail['User']['id'];
					if($existEmail['User']['role_id'] != 2) {
						 
						$sending = SITE_URL."thanks";
						$this->Session->setFlash(__("Sorry! You are unauthorized user. Contact us at help@guild.im", true), 'default', array('class' => 'notclass'));
						echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
						die;
						 
					} else {
                                           
                                         
						$saveData['User']['id'] = $existEmail['User']['id'];
						$saveData['User']['socialId'] = $oid;
						if($this->User->save($saveData,false))
						{
							if($linkData[5]!='' || $linkData[7]!='')
							{
								$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
								if(empty($socialLink))
								{
									$socialData['Social']['user_id'] = $id;
									$socialData['Social']['created_by'] = $id;
									$socialData['Social']['social_name'] = $linkData[5];
								}
								else
								{
									$socialData['Social']['id'] = $socialLink['Social']['id'];
									$socialData['Social']['modified_by'] = $id;
									$socialData['Social']['social_name'] = $linkData[5];
								}
								$this->Social->save($socialData);
								if($linkData[7]!='')
								{
									$twittLink = explode(',',$linkData[7]);
									$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
									for($i=0;$i<count($twittLink);$i++)
									{
										$this->Social->create();
										$twitData['Social']['user_id'] = $id;
										$twitData['Social']['created_by'] = $id;
										$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
										$this->Social->save($twitData);
									}
								}
							}
							$user = $this->User->findById($id);
							if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
								$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
							else
								$this->Session->write('User.image','');
							$this->Session->write('Auth.User',$user['User']);
							if($user['User']['access_specifier']=='draft')
								$sending = SITE_URL."members/member_full_profile";
							else
								$sending = SITE_URL."users/my_account";
							 
							echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
							die;
						}
					}
				}
			} else {
				$sending = SITE_URL."thanks";
				$this->Session->setFlash(__(" <b>Failed to claim profile</b>. Linked In has returned a different name ($fname $lname) from what we have in our records ($dfname $dlname). Please email us at <a href='mailto:help@guild.im' class='delete'>help@guild.im</a>.", true), 'default', array('class' => 'notclass'));
				echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
				die;
			}
		}
		 
	}

	function signuplinkedinMentorPopup($oid, $email)
	{
	   if(isset($oid) && $oid!='')
		{
						//$userData = $this->User->find('first',array('conditions'=>array('OR'=>array('User.username'=>$email,'User.socialId'=>$oid)),'recursive'=>'0'));
						
	
				/* Read linkedin data from text file  */
				$current = file_get_contents(SITE_URL.$oid.".txt");
				$linkData = explode("<br/>",$current);
				unlink($oid.".txt");
				$this->__subscribenewsletter($linkData[4]);
				$saveData['User']['status'] = '1';
				$saveData['User']['activation_key'] = substr(md5(uniqid()), 0, 20);
				$existEmail = $this->User->find('first',array('conditions'=>array('OR'=>array('User.username'=>$email,'User.socialId'=>$oid)),'recursive'=>'0'));
				if(empty($existEmail))
				{
					$this->User->create();
					$saveData['User']['access_specifier'] = 'draft';
					$saveData['User']['is_approved'] = '1';
					$saveData['User']['role_id'] = '2';
					$saveData['User']['username'] = $linkData[4];
					$saveData['User']['socialId'] = $oid;
					$password = $this->General->randamPassword();
					$saveData['User']['password'] = $password;//Security::hash($password, null, true);
					$saveData['User']['password2'] = Security::hash($password, null, true);

					if($this->User->save($saveData,false))
					{
						$id = $this->User->id;
						if($linkData[5]!='' || $linkData[7]!='')
						{
							$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
							if(empty($socialLink))
							{
								$socialData['Social']['user_id'] = $id;
								$socialData['Social']['created_by'] = $id;
								$socialData['Social']['social_name'] = $linkData[5];
							}
							else
							{
								$socialData['Social']['id'] = $socialLink['Social']['id'];
								$socialData['Social']['modified_by'] = $id;
								$socialData['Social']['social_name'] = $linkData[5];
							}
							$this->Social->save($socialData);
							$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
							if($linkData[7]!='')
							{
								$twittLink = explode(',',$linkData[7]);
								for($i=0;$i<count($twittLink);$i++)
								{
									$this->Social->create();
									$twitData['Social']['user_id'] = $id;
									$twitData['Social']['created_by'] = $id;
									$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
									$this->Social->save($twitData);
								}
							}
						}

						$this->UserReference->create();
						$refData['UserReference']['first_name'] = $linkData[1];
						$refData['UserReference']['last_name'] = $linkData[2];
						$refData['UserReference']['user_id'] = $id;
						$refData['UserReference']['background_summary']  =  $linkData[6];
                        $refData['UserReference']['linkedin_headline']  = $linkData[8];

						$this->UserReference->save($refData);
						$this->User->createUrlKey($id);
						if(isset($linkData[3]) && $linkData[3]!='')
						{
							//$db_imgname = "user_".time().".jpeg";
                            $db_imgname = $linkData[1]."-".$linkData[2]."-Guild.jpeg";
							$file_name = IMAGES . MENTORS_IMAGE_PATH . DS . $id;

							$destination = IMAGES . MENTORS_IMAGE_PATH . DS . $id;
							$imagePath = $linkData[3];
							$des1 = $file_name.DS.'uploaded';
							$des2 = $file_name.DS.'small/';
							if (!is_dir($destination)) {
								mkdir($destination, 0777);
								mkdir($destination.DS.'small', 0777);
								mkdir($destination.DS.'uploaded', 0777);
							}
							if(!copy($imagePath,$file_name.DS.'uploaded'.DS.$db_imgname))
							{
								$linkData[3]="";
							}
							$cropdata = array();
							$cropdata['x'] = 0; $cropdata['y'] = 0; $cropdata['w'] = 40; $cropdata['h'] = 40; $cropdata['name'] = $db_imgname;
							$rr = $this->imageCrop($des1.DS,$cropdata,$des2,40,40);
							$rr = $this->imageCrop($des1.DS,$cropdata,$destination,200,200);
							$image_data['UserImage']['user_id'] = $id;
							$image_data['UserImage']['image_name'] = $db_imgname;
							$this->UserImage->save($image_data);
						}

						$user = $this->User->findById($id);
						if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
							$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
						else
							$this->Session->write('User.image','');
						$this->Session->write('Auth.User',$user['User']);

                          //EasticEmail Integration - Begin

                           $res = "";
                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $linkData[4];
                           $subject = "Welcome to GUILD";
                           $urlkey = $user['User']['url_key'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("consultant_registration_email");
                           $data .= "&merge_userfname=".urlencode($linkData[1]);
                           $data .= "&merge_userurlkey=".urlencode($urlkey);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);

                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End



						$sending = SITE_URL."members/member_full_profile";

						echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
						die;
					}
				}
				else
				{
					if($existEmail['User']['role_id']=='2')
					{
						$id = $existEmail['User']['id'];
						$saveData['User']['id'] = $existEmail['User']['id'];
						$saveData['User']['socialId'] = $oid;
						if($this->User->save($saveData,false))
						{
							if($linkData[5]!='' || $linkData[7]!='')
							{
								$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
								if(empty($socialLink))
								{
									$socialData['Social']['user_id'] = $id;
									$socialData['Social']['created_by'] = $id;
									$socialData['Social']['social_name'] = $linkData[5];
								}
								else
								{
									$socialData['Social']['id'] = $socialLink['Social']['id'];
									$socialData['Social']['modified_by'] = $id;
									$socialData['Social']['social_name'] = $linkData[5];
								}
								$this->Social->save($socialData);
								if($linkData[7]!='')
								{
									$twittLink = explode(',',$linkData[7]);
									$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
									for($i=0;$i<count($twittLink);$i++)
									{
										$this->Social->create();
										$twitData['Social']['user_id'] = $id;
										$twitData['Social']['created_by'] = $id;
										$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
										$this->Social->save($twitData);
									}
								}
							}
							$user = $this->User->findById($id);
							if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
								$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
							else
								$this->Session->write('User.image','');
							$this->Session->write('Auth.User',$user['User']);
							if($user['User']['access_specifier']=='draft')
								$sending = SITE_URL."members/member_full_profile";
							else
								$sending = SITE_URL."users/my_account";
							echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
							die;
						}
					}
					else
					{
						$sending = SITE_URL."thanks";
						$this->Session->setFlash('Sorry! You have already registered with other Role in GUILD.');
						echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
						die;
					}
				}



			
	    }  else
		    {
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("Sorry! You are unauthorized user.", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
			die;
		   }
		
	
	}


	function signuplinkedinMenteePopup()
	{
		if(isset($oid) && $oid!=''&& $_REQUEST['email']!='')
		{
			$userData = $this->User->find('first',array('conditions'=>array('OR'=>array('User.username'=>$_REQUEST['email'],'User.socialId'=>$oid)),'recursive'=>'0'));
			/* Read linkedin data from text file  */
			$current = file_get_contents(SITE_URL.$oid.".txt");
			$linkData = explode("<br/>",$current);
			unlink($oid.".txt");
			$this->__subscribenewsletter($linkData[4]);
				
			if(empty($userData))
			{
				$saveData['User']['status'] = '1';
				$saveData['User']['activation_key'] = substr(md5(uniqid()), 0, 20);
				$existEmail = $this->User->find('first',array('conditions'=>array('User.username'=>$linkData[4])));
				if(empty($existEmail))
				{
					$this->User->create();
					$saveData['User']['access_specifier'] = 'draft';
					$saveData['User']['is_approved'] = '1';
					$saveData['User']['role_id'] = 3;
					$saveData['User']['username'] = $linkData[4];
					$saveData['User']['socialId'] = $oid;
					$password = $this->General->randamPassword();
					$saveData['User']['password'] = $password;//Security::hash($password, null, true);
					$saveData['User']['password2'] = Security::hash($password, null, true);

					if($this->User->save($saveData,false))
					{
						$id = $this->User->id;

						if($linkData[5]!='' || $linkData[7]!='')
						{
							$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
							if(empty($socialLink))
							{
								$socialData['Social']['user_id'] = $id;
								$socialData['Social']['created_by'] = $id;
								$socialData['Social']['social_name'] = $linkData[5];
							}
							else
							{
								$socialData['Social']['id'] = $socialLink['Social']['id'];
								$socialData['Social']['modified_by'] = $id;
								$socialData['Social']['social_name'] = $linkData[5];
							}
							$this->Social->save($socialData);
							$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
							if($linkData[7]!='')
							{
								$twittLink = explode(',',$linkData[7]);
								for($i=0;$i<count($twittLink);$i++)
								{
									$this->Social->create();
									$twitData['Social']['user_id'] = $id;
									$twitData['Social']['created_by'] = $id;
									$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
									$this->Social->save($twitData);
								}
							}
						}

						$this->UserReference->create();
						$refData['UserReference']['first_name'] = $linkData[1];
						$refData['UserReference']['last_name'] = $linkData[2];
						$refData['UserReference']['user_id'] = $id;
						$refData['UserReference']['background_summary']  =  $linkData[6];
						$refData['UserReference']['headline']  =  $linkData[8];
						$this->User->createUrlKey($id);
						$this->UserReference->save($refData);

						if(isset($linkData[3]) && $linkData[3]!='')
						{
							$db_imgname = "user_".time().".jpeg";
							$file_name = IMAGES . MENTEES_IMAGE_PATH . DS . $id;

							$destination = IMAGES . MENTEES_IMAGE_PATH . DS . $id;
							$imagePath = $linkData[3];
							$des1 = $file_name.DS.'uploaded';
							$des2 = $file_name.DS.'small/';
							if (!is_dir($destination)) {
								mkdir($destination, 0777);
								mkdir($destination.DS.'small', 0777);
								mkdir($destination.DS.'uploaded', 0777);
							}
							if(!copy($imagePath,$file_name.DS.'uploaded'.DS.$db_imgname))
							{
								$linkData[3]="";
							}
							$cropdata = array();
							$cropdata['x'] = 0; $cropdata['y'] = 0; $cropdata['w'] = 40; $cropdata['h'] = 40; $cropdata['name'] = $db_imgname;
							$rr = $this->imageCrop($des1.DS,$cropdata,$des2,40,40);
							$rr = $this->imageCrop($des1.DS,$cropdata,$destination,200,200);
							$image_data['UserImage']['user_id'] = $id;
							$image_data['UserImage']['image_name'] = $db_imgname;
							$this->UserImage->save($image_data);
						}

						$user = $this->User->findById($id);
						if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
							$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
						else
							$this->Session->write('User.image','');
						$this->Session->write('Auth',$user);

						$sending = SITE_URL."users/registration_step2";
						//$sending = SITE_URL."mentors/activate/".$linkData[4]."/".$userData['User']['activation_key'];
						echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
						die;
					}
				}
				else
				{
					if($existEmail['User']['role_id']=='3')
					{
						$id = $existEmail['User']['id'];
						$saveData['User']['id'] = $existEmail['User']['id'];
						$saveData['User']['socialId'] = $oid;
						if($this->User->save($saveData,false))
						{
							if($linkData[5]!='' || $linkData[7]!='')
							{
								$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
								if(empty($socialLink))
								{
									$socialData['Social']['user_id'] = $id;
									$socialData['Social']['created_by'] = $id;
									$socialData['Social']['social_name'] = $linkData[5];
								}
								else
								{
									$socialData['Social']['id'] = $socialLink['Social']['id'];
									$socialData['Social']['modified_by'] = $id;
									$socialData['Social']['social_name'] = $linkData[5];
								}
								$this->Social->save($socialData);
								if($linkData[7]!='')
								{
									$twittLink = explode(',',$linkData[7]);
									$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
									for($i=0;$i<count($twittLink);$i++)
									{
										$this->Social->create();
										$twitData['Social']['user_id'] = $id;
										$twitData['Social']['created_by'] = $id;
										$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
										$this->Social->save($twitData);
									}
								}
							}
							$user = $this->User->findById($id);
							if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
								$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
							else
								$this->Session->write('User.image','');
							$this->Session->write('Auth.User',$user['User']);

							if($existEmail['User']['access_specifier'] =='draft')
							{
								if($existEmail['User']['email_send'] =='0'):
								$sending = SITE_URL."users/registration_step2";
								elseif($existEmail['User']['email_send'] =='1' && $existEmail['User']['is_approved']=='0'):
								$id = $existEmail['User']['id'];
								//$this->Session->destroy();
								$sending = SITE_URL."users/thanksuser";
								else:
								$user = $this->User->findById($this->Auth->user('id'));
								$this->Session->write('Auth',$user);
								$sending = SITE_URL."clients/my_account";
								endif;
							}
							else{
								$sending = SITE_URL."fronts/index";
								//$sending = SITE_URL."clients/my_account";
							}
							echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
							die;
						}
					}
					else
					{
						$sending = SITE_URL."thanks";
						$this->Session->setFlash(__("You are already registered, please login to access your account or contact <a href='mailto:help@guild.im' class='delete'>help@guild.im</a>", true), 'default', array('class' => 'notclass'));
						echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
						die;
					}
				}

			}
			else
			{
				$existEmail = $userData;
				if($existEmail['User']['role_id']=='3')
				{
					$id = $existEmail['User']['id'];
					$saveData['User']['id'] = $existEmail['User']['id'];
					$saveData['User']['socialId'] = $oid;
					if($this->User->save($saveData,false))
					{
						if($linkData[5]!='' || $linkData[7]!='')
						{
							$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
							if(empty($socialLink))
							{
								$socialData['Social']['user_id'] = $id;
								$socialData['Social']['created_by'] = $id;
								$socialData['Social']['social_name'] = $linkData[5];
							}
							else
							{
								$socialData['Social']['id'] = $socialLink['Social']['id'];
								$socialData['Social']['modified_by'] = $id;
								$socialData['Social']['social_name'] = $linkData[5];
							}
							$this->Social->save($socialData);
							if($linkData[7]!='')
							{
								$twittLink = explode(',',$linkData[7]);
								$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
								for($i=0;$i<count($twittLink);$i++)
								{
									$this->Social->create();
									$twitData['Social']['user_id'] = $id;
									$twitData['Social']['created_by'] = $id;
									$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
									$this->Social->save($twitData);
								}
							}
						}
						$user = $this->User->findById($id);
						if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
							$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
						else
							$this->Session->write('User.image','');
						$this->Session->write('Auth.User',$user['User']);
						if($existEmail['User']['access_specifier'] =='draft')
						{
							if($existEmail['User']['email_send'] =='0'):
							$sending = SITE_URL."users/registration_step2";
							elseif($existEmail['User']['email_send'] =='1' && $existEmail['User']['is_approved']=='0'):
							$id = $existEmail['User']['id'];
							//$this->Session->destroy();
							$sending = SITE_URL."users/thanksuser";
							else:
							$user = $this->User->findById($this->Auth->user('id'));
							$this->Session->write('Auth',$user);
							$sending = SITE_URL."clients/my_account";
							endif;
						}
						else{
							$sending = SITE_URL."fronts/index";
							//$sending = SITE_URL."clients/my_account";
						}
						echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
						die;
					}
				}
				else
				{
					$sending = SITE_URL."thanks";
					$this->Session->setFlash(__("You are already registered, please login to access your account or contact <a href='mailto:help@guild.im' class='delete'>help@guild.im</a>", true), 'default', array('class' => 'notclass'));
					echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
					die;
				}
			}
		}
		else
		{
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("Sorry! You are unauthorized user.", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
			die;
		}
	}

	function linkedinQnAAuth($oid, $email)
	{
		if(isset($oid) && $oid!=''&& $email!='')
		{
			/* Read linkedin data from text file  */
			$current = file_get_contents(SITE_URL.$oid.".txt");
			$linkData = explode("<br/>",$current);
			unlink($oid.".txt");
			$this->__subscribenewsletter($linkData[4]);

		       $Question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.id' =>$this->Session->read('questionId'))));
                       $this->Session->delete('questionId');

			if($linkData[4] == '' || $linkData[1] == '') {
				 
				$linkData[0] = $oid;
				$linkData[4] = $email;
			}
			 
			$existEmail = $this->User->find('first',array('conditions'=>array('User.username'=>$linkData[4])));
			if(empty($existEmail)) //Email Id was not found in DB -- Register as a client
			{
				$id = $this->__registerUsingLinkedin($oid, $linkData);
                                $Question['QnaQuestion']['user_id'] = $id;
                                $this->QnaQuestion->save($Question);
			}
			else //Email Id found in DB -- Login
			{
				if($existEmail['User']['role_id']==3 || $existEmail['User']['role_id']==2)
				{
					$id = $existEmail['User']['id'];
                                        $Question['QnaQuestion']['user_id'] = $id;
                                        $this->QnaQuestion->save($Question);
					$this->__loginUsingLinkedin($oid, $existEmail, $linkData);
				}
				else
				{
					$sending = SITE_URL."thanks";
					$this->Session->setFlash(__("You are already registered, please login to access your account or contact <a href='mailto:help@guild.im' class='delete'>help@guild.im</a>", true), 'default', array('class' => 'notclass'));
					echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
					die;
				}
			}
			
			//if(isset($linkData[9]) && $linkData[9]!=''){
    				


         if($id !=''){


    			
	               $checkBoxes=$this->Session->read('checkBoxes');
			$cat = explode(',',$checkBoxes);
				
			for($i=0;$i<count($cat);$i++){
			
				if(isset($cat[$i]) && $cat[$i] != '') {
					
					$topic = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$cat[$i])));
					
					$Question_category['qna_question_id'] = $this->QnaQuestion->id;
					$Question_category['topic_id'] = $topic['Topic']['id'];
					
					$this->QnaQuestionCategory->save($Question_category);
					$this->QnaQuestionCategory->id = '';
					$this->Topic->id = '';
				}
			}
                     $userData = $this->User->find('first',
    				array(
    						'contain'=>array(
    						'UserReference'=>array(
    										'fields'=>array('first_name','last_name','zipcode'),
    										),
    										'UserImage' ,
    						),
    						'conditions'=>array('User.id'=>$id)
    					)
    				);
    			


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $helpemail = "help@guild.im";
                           $to = $helpemail.",".$userData['User']['username'];
                           $subject = "Your question has been submitted";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("question_submitted");
                           $data .= "&merge_userfname=".urlencode($linkData[1]);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
    			
    			//}
    		
			$sending = SITE_URL."qna/thanks_for_asking";
			echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
			die;
                  }
		}
		else
		{
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("Sorry! You are unauthorized user.", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
			die;
		}
	}

	function QFromMailAuth()
	{
		if(isset($oid) && $oid!=''&& $_REQUEST['email']!='')
		{
			/* Read linkedin data from text file  */
			$current = file_get_contents(SITE_URL.$oid.".txt");
			$linkData = explode("<br/>",$current);
			unlink($oid.".txt");
			$this->__subscribenewsletter($linkData[4]);

			if($linkData[4] == '' || $linkData[1] == '') {
				 
				$linkData[0] = $oid;
				$linkData[4] = $_REQUEST['email'];
			}
			 
			$existEmail = $this->User->find('first',array('conditions'=>array('User.username'=>$linkData[4])));
			if(empty($existEmail)) //Email Id was not found in DB -- Register as a client
			{
				$id = $this->__registerUsingLinkedin($oid, $linkData);
			}
			else //Email Id found in DB -- Login
			{
				if($existEmail['User']['role_id']==3 || $existEmail['User']['role_id']==2)
				{
					$id = $existEmail['User']['id'];
						
					$this->__loginUsingLinkedin($oid, $existEmail, $linkData);
				}
				else
				{
					$sending = SITE_URL."thanks";
					$this->Session->setFlash(__("You are already registered, please login to access your account or contact <a href='mailto:help@guild.im' class='delete'>help@guild.im</a>", true), 'default', array('class' => 'notclass'));
					echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
					die;
				}
			}
			if(isset($linkData[9]) && $linkData[9]!=''){
				$questionURL = $linkData[9];
			}

			$sending = SITE_URL."qna/question/".$questionURL;

			echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
			die;
		}
		else
		{
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("Sorry! You are unauthorized user.", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
			die;
		}
	}
	 
	function projectAuth() {
		 
		if(isset($_REQUEST['oid']) && $_REQUEST['oid']!=''&& $_REQUEST['email']!='')
		{
			/* Read linkedin data from text file  */
			$current = file_get_contents(SITE_URL.$_REQUEST['oid'].".txt");
			$linkData = explode("<br/>",$current);
			unlink($_REQUEST['oid'].".txt");
			$this->__subscribenewsletter($linkData[4]);

			if($linkData[4] == '' || $linkData[1] == '') {
				 
				$linkData[0] = $_REQUEST['oid'];
				$linkData[4] = $_REQUEST['email'];
			}
			 
			$existEmail = $this->User->find('first',array('conditions'=>array('User.username'=>$linkData[4])));
			if(empty($existEmail)) //Email Id was not found in DB -- Register as a client
			{
				$id = $this->__registerUsingLinkedin($_REQUEST['oid'], $linkData);
			}
			else //Email Id found in DB -- Login
			{
				if($existEmail['User']['role_id']==3 || $existEmail['User']['role_id']==2)
				{
					$id = $existEmail['User']['id'];
					 
					$this->__loginUsingLinkedin($_REQUEST['oid'], $existEmail, $linkData);
				}
				else
				{
					$sending = SITE_URL."thanks";
					$this->Session->setFlash(__("You are already registered, please login to access your account or contact <a href='mailto:help@guild.im' class='delete'>help@guild.im</a>", true), 'default', array('class' => 'notclass'));
					echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
					die;
				}
			}
			if(isset($linkData[9]) && $linkData[9]!=''){
				$projectId = $linkData[9];
				$project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectId)));
				$project['Project']['user_id'] = $id;
				$project['Project']['status'] = 0;
				$this->Project->save($project);
			}
			 
			$sending = SITE_URL."project/create/".$projectId;
			 
			echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
			die;
		}
		else
		{
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("Sorry! You are unauthorized user.", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
			die;
		}
	}

	 
	function directoryConsultAuth() {

		if(isset($_REQUEST['oid']) && $_REQUEST['oid']!=''&& $_REQUEST['email']!='')
		{
			/* Read linkedin data from text file  */
			$current = file_get_contents(SITE_URL.$_REQUEST['oid'].".txt");
			$linkData = explode("<br/>",$current);
			unlink($_REQUEST['oid'].".txt");
			$this->__subscribenewsletter($linkData[4]);

			if($linkData[4] == '' || $linkData[1] == '') {

				$linkData[0] = $_REQUEST['oid'];
				$linkData[4] = $_REQUEST['email'];
			}

			$existEmail = $this->User->find('first',array('conditions'=>array('User.username'=>$linkData[4])));
			if(empty($existEmail)) //Email Id was not found in DB -- Register as a client
			{
				$id = $this->__registerUsingLinkedin($_REQUEST['oid'], $linkData);
			}
			else //Email Id found in DB -- Login
			{
				if($existEmail['User']['role_id']==3 || $existEmail['User']['role_id']==2)
				{
					$id = $existEmail['User']['id'];
						
					$this->__loginUsingLinkedin($_REQUEST['oid'], $existEmail, $linkData);
				}
				else
				{
					$sending = SITE_URL."thanks";
					$this->Session->setFlash(__("You are already registered, please login to access your account or contact <a href='mailto:help@guild.im' class='delete'>help@guild.im</a>", true), 'default', array('class' => 'notclass'));
					echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
					die;
				}
			}
			if(isset($linkData[9]) && $linkData[9]!=''){
				$directoryConsultationId = $linkData[9];
				$dConsult = $this->DirectoryConsultation->find('first', array('conditions' => array('DirectoryConsultation.id' => $directoryConsultationId)));
				$dConsult['DirectoryConsultation']['client_id'] = $id;
				$this->DirectoryConsultation->save($dConsult);
			}

			//Send Email to Directory user
			$dUser = $this->DirectoryUser->find('first', array('conditions' => array('DirectoryUser.id' => $dConsult['DirectoryConsultation']['d_user_id'])));
			 
		      $prospect['Prospect']['firstname'] = $linkData[1];
		      $prospect['Prospect']['lastname'] = $linkData[2];
		      $prospect['Prospect']['email'] = $linkData[4];
		      $prospect['Prospect']['company'] = "GUILD Member";
		      $prospect['Prospect']['phone'] = "";
		      $prospect['Prospect']['answer'] = trim($dConsult['DirectoryConsultation']['message']);
		      $prospect['Prospect']['member_id'] = $dConsult['DirectoryConsultation']['d_user_id'];
		      $prospect['Prospect']['prospect_type'] = "DIRECTORY CONSULTATION";
		
		      $this->Prospect->save($prospect);

			//EasticEmail Integration - Begin

                           /**$res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $dUser['DirectoryUser']['username'];
                           $subject = "Request for Initial Consultation";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("directory_user_consultation");
                           $data .= "&merge_duserfname=".urlencode($dUser['DirectoryUser']['first_name']);                           
                           $data .= "&merge_clientfname=".urlencode($linkData[1]);
                           $data .= "&merge_clientlname=".urlencode($linkData[2]);
                           $data .= "&merge_clientemail=".urlencode($linkData[4]);
                           $data .= "&merge_clientneed=".urlencode($dConsult['DirectoryConsultation']['message']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }**/


                           //EasticEmail Integration - End

                     // Send Email to Admin


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "ankit@guild.im";
                           $subject = "A new consultation request for Directory Member has been submitted by a new register client";


                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("visitor_consultation_submit");
                           $data .= "&merge_consultantfname=".urlencode($dUser['DirectoryUser']['first_name']);
                           $data .= "&merge_consultantlname=".urlencode($dUser['DirectoryUser']['last_name']);
                           $data .= "&merge_visitorfname=".urlencode($linkData[1]);
                           $data .= "&merge_visitorlname=".urlencode($linkData[2]);
                           $data .= "&merge_visitoremail=".urlencode($linkData[4]);
                           $data .= "&merge_visitorphone=".urlencode($linkData[4]);
                           $data .= "&merge_visitorneed=".urlencode(trim($dConsult['DirectoryConsultation']['message']));
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $linkData[4];
                           $subject = "Your request for an initial consultation";


                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("visitor_application_request_sent");
                           $data .= "&merge_consultantfname=".urlencode($dUser['DirectoryUser']['first_name']);
                           $data .= "&merge_consultantlname=".urlencode($dUser['DirectoryUser']['last_name']);
                           $data .= "&merge_visitorfname=".urlencode($linkData[1]);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End





			$sending = SITE_URL."fronts/thanks_for_consulting";
			 
			echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
			die;
		}
		else
		{
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("Sorry! You are unauthorized user.", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
			die;
		}
	}

	function feedbackAuth() {

		if(isset($_REQUEST['oid']) && $_REQUEST['oid']!=''&& $_REQUEST['email']!='')
		{
			/* Read linkedin data from text file  */
			$current = file_get_contents(SITE_URL.$_REQUEST['oid'].".txt");
			$linkData = explode("<br/>",$current);
			unlink($_REQUEST['oid'].".txt");
			$this->__subscribenewsletter($linkData[4]);

			if($linkData[4] == '' || $linkData[1] == '') {

				$linkData[0] = $_REQUEST['oid'];
				$linkData[4] = $_REQUEST['email'];
			}

			$existEmail = $this->User->find('first',array('conditions'=>array('User.username'=>$linkData[4])));
			if(empty($existEmail)) //Email Id was not found in DB -- Register as a client
			{
				$id = $this->__registerUsingLinkedin($_REQUEST['oid'], $linkData);
			}
			else //Email Id found in DB -- Login
			{
				if($existEmail['User']['role_id']==3 || $existEmail['User']['role_id']==2)
				{
					$id = $existEmail['User']['id'];
						
					$this->__loginUsingLinkedin($_REQUEST['oid'], $existEmail, $linkData);
				}
				else
				{
					$sending = SITE_URL."thanks";
					$this->Session->setFlash(__("You are already registered, please login to access your account or contact <a href='mailto:help@guild.im' class='delete'>help@guild.im</a>", true), 'default', array('class' => 'notclass'));
					echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
					die;
				}
			}
			if(isset($linkData[9]) && $linkData[9]!=''){
				$feedbackId = $linkData[9];
				$dConsult = $this->Feedback->find('first', array('conditions' => array('Feedback.id' => $feedbackId)));
				$dConsult['Feedback']['client_id'] = $id;
				$this->Feedback->save($dConsult);
			}
			 
			$sending = SITE_URL."fronts/thanks_for_feedback";
			 
			echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
			die;
		}
		else
		{
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("Sorry! You are unauthorized user.", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
			die;
		}
	}
      
       function linkedinSearchAuth($oid, $email){
            //prd($oid);
            if(isset($oid) && $oid!=''&& $email!='')
    	     {
    		/* Read linkedin data from text file  */
    		$current = file_get_contents(SITE_URL.$oid.".txt");
    		$linkData = explode("<br/>",$current);
    		unlink($oid.".txt");
    		$this->__subscribenewsletter($linkData[4]);
    		
    		if($linkData[4] == '' || $linkData[1] == '') {
    			
    			$linkData[0] = $oid;
    			$linkData[4] = $email;
    		}
    			
    		$existEmail = $this->User->find('first',array('conditions'=>array('User.username'=>$linkData[4])));
    		if(empty($existEmail) && $linkData != '' ) //Email Id was not found in DB -- Register as a client
    		{
    			$id = $this->__registerUsingLinkedin($oid, $linkData);
    		}
    		else //Email Id found in DB -- Login
    		{
    			if($existEmail['User']['role_id']==3 || $existEmail['User']['role_id']==2)
    			{
    					$id = $existEmail['User']['id'];
    					$this->__loginUsingLinkedin($oid, $existEmail, $linkData);
    			}
    			else
    			{
    				$sending = SITE_URL."thanks";
    				$this->Session->setFlash(__("You are already registered, please login to access your account or contact <a href='mailto:help@guild.im' class='delete'>help@guild.im</a>", true), 'default', array('class' => 'notclass'));
    				echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
    				die;
    			}
    		}
                       

                       
                   
                                     if ($this->Session->check('login_keyword') && $this->Session->check('login_city') && $this->Session->check('login_state')) {

                                       $loginIndustry = $this->Session->read('login_industry');
                                       $loginSubindustry = $this->Session->read('login_subindustry');                                       
                                       $loginKeyword = $this->Session->read('login_keyword');
                                       $loginCity = $this->Session->read('login_city');
                                       $loginState = $this->Session->read('login_state');

                                       $this->Session->delete('login_industry');
                                       $this->Session->delete('login_subindustry');
                                       $this->Session->delete('login_keyword');
                                       $this->Session->delete('login_city');
                                       $this->Session->delete('login_state');
                                       $this->redirect(SITE_URL."browse/search_result/".$loginIndustry.'/'.$loginSubindustry.'/'.$loginKeyword.'/'.$loginState.'/'.$loginCity);
                                      }

                                       else if($this->Session->check('invoice_Id'))
                                       {
				
			              $invoiceId = $this->Session->read('invoice_Id');
                                      $this->Session->delete('invoice_Id');
                                      $this->redirect(array('controller'=>'invoice','action'=>'order_confirm/'.$invoiceId)); 
                                      }
                                       else{
                                          $sending = SITE_URL."fronts/index";
                                      }
						
					    
  
                   }
                                       
                                                  
                    else {
         	   $sending = SITE_URL."thanks";
                 $this->Session->setFlash(__("Sorry! You are unauthorized user.", true), 'default', array('class' => 'notclass'));
                 echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
                 die;
              }            

      }


	function MentorshipRequestAuth($oid, $email) {

		if(isset($oid) && $oid!=''&& $email!='')
		{
			/* Read linkedin data from text file  */
			$current = file_get_contents(SITE_URL.$oid.".txt");
			$linkData = explode("<br/>",$current);
			unlink($oid.".txt");
			$this->__subscribenewsletter($linkData[4]);

			if($linkData[4] == '' || $linkData[1] == '') {

				$linkData[0] = $oid;
				$linkData[4] = $email;
			}

			$existEmail = $this->User->find('first',array('conditions'=>array('User.username'=>$linkData[4])));
			if(empty($existEmail)) //Email Id was not found in DB -- Register as a client
			{
				$id = $this->__registerUsingLinkedin($oid, $linkData);
			}
			else //Email Id found in DB -- Login
			{
				if($existEmail['User']['role_id']==3)
				{
					$id = $existEmail['User']['id'];
						
					$this->__loginUsingLinkedin($oid, $existEmail, $linkData);
				}
				else if ($existEmail['User']['role_id']==2) {

					if($this->Session->check('mentorshipId') && $this->Session->read('mentorshipId')!=''){
				             $mentorshipId = $this->Session->read('mentorshipId');
				             $mentorshipData = $this->ConsultationRequest->find('first',array('conditions'=>array('ConsultationRequest.id'=>$mentorshipId)));

					       $id = $existEmail['User']['id'];
						
					       $this->__loginUsingLinkedin($oid, $existEmail, $linkData);

                                         if($mentorshipData['ConsultationRequest']['consultant_id'] == $existEmail['User']['id']){
						$this->ConsultationRequest->deleteAll(array('ConsultationRequest.id'=>$linkData[9]));
				              $this->Session->setFlash(__("This action is not allowed", true), 'default', array('class' => 'success'));
                                          $this->redirect(array('controller'=>'users','action'=>'my_account'));


                                          }
					}
				}
			}
			if($this->Session->check('mentorshipId') && $this->Session->read('mentorshipId')!=''){
				$mentorshipId = $this->Session->read('mentorshipId');
                                $consultationId = $this->Session->read('consultationId');
                                $this->Session->delete('mentorshipId');
                                $this->Session->delete('consultationId');
				$mentorshipData = $this->ConsultationRequest->find('first',array('conditions'=>array('ConsultationRequest.id'=>$consultationId)));
                                $project = $this->Project->find('first',array('conditions'=>array('Project.id'=>$mentorshipId)));
				 
				$mentorshipData['ConsultationRequest']['client_id'] = $this->Auth->user('id');
				$this->ConsultationRequest->save($mentorshipData);

				$project['Project']['user_id'] = $this->Auth->user('id');
				$this->Project->save($project);

				$menteeData = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
				$mentorData = $this->User->find('first',array('conditions'=>array('User.id'=>$mentorshipData['ConsultationRequest']['consultant_id'])));
				 
                                          $this->Session->write('FirstName',$menteeData['UserReference']['first_name']);
                                          $this->Session->write('LastName',$menteeData['UserReference']['last_name']);
                                          $this->Session->write('Email',$menteeData['User']['username']);
				 
			//EasticEmail Integration - Begin




                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $menteeData['User']['username'];
                           $subject = "Your requirement has been submitted for review";
                           $link = SITE_URL.'clients/my_account/'.$menteeData['User']['id'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("consultation_project_submit_confirmation");
                           $data .= "&merge_userfname=".urlencode($menteeData['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           $data .= "&merge_projecttitle=".urlencode($project['Project']['title']);
                           $data .= "&merge_profilelink=".urlencode($link);

                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }

                           //EasticEmail Integration - End

				 
				//send email to mentor
				if($menteeData != ''){


		               $prospect['Prospect']['firstname'] = $menteeData['UserReference']['first_name'];
		               $prospect['Prospect']['lastname'] = $menteeData['UserReference']['last_name'];
		               $prospect['Prospect']['email'] = $menteeData['User']['username'];
		               $prospect['Prospect']['company'] = "GUILD";
		               $prospect['Prospect']['phone'] = " ";
		               $prospect['Prospect']['answer'] = trim($mentorshipData['ConsultationRequest']['req_message']);
		               $prospect['Prospect']['member_id'] = $mentorData['User']['id'];
		               $prospect['Prospect']['prospect_type'] = "CONSULTATION";
		
		               $this->Prospect->save($prospect);
		


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "iqbal@guild.im";
                           $subject = "A new consultation request has been submitted by a logged in User";


                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("visitor_consultation_submit");
                           $data .= "&merge_consultantfname=".urlencode($mentorData['UserReference']['first_name']);
                           $data .= "&merge_consultantlname=".urlencode($mentorData['UserReference']['last_name']);
                           $data .= "&merge_visitorfname=".urlencode($menteeData['UserReference']['first_name']);
                           $data .= "&merge_visitorlname=".urlencode($menteeData['UserReference']['last_name']);
                           $data .= "&merge_visitoremail=".urlencode($menteeData['User']['username']);
                           $data .= "&merge_visitorphone=".urlencode(" ");
                           $data .= "&merge_visitorneed=".urlencode(trim($mentorshipData['ConsultationRequest']['req_message']));
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


                          }
                          else{



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "ankit@guild.im";
                           $subject = "Consultation request received without client Deatils";


                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentors_application_request");
                           $data .= "&merge_consultantfname=".urlencode($mentorData['UserReference']['first_name']);
                           $data .= "&merge_consultantlname=".urlencode($mentorData['UserReference']['last_name']);
                           $data .= "&merge_visitorfname=".urlencode($menteeData['UserReference']['first_name']);
                           $data .= "&merge_visitorlname=".urlencode($menteeData['UserReference']['last_name']);
                           $data .= "&merge_visitoremail=".urlencode($menteeData['User']['username']);
                           $data .= "&merge_visitorphone=".urlencode(" ");
                           $data .= "&merge_visitorneed=".urlencode(trim($mentorshipData['Mentorship']['mentee_need']));
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


                           }



				 
			}


				
				if($this->Auth->user('role_id') == 3)
					$this->redirect(array('controller'=>'project','action'=>'schedule_call',$project['Project']['id']));
				else 
					$this->redirect(array('controller'=>'project','action'=>'schedule_call',$project['Project']['id']));

		}
		else
		{
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("Sorry! You are unauthorized user.", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
			die;
		}
	}

	/* sign up with linkedin social network  END */

	function imageCrop($src,$cropdata,$dest,$targ_w =300,$targ_h=250)
	{
		$Imgpath = $src;
		$srcPath = str_replace("\\","/",$Imgpath.$cropdata['name']);
		$dest = str_replace("\\","/",$dest);
		$dest = $dest.DS.$cropdata['name'];
		$jpeg_quality = 100;
		$img_r = imagecreatefromjpeg($srcPath);
		$maxW = $targ_w;
		$maxH = $targ_h;
		$uploadSize = getimagesize($srcPath);
		$uploadWidth = $uploadSize[0];
		$uploadHeight = $uploadSize[1];
		$uploadType = $uploadSize[2];

		/**if ($uploadType != 1 && $uploadType != 2 && $uploadType != 3) {
			$this->error("File type must be GIF, PNG, or JPG to resize");
		}**/

		switch ($uploadType) {
			case 1: $srcImg = imagecreatefromgif($srcPath);
			break;
			case 2: $srcImg = imagecreatefromjpeg($srcPath);
			break;
			case 3: $srcImg = imagecreatefrompng($srcPath);
			break;
			//default: $this->error("File type must be GIF, PNG, or JPG to resize");
		}

			/**$ratioX = $maxW / $uploadWidth;
			$ratioY = $maxH / $uploadHeight;
		
			#figure out new dimensions
			if (($uploadWidth == $maxW) && ($uploadHeight == $maxH)) {
				$newX = $uploadWidth;
				$newY = $uploadHeight;
			} else if (($ratioX * $uploadHeight) > $maxH) {
				$newX = $maxW;
				$newY = ceil($ratioX * $uploadHeight);
			} else {
				$newX = ceil($ratioY * $uploadWidth);
				$newY = $maxH;
			}
			//imagefilter($srcImg, IMG_FILTER_GRAYSCALE);
			$dstImg = imagecreatetruecolor($newX, $newY);
			imagecopyresampled($dstImg, $srcImg, 0, 0, 0, 0, $newX, $newY, $uploadWidth, $uploadHeight);**/


                $ratioX = $maxW / $uploadWidth;
                $ratioY = $maxH / $uploadHeight;

                if ($ratioX < $ratioY) {
                    $newX = round(($uploadWidth - ($maxW / $ratioY)) / 2);
                    $newY = 0;
                    $uploadWidth = round($maxW / $ratioY);
                    $uploadHeight = $uploadHeight;
                } else {
                    $newX = 0;
                    $newY = round(($uploadHeight - ($maxH / $ratioX)) / 2);
                    $uploadWidth = $uploadWidth;
                    $uploadHeight = round($maxH / $ratioX);
                }
		//imagefilter($srcImg, IMG_FILTER_GRAYSCALE);
                $dstImg = imagecreatetruecolor($maxW, $maxH);
                $white = imagecolorallocate($dstImg, 255, 255, 255);
                imagefill($dstImg, 0, 0, $white);
                imagecolortransparent($dstImg,$white);
                imagecopyresampled($dstImg, $srcImg, 0, 0, $newX, $newY, $maxW, $maxH, $uploadWidth, $uploadHeight);


		$write =imagejpeg($dstImg,$dest,$jpeg_quality);
		return $write;
	}


	function checkUser()
	{
		if($this->Session->read('Auth.User.id')!='')
			echo $this->Session->read('Auth.User.id');
		else
			echo '0';
		die;
	}



	function cancellinkedin()
	{
		$sending = SITE_URL;
		echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
		die;
	}

	function __subscribenewsletter($email=null)
	{
		if($email!="")
		{
			$newsletterData = $this->NewsLetter->find('first',array('conditions'=>array('NewsLetter.email'=>$email)));
			if(empty($newsletterData))
			{
				$newsData['NewsLetter']['email'] = $email;
				$newsData['NewsLetter']['status'] = '1';
				$this->NewsLetter->save($newsData);
			}
		}
	}

	function __registerUsingLinkedin($socialId, $linkData){

		$saveData['User']['status'] = '1';
		$saveData['User']['activation_key'] = substr(md5(uniqid()), 0, 20);

		$this->User->create();
		$saveData['User']['access_specifier'] = 'publish';
		$saveData['User']['is_approved'] = '1';
		$saveData['User']['role_id'] = 3;
		$saveData['User']['username'] = $linkData[4];
		$saveData['User']['socialId'] = $socialId;
		$password = $this->General->randamPassword();
		$saveData['User']['password'] = $password;//Security::hash($password, null, true);
		$saveData['User']['password2'] = Security::hash($password, null, true);
			
		if($this->User->save($saveData,false))
		{
			$id = $this->User->id;

			if($linkData[5]!='' || $linkData[7]!='')
			{
				$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
				if(empty($socialLink))
				{
					$socialData['Social']['user_id'] = $id;
					$socialData['Social']['created_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				else
				{
					$socialData['Social']['id'] = $socialLink['Social']['id'];
					$socialData['Social']['modified_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				$this->Social->save($socialData);
				$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
				if($linkData[7]!='')
				{
					$twittLink = explode(',',$linkData[7]);
					for($i=0;$i<count($twittLink);$i++)
					{
						$this->Social->create();
						$twitData['Social']['user_id'] = $id;
						$twitData['Social']['created_by'] = $id;
						$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
						$this->Social->save($twitData);
					}
				}
			}

			$this->UserReference->create();
			$refData['UserReference']['first_name'] = $linkData[1];
			$refData['UserReference']['last_name'] = $linkData[2];
			$refData['UserReference']['user_id'] = $id;
			$refData['UserReference']['background_summary']  =  $linkData[6];
			$refData['UserReference']['headline']  =  $linkData[8];
                        $refData['UserReference']['client_companyname']  =  $linkData[9];
                        $refData['UserReference']['client_companydesignation']  =  $linkData[10];
                        $refData['UserReference']['client_companycontactname']  =  $linkData[1]." ".$linkData[2];
                        $refData['UserReference']['client_companyaddress']  =  $linkData[4];
                        $refData['UserReference']['client_linkedinurl']  =  $linkData[5];
			$this->User->createUrlKey($id);
			$this->UserReference->save($refData);

			if(isset($linkData[3]) && $linkData[3]!='')
			{
				$db_imgname = "user_".time().".jpeg";
				$file_name = IMAGES . MENTEES_IMAGE_PATH . DS . $id;

				$destination = IMAGES . MENTEES_IMAGE_PATH . DS . $id;
				$imagePath = $linkData[3];
				$des1 = $file_name.DS.'uploaded';
				$des2 = $file_name.DS.'small/';
				if (!is_dir($destination)) {
					mkdir($destination, 0777);
					mkdir($destination.DS.'small', 0777);
					mkdir($destination.DS.'uploaded', 0777);
				}
				if(!copy($imagePath,$file_name.DS.'uploaded'.DS.$db_imgname))
				{
					$linkData[3]="";
				}
				$cropdata = array();
				$cropdata['x'] = 0; $cropdata['y'] = 0; $cropdata['w'] = 40; $cropdata['h'] = 40; $cropdata['name'] = $db_imgname;
				$rr = $this->imageCrop($des1.DS,$cropdata,$des2,40,40);
				$rr = $this->imageCrop($des1.DS,$cropdata,$destination,200,200);
				$image_data['UserImage']['user_id'] = $id;
				$image_data['UserImage']['image_name'] = $db_imgname;
				$this->UserImage->save($image_data);
			}

			$user = $this->User->findById($id);
			if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
				$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
			else
				$this->Session->write('User.image','');

			$this->Session->write('Auth',$user);



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $linkData[4];
                           $subject = "Welcome to GUILD";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_register_linkedin");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_useremail=".urlencode($linkData[4]);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

				
			return $id;
		}
	}

	function __loginUsingLinkedin($socialId, $existEmail, $linkData){

		$saveData['User']['status'] = '1';
		$saveData['User']['activation_key'] = substr(md5(uniqid()), 0, 20);

		$id = $existEmail['User']['id'];
		$saveData['User']['id'] = $existEmail['User']['id'];
		$saveData['User']['socialId'] = $socialId;
	
		if($this->User->save($saveData,false))
		{
			if($linkData[5]!='' || $linkData[7]!='')
			{
				$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
				if(empty($socialLink))
				{
					$socialData['Social']['user_id'] = $id;
					$socialData['Social']['created_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				else
				{
					$socialData['Social']['id'] = $socialLink['Social']['id'];
					$socialData['Social']['modified_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				$this->Social->save($socialData);
					
				if($linkData[7]!='')
				{
					$twittLink = explode(',',$linkData[7]);
					$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
					for($i=0;$i<count($twittLink);$i++)
					{
						$this->Social->create();
						$twitData['Social']['user_id'] = $id;
						$twitData['Social']['created_by'] = $id;
						$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
						$this->Social->save($twitData);
					}
				}
			}

			$user = $this->User->findById($id);
			if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
				$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
			else
				$this->Session->write('User.image','');
                          $this->Session->write('Auth.User',$user['User']);
		}
	}
    function _getCityData($zipcode){
		$this->loadModel('City');
		$mentorCityArr = $this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($zipcode))));
		return $mentorCityArr['City'];
	}
 
   function login(){


		if ($this->RequestHandler->isAjax()) {

			Configure::write('debug',0);
             $tmpUser['User']['username'] = trim($this->request->data['email']);
             $tmpUser['User']['password2'] = Security::hash($this->request->data['pass'], null, true);

           $countEmail = $this->User->find('count', array('conditions' => array('User.status' =>1,'User.username' =>trim($tmpUser['User']['username']))));
           $user = $this->User->find('first', array('conditions' => array('AND' => array(array('User.username' => trim($tmpUser['User']['username'])),array('User.password2' => $tmpUser['User']['password2'])))));
           $abc = 0;
        if($this->Auth->login($tmpUser))
                     {

                       $abc = 2;
			$UserData = $this->User->read('password',$this->Auth->user('id'));

                        $this->Session->write('UserId',$user['User']['id']);
                        $this->Session->write('UserRole',$user['User']['role_id']);
                        $this->Session->write('loggeduser',1);
                        $this->Session->write('Auth',$user);
			//set image logined user
			if(isset($UserData['UserImage'][0]['image_name']) &&  count($UserData['UserImage'])>0){
				$this->Session->write('User.image',$UserData['UserImage'][0]['image_name']);
			}
			else
			{
				$this->Session->write('User.image','');
			}
        }
    
			$this->layout = '';
			$this->render(false);

			if(isset($countEmail) && $countEmail!=0)
				echo json_encode(array('value' => $user['User']['id'],'value1'=> $abc));
			else
				echo json_encode(array('value' => '0'));
			exit();


      }


     }

      function sessionlocation(){

		if ($this->RequestHandler->isAjax()) {

			Configure::write('debug',0);

                       $pageurl = trim($this->request->data['url']);
                       $this->Session->write('PageUrl',$pageurl);
                       $this->layout = '';
		       $this->render(false);
                       echo json_encode(array('value' => '1'));
                       exit();

        }


       }

 
 	function profile_edit($id= null){

                $this->layout= 'defaultnew';

		if($id == "" && $this->Session->read('Auth.User.id')=='' && ($this->Session->read('Auth.User.role_id')!='2' && $this->Session->read('Auth.User.role_id')!='3'))
		{

			$this->redirect(array('controller'=>'fronts','action'=>'index','openloginpopup:'.true));
		}
		if($id=="")
		{

			$id=$this->Auth->user('id');
		}
		else
		{


                       $newuser = $this->User->find('first',array('conditions'=>array('User.url_key'=>$id)));

			  $id=$newuser['User']['id'];

			if($id=="")
			{
				$this->redirect(array('controller'=>'fronts','action'=>'index'));
				$this->set("ERROR","No Records Exist");
			}
		}

     		//GetQ categories
		$result = $this->Topic->find('all');
		$qcategories = array();
		foreach($result as $value){
			$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
		}
		$this->set("categories",$qcategories);

				$keywords = $this->Topic->find('all',array(
						'fields' => array('DISTINCT (Topic.autocomplete_text) AS autocomplete_text'),
						'conditions'=>array('Topic.isName'=>0)
				));

				$this->set('keywords',$keywords);
				
				$key = array();
				$cn = 0;
				foreach($keywords as $value){
					$key[$cn++] = $value['Topic']['autocomplete_text'];
				}
				$this->set('allKeywords', $key);
		

				$industry_categories = $this->IndustryCategory->find('all',array('order'=>'IndustryCategory.id asc'));
		  		$this->set('industry_categories', $industry_categories);


		                $indcategories = array();
		                foreach($industry_categories as $value){
			        $indcategories[$value['IndustryCategory']['id']] = $value['IndustryCategory']['category'];
		                }
		                $this->set("indcategories",$indcategories);


              
              $this->User->Behaviors->attach('Containable');

		$this->data = $this->User->find('first',
				array(
						'contain'=>array(
								'UserReference'=>array(
										'fields'=>array('first_name','last_name','background_summary','area_of_expertise','accept_application','desire_mentee_profile','fee_first_hour','fee_regular_session','regular_session_per','created','zipcode','past_clients','guarantee','feedback_link','feeNSession','linkedin_headline','business_website','intro_link'),
								),
								'UserImage' ,
								
								'Communication',
								'Availablity',
								'Social',
								'MemberIndustryCategory',
                                                                'ShowcaseClient'
						),
						'conditions'=>array('User.id'=>$id)
				)

		);

          
          
		if($this->data['User']['status'] == 0) {
			$this->redirect(array('controller'=>'fronts','action'=>'index'));
		}


	               
		$this->loadModel('City');
		$dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($this->data['UserReference']['zipcode']))));
		$this->set("city",$dataZip['City']['city_name']);
		$this->set("state",$dataZip['City']['state']);
		$this->set("resourcepath","img/".MENTORS_RESOURCE_PATH.DS.$id.DS);

		$userTitle = $this->data['UserReference']['first_name'].' '.$this->data['UserReference']['last_name'];

		$this->set("title_for_layout",$userTitle);

    }

          function save_profileinfo(){



		if($this->Auth->user('id') != "") {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);
				$fname = ucfirst($this->request->data['fname']);
				$lname = ucfirst($this->request->data['lname']);
				$zipcode = $this->request->data['zipcode'];
                                $profileheadline= $this->request->data['profileheadline'];


				
				$mentor_data = $this->User->find('first',
						array(
							'contain'=>array(
								'UserReference'=>array(
									'fields'=>array('first_name','last_name','zipcode','linkedin_headline'),
								),
								
							),
							'conditions'=>array('User.id'=>$this->Auth->user('id'))		
						)
				
					);
				
				$mentor_data['UserReference']['first_name']			=	$fname;
				$mentor_data['UserReference']['last_name']			=	$lname;
				$mentor_data['UserReference']['zipcode']			=	$zipcode;
                                $mentor_data['UserReference']['linkedin_headline']    =   $profileheadline;

				$this->UserReference->save($mentor_data['UserReference'], false);
                $city = $this->_getCityData($zipcode);
				$user_e = $this->User->find('first',array('conditions' => array('User.id' => $this->Auth->user('id'))));
				$full_name = $user_e['UserReference']['first_name'] . " " . $user_e['UserReference']['last_name'];

		    //ElasticSearch data update start
			$client = ClientBuilder::create()->build();	
			
			$params = array(
			    'index' => 'guild_members',
			    'type' => 'guildlive_new1',
			    'id' => $this->Auth->user('id'),
			    'body' => array(
			        'doc' => array(
			        	'first_name' => $fname,
					    'last_name' => $lname,
					    'headline' => $profileheadline,
					    'extension' => $user_e['User']['extension'],
					    'url_key' => $user_e['User']['url_key'],
					    'user_image' => $user_e['UserImage'][0]['image_name'],
					    'city_name' => $city['city_name'],
                        'state' => $city['state'],
						'location' => $city['latitude'] . ", ". $city['longitude'],
						'name' => $full_name						
			        )
			    )
			);
			
			// Update doc at /my_index/my_type/my_id
			$response = $client->update($params);
			
			//ElasticSearch data update end


			//EasticEmail Integration - Begin

                          /** $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "deshmukh@guild.im";
                           $subject = "A profile has updated";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentor_profile_edit_notification");
                           $data .= "&merge_userfname=".urlencode($fname);
                           $data .= "&merge_userlname=".urlencode($lname);
                           $data .= "&merge_userkey=".urlencode(strtolower($this->Session->read('Auth.User.url_key')));
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }**/


                           //EasticEmail Integration - End


	
				$this->layout = '';
				$this->render(false);
			        echo json_encode(array('value' => 1));
				exit();
			}
		}



       }

         function save_industrycategory(){

		if($this->Auth->user('id') != "") {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);
				$UserReferenceIndustry1= $this->request->data['UserReferenceIndustry1'];
				$UserReferenceCategory1= $this->request->data['UserReferenceCategory1'];
				$UserReferenceIndustry2= $this->request->data['UserReferenceIndustry2'];
				$UserReferenceCategory2= $this->request->data['UserReferenceCategory2'];
				$UserReferenceIndustry3= $this->request->data['UserReferenceIndustry3'];
				$UserReferenceCategory3= $this->request->data['UserReferenceCategory3'];


				$memberIndustryCategory = $this->MemberIndustryCategory->find('first',array('conditions'=>array('MemberIndustryCategory.user_id'=>$this->Auth->user('id'))));
				
				if(empty($memberIndustryCategory)) {
					$memberIndustryCategory['MemberIndustryCategory']['user_id'] = $this->Auth->user('id');
					$memberIndustryCategory['MemberIndustryCategory']['category1'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['category2'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['category3'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['industry1'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['industry2'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['industry3'] = -1;
				}

				if($UserReferenceIndustry1 != -1) {
					$industry_obj1 = $this->IndustryCategory->find('first',array('conditions'=>array('IndustryCategory.category'=>$UserReferenceIndustry1)));

					$cat_list = $this->IndustryCategory->find('all',array('conditions'=>array('IndustryCategory.category'=>$UserReferenceCategory1)));
					
					foreach ($cat_list as $key => $value) {

						if($value['IndustryCategory']['parent'] == $industry_obj1['IndustryCategory']['id']) {
							$memberIndustryCategory['MemberIndustryCategory']['industry1'] = $value['IndustryCategory']['parent'];
							$memberIndustryCategory['MemberIndustryCategory']['category1'] = $value['IndustryCategory']['id'];
						}
					}
				}

				if($UserReferenceIndustry2 != -1){
					$industry_obj2 = $this->IndustryCategory->find('first',array('conditions'=>array('IndustryCategory.category'=>$UserReferenceIndustry2)));

					$cat_list = $this->IndustryCategory->find('all',array('conditions'=>array('IndustryCategory.category'=>$UserReferenceCategory2)));
					
					foreach ($cat_list as $key => $value) {

						if($value['IndustryCategory']['parent'] == $industry_obj2['IndustryCategory']['id']) {
							$memberIndustryCategory['MemberIndustryCategory']['industry2'] = $value['IndustryCategory']['parent'];
							$memberIndustryCategory['MemberIndustryCategory']['category2'] = $value['IndustryCategory']['id'];
						}
					}
				}

				if($UserReferenceIndustry3 != -1) {
					$industry_obj3 = $this->IndustryCategory->find('first',array('conditions'=>array('IndustryCategory.category'=>$UserReferenceIndustry3)));

					$cat_list = $this->IndustryCategory->find('all',array('conditions'=>array('IndustryCategory.category'=>$UserReferenceCategory3)));

					foreach ($cat_list as $key => $value) {

						if($value['IndustryCategory']['parent'] == $industry_obj3['IndustryCategory']['id']) {
							$memberIndustryCategory['MemberIndustryCategory']['industry3'] = $value['IndustryCategory']['parent'];
							$memberIndustryCategory['MemberIndustryCategory']['category3'] = $value['IndustryCategory']['id'];
						}
					}
				}

				$this->MemberIndustryCategory->save($memberIndustryCategory);
				
			$user_e = $this->User->find('first',array('conditions' => array('User.id' => $this->Auth->user('id'))));	
				
			$ind_obj1 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['industry1'])));
			$cat_obj1 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['category1'])));
			
			$ind_obj2 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['industry2'])));
			$cat_obj2 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['category2'])));
			
			$ind_obj3 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['industry3'])));
			$cat_obj3 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['category3'])));
				


				if(empty($ind_obj1) == false && empty($cat_obj1) == false) {
					
                                $industry1 = $ind_obj1['IndustryCategory']['category'];
				                $category1 = $cat_obj1['IndustryCategory']['category'];	

				}else{
                                 
                                $industry1 ='';
                                $category1 ='';
                                }   
		
				if(empty($ind_obj2) == false && empty($cat_obj2) == false) {
					
                                $industry2 = $ind_obj2['IndustryCategory']['category'];
				                $category2 = $cat_obj2['IndustryCategory']['category'];	
					

				}else{
                                 
                                $industry2 ='';
                                $category2 ='';
                                } 
		
				if(empty($ind_obj3) == false && empty($cat_obj3) == false) {
					
                                $industry3 = $ind_obj3['IndustryCategory']['category'];
				                $category3 = $cat_obj3['IndustryCategory']['category'];	
					

				}else{
                                 
                                $industry3 ='';
                                $category3 ='';
                                }				
				
				
		    //ElasticSearch data update start
			$client = ClientBuilder::create()->build();	
			
			$params = array(
			    'index' => 'guild_members',
			    'type' => 'guildlive_new1',
			    'id' => $this->Auth->user('id'),
			    'body' => array(
			        'doc' => array(

					    'industry1' => $industry1,
					    'industry2' => $industry2,
					    'industry3' => $industry3,
					    'category1' => $category1,
					    'category2' => $category2,
					    'category3' => $category3
			        )
			    )
			);
			
			// Update doc at /my_index/my_type/my_id
			$response = $client->update($params);
			
			//ElasticSearch data update end					
				
				
				//End -- Saving Industry Categories


				$this->layout = '';
				$this->render(false);
			        echo json_encode(array('value' => 1));
				exit();
			}
		}



   }


     function save_professionalsummary(){

		if($this->Auth->user('id') != "") {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);
                                $professionalsummary = $this->request->data['professionalsummary'];


				
				$mentor_data = $this->User->find('first',
						array(
							'contain'=>array(
								'UserReference'=>array(
									'fields'=>array('background_summary'),
								),
								
							),
							'conditions'=>array('User.id'=>$this->Auth->user('id'))		
						)
				
					);
				

                $mentor_data['UserReference']['background_summary'] = $professionalsummary;
				$this->UserReference->save($mentor_data['UserReference'], false);
                                           
		    //ElasticSearch data update start
			$client = ClientBuilder::create()->build();	
			
			$params = array(
			    'index' => 'guild_members',
			    'type' => 'guildlive_new1',
			    'id' => $this->Auth->user('id'),
			    'body' => array(
			        'doc' => array(

					    'background_summary' => $professionalsummary
			        )
			    )
			);
			
			// Update doc at /my_index/my_type/my_id
			$response = $client->update($params);
			
			//ElasticSearch data update end										   
	
				$this->layout = '';
				$this->render(false);
			        echo json_encode(array('value' => 1));
				exit();
			}
		}


  }



     function save_engagementovierview(){

		if($this->Auth->user('id') != "") {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);
                                $engagementovierview = $this->request->data['engagementovierview'];


				
				$mentor_data = $this->User->find('first',
						array(
							'contain'=>array(
								'UserReference'=>array(
									'fields'=>array('feeNSession'),
								),
								
							),
							'conditions'=>array('User.id'=>$this->Auth->user('id'))		
						)
				
					);
				

                                $mentor_data['UserReference']['feeNSession'] = $engagementovierview;
				$this->UserReference->save($mentor_data['UserReference'], false);
                                           
	
				$this->layout = '';
				$this->render(false);
			        echo json_encode(array('value' => 1));
				exit();
			}
		}


  }

     function save_pastclients(){

		if($this->Auth->user('id') != "") {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);
                                $pastclients = $this->request->data['pastclients'];


				
				$mentor_data = $this->User->find('first',
						array(
							'contain'=>array(
								'UserReference'=>array(
									'fields'=>array('past_clients'),
								),
								
							),
							'conditions'=>array('User.id'=>$this->Auth->user('id'))		
						)
				
					);
				

                                $mentor_data['UserReference']['past_clients'] = $pastclients;
				$this->UserReference->save($mentor_data['UserReference'], false);
                                           
	
				$this->layout = '';
				$this->render(false);
			        echo json_encode(array('value' => 1));
				exit();
			}
		}


  }

     function save_availability(){

		if($this->Auth->user('id') != "") {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);

                                 $availabititytime= $this->request->data['availablitytime'];

				$communicationcheckbox1= $this->request->data['communicationcheckbox1'];
				$communication1= $this->request->data['communication1'];
				$communicationcheckbox2= $this->request->data['communicationcheckbox2'];
				$communication2= $this->request->data['communication2'];
				$communicationcheckbox3= $this->request->data['communicationcheckbox3'];
				$communication3= $this->request->data['communication3'];

				$mentor_data = $this->User->find('first',
						array(
							'contain'=>array(

								'Communication',
								'Availablity',

								
							),
							'conditions'=>array('User.id'=>$this->Auth->user('id'))		
						)
				
					);

                                        if($availabititytime !='' && $availabititytime !='undefined'){
					$mentor_data['Availablity']['user_id'] = $this->Auth->user('id');
					$mentor_data['Availablity']['day_time'] = $availabititytime;
					$this->Availablity->save($mentor_data['Availablity'], false);
                                        }
                                           

    		      $communicationlinks = $this->Communication->find('all', array('conditions' => array('Communication.user_id' => $this->Auth->user('id'))));
    		 
    		        foreach ($communicationlinks as $links) {
    		
    			         $this->Communication->delete($links['Communication']['id']);
    		      }


                                        if($communicationcheckbox1 !='' && $communicationcheckbox1 !='undefined' && $communication1!='undefined'){

                                          $mentor_data['Communication']['user_id'] = $this->Auth->user('id');
					                      $mentor_data['Communication']['mode_type']	= $communicationcheckbox1;
					                      $mentor_data['Communication']['mode']	=	$communication1;
					                      $this->Communication->save($mentor_data['Communication'],false);
                                                              $this->Communication->id ='';
                                        }  

                                        if($communicationcheckbox2 !=''&& $communicationcheckbox2 !='undefined'  && $communication2!='undefined'){

                                           $mentor_data['Communication']['user_id'] = $this->Auth->user('id');
					                       $mentor_data['Communication']['mode_type']	=	$communicationcheckbox2;
					                       $mentor_data['Communication']['mode']	=	$communication2;
					                       $this->Communication->save($mentor_data['Communication'],false);
                                                               $this->Communication->id ='';
                                          }

                                        if($communicationcheckbox3 !=''&& $communicationcheckbox3 !='undefined' && $communication3!='undefined'){

                                           $mentor_data['Communication']['user_id'] = $this->Auth->user('id');
					                       $mentor_data['Communication']['mode_type']	=	$communicationcheckbox3;
					                       $mentor_data['Communication']['mode']	=	$communication3;
					                       $this->Communication->save($mentor_data['Communication'],false);
                                          }


			 //EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "deshmukh@guild.im";
                           $subject = "A profile has updated";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentor_profile_edit_notification");
                           $data .= "&merge_userfname=".urlencode($mentor_data['UserReference']['first_name']);
                           $data .= "&merge_userlname=".urlencode($mentor_data['UserReference']['last_name']);
                           $data .= "&merge_userkey=".urlencode(strtolower($this->Session->read('Auth.User.url_key')));
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

	
				$this->layout = '';
				$this->render(false);
			        echo json_encode(array('value' => 1));
				exit();
			}
		}

    }


     function save_sociallinks(){

		if($this->Auth->user('id') != "") {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);
                                $businesswebsite = $this->request->data['businesswebsite'];
                                $introvideo = $this->request->data['introvideo'];
                                $sociallink1 = $this->request->data['sociallink1'];
                                $sociallink2 = $this->request->data['sociallink2'];
                                $sociallink3 = $this->request->data['sociallink3'];
                                $sociallink4 = $this->request->data['sociallink4'];
                                $sociallink5 = $this->request->data['sociallink5'];
 


				
				$mentor_data = $this->User->find('first',
						array(
							'contain'=>array(
								'UserReference'=>array(
									'fields'=>array('business_website','intro_link'),
								),
								'Social',
							),
							'conditions'=>array('User.id'=>$this->Auth->user('id'))		
						)
				
					);
				

                                $mentor_data['UserReference']['business_website'] = $businesswebsite;
                                $mentor_data['UserReference']['intro_link'] = $introvideo;
     
				$this->UserReference->save($mentor_data['UserReference'], false);

    		      $sociallinks = $this->Social->find('all', array('conditions' => array('Social.user_id' => $this->Auth->user('id'))));
    		 
    		        foreach ($sociallinks as $links) {
    		
    			         $this->Social->delete($links['Social']['id']);
    		      }

                                        $icon1 = '';
                                        $icon2 = '';
                                        $icon3 = '';
                                        $icon4 = '';
                                        $icon5 = '';

                                        if($sociallink1 !='' && $sociallink1 !='undefined'){

                                          $mentor_data['Social']['user_id'] = $this->Auth->user('id');
					                      $mentor_data['Social']['social_name']	= $sociallink1;
					                      $this->Social->save($mentor_data['Social'], false);
                                          $this->Social->id ='';
                                          $icon1 = $this->getIcon($sociallink1);
                                        } 
                                        if($sociallink2 !='' && $sociallink2 !='undefined'){

                                          $mentor_data['Social']['user_id'] = $this->Auth->user('id');
					                      $mentor_data['Social']['social_name']	= $sociallink2;
					                      $this->Social->save($mentor_data['Social'], false);
                                          $this->Social->id ='';
                                          $icon2 = $this->getIcon($sociallink2);
                                        } 
                                        if($sociallink3 !='' && $sociallink3 !='undefined'){

                                          $mentor_data['Social']['user_id'] = $this->Auth->user('id');
					                      $mentor_data['Social']['social_name']	= $sociallink3;
					                      $this->Social->save($mentor_data['Social'], false);
                                          $this->Social->id ='';
                                          $icon3 = $this->getIcon($sociallink3);
                                        } 
                                        if($sociallink4 !='' && $sociallink4 !='undefined'){

                                          $mentor_data['Social']['user_id'] = $this->Auth->user('id');
					                      $mentor_data['Social']['social_name']	= $sociallink4;
					                      $this->Social->save($mentor_data['Social'], false);
                                          $this->Social->id ='';
                                          $icon4 = $this->getIcon($sociallink4);
                                        } 
                                        if($sociallink5 !='' && $sociallink5 !='undefined'){

                                          $mentor_data['Social']['user_id'] = $this->Auth->user('id');
					                      $mentor_data['Social']['social_name']	= $sociallink5;
					                      $this->Social->save($mentor_data['Social'], false);
                                          $this->Social->id ='';
                                          $icon5 = $this->getIcon($sociallink5);
                                        } 
                                           
	
				$this->layout = '';
				$this->render(false);
			       echo json_encode(array('icon1' => $icon1, 'icon2' => $icon2, 'icon3' => $icon3, 'icon4' => $icon4, 'icon5' => $icon5));
				exit();
			}
		}


  }


    function getIcon($url = null){
    	
        if(($pos =strpos($url,'linkedin'))!==false)
    	{
    		return 'media_new/svg/linkedin.svg';
    	}
    	else if(($pos =strpos($url,'twitter'))!==false)
    	{
    		return 'media_new/svg/twitter.svg';
    	}
    	else if(($pos =strpos($url,'facebook'))!==false)
    	{
    		return 'media_new/svg/facebook.svg';
    	}
    	else if(($pos =strpos($url,'google'))!==false)
    	{
    		return 'media_new/svg/googleplus.svg';
    	}
    	else if(($pos =strpos($url,'blogspot'))!==false)
    	{
    		return 'media_new/svg/blogger.svg';
    	}
    	else if(($pos =strpos($url,'wordpress'))!==false)
    	{
    		return 'media_new/svg/wordpress.svg';
    	}
    	else if(($pos =strpos($url,'youtube'))!==false)
    	{
    		return 'media_new/svg/youtube.svg';
    	}
    	else if(($pos =strpos($url,'yahoo'))!==false)
    	{
    		return 'media_new/svg/yahoo.svg';
    	}
    	else if(($pos =strpos($url,'delicious'))!==false)
    	{
    		return 'media_new/svg/delicious.svg';
    	}
    	else if(($pos =strpos($url,'digg'))!==false)
    	{
    		return 'media_new/svg/digg.svg';
    	}
    	else if(($pos =strpos($url,'orkut'))!==false)
    	{
    		return 'media_new/svg/orkut.svg';
    	}
    	else if(($pos =strpos($url,'www.ted.com'))!==false)
    	{
    		return 'media_new/svg/TED.svg';
    	}
    	else if(($pos =strpos($url,'forbes'))!==false)
    	{
    		return 'media_new/svg/forbes.svg';
    	}
    	else if(($pos =strpos($url,'hbr'))!==false)
    	{
    		return 'media_new/svg/HBR.svg';
    	}
    	else if(($pos =strpos($url,'about.me'))!==false)
    	{
    		return 'media_new/svg/aboutdotme.svg';
    	}
    	else if(($pos =strpos($url,'psychologytoday.com'))!==false)
    	{
    		return 'media_new/svg/psychologytoday.svg';
    	}
    	else if(($pos =strpos($url,'vimeo'))!==false)
    	{
    		return 'media_new/svg/youtube.svg';
    	}
     	else if(($pos =strpos($url,'www.inc.com'))!==false)
    	{
    		return 'media_new/svg/inc.svg';
    	}
    	else if(($pos =strpos($url,'soundcloud'))!==false)
    	{
    		return 'media_new/svg/SoundCloud.svg';
    	}
    	else if(($pos =strpos($url,'amazon.com'))!==false)
    	{
    		return 'media_new/svg/amazon.svg';
    	}
       else if(($pos =strpos($url,'amazon.in'))!==false)
    	{
    		return 'media_new/svg/amazon.svg';
    	}
        else if(($pos =strpos($url,'amzn.com'))!==false)
    	{
    		return 'media_new/svg/amazon.svg';
    	}
    	else if(($pos =strpos($url,'entrepreneur.com'))!==false)
    	{
    		return 'media_new/svg/twitter.svg';
    	}
    	else if(($pos =strpos($url,'time.com'))!==false)
    	{
    		return 'media_new/svg/time.svg';
    	}
    	else if(($pos =strpos($url,'entrepreneur.com'))!==false)
    	{
    		return 'media_new/svg/TrainingMag.svg';
    	}		
    	else if(($pos =strpos($url,'cbsnews.com'))!==false)
    	{
    		return 'media_new/svg/cbs.svg';
    	}
    	else if(($pos =strpos($url,'corporatecomplianceinsights.com'))!==false)
    	{
    		return 'media_new/svg/cci.svg';
    	}
    	else if(($pos =strpos($url,'slideshare.net'))!==false)
    	{
    		return 'media_new/svg/slideshare.svg';
    	}
    	else if(($pos =strpos($url,'ceo.com'))!==false)
    	{
    		return 'media_new/svg/CEOdotCOM.svg';
    	}
    	else if(($pos =strpos($url,'huffingtonpost'))!==false)
    	{
    		return 'media_new/svg/huffington.svg';
    	}
    	else if(($pos =strpos($url,'medium.com'))!==false)
    	{
    		return 'media_new/svg/medium.svg';
    	}
    	else if(($pos =strpos($url,'ere.net'))!==false)
    	{
    		return 'media_new/svg/ere.svg';
    	}
    	else if(($pos =strpos($url,'aom.org'))!==false)
    	{
    		return 'media_new/svg/aom.svg';
    	} 
    	else if(($pos =strpos($url,'examiner.com'))!==false)
    	{
    		return 'media_new/svg/examiner.svg';
    	}
    	else if(($pos =strpos($url,'50topcoaches.com'))!==false)
    	{
    		return 'media_new/svg/icon50.svg';
    	}
    	else if(($pos =strpos($url,'leadingauthorities.com'))!==false)
    	{
    		return 'media_new/svg/LeadingAuthorities.svg';
    	}
    	else if(($pos =strpos($url,'udemy.com'))!==false)
    	{
    		return 'media_new/svg/udemy.svg';
    	}
    	else if(($pos =strpos($url,'nxtbook.com'))!==false)
    	{
    		return 'media_new/svg/managementtoday.svg';
    	}
    	else if(($pos =strpos($url,'astd.org'))!==false)
    	{
    		return 'media_new/svg/astd.svg';
    	}
    	else if(($pos =strpos($url,'orgsurvival.com'))!==false)
    	{
    		return 'media_new/svg/OSP.svg';
    	}
    	else if(($pos =strpos($url,'ftpress'))!==false)
    	{
    		return 'media_new/svg/FTPress.svg';
    	}
     	else if(($pos =strpos($url,'executivetravelmagazine.com'))!==false)
    	{
    		return 'media_new/svg/facebook.svg';
    	}
    	else if(($pos =strpos($url,'cfo.com'))!==false)
    	{
    		return 'media_new/svg/cfo.svg';
    	}
    	else if(($pos =strpos($url,'scribd.com'))!==false)
    	{
    		return 'media_new/svg/scribd.svg';
    	}
       else if(($pos =strpos($url,'strategy-business.com'))!==false)
    	{
    		return 'media_new/svg/strategy-business.svg';
    	}
       else if(($pos =strpos($url,'hr.com'))!==false)
    	{
    		return 'media_new/svg/hr.svg';
    	}
       else if(($pos =strpos($url,'paper.li'))!==false)
    	{
    		return 'media_new/svg/paperli.svg';
    	}
       else if(($pos =strpos($url,'paypal'))!==false)
    	{
    		return 'media_new/svg/paypal.svg';
    	}
       else if(($pos =strpos($url,'skype'))!==false)
    	{
    		return 'media_new/svg/skype.svg';
    	}
       else if(($pos =strpos($url,'wikipedia'))!==false)
    	{
    		return 'media_new/svg/wikipedia.svg';
    	}		
    	else
    	{
    		return 'media_new/svg/blognew.svg';
    	}
    }

     function save_engfee(){

		if($this->Auth->user('id') != "") {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);
                                $engfee = $this->request->data['engfee'];
                                $engper = $this->request->data['engper'];


				
				$mentor_data = $this->User->find('first',
						array(
							'contain'=>array(
								'UserReference'=>array(
									'fields'=>array('fee_regular_session','regular_session_per'),
								),
								
							),
							'conditions'=>array('User.id'=>$this->Auth->user('id'))		
						)
				
					);
				

                                $mentor_data['UserReference']['fee_regular_session'] = $engfee;
                                $mentor_data['UserReference']['regular_session_per'] = $engper;
     
				$this->UserReference->save($mentor_data['UserReference'], false);
                                           
	
				$this->layout = '';
				$this->render(false);
			        echo json_encode(array('value' => 1));
				exit();
			}
		}


  }



     function save_topics(){


		if($this->Auth->user('id') != "") {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);
                                $topics = $this->request->data['topics'];


				$mentor_data = $this->User->find('first',
						array(
							'contain'=>array(
								'UserReference'=>array(
									'fields'=>array('fee_regular_session','regular_session_per'),
								),
								
							),
							'conditions'=>array('User.id'=>$this->Auth->user('id'))		
						)
				
					);

				
				$memberIndustryCategory = $this->MemberIndustryCategory->find('first',array('conditions'=>array('MemberIndustryCategory.user_id'=>$this->Auth->user('id'))));
				


				//Saving Expertise for Search Autocomplete
				$topic_arr = array(-1,-1,-1,-1,-1);

				$count = 0;
					
				for($i=0;$i<5;$i++){
				
					if(isset($topics[$i]) && $topics[$i] != '' && $topics[$i] != -1)
						$allExp[$count++] = ucfirst(trim($topics[$i]));
				}
					
				$uniqueAry = array_unique($allExp);
				
				$count = 0;
				
				foreach($uniqueAry as $exp){
						
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$exp)));
					
					$topic_arr[$count] = $match['Topic']['id'];
                                        if($match['Topic']['topic_subset'] == 0)
                                        {
                                        $this->Topic->updateAll(array('Topic.topic_subset' => 1),array('Topic.id ' => $match['Topic']['id']));

                                        }
					
					if(empty($match)) {
						$this->Topic->create( );
						$this->Topic->save(array('autocomplete_text'=>$exp));

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "deshmukh@guild.im";
                           $subject = "New Topic has just added in a profile";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("new_topic_addition");
                           $data .= "&merge_userfname=".urlencode($mentor_data['UserReference']['first_name']);
                           $data .= "&merge_userlname=".urlencode($mentor_data['UserReference']['last_name']);
                           $data .= "&merge_newtopic=".urlencode($exp);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End




						$topic_arr[$count] = $this->Topic->id;
						unset($exp);
					}
					$count++;
				}


				$memberIndustryCategory['MemberIndustryCategory']['topic1'] = $topic_arr[0];
				$memberIndustryCategory['MemberIndustryCategory']['topic2'] = $topic_arr[1];
				$memberIndustryCategory['MemberIndustryCategory']['topic3'] = $topic_arr[2];
				$memberIndustryCategory['MemberIndustryCategory']['topic4'] = $topic_arr[3];
				$memberIndustryCategory['MemberIndustryCategory']['topic5'] = $topic_arr[4];
				
                                $this->MemberIndustryCategory->save($memberIndustryCategory);

				//End Saving Autocomplete text
				
				//Topics Begin
				
				$topic1 = $topic_arr[0];
				$topic2 = $topic_arr[1];
				$topic3 = $topic_arr[2];
				$topic4 = $topic_arr[3];
				$topic5 = $topic_arr[4];
				$topic_synonym = "";

				if($topic1 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic1)));
					$topic_first = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_first ='';
                                }

				if($topic2 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic2)));
					$topic_second = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.','.$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_second  ='';
                                }

				if($topic3 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic3)));
					$topic_third = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.','.$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_third ='';
                                }

				if($topic4 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic4)));
					$topic_fourth = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.','.$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_fourth ='';
                                }

				if($topic5 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic5)));
					$topic_fifth = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.','.$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_fifth ='';
                                }
				//Topics End
				
				
		    //ElasticSearch data update start
			$client = ClientBuilder::create()->build();	
			
			$params = array(
			    'index' => 'guild_members',
			    'type' => 'guildlive_new1',
			    'id' => $this->Auth->user('id'),
			    'body' => array(
			        'doc' => array(

					    'topic1' => $topic_first,
					    'topic2' => $topic_second,
					    'topic3' => $topic_third,
					    'topic4' => $topic_fourth,
					    'topic5' => $topic_fifth,
                                            'topic_synonyms' => $topic_synonym
			        )
			    )
			);
			
			// Update doc at /my_index/my_type/my_id
			$response = $client->update($params);
			
			//ElasticSearch data update end					
                                           
	
				$this->layout = '';
				$this->render(false);
			        echo json_encode(array('value' => 1));
				exit();
			}
		}


  }


        function stripecheckout(){
    	  if ($this->RequestHandler->isAjax()) {
    		
    			
    		$projectId = $this->request->data['projectId'];
                $amount = $this->request->data['price'];
                $token = $this->request->data['token'];
                $email = $this->request->data['email'];

                Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");
                
 

			try {
				$charge = Stripe_Charge::create(array(
						"amount" => $amount, // amount in cents, again
						"currency" => "usd",
						"card" => $token,
						"receipt_email"=> $email,
						"description" => "Project Deposit")
                        
				);


		    	$project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectId)));

		    	
		    	$project['Project']['status'] = 5; //Project advanced payment done by client
		    	 
		    	$this->Project->save($project);
 
		echo json_encode($projectId);
    		$this->render(false);
    		exit();
                 
                } catch(Stripe_CardError $e) {
              			//pr("There is some error");
				//pr($e);
                                //die;

               }


            }


         }


	function client_auth_popup(){

	
		$this->layout = 'ajax';
		$this->Session->write('popup', 1);

                if($this->Session->check('menteePopup')){
                $this->Session->delete('menteePopup');
                }

                if($this->Session->check('projectId')){
                $this->Session->delete('projectId');
                } 
                if($this->Session->check('questionId')){
                $this->Session->delete('questionId');
                } 

                if($this->Session->check('duserid')){
                $this->Session->delete('duserid');
                } 
                if($this->Session->check('login_industry')){
                $this->Session->delete('login_industry');
                } 
                if($this->Session->check('mentor')){
                $this->Session->delete('mentor');
                } 
                if($this->Session->check('mentorshipId')){
                $this->Session->delete('mentorshipId');
                } 
                if($this->Session->check('consultantPopup')){
                $this->Session->delete('consultantPopup');
                }
                if($this->Session->check('directoryclaim')){
                $this->Session->delete('directoryclaim');
                } 


                $this->Session->write('menteePopup', 1);
                $this->Session->write('planID', 1);
		if(!empty($this->data)){ //popup submit

			
				$this->__subscribenewsletter($this->data['User']['username']);
	
				$plan_id = "1";
				

				
				$user = $this->User->find('first',array('conditions'=>array('User.username'=>$this->data['User']['username'])));
				
				if(!empty($user)) { // Already a user. 
					
					$this->Session->write('Auth', $user);
					
					$this->redirect(array('controller' => 'clients', 'action' => 'my_account'));
					
				} else { // Register as a new user
					
					$activationKey = substr(md5(uniqid()), 0, 20);
   
					$this->request->data['User']['role_id'] = 3;
					$this->request->data['User']['status'] = 1;
					$this->request->data['User']['password'] = $this->data['User']['password2'];//Security::hash($this->data['User']['password2'], null, true);
                                        $this->request->data['User']['password2'] = Security::hash($this->data['User']['password2'], null, true);
					$this->request->data['User']['plan_type'] = "Basic";
					$this->request->data['User']['isPlanAdmin'] = 0;

					$this->request->data['User']['activation_key'] = $activationKey;
					$this->request->data['User']['access_specifier'] = 'publish';
				        $this->request->data['User']['is_approved'] = 1;
				        $this->request->data['User']['email_send'] = '1';
					
						
					$this->User->saveAll($this->request->data);

					if(!isset($this->data['User']['id'])){
						$id = $this->User->getLastInsertID();
					}else{
						$id = $this->data['User']['id'];
					}

                                        $refData = $this->UserReference->find('first',array('conditions'=>array('UserReference.user_id'=>$id)));

                                        $refData['UserReference']['client_companycontactname']  =  $this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'];
                                        $refData['UserReference']['client_companyaddress']  =  $this->data['User']['username'];
				        $this->User->createUrlKey($id);
				        $this->UserReference->save($refData);
					

					


						


                                          $data = $this->User->read(null,$id);
					
					       $this->Session->write('Auth', $data);						
				


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $this->data['User']['username'];
                           $subject = "Welcome to GUILD";
                           $link = SITE_URL.'clients/my_account/'.$id;

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_register");
                           $data .= "&merge_userfname=".urlencode($this->data['UserReference']['first_name']);
                           $data .= "&merge_useremail=".urlencode($this->data['User']['username']);
                           $data .= "&merge_profilelink=".urlencode($link);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End



                                    $this->redirect(array('controller' => 'clients', 'action' => 'my_account'));
				}
			}

	}

  
}
?>