<?php
	/**
	* NewsLetters Controller
	*
	* PHP version 5
	*
	*/
	class FeedbackController extends AppController{
		/**
		* Controller name
		* @var string
		* @access public
		*/
		var $name = 'FeedBack';
		/**
		* Models used by the Controller
		* @var array
		* @access public
		*/	
		var $uses = array('User','Mentorship','Invoice','City','FeedBack','UserReference');
		var $helpers = array('General', 'Form', 'Ajax', 'javascript', 'Paginator','Ical');
		var $components = array("Session", "Email", "Auth", "RequestHandler" ,"Upload");
        
		function beforeFilter(){			
			parent::beforeFilter();	 
			$this->Auth->allow('feedbackpublic','feedbackall');
		}
		
        function feedbackpublic($id=null)
        {
            $this->layout = 'ajax';
            $userId = $id;
            $this->Feedback->bindModel(
                        array(
                        'belongsTo' => array(
                            'Mentee' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('client_id=Mentee.user_id')),
                            'Mentor' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('member_id=Mentor.user_id'))
                            )));
            $fields = array('Feedback.created','Feedback.description','Feedback.client_id','Feedback.member_id','Mentee.first_name','Mentee.last_name','Mentor.first_name','Mentor.last_name');
            $data = $this->Feedback->find('all',array('conditions' => array('Feedback.member_id'=>$userId,'Feedback.type'=>'1'),'fields'=>$fields));
            $mentorData = $this->UserReference->find('first',array('conditions'=>array('UserReference.user_id'=>$userId)));
            $this->set('data',$data);
            $this->set('mentorData',$mentorData);
            $this->set('id',$userId);
        }

        function feedbackall($id=null,$type=null)
        {
            $userId = $this->Session->read('Auth.User.id');
            if($userId!=$id)
            {
                $type = '1';
            }
            if(!empty($this->data))
            {
                for($i=1;$i<=$this->data['FeedBack']['feedbackCount'];$i++)
                {
                   $feedid = explode("feedId_",$this->data['FeedBack']['feedId'.$i]);
                   $this->Feedback->id = $feedid[1];
                   $this->Feedback->saveField('type', $this->data['FeedBack']['inv_mode'.$feedid[1]]);  
                }
                $this->Session->setFlash(__("Feedback updated", true), 'default', array('class' => 'success'));
                $this->redirect(array('controller'=>'users','action'=>'my_account'));
            }
            $this->Feedback->bindModel(
                    array(
                    'belongsTo' => array(
                        'Mentee' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('client_id=Mentee.user_id')),
                        )),false);
            $fields = array('Feedback.*','Mentee.first_name','Mentee.last_name'/*,'Mentor.first_name','Mentor.last_name'*/);
            if(isset($type) && ($type=='0' || $type=='1')){$conditions['Feedback.type'] = $type; }
           
            $conditions['Feedback.member_id'] = $id;
            $mentorData = $this->UserReference->find('first',array('conditions'=>array('UserReference.user_id'=>$id)));
            $this->paginate['FeedBack'] = array('conditions'=>$conditions,'fields' => $fields,'order'=>'Feedback.modified DESC','limit'=>'10');
            $data=$this->paginate('FeedBack');
            $this->set(compact('data'));
            $this->set('mentorData',$mentorData);
            $this->set('id',$id);
             
             /* for paging data*/ 
            /* $this->Feedback->bindModel(array('belongsTo' => array('Mentorship')),false);
            $this->paginate['FeedBack'] = array('conditions'=>$conditions,'fields' => array('Feedback.*','Mentorship.id'),'order'=>'Feedback.modified DESC','limit'=>'1');
            $feedbacks=$this->paginate('FeedBack'); 
            pr($feedbacks);exit;
            $this->set(compact('feedbacks'));*/
            
            //$this->set('data',$data);
        }
        function typechange()
        {
            if(isset($_REQUEST['id']) && isset($_REQUEST['type']))
            {
                $type = $_REQUEST['type'];
                $this->Feedback->id = $_REQUEST['id'];
                $this->Feedback->saveField('type', $type);
                echo json_encode(array('value' => '1'));
                exit(); 
            }
        }
        
        function modechange()
        {
            if(!empty($this->data))
            {
                for($i=1;$i<=$this->data['FeedBack']['feedbackCount'];$i++)
                {
                   $feedid = explode("feedId_",$this->data['FeedBack']['feedId'.$i]);
                   $this->Feedback->id = $feedid[1];
                   $this->Feedback->saveField('type', $this->data['FeedBack']['inv_mode'.$feedid[1]]);  
                }
                $this->Session->setFlash(__("Feedback updated", true), 'default', array('class' => 'success'));
                $this->redirect(array('controller'=>'users','action'=>'my_account'));
            }
        }
        
	}
	?>