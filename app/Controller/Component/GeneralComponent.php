<?php
/**
 * General Component
 *
 * PHP version 5
 *
 * @category Component 
 */
class GeneralComponent extends Component{	

	var $uses 		= array('User','UserReference');
	
	function createSlug($string){		
		return low(Inflector::slug($string, '-'));		
	}
	/* 
	* convert into Mondays and Fridays||5 to 8
	*/
	function convertAvailablityToDB($availArr){	
		
		$availStr = '';
		if(!empty($availArr['day'])){
			$dayStr = '';
			$key_index = count($availArr['day']) - 1 ;
			foreach($availArr['day'] as $key=>$value ){
				if($key_index == $key){
					$dayStr .= $value;
				}else{
					$dayStr .= $value.' and ';
				}
			}
			$availStr = $dayStr.'||'.$availArr['hour_from'].':'.$availArr['minute_from'].' to '.$availArr['hour_to'].':'.$availArr['minute_to'];
		}
		return $availStr;
	}	
	function convertAvailablityToform($availStr){	
		$availArr =Array(
		    'hour_from' => 01,
		    'minute_from' => 00,
		    'hour_to' => 01,
		    'minute_to' => 00,
		    'day' => Array()

		);
		if(isset($availStr['day_time'])){
			//pr($availStr);			
			$availStrToArr = explode('||',$availStr['day_time']);
			//for day str to day array
			if(isset($availStrToArr[0])){
				$availStrToDay = explode('and',$availStrToArr[0]);
				foreach($availStrToDay as $key=>$value){
					$trimValue = trim($value);
					$availArr['day'][$trimValue] = $trimValue;
				}
				
			}
			if(isset($availStrToArr[1])){
				$availStrTotime = explode('to',$availStrToArr[1]);
				//////from time
				if(isset($availStrTotime[0])){
					$fromTimeArr = explode(':',$availStrTotime[0]);
					if(isset($fromTimeArr[0])){
						$availArr['hour_from'] =trim($fromTimeArr[0]);
					}
					if(isset($fromTimeArr[1])){
						$availArr['minute_from'] =trim($fromTimeArr[1]);
					}					
				}
				//////to time
				if(isset($availStrTotime[1])){
					$toTimeArr = explode(':',$availStrTotime[1]);
					if(isset($toTimeArr[0])){
						$availArr['hour_to'] =trim($toTimeArr[0]);
					}
					if(isset($toTimeArr[1])){
						$availArr['minute_to'] =trim($toTimeArr[1]);
					}					
				}				
				
				
			}
			
		}
		return $availArr;

	}
	/* add new at 27-3-12  */
	function myclean($data = null){
		if($data){
			foreach($data as $key=>$value){
				$data[$key] = trim($value);
			
			}
		}
		return $data;
	
	}
	/* To delete tree structure of given folder or directory */
	function deleteDirectory($dirname) {
		chmod($dirname,0777);
		if (is_dir($dirname)){
			
			$dir_handle = opendir($dirname);
			chmod($dir_handle,0777);
		}
		if (!$dir_handle){
			return false;
		}
		while($file = readdir($dir_handle)) {
			
			if ($file != "." && $file != ".." && $file != "Thumbs.db") {
				if (!is_dir($dirname."/".$file)){
					chmod($dirname."/".$file,0777);
					@unlink($dirname."/".$file);
				}else{
					chmod($dirname."/".$file,0777);
					$this->deleteDirectory($dirname.'/'.$file);
				}
			}
		}
		if(file_exists($dirname.'/Thumbs.db')){
			unlink($dirname.'/Thumbs.db');
		} 		
		closedir($dir_handle);
		rmdir($dirname);
		return true;
	}	
	
	function getPaypalInfo($mentor_id){
		$paypal_info = ClassRegistry::init('UserReference')->find('first',array('conditions' => array('UserReference.user_id' => $mentor_id),'fields' => array('User.mentor_type','UserReference.paypal_account')));
		return $paypal_info;
	}
    
    function randamPassword()
    {
        $string = array();
        $string['ALPHHA'] = "ABCDEFHIJKLMNOPQRSTUVXYZ";
        $string['alpha']  = "abcdefghijklmlonpqrstuvwxyz";
        $string['NUM']    = "0123456789";
        $string['SPEC']   = "#$*";
        $string = str_shuffle($string['ALPHHA'].$string['SPEC'].$string['alpha'].$string['NUM']);
        return substr($string,0,6);
    }
    
    function timeformatchange1($data)
    {
    	$timedata = explode(':',$data);
    	if($timedata[0]=='12' || $timedata[0]=='11')
    	{
    		if($timedata[2]=='AM')
    		{
    			$finaltimeFirst = str_pad($timedata[0],2,0,STR_PAD_LEFT).":".str_pad($timedata[1],2,0,STR_PAD_LEFT)." ".$timedata[2];
    			if($timedata[0]=='11')
    			{
    				$finaltimeSecond = "12:".str_pad($timedata[1],2,0,STR_PAD_LEFT);
    				$sendTime = $finaltimeFirst." - ".$finaltimeSecond." PM";
    			}
    			else
    			{
    				$finaltimeSecond = "01:".str_pad($timedata[1],2,0,STR_PAD_LEFT);
    				$sendTime = $finaltimeFirst." - ".$finaltimeSecond." AM";
    			}
    		}
    		else
    		{
    			$finaltimeFirst = str_pad($timedata[0],2,0,STR_PAD_LEFT).":".str_pad($timedata[1],2,0,STR_PAD_LEFT)." ".$timedata[2];
    			if($timedata[0]=='11')
    			{
    				$finaltimeSecond = "12:".str_pad($timedata[1],2,0,STR_PAD_LEFT);
    				$sendTime = $finaltimeFirst." - ".$finaltimeSecond." AM";
    			}
    			else
    			{
    				$finaltimeSecond = "01:".str_pad($timedata[1],2,0,STR_PAD_LEFT);
    				$sendTime = $finaltimeFirst." - ".$finaltimeSecond." PM";
    			}
    		}
    	}
    	else
    	{
    		$finaltimeFirst = str_pad($timedata[0],2,0,STR_PAD_LEFT).":".str_pad($timedata[1],2,0,STR_PAD_LEFT)." ".$timedata[2];
    		$finaltimeSecond = str_pad(($timedata[0]+1),2,0,STR_PAD_LEFT).":".str_pad($timedata[1],2,0,STR_PAD_LEFT)." ".$timedata[2];
    		$sendTime = $finaltimeFirst." - ".$finaltimeSecond;
    	}
    	return $sendTime;
    }
    
    function timeformatchange($data, $duration_hours, $duration_minutes)
    {
    	$timedata = explode(':',$data);
    	
    	$hours = $timedata[0] + $duration_hours;
    	$minutes = $timedata[1] + $duration_minutes;
    	if($minutes > 59){
    		$minutes = $minutes%60;
    		$hours++;
    	}
    	if($hours > 12) {
    		$hours = $hours%12;
    	}
    	
    	$finaltimeFirst = str_pad($timedata[0],2,0,STR_PAD_LEFT).":".str_pad($timedata[1],2,0,STR_PAD_LEFT)." ".$timedata[2];
    	$finaltimeSecond = str_pad($hours,2,0,STR_PAD_LEFT).":".str_pad($minutes,2,0,STR_PAD_LEFT);
    	
    	if(($hours < $timedata[0]) || ($hours == 12 && $timedata[0] < 12)) {  //Change from AM to PM and vice versa
    		
    		if($timedata[2]=='AM') { //Second time will be PM
    			
    			$sendTime = $finaltimeFirst." - ".$finaltimeSecond." PM";
    		} else {//Second time will be AM
    			
    			$sendTime = $finaltimeFirst." - ".$finaltimeSecond." AM";
    		}
    	} else {
    		$sendTime = $finaltimeFirst." - ".$finaltimeSecond.$timedata[2];
    	}
    	return $sendTime;
    }
    
    function getTimeValue($data)
    {
    	$timedata = explode(':',$data);
    	 
    	$hours = $timedata[0];
    	$minutes = $timedata[1];
    	
    	if($timedata[2]=='PM') {
    		
    		$hours = $hours + 12;
    	}
    	
    	$sendTime = (($hours * 60) + $minutes) * 60;
    	
    	return $sendTime;
    }
    
    function addhttp($url)
    {
    	if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
    		$url = "http://" . $url;
    	}
    	return $url;
    }
    
    function getTimeZoneById($id)
    {
    	$timezone = ClassRegistry::init('Timezone')->findById($id);
    	if(empty($timezone))
    		return 0;
    	else
    		return $timezone['Timezone']['name'];
    }
    
}

?>