<?php
	App::import('Vendor', 'fileuploader', array('file' => 'fileuploader' . DS . 'file_uloader.php'));
	App::import('Vendor', 'fileuploader', array('file' => 'fileuploader' . DS . 'upload_resize.php'));	
	class FileUploaderComponent extends Object
	{

		function profile_upload($id = null,$role=null) {	
			if(!$id){
				return false;
			}
			$siteFolder  = dirname(dirname($_SERVER['SCRIPT_FILENAME']));
			if($role!='')
			{
				if($role=='2')
					$image_forder_url = WWW_ROOT.'img/members';
				if($role=='3')
					$image_forder_url = WWW_ROOT.'img/clients';			
			}
			else
				$image_forder_url = WWW_ROOT.'img/members';
			
			if(!is_dir($image_forder_url)) mkdir($image_forder_url,0777,true);
			if(!is_dir($image_forder_url.'/profile_images')) mkdir($image_forder_url.'/profile_images',0777,true);
			
			
			if(!is_dir($image_forder_url.'/profile_images/'.$id)) mkdir($image_forder_url.'/profile_images/'.$id,0777,true);
			if(!is_dir($image_forder_url.'/profile_images/'.$id.'/small')) mkdir($image_forder_url.'/profile_images/'.$id.'/small',0777,true);
			if(!is_dir($image_forder_url.'/profile_images/'.$id.'/uploaded')) mkdir($image_forder_url.'/profile_images/'.$id.'/uploaded',0777,true);
			
			/*chmod($image_forder_url.'/profile_images', 0777);
			chmod($image_forder_url.'/profile_images/'.$id, 0777);
			chmod($image_forder_url.'/profile_images/'.$id.'/uploaded', 0777);
			chmod($image_forder_url.'/profile_images/'.$id.'/uploaded/small', 0777);*/
			
			define('IMAGE_LOADED_PATH',$image_forder_url.'/profile_images/'.$id.'/uploaded/');
			define('IMAGE_THUMB_PATH',$image_forder_url.'/profile_images/'.$id.'/small/');
			define('IMAGE_THUMB150_PATH',$image_forder_url.'/profile_images/'.$id.'/');
			
			// list of valid extensions, ex. array("jpeg", "xml", "bmp")
			$allowedExtensions = array('jpg', 'jpeg','JPG','JPEG','PNG','png','gif','GIF');
			// max file size in bytes
			$sizeLimit = 10 * 1024 * 1024;
				
			$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
			$result = $uploader->handleUpload(IMAGE_LOADED_PATH);
			return $result;
						
		}


		function logo_upload($id = null,$role=null) {	
			if(!$id){
				return false;
			}

			$role = '3';

			$siteFolder  = dirname(dirname($_SERVER['SCRIPT_FILENAME']));

				$image_forder_url = WWW_ROOT.'img/clients';
			
			if(!is_dir($image_forder_url)) mkdir($image_forder_url,0777,true);
			if(!is_dir($image_forder_url.'/logo')) mkdir($image_forder_url.'/logo/',0777,true);
			if(!is_dir($image_forder_url.'/logo/'.$id.'/uploaded')) mkdir($image_forder_url.'/logo/'.$id.'/uploaded',0777,true);
			
			
			if(!is_dir($image_forder_url.'/logo/'.$id)) mkdir($image_forder_url.'/logo/'.$id,0777,true);
			if(!is_dir($image_forder_url.'/logo/'.$id.'/small')) mkdir($image_forder_url.'/logo/'.$id.'/small',0777,true);
                        if(!is_dir($image_forder_url.'/logo/'.$id.'/uploaded')) mkdir($image_forder_url.'/logo/'.$id.'/uploaded',0777,true);

			
			define('IMAGE_LOADED_PATH',$image_forder_url.'/logo/'.$id.'/uploaded/');
			define('IMAGE_THUMB_PATH',$image_forder_url.'/logo/'.$id.'/small/');
			define('IMAGE_THUMB150_PATH',$image_forder_url.'/logo/'.$id.'/');
			
			// list of valid extensions, ex. array("jpeg", "xml", "bmp")
			$allowedExtensions = array('jpg', 'jpeg','JPG','JPEG','PNG','png','gif','GIF');
			// max file size in bytes
			$sizeLimit = 10 * 1024 * 1024;
				
			$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
			$result = $uploader->handleUpload(IMAGE_LOADED_PATH);
			return $result;
						
		}
	}

		