<?php
/**
 * Users Controller
 *
 * PHP version 5
 *
 * @category Controller
 */
//require('../webroot/mandrill/Mandrill.php');

class InsightController extends AppController{
	/**
	* Controller name
	*
	* @var string
	* @access public
	*/
	var $name = 'Insight';
	var $uses = array('User', 'UserReference', 'Insight', 'InsightCategory', 'QnaAnswer','QnaQuestionCategory','Social',
				'Topic','MemberIndustryCategory','IndustryCategory','QnaQuestion');
	/**
	 * Models used by the Controller
	 *
	 * @var array
	 * @access public
	*/	 
	function beforeFilter(){
		parent::beforeFilter();	 
		$this->Auth->allow('insight','edit','preview', 'thanks_for_sharing', 'add_insight','index','save_feed','activity_feed','upvote','share_insight');
	}
	
	function edit(){
                 $this->layout= 'defaultnew';
		if(empty($this->data) == false) {



    	$feed_url = trim($this->data['Insight']['title']);
    	if (!empty($feed_url)) {
    
    		$url_end = 'https://api.embed.ly/1/extract?key=5798e7e0bf594bfdad4eed21b7cb3314&url='.$feed_url;
    
    		$curlOptions = array (
    				CURLOPT_URL => $url_end,
    				CURLOPT_VERBOSE => 1,
    				CURLOPT_SSL_VERIFYPEER => 0,
    				CURLOPT_SSL_VERIFYHOST => 0,
    				CURLOPT_RETURNTRANSFER => 1,
    		);
    
    		$ch = curl_init();
    		curl_setopt_array($ch,$curlOptions);
    
    		$response = curl_exec($ch);
             
    		if (curl_errno($ch)) {
    			echo(curl_error($ch));
    			$this -> _errors = curl_error($ch);
    			curl_close($ch);
    			return false;
    
    		} else  {
    			curl_close($ch);
    			$response_arr = json_decode($response,true);
    			$title = $response_arr['title'];
    			 
                        $favicon_url = $response_arr['favicon_url'];
                     
                  } 
    		$embed_end = 'https://api.embed.ly/1/oembed?key=5798e7e0bf594bfdad4eed21b7cb3314&url='.$feed_url;
    
    		$curlOptions = array (
    				CURLOPT_URL => $embed_end,
    				CURLOPT_VERBOSE => 1,
    				CURLOPT_SSL_VERIFYPEER => 0,
    				CURLOPT_SSL_VERIFYHOST => 0,
    				CURLOPT_RETURNTRANSFER => 1,
    		);
    
    		$chs = curl_init();
    		curl_setopt_array($chs,$curlOptions);
    
    		$responses = curl_exec($chs);
             
    		if (curl_errno($chs)) {
    			echo(curl_error($chs));
    			$this -> _errors = curl_error($chs);
    			curl_close($chs);
    			return false;
    
    		} else  {
    			curl_close($chs);
    			$responses_arr = json_decode($responses,true);
    			$summary_image = $responses_arr['thumbnail_url'];
    			 
                   }

                    if($summary_image =='')
                    {
                     $user = $this->User->findById($this->Auth->user('id'));
                     $summary_image = SITE_URL."img/".MENTORS_IMAGE_PATH.DS.$user['User']['id'].DS.$user['UserImage'][0]['image_name'];
                    }

    			
    		
    	}



			$this->set('Insight_title', $title);
			$this->set('Insight_insight', '');
                        $this->set('Insight_image', $summary_image);
                        $this->set('Insight_posturl', $feed_url);
                        $this->set('Insight_favicon', $favicon_url);
                        $this->set('Summary', $summary_final);
			$this->Session->write('Insight_title', $title);
			$this->Session->write('Insight_insight', '');
                        $this->Session->write('Insight_image', $summary_image);
                        $this->Session->write('Insight_posturl', $feed_url);
                        $this->Session->write('Insight_favicon', $favicon_url);
                        $this->Session->write('Summary', $summary_final);
		}else if($this->Session->read('Insight_title') != '' && $this->Session->read('Insight_insight') != '') {
			
			$this->set('Insight_title', $this->Session->read('Insight_title'));
			$this->set('Insight_insight', $this->Session->read('Insight_insight'));
			$this->set('Insight_id', $this->Session->read('Insight_id'));
                        $this->set('Insight_image', $this->Session->read('Insight_image'));
                        $this->set('Insight_posturl', $this->Session->read('Insight_posturl'));
                        $this->set('Insight_favicon', $this->Session->read('Insight_favicon'));
                        $this->Session->write('Insight_image', $this->Session->read('Insight_image'));
                        $this->Session->write('Insight_posturl',$this->Session->read('Insight_posturl'));
                        $this->Session->write('Insight_favicon', $this->Session->read('Insight_favicon'));
		}
	}
	
	function preview(){
	         $this->layout= 'defaultnew';

		//GetQ categories
		$keywords = $this->Topic->find('all',array(
				'fields' => array('DISTINCT (Topic.autocomplete_text) AS autocomplete_text'),
				'conditions'=>array('Topic.isName'=>0)
		));

		$this->set('keywords',$keywords);
		
		$key = array();
		$cn = 0;
		foreach($keywords as $value){
			$key[$cn++] = $value['Topic']['autocomplete_text'];
		}
		$this->set('allKeywords', $key);

			$this->User->Behaviors->attach('Containable');
			
			$userData = $this->User->find('first',
					array(
							'contain'=>array(
									'UserReference'=>array(
											'fields'=>array('first_name','last_name','zipcode','linkedin_headline'),
									),
									'UserImage' ,
                                                                        'Social',
							),
							'conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))
					)
			
			);
                        $this->set("userData", $userData);

		
		if(empty($this->data) == false) {

			$Insight = null;
			if(isset($this->data['Insight']['id'])) {
				$Insight = $this->Insight->find('first', array('conditions' => array('Insight.id' => $this->data['Insight']['id'])));
			}
                        $readlink = '<a href="<?php echo($postlink);?>" target="_blank"> Read Full Post</a>';
                      

                         $finalinsight = $this->data['Insight']['insight'];

                          preg_match('#(.*)(</p>)#si', $finalinsight, $matches);
                          $finalinsight = str_replace($matches[1], $matches[1].$readlink, $finalinsight);

			$Insight['Insight']['title'] = $this->data['Insight']['title'];
			$Insight['Insight']['insight'] = $finalinsight;
			$Insight['Insight']['user_id'] = $this->Session->read('Auth.User.id');
			$Insight['Insight']['image'] = $this->Session->read('Insight_image');
                        $Insight['Insight']['post_url'] = $this->Session->read('Insight_posturl');
                        $Insight['Insight']['feed_favicon'] = $this->Session->read('Insight_favicon');
			$this->Insight->save($Insight['Insight']);


			
			$this->Insight->createUrlKey($this->Insight->id);
			
			$this->set('Insight_title', $this->data['Insight']['title']);
			$this->set('Insight_insight', $this->data['Insight']['insight']);
			$this->set('Insight_id', $this->Insight->id);
                        $this->set('Insight_image', $this->Session->read('Insight_image'));
                        $this->set('Insight_posturl', $this->Session->read('Insight_posturl'));
			$this->set('Insight_favicon', $this->Session->read('Insight_favicon'));

			$this->Session->write('Insight_title', $this->data['Insight']['title']);
			$this->Session->write('Insight_insight', $this->data['Insight']['insight']);
                        $this->Session->write('Insight_image', $this->Session->read('Insight_image'));
                        $this->Session->write('Insight_posturl', $this->Session->read('Insight_posturl'));
                        $this->Session->write('Insight_favicon', $this->Session->read('Insight_favicon'));
			$this->Session->write('Insight_id', $this->Insight->id);
			
		}  else {
			$this->redirect(array('controller' => 'fronts', 'action' => 'index'));
		}
	}

	
	function add_insight(){
		
		if(empty($this->data) == false){
				
			if($this->Session->read('Insight_title') != '')
				$this->Session->delete('Insight_title');
			if($this->Session->read('Insight_insight') != '')
				$this->Session->delete('Insight_insight');

			if($this->Session->read('Insight_image') != '')
				$this->Session->delete('Insight_image');
			if($this->Session->read('Insight_posturl') != '')
				$this->Session->delete('Insight_posturl');
			if($this->Session->read('Insight_favicon') != '')
				$this->Session->delete('Insight_favicon');
				
			$Insight = $this->Insight->find('first', array('conditions' => array('Insight.id' => $this->Session->read('Insight_id'))));
			$Insight['Insight']['user_id'] = $this->Session->read('Auth.User.id');
			$Insight['Insight']['published']= 1;	
			$this->Insight->save($Insight);
				
			$this->Insight->createUrlKey($this->Session->read('Insight_id'));
			
	
			$checkBoxes = $this->data['Insight']['select'];
			$cat = explode(',',$checkBoxes);
			
			for($i=0;$i<count($cat);$i++){
			
				if(isset($cat[$i]) && $cat[$i] != '') {
					
					$topic = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$cat[$i])));
		               if(empty($topic)) {
			
                                $this->Topic->create( );
				    $this->Topic->save(array('autocomplete_text'=>$cat[$i]));

                                   $topicid = $this->Topic->getLastInsertID();
                                   $this->Topic->updateAll(array('Topic.topic_subset' => 0),array('Topic.id ' => $topicid));
                                   $topic = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topicid)));
				    
				}					
					$Insight_category['insight_id'] = $this->Session->read('Insight_id');
					$Insight_category['topic_id'] = $topic['Topic']['id'];
					
					$this->InsightCategory->save($Insight_category);
					$this->InsightCategory->id = '';
					$this->Topic->id = '';
				}
			}
			
		
			if($this->Session->read('Insight_id') != '')
				$this->Session->delete('Insight_id');
			$this->redirect(array('action'=>'insight/'.$this->Insight->data['Insight']['url_key']));
		}
	}
	
	function thanks_for_sharing(){
		
	}
	
	function insight($questionUrl = null){
		$this->layout= 'defaultnew';
		if($questionUrl == "") {
			
			$this->redirect(array('controller' => 'qna', 'action' => 'index'));
		} else {
			
			//$q = mysql_fetch_array(mysql_query("SELECT * FROM insights WHERE url_key = '".$questionUrl."'"));
                       $q = $this->Insight->find('first', array('conditions' => array('Insight.url_key' => $questionUrl)));
			$id = $q['Insight']['id'];
			if($id == "")
			{
				$this->redirect(array('controller' => 'qna', 'action' => 'index'));
			}
		}
			
		if($id != null) {

                        if($this->Session->read('Auth.User.id')!=''){



					$loggedinuser = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name','zipcode','linkedin_headline'),
											),
											'UserImage' ,
                                                                              'Social',
									),
									'conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))
							)
			
					);
                                         
                                        $this->set("loggedinuser",$loggedinuser);


                         }
			
			$this->Insight->Behaviors->attach('Containable');
			$Insight = $this->Insight->find('first', array('conditions' => array('Insight.id' => $id)));
			
			$this->set("title_for_layout",$Insight['Insight']['title']);
			
			$this->set("insight",$Insight);
			
			$this->User->Behaviors->attach('Containable');
			
			$userData = $this->User->find('first',
					array(
							'contain'=>array(
									'UserReference'=>array(
											'fields'=>array('first_name','last_name','zipcode','linkedin_headline'),
									),
									'UserImage' ,
                                                                        'Social',
							),
							'conditions'=>array('User.id'=>$Insight['Insight']['user_id'])
					)
			
			);
			
		        //Get all questions answered by him
		         $answers = $this->QnaAnswer->find('all',array('conditions' => array('QnaAnswer.member_id' => $Insight['Insight']['user_id'])));
		         $answeredQs = array();
		         $aqCount = 0;
		         foreach ($answers as $answer)
                        {
			 
			$question = $this->QnaQuestion->find('first',array('conditions' => array('QnaQuestion.id' => $answer['QnaAnswer']['question_id'])));
			$question['answer_text'] = $answer['QnaAnswer']['answer_text'];
			$answeredQs[$answer['QnaAnswer']['question_id']] = $question;
		        }

		        $answersCount = count($answeredQs);

			
			$insightsCount = $this->Insight->find('count',array('conditions' => array('Insight.user_id' => $Insight['Insight']['user_id'], 'Insight.published' => 1)));
			$userData['ans_count'] = $answersCount + $insightsCount ;
			$this->set("userData", $userData);
			$this->set("referrer", $this->referer());
			
			//GetQ categories
			$result = $this->Topic->find('all');
			$qcategories = array();
			foreach($result as $value){
				$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
			}
			$this->set("categories",$qcategories);
		} else {
			
			$this->redirect(array('controller' => 'qna', 'action' => 'index'));
		}
		
	}

         function index(){

             if($this->Session->read('Auth.User.role_id')==2){

		        $this->Session->write('popup', 1);

			//Get Insights
			$this->Insight->Behaviors->attach('Containable');
			$insights = $this->Insight->find('all', array('conditions'=>array('Insight.published'=>1),'order'=>'Insight.id desc'));
                        $total = count($insights);
			$insightArray = array();
			$qCount = 0;
			foreach($insights as $aValue){
					if($qCount < $total){
					$this->User->Behaviors->attach('Containable');
			
					$userData = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name','zipcode'),
											),
											'UserImage' ,
									),
									'conditions'=>array('User.id'=>$aValue['Insight']['user_id'])
							)
			
					);
						
					if(empty($userData['UserImage']) == false)
				       $aValue['User']['user_image'] = $userData['UserImage'][0]['image_name'];
					$aValue['User']['first_name'] = $userData['UserReference']['first_name'];
					$aValue['User']['last_name'] = $userData['UserReference']['last_name'];
					$aValue['User']['url_key'] = $userData['User']['url_key'];
						
					$insightArray[$qCount++] = $aValue;
			}
                   }
			$this->set('insights', $insightArray);


			//GetQ categories
			$result = $this->Topic->find('all');
			$qcategories = array();
			foreach($result as $value){
				$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
			}
			$this->set("categories",$qcategories);


           } else{


			
			$this->redirect(array('controller' => 'qna', 'action' => 'index'));
		}
		

             }

    function save_feed() {
    	
    	$feed_url = trim($this->request->data['feed_url']);
    	$feed_title = trim($this->request->data['feed_title']);
    	$feed_summary = trim($this->request->data['feed_summary']);
        $feed_image = trim($this->request->data['feed_image']);
        $feed_favicon = trim($this->request->data['feed_favicon']);
        
    	if (!empty($feed_url)) {
    		
    			$feed['user_id'] = $this->Auth->user('id');;
    			
    			$feed['title'] = $feed_title;
    			$feed['insight'] = $feed_summary;

                        $feed['image'] = $feed_image;
                        $feed['post_url'] = $feed_url;
    			$feed['feed_favicon'] = $feed_favicon;

    			$this->set('Insight_title', $feed_title);
			$this->set('Insight_insight', $feed_summary);
			$this->set('Insight_image', $feed_image);
                        $this->set('Insight_posturl', $feed_url);
                        $this->set('Insight_favicon', $feed_favicon);
			$this->Session->write('Insight_title', $feed_title);
			$this->Session->write('Insight_insight', $feed_summary);
                        $this->Session->write('Insight_image', $feed_image);
                        $this->Session->write('Insight_posturl', $feed_url);
                        $this->Session->write('Insight_favicon', $feed_favicon);

    			echo json_encode(array('id'=>$this->Feed->id, 'title'=>$feed_title, 'summary'=>$feed_summary,'image'=>$feed_image));
    			$this->layout = '';
    			$this->render(false);
    			exit();
    			 
    		}
    }
    
    function activity_feed($url = null) {
    
    	$feed_url = trim($this->request->data['feed_url']);
    	if (!empty($feed_url)) {
    
    		$url_end = 'https://api.embed.ly/1/extract?key=5798e7e0bf594bfdad4eed21b7cb3314&url='.$feed_url;
    
    		$curlOptions = array (
    				CURLOPT_URL => $url_end,
    				CURLOPT_VERBOSE => 1,
    				CURLOPT_SSL_VERIFYPEER => 0,
    				CURLOPT_SSL_VERIFYHOST => 0,
    				CURLOPT_RETURNTRANSFER => 1,
    		);
    
    		$ch = curl_init();
    		curl_setopt_array($ch,$curlOptions);
    
    		$response = curl_exec($ch);
             
    		if (curl_errno($ch)) {
    			prd(curl_error($ch));
    			$this -> _errors = curl_error($ch);
    			curl_close($ch);
    			return false;
    
    		} else  {
    			curl_close($ch);
    			$response_arr = json_decode($response,true);
    			$title = $response_arr['title'];
    			 
    			$summary_final = '';

                     $summary_final = $response_arr['description'];
    			$published = $response_arr['published'];
                        $favicon_url = $response_arr['favicon_url'];
                     
                  } 
    		$embed_end = 'https://api.embed.ly/1/oembed?key=5798e7e0bf594bfdad4eed21b7cb3314&url='.$feed_url;
    
    		$curlOptions = array (
    				CURLOPT_URL => $embed_end,
    				CURLOPT_VERBOSE => 1,
    				CURLOPT_SSL_VERIFYPEER => 0,
    				CURLOPT_SSL_VERIFYHOST => 0,
    				CURLOPT_RETURNTRANSFER => 1,
    		);
    
    		$chs = curl_init();
    		curl_setopt_array($chs,$curlOptions);
    
    		$responses = curl_exec($chs);
             
    		if (curl_errno($chs)) {
    			prd(curl_error($chs));
    			$this -> _errors = curl_error($chs);
    			curl_close($chs);
    			return false;
    
    		} else  {
    			curl_close($chs);
    			$responses_arr = json_decode($responses,true);
    			$summary_image = $responses_arr['thumbnail_url'];
    			 
                   }

                    if($summary_image =='')
                    {
                     $user = $this->User->findById($this->Auth->user('id'));
                     $summary_image = SITE_URL."img/".MENTORS_IMAGE_PATH.DS.$user['User']['id'].DS.$user['UserImage'][0]['image_name'];
                    }
    			echo json_encode(array('title'=>$title, 'summary'=>$summary_final, 'image'=>$summary_image,'published' =>$published,'favicon_url' =>$favicon_url));
    			$this->layout = '';
    			$this->render(false);
    			exit();
    			
    		
    	}
    }

      function upvote($insightid = 0) {
		
		if($this->Session->read('Auth.User.id') != '' && $insightid != 0) {
				
			$insight = $this->Insight->find('first',array('conditions' => array('Insight.id' => $insightid)));
			

		
			$oldVotes = $insight['Insight']['upvotes'];
			$upvote_users = $insight['Insight']['upvote_users'];
			
			$current_userid = $this->Session->read('Auth.User.id');
			
			$already_voted = 0;
			
			/*if($upvote_users) {
				
				$userids = explode(',',$upvote_users);
					
				for($i=0;$i<count($userids);$i++){
				
					if(isset($userids[$i]) && $userids[$i] != '') {
						
						if($userids[$i] == $current_userid) {
							$already_voted++;
						}
					}
				}
			}*/
			
			if($already_voted == 0) {
				
				$oldVotes++;
				
				$insight['Insight']['upvotes'] = $oldVotes;
				
				/*if($answer['Insight']['upvote_users']){
					$insight['Insight']['upvote_users'] = $insight['Insight']['upvote_users'].",".$current_userid;
				}
				else
					$insight['Insight']['upvote_users'] = $current_userid;*/
				
				$this->Insight->save($insight);
				
				echo json_encode(array('value' => $oldVotes, 'id' => $insight['Insight']['id']));
			}
			
			exit();
				
		} else {
			
			echo json_encode(array('value' => -1));
			exit();
		}
	}

        function share_insight() {



             if(!empty($this->data)) {

			$this->User->Behaviors->attach('Containable');
			$user = $this->User->findById($this->Auth->user('id'));


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = $user['User']['username'];
                           $fromName = $user['UserReference']['first_name']." ".$user['UserReference']['last_name'];
                           $websiteurl = SITE_URL;
                           $to = $this->data[EAddress];
                           $link = SITE_URL;
                           $subject = $this->data[ESubject];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("share_insight");
                           $data .= "&merge_fname=".urlencode(nl2br($this->data[FName]));
                           $data .= "&merge_emailcontent=".urlencode($this->data[EText]);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
                          $this->redirect($this->referer());
                     }
                



	}


 
}   
?>