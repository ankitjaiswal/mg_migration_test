<?php
/**
 * Mandrill Controller
 *
 * PHP version 5
 *
 * @category Controller
 */

require('../webroot/mandrill/Mandrill.php');

class MandrillsController extends AppController{
	/**
	* Controller name
	*
	* @var string
	* @access public
	*/
	var $name = 'Mandrills';
	
	function beforeFilter(){
		parent::beforeFilter();	 
		$this->Auth->allow('sendMail');
	}
	
	public function sendMail($user_fname = null, $user_lname = null)
	{
		pr(Debugger::trace());
		
		$mandrill = new Mandrill();
	
		$template_content = array(
				
		);
		
		$params = array(
				'subject' => 'Application declined',
				'from_email' => 'help@guild.im',
				'from_name' => "Guild",
				'to' => array(
						array('email' => 'pankajnirwan103@gmail.com', 'name' => 'Mr Tester')
				),
				'global_merge_vars' => array(
						array('name' => 'USER_FNAME', 'content' => $user_fname ),
						array('name' => 'USER_LNAME', 'content' => $user_lname ),
						array('name' => 'SITE_URL', 'content' => SITE_URL)
				)
		);
		
		$result = $mandrill->messages->sendtemplate('mentors_declined', $template_content, $params);
		
	
		echo '<pre>';
		print_r($result);
	}
 	
 
}   
?>