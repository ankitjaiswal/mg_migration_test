<?php

/**
 * Members Controller
 *
 * PHP version 5
 *
 * @category Controller
 */
App::uses('Sanitize', 'Utility');
//require('../webroot/mandrill/Mandrill.php');
require('../webroot/stripe-php-1.17.2/lib/Stripe.php');
require('../webroot/CryptClass.php');

use Elasticsearch\ClientBuilder;
require 'vendor/autoload.php';

class MembersController extends AppController {

    /**
     * Controller name
     * @var string
     * @access public
     */
    var $name = 'Members';
    /**
     * Models used by the Controller
     * @var array
     * @access public
     */
    var $uses = array('User','UserReference','UserImage', 'Social','Communication','Availablity','Mentorship',
    		'Invoice','StaticPage','Feedback','MessageLink','Message','Answer','NewsLetter', 'Topic','CaseStudy',
    		'DirectoryUser','Directory_consultations','Project', 'Plan', 'PlanUser','Feed',
    		'Qna_category','MediaRecords','ProjectMemberConfirm','Member','City','TakeawayData','Takeaway_prospect','Prospect',
			'MemberIndustryCategory','IndustryCategory','Testimonial','ShowcaseClient','CasestudyTopic','Synonym','ProjectInvoice','MentorApply','WebsiteProspect');
	var $helpers = array('General', 'Form', 'Ajax', 'javascript', 'Paginator', 'Html', 'Image');
	var $components = array("Session", "Email", "Auth", "RequestHandler" ,"Upload", "General",'Paginator','Security');
    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('activate','apply','member_full_profile', 
                'account_setting','eligibility','invitation_eligibility','checkEmail','delete','file_upload','password_setting_popup',
        		'general','delaccount','status_change','application_review', 'mentor_draft_autosave','add_casestudy','edit_casestudy',
        		'delete_casestudy','casestudy_preview','my_badges','unsubscribe', 'unsubscribe_qna', 'unsubscribed','logged_in',
        		'activity_feed', 'save_feed', 'del_feed','badges','premium','website','pr','stripecheckout', 'striperecurr', 
			'striperecurrcancel','advantage','subscribe_to_premium','checkbox_alert','cancel_plan','flevy',
			'flevy_email','prospect_client','comment','ninelenses','member_register', 'admin_search','admin_index','admin_add','admin_featured','admin_pending','admin_directory_index','admin_directory_add','admin_delete','admin_edit','admin_changepassword','switch_user','admin_directory_edit','admin_directory_delete','admin_directory_clean','admin_activateMentor',
                        'admin_declineMentor','admin_detail','admin_specifierChange','admin_deactivate_featured','linkedin_badge','stripeupdate','report','customplan_request','master','case_studies','casestudy','casestudy_pdf_preview','preview_pdf','preview_pdf1','finalize_casestudy','add_testimonial','edit_testimonial','del_testimonial','service_request','loggedin_service_request','admin_casestudyreview','casestudy_adminedit','admin_deletecasestudy','customshare_input','customshare','customshare_process','save_draft_profile','get_testimonial','checkUsername','logo_upload','blog','image_count','website_prospect','thanks_for_request');
    }





    function del_feed() {
    	 
    	$feed_id = trim($this->request->data['feed_id']);
    	 
    	if (!empty($feed_id)) {
    
    		$this->Feed->delete($feed_id);
    		    		 
    		echo json_encode(array('id'=>$feed_id));
    		$this->layout = '';
    		$this->render(false);
    		exit();
    
    	}
    }
    
    function save_feed() {
    	
    	$feed_url = trim($this->request->data['feed_url']);
    	$feed_title = trim($this->request->data['feed_title']);
    	$feed_summary = trim($this->request->data['feed_summary']);
       $feed_image = trim($this->request->data['feed_image']);
       $feed_published = trim($this->request->data['feed_published']);
       $feed_favicon = trim($this->request->data['feed_favicon']);
       $feed_utcpublished = trim($this->request->data['feed_utcpublished']);
    	if (!empty($feed_url)) {
    		
    			$feed['user_id'] = $this->Auth->user('id');
    			$feed['url'] = $feed_url;
    			$feed['title'] = $feed_title;
    			$feed['summary'] = $feed_summary;
    			$feed['image'] = $feed_image;
                     $feed['published'] = $feed_published;
                     $feed['feed_favicon'] = $feed_favicon;
                     $feed['utc_published'] = $feed_utcpublished;
    			$this->Feed->save($feed);
    	
    			echo json_encode(array('id'=>$this->Feed->id, 'title'=>$feed_title, 'summary'=>$feed_summary,'image'=>$feed_image,  'feed_favicon'=>$feed_favicon,'feed_published'=>$feed_published));
    			$this->layout = '';
    			$this->render(false);
    			exit();
    			 
    		}
    }
    
    function activity_feed($url = null) {
    
    	$feed_url = trim($this->request->data['feed_url']);
    	if (!empty($feed_url)) {
    
    		$url_end = 'http://api.embed.ly/1/extract?key=5798e7e0bf594bfdad4eed21b7cb3314&url='.$feed_url;
    
    		$curlOptions = array (
    				CURLOPT_URL => $url_end,
    				CURLOPT_VERBOSE => 1,
    				CURLOPT_SSL_VERIFYPEER => 0,
    				CURLOPT_SSL_VERIFYHOST => 0,
    				CURLOPT_RETURNTRANSFER => 1,
    		);
    
    		$ch = curl_init();
    		curl_setopt_array($ch,$curlOptions);
    
    		$response = curl_exec($ch);
             
    		if (curl_errno($ch)) {
    			prd(curl_error($ch));
    			$this -> _errors = curl_error($ch);
    			curl_close($ch);
    			return false;
    
    		} else  {
    			curl_close($ch);
    			$response_arr = json_decode($response,true);
    			$title = $response_arr['title'];
    			 
    			$summary_final = '';

                     $summary_final = $response_arr['description'];
    			$published = $response_arr['published'];

                     $favicon_url = $response_arr['favicon_url'];
                  } 
    		$embed_end = 'http://api.embed.ly/1/oembed?key=5798e7e0bf594bfdad4eed21b7cb3314&url='.$feed_url;
    
    		$curlOptions = array (
    				CURLOPT_URL => $embed_end,
    				CURLOPT_VERBOSE => 1,
    				CURLOPT_SSL_VERIFYPEER => 0,
    				CURLOPT_SSL_VERIFYHOST => 0,
    				CURLOPT_RETURNTRANSFER => 1,
    		);
    
    		$chs = curl_init();
    		curl_setopt_array($chs,$curlOptions);
    
    		$responses = curl_exec($chs);
             
    		if (curl_errno($chs)) {
    			prd(curl_error($chs));
    			$this -> _errors = curl_error($chs);
    			curl_close($chs);
    			return false;
    
    		} else  {
    			curl_close($chs);
    			$responses_arr = json_decode($responses,true);
    			$summary_image = $responses_arr['thumbnail_url'];
    			 
                   }

                    if($summary_image =='')
                    {
                     $user = $this->User->findById($this->Auth->user('id'));
                     $summary_image = SITE_URL."img/".MENTORS_IMAGE_PATH.DS.$user['User']['id'].DS.$user['UserImage'][0]['image_name'];
                    }
    			echo json_encode(array('title'=>$title, 'summary'=>$summary_final, 'image'=>$summary_image,'favicon_url' =>$favicon_url,'published' =>$published));
    			$this->layout = '';
    			$this->render(false);
    			exit();
    			
    		
    	}
    }
    
    function switch_user($id = null){
    	
    	if($this->Auth->user('id') == 1){
    		$this->Session->write('User_type', 'Fake');
    	}
    	$user = $this->User->read(null, $id);
		
		if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
			$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
		else
			$this->Session->write('User.image','');
        
        $this->Session->write('Auth.User',$user['User']);
						  
    	//$this->Auth->login($user);
    	
    	if($user['User']['role_id']== 3) {
    		$this->redirect(array('controller'=>'clients','action'=>'my_account'));
    	}else if($user['User']['role_id']== 2 && $user['User']['access_specifier']=="draft") {

                 $this->redirect(array('action' => 'member_full_profile'));

    	}
    	else if($user['User']['role_id']== 2) {
    		$this->redirect(array('controller'=>'users','action'=>'my_account'));
    	}
    	if($user['User']['id'] == 1){
    		$this->Session->write('Admin.email', $this->data['User']['email']);
            $this->Session->write('Admin.password', $_POST['data']['User']['password']);
            $this->Session->delete('User_type');
            $this->redirect(array('controller' => 'users', 'action' => 'admin_dashboard'));
    	}
    }
               
      function logged_in($email = null){//Redirecting of Consultant on Initial consultantion request

          if($this->Auth->user('role_id') != '' && $this->Auth->user('role_id') == 2){
           $this->redirect(array('controller'=>'users','action' => 'my_account'));
        }
          
          else if($this->Auth->user('role_id') != '' && $this->Auth->user('role_id') == 3){
           $this->redirect(array('controller'=>'fronts','action' => 'index'));
	   }
          
          else{
            $this->Session->write('myaccount',$email);
            $this->Session->write('menteeActEmail',$email);
           
           $this->redirect(array('controller'=>'fronts','action' => 'index'));
           }
     }
    
      function badges()
    {
    $id= $this->request['url']['id'];
    $type= $this->request['url']['type'];
    $this->layout= 'ajax';
    //prd($type);
       $this->User->Behaviors->attach('Containable');
              $condionArr = array();
		$condionArr['User.url_key'] = $id;
              $condionArr['User.status'] = 1;
	       $condionArr['User.is_approved'] = 1;
		$condionArr['User.access_specifier'] = 'publish';
		$this->data = $this->User->find('first',
				array(
						'contain'=>array(
								'UserReference'=>array(
										'fields'=>array('first_name','last_name'),
								),
								'UserImage' ,
								
						),
						'conditions'=>$condionArr,
				)

		);
                $this->data = $this->User->find('first',array('conditions'=>array('User.url_key'=>$id)));

		$dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($this->data['UserReference']['zipcode']))));
		$this->set("city",$dataZip['City']['city_name']);
		$this->set("state",$dataZip['City']['state']);

         $this->Session->write('badgetype',$type);
    }

      function linkedin_badge()
    {
    $id= $this->request['url']['id'];
    $this->layout= 'ajax';

       $this->User->Behaviors->attach('Containable');
              $condionArr = array();
		$condionArr['User.url_key'] = $id;
              $condionArr['User.status'] = 1;
	       $condionArr['User.is_approved'] = 1;
		$condionArr['User.access_specifier'] = 'publish';
		$this->data = $this->User->find('first',
				array(
						'contain'=>array(
								'UserReference'=>array(
										'fields'=>array('first_name','last_name'),
								),
								'Social' ,
								
						),
						'conditions'=>$condionArr,
				)

		);

    }

      function prospect_client(){
    	$url = trim($this->request->data['url']);
    	$link = trim($this->request->data['link']);
       $member_id = trim($this->request->data['member_detail']);
       $prospect_email = trim($this->request->data['client']);
       $consultant_detail = $this->User->find('first',array('conditions'=>array('User.id'=>$member_id)));
       $consultant_name = $consultant_detail['UserReference']['first_name']." ".$consultant_detail['UserReference']['last_name'];

       $takeaway_detail = $this->TakeawayData->find('first',array('conditions'=>array('TakeawayData.takeaway_link'=>$url)));
       $takeaway_input = $takeaway_detail['TakeawayData']['takeaway_input'];
                     if($prospect_email != ''){
                    
    			$takeaway['takeaway_link'] = $url;
                     $takeaway['takeaway_input'] = $takeaway_input;
                     $takeaway['consultant_name'] = $consultant_name;
                     $takeaway['prospect_email'] = $prospect_email;	

                     $this->Takeaway_prospect->save($takeaway);



                      }

                       

    		echo json_encode(array('link'=>$link));
              
    		$this->layout = '';
    		$this->render(false);
    		exit();
       }


       function customshare_input(){
  
        $this->layout= 'ajax';
        $member = $this->Session->read('Auth.User.url_key');
        $this->User->Behaviors->attach('Containable');
              $condionArr = array();
		$condionArr['User.url_key'] = $member;
              $condionArr['User.status'] = 1;
	       $condionArr['User.is_approved'] = 1;
		$condionArr['User.access_specifier'] = 'publish';
		$this->data = $this->User->find('first',
				array(
						'contain'=>array(
								'UserReference'=>array(
										'fields'=>array('first_name','last_name','business_website'),
								),
								'UserImage' ,
								'Social',
						),
						'conditions'=>$condionArr,
				)

		);


         }


      function customshare_process(){

    	$url = trim($this->request->data['url']);
    	$comment = trim($this->request->data['comment']);
       $destinationurl = trim($this->request->data['destinationurl']);
       $checkbox_status = trim($this->request->data['checkbox']);
       $member = $this->Session->read('Auth.User.url_key');
       $buttontext = trim($this->request->data['buttontext']);

       $login = "o_4urqen5mkq";
       $appkey = "R_7aa87ace460abde4cfc31fa31b2e59ef";
       $usource = "GUILD";
       $umedium = "referral";
       $ucampaign = "GUILD_Promote";
    	if (!empty($comment)) {
              
              $encrypt = "?link=".$url."&comment=".$comment."&member=".$member."&utm_source=".$usource."&utm_medium=".$umedium."&utm_campaign=".$ucampaign."&utm_content=".$url;

 
             Crypt::$key = md5(IOden2FIOU0J9nnfQ);
             $encrypted = Crypt::encrypt($encrypt);
             $link = "https://www.guild.im/members/customshare/".$encrypted;

                $dblink = $link;
              
              $link = $this->__make_bitly_url($link,$login, $appkey);

    		
    		$url_end = 'http://api.embed.ly/1/extract?key=5798e7e0bf594bfdad4eed21b7cb3314&url='.$url;
    
    		$curlOptions = array (
    				CURLOPT_URL => $url_end,
    				CURLOPT_VERBOSE => 1,
    				CURLOPT_SSL_VERIFYPEER => 0,
    				CURLOPT_SSL_VERIFYHOST => 0,
    				CURLOPT_RETURNTRANSFER => 1,
    		);
    
    		$ch = curl_init();
    		curl_setopt_array($ch,$curlOptions);
    
    		$response = curl_exec($ch);
             
    		if (curl_errno($ch)) {
    			prd(curl_error($ch));
    			$this -> _errors = curl_error($ch);
    			curl_close($ch);
    			return false;
    
    		} else  {
    			curl_close($ch);
    			$response_arr = json_decode($response,true);
    			$title = $response_arr['title'];
    			 
    			$summary_final = '';

                        $summary_final = $response_arr['description'];



                  }


    		       $length = strlen($title);
           
                     if($length >= 65){
                     $result = preg_replace('/\s+?(\S+)?$/', '', substr($title, 0, 65)); 
                     $title = $result."..";
                     }
                     else
                     $title = $title;
                     if($this->Session->read('Auth.User.role_id') == '2')
                     {
                     $destinationurl = "";
                     }

    		       $description_length = strlen($summary_final);
           
                     if($description_length >= 155){
                     $description_result = preg_replace('/\s+?(\S+)?$/', '', substr($summary_final, 0, 155)); 
                     $description = $description_result."..";
                     }
                     else
                     $description = $summary_final;

              	     $takeaway['user_id'] = $this->Auth->user('id');
    			
    		     $takeaway['takeaway_title'] = $title;
                     $takeaway['takeaway_description'] = $description;
                     $takeaway['takeaway_input'] = $url;
    		     $takeaway['takeaway_dblink'] = $dblink;
                     $takeaway['takeaway_link'] = $link;
                     $takeaway['takeaway_comment'] = $comment;	
                     $takeaway['destination_url'] = $destinationurl;
                     $takeaway['checkbox_status'] = $checkbox_status;
                     $takeaway['button_text'] = $buttontext;
                     $this->TakeawayData->save($takeaway);
 

                       

    		echo json_encode(array('link'=>$link, 'title'=>$title));
              
    		$this->layout = '';
    		$this->render(false);
    		exit();
    
    	}

        }





     function __make_bitly_url($url, $login, $appkey, $format='xml', $history=1, $version='2.0.1'){

      //create the URL
       $bitly = 'http://api.bit.ly/shorten';
       $param = 'version='.$version.'&longUrl='.urlencode($url).'&login='
       .$login.'&apiKey='.$appkey.'&format='.$format.'&history='.$history;

      //get the url
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $bitly . "?" . $param);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $response = curl_exec($ch);
      curl_close($ch);

     //parse depending on desired format
      if(strtolower($format) == 'json') {
        $json = @json_decode($response,true);
        return $json['results'][$url]['shortUrl'];
      } else {
        $xml = simplexml_load_string($response);
        return 'http://bit.ly/'.$xml->results->nodeKeyVal->hash;
     }
}




      function customshare(){
    
       $this->layout= 'ajax';
       $url = $this->request['url']['url'];
       $dblink = SITE_URL.$url;
   
       $string = explode('members/customshare/', $url);
       $decrypt = $string[1];
       Crypt::$key = md5(IOden2FIOU0J9nnfQ);
                    
       $decrypted = Crypt::decrypt($decrypt);
       $parts = parse_url($decrypted);
       parse_str($parts['query'], $query);
       $link = $query['link'];
       $comment = $query['comment'];
       $member = $query['member'];


    
        $this->layout= 'ajax';
        $this->User->Behaviors->attach('Containable');
              $condionArr = array();
		$condionArr['User.url_key'] = $member;
              $condionArr['User.status'] = 1;
	       $condionArr['User.is_approved'] = 1;
		$condionArr['User.access_specifier'] = 'publish';
		$this->data = $this->User->find('first',
				array(
						'contain'=>array(
								'UserReference'=>array(
										'fields'=>array('first_name','last_name','business_website'),
								),
								'UserImage' ,
								
						),
						'conditions'=>$condionArr,
				)

		);


       $TakeawayData = $this->TakeawayData->find('first', array('conditions' => array('AND' =>array(array('AND' => array(array('TakeawayData.takeaway_comment' => $comment),array('TakeawayData.takeaway_dblink' => $dblink))),array('OR' => array(array('TakeawayData.user_id' => NULL),array('TakeawayData.user_id' => $this->data['User']['id'])))))));     
          
                             
             $title = $TakeawayData['TakeawayData']['takeaway_title']; 
             $user = $TakeawayData['TakeawayData']['user_id']; 
             $description = $TakeawayData['TakeawayData']['takeaway_description'];                       
             $takeawayshortlink = $TakeawayData['TakeawayData']['takeaway_link'];
             $destinationurl = $TakeawayData['TakeawayData']['destination_url'];
             $takeawayinput = $TakeawayData['TakeawayData']['takeaway_input'];
             $checkbox = $TakeawayData['TakeawayData']['checkbox_status'];
             $buttontext = $TakeawayData['TakeawayData']['button_text'];
             $this->Session->write('takeawayshortlink',$takeawayshortlink);
             $this->Session->write('destinationurl',$destinationurl);
             $this->Session->write('title',$title);
             $this->Session->write('description',$description);
             $this->Session->write('link',$link);
             $this->Session->write('comment',$comment);
             $this->Session->write('checkbox',$checkbox);
             $this->Session->write('user',$user);
             $this->Session->write('takeawayinput',$takeawayinput);
             $this->Session->write('buttontext',$buttontext);
    }



      function comment()
    {
              

       $url = $this->request['url']['url'];
    
       $dblink = SITE_URL.$url;
   
       $string = explode('members/comment/', $url);
       $decrypt = $string[1];
       Crypt::$key = md5(IOden2FIOU0J9nnfQ);
                    
       $decrypted = Crypt::decrypt($decrypt);
       $parts = parse_url($decrypted);
       parse_str($parts['query'], $query);
       $link = $query['link'];
       $comment = $query['comment'];
       $member = $query['member'];



    
        $this->layout= 'ajax';
        $this->User->Behaviors->attach('Containable');
              $condionArr = array();
		$condionArr['User.url_key'] = $member;
              $condionArr['User.status'] = 1;
	       $condionArr['User.is_approved'] = 1;
		$condionArr['User.access_specifier'] = 'publish';
		$this->data = $this->User->find('first',
				array(
						'contain'=>array(
								'UserReference'=>array(
										'fields'=>array('first_name','last_name','business_website'),
								),
								'UserImage' ,
								
						),
						'conditions'=>$condionArr,
				)

		);

        $TakeawayData = $this->TakeawayData->find('first', array('conditions' => array('AND' =>array(array('AND' => array(array('TakeawayData.takeaway_comment' => $comment),array('TakeawayData.takeaway_dblink' => $dblink))),array('OR' => array(array('TakeawayData.user_id' => NULL),array('TakeawayData.user_id' => $this->data['User']['id'])))))));     

                           

                             
             $title = $TakeawayData['TakeawayData']['takeaway_title']; 
             $description = $TakeawayData['TakeawayData']['takeaway_description'];                       
             $url = $TakeawayData['TakeawayData']['takeaway_link'];
             $destinationurl = $TakeawayData['TakeawayData']['destination_url'];
             $checkbox = $TakeawayData['TakeawayData']['checkbox_status'];
             $this->Session->write('url',$url);
             $this->Session->write('destinationurl',$destinationurl);
             $this->Session->write('title',$title);
             $this->Session->write('description',$description);
             $this->Session->write('link',$link);
             $this->Session->write('comment',$comment);
             $this->Session->write('checkbox',$checkbox);

    }






    function eligibility(){


        $this->set('title_for_layout', 'Members Eligibility');
        $showApply = true;
    	$this->Session->write('popup', 1);
       if($this->Auth->user('id') == "")
    	{
    		$showApply = false;
    	}
    	$this->set('showApply',$showApply);
        $this->set('Mentor eligibility', 'Mentor');
		$this->data = $this->StaticPage->find('first',array('conditions'=>array('StaticPage.slug'=>'mentor_eligibility')));
    }

    function invitation_eligibility(){

         $this->layout= 'defaultnew';
        $this->set('Mentor eligibility', 'Mentor');
		//$this->data = $this->StaticPage->find('first',array('conditions'=>array('StaticPage.slug'=>'invitation_eligibility')));
  


   }

    function admin_index() {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';
        if (!isset($this->request['named']['page'])) {
            $this->Session->delete('Mentorsearch');
        }
              $userIds = $this->Member->find('all');
		foreach($userIds as $key =>$value)
		{
               
               $id[$key] = $value['Member']['member_id'];
              }
         $condionArr[] = array('User.id' => $id,'User.role_id' => 2, "User.status !=" => 2,'User.is_approved'=>1);
         if (!empty($this->data)) {
            $this->Session->delete('Mentorsearch');
            $keyword = $this->data['UserReference']['first_name'];
            if (!empty($this->data['UserReference']['first_name'])) {
                //App::import('Sanitize');
                //$keyword = Sanitize::escape($this->data['UserReference']['first_name']);
                $keyword = ($this->data['UserReference']['first_name']);
                $this->Session->write('Mentorsearch', $keyword);
            }
        }
        if ($this->Session->check('Mentorsearch')) {
              
            $first_name = array('UserReference.first_name LIKE' => "%" . $this->Session->read('Mentorsearch') . "%");
            $last_name = array('UserReference.last_name LIKE' => "%" . $this->Session->read('Mentorsearch') . "%");
            $filters[] = array('AND' =>array(array('OR' => array($first_name, $last_name)),array('User.role_id' => 2)));

              $this->User->Behaviors->attach('Containable');
       	$this->Paginator->settings = array(
              'recursive' => 0,
              'contain'=>array(
          			 'UserReference'=>array(
          			 'fields'=>array('first_name','last_name'),
          			 )
          		      ),
            
            'order' => array('User.id' => 'DESC'),
            'conditions' => $filters			
         );
        $result = $this->Paginator->paginate('User');		
        $this->set('data', $result);
        }     

          else{    
                          $userIds = $this->Member->find('all');
              
                            $mentorsdata = array();
				$this->User->Behaviors->attach('Containable');
				foreach($userIds as $value)
				{
				
					$condionArr = array();
					$condionArr['User.id'] = $value['Member']['member_id'];
				       
					
					$mentorsdata[$value['Member']['member_id']] = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name'),
											)
									),
									'conditions'=>$condionArr
							));
                               
				}
			
                   $this->set('data', $mentorsdata);
               }

        $this->set('title_for_layout', 'Member');
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }

function admin_search(){
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';
        if (!isset($this->request['named']['page'])) {
            $this->Session->delete('Mentorsearch');
        }
              $userIds = $this->Member->find('all');
		foreach($userIds as $key =>$value)
		{
               
               $id[$key] = $value['Member']['member_id'];
              }
         $condionArr[] = array('User.id' => $id,'User.role_id' => 2, "User.status !=" => 2,'User.is_approved'=>1);
         if (!empty($this->data)) {
            $this->Session->delete('Mentorsearch');
            $keyword = $this->data['UserReference']['first_name'];
            if (!empty($this->data['UserReference']['first_name'])) {
                //App::import('Sanitize');
                //$keyword = Sanitize::escape($this->data['UserReference']['first_name']);
                  $keyword = ($this->data['UserReference']['first_name']);
                $this->Session->write('Mentorsearch', $keyword);
            }
        }

             if ($this->Session->check('Mentorsearch')) {
              
            $first_name = array('UserReference.first_name LIKE' => "%" . $this->Session->read('Mentorsearch') . "%");
            $last_name = array('UserReference.last_name LIKE' => "%" . $this->Session->read('Mentorsearch') . "%");
            $filters[] = array('AND' =>array(array('OR' => array($first_name, $last_name)),array('User.role_id' => 2)));

              $this->User->Behaviors->attach('Containable');
       	$this->Paginator->settings = array(
              'recursive' => 0,
              'contain'=>array(
          			 'UserReference'=>array(
          			 'fields'=>array('first_name','last_name'),
          			 )
          		      ),
            
            'order' => array('User.id' => 'DESC'),
            'conditions' => $filters			
         );
        $result = $this->Paginator->paginate('User');		
        $this->set('data', $result);
        } 
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
}




    /*
     * Featured mentor 
     * 
     */
    function admin_deactivate_featured($id = null) {
    	if($this->Auth->user('id') == 1){
    	$this->layout = 'admin';
        $this->loadModel('FeaturedMentor');
        if ($id) {
                  $this->FeaturedMentor->deleteAll(array('FeaturedMentor.user_id' => $id));
            $this->redirect($this->referer());
        }
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }

      }

    function admin_featured() {
    	if($this->Auth->user('id') == 1){
    	$this->layout = 'admin';
        $this->loadModel('FeaturedMentor');
        if (empty($this->data) == false){
             $this->FeaturedMentor->deleteAll(array('not' => array('FeaturedMentor.order' => null)));
              
              foreach ($this->data['User']['Featured_member'] as $value){
                          
				$order = $value['order'];
                            if($order !=''){
				$id = $value['user_id'];

                            $data = array('user_id' => $id, 'created_by' => 1, 'is_featured' => 1,'order' => $order);
                            $this->FeaturedMentor->create();
			       $this->FeaturedMentor->save($data);
                            }
      
            }
            $this->redirect(array('action' => 'featured'));
        }

        $filters[] = array('User.role_id' => 2, 'User.status' => 1,'User.is_approved'=>1,'User.access_specifier'=>'publish');
        $featuredArr = $this->FeaturedMentor->find('all', array('order' => 'FeaturedMentor.id desc', 'limit' => 8));
        if (count($featuredArr) > 0) {
            $this->set('featuredDataResult', $featuredArr);
        } else {
            $this->set('featured', 0);
        }

            $this->User->Behaviors->attach('Containable');
	    $this->User->unbindModel(array('hasOne'=>array('Availablity'),'hasMany'=>array('UserImage','Social','Communication')),false);
	    $userslist = $this->User->find('all',array('conditions' => array('AND' => array(array('User.role_id' => 2),array('User.access_specifier' => 'publish'),array('User.status' => 1)))));

			
            $this->set('data', $userslist);

        $this->set('title_for_layout', 'Featured Member');
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }

    function admin_pending() {
    	if($this->Auth->user('id') == 1){
        $this->layout = 'admin';
        $filters[] = array('MentorApply.isApproved' =>array(0,1,2));
        $this->Paginator->settings = array(
            'limit' => 100,
            'order' => array('MentorApply.id' => 'DESC'),
            'conditions' => $filters
        );
        $result = $this->Paginator->paginate('MentorApply');
        $this->set('data', $result);
        $this->set('title_for_layout', 'Pending Member');
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }
    function admin_detail($id = null){
    	if($this->Auth->user('id') == 1){
    	$this->layout = 'admin';
        if(!$id){
			$this->Session->setFlash('Invalid Id!');
			$this->redirect(array('action'=>'admin_pending'));
		}	
		$mentor_data = $this->MentorApply->findById($id);		
		$this->set("mentor_data", $mentor_data);
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
      
    }
    function admin_activateMentor($id = null) {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';
		if ($id) {
            $uData = $this->MentorApply->find('first', array('conditions' => array('MentorApply.id' =>$id)));
			
			if (!empty($uData)) { 
				if($uData['MentorApply']['activation_key'] ==''){
					$uData['MentorApply']['activation_key'] = $this->_randomPrefix(15);
					$this->MentorApply->save(array("MentorApply" => array('activation_key'=>$uData['MentorApply']['activation_key'],'isApproved'=>1,"id" => $uData['MentorApply']['id'])));
				}else{
					$this->MentorApply->save(array("MentorApply" => array('isApproved'=>1,"id" => $uData['MentorApply']['id'])));
				}

				

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $deshmukhemail = "ankit@guild.im";
                           $to = $uData['MentorApply']['email'].";".$deshmukhemail;
                           $subject = "Membership application accepted";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentors_activate");
                           $data .= "&merge_userfname=".urlencode($uData['MentorApply']['firstname']);
                           $data .= "&merge_useremail=".urlencode($uData['MentorApply']['email']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

				
				$this->Session->setFlash('Activation mail sent to Mentor', 'admin_flash_good');
            } else {
                $this->Session->setFlash('Member not found.', 'admin_flash_bad');
            }
           
        } else {
            $this->Session->setFlash('Invalid Action.', 'admin_flash_bad');
        }
        $this->redirect($this->referer());
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }
    function admin_declineMentor($id = null){
    	if($this->Auth->user('id') == 1){
    	$this->layout = 'admin';
        if ($id) {
            $uData = $this->MentorApply->find('first', array('conditions' => array('MentorApply.id' =>$id)));
            if (!empty($uData)) { 
                $this->MentorApply->save(array("MentorApply" => array("isApproved" => 2, "id" => $uData['MentorApply']['id'])));            

                


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $deshmukhemail = "ankit@guild.im";
                           $to = $uData['MentorApply']['email'].";".$deshmukhemail;
                           $subject = "Membership application declined";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentors_application_declined");
                           $data .= "&merge_userfname=".urlencode($uData['MentorApply']['firstname']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

				
				$this->Session->setFlash('Member request declined', 'admin_flash_good');
           
		   } else {
                $this->Session->setFlash('Member not found.', 'admin_flash_bad');
            }
            
        } else {
            $this->Session->setFlash('Invalid Action.', 'admin_flash_bad');
        }
        $this->redirect($this->referer());
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }
    function admin_add() {
    	if($this->Auth->user('id') == 1){
    	$this->layout = 'admin';
        $this->set('title_for_layout', 'Add Member');
        if (!empty($this->data)) {
            $this->request->data['User']['role_id'] 		= 2;
            $this->request->data['User'] 				= $this->General->myclean($this->data['User']);
            $this->request->data['UserReference']	 	= $this->General->myclean($this->data['UserReference']);
            

            $this->User->set($this->request->data);
            $this->User->setValidation('admin');
            $this->UserReference->set($this->request->data);
            $this->UserReference->setValidation('admin');
            $this->request->data['User']['password'] 		= Security::hash('mentorsguild', null, true);
            //$this->data['Availablity']['day_time'] 	= $this->General->convertAvailablityToDB($this->data['Availablity1']);
            $this->request->data['User']['is_approved'] = 1;
			$this->request->data['User']['status'] = 1;				
			

			
			//////////////add created by and modified by
            $this->_setCreatedByModifiedBy();			
            if ($this->User->saveAll($this->request->data, array('validate' => 'first'))) {
                $id = $this->User->getInsertID();
                $this->_insertFile(array('id' => $id, 'userImage' => $this->data['UserImage1']));
                $this->Session->setFlash('Member has been saved', 'admin_flash_good');			
                $this->redirect(array('action' => 'index'));           
			} else {
                $this->Session->setFlash('Please correct the errors listed below.', 'admin_flash_bad');
            }
        }
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }

    }
    function _insertFile($imageDataArr) {
        //////uppload profile....

        $this->loadModel('UserImage');
       if (isset($imageDataArr['userImage']['image_name']['name']) && $imageDataArr['userImage']['image_name']['name'] != '') 
	   
	   {

            $destination = IMAGES . MENTORS_IMAGE_PATH . DS . $imageDataArr['id'];
            $smallfolder = IMAGES . MENTORS_IMAGE_PATH . DS . $imageDataArr['id'].'/small';
            if (!is_dir($destination)) {

                mkdir($destination, 0777);
            }
           else{

                 if ($handle = opendir($destination)) {



                 while (($file = readdir($handle))!==false) {



                  @unlink($destination.'/'.$file);

                  }

                 closedir($handle);

                }

                 if ($handle1 = opendir($smallfolder)) {



                 while (($file = readdir($handle1))!==false) {



                 @unlink($smallfolder.'/'.$file);

                  }

                 closedir($handle1);

                }

            }

            $destination .=DS;
            if (!is_dir($destination.'small')) {
                mkdir($destination.'small', 0777);
            }
            $imageArr = $imageDataArr['userImage']['image_name'];

            $result = $this->Upload->upload($imageArr, $destination, null, array('type' => 'resizecrop', 'size' => array('200', '200')));
            $result = $this->Upload->upload($imageArr, $destination.'small/', $this->Upload->result, array('type' => 'resizecrop', 'size' => array('40', '40')));
            
			$data = array('image_name' => $this->Upload->result, 'user_id' => $imageDataArr['id']);

            $this->UserImage->save($data);
           			
        }

    }

    function _setCreatedByModifiedBy() {
        $this->data['Availablity']['created_by'] = $this->Auth->user('id');
        $this->data['Availablity']['modified_by'] = $this->Auth->user('id');
        $this->data['User']['created_by'] = $this->Auth->user('id');
        $this->data['User']['modified_by'] = $this->Auth->user('id');

        $this->data['UserReference']['created_by'] = $this->Auth->user('id');
        $this->data['UserReference']['modified_by'] = $this->Auth->user('id');

        foreach ($this->data['Social'] as $key => $value) {
            $this->data['Social'][$key]['created_by'] = $this->Auth->user('id');
            $this->data['Social'][$key]['modified_by'] = $this->Auth->user('id');
            
        }


        foreach ($this->data['Communication'] as $key => $value) {
            $this->data['Communication'][$key]['created_by'] = $this->Auth->user('id');
            $this->data['Communication'][$key]['modified_by'] = $this->Auth->user('id');
            
        }
    }

    function admin_edit($id = null) {
    	if($this->Auth->user('id') == 1){

    	$this->layout = 'admin';
        $this->set('title_for_layout', 'Edit Member');
        if (!empty($this->data)) {
            //prd($this->data);
			
            $this->data['User']['role_id'] = 2;
            $this->data['User'] = $this->General->myclean($this->data['User']);
            $this->data['UserReference'] = $this->General->myclean($this->data['UserReference']);


            $this->User->set($this->data);
            $this->User->setValidation('admin');
            $this->UserReference->set($this->data);
            $this->UserReference->setValidation('admin');
            //////////////add created by and modified by
            $this->_setCreatedByModifiedBy();
            //////////////////////end here
            ///////////set mode_type
            foreach ($this->data['Communication'] as $key => $value) {
                if (!isset($value['mode_type'])) {
                    $this->data['Communication'][$key]['mode_type'] = '';
                }
            }
            
            $this->Social->deleteAll(array('Social.user_id'=>$id));
            
            ///////////////end here
			if ($this->User->saveAll($this->data, array('validate' => 'first'))) {
                $this->_insertFile(array('id' => $id,  'userImage' => $this->data['UserImage1']));
              
                mysql_query("DELETE FROM socials WHERE social_name= '' and user_id='$id'");

                

                
					//Saving image with new name
					
					$imageArr = $this->UserImage->find('first',array('conditions'=>array('UserImage.user_id'=>$id)));
					
					$currentName = explode('.',$imageArr['UserImage']['image_name']);
                 
	                $newName = $this->data['UserReference']['first_name'].'-'.$this->data['UserReference']['last_name'].'-'."GUILD.".$currentName[1];
	
	                $image_path = IMAGES . MENTORS_IMAGE_PATH . DS . $id;
	                if ($handle = opendir($image_path)) {
	                     	 
	                     while (false !== ($fileName = readdir($handle))) 
	                     {     
	                     	rename($image_path .DS . $imageArr['UserImage']['image_name'], $image_path .DS . $newName);
	                     }
	          
	                     closedir($handle);
	                }

					$small_image = IMAGES . MENTORS_IMAGE_PATH . DS . $id .DS. small;
        
	                 if ($small_handle = opendir($small_image)) {
	                     	 
	                     while (false !== ($fileName = readdir($small_handle))) {     
	            
		                     rename($small_image .DS . $imageArr['UserImage']['image_name'], $small_image .DS . $newName);
		                 }
          
	                     closedir($small_handle);
	          
	                 }
                 	$this->UserImage->updateAll(array('UserImage.image_name'=>"'".$newName."'"),array('UserImage.user_id'=>$id));


			$user_e = $this->User->find('first',array('conditions' => array('User.id' => $id)));

             

	//Fetch Data toupdate in ElasticSearch


                       if($user_e['User']['access_specifier'] =='publish'){

			$full_name = $user_e['UserReference']['first_name'] . " " . $user_e['UserReference']['last_name'] ;
			
			$city = $this->_getCityData($user_e['UserReference']['zipcode']);
			
			$ind_obj1 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['industry1'])));
			$cat_obj1 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['category1'])));
			
			$ind_obj2 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['industry2'])));
			$cat_obj2 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['category2'])));
			
			$ind_obj3 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['industry3'])));
			$cat_obj3 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['category3'])));
				


				if(empty($ind_obj1) == false && empty($cat_obj1) == false) {
					
                                $industry1 = $ind_obj1['IndustryCategory']['category'];
				$category1 = $cat_obj1['IndustryCategory']['category'];	

				}else{
                                 
                                $industry1 ='';
                                $category1 ='';
                                }   
		
				if(empty($ind_obj2) == false && empty($cat_obj2) == false) {
					
                                $industry2 = $ind_obj2['IndustryCategory']['category'];
				$category2 = $cat_obj2['IndustryCategory']['category'];	
					

				}else{
                                 
                                $industry2 ='';
                                $category2 ='';
                                } 
		
				if(empty($ind_obj3) == false && empty($cat_obj3) == false) {
					
                                $industry3 = $ind_obj3['IndustryCategory']['category'];
				$category3 = $cat_obj3['IndustryCategory']['category'];	
					

				}else{
                                 
                                $industry3 ='';
                                $category3 ='';
                                }


				//Topics Begin
				
				$topic1 = $user_e['MemberIndustryCategory']['topic1'];
				$topic2 = $user_e['MemberIndustryCategory']['topic2'];
				$topic3 = $user_e['MemberIndustryCategory']['topic3'];
				$topic4 = $user_e['MemberIndustryCategory']['topic4'];
				$topic5 = $user_e['MemberIndustryCategory']['topic5'];
				$topic_synonym = "";

				if($topic1 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic1)));
					$topic_first = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_first ='';
                                }

				if($topic2 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic2)));
					$topic_second = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_second  ='';
                                }

				if($topic3 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic3)));
					$topic_third = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_third ='';
                                }

				if($topic4 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic4)));
					$topic_fourth = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_fourth ='';
                                }

				if($topic5 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic5)));
					$topic_fifth = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_fifth ='';
                                }
				//Topics End

                //End Fetch Data toupdate in ElasticSearch


                    //ElasticSearch data update start

			$client = ClientBuilder::create()->build();	
			
			$params = array(
			    'index' => 'guild_members',
			    'type' => 'guildlive_new1',
			    'id' => $user_e['User']['id'],
			    'body' => array(
			        'doc' => array(
			        	'first_name' => $user_e['UserReference']['first_name'],
					    'last_name' => $user_e['UserReference']['last_name'],
					    'extension' => $user_e['User']['extension'],
					    'url_key' => $user_e['User']['url_key'],
					    'user_image' => $user_e['UserImage'][0]['image_name'],
					    'city_name' => $city['city_name'],
					    'state' => $city['state'],
					    'industry1' => $industry1,
					    'industry2' => $industry2,
					    'industry3' => $industry3,
					    'category1' => $category1,
					    'category2' => $category2,
					    'category3' => $category3,
					    'background_summary' => $user_e['UserReference']['background_summary'],
					    'topic1' => $topic_first,
					    'topic2' => $topic_second,
					    'topic3' => $topic_third,
					    'topic4' => $topic_fourth,
					    'topic5' => $topic_fifth,
                                            'topic_synonyms' => $topic_synonym,
						'location' => $city['latitude'] . ", ". $city['longitude'],
						'name' => $full_name,
						'headline' => $user_e['UserReference']['linkedin_headline']
			        )
			    )
			);
			
			// Update doc at /my_index/my_type/my_id
			$response = $client->update($params);


                     }   
			
			//ElasticSearch data update end



                $this->Session->setFlash('Member has been saved', 'admin_flash_good');
                $this->redirect(array('action' => 'search'));
            } else {
                $this->Session->setFlash('Please correct the errors listed below.', 'admin_flash_bad');
            }
        } else {
            $this->data = $this->User->read(null, $id);
           
        }
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }

    }
    function _getUserImages($user_id = null) {
        /* ==========get images for a particular useres=========== */
        if ($user_id) {
            $this->loadModel('UserImage');
            $this->UserImage->unbindModel(array('belongsTo' => array('User')), false);
            $data = $this->UserImage->find('all', array('conditions' => array('UserImage.user_id' => $user_id)));
            $dataArr = array();
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    $dataArr['UserImage'][] = $value['UserImage'];
                }
                $this->data['UserImage'] = $dataArr['UserImage'];
            } else {
                $this->data['UserImage'] = array();
            }

            //pr($this->data);die;
        }
        /* ==================end here=================== */
    }

    function admin_delete($id = null) {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';
        if (!$id) {
            $this->Session->setFlash('Invalid id for Admin');
            $this->redirect($this->referer());
        }
        $admin = $this->User->read(null, $id);
        if (empty($admin)) {
            $this->Session->setFlash('Invalid Member Id', 'admin_flash_bad');
             $this->redirect($this->referer());
        }

        if ($this->User->delete($id)) {
			$delData = $this->deleteAllFolder($id); 
            $del = $this->deleteTablesData($id);
            $this->Session->setFlash('Member has been deleted successfully', 'admin_flash_good');
            $this->redirect($this->referer());
        }
        $this->Session->setFlash('Member has not been deleted', 'admin_flash_bad');
         $this->redirect($this->referer());
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }
    function deleteTablesData($id = null)
    {
       $this->MessageLink->deleteAll(array('MessageLink.mentor_id'=>$id));
       $this->Message->deleteAll(array('Message.mentor_id'=>$id));
       $this->Member->deleteAll(array('Member.member_id'=>$id));


       
       $ids = $this->Mentorship->find('all',array('conditions'=>array('Mentorship.mentor_id'=>$id),'fields'=>array('Mentorship.id')));
       if(!empty($ids))
       {
          foreach($ids as $mentId)
          {
              $mentIds[] = $mentId['Mentorship']['id']; 
              $this->Feedback->deleteAll(array('Feedback.mentorship_id'=>$mentId['Mentorship']['id']));
              $this->Invoice->deleteAll(array('Invoice.mentorship_id'=>$mentId['Mentorship']['id']));
              $this->Mentorship->deleteAll(array('Mentorship.id'=>$mentId['Mentorship']['id']));
              $this->Answer->deleteAll(array('Answer.mentorship_id'=>$mentId['Mentorship']['id']));
              $this->Member->deleteAll(array('Member.member_id'=>$mentId['Mentorship']['id']));
              
          }
       }
    }
    function multipleDeleteRecord($ids)
    {
        foreach($ids as $id)
        {
            $this->deleteAllFolder($id);
            $this->deleteTablesData($id);
        }
    }
    

    function admin_process() {
    	if($this->Auth->user('id') == 1){
    	$this->layout = 'admin';
        if (!empty($this->data)) {
            App::import('Sanitize');
            $action = Sanitize::escape($_POST['pageAction']);

            foreach ($this->data['User'] AS $value) {
                if ($value != 0) {
                    $ids[] = $value;
                }
            }

            if (count($this->data) == 0 || $this->data['User'] == null) {
                $this->Session->setFlash('No items selected.', 'admin_flash_bad');
                $this->redirect(array('controller' => 'members', 'action' => 'index'));
            }
            if ($action == "delete") {

                $this->User->deleteAll(array('User.id' => $ids));
                $this->multipleDeleteRecord($ids);
                $this->Session->setFlash('Member have been deleted successfully', 'admin_flash_good');
                $this->redirect(array('controller' => 'members', 'action' => 'index'));
            }
            if ($action == "activate") {
 
                $this->User->updateAll(array('User.status' => 1), array('User.id' => $ids));
                $this->Session->setFlash('Member have been activated successfully', 'admin_flash_good');
                $this->redirect(array('controller' => 'members', 'action' => 'index'));
            }
            if ($action == "deactivate") {
                $this->User->updateAll(array('User.status' => Configure::read('App.Status.inactive')), array('User.id' => $ids));
                $this->Session->setFlash('Member have been deactivated successfully', 'admin_flash_good');
                $this->redirect(array('controller' => 'members', 'action' => 'index'));
            }
        } else {
            $this->redirect(array('controller' => 'members', 'action' => 'index'));
        }
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }

    function admin_changepassword($id = null) {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';
        $this->set('title_for_layout', 'Change Password');
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid User', true));
            $this->redirect(array('action' => 'search'));
        }
        if (!empty($this->data)) {
            $this->User->set($this->data);
            $this->User->setValidation('change_password');
            if ($this->User->validates()) {
                $user = $this->User->find('all',array('conditions'=>array('User.id'=>$id)));
                $user['User']['password'] = $this->data['User']['password2'];//Security::hash($this->data['User']['password2'], null, true);
                $user['User']['password2'] = Security::hash($this->data['User']['password2'], null, true);               
                $this->User->save($user,false);
                //$this->User->updateAll(array('password' => "'" . $user['User']['password'] . "'"), array('User.id' => $id));
                $this->Session->setFlash('Password has been changed successfully', 'admin_flash_good');
                $this->redirect(array('action' => 'search'));
            }
        } else {
            $this->data = $this->User->read(null, $id);
        }
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }

    function _randomPrefix($length) {
        $random = "";

        srand((double) microtime() * 1000000);

        $data = "AbcDE123IJKLMN67QRSTUVWXYZ";
        $data .= "aBCdefghijklmn123opq45rs67tuv89wxyz";
        $data .= "0FGH45OP89";

        for ($i = 0; $i < $length; $i++) {
            $random .= substr($data, (rand() % (strlen($data))), 1);
        }

        return $random;
    }
    function apply() {


         $this->layout= 'defaultnew';
    	$this->set('title_for_layout', 'Apply to be a Member');
        $this->Session->write('popup', 1);

                if($this->Session->check('mentorPopup')){
                $this->Session->delete('mentorPopup');
                }
                if($this->Session->check('menteePopup')){
                $this->Session->delete('menteePopup');
                }
                if($this->Session->check('projectId')){
                $this->Session->delete('projectId');
                } 
                if($this->Session->check('questionId')){
                $this->Session->delete('questionId');
                } 
                if($this->Session->check('duserid')){
                $this->Session->delete('duserid');
                } 
                if($this->Session->check('mentorshipId')){
                $this->Session->delete('mentorshipId');
                }
                if($this->Session->check('login_industry')){
                $this->Session->delete('login_industry');
                } 
                if($this->Session->check('mentor')){
                $this->Session->delete('mentor');
                }
                if($this->Session->check('consultantPopup')){
                $this->Session->delete('consultantPopup');
                }
                if($this->Session->check('directoryclaim')){
                $this->Session->delete('directoryclaim');
                } 


        $this->Session->write('mentor',1);

        if (!empty($this->data)) {




                 $activationKey = substr(md5(uniqid()), 0, 20);
                 $this->request->data['MentorApply']['activation_key'] = $activationKey;
                 $date = date('Y-m-d H:i:s');
                 $this->request->data['MentorApply']['submitted'] = $date;
                 $this->request->data['MentorApply']['isApproved'] = 0;
                 $this->MentorApply->save($this->request->data);

                


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "ankit@guild.im";
                           $subject = "New member request";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentors_apply");
                           $data .= "&merge_userfname=".urlencode($this->data['MentorApply']['firstname']);
                           $data .= "&merge_userlname=".urlencode($this->data['MentorApply']['lastname']);
                           $data .= "&merge_useremail=".urlencode($this->data['MentorApply']['email']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

                $this->Session->setFlash('<p>Congratulations! Your membership application has been submitted.</p><p>We will contact you as a part of our review process. </p>', 'flash_good');
               // $this->redirect(array('controller' => 'staticPages', 'action' => 'thanks'));
               $this->redirect(SITE_URL.'application_submitted');
            //}

        }
    }
    /* popup_sign up ajax reponse processing */
    function _popupSignUpAjaxResponse($reponseArr) {
        $this->layout = '';
        $this->render(false);
        $this->disableCache();
        /* Set error messages */
        if (isset($this->UserReference->validationErrors['terms_condtions'])) {
            $reponseArr['UserReference']['terms_condtions'] = $this->UserReference->validationErrors['terms_condtions'];
        } else {
            $reponseArr['UserReference']['terms_condtions'] = '';
        }
        if (isset($this->User->validationErrors['email'])) {
            $reponseArr['User']['email'] = $this->User->validationErrors['email'];
        } else {
            $reponseArr['User']['email'] = '';
        }
        if (isset($this->User->validationErrors['password2'])) {
            $reponseArr['User']['password2'] = $this->User->validationErrors['password2'];
        } else {
            $reponseArr['User']['password2'] = '';
        }
        if (isset($this->User->validationErrors['confirm_password'])) {
            $reponseArr['User']['confirm_password'] = $this->User->validationErrors['confirm_password'];
        } else {
            $reponseArr['User']['confirm_password'] = '';
        }

        /* End here */
        echo json_encode(array('value' => $reponseArr));
        exit();
    }

    /* Send Activation email to user */

    function _signupEmail($uData) {
        $this->loadModel('EmailTemplate');
        $mailMessage = "";
        $this->data = $uData;
        $message = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.slug' => 'mentor_register')));
        $activation_url = SITE_URL."/members/activate/{$this->data['User']['username']}/{$this->data['User']['activation_key']}";
        $activation_link = '<a href=' . $activation_url . '>' . $activation_url . '</a>';
        $mailMessage = str_replace(array('{name}', '{username}', '{activation_key}', '{password}'), array($this->data['UserReference']['first_name'], $this->data['User']['username'], $activation_link, $this->data['User']['password']), $message['EmailTemplate']['description']);
		
	   /* sending  mail to client for confirmation   */
        $this->set('message', $mailMessage);
        $this->Email->to = $this->data['User']['username'];
        $this->Email->from = Configure::read('Site.email');
        $this->Email->subject = $message['EmailTemplate']['subject'];
        $this->Email->template = 'user_register';
        $this->Email->sendAs = 'html';
        $this->Email->send();
    }

    /* For Activate sign up status */

    function activate($email = null, $activation_key = null){
		if($this->Session->check('Mentor')){
			$this->Session->delete('Mentor');
		}
		
		//draft and password null
		$DraftPasswordNull = $this->User->find('count', array('conditions' => array('User.username' => $email,'User.activation_key' => $activation_key,'User.access_specifier' => 'draft','User.password' =>null)));
		$data = $this->User->find('first', array('conditions' => array('User.username' => $email,'User.activation_key' => $activation_key,'User.access_specifier' => 'draft','User.password' =>null)));

		if ($DraftPasswordNull){			 
            $this->Session->write('Mentor.data',$data );
			$this->redirect(array('action' => 'member_full_profile'));
			
        }else {
            $this->Session->setFlash(__('Invalid activation link, please contact site administrator.', true), 'flash_bad');
            $this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
        }
    }
    function general($generalKey = null){
		if($this->Session->check('Mentor')){
			$this->Session->delete('Mentor');
		}	
		//draft and password null
		if ($generalKey == GENERAL_KEY){			 
            $this->Session->write('Mentor.data',array('User.hash'=>$generalKey));
			$this->redirect(array('controller'=>'members','action'=>'member_register','OpenMemberRegisterPopup:'.true));
			
        }else {
            $this->Session->setFlash(__('Invalid activation link, please contact site administrator.', true), 'flash_bad');
            $this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
        }
    }
	//open popup on member_full_profile
	
    /*     * ************************Sign up processing**************************** */
    /*     * *****************************End here********************************* */
	function password_setting_popup(){
		$this->layout = 'ajax';

		$this->Session->write('popup', 1);


                if($this->Session->check('menteePopup')){
                $this->Session->delete('menteePopup');
                }
                if($this->Session->check('projectId')){
                $this->Session->delete('projectId');
                } 
                if($this->Session->check('questionId')){
                $this->Session->delete('questionId');
                } 
                if($this->Session->check('login_industry')){
                $this->Session->delete('login_industry');
                } 
                if($this->Session->check('mentor')){
                $this->Session->delete('mentor');
                }
                if($this->Session->check('duserid')){
                $this->Session->delete('duserid');
                } 
                if($this->Session->check('mentorshipId')){
                $this->Session->delete('mentorshipId');
                } 
                if($this->Session->check('consultantPopup')){
                $this->Session->delete('consultantPopup');
                } 
                if($this->Session->check('directoryclaim')){
                $this->Session->delete('directoryclaim');
                } 

                $this->Session->write('consultantPopup', 1);


                //if($this->Session->check('Mentor.data')){
			
			if (!empty($this->data)) {


				$this->__subscribenewsletter($this->data['User']['username']);
				$this->request->data['User']['role_id'] = 2;
				$this->request->data['User']['status'] = 1;
				$this->request->data['User']['access_specifier'] = 'draft';
				$this->request->data['User']['is_approved'] = 1;
				$this->request->data['User']['password'] = $this->data['User']['password2'];//Security::hash($this->data['User']['password2'], null, true);
				$this->request->data['User']['password2'] = Security::hash($this->data['User']['password2'], null, true);				
				$this->User->saveAll($this->request->data);

				if(!isset($this->data['User']['id'])){
					$id = $this->User->getLastInsertID();
				}else{
					$id = $this->data['User']['id'];
				}
				$data = $this->User->read(null,$id);

				$this->Session->write('Mentor.data',$data);
				$this->Session->write('Mentor.PasswordSet',true);
				$this->Session->write('Auth', $data);
				$this->Session->write('User.image','');

                          //EasticEmail Integration - Begin

                           $res = "";
                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $this->data['User']['username'];
                           $subject = "Welcome to GUILD";
                           $urlkey = $data['User']['url_key'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("consultant_registration_email");
                           $data .= "&merge_userfname=".urlencode($this->data['UserReference']['first_name']);
                           $data .= "&merge_userurlkey=".urlencode($urlkey);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);

                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


				$this->redirect(array('action'=>'member_full_profile'));



				
			}else{
				$this->data = $this->Session->read('Mentor.data');
			}
		/** }else{
			$this->redirect(array('controller'=>'fronts','action'=>'index'));		
		}**/


	}
	
	function mentor_draft_autosave(){
		
		if($this->Auth->user('id') != "") {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);
				/*$UserReferenceFirstName = $this->request->data['UserReferenceFirstName'];
				$UserReferenceLastName = $this->request->data['UserReferenceLastName'];
				$UserReferenceZipcode = $this->request->data['UserReferenceZipcode'];
                            $UserReferenceLinkedinHeadline= $this->request->data['UserReferenceLinkedinHeadline'];
				$summary = $this->request->data['summary'];
                            $UserReferencePastClient= $this->request->data['UserReferencePastClient'];
				$UserReferenceAreaOfExpertise = $this->request->data['UserReferenceAreaOfExpertise'];
				$UserReferenceDesireMenteeProfile= $this->request->data['UserReferenceDesireMenteeProfile'];
                            $UserReferenceWebsite= $this->request->data['UserReferenceWebsite'];
				$UserReferenceFeeFirstHour= $this->request->data['UserReferenceFeeFirstHour'];
				$UserReferenceFeeRegularSession= $this->request->data['UserReferenceFeeRegularSession'];
				$UserReferencePaypalAccount= $this->request->data['UserReferencePaypalAccount'];
				$UserReferenceEngagementOverview= $this->request->data['UserReferenceEngagementOverview'];
				
				$mentor_data = $this->User->find('first',
						array(
							'contain'=>array(
								'UserReference'=>array(
									'fields'=>array('first_name','last_name','background_summary','accept_application','area_of_expertise','desire_mentee_profile','fee_first_hour','fee_regular_session','created','zipcode','application_5_fee','paypal_account','linkedin_headline','past_clients','business_website','feeNSession'),
								),
								'UserImage',
								
								'Communication',
								'Availablity',
								'Social',
								
							),
							'conditions'=>array('User.id'=>$this->Auth->user('id'))		
						)
				
					);
				
				$mentor_data['UserReference']['first_name']			=	$UserReferenceFirstName;
				$mentor_data['UserReference']['last_name']			=	$UserReferenceLastName;
				$mentor_data['UserReference']['zipcode']			=	$UserReferenceZipcode;
                            $mentor_data['UserReference']['linkedin_headline']    =   $UserReferenceLinkedinHeadline;
				$mentor_data['UserReference']['background_summary'] = $summary;
                            $mentor_data['UserReference']['past_clients']    =   $UserReferencePastClient;
				$mentor_data['UserReference']['area_of_expertise']    =   $UserReferenceAreaOfExpertise;
				$mentor_data['UserReference']['desire_mentee_profile']    =   $UserReferenceDesireMenteeProfile;
                            $mentor_data['UserReference']['business_website']    =   $UserReferenceWebsite;
				$mentor_data['UserReference']['fee_first_hour']    =   0;
				$mentor_data['UserReference']['fee_regular_session']    =   $UserReferenceFeeRegularSession;
				$mentor_data['UserReference']['paypal_account']    =   $UserReferencePaypalAccount;
				$mentor_data['UserReference']['feeNSession']    =   $UserReferenceEngagementOverview;
				$this->UserReference->save($mentor_data['UserReference'], false);*/
	
				$this->layout = '';
				$this->render(false);
	// 			echo json_encode(array('value' => $count));
				exit();
			}
		}
	
	}


       function member_full_profile($id=null){
  
        $this->layout= 'defaultnew';
               

   
		$mentor_data = $this->request->data;//$this->Session->read('Mentor.data');
		
		if(empty($mentor_data)) {
			$mentor_data = $this->Session->read('Mentor.data');
                        $memberaccept = $this->Session->read('Mentor.data');
                        $this->set('memberaccept',$memberaccept);
		}

  		//indexing for blank search  & admin table member
  		$user = $this->User->find('first', array('conditions'=> array('User.username'=>$this->Auth->user('username'))));
  		$member_id = $user['User']['id'];

   		if($this->Auth->user('id') =='' & $mentor_data !=''){
   			$member_id = $mentor_data['User']['id'];
   		}

      	$match = $this->Member->find('first', array('conditions'=> array('Member.member_id'=>$member_id)));
  		if(empty($match) && $member_id !='') 
  		{
  			$member['member_id'] = $member_id;
  			$this->Member->save($member);
  		}       
		if($this->Auth->user('id') != "" && $this->Auth->user('role_id') == 2 && $this->Auth->user('access_specifier') =='publish'){
			$this->redirect(array('controller'=>'users','action'=>'my_account'));
		}
		if($this->Auth->user('role_id') == 2 && $this->Auth->user('access_specifier') =='draft'){
			$mentor_data = $this->User->read(Null,$this->Auth->user('id'));
		}
		
						$keywords = $this->Topic->find('all',array(
						'fields' => array('DISTINCT (Topic.autocomplete_text) AS autocomplete_text'),
						'conditions'=>array('Topic.isName'=>0)
				));

				$this->set('keywords',$keywords);
				
				$key = array();
				$cn = 0;
				foreach($keywords as $value){
					$key[$cn++] = $value['Topic']['autocomplete_text'];
				}
				$this->set('allKeywords', $key);
		

				$industry_categories = $this->IndustryCategory->find('all',array('order'=>'IndustryCategory.id asc'));
		  		$this->set('industry_categories', $industry_categories);


		//GetQ categories
		$result = $this->Topic->find('all');
		$qcategories = array();
		foreach($result as $value){
			$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
		}
		$this->set("categories",$qcategories);
				
       if (!empty($mentor_data)) {


            if(isset($_POST['_method'])) {

                                                             //pr($this->data['UserReference']['select']);
                                                             //die;

                       $this->UserReference->setValidation('admin');   

                
                                $user['User']['role_id'] = 2;
				                $user['User']['access_specifier'] = $this->data['User']['access_specifier'];
				                $user['User']['status'] = 1;
                                                $user['User']['password'] = $user['User']['password2'];
                
                if ($this->User->save($user, false)) {
					$id = $this->User->id;
                                        //pr($id);
                                        //pr("ankit");
                                        $this->request->data['UserReference']['first_name'] = $this->data[firstname];
					                    $this->request->data['UserReference']['last_name'] = $bodytag = str_replace("'", "_slash_", $this->data[lastname]);
                                        $this->request->data['UserReference']['zipcode'] = $this->data[zipcode];
                                        $this->request->data['UserReference']['background_summary'] = $this->data[professionalsummary];
                                        $this->request->data['UserReference']['linkedin_headline'] = $this->data[profileheadline];
                                        $this->request->data['UserReference']['business_website'] = $this->data[businesswebsite];
                                        $this->request->data['UserReference']['feeNSession'] = $this->data[engoverview];
                                        $this->request->data['UserReference']['past_clients'] = $this->data[pastclients];
                                        $this->request->data['UserReference']['accept_application'] = "Y";
					                    $this->request->data['UserReference']['user_id'] = $id;

					                    $this->request->data['UserReference']['fee_first_hour'] = 0;
                                        $this->request->data['UserReference']['fee_regular_session'] = $this->data[consultationfee];
                                        $this->request->data['UserReference']['regular_session_per'] = $this->data[regularsessionper];
					                    $this->UserReference->save($this->request->data, false);
					

					//Saving image with new name
					
					$imageArr = $this->UserImage->find('first',array('conditions'=>array('UserImage.user_id'=>$id)));
					
					$currentName = explode('.',$imageArr['UserImage']['image_name']);
         

	                $newName = $this->data[firstname].'-'.$this->data[lastname].'-'."GUILD.".$currentName[1];
	
	                $image_path = IMAGES . MENTORS_IMAGE_PATH . DS . $id;
	                if ($handle = opendir($image_path)) {
	                     	 
	                     while (false !== ($fileName = readdir($handle))) 
	                     {     
	                     	rename($image_path .DS . $imageArr['UserImage']['image_name'], $image_path .DS . $newName);
	                     }
	          
	                     closedir($handle);
	                }

					$small_image = IMAGES . MENTORS_IMAGE_PATH . DS . $id .DS. small;
        
	                 if ($small_handle = opendir($small_image)) {
	                     	 
	                     while (false !== ($fileName = readdir($small_handle))) {     
	            
		                     rename($small_image .DS . $imageArr['UserImage']['image_name'], $small_image .DS . $newName);
		                 }
          
	                     closedir($small_handle);
	          
	                 }
                 	$this->UserImage->updateAll(array('UserImage.image_name'=>"'".$newName."'"),array('UserImage.user_id'=>$id));

					$this->Session->write('User.image', $newName);
					
				 


    		      $sociallinks = $this->Social->find('all', array('conditions' => array('Social.user_id' => $id)));
    		 
    		        foreach ($sociallinks as $links) {
    		
    			         $this->Social->delete($links['Social']['id']);
    		      }


                                        if($this->data[Social][0][social_name] !='' ){

                                                              $social_data['Social']['user_id'] = $id;
					                      $social_data['Social']['social_name']	= $this->data[Social][0][social_name];
					                      $this->Social->save($social_data['Social'], false);
                                                              $this->Social->id ='';
                                        } 
                                        if($this->data[Social][1][social_name] !='' ){

                                                              $social_data['Social']['user_id'] = $id;
					                      $social_data['Social']['social_name']	= $this->data[Social][1][social_name];
					                      $this->Social->save($social_data['Social'], false);
                                                              $this->Social->id ='';
                                        } 
                                        if($this->data[Social][2][social_name] !='' ){

                                                              $social_data['Social']['user_id'] = $id;
					                      $social_data['Social']['social_name']	= $this->data[Social][2][social_name];
					                      $this->Social->save($social_data['Social'], false);
                                                              $this->Social->id ='';
                                        } 
                                        if($this->data[Social][3][social_name] !='' ){

                                                              $social_data['Social']['user_id'] = $id;
					                      $social_data['Social']['social_name']	= $this->data[Social][3][social_name];
					                      $this->Social->save($social_data['Social'], false);
                                                              $this->Social->id ='';
                                        } 
                                        if($this->data[Social][4][social_name] !='' ){

                                                              $social_data['Social']['user_id'] = $id;
					                      $social_data['Social']['social_name']	= $this->data[Social][4][social_name];
					                      $this->Social->save($social_data['Social'], false);
                                                              $this->Social->id ='';
                                        } 





    		      $communicationlinks = $this->Communication->find('all', array('conditions' => array('Communication.user_id' => $id)));
    		 
    		        foreach ($communicationlinks as $links) {
    		
    			         $this->Communication->delete($links['Communication']['id']);
    		             }



                                        if($this->data[communication1checkbox] !=''){

                                          $communication_data['Communication']['user_id'] = $id;
					                      $communication_data['Communication']['mode_type']	= $this->data[communication1checkbox];
					                      $communication_data['Communication']['mode']	=	$this->data[communication1];
					                      $this->Communication->save($communication_data['Communication'],false);
                                                              $this->Communication->id ='';
                                        }  

                                       if($this->data[communication2checkbox] !=''){

                                           $communication_data['Communication']['user_id'] = $id;
					                       $communication_data['Communication']['mode_type']	=	$this->data[communication2checkbox];
					                       $communication_data['Communication']['mode']	=	$this->data[communication2];
					                       $this->Communication->save($communication_data['Communication'],false);
                                                               $this->Communication->id ='';
                                          }

                                        if($this->data[communication3checkbox] !=''){

                                           $communication_data['Communication']['user_id'] = $id;
					                       $communication_data['Communication']['mode_type']	=	$this->data[communication3checkbox];
					                       $communication_data['Communication']['mode']	=	$this->data[communication3];
					                       $this->Communication->save($communication_data['Communication'],false);
                                                               $this->Communication->id ='';
                                          }





					$availablity_data	=	array();
					if(isset($this->data['Availablity']['id'])){
						$availablity_data['Availablity']['id'] = $this->data['Availablity']['id'];
					}
					$availablity_data['Availablity']['user_id'] = $id;
					$availablity_data['Availablity']['day_time'] = $this->data[availablitytime];
					$this->Availablity->save($availablity_data);
	
					
				//Saving Expertise for Search Autocomplete
				$topic_arr = array(-1,-1,-1,-1,-1);
				$stringExp = $this->data['UserReference']['select'];
				$Exp3 = explode(',',$stringExp);
				$allExp = array();
				$count = 0;
					
				for($i=0;$i<count($Exp3);$i++){
				
					if(isset($Exp3[$i]) && $Exp3[$i] != '' && $Exp3[$i] != -1)
						$allExp[$count++] = ucfirst(trim($Exp3[$i]));
				}
					
				$uniqueAry = array_unique($allExp);
				
				$count = 0;
				
				foreach($uniqueAry as $exp){
						
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$exp)));
					
					$topic_arr[$count] = $match['Topic']['id'];
                                        if($match['Topic']['topic_subset'] == 0)
                                        {
                                        $this->Topic->updateAll(array('Topic.topic_subset' => 1),array('Topic.id ' => $match['Topic']['id']));

                                        }
					
					if(empty($match)) {
						$this->Topic->create( );
						$this->Topic->save(array('autocomplete_text'=>$exp));


     

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "iqbal@guild.im";
                           $subject = "New Topic has just added in a profile";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("new_topic_addition");
                           $data .= "&merge_userfname=".urlencode($this->data['UserReference']['first_name']);
                           $data .= "&merge_userlname=".urlencode($this->data['UserReference']['last_name']);
                           $data .= "&merge_newtopic=".urlencode($exp);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End



						$topic_arr[$count] = $this->Topic->id;
						unset($exp);
					}
					$count++;
				}
				

				//End Saving Autocomplete text
				
				//Saving Industry Categories
				$industry1 = $this->data['UserReference']['industry1'];
				$category1 = $this->data['UserReference']['category1'];
				
				$industry2 = $this->data['UserReference']['industry2'];
				$category2 = $this->data['UserReference']['category2'];
				
				$industry3 = $this->data['UserReference']['industry3'];
				$category3 = $this->data['UserReference']['category3'];
			
				$memberIndustryCategory = $this->MemberIndustryCategory->find('first',array('conditions'=>array('MemberIndustryCategory.user_id'=>$this->Auth->user('id'))));
				
				if(empty($memberIndustryCategory)) {
					$memberIndustryCategory['MemberIndustryCategory']['user_id'] = $this->Auth->user('id');
					$memberIndustryCategory['MemberIndustryCategory']['category1'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['category2'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['category3'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['industry1'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['industry2'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['industry3'] = -1;
				}
				
				$memberIndustryCategory['MemberIndustryCategory']['topic1'] = $topic_arr[0];
				$memberIndustryCategory['MemberIndustryCategory']['topic2'] = $topic_arr[1];
				$memberIndustryCategory['MemberIndustryCategory']['topic3'] = $topic_arr[2];
				$memberIndustryCategory['MemberIndustryCategory']['topic4'] = $topic_arr[3];
				$memberIndustryCategory['MemberIndustryCategory']['topic5'] = $topic_arr[4];
				
				if($industry1 != -1) {
					$industry_obj1 = $this->IndustryCategory->find('first',array('conditions'=>array('IndustryCategory.category'=>$industry1)));

					$cat_list = $this->IndustryCategory->find('all',array('conditions'=>array('IndustryCategory.category'=>$category1)));
					
					foreach ($cat_list as $key => $value) {

						if($value['IndustryCategory']['parent'] == $industry_obj1['IndustryCategory']['id']) {
							$memberIndustryCategory['MemberIndustryCategory']['industry1'] = $value['IndustryCategory']['parent'];
							$memberIndustryCategory['MemberIndustryCategory']['category1'] = $value['IndustryCategory']['id'];
						}
					}
				}

				if($industry2 != -1){
					$industry_obj2 = $this->IndustryCategory->find('first',array('conditions'=>array('IndustryCategory.category'=>$industry2)));

					$cat_list = $this->IndustryCategory->find('all',array('conditions'=>array('IndustryCategory.category'=>$category2)));
					
					foreach ($cat_list as $key => $value) {

						if($value['IndustryCategory']['parent'] == $industry_obj2['IndustryCategory']['id']) {
							$memberIndustryCategory['MemberIndustryCategory']['industry2'] = $value['IndustryCategory']['parent'];
							$memberIndustryCategory['MemberIndustryCategory']['category2'] = $value['IndustryCategory']['id'];
						}
					}
				}

				if($industry3 != -1) {
					$industry_obj3 = $this->IndustryCategory->find('first',array('conditions'=>array('IndustryCategory.category'=>$industry3)));

					$cat_list = $this->IndustryCategory->find('all',array('conditions'=>array('IndustryCategory.category'=>$category3)));

					foreach ($cat_list as $key => $value) {

						if($value['IndustryCategory']['parent'] == $industry_obj3['IndustryCategory']['id']) {
							$memberIndustryCategory['MemberIndustryCategory']['industry3'] = $value['IndustryCategory']['parent'];
							$memberIndustryCategory['MemberIndustryCategory']['category3'] = $value['IndustryCategory']['id'];
						}
					}
				}

				$this->MemberIndustryCategory->save($memberIndustryCategory);
				$this->MemberIndustryCategory->id = null;
				
				//End -- Saving Industry Categories
				
			//Updating ElasticSearch data
			$user_e = $this->User->find('first',array('conditions' => array('User.id' => $id)));
			$full_name = $user_e['UserReference']['first_name'] . " " . $user_e['UserReference']['last_name'] ;
			
			$city = $this->_getCityData($user_e['UserReference']['zipcode']);
			
			$ind_obj1 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['industry1'])));
			$cat_obj1 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['category1'])));
			
			$ind_obj2 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['industry2'])));
			$cat_obj2 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['category2'])));
			
			$ind_obj3 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['industry3'])));
			$cat_obj3 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $user_e['MemberIndustryCategory']['category3'])));
				


				if(empty($ind_obj1) == false && empty($cat_obj1) == false) {
					
                                $industry1 = $ind_obj1['IndustryCategory']['category'];
				$category1 = $cat_obj1['IndustryCategory']['category'];	

				}else{
                                 
                                $industry1 ='';
                                $category1 ='';
                                }   
		
				if(empty($ind_obj2) == false && empty($cat_obj2) == false) {
					
                                $industry2 = $ind_obj2['IndustryCategory']['category'];
				$category2 = $cat_obj2['IndustryCategory']['category'];	
					

				}else{
                                 
                                $industry2 ='';
                                $category2 ='';
                                } 
		
				if(empty($ind_obj3) == false && empty($cat_obj3) == false) {
					
                                $industry3 = $ind_obj3['IndustryCategory']['category'];
				$category3 = $cat_obj3['IndustryCategory']['category'];	
					

				}else{
                                 
                                $industry3 ='';
                                $category3 ='';
                                }


				//Topics Begin
				
				$topic1 = $user_e['MemberIndustryCategory']['topic1'];
				$topic2 = $user_e['MemberIndustryCategory']['topic2'];
				$topic3 = $user_e['MemberIndustryCategory']['topic3'];
				$topic4 = $user_e['MemberIndustryCategory']['topic4'];
				$topic5 = $user_e['MemberIndustryCategory']['topic5'];
				$topic_synonym = "";

				if($topic1 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic1)));
					$topic_first = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_first ='';
                                }

				if($topic2 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic2)));
					$topic_second = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_second  ='';
                                }

				if($topic3 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic3)));
					$topic_third = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_third ='';
                                }

				if($topic4 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic4)));
					$topic_fourth = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_fourth ='';
                                }

				if($topic5 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic5)));
					$topic_fifth = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_fifth ='';
                                }

                                   

			$client = ClientBuilder::create()->build();
			$params = array(
				    'index' => 'guild_members',
				    'type' => 'guildlive_new1',
				    'id' => $user_e['User']['id'],
				    'body' => array(
					    'first_name' => $user_e['UserReference']['first_name'],
					    'last_name' => $user_e['UserReference']['last_name'],
					    'extension' => $user_e['User']['extension'],
					    'url_key' => $user_e['User']['url_key'],
					    'user_image' => $user_e['UserImage'][0]['image_name'],
					    'city_name' => $city['city_name'],
					    'state' => $city['state'],
					    'industry1' => $industry1,
					    'industry2' => $industry2,
					    'industry3' => $industry3,
					    'category1' => $category1,
					    'category2' => $category2,
					    'category3' => $category3,
					    'background_summary' => $user_e['UserReference']['background_summary'],
					    'topic1' => $topic_first,
					    'topic2' => $topic_second,
					    'topic3' => $topic_third,
					    'topic4' => $topic_fourth,
					    'topic5' => $topic_fifth,
                                            'topic_synonyms' => $topic_synonym,
					    'quality' => $user_e['MemberIndustryCategory']['quality'],
						'location' => $city['latitude'] . ", ". $city['longitude'],
						'name' => $full_name,
						'headline' => $user_e['UserReference']['linkedin_headline']
					)
				);
				
				$response = $client->index($params);

			
			//ElasticSearch data update end
				



					

					
					$this->Session->delete('Mentor');




                     //condition need to change

                     if($this->Auth->user('id') || $this->data['User']['access_specifier'] =='publish'){
		      $user = $this->User->findById($this->Auth->user('id'));


                      $match = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$full_name)));
			
		       if(empty($match)) {
			
                                $this->Topic->create( );
				    $this->Topic->save(array('autocomplete_text'=>$full_name,'isName'=>"1"));
				    
				}
						
                        $this->Session->write('Auth',$user);
                        


                        //  sending  mail to client after publish profile   
                        $mailData = $this->UserReference->find('first',array('conditions'=>array('UserReference.user_id'=>$this->Auth->user('id')),'fields'=>array('UserReference.first_name','UserReference.last_name')));
                        

                        $user = $this->User->findById($this->Auth->user('id'));
			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $this->Auth->user('username');
                           $subject = "Your profile has been published";
                           $urlkey = $user['User']['url_key'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentors_draft_publish");
                           $data .= "&merge_userfname=".urlencode($mailData['UserReference']['first_name']);
                           $data .= "&merge_userurlkey=".urlencode($urlkey);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "deshmukh@guild.im";
                           $subject = "A new consultant has published profile";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentor_profile_create_notification");
                           $data .= "&merge_userfname=".urlencode($mailData['UserReference']['first_name']);
                           $data .= "&merge_userlname=".urlencode($mailData['UserReference']['last_name']);
                           $data .= "&merge_userkey=".urlencode(strtolower($this->Session->read('Auth.User.url_key')));
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End



			//$this->Session->setFlash(__("Profile updated", true), 'default', array('class' => 'success'));

			$this->redirect(array('controller'=>'users','action'=>'my_account'));
					}elseif($this->Auth->user('id') ==''){
						$this->redirect(array('controller'=>'fronts','action'=>'index','back_creation:'.true));
					}
					
	             
				} else {
                    $this->Session->setFlash('Please correct the errors listed below.', 'flash_bad');
                }
                
            }else{
				$this->request->data = $mentor_data;
			}
                        $this->set('title_for_layout', 'Member Profile Creation');
			$mentor_data = $this->User->read(Null,$this->Auth->user('id'));
                        $mentor_data['UserReference']['last_name'] = $bodytag = str_replace("_slash_", "'", $mentor_data['UserReference']['last_name']);
			$this->request->data = $mentor_data;
       } else {

                     if(strpos($id, ".")!== false){
                        $user = $this->User->find('first',array('conditions'=>array('User.url_key'=>$id)));
                        $id =  $user['User']['id'];
                        $mentor_data = $this->User->read(Null,$id);
                        $this->request->data = $mentor_data;
                        }else{
                        $this->Session->setFlash(__('This action is not allowed.', true), 'flash_bad');
                        $this->redirect(array('controller' => 'staticPages', 'action' => 'thanks'));

                       }
        }



         }

	

	function account_setting(){
                $this->layout= 'defaultnew';
		$this->set('title_for_layout','Account Setting');
		$userId = $this->Session->read('Auth.User.id');

     $customer = $this->User->find('first', array('conditions' => array('User.id' => $userId)));

     $customer_id = $customer['User']['paypalRecurringId'];

     Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");

     if (isset($_POST['stripeToken'])){
     try {
    $cu = \Stripe\Customer::retrieve($customer_id); // stored in your application
    $cu->source = $_POST['stripeToken']; // obtained with Checkout
    $cu->save();
    
    $this->User->updateAll(array('User.paypalRecurringId'=>"'".$cu['id']."'"),array('User.id'=>$this->Auth->user('id')));
    $success = "Your card details have been updated!";
    }
    catch(\Stripe\Error\Card $e) {

    // Use the variable $error to save any errors
    // To be displayed to the customer later in the page
    $body = $e->getJsonBody();
    $err  = $body['error'];
    $error = $err['message'];
     }
  // Add additional error handling here as needed
      }
		if(empty($userId) || $this->Session->read('Auth.User.role_id')!='2')
			$this->redirect(array('controller'=>'fronts','action'=>'index'));

               		//finding number of invoices
		$mentor_id = $this->Session->read('Auth.User.id');
	        $clientinvoicedata = $this->ProjectInvoice->find('all', array('conditions' => array('ProjectInvoice.inv_client_id' => $mentor_id)));

	        $consultantinvoicedata = $this->ProjectInvoice->find('all', array('conditions' => array('ProjectInvoice.inv_mentor_id' => $mentor_id)));

                $all_invoice = array_merge($consultantinvoicedata,$clientinvoicedata);
 
                               
  
				$data = Set::sort($all_invoice, '{n}.ProjectInvoice.created', 'DESC');

				$this->set('invoicedata', $data);             





		$mentorData = $this->UserReference->find('first',array('conditions'=>array('UserReference.user_id'=>$mentor_id),'fields'=>array('first_name','last_name')));



		 
		//findinf number of projects
		$pQ = $this->Project->find('count', array('conditions' => array('Project.user_id' => $mentor_id)));
		//$pQ += $this->Project->find('count', array('conditions' => array('Project.status' => '4')));
              $pQ += $this->ProjectMemberConfirm->find('count', array('conditions' => array('AND' => array(array('ProjectMemberConfirm.member_id' => $mentor_id),array('ProjectMemberConfirm.status' => '1')))));

		if($pQ == 0) {
			$this->set("numberOfProjects", 0);
		} else {
			$this->set("numberOfProjects", 1);
		}   


              
                $shown_interest = $this->ProjectMemberConfirm->find('all', array('conditions' => array('AND' => array(array('ProjectMemberConfirm.member_id' => $mentor_id),array('ProjectMemberConfirm.status' => '1')))));

                     $project = array();
  			foreach($shown_interest as $key=>$value)
			{

                     $project[$key] = $this->Project->find('first', array('conditions' => array('Project.id' => $value['ProjectMemberConfirm']['project_id'])));			           
                     }
                  
                  
              
	    	  $subcontract = $this->Project->find('all',array('conditions'=>array('Project.user_id'=>$mentor_id)));

                  $all_project = array_merge($subcontract,$project);

                              

				$data = Set::sort($all_project, '{n}.Project.id', 'DESC');
				$this->set('data', $data);               


		if(!empty($this->data))
		{
			$email = $this->Session->read('Auth.User.username');
			$catCheck = $_POST['catCheck'];
			$email_done = 0;
			$question_done = 0;
			$guarantee_done = 0;
			$this->request->data['UserReference']['accept_application'] = $_POST['accept-client'];
                        $this->UserReference->save($this->request->data['UserReference'],false);

                         $usernameupdate = $this->User->find('first',array('conditions'=>array('User.id'=>$userId)));

                         if($usernameupdate['User']['username'] != $this->data['User']['username']){
                          $this->User->updateAll(array('User.username'=>"'".$this->data['User']['username']."'"),array('User.id'=>$userId));


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "deshmukh@guild.im";
                           $subject = "Consultant has changed email address";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("consultant_email_change");
                           $data .= "&merge_userfname=".urlencode($usernameupdate['UserReference']['first_name']);
                           $data .= "&merge_userlname=".urlencode($usernameupdate['UserReference']['last_name']);
                           $data .= "&merge_profilelink=".urlencode($usernameupdate['User']['url_key']);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

                         }
			
			foreach($catCheck as $value) {
				
				if($value == 'email_notification') {
					
					$type = 'W';
					$this->request->data['UserReference']['email_notification'] = 'W';
					$this->__subscribenewsletter($email,$type);
					$email_done = 1;
					
				}
				if($value == 'question_notification') {
						
					$this->request->data['UserReference']['question_notification'] = 'Y';
					$question_done = 1;
				}
				if($value == 'guarantee') {
				
					$this->request->data['UserReference']['guarantee'] = 'Y';
					$guarantee_done = 1;
				}
			}
			
			if($email_done == 0) {
				
				$type = 'N';
				$this->request->data['UserReference']['email_notification'] = 'N';
				$this->__subscribenewsletter($email,$type);
			}
			
			if($question_done == 0) {
				$this->request->data['UserReference']['question_notification'] = 'N';
			}
			
			if($guarantee_done == 0) {
				$this->request->data['UserReference']['guarantee'] = 'N';
			}
			
			if($this->data['User']['oldpass']!='' && $this->data['User']['newpass']!='' && $this->data['User']['confpass']!='')
			{
				$oldPass = $this->data['User']['oldpass'];//Security::hash($this->data['User']['oldpass'], null, true);
				$userData = $this->User->find('first',array('fields'=>array('User.password'),'conditions'=>array('User.id'=>$userId),'recursive'=>'0'));
				if($this->data['User']['newpass']==$this->data['User']['confpass'])
				{
					$this->request->data['user']['id'] = $userId;
					$this->request->data['user']['password'] = $this->data['User']['newpass'];//Security::hash($this->data['User']['newpass'], null, true);
                                        $this->request->data['user']['password2'] = Security::hash($this->data['User']['newpass'], null, true);
					$this->User->save($this->request->data['user'],false);
                    $this->Session->setFlash(__("Password updated", true), 'default', array('class' => 'success'));
				}
				else
				{
					$this->Session->write('pass_error', 'Password incorrect');
					$this->redirect($this->referer());
				}
			}
			$this->UserReference->save($this->request->data['UserReference']);
			/*$this->UserReference->updateAll(array('UserReference.paypal_account' => $this->data['User']['paypal_account'],'UserReference.email_notification' => $this->data['User']['email_notification']),array('UserReference.user_id ' => $userId));
			$this->redirect(array('controller'=>'users','action'=>'my_account'));*/
			$this->redirect(array('controller'=>'users','action'=>'my_account'));
		}
		$userData = $this->User->find('all',array('fields'=>array('User.username,UserReference.id,UserReference.user_id,UserReference.question_notification, UserReference.email_notification, UserReference.guarantee, UserReference.accept_application'),'conditions'=>array('User.id'=>$userId),'recursive'=>'0'));
		$this->set('userData',$userData[0]);
	}

//************Function for Account settings page***********************//


        function premium(){



              $this->set('title_for_layout','GUILD Premium');

              $userId = $this->Session->read('Auth.User.id');

              if (!empty($this->data)) {
	       
		$prospect['Prospect']['firstname'] = $this->data['Mentorship']['firstname'];
		$prospect['Prospect']['lastname'] = $this->data['Mentorship']['lastname'];
		$prospect['Prospect']['email'] = $this->data['Mentorship']['email'];
		$prospect['Prospect']['company'] = $this->data['Mentorship']['company'];
		$prospect['Prospect']['phone'] = $this->data['Mentorship']['phone'];

		$prospect['Prospect']['prospect_type'] = "Interest in Premium";
		
		$this->Prospect->save($prospect);
              

               

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "iqbal@guild.im";
                           $subject = "Interest Request for GUILD Service";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("guild_premium_request");
                           $data .= "&merge_userfname=".urlencode($this->data['Mentorship']['firstname']);
                           $data .= "&merge_userlname=".urlencode($this->data['Mentorship']['lastname']);
                           $data .= "&merge_useremail=".urlencode($this->data['Mentorship']['email']);
                           $data .= "&merge_userorg=".urlencode($this->data['Mentorship']['company']);
                           $data .= "&merge_userphone=".urlencode($this->data['Mentorship']['phone']);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
                

              $sending = SITE_URL."thanks";
		$this->Session->setFlash(__("We will be in touch with you soon.", true), 'default', array('class' => 'notclass'));    				 
              echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
		die;
                } 



           }



        function master(){

              $this->layout= 'defaultnew';
              $this->Session->write('popup', 1);
              $this->set('title_for_layout','GUILD Master');

              $this->User->Behaviors->attach('Containable');
              $condionArr = array();
	      $condionArr['User.id'] = $this->Auth->user('id');
              $condionArr['User.status'] = 1;
	      $condionArr['User.is_approved'] = 1;
	      $condionArr['User.access_specifier'] = 'publish';
	      $this->data = $this->User->find('first',
				array(
						'contain'=>array(
								'UserReference'=>array(
										'fields'=>array('first_name','last_name','linkedin_headline','business_website'),
								),
								'UserImage' ,
                                                        'Communication',
                                                        'Social'
								
						),
						'conditions'=>$condionArr,
				)

		);



           }


         function website(){

              $this->layout= 'defaultnew2';
              $this->Session->write('popup', 1);
              $this->set('title_for_layout','GUILD Websites');

              $this->User->Behaviors->attach('Containable');
              $condionArr = array();
	      $condionArr['User.id'] = $this->Auth->user('id');
              $condionArr['User.status'] = 1;
	      $condionArr['User.is_approved'] = 1;
	      $condionArr['User.access_specifier'] = 'publish';
	      $this->data = $this->User->find('first',
				array(
						'contain'=>array(
								'UserReference'=>array(
										'fields'=>array('first_name','last_name','linkedin_headline','business_website'),
								),
								'UserImage' ,
                                                        'Communication',
                                                        'Social'
								
						),
						'conditions'=>$condionArr,
				)

		);


      }


         function advantage(){

              $this->layout= 'defaultnew';
              $this->Session->write('popup', 1);
              $this->set('title_for_layout','GUILD Advantage');


              if($this->Session->check('mediaUrl') && $this->Session->read('mediaUrl') != '')
              {
                  $this->Session->delete('mediaUrl');
              }
              if($this->Session->check('flevyUrl') && $this->Session->read('flevyUrl') != '')
              {
                  $this->Session->delete('flevyUrl');
              }
              if($this->Session->check('nlensesUrl') && $this->Session->read('nlensesUrl') != '')
              {
                  $this->Session->delete('nlensesUrl');
              }
              if($this->Session->check('sservicesUrl') && $this->Session->read('sservicesUrl') != '')
              {
                  $this->Session->delete('sservicesUrl');
              }
	       $member_serviceUrl= SITE_URL.'members/advantage';
	       $this->Session->write('member_serviceUrl',$member_serviceUrl);



              $this->User->Behaviors->attach('Containable');
              $condionArr = array();
	      $condionArr['User.id'] = $this->Auth->user('id');
              $condionArr['User.status'] = 1;
	      $condionArr['User.is_approved'] = 1;
	      $condionArr['User.access_specifier'] = 'publish';
	      $this->data = $this->User->find('first',
				array(
						'contain'=>array(
								'UserReference'=>array(
										'fields'=>array('first_name','last_name','linkedin_headline','business_website'),
								),
								'UserImage' ,
                                                        'Communication',
                                                        'Social'
								
						),
						'conditions'=>$condionArr,
				)

		);
			$this->data = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));

		
		$dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($this->data['UserReference']['zipcode']))));
		$this->set("city",$dataZip['City']['city_name']);
		$this->set("state",$dataZip['City']['state']);


      }

	function delete(){
		if($this->Session->read('SaveASDraftOrPublishUserID')){
			$this->User->delete($this->Session->read('SaveASDraftOrPublishUserID'));
			$this->Session->delete('SaveASDraftOrPublishUserID');
			$this->redirect(array('controller'=>'fronts','action'=>'index'));
		}else{
            $this->Session->setFlash(__('This action is not allowed.', true), 'flash_bad');
            $this->redirect(array('controller' => 'staticPages', 'action' => 'thanks'));			
		}
		
	}
    function checkEmail(){
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $email = $this->request->data['email'];
			$id = $this->request->data['id'];
			if($id == ''){
				$cond = array('User.status' =>array(0,1),'User.username' =>trim($email));
			
			}else{ 
				$cond = array('User.id !='=>$id,'User.status' =>array(0,1),'User.username' =>trim($email));
            }
			$count = $this->User->find('count', array('conditions' => $cond));
            
          
            $this->layout = '';
            $this->render(false);
            echo json_encode(array('value' => $count));
            exit();
        }   	
    	
    }

	function file_upload($id = null){
		//$this->User->unbindModel(array('hasOne'=>array('UserReference','Availablity'),'hasMany'=>array('Social','Communication')));
		//$userArr = $this->User->find('first',array('conditions'=>array('User.id'=>$id),'fields'=>array('id')));
		//$this->loadModel('UserImage');
		//$imageArr = $this->UserImage->find('first',array('conditions'=>array('UserImage.user_id'=>$id)));
		//prd($imageArr);
		 Configure::write('debug', 2);
		 $this->layout = '';
		 $this->render(false);
		 App::import('Component', 'FileUploader');
		 if($this->Session->read('Auth.User.role_id')!='')
		 	$role = $this->Session->read('Auth.User.role_id');
		 else
		 	$role = '2';
		 $FileUploader = new FileUploaderComponent();
		 $result = $FileUploader->profile_upload($id,$role);
		 if(isset($result['success']) && $result['success']==true){
			$this->loadModel('UserImage');
			$imageArr = $this->UserImage->find('first',array('conditions'=>array('UserImage.user_id'=>$id)));
			if(isset($imageArr['UserImage']['id'])){
				if($imageArr['User']['role_id']=='2')
				{
					 $path = IMAGES . MENTORS_IMAGE_PATH . DS . $imageArr['User']['id'].DS.$imageArr['UserImage']['image_name'];
					 $path1 = IMAGES . MENTORS_IMAGE_PATH . DS . $imageArr['User']['id'];
					 if(file_exists($path))
					 {
						@unlink($path1.'/uploaded/'.$imageArr['UserImage']['image_name']);
						@unlink($path1.'/small/'.$imageArr['UserImage']['image_name']);
						@unlink($path);
					 }
				}
				if($imageArr['User']['role_id']=='3')
				{
					 $path = IMAGES . MENTEES_IMAGE_PATH . DS . $imageArr['User']['id'].DS.$imageArr['UserImage']['image_name'];
					 $path1 = IMAGES . MENTEES_IMAGE_PATH . DS . $imageArr['User']['id'];
					 if(file_exists($path))
					 {
						@unlink($path1.'/uploaded/'.$imageArr['UserImage']['image_name']);
						@unlink($path1.'/small/'.$imageArr['UserImage']['image_name']);
						@unlink($path);
					 }
				}
				$this->UserImage->save(array('id'=>$imageArr['UserImage']['id'],'image_name'=>$result['filename']));
			}else{
				$this->UserImage->save(array('image_name'=>$result['filename'],'user_id'=>$id));
			}
			$this->Session->write('User.image', $result['filename']);
		 }
		 echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
	}
	
	function status_change()
	{
		if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $userId = $this->Session->read('Auth.User.id');
            $this->User->id = $userId;
			$this->User->saveField('status', '0');
			$this->layout = '';
			$user = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
                        if($user['User']['role_id'] == 3)
                          {
                           $usertype = "Client";
                          }
                        else
                          {
                           $usertype = "Consultant";
                          }   

                          $username = $user['UserReference']['first_name']." ".$user['UserReference']['last_name'];
			  $useremail = $user['User']['username'];
                          $social = $this->Social->find('all',array('conditions' => array('Social.user_id' => $user['User']['id'])));

                          foreach($social as $value){

			       if(($pos =strpos($value['Social']['social_name'],'linkedin'))!==false){
				$linkedinURL = $value['Social']['social_name'];
                                }
                          }

                          $communication = $this->Communication->find('all',array('conditions' => array('Communication.user_id' => $user['User']['id'])));

                          foreach($communication as $value){

                                
				if($value['Communication']['mode_type']=='phone'){
				$phone = $value['Communication']['mode'];
                                }
                          }
                          

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "ankit@guild.im";
                           $subject = "Member deactivated GUILD account";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("deactivate");
                           $data .= "&merge_memberid=".urlencode($user['User']['id']);
                           $data .= "&merge_usertype=".urlencode($usertype);
                           $data .= "&merge_username=".urlencode($username);
                           $data .= "&merge_useremail=".urlencode($useremail);
                           $data .= "&merge_userlinkedin=".urlencode($linkedinURL);
                           $data .= "&merge_userphone=".urlencode($phone);


                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $useremail;
                           $subject = "Your GUILD account has been deactivated";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("cancel_consultant_membership");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
			

			
	
			
            $this->render(false);
			//$this->Auth->logout();
            echo json_encode(array('value' => '1'));
            exit();
        }   	
	}
	
	function delaccount()
	{
		$this->layout = 'ajax';
	}
	
	function checkoldpass()
	{
		if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $oldPass = $this->request->data['oldPass'];//Security::hash($this->request->data['oldPass'], null, true);
			$userId = $this->Session->read('Auth.User.id');
			$userData = $this->User->find('first',array('fields'=>array('User.password'),'conditions'=>array('User.id'=>$userId),'recursive'=>'0'));
			$orgPass = $userData['User']['password'];
			
			$count = $this->User->find('count', array('conditions' => $cond));
            
          
            $this->layout = '';
            $this->render(false);
            echo json_encode(array('value' => $count));
            exit();
		}
	}
	
	public function admin_download_file($id=null,$filename=null)
	{
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';
		$this->layout = false;
		$this->render = false;
		if(is_null($filename) || is_null($id))
		{
			$this->Session->setFlash(__('There is a problem in processing your request.', true), 'flash_bad');
			$this->redirect($this->referer());
		}
		
		
		$download_path = "img/members/resumes/".$id;
		if(!$filename) die("I'm sorry, you must specify a file name to download.");
		if(eregi("\.\.", $filename)) die("I'm sorry, you may not download that file.");
		$file = str_replace("..", "", $filename);
		if(eregi("\.ht.+", $filename)) die("I'm sorry, you may not download that file.");
		
		$file = "$download_path/$file";
		
		$today = date("F j, Y, g:i a");
		$time = time();

	   if(file_exists($file))
		{
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.basename($filename));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			readfile($file);
		}
		else
		{
			$this->Session->setFlash(__('File not found on the given path.', true), 'flash_bad');
			$this->redirect($this->referer());
		}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}
	
	public function admin_specifierChange($id,$status)	
	{
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';
		$this->User->id = $id;
		if($status=='draft') {
		  $this->User->saveField('access_specifier', 'publish');
		  //$this->__toggleDirectoryUserStatus($id);
		}
		if($status=='publish') {
		  $this->User->saveField('access_specifier', 'draft');
		 // $this->__toggleDirectoryUserStatus($id);
		}
		$this->redirect($this->referer());
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}
	
	function deleteAllFolder($id)
	{
		$dirnameImage = IMAGES . MENTORS_IMAGE_PATH . DS . $id;
		$dirnameResource = IMAGES . MENTORS_RESOURCE_PATH . DS . $id;
		$dirnameResume = IMAGES . MENTORS_RESUME_PATH . DS . $id;
		if(is_dir($dirnameImage))
		{
			$this->RmDirRecursive($dirnameImage); 
			rmdir($dirnameImage);
		}
		if(is_dir($dirnameResource))
		{
			$this->RmDirRecursive($dirnameResource); 
			rmdir($dirnameResource);
		}
		if(is_dir($dirnameResume))
		{
			$this->RmDirRecursive($dirnameResume); 
			rmdir($dirnameResume);
		}
		return true;
	}
	
	function RmDirRecursive($path) 
	{
		$realPath = realpath($path);
		if (!file_exists($realPath)) 
			return false;
		$DirIter = new RecursiveDirectoryIterator($realPath);	
		$fileObjects = new RecursiveIteratorIterator($DirIter, RecursiveIteratorIterator::CHILD_FIRST);
		foreach ($fileObjects as $name => $fileObj) {
			if ($fileObj->isDir()) 
				rmdir($name);
			else
				unlink($name);
		}
		return true;
	}
	
	function application_review($mentorship_id = null)
	{
		$userId = $this->Session->read('Auth.User.id');
		$this->Mentorship->bindModel(
				array(
				'belongsTo' => array(
					'Mentee' => array(
						'className' => 'User',
						'foreignKey' => 'mentee_id',
				 	),
					'UserReference' => array(
						'className' => 'UserReference',
						'foreignKey' => false,
						'conditions' =>array('Mentorship.mentee_id=UserReference.user_id'),
				 	),
				 	'Mentor' => array(
                        'className' => 'UserReference',
                        'foreignKey' => false,
                        'conditions' =>array('Mentorship.mentor_id=Mentor.user_id'),
                    )
					)));
		if(!empty($this->data)){
			if($this->data['Mentorship']['clickValue']=='accept')
			{
				/*$this->Mentorship->id = $this->data['Mentorship']['id'];
				$this->Mentorship->saveField('applicationType', '3');
				$this->Mentorship->recursive = '0';
				$mentorshipData = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.id'=>$this->Mentorship->id),'fields'=>array('Mentorship.*')));
			
				 create new invoice for mentee 
				$invoiceData['Invoice']['inv_no'] = "Invoice-".$mentorshipData['Mentorship']['mentor_id']."-".$mentorshipData['Mentorship']['mentee_id']."-".$this->Mentorship->id;
				$invoiceData['Invoice']['mentorship_id'] = $this->Mentorship->id;
				$this->Invoice->save($invoiceData);*/
				
				$this->redirect(array('controller'=>'invoice','action'=>'member_create/'.$this->data['Mentorship']['id']));
			}
			if($this->data['Mentorship']['clickValue']=='decline')
			{
				$this->Mentorship->recursive = '0';
				$menteeData = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.id'=>$this->data['Mentorship']['id']),'fields'=>array('Mentee.username','Mentee.url_key','Mentor.first_name','Mentor.last_name','UserReference.first_name','UserReference.last_name')));
                

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $menteeData['Mentee']['username'];
                           $subject = "Initial consultation declined";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentors_declined");
                           $data .= "&merge_userfname=".urlencode($menteeData['UserReference']['first_name']);
                           $data .= "&merge_consultantfname=".urlencode($menteeData['Mentor']['first_name']);
                           $data .= "&merge_consultantlname=".urlencode($menteeData['Mentor']['last_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


				$this->Mentorship->id = $this->data['Mentorship']['id'];
				$this->Mentorship->saveField('is_deleted', '1');	
                $this->Answer->deleteAll(array('Answer.mentorship_id'=>$this->data['Mentorship']['id']));	
			//	$this->Mentorship->delete($this->data['Mentorship']['id'],true);
				
			}
			$this->redirect(array('controller'=>'users','action'=>'my_account'));
			
		}else{
			$data = $this->Mentorship->find('first',array(
							'conditions'=>array('Mentorship.id'=>$mentorship_id,'Mentorship.mentor_id'=>$userId),
							'fields'=>array('UserReference.first_name','UserReference.last_name','Mentee.url_key','Mentorship.id','Mentorship.mentee_need','Mentee.role_id')
							));
			if(empty($data['Mentorship']))
				$this->redirect(array('controller'=>'users','action'=>'my_account'));
			else
				$this->set('data',$data);
		}
	}

    /* admin functionality related to reporting */
    
    function admin_mentor_static($mentorId=null)
    {
    	if($this->Auth->user('id') == 1){
    	$this->layout = 'admin';
        if(!empty($this->data))
        {
            pr($this->data); die;
        }
        $menteeCount = $this->Mentorship->find('count',array('conditions'=>array('Mentorship.mentor_id'=>$mentorId,'Mentorship.is_deleted!=1'),'group'=>'Mentorship.mentee_id'));
        $SessionCount = $this->Mentorship->find('count',array('conditions'=>array('Mentorship.mentor_id'=>$mentorId,'Mentorship.is_deleted!=1')));
        $ids = $this->Mentorship->find('all',array('conditions'=>array('Mentorship.mentor_id'=>$mentorId,'Mentorship.is_deleted!=1'),'fields'=>array('Mentorship.id')));
        if(!empty($ids))
         {
             
            foreach($ids as $mentId)
            {
                $mentIds[] = $mentId['Mentorship']['id']; 
            }
            $totalEarning = $this->Invoice->find('first',array('conditions'=>array('Invoice.mentorship_id'=>$mentIds),'fields'=>array('sum(((Invoice.total) - ((Invoice.total * Invoice.rate)/100))) AS Amount,sum((Invoice.total * Invoice.rate)/100) as commission ')));
            $data['amount']= $totalEarning[0]['Amount'];
            $data['commission']= $totalEarning[0]['commission'];
         }
        else
        {
             $data['amount']= 0.00; $data['commission'] = 0.00;
        }
        $data['menteeCount'] = $menteeCount;
        $data['sessionCount']= $SessionCount;
        $data['userId']= $mentorId;
        
        $this->set('data',$data);
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    } 

    function admin_mentor_programlist($mentor_id=null)
    {}
	
	function _findexts ($filename)
    {
        $filename = strtolower($filename) ;
        $exts = split("[/\\.]", $filename) ;
        $n = count($exts)-1;
        $exts = $exts[$n];
        return $exts;
    } 
	

	
	function __subscribenewsletter($email=null,$notify=null)
	{
		if($email!="")
		{
			$newsletterData = $this->NewsLetter->find('first',array('conditions'=>array('NewsLetter.email'=>$email)));
			if(empty($newsletterData))
			{
				$newsData['NewsLetter']['email'] = $email;
				$newsData['NewsLetter']['status'] = '1'; 
				$this->NewsLetter->save($newsData);
			}
			if($notify!="" && $notify=='N')
			{
				$conditions = array('NewsLetter.email'=>$email);
				$this->NewsLetter->deleteAll($conditions);
			}
		}
	}  
	
	function add_casestudy($id=null){

                 $this->layout= 'defaultnew';
		
		//if id is null. a new CS is being added
		//if not null then it is an edit
		if($this->Auth->user('id') != "" && $this->Auth->user('role_id') == 2 && $this->Auth->user('access_specifier') =='publish') {
			if(empty($this->data['CaseStudy']) == true) { //memeber has clicked on add new button

                         $case_study = $this->CaseStudy->find('first',array('conditions'=>array('CaseStudy.id' => $id)));
                         $this->set('case_study', $case_study);

			} else { // member has clicked on save button
					$case_study = $this->data['CaseStudy'];
					$case_study['user_id'] = $this->Auth->user('id');
					if($case_study['CaseStudy']['url'] != '')
						$case_study['url'] = $this->General->addhttp($this->data['CaseStudy']['url']);
					
					$random = substr(md5(uniqid()), 0, 5);
					
					$urlKey = preg_replace('/\PL/u', '-', $case_study['title']);
					$urlKey .= '-'.$random;
					
					$case_study['url_key'] = $urlKey;
					
					$this->CaseStudy->save($case_study);
					$case_study['id'] = $this->CaseStudy->getInsertID();	
					//$this->CaseStudy->updateAll(array('CaseStudy.url_key'=>"'".$urlKey."'"),array('CaseStudy.id'=>$id));
					
					$this->redirect(array('controller' => 'members', 'action' => 'casestudy_preview',$case_study['id']));
			}
		} else {
			$this->redirect(array('controller' => 'fronts', 'action' => 'index'));
		}
	}
	
	function edit_casestudy($id=null,$action=null){
                  $this->layout= 'defaultnew';
		//if id is null. a new CS is being added
		//if not null then it is an edit
		if($this->Auth->user('id') != "" && $this->Auth->user('role_id') == 2 && $this->Auth->user('access_specifier') =='publish') {
			if(empty($this->data['CaseStudy']) == true) { //memeber has clicked on add new button
				if($id != null) { 
					if($action == 1) { //Publishing
						$case_study = $this->CaseStudy->find('first',array('conditions'=>array('CaseStudy.id' => $id)));
						$case_study['CaseStudy']['user_id'] = $this->Auth->user('id');
						if($case_study['CaseStudy']['url'] != '')
							$case_study['CaseStudy']['url'] = $this->General->addhttp($case_study['CaseStudy']['url']);
						$case_study['CaseStudy']['specifier'] = 'Publish';
						
						$random = substr(md5(uniqid()), 0, 5);
						
						$urlKey = preg_replace('/\PL/u', '-', $case_study['CaseStudy']['title']);
						$urlKey .= '-'.$random;
						
						$case_study['CaseStudy']['url_key'] = $urlKey;
						
						$this->CaseStudy->save($case_study['CaseStudy']);
						
						$this->redirect(array('controller' => 'users', 'action' => 'my_account#case_studies'));
					} else {
						$case_study = $this->CaseStudy->find('first',array('conditions'=>array('CaseStudy.id' => $id)));
						$this->set('case_study', $case_study);
					}
				}else{
					$this->redirect(array('controller' => 'users', 'action' => 'edit_account'));
				}
			} else { // member has clicked on save button
				$case_study = $this->data['CaseStudy'];
				$case_study['user_id'] = $this->Auth->user('id');
				if($case_study['CaseStudy']['url'] != '')
					$case_study['url'] = $this->General->addhttp($this->data['CaseStudy']['url']);
				
				$random = substr(md5(uniqid()), 0, 5);
				
				$urlKey = preg_replace('/\PL/u', '-', $case_study['title']);
				$urlKey .= '-'.$random;
				
				$case_study['url_key'] = $urlKey;
				
				$this->CaseStudy->save($case_study);

				$this->redirect(array('controller' => 'members', 'action' => 'casestudy_preview',$case_study['id']));
			}
		} else {
			$this->redirect(array('controller' => 'fronts', 'action' => 'index'));
		}
	}
	
	function delete_casestudy($id=null){
	
		//if id is null. a new CS is being added
		//if not null then it is an edit
		if($this->Auth->user('id') != "" && $this->Auth->user('role_id') == 2) {
			if($id != null) { // add new
				$this->CaseStudy->delete($id);
				$this->redirect(array('controller' => 'users', 'action' => 'edit_account','OpenCaseStudiesTab:'.true));
			}
		} else {
			$this->redirect(array('controller' => 'fronts', 'action' => 'index'));
		}
	}
	
	function casestudy_preview($id=null){



               $this->layout= 'defaultnew';


			$result = $this->Topic->find('all');
			$qcategories = array();
			foreach($result as $value){
				$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
			}
			$this->set("categories",$qcategories);


		$keywords = $this->Topic->find('all',array(
				'fields' => array('DISTINCT (Topic.autocomplete_text) AS autocomplete_text'),
				'conditions'=>array('Topic.isName'=>0)
		));

		$this->set('keywords',$keywords);
		
		$key = array();
		$cn = 0;
		foreach($keywords as $value){
			$key[$cn++] = $value['Topic']['autocomplete_text'];
		}
		$this->set('allKeywords', $key);

		
		if($this->Auth->user('id') != "" && $this->Auth->user('role_id') == 2 && $this->Auth->user('access_specifier') =='publish' || $this->Auth->user('role_id') == 1) {
			if($id != ''){
				$case_study = $this->CaseStudy->find('first',array('conditions'=>array('CaseStudy.id' => $id)));
                                $user = $this->User->findById($case_study['CaseStudy']['user_id']);

                                $this->set('user',$user);

				$this->set('case_study_session',$case_study);

		$dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($user['UserReference']['zipcode']))));
		$this->set("city",$dataZip['City']['city_name']);
		$this->set("state",$dataZip['City']['state']);

                       if($case_study['CaseStudy']['casestudy_url']==''){

  	               $urlKey = preg_replace('/\PL/u', '-', $case_study['CaseStudy']['title']);
		       $urlKey = $urlKey.'-'.$case_study['CaseStudy']['id']; 

	    	       $this->CaseStudy->updateAll(array('CaseStudy.casestudy_url'=>"'".$urlKey."'"),array('CaseStudy.id'=>$case_study['CaseStudy']['id']));
                       }

			}




		} else {
			$this->redirect(array('controller' => 'fronts', 'action' => 'index'));
		}
	}
	
	function my_badges(){

               $this->layout= 'defaultnew';
		
		if($this->Auth->user('id') != "" && $this->Auth->user('role_id') == 2) {

              $this->User->Behaviors->attach('Containable');
              $condionArr = array();
		$condionArr['User.id'] = $this->Auth->user('id');
              $condionArr['User.status'] = 1;
	       $condionArr['User.is_approved'] = 1;
		$condionArr['User.access_specifier'] = 'publish';
		$this->data = $this->User->find('first',
				array(
						'contain'=>array(
								'UserReference'=>array(
										'fields'=>array('first_name','last_name','linkedin_headline','business_website'),
								),
								'UserImage' ,
                                                        'Communication',
                                                        'Social'
								
						),
						'conditions'=>$condionArr,
				)

		);
			$this->data = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));

		
		$dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($this->data['UserReference']['zipcode']))));
		$this->set("city",$dataZip['City']['city_name']);
		$this->set("state",$dataZip['City']['state']);
              
		} else {
			$this->redirect(array('controller' => 'fronts', 'action' => 'index'));
		}
	}
	
	function __toggleDirectoryUserStatus($userId = null){
		
		$dUser = $this->DirectoryUser->find('first',array('conditions'=>array('DirectoryUser.user_id'=>$userId)));
		if($dUser['DirectoryUser']['hasPublished'] == 0) { //Publish it
			
			$dUser['DirectoryUser']['hasPublished'] = 1;
			$this->DirectoryUser->save($dUser);
			
			//Link feedback with new user
			$feedbacks = $this->Feedback->find('all',array('conditions'=>array('Feedback.d_user_id'=>$this->DirectoryUser->id)));
			
			foreach ($feedbacks as $value){
				
				$value['Feedback']['member_id'] = $userId;
				$this->Feedback->save($value);
				unset($this->Feedback->id);
			}
			
			//Link Consultations with new user
			$dConsultation = $this->Directory_consultations->find('all',array('conditions'=>array('Directory_consultations.d_user_id'=>$this->DirectoryUser->id)));
				
			foreach ($dConsultation as $value){
			
				$value['Directory_consultations']['member_id'] = $userId;
				$this->Directory_consultations->save($value);
				unset($this->Directory_consultations->id);
			}
			
		} else {
			
			$dUser['DirectoryUser']['hasPublished'] = 0;
			$this->DirectoryUser->save($dUser);
		}
		
	}
	
	function admin_directory_index() {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';
		if (!isset($this->request['named']['page'])) {
			$this->Session->delete('MentorDsearch');
                     $this->Session->delete('MentorDexpertise');
		}
		$filters[] = array('DirectoryUser.hasPublished' => 0);
              $filter[] = array('DirectoryUser.hasPublished' => 0);
		if (!empty($this->data)) {
			$this->Session->delete('MentorDsearch');
			$this->Session->delete('MentorDexpertise');
                     $keyword = $this->data['DirectoryUser']['first_name'];
			$expertise = $this->data['DirectoryUser']['area_of_expertise'];
                     if (!empty($this->data['DirectoryUser']['first_name'])) {
				//App::import('Sanitize');
				//$keyword = Sanitize::escape($this->data['DirectoryUser']['first_name']);
				$keyword = ($this->data['DirectoryUser']['first_name']);
                             $this->Session->write('MentorDsearch', $keyword);
                             
			}
                    if (!empty($this->data['DirectoryUser']['area_of_expertise']))	{	
                       // App::import('Sanitize');
                      // $expertise = Sanitize::escape($this->data['DirectoryUser']['area_of_expertise']);
                       $expertise = ($this->data['DirectoryUser']['area_of_expertise']);
                       $this->Session->write('MentorDexpertise', $expertise);
                    }

                }


 
		if ($this->Session->check('MentorDsearch')) {
			$first_name = array('DirectoryUser.first_name LIKE' => "%" . $this->Session->read('MentorDsearch') . "%");
			$last_name = array('DirectoryUser.last_name LIKE' => "%" . $this->Session->read('MentorDsearch') . "%");
			$filters[] = array('OR' => array($first_name, $last_name));
		}
              if ($this->Session->check('MentorDexpertise')) {
			$area_of_expertise = array('DirectoryUser.area_of_expertise LIKE' => "%" . $this->Session->read('MentorDexpertise') . "%");
                     $filter[] = array($area_of_expertise);

		}
              if ($this->Session->check('MentorDsearch')){  
              $this->Paginator->settings = array(
				'limit' => 50,
				'order' => array('DirectoryUser.id' => 'DESC'),
				'conditions' => $filters
		);
              $result = $this->Paginator->paginate('DirectoryUser');
              $this->set('data', $result);
              }
              else{ 
              $this->Paginator->settings = array(
				'limit' => 50,
				'order' => array('DirectoryUser.id' => 'DESC'),
				'conditions' => $filter
		);
		
              $results = $this->Paginator->paginate('DirectoryUser');
		
              $this->set('data', $results);
		}//prd($result);
              $this->set('title_for_layout', 'Directory User');
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}
	
	function admin_directory_edit($id = null) {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';
		$this->set('title_for_layout', 'Edit Directory User');
		if (!empty($this->data)) {
			//prd($this->data);
				
			$this->DirectoryUser->save($this->data); 
	
			$this->Session->setFlash('Member has been saved', 'admin_flash_good');
			$this->redirect(array('controller' => 'members','action' => 'admin_directory_index'));
		} else {
			$this->data = $this->DirectoryUser->read(null, $id);
		}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}
	
	function admin_directory_delete($id = null) {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';
		if (!$id) {
			$this->Session->setFlash('Invalid id for Directory user');
			$this->redirect($this->referer());
		}
		$admin = $this->DirectoryUser->read(null, $id);
		if (empty($admin)) {
			$this->Session->setFlash('Invalid Directory user Id', 'admin_flash_bad');
			$this->redirect($this->referer());
		}
	
		if ($this->DirectoryUser->delete($id)) {
			$this->Session->setFlash('Directory user has been deleted successfully', 'admin_flash_good');
			$this->redirect($this->referer());
		}
		$this->Session->setFlash('Directory user has not been deleted', 'admin_flash_bad');
		$this->redirect($this->referer());
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}
	
	function admin_directory_clean() {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';
		$publishedUsers = $this->User->find('all',array('conditions'=>array('User.access_specifier'=>'publish')));
		
		$changedEmails = array();
		$count = 0;
		foreach ($publishedUsers as $value){

			$username = $value['User']['username'];
			$dUser = $this->DirectoryUser->find('first',array('conditions'=>array('DirectoryUser.username'=>$username, 'DirectoryUser.hasPublished'=>0)));

			if(empty($dUser)) {
				foreach ($value['Social'] as $social) {
					
					$SocialName = $social['social_name'];
					$dUser = $this->DirectoryUser->find('first',array('conditions'=>array('DirectoryUser.linkedin_url'=>$SocialName, 'DirectoryUser.hasPublished'=>0)));
				}
			}
			
			if(empty($dUser) == false) {
				
				$dUser['DirectoryUser']['hasPublished'] = 1;
				$dUser['DirectoryUser']['user_id'] = $value['User']['id'];
				$this->DirectoryUser->save($dUser);
				unset($this->DirectoryUser->id);
				
				$changedEmails[$count++] = $username;
			}
		}
		//Generating random key for unsubscribe
		$dUsers = $this->DirectoryUser->find('all');
		foreach ($dUsers as $value) {
			
			if(isset($value['DirectoryUser']['random_key']) == false || $value['DirectoryUser']['random_key'] == -1) {
				
				$value['DirectoryUser']['random_key'] = substr(md5(uniqid()), 0, 20);
				$this->DirectoryUser->save($value['DirectoryUser']);
				unset($this->DirectoryUser->id);
			}
			
			//Saving new autocomplete expertise
			$stringExp = $value['DirectoryUser']['area_of_expertise'];
			$Exp3 = explode(',',$stringExp);
			$allExp = array();
			$count = 0;
				
			$addedExpertise = array();
			$aCount = 0;
			for($i=0;$i<count($Exp3);$i++){
			
				if(isset($Exp3[$i]) && $Exp3[$i] != '')
					$allExp[$count++] = ucfirst(trim($Exp3[$i]));
			}
				
			$uniqueAry = array_unique($allExp);
			
			foreach($uniqueAry as $exp){
			
				$match = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$exp)));
			
				if(empty($match)) {
					$this->Topic->create( );
					$this->Topic->save(array('autocomplete_text'=>$exp));
					$addedExpertise[$aCount++] = $exp;
					unset($this->Topic->id);
				}
			}
			
			//End Saving Autocomplete text
			
		}
		//End
		
		$this->set('changedEmails', $changedEmails);
		$this->set('addedExpertise', $addedExpertise);
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}
	
	function unsubscribe($email = null, $random_key = null)
	{
		if($email != null && $random_key != null) {
			$userData = $this->DirectoryUser->find('first', array('conditions' => array('DirectoryUser.username' => $email,'DirectoryUser.random_key' => $random_key)));
			if (isset($userData) && !empty($userData)){
				
				$this->set('id', $userData['DirectoryUser']['id']);
				//ask for confirmation
			}else {
				$this->Session->setFlash(__('Invalid unsubscribe link, please contact site administrator.', true), 'flash_bad');
				$this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
			}
		} else if(!empty($this->data)) {
			
			$selectedBox = $this->request->data['Role'];
			
			$userData = $this->DirectoryUser->find('first', array('conditions' => array('DirectoryUser.id' => $this->data['DirectoryUser']['id'])));
			
			if($selectedBox == 'Yes') {
    			
    			$userData['DirectoryUser']['sendEmail'] = 0;
    			
    		} else {
    			
    			$userData['DirectoryUser']['sendEmail'] = 1;
    		}
			
			$this->DirectoryUser->save($userData['DirectoryUser']);
			unset($this->DirectoryUser->id);
			
			$this->Session->setFlash(__('Unsubscribed.', true), 'flash_good');
			$this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
			
		} else {
			$this->Session->setFlash(__('Invalid unsubscribe link, please contact site administrator.', true), 'flash_bad');
			$this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
		}
	}
	
	function unsubscribe_qna($email = null)
	{
		if($email != null) {
			
			$userData = $this->User->find('first', array('conditions' => array('User.username' => $email)));
			if (isset($userData) && !empty($userData)){
	
				$this->set('id', $userData['User']['username']);
				//ask for confirmation
			}else {
				$this->Session->setFlash(__('Invalid unsubscribe link, please contact us at help@guild.im.', true), 'flash_bad');
				$this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
			}
		} else if(!empty($this->data)) {
				
			$selectedBox = $this->request->data['Role'];
				
			$userData = $this->User->find('first', array('conditions' => array('User.username' => $this->data['User']['id'])));
				
			$going = null;
			if($selectedBox == 'Yes') {
				 
				$userData['UserReference']['question_notification'] = 'N';
				 
			} else {
				 
				$userData['UserReference']['question_notification'] = 'Y';
				$going = "N";
			}
				
			$this->UserReference->save($userData['UserReference']);
			unset($this->UserReference->id);
				
			if($going != null) {
				$this->redirect(array('controller' => 'fronts', 'action' => 'index'));
			} else {
				$this->redirect(array('action' => 'unsubscribed'));
			}
				
		} else {
			$this->Session->setFlash(__('Invalid unsubscribe link, please contact us at help@guild.im.', true), 'flash_bad');
			$this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
		}
	}
	
	function unsubscribed(){
	}
	
	function admin_directory_add() {
		$this->layout = 'admin';
		if(!empty($this->data)){
			
			$this->DirectoryUser->save($this->data['DirectoryUser']);
			$this->Session->setFlash(__('Directory expert added.', true), 'flash_good');
			$this->redirect(array('action' => 'admin_directory_index'));
		}
	}


   	function pr(){
		 

                $this->layout= 'defaultnew';

              if(isset($this->request['named']['OpenSubscribePopup']) && $this->request['named']['OpenSubscribePopup'] !=''){
              	$this->set('OpenSubscribePopup',1);
              }

		if($this->Auth->user('role_id') == '2') {
	              $this->set("title_for_layout","PR");
			$mediaupdates = $this->MediaRecords->find('all',array('order'=>'deadline ASC'));
	
                    $this->set('mediaupdates', $mediaupdates);
              }
              else if($this->Auth->user('role_id') == '3'){
			$this->redirect(array('controller' => 'fronts', 'action' => 'index'));
               
         
		} else {
	              $this->set("title_for_layout","PR");
			$mediaupdates = $this->MediaRecords->find('all',array('order'=>'deadline ASC'));
		       $mediaUrl= SITE_URL.'members/pr';
                  if($this->Session->check('member_serviceUrl') && $this->Session->read('member_serviceUrl') != '')
                  {
                    $this->Session->delete('member_serviceUrl');
                  }
                  if($this->Session->check('flevyUrl') && $this->Session->read('flevyUrl') != '')
                  {
                    $this->Session->delete('flevyUrl');
                  }
                  if($this->Session->check('nlensesUrl') && $this->Session->read('nlensesUrl') != '')
                  {
                    $this->Session->delete('nlensesUrl');
                  }
                  if($this->Session->check('sservicesUrl') && $this->Session->read('sservicesUrl') != '')
                  {
                    $this->Session->delete('sservicesUrl');
                  }
		       $this->Session->write('mediaUrl',$mediaUrl);

                     $this->set('mediaupdates', $mediaupdates);

		}
	}


           function striperecurrcancel(){
		
		Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");
		
		$user = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
		
		$customer = Stripe_Customer::retrieve($user['User']['websitePaymentId']);
		
		$subscription = $customer->subscriptions;
		
		$subscription['data'][0]->cancel();
		
		$user['User']['premium_website'] = 0;
		$user['User']['websitePaymentId'] = "NULL";
		
		$this->User->save($user);
		
		//  sending  mail to client after publish profile 
  
              $mailData = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
                        
 

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "tim@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $iqbalemail = "iqbal@guild.im";
                           $hubspot ="5109946@bcc.hubspot.com";
                           $to = $this->Auth->user('username').";".$iqbalemail.";".$hubspot;
                           $subject = "Your Premium Website service has been canceled";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("cancel_premium_website");
                           $data .= "&merge_userfname=".urlencode($mailData['UserReference']['first_name']);
                           $data .= "&merge_link=".urlencode("members/website");
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }

		
		$this->Session->write('Auth', $user);
		//$this->Session->setFlash(__("Your Premium Website subscription has been canceled.", true), 'default', array('class' => 'success'));
		$this->redirect(array('controller'=>'members','action'=>'website'));
		
	}
	
	function striperecurr() {
 
               $this->layout= 'defaultnew';

               $user = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
	
		if(!empty($_POST['stripeToken'])) {
	
			// Set your secret key: remember to change this to your live secret key in production
			// See your keys here https://dashboard.stripe.com/account
			Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");
	
			// Get the credit card details submitted by the form
			$token = $_POST['stripeToken'];
				
			// Create the charge on Stripe's servers - this will charge the user's card
			try {
				$customer = Stripe_Customer::create(array(
					"card" => $token,
					"plan" => "plan_ELDm2Qae89CC7w",
					"email" => $this->Auth->user('username'))
				);
	
                       $user = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
				
				$user['User']['premium_website'] = 1;
				$user['User']['websitePaymentId'] = $customer['id'];
				
				$this->User->save($user);
                         
  

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "tim@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $iqbalemail = "iqbal@guild.im";
                           $hubspot ="5109946@bcc.hubspot.com";
                           $to = $user['User']['username'].";".$iqbalemail.";".$hubspot;
                           $subject = "Welcome to our Premium Website Service!";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("premium_website");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


				
				$this->Session->write('Auth', $user);
				//$this->Session->setFlash(__("You have subscribed our Premium Website service.", true), 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'members','action'=>'website'));
	
			} catch(Stripe_CardError $e) {
				pr("There is some error");
				prd($e);
				// The card has been declined
			}
		} else {
				
			$this->set("title_for_layout","Get your premium website");
			$this->User->Behaviors->attach('Containable');
			$this->data = $this->User->find('first',array('contain'=>array('UserReference'=>array('fields'=>array('zipcode','first_name','last_name','background_summary','area_of_expertise','application_5_fee')),'UserImage'),'conditions'=>array('role_id'=>2,'User.id'=>$this->Session->read('Auth.User.id'),'status'=>1)));			
		}
	}
	
	function stripecheckout($fee = null, $message = null) {
		
		if(!empty($_POST['stripeToken'])) {
		
			// Set your secret key: remember to change this to your live secret key in production
			// See your keys here https://dashboard.stripe.com/account
			Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");
		
			// Get the credit card details submitted by the form
			$token = $_POST['stripeToken'];
			$amount = $_POST['FEE'];

			// Create the charge on Stripe's servers - this will charge the user's card
			try {
				$charge = Stripe_Charge::create(array(
						"amount" => $amount, // amount in cents, again
						"currency" => "usd",
						"card" => $token,
						"description" => $this->Auth->user('email'))
				);
				
				//pr("Charged");
				//prd($charge);
				
				$this->redirect(array('controller'=>'pricing','action'=>'actionAfterPaymentSuccess'));
				
			} catch(Stripe_CardError $e) {
				pr("There is some error");
				prd($e);
				// The card has been declined
			}
		} else {
			
			$centFee = $fee * 100;
			
			$this->set('centFee', $centFee);
			$this->set('fee', $fee);
			$this->set('message', $message);
			
		}
		//$this->layout=false;
	}



      	function subscribe_to_premium(){
		
		$this->layout = 'ajax';
		$this->Session->write('popup', 1);
              
	}

       function checkbox_alert(){
		
		$this->layout = 'ajax';
		$this->Session->write('popup', 1);
              
	}

      function cancel_plan(){
			$this->layout= 'defaultnew';	
			$this->set("title_for_layout","Cancel premium website");
			$this->User->Behaviors->attach('Containable');
			$this->data = $this->User->find('first',array('contain'=>array('UserReference'=>array('fields'=>array('zipcode','first_name','last_name','background_summary')),'UserImage'),'conditions'=>array('role_id'=>2,'User.id'=>$this->Session->read('Auth.User.id'),'status'=>1)));			
		     }

      function flevy(){
			
                    $this->layout= 'defaultnew';	
		    $this->set("title_for_layout","Premium Templates by Flevy");
                  if($this->Session->check('member_serviceUrl') && $this->Session->read('member_serviceUrl') != '')
                  {
                    $this->Session->delete('member_serviceUrl');
                  }
                  if($this->Session->check('mediaUrl') && $this->Session->read('mediaUrl') != '')
                  {
                    $this->Session->delete('mediaUrl');
                  }
                  if($this->Session->check('nlensesUrl') && $this->Session->read('nlensesUrl') != '')
                  {
                    $this->Session->delete('nlensesUrl');
                  }
                  if($this->Session->check('sservicesUrl') && $this->Session->read('sservicesUrl') != '')
                  {
                    $this->Session->delete('sservicesUrl');
                  }
                     $flevyUrl= SITE_URL.'members/flevy';
                     $this->Session->write('flevyUrl',$flevyUrl);
		  }

      function flevy_email(){


         	$title = trim($this->request->data['title']);
    	       $member = trim($this->request->data['member']);
              $url = trim($this->request->data['url']);

              $this->User->Behaviors->attach('Containable');
              $condionArr = array();
		$condionArr['User.id'] = $member;
              $condionArr['User.status'] = 1;
	       $condionArr['User.is_approved'] = 1;
		$condionArr['User.access_specifier'] = 'publish';
		$mentor = $this->User->find('first',
				array(
						'contain'=>array(
								'UserReference'=>array(
										'fields'=>array('first_name','last_name'),
								),
								
								
						),
						'conditions'=>$condionArr,
				)

		);



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "dave@flevy.com";
                           $subject = "A Flevy document was downloaded";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("flevy_email_notification");
                           $data .= "&merge_consultantfname=".urlencode($mentor['UserReference']['first_name']);
                           $data .= "&merge_consultantlname=".urlencode($mentor['UserReference']['last_name']);
                           $data .= "&merge_consultantemail=".urlencode($mentor['User']['username']);
                           $data .= "&merge_documenttitle=".urlencode($title);
                           $data .= "&merge_consultanturlkey=".urlencode($mentor['User']['url_key']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

                     echo json_encode($url);
    		       $this->layout = '';
    		       $this->render(false);
    		       exit();

     }

      function ninelenses(){
               $this->layout= 'defaultnew';
				
		    $this->set("title_for_layout","Smarter Way to Engage Clients");
                  if($this->Session->check('member_serviceUrl') && $this->Session->read('member_serviceUrl') != '')
                  {
                    $this->Session->delete('member_serviceUrl');
                  }
                  if($this->Session->check('mediaUrl') && $this->Session->read('mediaUrl') != '')
                  {
                    $this->Session->delete('mediaUrl');
                  }
                  if($this->Session->check('flevyUrl') && $this->Session->read('flevyUrl') != '')
                  {
                    $this->Session->delete('flevyUrl');
                  }
                  if($this->Session->check('sservicesUrl') && $this->Session->read('sservicesUrl') != '')
                  {
                    $this->Session->delete('sservicesUrl');
                  }
                     $nlensesUrl= SITE_URL.'members/ninelenses';
                     $this->Session->write('nlensesUrl',$nlensesUrl);
		  }



    function member_register(){

              $this->layout= 'defaultnew2';
              $this->Session->write('popup', 1);

                if($this->Session->check('menteePopup')){
                $this->Session->delete('menteePopup');
                }
                if($this->Session->check('consultantPopup')){
                $this->Session->delete('consultantPopup');
                }
                if($this->Session->check('projectId')){
                $this->Session->delete('projectId');
                } 
                if($this->Session->check('questionId')){
                $this->Session->delete('questionId');
                } 
                if($this->Session->check('login_industry')){
                $this->Session->delete('login_industry');
                } 
                if($this->Session->check('mentor')){
                $this->Session->delete('mentor');
                }
                if($this->Session->check('duserid')){
                $this->Session->delete('duserid');
                } 
                if($this->Session->check('mentorshipId')){
                $this->Session->delete('mentorshipId');
                } 
                if($this->Session->check('directoryclaim')){
                $this->Session->delete('directoryclaim');
                }  

                $this->Session->write('consultantPopup', 1);
                $this->Session->write('invited_member',"invited_member");

               $this->set("title_for_layout","Register as a consultant");

              if(isset($this->params['named']['OpenMemberRegisterPopup']) && $this->params['named']['OpenMemberRegisterPopup'] !=''){
              	$this->set('OpenMemberRegisterPopup',1);
              }
              $member_serviceUrl= SITE_URL.'members/advantage';
		$this->Session->write('member_serviceUrl',$member_serviceUrl);

              $this->User->Behaviors->attach('Containable');
              $condionArr = array();
	      $condionArr['User.id'] = $this->Auth->user('id');
              $condionArr['User.status'] = 1;
	      $condionArr['User.is_approved'] = 1;
	      $condionArr['User.access_specifier'] = 'publish';
	      $this->data = $this->User->find('first',
				array(
						'contain'=>array(
								'UserReference'=>array(
										'fields'=>array('first_name','last_name','linkedin_headline','business_website'),
								),
								'UserImage' ,
                                                        'Communication',
                                                        'Social'
								
						),
						'conditions'=>$condionArr,
				)

		);
			$this->data = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));

		
		$dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($this->data['UserReference']['zipcode']))));
		$this->set("city",$dataZip['City']['city_name']);
		$this->set("state",$dataZip['City']['state']);


     }


    function stripeupdate(){

     $customer = $this->User->find('first', array('conditions' => array('User.id' => $this->Auth->user('id'))));

     $customer_id = $customer['User']['paypalRecurringId'];

     Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");

     if (isset($_POST['stripeToken'])){
     try {
    $cu = \Stripe\Customer::retrieve($customer_id); // stored in your application
    $cu->source = $_POST['stripeToken']; // obtained with Checkout
    $cu->save();
    
    $this->User->updateAll(array('User.paypalRecurringId'=>"'".$cu['id']."'"),array('User.id'=>$this->Auth->user('id')));
    $success = "Your card details have been updated!";
    }
    catch(\Stripe\Error\Card $e) {

    // Use the variable $error to save any errors
    // To be displayed to the customer later in the page
    $body = $e->getJsonBody();
    $err  = $body['error'];
    $error = $err['message'];
     }
  // Add additional error handling here as needed
      }

      
       else {
				
			$this->set("title_for_layout","Update your card details");
			$this->User->Behaviors->attach('Containable');
			$this->data = $this->User->find('first',array('contain'=>array('UserReference'=>array('fields'=>array('zipcode','first_name','last_name','background_summary','area_of_expertise','application_5_fee')),'UserImage'),'conditions'=>array('role_id'=>2,'User.id'=>$this->Session->read('Auth.User.id'),'status'=>1)));			
		}
}

    function report(){
      $this->layout = 'ajax';

      }
    function _getCityData($zipcode){
		$this->loadModel('City');
		$mentorCityArr = $this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($zipcode))));
		return $mentorCityArr['City'];
	}




    function customplan_request() {
    	 
    	if ($this->RequestHandler->isAjax()) {
    		Configure::write('debug', 0);
    			
    		$website = $this->request->data['website'];
    		$ebook = $this->request->data['ebook'];
    		$ebooknumber = $this->request->data['ebooknumber'];
                $larticles = $this->request->data['larticles'];
    		$larticlesnumber = $this->request->data['larticlesnumber'];
                $ssupport = $this->request->data['ssupport'];
                $dsmmgmt = $this->request->data['dsmmgmt'];
                $wsupport = $this->request->data['wsupport'];
    		$wsupportnumber = $this->request->data['wsupportnumber'];
                $blog = $this->request->data['blog'];
    		$blognumber = $this->request->data['blognumber'];
                $spoptimization = $this->request->data['spoptimization'];
                $dmarketing = $this->request->data['dmarketing'];
                $quoteamount = $this->request->data['quoteamount'];

   

                $userData = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
                   


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "ankit@guild.im";
                           $subject = "A new request for Customized Membership Plan";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("customized_membership_plan");
                           $data .= "&merge_fname=".urlencode($userData['UserReference']['first_name']);
                           $data .= "&merge_lname=".urlencode($userData['UserReference']['last_name']);
                           $data .= "&merge_urlkey=".urlencode($userData['User']['url_key']);
                           $data .= "&merge_website=".urlencode($website);
                           $data .= "&merge_ebook=".urlencode($ebook);
                           $data .= "&merge_ebooknumber=".urlencode($ebooknumber);
                           $data .= "&merge_larticles=".urlencode($larticles);
                           $data .= "&merge_larticlesnumber=".urlencode($larticlesnumber);
                           $data .= "&merge_ssupport=".urlencode($ssupport);
                           $data .= "&merge_dsmmgmt=".urlencode($dsmmgmt);
                           $data .= "&merge_wsupport=".urlencode($wsupport);
                           $data .= "&merge_wsupportnumber=".urlencode($wsupportnumber);
                           $data .= "&merge_blog=".urlencode($blog);
                           $data .= "&merge_blognumber=".urlencode($blognumber);
                           $data .= "&merge_spoptimization=".urlencode($spoptimization);
                           $data .= "&merge_dmarketing=".urlencode($dmarketing);
                           $data .= "&merge_quoteamount=".urlencode($quoteamount);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


		echo json_encode($id);
    		$this->render(false);
    		exit();
    	}
    }





         function case_studies($category = null){
                     $this->layout= 'defaultnew';

                     $this->set("title_for_layout","Members Casestudies");

			//GetCasestudy topics


			$result = $this->Topic->find('all');
			$qcategories = array();
			foreach($result as $value){
				$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
			}
			$this->set("categories",$qcategories);



                        $casestudies_topics = $this->CasestudyTopic->find('all', array('fields'=>'DISTINCT topic_id'));

                   
                        $ctopiclist = array();

			foreach($casestudies_topics as $key=>$aValue){
				
				$ctopiclist[$key] = $qcategories[$aValue['CasestudyTopic']['topic_id']];
			}

                        $this->set("topiclist",$ctopiclist);

                       if($category == null){

                       $casestudyData = $this->CaseStudy->find('all',array('conditions'=>array('CaseStudy.specifier'=>"Publish"),'order'=>'CaseStudy.modified desc'));

                        $casestudyArray = array();
                        $count = 0;
                        $total1 = count($casestudyData);
                        $qCount1 = 0;

			$this->User->Behaviors->attach('Containable');
			foreach($casestudyData as $aValue){
					if($qCount1 < $total1){

			
					$userData = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name','linkedin_headline'),
											),
											'UserImage' ,
									),
									'conditions'=>array('User.id'=>$aValue['CaseStudy']['user_id'])
							)
			
					);
						
					if(empty($userData['UserImage']) == false)
				        $aValue['User']['user_image'] = $userData['UserImage'][0]['image_name'];
					$aValue['User']['first_name'] = $userData['UserReference']['first_name'];
					$aValue['User']['last_name'] = $userData['UserReference']['last_name'];
                                        $aValue['User']['headline'] = $userData['UserReference']['linkedin_headline'];
					$aValue['User']['url_key'] = $userData['User']['url_key'];
				        $qCount1++;
					$casestudyArray[$count++] = $aValue;
			}
                   }

                                  $this->set("allcasestudies", $casestudyArray);
                                  $title = 'All Case Studies';
                                  $this->set('queryString',$title );



                      }else{

                           $keyword = $category;
                           if(false !== stripos($category,"_and"))
                           $keyword = str_replace("_and","&",$category);

                           if(false !== stripos($category,"_slash"))
                           $keyword = str_replace("_slash","/",$category);

                           $keyword = str_replace("-"," ",$keyword);

                           if(false !== stripos($keyword,"Non profit"))
                           $keyword = str_replace("Non profit","Non-profit",$keyword);

                           if(false !== stripos($keyword,"Cross cultural practices"))
                           $keyword = str_replace("Cross cultural practices","Cross-cultural practices",$keyword);

                           if(false !== stripos($keyword,"Post merger integration (PMI)"))
                           $keyword = str_replace("Post merger integration (PMI)","Post-merger integration (PMI)",$keyword);

		           $topic = $this->Topic->find('first', array('conditions' => array('Topic.autocomplete_text' => $keyword)));
                    
                           $id = $topic['Topic']['id'];

			   $CasestudisCs = $this->CasestudyTopic->find('all', array('conditions' => array('CasestudyTopic.topic_id' => $id), 'order'=>'CasestudyTopic.id desc'));

                          $total = count($CasestudisCs);
                                
			  $casestudyArray = array();
                          $datetime = array();

                          $cCount = 0;
                          $count = 0;

			  foreach($CasestudisCs as $aValue){
                          if($cCount < $total){

                          $casestudies = $this->CaseStudy->find('first', array('conditions' => array('CaseStudy.id' => $aValue['CasestudyTopic']['case_study_id']), 'order'=>'CaseStudy.modified desc'));
                           if($casestudies['CaseStudy']['specifier'] =="Publish"){
					$userData = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name','linkedin_headline'),
											),
											'UserImage' ,
									),
									'conditions'=>array('User.id'=>$casestudies['CaseStudy']['user_id'])
							)
			
					);
						
					if(empty($userData['UserImage']) == false)
				        $casestudies['User']['user_image'] = $userData['UserImage'][0]['image_name'];
					$casestudies['User']['first_name'] = $userData['UserReference']['first_name'];
					$casestudies['User']['last_name'] = $userData['UserReference']['last_name'];
                                        $casestudies['User']['headline'] = $userData['UserReference']['linkedin_headline'];
					$casestudies['User']['url_key'] = $userData['User']['url_key'];
                                        $casestudies['User']['time'] = strtotime($casestudies['CaseStudy']['modified']);
                                        $cCount++;
                                        $datetime[$count++] = strtotime($casestudies['CaseStudy']['modified']);
                                        $casestudyArray[$count++] = $casestudies;
                                       

                        }
                                 
                       }
                        
                       }

                                  $data = Set::sort($casestudyArray, '{n}.User.time', 'desc');
			          $this->set("allcasestudies", $data);
                                  $title = 'Case studies for "'.$keyword.'"';
                                  $this->set('queryString',$title );


 
         }
           

        }

     
        function casestudy($url = null){

        $this->layout= 'defaultnew';
        $casestudy = $this->CaseStudy->find('first', array('conditions' => array('CaseStudy.casestudy_url' => $url)));
        if($casestudy['CaseStudy']['id'] != ''){

        $this->set("title_for_layout",$casestudy['CaseStudy']['title']);

			//GetCasestudy topics
			$result = $this->Topic->find('all');
			$qcategories = array();
			foreach($result as $value){
				$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
			}
			$this->set("categories",$qcategories);

                     $user = $this->User->findById($casestudy['CaseStudy']['user_id']);
                     $this->set('casestudy',$casestudy);
                     $this->set('user',$user);

		     $this->loadModel('City');

                     if($this->Auth->user('id') == $casestudy['CaseStudy']['user_id'] || $this->Auth->user('role_id') ==1){
                     $edit ="yes";
                     $this->set('edit',$edit);
                     }

		$dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($user['UserReference']['zipcode']))));
		$this->set("city",$dataZip['City']['city_name']);
		$this->set("state",$dataZip['City']['state']);
         }else{

           $this->redirect(array('controller' => 'members', 'action' => 'case_studies'));

          }

      }  


        function casestudy_pdf_preview($casestudyid)
        {


                        $this->layout= 'defaultnew';

          
                        $casestudydata = $this->CaseStudy->find('first', array('conditions' => array('CaseStudy.id' => $casestudyid)));

                        $casestudydata['CaseStudy']['title'] = $casestudydata['CaseStudy']['title'];
                        $casestudydata['CaseStudy']['client_details'] = $casestudydata['CaseStudy']['client_details'];
                        $casestudydata['CaseStudy']['situation'] = $casestudydata['CaseStudy']['situation'];
                        $casestudydata['CaseStudy']['actions'] = $casestudydata['CaseStudy']['actions']; 
                        $casestudydata['CaseStudy']['result'] = $casestudydata['CaseStudy']['result']; 

  


                     $user = $this->User->findById($casestudydata['CaseStudy']['user_id']);
                     $this->set('casestudydata',$casestudydata);
                     $this->set('user',$user);



		$this->loadModel('City');
		$dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($user['UserReference']['zipcode']))));
		$this->set("city",$dataZip['City']['city_name']);
		$this->set("state",$dataZip['City']['state']);

                $this->set('pdf_preview_data', $casestudydata);
           	$this->Session->write('pdf_preview_data', $casestudydata);
           	$this->Session->write('user', $user);
           	$this->Session->write('city', $dataZip['City']['city_name']);
           	$this->Session->write('state', $dataZip['City']['state']);

        }
        
        function preview_pdf($data = null)
        {
        	$this->layout = 'pdf';
        	$pdf_preview_data = $this->Session->read('pdf_preview_data');

        	$user = $this->Session->read('user');
        	$city = $this->Session->read('city');
        	$state = $this->Session->read('state');


			$this->response->header(array('Content-type: application/pdf')); 
			$this->response->type('pdf'); 
        	$this->set('casestudydata',$pdf_preview_data);
        	$this->set('user',$user);
        	$this->set('city',$city);
        	$this->set('state',$state);
        }

        function preview_pdf1($data = null)
        {
        	$this->layout = 'pdf';
        	$pdf_preview_data = $this->Session->read('pdf_preview_data');

        	$user = $this->Session->read('user');
        	$city = $this->Session->read('city');
        	$state = $this->Session->read('state');


			$this->response->header(array('Content-type: application/pdf')); 
			$this->response->type('pdf'); 
        	$this->set('casestudydata',$pdf_preview_data);
        	$this->set('user',$user);
        	$this->set('city',$city);
        	$this->set('state',$state);
        }


    function finalize_casestudy($id = null){

    	if(!(empty($this->data))){
    		
              	    $casestudy = $this->CaseStudy->find('first', array('conditions' => array('CaseStudy.id' => $id)));


                        $value = "Publish";
	    	        $this->CaseStudy->updateAll(array('CaseStudy.specifier'=>"'".$value."'"),array('CaseStudy.id'=>$casestudy['CaseStudy']['id']));
                        $this->CasestudyTopic->deleteAll(array('CasestudyTopic.case_study_id'=>$casestudy['CaseStudy']['id']));
			$checkBoxes = $this->data['CaseStudy']['select'];

			$cat = explode(',',$checkBoxes);
                        $cat = array_unique($cat);

			for($i=0;$i<3;$i++){
			
				if(isset($cat[$i]) && $cat[$i] != '' && $cat[$i] != -1) {
					
					$topic = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$cat[$i])));
		               if(empty($topic)) {
			
                                $this->Topic->create( );
				    $this->Topic->save(array('autocomplete_text'=>$cat[$i]));

                                   $topicid = $this->Topic->getLastInsertID();
                                   $this->Topic->updateAll(array('Topic.topic_subset' => 0),array('Topic.id ' => $topicid));
                                   $topic = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topicid)));
				    
				}
					
					$casestudy_topic['case_study_id'] = $casestudy['CaseStudy']['id'];
					$casestudy_topic['topic_id'] = $topic['Topic']['id'];
					
					$this->CasestudyTopic->save($casestudy_topic);
					$this->CasestudyTopic->id = '';
					$this->Topic->id = '';
				}
			}
			


		    if($this->Auth->user('role_id') == 1){

                    $this->Session->setFlash('Proposal has been updated', 'admin_flash_good');
                    $this->redirect(array('action' => 'admin_casestudyreview'));
                    }


                                $this->redirect(array('controller' => 'users', 'action' => 'my_account#case_studies'));



    	} else {
    		$this->redirect(array('controller'=>'fronts','action'=>'index'));
    	}



   }

	function add_testimonial(){

		if($this->Auth->user('id') != "" && $this->Auth->user('role_id') == 2) {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);

                                $final_testimonial = $this->request->data['final_testimonial'];
                                $client_name = $this->request->data['client_name'];
                                $client_designation = $this->request->data['client_designation'];
                                $client_organization = $this->request->data['client_organization'];



				

				
                                $testimonial['user_id'] = $this->Auth->user('id');
                                $testimonial['testimonial_detail'] = $final_testimonial;
                                $testimonial['client_name'] = $client_name;
                                $testimonial['client_designation'] = $client_designation;
                                $testimonial['client_organization'] = $client_organization;
                                $this->Testimonial->save($testimonial);
                                           
	
				$this->layout = '';
				$this->render(false);
			        echo json_encode(array('id'=>$this->Testimonial->id));
				exit();
			}
		}
		
	}

	function edit_testimonial(){

		if($this->Auth->user('id') != "" && $this->Auth->user('role_id') == 2) {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);
                                $id = $this->request->data['id'];
                                $final_testimonial = $this->request->data['final_testimonial'];
                                $client_name = $this->request->data['client_name'];
                                $client_designation = $this->request->data['client_designation'];
                                $client_organization = $this->request->data['client_organization'];


                                $this->Testimonial->delete($id);
				
				
                                $testimonial['user_id'] = $this->Auth->user('id');
                                $testimonial['testimonial_detail'] = $final_testimonial;
                                $testimonial['client_name'] = $client_name;
                                $testimonial['client_designation'] = $client_designation;
                                $testimonial['client_organization'] = $client_organization;
                                $this->Testimonial->save($testimonial);
                                           
	
				$this->layout = '';
				$this->render(false);
			        echo json_encode(array('id'=>$this->Testimonial->id));
				exit();
			}
		}


	}

    function get_testimonial() {
    	 
    	$testimonial_id = trim($this->request->data['testimonial_id']);
    	 
    	if (!empty($testimonial_id)) {
    
    		$testimonial = $this->Testimonial->find('first',array('conditions'=>array('Testimonial.id' => $testimonial_id)));

               if(strpos($testimonial['Testimonial']['testimonial_detail'], 'contenteditable="true"')== false){
               $final = '<div class="ql-editor ql-blank" contenteditable="true">'.$testimonial['Testimonial']['testimonial_detail'].'</div>';
                }else{
                 $final = $testimonial['Testimonial']['testimonial_detail'];
    		 }   		 
    		echo json_encode(array('id'=>$testimonial_id,'testimonial_detail'=>$final,'client_name'=>$testimonial['Testimonial']['client_name'],'client_designation'=>$testimonial['Testimonial']['client_designation'],'client_organization'=>$testimonial['Testimonial']['client_organization']));
    		$this->layout = '';
    		$this->render(false);
    		exit();
    
    	}
    }

    function del_testimonial() {
    	 
    	$testimonial_id = trim($this->request->data['testimonial_id']);
    	 
    	if (!empty($testimonial_id)) {
    
    		$this->Testimonial->delete($testimonial_id);
    		    		 
    		echo json_encode(array('id'=>$testimonial_id));
    		$this->layout = '';
    		$this->render(false);
    		exit();
    
    	}
    }

         function loggedin_service_request(){

    			
    		$loggedUserId = $this->request->data['loggedUserId'];

                $user = $this->User->find('first', array('conditions' => array('User.id' => $loggedUserId)));

		$prospect['Prospect']['firstname'] = $user['UserReference']['first_name'];
		$prospect['Prospect']['lastname'] = $user['UserReference']['last_name'];
		$prospect['Prospect']['email'] = $user['User']['username'];
		$prospect['Prospect']['company'] = "GUILD Member";
		$prospect['Prospect']['phone'] = "xxxxx";
                $name = $user['UserReference']['first_name'].' '.$user['UserReference']['last_name'];
		$prospect['Prospect']['prospect_type'] = trim($this->request->data['purpose']);
		
		$this->Prospect->save($prospect);
              
                $organization = "Guild Member";


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "bethaney@guild.im";
                           $subject = "Interest Request for GUILD Service";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("guild_premium_request");
                           $data .= "&merge_username=".urlencode($name);
                           $data .= "&merge_useremail=".urlencode($user['User']['username']);
                           $data .= "&merge_userpurpose=".urlencode($this->request->data['purpose']);
                           $data .= "&merge_userphone=".urlencode("Guild Profile");
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

		echo json_encode($loggedUserId);
    		$this->render(false);
    		exit();


          


         }


    function service_request(){


		$prospect['Prospect']['firstname'] = trim($this->request->data['name']);
		$prospect['Prospect']['lastname'] = "";
		$prospect['Prospect']['email'] = trim($this->request->data['email']);
 
		$prospect['Prospect']['company'] = "Visitor";
		$prospect['Prospect']['phone'] = trim($this->request->data['phone']);

		$prospect['Prospect']['prospect_type'] = trim($this->request->data['purpose']);
		
		$this->Prospect->save($prospect);
              

                         
			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "bethaney@guild.im";
                           $subject = "Interest Request for GUILD Service";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("guild_premium_request");
                           $data .= "&merge_username=".urlencode($this->request->data['name']);
                           $data .= "&merge_useremail=".urlencode($this->request->data['email']);
                           $data .= "&merge_userpurpose=".urlencode($this->request->data['purpose']);
                           $data .= "&merge_userphone=".urlencode($this->request->data['phone']);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
                      

    		echo json_encode(array('id'=>"1"));
    		$this->layout = '';
    		$this->render(false);
    		exit();

                 
  
  }


        function admin_casestudyreview(){


      	if($this->Auth->user('id') == 1){  
	$this->layout = 'admin';  
    	$caseStudies = $this->CaseStudy->find('all',array('order'=>'CaseStudy.modified desc'));

    	$this->set('caseStudies', $caseStudies);
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }


       }


       function casestudy_adminedit($id = null){


                  $this->layout= 'defaultnew';

		if($this->Auth->user('id') != "" && $this->Auth->user('role_id') == 1) {

		     if($id != null) { 

						$case_study = $this->CaseStudy->find('first',array('conditions'=>array('CaseStudy.id' => $id)));
						$this->set('case_study', $case_study);
		     }

			else if(!(empty($this->data))){ // member has clicked on save button
				$case_study = $this->data['CaseStudy'];


				
				$this->CaseStudy->save($case_study);

				$this->redirect(array('controller' => 'members', 'action' => 'casestudy_preview',$case_study['id']));
			}
		} else {
			$this->redirect(array('controller' => 'fronts', 'action' => 'index'));
		}



            }

    function admin_deletecasestudy($id = null){
    	if($this->Auth->user('id') == 1){    
		$this->layout = 'admin';  
    	if($this->CaseStudy->delete($id)) {
    
    		$casestudyTopic = $this->CasestudyTopic->find('all', array('conditions' => array('CasestudyTopic.case_study_id' => $id)));
    		 
    		foreach ($casestudyTopic as $topics) {
    		
    			$this->CasestudyTopic->delete($topics['CasestudyTopic']['id']);
    		}
    		
    		$this->Session->setFlash('Case study has been deleted', 'admin_flash_good');
    		$this->redirect(array('action' => 'admin_casestudyreview'));
    	} else {
    		$this->Session->setFlash('Proposal could not be deleted', 'admin_flash_bad');
    	}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }

	
	function save_draft_profile(){

		
		if($this->Auth->user('id') != "") {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('debug', 0);
				$fname = ucfirst($this->request->data['fname']);
				$lname = ucfirst($this->request->data['lname']);
				$zipcode = $this->request->data['zipcode'];
                                $profileheadline= $this->request->data['profileheadline'];
				$professionalsummary = $this->request->data['professionalsummary'];
                                $pastclient= $this->request->data['pastclient'];
                                $websiteaddress= $this->request->data['websiteaddress'];                          
                                $availabititytime= $this->request->data['availabititytime'];
				$engfee= $this->request->data['engfee'];
				$engper= $this->request->data['engper'];
				$engoverview= $this->request->data['engoverview'];
                                $topics = $this->request->data['topics'];
                                //echo(count($topics));
				$sociallink1= $this->request->data['sociallink1'];
				$sociallink2= $this->request->data['sociallink2'];
				$sociallink3= $this->request->data['sociallink3'];
				$sociallink4= $this->request->data['sociallink4'];
				$sociallink5= $this->request->data['sociallink5'];
				$sociallink6= $this->request->data['sociallink6'];
				$sociallink7= $this->request->data['sociallink7'];
				$communicationcheckbox1= $this->request->data['communicationcheckbox1'];
				$communication1= $this->request->data['communication1'];
				$communicationcheckbox2= $this->request->data['communicationcheckbox2'];
				$communication2= $this->request->data['communication2'];
				$communicationcheckbox3= $this->request->data['communicationcheckbox3'];
				$communication3= $this->request->data['communication3'];
				$UserReferenceIndustry1= $this->request->data['UserReferenceIndustry1'];
				$UserReferenceCategory1= $this->request->data['UserReferenceCategory1'];
				$UserReferenceIndustry2= $this->request->data['UserReferenceIndustry2'];
				$UserReferenceCategory2= $this->request->data['UserReferenceCategory2'];
				$UserReferenceIndustry3= $this->request->data['UserReferenceIndustry3'];
				$UserReferenceCategory3= $this->request->data['UserReferenceCategory3'];

				
				$mentor_data = $this->User->find('first',
						array(
							'contain'=>array(
								'UserReference'=>array(
									'fields'=>array('first_name','last_name','background_summary','accept_application','area_of_expertise','desire_mentee_profile','fee_first_hour','fee_regular_session','created','zipcode','application_5_fee','paypal_account','linkedin_headline','past_clients','business_website','feeNSession'),
								),
								'UserImage',
								
								'Communication',
								'Availablity',
								'Social',
								
							),
							'conditions'=>array('User.id'=>$this->Auth->user('id'))		
						)
				
					);
				
				$mentor_data['UserReference']['first_name']			=	$fname;
				$mentor_data['UserReference']['last_name']			=	$lname;
				$mentor_data['UserReference']['zipcode']			=	$zipcode;
                $mentor_data['UserReference']['linkedin_headline']    =   $profileheadline;
				$mentor_data['UserReference']['background_summary'] = $professionalsummary;
                $mentor_data['UserReference']['past_clients']    =   $pastclient;
                $mentor_data['UserReference']['business_website']    =   $websiteaddress;
				$mentor_data['UserReference']['fee_first_hour']    =   0;
				$mentor_data['UserReference']['fee_regular_session']    =   $engfee;
				$mentor_data['UserReference']['	regular_session_per']    =   $engper;
				$mentor_data['UserReference']['feeNSession']    =   $engoverview;
				$this->UserReference->save($mentor_data['UserReference'], false);
                                           
                                        if($availabititytime !='' && $availabititytime !='undefined'){
					$mentor_data['Availablity']['user_id'] = $this->Auth->user('id');
					$mentor_data['Availablity']['day_time'] = $availabititytime;
					$this->Availablity->save($mentor_data['Availablity'], false);
                                        }



    		      $sociallinks = $this->Social->find('all', array('conditions' => array('Social.user_id' => $this->Auth->user('id'))));
    		 
    		        foreach ($sociallinks as $links) {
    		
    			         $this->Social->delete($links['Social']['id']);
    		      }


                                        if($sociallink1 !='' && $sociallink1 !='undefined'){

                                          $mentor_data['Social']['user_id'] = $this->Auth->user('id');
					                      $mentor_data['Social']['social_name']	= $sociallink1;
					                      $this->Social->save($mentor_data['Social'], false);
                                                              $this->Social->id ='';
                                        } 
                                        if($sociallink2 !='' && $sociallink2 !='undefined'){

                                          $mentor_data['Social']['user_id'] = $this->Auth->user('id');
					                      $mentor_data['Social']['social_name']	= $sociallink2;
					                      $this->Social->save($mentor_data['Social'], false);
                                                              $this->Social->id ='';
                                        } 
                                        if($sociallink3 !='' && $sociallink3 !='undefined'){

                                          $mentor_data['Social']['user_id'] = $this->Auth->user('id');
					                      $mentor_data['Social']['social_name']	= $sociallink3;
					                      $this->Social->save($mentor_data['Social'], false);
                                                              $this->Social->id ='';
                                        } 
                                        if($sociallink4 !='' && $sociallink4 !='undefined'){

                                          $mentor_data['Social']['user_id'] = $this->Auth->user('id');
					                      $mentor_data['Social']['social_name']	= $sociallink4;
					                      $this->Social->save($mentor_data['Social'], false);
                                                              $this->Social->id ='';
                                        } 
                                        if($sociallink5 !='' && $sociallink5 !='undefined'){

                                          $mentor_data['Social']['user_id'] = $this->Auth->user('id');
					                      $mentor_data['Social']['social_name']	= $sociallink5;
					                      $this->Social->save($mentor_data['Social'], false);
                                                              $this->Social->id ='';
                                        } 




    		      $communicationlinks = $this->Communication->find('all', array('conditions' => array('Communication.user_id' => $this->Auth->user('id'))));
    		 
    		        foreach ($communicationlinks as $links) {
    		
    			         $this->Communication->delete($links['Communication']['id']);
    		      }


                                        if($communicationcheckbox1 !='' && $communicationcheckbox1 !='undefined' && $communication1!='undefined'){

                                          $mentor_data['Communication']['user_id'] = $this->Auth->user('id');
					                      $mentor_data['Communication']['mode_type']	= $communicationcheckbox1;
					                      $mentor_data['Communication']['mode']	=	$communication1;
					                      $this->Communication->save($mentor_data['Communication'],false);
                                                              $this->Communication->id ='';
                                        }  

                                        if($communicationcheckbox2 !=''&& $communicationcheckbox2 !='undefined'  && $communication2!='undefined'){

                                           $mentor_data['Communication']['user_id'] = $this->Auth->user('id');
					                       $mentor_data['Communication']['mode_type']	=	$communicationcheckbox2;
					                       $mentor_data['Communication']['mode']	=	$communication2;
					                       $this->Communication->save($mentor_data['Communication'],false);
                                                               $this->Communication->id ='';
                                          }

                                        if($communicationcheckbox3 !=''&& $communicationcheckbox3 !='undefined' && $communication3!='undefined'){

                                           $mentor_data['Communication']['user_id'] = $this->Auth->user('id');
					                       $mentor_data['Communication']['mode_type']	=	$communicationcheckbox3;
					                       $mentor_data['Communication']['mode']	=	$communication3;
					                       $this->Communication->save($mentor_data['Communication'],false);
                                          }



				

				//End Saving Autocomplete text
				

			
				$memberIndustryCategory = $this->MemberIndustryCategory->find('first',array('conditions'=>array('MemberIndustryCategory.user_id'=>$this->Auth->user('id'))));
				
				if(empty($memberIndustryCategory)) {
					$memberIndustryCategory['MemberIndustryCategory']['user_id'] = $this->Auth->user('id');
					$memberIndustryCategory['MemberIndustryCategory']['category1'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['category2'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['category3'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['industry1'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['industry2'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['industry3'] = -1;
				}


				//Saving Expertise for Search Autocomplete
				$topic_arr = array(-1,-1,-1,-1,-1);
				//$stringExp = $topics;
				//$Exp3 = explode(',',$stringExp);
				//$allExp = array();
				$count = 0;
					
				for($i=0;$i<5;$i++){
				
					if(isset($topics[$i]) && $topics[$i] != '' && $topics[$i] != -1)
						$allExp[$count++] = ucfirst(trim($topics[$i]));
				}
					
				$uniqueAry = array_unique($allExp);
				
				$count = 0;
				
				foreach($uniqueAry as $exp){
						
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$exp)));
					
					$topic_arr[$count] = $match['Topic']['id'];
                                        if($match['Topic']['topic_subset'] == 0)
                                        {
                                        $this->Topic->updateAll(array('Topic.topic_subset' => 1),array('Topic.id ' => $match['Topic']['id']));

                                        }
					
					if(empty($match)) {
						$this->Topic->create( );
						$this->Topic->save(array('autocomplete_text'=>$exp));

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "deshmukh@guild.im";
                           $subject = "New Topic has just added in a profile";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("new_topic_addition");
                           $data .= "&merge_userfname=".urlencode($fname);
                           $data .= "&merge_userlname=".urlencode($lname);
                           $data .= "&merge_newtopic=".urlencode($exp);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End




						$topic_arr[$count] = $this->Topic->id;
						unset($exp);
					}
					$count++;
				}


				$memberIndustryCategory['MemberIndustryCategory']['topic1'] = $topic_arr[0];
				$memberIndustryCategory['MemberIndustryCategory']['topic2'] = $topic_arr[1];
				$memberIndustryCategory['MemberIndustryCategory']['topic3'] = $topic_arr[2];
				$memberIndustryCategory['MemberIndustryCategory']['topic4'] = $topic_arr[3];
				$memberIndustryCategory['MemberIndustryCategory']['topic5'] = $topic_arr[4];
				

				//End Saving Autocomplete text


				
				if($UserReferenceIndustry1 != -1) {
					$industry_obj1 = $this->IndustryCategory->find('first',array('conditions'=>array('IndustryCategory.category'=>$UserReferenceIndustry1)));

					$cat_list = $this->IndustryCategory->find('all',array('conditions'=>array('IndustryCategory.category'=>$UserReferenceCategory1)));
					
					foreach ($cat_list as $key => $value) {

						if($value['IndustryCategory']['parent'] == $industry_obj1['IndustryCategory']['id']) {
							$memberIndustryCategory['MemberIndustryCategory']['industry1'] = $value['IndustryCategory']['parent'];
							$memberIndustryCategory['MemberIndustryCategory']['category1'] = $value['IndustryCategory']['id'];
						}
					}
				}

				if($UserReferenceIndustry2 != -1){
					$industry_obj2 = $this->IndustryCategory->find('first',array('conditions'=>array('IndustryCategory.category'=>$UserReferenceIndustry2)));

					$cat_list = $this->IndustryCategory->find('all',array('conditions'=>array('IndustryCategory.category'=>$UserReferenceCategory2)));
					
					foreach ($cat_list as $key => $value) {

						if($value['IndustryCategory']['parent'] == $industry_obj2['IndustryCategory']['id']) {
							$memberIndustryCategory['MemberIndustryCategory']['industry2'] = $value['IndustryCategory']['parent'];
							$memberIndustryCategory['MemberIndustryCategory']['category2'] = $value['IndustryCategory']['id'];
						}
					}
				}

				if($UserReferenceIndustry3 != -1) {
					$industry_obj3 = $this->IndustryCategory->find('first',array('conditions'=>array('IndustryCategory.category'=>$UserReferenceIndustry3)));

					$cat_list = $this->IndustryCategory->find('all',array('conditions'=>array('IndustryCategory.category'=>$UserReferenceCategory3)));

					foreach ($cat_list as $key => $value) {

						if($value['IndustryCategory']['parent'] == $industry_obj3['IndustryCategory']['id']) {
							$memberIndustryCategory['MemberIndustryCategory']['industry3'] = $value['IndustryCategory']['parent'];
							$memberIndustryCategory['MemberIndustryCategory']['category3'] = $value['IndustryCategory']['id'];
						}
					}
				}

				$this->MemberIndustryCategory->save($memberIndustryCategory);
				//$this->MemberIndustryCategory->id = null;
				
				//End -- Saving Industry Categories


	
				$this->layout = '';
				$this->render(false);
			        echo json_encode(array('value' => $count));
				exit();
			}
		}

	}

    function checkUsername(){
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $email = $this->request->data['email'];
			

				$cond = array('User.username' =>trim($email));
            
			        $count = $this->User->find('count', array('conditions' => $cond));
            
          
            $this->layout = '';
            $this->render(false);
            echo json_encode(array('value' => $count));
            exit();
        }   	
    	
    }

	function logo_upload($id = null){

		 Configure::write('debug', 2);
		 $this->layout = '';
		 $this->render(false);
		 App::import('Component', 'FileUploader');
                 $role = 3;
		 $FileUploader = new FileUploaderComponent();
		 $result = $FileUploader->logo_upload($id,$role);
		 if(isset($result['success']) && $result['success']==true){
			

			$mentor_data = $this->User->find('first',
						array(
							'contain'=>array(
								'UserReference'=>array(
									'fields'=>array('client_companylogourl'),
								),
								
							),
							'conditions'=>array('User.id'=>$id)		
						)
				
					);
				
				$mentor_data['UserReference']['client_companylogourl']			=	$result['filename'];

				$this->UserReference->save($mentor_data['UserReference'], false);


		 }
                   echo json_encode(array('filepath' => $result['filename']));
		 //echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
	}

        function blog($url){
              if($url == "The-Technical-Elements-Every-Modern-Website-Needs"){
              $this->layout= 'defaultnew2';
              $this->Session->write('popup', 1);
              $this->set('title_for_layout','The Technical Elements Every Modern Website Needs');
              }else {
			$this->redirect(array('controller'=>'staticPages','action'=>'page_not_found'));
		}



     }

function image_count(){

          $this->User->unbindModel(array('hasOne'=>array('Availablity'),'hasMany'=>array('UserReference','Resume','Social','Source','Question','Communication')),false);
	   $userslist = $this->User->find('all',array('conditions'=>array('User.role_id'=>'2','User.status'=>'1')));
          
           $this->loadModel('UserImage');
           $image_data = array();
           $count = 0;

           foreach($userslist as $value){

           $image_data[$value['User']['id']] = $this->UserImage->find('first', array('conditions' => array('UserImage.user_id' => $value['User']['id'])));
 
           }
           
            
            foreach($image_data as $key=>$value){

            $id = $value['UserImage']['user_id'];

             
             $upload_image = IMAGES . MENTORS_IMAGE_PATH . DS . $id .DS. uploaded;
              $i = 0; 
             if ($upload_handle = opendir($upload_image)) { 
             while (false !== ($file = readdir($upload_handle))) { 
                  
            if (!in_array($file, array('.', '..')) && !is_dir($upload_image.$file)) 
                $i++;

             }

             closedir($upload_handle);
             if($i>1){
              echo nl2br($id."---".$i."\n");
             }
          
             }

              }
             exit();
           
           }



  function website_prospect(){


              if(!(empty($this->data))){
		
                //pr($this->data);
                
		$prospect['WebsiteProspect']['name'] = $this->data['WebsiteProspect']['name'];
		$prospect['WebsiteProspect']['email'] = $this->data['WebsiteProspect']['email'];
		$prospect['WebsiteProspect']['company'] = $this->data['WebsiteProspect']['company'];
		$prospect['WebsiteProspect']['phone'] = $this->data['WebsiteProspect']['phone'];
		$prospect['WebsiteProspect']['comment'] = trim($this->data['WebsiteProspect']['comment']);
                $date = date('Y-m-d H:i:s');
		$prospect['WebsiteProspect']['submitted'] = $date;
		
		$this->WebsiteProspect->save($prospect);




                    //die;

	





			//EasticEmail Integration - Begin




                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "ankit@guild.im";
                           $subject = "A prospect requested for GUILD Website Service";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("website_prospect");
                           $data .= "&merge_name=".urlencode($this->data['WebsiteProspect']['name']);
                           $data .= "&merge_email=".urlencode($this->data['WebsiteProspect']['email']);
                           $data .= "&merge_phone=".urlencode($this->data['WebsiteProspect']['phone']);
                           $data .= "&merge_company=".urlencode($this->data['WebsiteProspect']['company']);
                           $data .= "&merge_comment=".urlencode($this->data['WebsiteProspect']['comment']);

                           $data .= "&merge_websiteurl=".urlencode($websiteurl);

                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }

                           //EasticEmail Integration - End


                          $this->redirect(array('action'=>'thanks_for_request'));





               }



  }


    function thanks_for_request(){
          $this->layout= 'defaultnew';
          $this->set("title_for_layout","Request has submitted");

   }
	
 
      
}

?>