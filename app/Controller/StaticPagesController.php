<?php
/*
 @controller : SystemMails
 @created    : 31 jan 2013
 @author     :
 @use        : Manager email templates
 
*/
class StaticPagesController extends AppController {
	
    var $name = 'StaticPages';
    var $uses = array('StaticPage');

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('thanks','insights','consultant_pricing_thanks','client_thanks','admin_index','admin_add','admin_edit','page_not_found','application_submitted');
    }

    public function admin_index()
	{
		  $this->layout = 'admin';  
		  $this->set('title_for_layout',__('Page Manager', true)) ;
		  $this->Page->recursive = -1 ;
		  $this->Paginate = array('limit'=>'10');
		  $this->set('pages',$this->Paginate('StaticPage')) ;

	}

	

	public function admin_add() 
	{
		$this->layout = 'admin';  
		if (!empty($this->data)) {
			if ($this->StaticPage->save($this->data,true)) {
			$this->Session->setFlash(__('Page saved sucessfully',true),'default',array('class'=>'flash_good'));
			$this->redirect(array('action' => 'index'));
			}
		}
    }

    function application_submitted(){
      $this->layout = 'defaultnew'; 
      $this->set('title_for_layout',__('Application submitted',true)) ;

   }
    function page_not_found(){

      $this->layout = 'defaultnew'; 
      $this->set('title_for_layout',__('Page not found',true)) ;

     }

	public function admin_edit($id= null)
	{
	$this->layout = 'admin';  
	   $this->set('title_for_layout',__('Edit Page',true)) ;
	   if(is_null($id))
	   {
	       $this->Session->setFlash(__('invalid page'),'default','flash_bad');
		   $this->redirect(array('action'=>'index'));
	   }
	  
	   if(!empty($this->data))
	   {
		   if($this->StaticPage->save($this->data))
		   {
				$this->Session->setFlash(__('Page  updated sucessfully',true),'default',array('class'=>'flash_good'));
				$this->redirect(array('action'=>'admin_index'));
	
		   }
	
		   else
		   {
			   $this->Session->setFlash(__('Page could not be saved please try again', true),'default',array('class'=>'flash_bad'));
	
		   }
	   }
	   else
	   {
		    $this->data  =  $this->StaticPage->read(null ,$id) ;
			if(empty($this->data ))
			{
			    $this->redirect(array('action'=>'admin_index'));	
			}
			
	   }

	   

	}
  function thanks(){
 
$this->layout = 'defaultnew';
        
    }

   
   function insights() {
    $this->set("title_for_layout","Weekly insights");

   }
function consultant_pricing_thanks(){
        
    }




function client_thanks(){

}

}
?>