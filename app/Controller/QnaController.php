<?php

/**
 * Users Controller
 *
 * PHP version 5
 *
 * @category Controller
 */


use Elasticsearch\ClientBuilder;
require 'vendor/autoload.php';

class QnaController extends AppController{
	/**
	* Controller name
	*
	* @var string
	* @access public
	*/
       
	var $name = 'Qna';
	var $uses = array('User', 'UserReference','QnaQuestion', 'QnaQuestionCategory', 
			'QnaAnswer', 'QnaIgnoredQuestion','Insight', 'InsightCategory','IndustryCategory',
			'MediaRecords','Prospect', 'MemberIndustryCategory', 'Topic','CacheUrl');
       var $helpers = array('General','Front');
	var $components = array('Upload','RequestHandler','General','Paginator');
	/**
	 * Models used by the Controller
	 *
	 * @var array
	 * @access public
	*/	 
	function beforeFilter(){
		parent::beforeFilter();	 
		$this->Auth->allow('index','ask_question','complete_question','question_preview','add_question','answer_question',
				'add_answer','category','question', 'review_your_question', 'thanks_for_asking','search',
				'admin_deleteall', 'upvote','ignore_question','admin_member_categories','admin_categories','admin_question_review','admin_editquestion','admin_media_add','admin_editmediarecord','admin_deleteall','admin_addcategories','admin_editcategories','admin_deletecategories','admin_category_member_search','admin_media_record','admin_category_member_search','admin_deletequestion','refresh_cache','share_question','share_answer');
	}
	
	function review_your_question()
	{}

	function refresh_cache(){
		
		$unique_urls = $this->CacheUrl->find('first', array('fields'=>array('id','url'), 'order' => 'modified asc'));
		
		$unique_urls['CacheUrl']['modified'] = date("Y-m-d H:i:s");
		
		$this->CacheUrl->save($unique_urls);
		
		set_time_limit(0);
		
	   	if (!empty($unique_urls['CacheUrl']['url']) && $unique_urls['CacheUrl']['url'] != '') {

    		$curlOptions = array (
				CURLOPT_URL => $unique_urls['CacheUrl']['url'].'?isCron=1',
    				CURLOPT_VERBOSE => 1,
    				CURLOPT_SSL_VERIFYPEER => 0,
    				CURLOPT_SSL_VERIFYHOST => 0,
    				CURLOPT_RETURNTRANSFER => 1,
    		);

    		$ch = curl_init();
    		curl_setopt_array($ch,$curlOptions);
    
    		$response = curl_exec($ch);

			curl_close($ch);
			
		}
	}
	
	function admin_categories(){
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';   
		$result = $this->IndustryCategory->find('all',array('order'=>'IndustryCategory.id asc'));
		$count = 0;
		foreach($result as $category) {
			
			$count1 = $this->MemberIndustryCategory->find('count', array('conditions' => array('MemberIndustryCategory.category1' => $category['IndustryCategory']['id'])));
			$count2 = $this->MemberIndustryCategory->find('count', array('conditions' => array('MemberIndustryCategory.category2' => $category['IndustryCategory']['id'])));
			$count3 = $this->MemberIndustryCategory->find('count', array('conditions' => array('MemberIndustryCategory.category3' => $category['IndustryCategory']['id'])));
			
			$result[$count++]['IndustryCategory']['assigned_to'] = $count1 + $count2 + $count3;
		}
		$this->set('data', $result);
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}
	

	

	

	
	function admin_member_categories(){
   	if($this->Auth->user('id') == 1){
	    	
	    $this->layout = 'admin';  

		//GetQ categories
		$result = $this->Topic->find('all');
		$qcategories = array();
		foreach($result as $value){
			$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
		}
		$this->set("categories",$qcategories);

		if(empty($this->data) == false){
			
                //  prd($this->data);	

			foreach ($this->data['User']['Member_category'] as $value){
				
				$cat_tags = $value['categories_tags'];
				$id = $value['user_id'];
				
				$topic_arr = array(-1,-1,-1,-1,-1);
				//Saving Expertise for Search Autocomplete
				$stringExp = $cat_tags;
				$Exp3 = explode(',',$stringExp);
				$allExp = array();
				$count = 0;
					
				for($i=0;$i<count($Exp3);$i++){
				
					if(isset($Exp3[$i]) && $Exp3[$i] != '')
						$allExp[$count++] = ucfirst(trim($Exp3[$i]));
				}
					
				$uniqueAry = array_unique($allExp);
				
				$count = 0;
				
				foreach($uniqueAry as $exp){
						
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$exp)));
					
					$topic_arr[$count] = $match['Topic']['id'];
					
					if(empty($match)) {
						$this->Topic->create( );
						$this->Topic->save(array('autocomplete_text'=>$exp));
						$topic_arr[$count] = $this->Topic->id;
						unset($exp);
					}
					$count++;
				}
				
				//End Saving Autocomplete text
				
				//Saving Industry Categories
				$industry1 = $value['industry_id1'];
				$category1 = $value['ind_category_id1'];
				
				$industry2 = $value['industry_id2'];
				$category2 = $value['ind_category_id2'];
				
				$industry3 = $value['industry_id3'];
				$category3 = $value['ind_category_id3'];
				
				$topic1 = -1;
				$topic2 = -1;
				$topic3 = -1;
				$topic4 = -1;
				$topic5 = -1;
				
				$memberIndustryCategory = $this->MemberIndustryCategory->find('first',array('conditions'=>array('MemberIndustryCategory.user_id'=>$id)));

				if(empty($memberIndustryCategory)) {
					$memberIndustryCategory['MemberIndustryCategory']['user_id'] = $id;
					$memberIndustryCategory['MemberIndustryCategory']['category1'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['category2'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['category3'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['industry1'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['industry2'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['industry3'] = -1;
				}
				
				$memberIndustryCategory['MemberIndustryCategory']['topic1'] = $topic_arr[0];
				$memberIndustryCategory['MemberIndustryCategory']['topic2'] = $topic_arr[1];
				$memberIndustryCategory['MemberIndustryCategory']['topic3'] = $topic_arr[2];
				$memberIndustryCategory['MemberIndustryCategory']['topic4'] = $topic_arr[3];
				$memberIndustryCategory['MemberIndustryCategory']['topic5'] = $topic_arr[4];
				
				$memberIndustryCategory['MemberIndustryCategory']['quality'] = $value['quality'];
				
				$industry_string = '';

				if($industry1 != -1) {
					$industry_obj1 = $this->IndustryCategory->find('first',array('conditions'=>array('IndustryCategory.category'=>$industry1)));

					$cat_list = $this->IndustryCategory->find('all',array('conditions'=>array('IndustryCategory.category'=>$category1)));
					
					foreach ($cat_list as $key => $value) {

						if($value['IndustryCategory']['parent'] == $industry_obj1['IndustryCategory']['id']) {
							$memberIndustryCategory['MemberIndustryCategory']['industry1'] = $value['IndustryCategory']['parent'];
							$memberIndustryCategory['MemberIndustryCategory']['category1'] = $value['IndustryCategory']['id'];
						}
					}
					if($category1 == "ALL CATEGORIES")
						$industry_string = $industry_string . $industry1;
					else
						$industry_string = $industry_string . $industry1 . ":  " . $category1;
					
					$industry_string = $industry_string . "|";
					
				}

				if($industry2 != -1){
					$industry_obj2 = $this->IndustryCategory->find('first',array('conditions'=>array('IndustryCategory.category'=>$industry2)));

					$cat_list = $this->IndustryCategory->find('all',array('conditions'=>array('IndustryCategory.category'=>$category2)));
					
					foreach ($cat_list as $key => $value) {

						if($value['IndustryCategory']['parent'] == $industry_obj2['IndustryCategory']['id']) {
							$memberIndustryCategory['MemberIndustryCategory']['industry2'] = $value['IndustryCategory']['parent'];
							$memberIndustryCategory['MemberIndustryCategory']['category2'] = $value['IndustryCategory']['id'];
						}
					}
					if($category2 == "ALL CATEGORIES")
						$industry_string = $industry_string . $industry2;
					else
						$industry_string = $industry_string . $industry2 . ":  " . $category2;
					
					$industry_string = $industry_string . "|";
				}

				if($industry3 != -1) {
					$industry_obj3 = $this->IndustryCategory->find('first',array('conditions'=>array('IndustryCategory.category'=>$industry3)));

					$cat_list = $this->IndustryCategory->find('all',array('conditions'=>array('IndustryCategory.category'=>$category3)));

					foreach ($cat_list as $key => $value) {

						if($value['IndustryCategory']['parent'] == $industry_obj3['IndustryCategory']['id']) {
							$memberIndustryCategory['MemberIndustryCategory']['industry3'] = $value['IndustryCategory']['parent'];
							$memberIndustryCategory['MemberIndustryCategory']['category3'] = $value['IndustryCategory']['id'];
						}
					}
					if($category3 == "ALL CATEGORIES")
						$industry_string = $industry_string . $industry3;
					else
						$industry_string = $industry_string . $industry3 . ":  " . $category3;
					
					$industry_string = $industry_string . "|";
					
				}

				$this->MemberIndustryCategory->save($memberIndustryCategory);
				$this->MemberIndustryCategory->id = null;
				
				//End -- Saving Industry Categories
				
				
                            


			$client = ClientBuilder::create()->build();	
			
			$params = array(
			    'index' => 'guild_members',
			    'type' => 'guild',
			    'id' => $id,
			    'body' => array(
			        'doc' => array(
					    'industry_category' => $industry_string,
					    'categories_tags' => $cat_tags,
					    'quality' => $value['quality']
			        )
			    )
			);
			
			// Update doc at /my_index/my_type/my_id
			$response = $client->update($params);


			}
		}
		
		if (!isset($this->request['named']['page'])) {
			$this->Session->delete('Mentorsearch');
		}
		$filters[] = array('User.role_id' => 2, "User.status !=" => 2,'User.is_approved'=>1, 'User.access_specifier !='=>'draft');
		
		$this->User->unbindModel(array('hasOne'=>array('Availablity'),'hasMany'=>array('UserImage','Social','Communication','CaseStudy','Feed','Testimonial')),false);
		  $this->Paginator->settings = array(
                'limit' => 100,
                'order' => array('User.id' => 'DESC'),
                'conditions' => $filters			
               );
                $result = $this->Paginator->paginate('User');	
	
                $this->set('data', $result);
		 // $categories = $this->QnaCategory->find('all',array('order'=>'QnaCategory.id asc'));
		 // $this->set('categories', $categories);
		  
		  
		  $industry_categories = $this->IndustryCategory->find('all',array('order'=>'IndustryCategory.id asc'));
		  $this->set('industry_categories', $industry_categories);
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}
	


	function admin_category_member_search(){
   	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  

		//GetQ categories
		$result = $this->Topic->find('all');
		$qcategories = array();
		foreach($result as $value){
			$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
		}
		$this->set("categories",$qcategories);

		if(empty($this->data) == false){
			
                	
		
			foreach ($this->data['User']['Member_category'] as $value){
				
				$cat_tags = $value['categories_tags'];
				$id = $value['user_id'];
				
				$topic_arr = array(-1,-1,-1,-1,-1);
				//Saving Expertise for Search Autocomplete
				$stringExp = $cat_tags;
				$Exp3 = explode(',',$stringExp);
				$allExp = array();
				$count = 0;
					
				for($i=0;$i<count($Exp3);$i++){
				
					if(isset($Exp3[$i]) && $Exp3[$i] != '')
						$allExp[$count++] = ucfirst(trim($Exp3[$i]));
				}
					
				$uniqueAry = array_unique($allExp);
				
				$count = 0;
				
				foreach($uniqueAry as $exp){
						
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$exp)));
					
					$topic_arr[$count] = $match['Topic']['id'];
					
					if(empty($match)) {
						$this->Topic->create( );
						$this->Topic->save(array('autocomplete_text'=>$exp));
						$topic_arr[$count] = $this->Topic->id;
						unset($exp);
					}
					$count++;
				}
				
				//End Saving Autocomplete text
				
				//Saving Industry Categories
				$industry1 = $value['industry_id1'];
				$category1 = $value['ind_category_id1'];
				
				$industry2 = $value['industry_id2'];
				$category2 = $value['ind_category_id2'];
				
				$industry3 = $value['industry_id3'];
				$category3 = $value['ind_category_id3'];
				
				$topic1 = -1;
				$topic2 = -1;
				$topic3 = -1;
				$topic4 = -1;
				$topic5 = -1;
				
				$memberIndustryCategory = $this->MemberIndustryCategory->find('first',array('conditions'=>array('MemberIndustryCategory.user_id'=>$id)));

				if(empty($memberIndustryCategory)) {
					$memberIndustryCategory['MemberIndustryCategory']['user_id'] = $id;
					$memberIndustryCategory['MemberIndustryCategory']['category1'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['category2'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['category3'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['industry1'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['industry2'] = -1;
					$memberIndustryCategory['MemberIndustryCategory']['industry3'] = -1;
				}
				
				$memberIndustryCategory['MemberIndustryCategory']['topic1'] = $topic_arr[0];
				$memberIndustryCategory['MemberIndustryCategory']['topic2'] = $topic_arr[1];
				$memberIndustryCategory['MemberIndustryCategory']['topic3'] = $topic_arr[2];
				$memberIndustryCategory['MemberIndustryCategory']['topic4'] = $topic_arr[3];
				$memberIndustryCategory['MemberIndustryCategory']['topic5'] = $topic_arr[4];
				
				$memberIndustryCategory['MemberIndustryCategory']['quality'] = $value['quality'];
				
				$industry_string = '';

				if($industry1 != -1) {
					$industry_obj1 = $this->IndustryCategory->find('first',array('conditions'=>array('IndustryCategory.category'=>$industry1)));

					$cat_list = $this->IndustryCategory->find('all',array('conditions'=>array('IndustryCategory.category'=>$category1)));
					
					foreach ($cat_list as $key => $value) {

						if($value['IndustryCategory']['parent'] == $industry_obj1['IndustryCategory']['id']) {
							$memberIndustryCategory['MemberIndustryCategory']['industry1'] = $value['IndustryCategory']['parent'];
							$memberIndustryCategory['MemberIndustryCategory']['category1'] = $value['IndustryCategory']['id'];
						}
					}
					if($category1 == "ALL CATEGORIES")
						$industry_string = $industry_string . $industry1;
					else
						$industry_string = $industry_string . $industry1 . ":  " . $category1;
					
					$industry_string = $industry_string . "|";
					
				}

				if($industry2 != -1){
					$industry_obj2 = $this->IndustryCategory->find('first',array('conditions'=>array('IndustryCategory.category'=>$industry2)));

					$cat_list = $this->IndustryCategory->find('all',array('conditions'=>array('IndustryCategory.category'=>$category2)));
					
					foreach ($cat_list as $key => $value) {

						if($value['IndustryCategory']['parent'] == $industry_obj2['IndustryCategory']['id']) {
							$memberIndustryCategory['MemberIndustryCategory']['industry2'] = $value['IndustryCategory']['parent'];
							$memberIndustryCategory['MemberIndustryCategory']['category2'] = $value['IndustryCategory']['id'];
						}
					}
					if($category2 == "ALL CATEGORIES")
						$industry_string = $industry_string . $industry2;
					else
						$industry_string = $industry_string . $industry2 . ":  " . $category2;
					
					$industry_string = $industry_string . "|";
				}

				if($industry3 != -1) {
					$industry_obj3 = $this->IndustryCategory->find('first',array('conditions'=>array('IndustryCategory.category'=>$industry3)));

					$cat_list = $this->IndustryCategory->find('all',array('conditions'=>array('IndustryCategory.category'=>$category3)));

					foreach ($cat_list as $key => $value) {

						if($value['IndustryCategory']['parent'] == $industry_obj3['IndustryCategory']['id']) {
							$memberIndustryCategory['MemberIndustryCategory']['industry3'] = $value['IndustryCategory']['parent'];
							$memberIndustryCategory['MemberIndustryCategory']['category3'] = $value['IndustryCategory']['id'];
						}
					}
					if($category3 == "ALL CATEGORIES")
						$industry_string = $industry_string . $industry3;
					else
						$industry_string = $industry_string . $industry3 . ":  " . $category3;
					
					$industry_string = $industry_string . "|";
					
				}

				$this->MemberIndustryCategory->save($memberIndustryCategory);
				$this->MemberIndustryCategory->id = null;
				
				//End -- Saving Industry Categories
				
				
                            


			$client = ClientBuilder::create()->build();	
			
			$params = array(
			    'index' => 'guild_members',
			    'type' => 'guild',
			    'id' => $id,
			    'body' => array(
			        'doc' => array(
					    'industry_category' => $industry_string,
					    'categories_tags' => $cat_tags,
					    'quality' => $value['quality']
			        )
			    )
			);
			
			// Update doc at /my_index/my_type/my_id
			$response = $client->update($params);

                    }

		}

		
		if (!isset($this->request['named']['page'])) {
                   

                     $this->Session->delete('Namesearch');
		}


             if (!empty($this->data)) {
              

               $this->Session->delete('Namesearch');



             if (!empty($this->data['UserReference']['first_name'])) {
                
                //App::import('Sanitize');
                //$name = Sanitize::escape($this->data['UserReference']['first_name']);
                $name = ($this->data['UserReference']['first_name']);
                
                $this->Session->write('Namesearch', $name);
            }
        }

		
           if($this->Session->check('Namesearch')) {

            $first_name = array('UserReference.first_name LIKE' => "%" . $this->Session->read('Namesearch') . "%");
            $last_name = array('UserReference.last_name LIKE' => "%" . $this->Session->read('Namesearch') . "%");
            $filters[] = array('AND' =>array(array('OR' => array($first_name, $last_name)),array('User.role_id' => 2,"User.status !=" => 2,'User.is_approved'=>1, 'User.access_specifier !='=>'draft')));
            $this->User->unbindModel(array('hasOne'=>array('Availablity'),'hasMany'=>array('UserImage','Social','Communication','CaseStudy')),false);
		  $this->Paginator->settings = array(
                'limit' => 100,
                'order' => array('User.id' => 'DESC'),
                'conditions' => $filters			
               );
                $result = $this->Paginator->paginate('User');		
                $this->set('data', $result);


            }



		  $industry_categories = $this->IndustryCategory->find('all',array('order'=>'IndustryCategory.id asc'));
		  $this->set('industry_categories', $industry_categories);
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
         

	}

	function admin_question_review(){
   	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		$questions = $this->QnaQuestion->find('all', array('conditions'=>array('QnaQuestion.isdraft'=>'1')));

		$this->set('questions', $questions);
	
		$result = $this->Topic->find('all');
		$qcategories = array();
		foreach($result as $value){
			$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
		}
		$this->set("categories",$qcategories);
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}
	
	function admin_deleteall(){
   	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		$questions = $this->QnaQuestion->find('all', array('conditions'=>array('QnaQuestion.isdraft'=>'1')));
		foreach ($questions as $q) {
			
			$this->QnaQuestion->delete($q['QnaQuestion']['id']);
		}
		$this->redirect(array('action' => 'question_review'));
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}
	
	function admin_editquestion($id = null){
   	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		$this->pageTitle = __('Edit question', true);
		if(!$id && empty($this->data)) {
			$this->Session->setFlash('Invalid Id');
			$this->redirect(array('action' => 'question_review'));
		}
	
		$this->set('modelName', 'QnaQuestion');
		$this->set('controllerName', 'QnA');
	
		$res = $this->QnaQuestion->read(null, $id);
		$this->set('res',$res);
	
		if(!empty($this->data))
		{
			$question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.id' => $this->data['QnaQuestion']['id'])));

			$question['QnaQuestion']['question_text'] = $this->data['QnaQuestion']['question_text'];
			$question['QnaQuestion']['question_context'] = $this->data['QnaQuestion']['question_context'];
			$question['QnaQuestion']['isdraft'] = 0;
			
			$this->QnaQuestionCategory->deleteAll(array('QnaQuestionCategory.qna_question_id' => $this->data['QnaQuestion']['id']));
			
			if ($this->QnaQuestion->save($question)){
				
				$this->QnaQuestion->createUrlKey($id);
				$this->Session->setFlash('Question has been saved', 'admin_flash_good');
				
				$cat_tags = $this->data['QnaQuestion']['select'];
				$Exp3 = explode(',',$cat_tags);
				
				for($i=0; $i<count($Exp3); $i++){
				
					if(isset($Exp3[$i]) && $Exp3[$i] != '' && $Exp3[$i] != -1)
						$allExp[$count++] = ucfirst(trim($Exp3[$i]));
				}
				
				if(isset($Exp3[0]) && $Exp3[0] != '' && $Exp3[0] != -1) {
					
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$Exp3[0] )));
					
					$question['QnaQuestionCategory'][0]['topic_id'] = $match['Topic']['id'];
					$question['QnaQuestionCategory'][0]['qna_question_id'] = $this->data['QnaQuestion']['id'];
					$this->QnaQuestionCategory->save($question['QnaQuestionCategory'][0]);
					unset($this->QnaQuestionCategory->id);
					
					$mTopic11 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic1' => $match['Topic']['id'])));
					$mTopic21 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic2' => $match['Topic']['id'])));
					$mTopic31 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic3' => $match['Topic']['id'])));
					$mTopic41 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic4' => $match['Topic']['id'])));
					$mTopic51 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic5' => $match['Topic']['id'])));
					
					$ind_category = $this->IndustryCategory->find('first',array('conditions' => array('IndustryCategory.category' => $match['Topic']['autocomplete_text'])));
					
					$mCategories11 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.category1' => $ind_category['IndustryCategory']['id'])));
					$mCategories21 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.category2' => $ind_category['IndustryCategory']['id'])));
					$mCategories31 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.category3' => $ind_category['IndustryCategory']['id'])));
						
				}
				
				if(isset($Exp3[1]) && $Exp3[1] != '' && $Exp3[1] != -1) {
					
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$Exp3[1] )));
					
					$question['QnaQuestionCategory'][1]['topic_id'] = $match['Topic']['id'];
					$question['QnaQuestionCategory'][1]['qna_question_id'] = $this->data['QnaQuestion']['id'];
					$this->QnaQuestionCategory->save($question['QnaQuestionCategory'][1]);
					unset($this->QnaQuestionCategory->id);
					
					$mTopic12 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic1' => $match['Topic']['id'])));
					$mTopic22 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic2' => $match['Topic']['id'])));
					$mTopic32 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic3' => $match['Topic']['id'])));
					$mTopic42 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic4' => $match['Topic']['id'])));
					$mTopic52 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic5' => $match['Topic']['id'])));
					
					$ind_category = $this->IndustryCategory->find('first',array('conditions' => array('IndustryCategory.category' => $match['Topic']['autocomplete_text'])));
					
					$mCategories12 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.category1' => $ind_category['IndustryCategory']['id'])));
					$mCategories22 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.category2' => $ind_category['IndustryCategory']['id'])));
					$mCategories32 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.category3' => $ind_category['IndustryCategory']['id'])));
				
				}
				
				if(isset($Exp3[2]) && $Exp3[2] != '' && $Exp3[2] != -1) {
					
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$Exp3[2] )));
					
					$question['QnaQuestionCategory'][2]['topic_id'] = $match['Topic']['id'];
					$question['QnaQuestionCategory'][2]['qna_question_id'] = $this->data['QnaQuestion']['id'];
					$this->QnaQuestionCategory->save($question['QnaQuestionCategory'][2]);
					unset($this->QnaQuestionCategory->id);
					
					$mTopic13 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic1' => $match['Topic']['id'])));
					$mTopic23 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic2' => $match['Topic']['id'])));
					$mTopic33 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic3' => $match['Topic']['id'])));
					$mTopic43 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic4' => $match['Topic']['id'])));
					$mTopic53 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic5' => $match['Topic']['id'])));
					
					$ind_category = $this->IndustryCategory->find('first',array('conditions' => array('IndustryCategory.category' => $match['Topic']['autocomplete_text'])));
					
					$mCategories13 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.category1' => $ind_category['IndustryCategory']['id'])));
					$mCategories23 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.category2' => $ind_category['IndustryCategory']['id'])));
					$mCategories33 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.category3' => $ind_category['IndustryCategory']['id'])));
				
				}
				
				$memberArray = array();
				
				$this->User->Behaviors->attach('Containable');
				
				foreach($mCategories11 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name','question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mCategories21 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mCategories31 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mCategories12 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mCategories22 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mCategories32 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mCategories13 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mCategories23 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mCategories33 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mTopic11 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mTopic21 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mTopic31 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mTopic41 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mTopic51 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mTopic12 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mTopic22 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mTopic32 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mTopic42 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mTopic52 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mTopic13 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mTopic23 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mTopic33 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mTopic43 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				foreach($mTopic53 as $value)
					$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
				
				$ques = $this->QnaQuestion->find('first',array('conditions' => array('QnaQuestion.id' => $this->data['QnaQuestion']['id'])));
				
				foreach($memberArray as $mValue) {
					//Send question email
					//question_approved

					if($mValue['UserReference']['question_notification'] == 'Y') {



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
			   $text = 'email_notification';
                           $to = $mValue['User']['username'];
                           $subject = "New questions in your area of expertise";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("question_approved");
                           $data .= "&merge_userfname=".urlencode($mValue['UserReference']['first_name']);
                           $data .= "&merge_questiontext=".urlencode($ques['QnaQuestion']['question_text']);
                           $data .= "&merge_questionurl=".urlencode($ques['QnaQuestion']['url_key']);
                           $data .= "&merge_useremail=".urlencode($mValue['User']['username']);
                           $data .= "&merge_text=".urlencode($text);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

					}
				}
				
				$this->redirect(array('action' => 'question_review'));
			}else{
				$this->Session->setFlash('Please correct the errors listed below.', 'admin_flash_bad');
			}
		}else{
			$this->data = $this->QnaQuestion->read(null, $id);
			
			$memberArray = array();
			
			$this->User->Behaviors->attach('Containable');
			
			$tags = '';
			
			foreach($res['QnaQuestionCategory'] as $key => $cValue) {
					
					
				$topic = $this->Topic->find('first',array('conditions' => array('Topic.id' => $cValue['topic_id'])));
				
				if($key == 0)
					$tags = $topic['Topic']['autocomplete_text'];
				else
					$tags = $tags . ',' . $topic['Topic']['autocomplete_text'];
				
				$ind_category = $this->IndustryCategory->find('first',array('conditions' => array('IndustryCategory.category' => $topic['Topic']['autocomplete_text'])));
				
				if(isset($ind_category)) {
					
					$mCategories1 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.category1' => $ind_category['IndustryCategory']['id'])));
					$mCategories2 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.category2' => $ind_category['IndustryCategory']['id'])));
					$mCategories3 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.category3' => $ind_category['IndustryCategory']['id'])));
					
					$mTopic1 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic1' => $topic['Topic']['id'])));
					$mTopic2 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic2' => $topic['Topic']['id'])));
					$mTopic3 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic3' => $topic['Topic']['id'])));
					$mTopic4 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic4' => $topic['Topic']['id'])));
					$mTopic5 = $this->MemberIndustryCategory->find('all',array('conditions' => array('MemberIndustryCategory.topic5' => $topic['Topic']['id'])));
					
					foreach($mCategories1 as $value)
						$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
					
					foreach($mCategories2 as $value)
						$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
					
					foreach($mCategories3 as $value)
						$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
					
					foreach($mTopic1 as $value)
						$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
					
					foreach($mTopic2 as $value)
						$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
					
					foreach($mTopic3 as $value)
						$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
					
					foreach($mTopic4 as $value)
						$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
					
					foreach($mTopic5 as $value)
						$memberArray[$value['MemberIndustryCategory']['user_id']] = $this->User->find('first', array('contain'=>array('UserReference'=>array('fields'=>array('first_name','last_name', 'question_notification'))),'conditions'=>array('User.id'=>$value['MemberIndustryCategory']['user_id'], 'User.status'=>1)));
					
				}
			}

			$membersAttached = array();
			foreach($memberArray as $mValue) {

				if($mValue['UserReference']['question_notification'] == 'Y') {
					$membersAttached[$mValue['User']['id']] = $mValue;
				}
			}
			$this->set('membersAttached', $membersAttached);
			
			$keywords = $this->Topic->find('all',array(
					'fields' => array('DISTINCT (Topic.autocomplete_text) AS autocomplete_text','id'),
					'conditions'=>array('Topic.isName'=>0)
			));

			$this->set('keywords',$keywords);
			
			$key = array();
			$cn = 0;
			foreach($keywords as $value){
				
				
				$mTopic13 = $this->MemberIndustryCategory->find('count',array('conditions' => array('MemberIndustryCategory.topic1' => $value['Topic']['id'])));
				$mTopic23 = $this->MemberIndustryCategory->find('count',array('conditions' => array('MemberIndustryCategory.topic2' => $value['Topic']['id'])));
				$mTopic33 = $this->MemberIndustryCategory->find('count',array('conditions' => array('MemberIndustryCategory.topic3' => $value['Topic']['id'])));
				$mTopic43 = $this->MemberIndustryCategory->find('count',array('conditions' => array('MemberIndustryCategory.topic4' => $value['Topic']['id'])));
				$mTopic53 = $this->MemberIndustryCategory->find('count',array('conditions' => array('MemberIndustryCategory.topic5' => $value['Topic']['id'])));
				
				$total = $mTopic13 + $mTopic23 + $mTopic33 + $mTopic43 + $mTopic53;
				
				$key[$cn++] = $value['Topic']['autocomplete_text'] . "(" . $total .")";
			}
			$this->set('allKeywords', $key);
	
			$this->set('tags', $tags);
		}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	
	}
	
	function admin_deletequestion($id = null){
   	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		if($this->QnaQuestion->delete($id)) {
				
			$this->Session->setFlash('Question has been deleted', 'admin_flash_good');
			$this->redirect(array('action' => 'question_review'));
		} else {
			$this->Session->setFlash('Question could not be deleted', 'admin_flash_bad');
		}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}
	
	function index(){

                        $this->layout= 'defaultnew';
		        $this->Session->write('popup', 1);
			$this->set("title_for_layout","Q&A");
		
			//GetQ categories
			$result = $this->Topic->find('all');
			$qcategories = array();
			foreach($result as $value){
				$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
			}
			$this->set("categories",$qcategories);


                        $unique_topics = $this->QnaQuestionCategory->find('all', array('fields'=>'DISTINCT topic_id,qna_question_id'));
 
                        $inunique_topics = $this->InsightCategory->find('all', array('fields'=>'DISTINCT topic_id,insight_id'));  

                        $qtopiclist = array();
                        $intopiclist = array();
                        $topiclist = array();
			foreach($unique_topics as $key=>$aValue){
				$answers = $this->QnaAnswer->find('first',array('conditions' => array('QnaAnswer.question_id' => $aValue['QnaQuestionCategory']['qna_question_id'])));
                                if($answers['QnaAnswer']['answer_text'] !=''){
 

				$qtopiclist[$key] = $qcategories[$aValue['QnaQuestionCategory']['topic_id']];
                               }
			}

			foreach($inunique_topics as $key=>$aValue){
				$insight = $this->Insight->find('first', array('conditions' => array('AND' => array(array('Insight.id' => $aValue['InsightCategory']['insight_id']),array('Insight.published'=>1)))));
                                if($insight['Insight']['insight']!=''){
				$intopiclist[$key] = $qcategories[$aValue['InsightCategory']['topic_id']];
                                }
			}

                        $topiclist = (array_unique(array_merge($qtopiclist,$intopiclist)));
                        
                        $this->set("topiclist",$topiclist); 


                        //Get recently asked question
			$answers = $this->QnaAnswer->find('all',array('order'=>'QnaAnswer.id desc', 'fields' => array('QnaAnswer.question_id')));

			foreach($answers as $aValue){
				
				$fAnswers[$aValue['QnaAnswer']['question_id']] = $aValue['QnaAnswer']['question_id'];
			}

			$questionArray = array();
			$qCount = 0;
			foreach($fAnswers as $aValue){
				if($qCount < 5){
					$question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.id' => $aValue)));
					
					//Find all answers to this question and user images
					
					//Find other answers to this question
					$answers = $this->QnaAnswer->find('all',array('conditions' => array('QnaAnswer.question_id' => $aValue)));
					
					$finalAnswers = array();
				
					foreach($answers as $answer){
							
						$userData = $this->User->find('first',
								array(
										'contain'=>array(
												'UserReference'=>array(
														'fields'=>array('first_name','last_name','zipcode'),
												),
												'UserImage' ,
										),
										'conditions'=>array('User.id'=>$answer['QnaAnswer']['member_id'])
								)
				
						);
						if(empty($userData['UserImage']) == false)
							$question['User'][$answer['QnaAnswer']['member_id']]['user_image'] = $userData['UserImage'][0]['image_name'];
						$question['User'][$answer['QnaAnswer']['member_id']]['first_name'] = $userData['UserReference']['first_name'];
						$question['User'][$answer['QnaAnswer']['member_id']]['last_name'] = $userData['UserReference']['last_name'];
						$question['User'][$answer['QnaAnswer']['member_id']]['url_key'] = $userData['User']['url_key'];
						$question['User'][$answer['QnaAnswer']['member_id']]['member_id'] = $answer['QnaAnswer']['member_id'];
							
					}
					
					$questionArray[$qCount++] = $question;
				}
			}
			$this->set('questionArray', $questionArray);
			
			//Get Insights
			$this->Insight->Behaviors->attach('Containable');
			$insights = $this->Insight->find('all', array('conditions'=>array('Insight.published'=>1),'order'=>'Insight.id desc'));
			$total = count($insights);
			$insightArray = array();
			$qCount = 0;
                        $this->loadModel('City');
			foreach($insights as $aValue){
					if($qCount < $total){
					$this->User->Behaviors->attach('Containable');
			
					$userData = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name','linkedin_headline','zipcode'),
											),
											'UserImage' ,
									),
									'conditions'=>array('User.id'=>$aValue['Insight']['user_id'])
							)
			
					);

		                        $dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($userData['UserReference']['zipcode']))));
						
					if(empty($userData['UserImage']) == false)
				        $aValue['User']['user_image'] = $userData['UserImage'][0]['image_name'];
					$aValue['User']['first_name'] = $userData['UserReference']['first_name'];
					$aValue['User']['last_name'] = $userData['UserReference']['last_name'];
                                        $aValue['User']['headline'] = $userData['UserReference']['linkedin_headline'];
					$aValue['User']['url_key'] = $userData['User']['url_key'];
					$aValue['User']['city'] = $dataZip['City']['city_name'];
					$aValue['User']['state'] = $dataZip['City']['state'];
						
					$insightArray[$qCount++] = $aValue;
			}
                   }
			$this->set('insights', $insightArray);
			

			
	              $plan_type= $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
                     $this->set("plan_type",$plan_type['User']['plan_type']);

        }
	
	function ask_question(){
                $this->layout= 'defaultnew';
		if(empty($this->data) == false) {
			$this->set('Question_text', $this->data['QnaQuestion']['question_text']);
			$this->set('Question_context', '');
			$this->Session->write('Question_text', $this->data['QnaQuestion']['question_text']);
			$this->Session->write('Question_context', '');
		}else if($this->Session->read('Question_text') != '' && $this->Session->read('Question_context') != '') {
			
			$this->set('Question_text', $this->Session->read('Question_text'));
			$this->set('Question_context', $this->Session->read('Question_context'));
			$this->set('Question_id', $this->Session->read('Question_id'));
		}else if($this->Session->read('Question_text') != '') {
			$this->set('Question_text', $this->Session->read('Question_text'));
			$this->set('Question_context', '');
		}
	}
	
	function question_preview(){


                $this->layout= 'defaultnew';
                if($this->Session->check('menteePopup')){
                $this->Session->delete('menteePopup');
                }
                if($this->Session->check('projectId')){
                $this->Session->delete('projectId');
                } 
                if($this->Session->check('questionId')){
                $this->Session->delete('questionId');
                } 
                if($this->Session->check('login_industry')){
                $this->Session->delete('login_industry');
                } 
                if($this->Session->check('mentor')){
                $this->Session->delete('mentor');
                }  
                if($this->Session->check('duserid')){
                $this->Session->delete('duserid');
                } 
                if($this->Session->check('mentorshipId')){
                $this->Session->delete('mentorshipId');
                }
                if($this->Session->check('consultantPopup')){
                $this->Session->delete('consultantPopup');
                }
                if($this->Session->check('directoryclaim')){
                $this->Session->delete('directoryclaim');
                } 

	    $this->Session->write('popup', 1);
		
		$keywords = $this->Topic->find('all',array(
				'fields' => array('DISTINCT (Topic.autocomplete_text) AS autocomplete_text'),
				'conditions'=>array('Topic.isName'=>0)
		));

		$this->set('keywords',$keywords);
		
		$key = array();
		$cn = 0;
		foreach($keywords as $value){
			$key[$cn++] = $value['Topic']['autocomplete_text'];
		}
		$this->set('allKeywords', $key);
		
		
		if(empty($this->data) == false) {
			
			if($this->data['QnaQuestion']['question_text'] != '' && $this->data['QnaQuestion']['question_context'] != ''){
			
				$Question = '';
				if(isset($this->data['QnaQuestion']['id'])) {
					$Question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.id' => $this->data['QnaQuestion']['id'])));
				}
				
				$Question['QnaQuestion']['question_text'] = $this->data['QnaQuestion']['question_text'];
				$Question['QnaQuestion']['question_context'] = $this->data['QnaQuestion']['question_context'];
				$Question['QnaQuestion']['isdraft'] = 1;
				
				$this->QnaQuestion->save($Question['QnaQuestion']);
				
				$this->QnaQuestion->createUrlKey($this->QnaQuestion->id);
				
				$this->set('Question_text', $this->data['QnaQuestion']['question_text']);
				$this->set('Question_context', $this->data['QnaQuestion']['question_context']);
				$this->set('Question_id', $this->QnaQuestion->id);
				
				$this->Session->write('Question_text', $this->data['QnaQuestion']['question_text']);
				$this->Session->write('Question_context', $this->data['QnaQuestion']['question_context']);
				$this->Session->write('Question_id', $this->QnaQuestion->id);
                                $this->Session->write('questionId',$this->QnaQuestion->id);
			}
			
		} else if($this->Session->read('Question_text') != '' && $this->Session->read('Question_context') != '') {
			
			$this->set('Question_text', $this->Session->read('Question_text'));
			$this->set('Question_context', $this->Session->read('Question_context'));
			$this->set('Question_id', $this->Session->read('Question_id'));
                        $this->Session->write('questionId',$this->Session->read('Question_id'));
		} else {
			$this->redirect(array('controller' => 'fronts', 'action' => 'index'));
		}
		
	}
	
	function upvote($answerid = 0) {
		
		if($this->Session->read('Auth.User.id') != '' && $answerid != 0) {
				
			$answer = $this->QnaAnswer->find('first',array('conditions' => array('QnaAnswer.id' => $answerid)));
			
			$cachefile = WWW_ROOT."cache/question-".$answer['QnaAnswer']['question_id'].".html";
			unlink($cachefile);
		
			$oldVotes = $answer['QnaAnswer']['upvotes'];
			$upvote_users = $answer['QnaAnswer']['upvote_users'];
			
			$current_userid = $this->Session->read('Auth.User.id');
			
			$already_voted = 0;
			
			/*if($upvote_users) {
				
				$userids = explode(',',$upvote_users);
					
				for($i=0;$i<count($userids);$i++){
				
					if(isset($userids[$i]) && $userids[$i] != '') {
						
						if($userids[$i] == $current_userid) {
							$already_voted++;
						}
					}
				}
			}*/
			
			if($already_voted == 0) {
				
				$oldVotes++;
				
				$answer['QnaAnswer']['upvotes'] = $oldVotes;
				
				/*if($answer['QnaAnswer']['upvote_users']){
					$answer['QnaAnswer']['upvote_users'] = $answer['QnaAnswer']['upvote_users'].",".$current_userid;
				}
				else
					$answer['QnaAnswer']['upvote_users'] = $current_userid;*/
				
				$this->QnaAnswer->save($answer);
				
				echo json_encode(array('value' => $oldVotes, 'id' => $answer['QnaAnswer']['id']));
			}
			
			exit();
				
		} else {
			
			echo json_encode(array('value' => -1));
			exit();
		}
	}
	
	function add_question(){
		
		if ($this->RequestHandler->isAjax()) {
			Configure::write('debug', 0);
			
			$checkBoxes = $_REQUEST['checkBoxes'];
			$qId = $_REQUEST['qId'];
			$this->set('Question_id', $qId);
            $this->Session->write('checkBoxes', $checkBoxes);
			$this->Session->write('Question_id', $qId);


			$this->render(false);
			echo json_encode(array('value' => '1'));
			exit();
		} else if(empty($this->data) == false && $this->Session->read('Auth.User.id') !=''){
				
			$Question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.id' =>$this->Session->read('Question_id'))));
			$Question['QnaQuestion']['question_text'] = $this->Session->read('Question_text');
			$Question['QnaQuestion']['question_context'] = $this->Session->read('Question_context');
			$Question['QnaQuestion']['isdraft'] = 1;
            $Question['QnaQuestion']['user_id'] = $this->Session->read('Auth.User.id');
			
			$this->QnaQuestion->save($Question);
				
			$this->QnaQuestion->createUrlKey($this->QnaQuestion->id);
			if($this->Session->read('Question_text') != '')
				$this->Session->delete('Question_text');
			if($this->Session->read('Question_context') != '')
				$this->Session->delete('Question_context');
			if($this->Session->read('Question_id') != '')
				$this->Session->delete('Question_id');	
			
			
			$checkBoxes = $this->data['QnaQuestion']['select'];
			$cat = explode(',',$checkBoxes);
			
			for($i=0;$i<count($cat);$i++){
			
				if(isset($cat[$i]) && $cat[$i] != '') {
					
					$topic = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$cat[$i])));
		               if(empty($topic)) {
			
                                $this->Topic->create( );
				    $this->Topic->save(array('autocomplete_text'=>$cat[$i]));

                                   $topicid = $this->Topic->getLastInsertID();
                                   $this->Topic->updateAll(array('Topic.topic_subset' => 0),array('Topic.id ' => $topicid));
                                   $topic = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topicid)));
				    
				}
					
					$Question_category['qna_question_id'] = $this->QnaQuestion->id;
					$Question_category['topic_id'] = $topic['Topic']['id'];
					
					$this->QnaQuestionCategory->save($Question_category);
					$this->QnaQuestionCategory->id = '';
					$this->Topic->id = '';
				}
			}
		
			$userData = $this->User->find('first',
					array(
							'contain'=>array(
									'UserReference'=>array(
											'fields'=>array('first_name','last_name','zipcode'),
									),
									'UserImage' ,
							),
							'conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))
					)
						
			);
			
			//Send Question Email
			//question_submitted


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $helpemail = "help@guild.im";
                           $to = $helpemail.",".$userData['User']['username'];
                           $subject = "Your question has been submitted";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("question_submitted");
                           $data .= "&merge_userfname=".urlencode($userData['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

			
			$this->redirect(array('action' => 'thanks_for_asking'));
		}
	}
	
	function thanks_for_asking(){

          $this->layout= 'defaultnew';
		
	}
	
	function question($questionUrl = null, $focusid = null){

               $this->layout= 'defaultnew';
		
		if($questionUrl == "") {
			
			$this->redirect(array('controller' => 'qna', 'action' => 'answers'));
		} else {
			 $q = $this->QnaQuestion->find('first',array('conditions' => array('QnaQuestion.url_key' => $questionUrl)));

			 $id = $q['QnaQuestion']['id'];
                         $title = $q['QnaQuestion']['question_text'];

                         $this->Session->write('qna_question',$questionUrl);
			if($id == "")
			{
				$this->redirect(array('controller' => 'qna', 'action' => 'index'));
			}
		}
		
              	
		if($id != null) {
                    
                        if($this->Session->read('Auth.User.id')!=''){



					$loggedinuser = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name','zipcode','linkedin_headline'),
											),
											'UserImage' ,
                                                                              'Social',
									),
									'conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))
							)
			
					);
                                         
                                        $this->set("loggedinuser",$loggedinuser);


                         }
			
			$cachefile = WWW_ROOT."cache/question-".$id.".html";
			$this->set('cache_keyword', $id);
			
			if($this->Session->read('Auth.User.role_id') == 2) {
					
				unlink($cachefile);
			} 
				
			if (file_exists($cachefile)) {
                               $this->set('focusid', $focusid);
			      $this->set("title_for_layout",$title);
				
                                    $this->set('Cached', true);

			       if($focusid !='' && $focusid != 'email_notification'){
                          
                 	        $this->set('focusid', $focusid);
                             }
                            else if ($this->Session->read('Auth.User.id') == '' && $focusid == 'email_notification') { //Ask for login
                             
				  $this->Session->write('qnaconsult',$questionUrl);
                              $this->set('openloginpopup',1);

			        }
                            else if($this->Session->read('Auth.User.role_id') == 3  && $focusid == 'email_notification'){ //No answer and user is mentee
			
				$this->redirect(array('controller' => 'qna', 'action' => 'index'));
			        }

                               	
			} else {
			
			//Find all answers to this question
			$answers = $this->QnaAnswer->find('all',array('conditions' => array('QnaAnswer.question_id' => $id), 'order'=>array('QnaAnswer.upvotes desc','QnaAnswer.id asc')));
				
			$this->set("answers",'');
			if(empty($answers) == false) {
			
				$finalAnswers = array();
			
				$aCount = 0;
				foreach($answers as $answer){
						
					$userData = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name','zipcode','linkedin_headline'),
											),
											'UserImage' ,
                                                                              'Social',
									),
									'conditions'=>array('User.id'=>$answer['QnaAnswer']['member_id'])
							)
			
					);
					if(empty($userData['UserImage']) == false)
						$answer['QnaAnswer']['user_image'] = $userData['UserImage'][0]['image_name'];
					$answer['QnaAnswer']['first_name'] = $userData['UserReference']['first_name'];
					$answer['QnaAnswer']['last_name'] = $userData['UserReference']['last_name'];
					$answer['QnaAnswer']['url_key'] = $userData['User']['url_key'];
					$answer['QnaAnswer']['status'] = $userData['User']['status'];
					$answer['QnaAnswer']['linkedin_headline'] = $userData['UserReference']['linkedin_headline'];
                                   $socialCount=0;
                                   
                                   while(count($userData['Social'])>$socialCount){
                                    
					if(($pos =strpos($userData['Social'][$socialCount]['social_name'],'twitter.com'))!==false)
						{
							$twitterURL = $userData['Social'][$socialCount]['social_name'];
                                                $path = explode("/", $twitterURL);
                                                $twitter_handle = end($path); 
                                                $answer['QnaAnswer']['social'] = $twitter_handle;    
						}
                                          $socialCount++;
						} 
                                           
                                
                                   //find all answers by this member
		                     $totalanswers = $this->QnaAnswer->find('all',array('order'=>'QnaAnswer.id desc', 'conditions' => array('QnaAnswer.member_id' => $answer['QnaAnswer']['member_id'])));
		                     $answeredQs = array();
		                     $aqCount = 0;
		                     foreach ($totalanswers as $tanswer){
			 
			              $tquestion = $this->QnaQuestion->find('first',array('conditions' => array('QnaQuestion.id' => $tanswer['QnaAnswer']['question_id'])));
			              $tquestion['answer_text'] = $tanswer['QnaAnswer']['answer_text'];
			              $answeredQs[$tanswer['QnaAnswer']['question_id']] = $tquestion;

		                      }  
					$answer['QnaAnswer']['ans_count'] = count($answeredQs);

		                      $tinsight = $this->Insight->find('count',array('conditions' => array('Insight.user_id' => $answer['QnaAnswer']['member_id'], 'Insight.published' => 1)));
                                        $answer['QnaAnswer']['insight_count'] = $tinsight;
						
					$finalAnswers[$aCount++] = $answer;
				}

				$this->set("answers",$finalAnswers);



                                

                if($focusid !=''){
                 $this->set('focusid', $focusid);
                }
			} else if($this->Session->read('Auth.User.role_id') == 3){ //No answer and user is mentee
			
				$this->redirect(array('controller' => 'qna', 'action' => 'index'));
			} else if ($this->Session->read('Auth.User.id') == '') { //Ask for login
				  $this->Session->write('qnaconsult',$questionUrl);
                              $this->set('openloginpopup',1);
				//$this->redirect(array('controller'=>'fronts','action'=>'index','question_url:'.$questionUrl));
			       
                      }
			
			$this->QnaQuestion->Behaviors->attach('Containable');
			$question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.id' => $id)));
			
			$this->set("title_for_layout",$question['QnaQuestion']['question_text']);
			
			$this->set("question",$question);
			
			//Find question with similar categories
			$qCatQs = array();
			foreach ($question['QnaQuestionCategory'] as $qCat) {
				
				$QuestionCs = $this->QnaQuestionCategory->find('all', array('conditions' => array('QnaQuestionCategory.category_id' => $qCat['category_id'])));
				$number = count($QuestionCs);
                            //prd($number);
                            if($number == 1)
                        {
                         $id = 793;
                         $question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.id' => $id)));

			    $qCatQs = array();
			    foreach ($question['QnaQuestionCategory'] as $qCat) {
				
			    $QuestionCs = $this->QnaQuestionCategory->find('all', array('conditions' => array('QnaQuestionCategory.category_id' => $qCat['category_id'])));


				foreach ($QuestionCs as $v) {
					
					if(isset($qCatQs[$v['QnaQuestionCategory']['qna_question_id']]))
						$qCatQs[$v['QnaQuestionCategory']['qna_question_id']]++;
					else 
						$qCatQs[$v['QnaQuestionCategory']['qna_question_id']] = 1;
				}
			}
			$sameQ1Id1 = -1;
			$sameQ2Id1 = -1;
			$sameQ3Id1 = -1;
			$sameQ1Id2 = -1;
			$sameQ2Id2 = -1;
			$sameQ3Id2 = -1;
			$sameQ1Id3 = -1;
			$sameQ2Id3 = -1;
			$sameQ3Id3 = -1;
			
			foreach ($qCatQs as $key => $sV) {
				
				if($key != $id) {
					if($sV == 1) {
						if($sameQ1Id1 == -1){
							$sameQ1Id1 = $key;
						}else if($sameQ2Id1 == -1){
							$sameQ2Id1 = $key;
						} else if($sameQ3Id1 == -1){
							$sameQ3Id1 = $key;
						}
						
					} else if($sV == 2) {
						
						if($sameQ1Id2 == -1){
							$sameQ1Id2 = $key;
						}else if($sameQ2Id2 == -1){
							$sameQ2Id2 = $key;
						} else if($sameQ3Id2 == -1){
							$sameQ3Id2 = $key;
						}
						
					} else {
						
						if($sameQ1Id3 == -1){
							$sameQ1Id3 = $key;
						}else if($sameQ2Id3 == -1){
							$sameQ2Id3 = $key;
						} else if($sameQ3Id3 == -1){
							$sameQ3Id3 = $key;
						}
					}
				}
			}
			
			$finalSameQ = array();
			$countSa = 0;
			if($sameQ1Id3 != -1) {
				$finalSameQ[$countSa++] = $sameQ1Id3;
			}
			if($sameQ2Id3 != -1) {
				$finalSameQ[$countSa++] =  $sameQ2Id3;
			}
			if($sameQ3Id3 != -1) {
				$finalSameQ[$countSa++] =  $sameQ3Id3;
			}
			if($countSa < 3 && $sameQ1Id2 != -1) {
				$finalSameQ[$countSa++] =  $sameQ1Id2;
			}
			if($countSa < 3 && $sameQ2Id2 != -1) {
				$finalSameQ[$countSa++] =  $sameQ2Id2;
			}
			if($countSa < 3 && $sameQ3Id2 != -1) {
				$finalSameQ[$countSa++] =  $sameQ3Id2;
			}
			if($countSa < 3 && $sameQ1Id1 != -1) {
				$finalSameQ[$countSa++] =  $sameQ1Id1;
			}
			if($countSa < 3 && $sameQ2Id1 != -1) {
				$finalSameQ[$countSa++] =  $sameQ2Id1;
			}
			if($countSa < 3 && $sameQ3Id1 != -1) {
				$finalSameQ[$countSa++] =  $sameQ3Id1;
			}
			
			$questionArray = array();
			$qCount = 0;
			
			foreach ($finalSameQ as $sameValue){
				
				$question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.id' => $sameValue)));
				
				//Find other answers to this question
				$answers = $this->QnaAnswer->find('all',array('conditions' => array('QnaAnswer.question_id' => $sameValue)));
					
				$finalAnswers = array();
				
				foreach($answers as $answer){
						
					$userData = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name','zipcode','linkedin_headline'),
											),
											'UserImage' ,
									),
									'conditions'=>array('User.id'=>$answer['QnaAnswer']['member_id'])
							)
				
					);
					if(empty($userData['UserImage']) == false)
					$question['User'][$answer['QnaAnswer']['member_id']]['user_image'] = $userData['UserImage'][0]['image_name'];
					$question['User'][$answer['QnaAnswer']['member_id']]['first_name'] = $userData['UserReference']['first_name'];
					$question['User'][$answer['QnaAnswer']['member_id']]['last_name'] = $userData['UserReference']['last_name'];
					$question['User'][$answer['QnaAnswer']['member_id']]['url_key'] = $userData['User']['url_key'];
					$question['User'][$answer['QnaAnswer']['member_id']]['member_id'] = $answer['QnaAnswer']['member_id'];
						
				}
					
				$questionArray[$qCount++] = $question;
				
			}
			$this->set('questionArray', $questionArray);

                      
                        }





				foreach ($QuestionCs as $v) {
					
					if(isset($qCatQs[$v['QnaQuestionCategory']['qna_question_id']]))
						$qCatQs[$v['QnaQuestionCategory']['qna_question_id']]++;
					else 
						$qCatQs[$v['QnaQuestionCategory']['qna_question_id']] = 1;
				}
			}
			$sameQ1Id1 = -1;
			$sameQ2Id1 = -1;
			$sameQ3Id1 = -1;
			$sameQ1Id2 = -1;
			$sameQ2Id2 = -1;
			$sameQ3Id2 = -1;
			$sameQ1Id3 = -1;
			$sameQ2Id3 = -1;
			$sameQ3Id3 = -1;
			
			foreach ($qCatQs as $key => $sV) {
				
				if($key != $id) {
					if($sV == 1) {
						if($sameQ1Id1 == -1){
							$sameQ1Id1 = $key;
						}else if($sameQ2Id1 == -1){
							$sameQ2Id1 = $key;
						} else if($sameQ3Id1 == -1){
							$sameQ3Id1 = $key;
						}
						
					} else if($sV == 2) {
						
						if($sameQ1Id2 == -1){
							$sameQ1Id2 = $key;
						}else if($sameQ2Id2 == -1){
							$sameQ2Id2 = $key;
						} else if($sameQ3Id2 == -1){
							$sameQ3Id2 = $key;
						}
						
					} else {
						
						if($sameQ1Id3 == -1){
							$sameQ1Id3 = $key;
						}else if($sameQ2Id3 == -1){
							$sameQ2Id3 = $key;
						} else if($sameQ3Id3 == -1){
							$sameQ3Id3 = $key;
						}
					}
				}
			}
			
			$finalSameQ = array();
			$countSa = 0;
			if($sameQ1Id3 != -1) {
				$finalSameQ[$countSa++] = $sameQ1Id3;
			}
			if($sameQ2Id3 != -1) {
				$finalSameQ[$countSa++] =  $sameQ2Id3;
			}
			if($sameQ3Id3 != -1) {
				$finalSameQ[$countSa++] =  $sameQ3Id3;
			}
			if($countSa < 3 && $sameQ1Id2 != -1) {
				$finalSameQ[$countSa++] =  $sameQ1Id2;
			}
			if($countSa < 3 && $sameQ2Id2 != -1) {
				$finalSameQ[$countSa++] =  $sameQ2Id2;
			}
			if($countSa < 3 && $sameQ3Id2 != -1) {
				$finalSameQ[$countSa++] =  $sameQ3Id2;
			}
			if($countSa < 3 && $sameQ1Id1 != -1) {
				$finalSameQ[$countSa++] =  $sameQ1Id1;
			}
			if($countSa < 3 && $sameQ2Id1 != -1) {
				$finalSameQ[$countSa++] =  $sameQ2Id1;
			}
			if($countSa < 3 && $sameQ3Id1 != -1) {
				$finalSameQ[$countSa++] =  $sameQ3Id1;
			}
			
			$questionArray = array();
			$qCount = 0;
			
			foreach ($finalSameQ as $sameValue){
				
				$question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.id' => $sameValue)));
				
				//Find other answers to this question
				$answers = $this->QnaAnswer->find('all',array('conditions' => array('QnaAnswer.question_id' => $sameValue)));
					
				$finalAnswers = array();
				
				foreach($answers as $answer){
						
					$userData = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name','zipcode'),
											),
											'UserImage' ,
									),
									'conditions'=>array('User.id'=>$answer['QnaAnswer']['member_id'])
							)
				
					);
					if(empty($userData['UserImage']) == false)
						$question['User'][$answer['QnaAnswer']['member_id']]['user_image'] = $userData['UserImage'][0]['image_name'];
					$question['User'][$answer['QnaAnswer']['member_id']]['first_name'] = $userData['UserReference']['first_name'];
					$question['User'][$answer['QnaAnswer']['member_id']]['last_name'] = $userData['UserReference']['last_name'];
					$question['User'][$answer['QnaAnswer']['member_id']]['url_key'] = $userData['User']['url_key'];
					$question['User'][$answer['QnaAnswer']['member_id']]['member_id'] = $answer['QnaAnswer']['member_id'];
						
				}
				if($answers != null){					
				$questionArray[$qCount++] = $question;
				}
			}
			$this->set('questionArray', $questionArray);
			 //prd($questionArray);
			//End -- similar category questions
			    if ($this->Session->read('Auth.User.id') == '' && $text != ''){// logged out consultant check his asked question mail view it here link
                                      //$this->Session->write('menteeActEmail',$text);
                                      $this->Session->write('qnaconsult',$questionUrl);
                                      $this->set('openloginpopup',1);
                       
                          }
                        
                            
                          if($this->Session->read('Auth.User.role_id') == 2) {
				
                            $this->User->Behaviors->attach('Containable');
				
				$userData = $this->User->find('first',
						array(
								'contain'=>array(
										'UserReference'=>array(
												'fields'=>array('first_name','last_name','zipcode','linkedin_headline'),
										),
										'UserImage' ,
								),
								'conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))
						)
				
				);
				
				//find all answers by this member
				$answersCount = $this->QnaAnswer->find('count',array('conditions' => array('QnaAnswer.member_id' => $this->Session->read('Auth.User.id'))));
				$userData['ans_count'] = $answersCount;
				
				$this->set("userData", $userData);
				$this->set("referrer", $this->referer());
			}
			
			//GetQ categories
			$result = $this->Topic->find('all');
			$qcategories = array();
			foreach($result as $value){
				$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
			}
			$this->set("categories",$qcategories);
			}
		} else {
			
			$this->redirect(array('controller' => 'qna', 'action' => 'answers'));
		}
		
	   $plan_type= $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
          $this->set("plan_type",$plan_type['User']['plan_type']);
        
    }
	
	
	function add_answer(){
		
		if($this->Session->read('Auth.User.id') != '' &&  $this->Session->read('Auth.User.role_id') == 2){ 
			if(empty($this->data) == false) {
				
				$cachefile = WWW_ROOT."cache/question-".$this->data['QnaAnswer']['id'].".html";
				unlink($cachefile);
			
				$QnaAnswer['question_id'] = $this->data['QnaAnswer']['id'];
				$QnaAnswer['answer_text'] = trim($this->data['QnaAnswer']['answer_text']);
				$QnaAnswer['member_id'] = $this->Session->read('Auth.User.id');
				
				if($this->QnaAnswer->save($QnaAnswer)) {
					
					//Send question email
					//answer_submitted
					
					$question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.id' => $this->data['QnaAnswer']['id'])));

                                  
                                   $dbchecck = "Question Id : ".$this->data['QnaAnswer']['id'];

                                   $visitor = $this->Prospect->find('first', array('conditions' => array('Prospect.answer' => $dbchecck)));

					$userData = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name','zipcode'),
											),
											'UserImage' ,
									),
									'conditions'=>array('User.id'=>$question['QnaQuestion']['user_id'])
							)
					
					);
					
					if(empty($userData) == false) {
						


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $helpemail = "help@guild.im";
                           $to = $helpemail.",".$userData['User']['username'];
                           $subject = "Your question has been answered";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("answer_submitted");
                           $data .= "&merge_userfname=".urlencode($userData['UserReference']['first_name']);
                           $data .= "&merge_questionurl=".urlencode($question['QnaQuestion']['url_key']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

					}

					elseif(empty($visitor) == false) {
						
                                        

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $helpemail = "help@guild.im";
                           $to = $helpemail.",".$userData['User']['username'];
                           $subject = "Your question has been answered";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("answer_submitted");
                           $data .= "&merge_userfname=".urlencode($visitor['Prospect']['firstname']);
                           $data .= "&merge_questionurl=".urlencode($question['QnaQuestion']['url_key']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


					}
					
					$this->redirect(array('action' => 'question', $question['QnaQuestion']['url_key']));
				}
			}
		} else {
			
			$this->redirect(array('controller' => 'qna', 'action' => 'answers'));
		}
	}
	
	function category($qCat=null){
 
           $this->layout= 'defaultnew';
           $this->Session->write('qna_category',$qCat);

			//GetQ categories
			$result = $this->Topic->find('all');
			$qcategories = array();
			foreach($result as $value){
				$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
			}
			$this->set("categories",$qcategories);


                        $unique_topics = $this->QnaQuestionCategory->find('all', array('fields'=>'DISTINCT topic_id,qna_question_id'));
 
                        $inunique_topics = $this->InsightCategory->find('all', array('fields'=>'DISTINCT topic_id,insight_id'));  

                        $qtopiclist = array();
                        $intopiclist = array();
                        $topiclist = array();
			foreach($unique_topics as $key=>$aValue){
				$answers = $this->QnaAnswer->find('first',array('conditions' => array('QnaAnswer.question_id' => $aValue['QnaQuestionCategory']['qna_question_id'])));
                                if($answers['QnaAnswer']['answer_text'] !=''){
 

				$qtopiclist[$key] = $qcategories[$aValue['QnaQuestionCategory']['topic_id']];
                               }
			}

			foreach($inunique_topics as $key=>$aValue){
				$insight = $this->Insight->find('first', array('conditions' => array('AND' => array(array('Insight.id' => $aValue['InsightCategory']['insight_id']),array('Insight.published'=>1)))));
                                if($insight['Insight']['insight']!=''){
				$intopiclist[$key] = $qcategories[$aValue['InsightCategory']['topic_id']];
                                }
			}

                        $topiclist = (array_unique(array_merge($qtopiclist,$intopiclist)));
                        
                        $this->set("topiclist",$topiclist); 

           $isCron = $_GET["isCron"];
		
		if($qCat != null) {
			
              
                   $cachefile = WWW_ROOT."cache/qna-".$qCat.".html";
                    $this->set('cache_keyword', $qCat);

			if($isCron == 1) { //Deleting this file
			
				unlink($cachefile);
			 }
                    

                    if (file_exists($cachefile)) {
                            
                             $keyword = $qCat;
                             if(false !== stripos($qCat,"_and"))
                             $keyword = str_replace("_and","&",$qCat);

                            if(false !== stripos($qCat,"_slash"))
                            $keyword = str_replace("_slash","/",$qCat);
                         
                             $keyword = str_replace("-"," ",$keyword);

                             if(false !== stripos($keyword,"Non profit"))
                             $keyword = str_replace("Non profit","Non-profit",$keyword);

                             if(false !== stripos($keyword,"Post merger integration (PMI)"))
                             $keyword = str_replace("Post merger integration (PMI)","Post-merger integration (PMI)",$keyword);

                            $this->set("title_for_layout","$keyword");

	                     $this->set('Cached', true);

				
			} else {

                           $keyword = $qCat;
                           if(false !== stripos($qCat,"_and"))
                           $keyword = str_replace("_and","&",$qCat);

                           if(false !== stripos($qCat,"_slash"))
                           $keyword = str_replace("_slash","/",$qCat);

                           $keyword = str_replace("-"," ",$keyword);

                           if(false !== stripos($keyword,"Non profit"))
                           $keyword = str_replace("Non profit","Non-profit",$keyword);

                           if(false !== stripos($keyword,"Post merger integration (PMI)"))
                           $keyword = str_replace("Post merger integration (PMI)","Post-merger integration (PMI)",$keyword);

			$this->QnaQuestion->Behaviors->attach('Containable');

		       $topic = $this->Topic->find('first', array('conditions' => array('Topic.autocomplete_text' => $keyword)));
                    
                     $id = $topic['Topic']['id'];
                     
                     
                     $this->set("title_for_layout","$keyword");

					if($isCron != 1) {//Saving qna caategory Keywords
                                                $foundindb = $this->CacheUrl->find('first', array('conditions' => array('CacheUrl.keyword' => $keyword)));

                                                if($foundindb['CacheUrl']['id'] ==''){//only enter when entry already not there

						$search_url = $_SERVER['REQUEST_URI'];

						$search_details = array('keyword'=>$keyword,'url'=>'https://www.guild.im'.$search_url);

						$this->CacheUrl->save($search_details);
						

                                             }
					}//End


			$QuestionCs = $this->QnaQuestionCategory->find('all', array('conditions' => array('QnaQuestionCategory.topic_id' => $id), 'order'=>'QnaQuestionCategory.id desc'));
				
			$questionArray = array();
			$qCount = 0;
			foreach($QuestionCs as $aValue){
				$question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.id' => $aValue['QnaQuestionCategory']['qna_question_id'], 'QnaQuestion.isdraft'=>'0')));
			
				if(isset($question) && $question != '') 
				{
					//Find all answers to this question and user images
				
					$answers = $this->QnaAnswer->find('all',array('conditions' => array('QnaAnswer.question_id' => $aValue['QnaQuestionCategory']['qna_question_id'])));
					$aCount = 0;
					
					foreach($answers as $answer){
				
						$userData = $this->User->find('first',
								array(
										'contain'=>array(
												'UserReference'=>array(
														'fields'=>array('first_name','last_name','zipcode'),
												),
												'UserImage' ,
										),
										'conditions'=>array('User.id'=>$answer['QnaAnswer']['member_id'])
								)
									
						);
						if(empty($userData['UserImage']) == false)
						$question['User'][$answer['QnaAnswer']['member_id']]['user_image'] = $userData['UserImage'][0]['image_name'];
						$question['User'][$answer['QnaAnswer']['member_id']]['first_name'] = $userData['UserReference']['first_name'];
						$question['User'][$answer['QnaAnswer']['member_id']]['last_name'] = $userData['UserReference']['last_name'];
						$question['User'][$answer['QnaAnswer']['member_id']]['url_key'] = $userData['User']['url_key'];
						$question['User'][$answer['QnaAnswer']['member_id']]['member_id'] = $answer['QnaAnswer']['member_id'];
						$aCount++;
				
					}
				
					if($aCount != 0) { //Unanswered question

						$question['QnaQuestion']['haveAnswer'] = 1;
                                                $questionArray[$qCount++] = $question;
					}
					
					
				}
			}
			$this->set('questionArray', $questionArray);
			
			//Get Insights
			$this->Insight->Behaviors->attach('Containable');
			$InsightCs = $this->InsightCategory->find('all', array('conditions' => array('InsightCategory.topic_id' => $id,'Insight.published'=>1), 'order'=>'InsightCategory.id desc'));
				
			$insightArray = array();
			$qCount = 0;
                        $this->loadModel('City');
			foreach($InsightCs as $aValue){
				$insight = $this->Insight->find('first', array('conditions' => array('Insight.id' => $aValue['InsightCategory']['insight_id'])));
					
				if(isset($insight) && $insight != '')
				{
					$this->User->Behaviors->attach('Containable');
						
					$userData = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name','linkedin_headline','zipcode'),
											),
											'UserImage' ,
									),
									'conditions'=>array('User.id'=>$insight['Insight']['user_id'])
							)
								
					);

		                        $dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($userData['UserReference']['zipcode']))));
					
					if(empty($userData['UserImage']) == false)
					$insight['User']['user_image'] = $userData['UserImage'][0]['image_name'];
					$insight['User']['first_name'] = $userData['UserReference']['first_name'];
					$insight['User']['last_name'] = $userData['UserReference']['last_name'];
					$insight['User']['url_key'] = $userData['User']['url_key'];
                                        $insight['User']['headline'] = $userData['UserReference']['linkedin_headline'];
					$insight['User']['city'] = $dataZip['City']['city_name'];
					$insight['User']['state'] = $dataZip['City']['state'];
					
					$insightArray[$qCount++] = $insight;
				}
			}
			$this->set('insights', $insightArray);
			
			

			
			$this->set('queryString',$qcategories[$id]);
			
			$this->set("title_for_layout",$qcategories[$id]." | QnA Search");
		 }	
		} else {
			
			$this->redirect(array('controller' => 'qna', 'action' => 'index'));
		}
			 
			
	}
	
	function ignore_question(){
	
              $id = trim($_REQUEST['ques_id']);
		if($this->Session->read('Auth.User.id') != '' &&  $this->Session->read('Auth.User.role_id') == 2){
			if($id != null) {
				$ignored_Q['member_id'] = $this->Session->read('Auth.User.id');
				$ignored_Q['question_id'] = $id;
					
				if($this->QnaIgnoredQuestion->save($ignored_Q)) {
		
					    		echo json_encode(array('id'=>$id));
    		                                   $this->layout = '';
    		                                   $this->render(false);
    		                                   exit();
				}
			}
		} else {
				
			$this->redirect(array('controller' => 'qna', 'action' => 'answers'));
		}
	}
	

	function admin_media_add() {

   	if($this->Auth->user('id') == 1){

		$keywords = $this->Topic->find('all',array(
				'fields' => array('DISTINCT (Topic.autocomplete_text) AS autocomplete_text'),
				'conditions'=>array('Topic.isName'=>0)
		));

		$this->set('keywords',$keywords);
		
		$key = array();
		$cn = 0;
		foreach($keywords as $value){
			$key[$cn++] = $value['Topic']['autocomplete_text'];
		}
		$this->set('allKeywords', $key);

		$this->layout = 'admin';  
		if(!empty($this->data)){


			$checkBoxes = $this->data['MediaRecords']['select'];
			$cat = explode(',',$checkBoxes);

			
                     $query['MediaRecords']['deadline']= $this->data['MediaRecords']['deadline'];
                     $query['MediaRecords']['media_outlet']= $this->data['MediaRecords']['media_outlet'];
                     $query['MediaRecords']['subject']= $this->data['MediaRecords']['subject'];
                    
                     $query['MediaRecords']['reporter_query']= $this->data['MediaRecords']['reporter_query'];
                     $query['MediaRecords']['requirement']= $this->data['MediaRecords']['requirement'];
                     $query['MediaRecords']['reporter_email']= $this->data['MediaRecords']['reporter_email'];
                     $query['MediaRecords']['reporter_name']= $this->data['MediaRecords']['reporter_name'];

                     if($cat[0] !='')
                     $query['MediaRecords']['category1']= $cat[0];
                     if($cat[1] !='')
                     $query['MediaRecords']['category2']= $cat[1];
                     if($cat[2] !='')
                     $query['MediaRecords']['category3']= $cat[2];
                                          
			$this->MediaRecords->save($query);

                     if($this->data['MediaRecords']['checkbox'] == 'Yes'){
                     $media_id = $this->MediaRecords->getLastInsertID();
                     $question['question_text']= $this->data['MediaRecords']['subject'];
                     $question['media_query_id']= $media_id;
                     $question['isdraft']= 0;
                     $question['question_context']= $this->data['MediaRecords']['reporter_query'];

                     $this->QnaQuestion->save($question);
                     $id = $this->QnaQuestion->getLastInsertID();

			$urlKey = preg_replace('/\PL/u', '-', $this->data['MediaRecords']['subject']);
			$urlKey .= '-'.$id;
                       $this->QnaQuestion->updateAll(array('QnaQuestion.url_key'=>"'".$urlKey."'"),array('QnaQuestion.id'=>$id));



			for($i=0;$i<count($cat);$i++){
			
				if(isset($cat[$i]) && $cat[$i] != '') {
					
			       $topic = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$cat[$i])));
		               if(empty($topic)) {
			
                                $this->Topic->create( );
				    $this->Topic->save(array('autocomplete_text'=>$cat[$i]));

                                   $topicid = $this->Topic->getLastInsertID();
                                   $this->Topic->updateAll(array('Topic.topic_subset' => 0),array('Topic.id ' => $topicid));
                                   $topic = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topicid)));
				    
				}
					
					$Question_category['qna_question_id'] = $id;
					$Question_category['topic_id'] = $topic['Topic']['id'];
					
					$this->QnaQuestionCategory->save($Question_category);
					$this->QnaQuestionCategory->id = '';
					$this->Topic->id = '';
				}
			}

                     }
			$this->Session->setFlash(__('Media query has added.', true), 'flash_good');
			$this->redirect(array('action' => 'admin_media_add'));
		}
          
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }

	}

	function admin_media_record(){
   	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		$media_records = $this->MediaRecords->find('all', array('order'=>'deadline ASC'));

              
		$this->set("media_records",$media_records);
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}
      function admin_editmediarecord($id = null){
   	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
              $this->pageTitle = __('Edit Media', true);
		if(!$id && empty($this->data)) {
			$this->Session->setFlash('Invalid Id');
			$this->redirect(array('action' => 'media_record'));
		}


		if(!empty($this->data))
		{
                   
			/**$record = $this->MediaRecords->find('first', array('conditions' => array('MediaRecords.id' => $this->data['MediaRecords']['id'])));

			$record['MediaRecords']['deadline'] = $this->data['MediaRecords']['deadline'];
			$record['MediaRecords']['media_outlet'] = $this->data['MediaRecords']['media_outlet'];
			$record['MediaRecords']['subject'] = $this->data['MediaRecords']['subject'];
			$record['MediaRecords']['reporter_query'] = $this->data['MediaRecords']['reporter_query'];
			$record['MediaRecords']['requirement'] = $this->data['MediaRecords']['requirement'];
			$record['MediaRecords']['category'] = $this->data['MediaRecords']['category'];**/

                      $this->MediaRecords->save($this->data);
                    
                	$this->Session->setFlash(__('Media query has updated.', true), 'flash_good');
			$this->redirect(array('action' => 'admin_media_record'));
		} else {
			$this->data = $this->MediaRecords->read(null, $id);
		}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }


      }

       function admin_media_delete($id = null) {
   	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
        if (!$id) {
            $this->Session->setFlash('Invalid id for Admin');
            $this->redirect($this->referer());
        }
        $admin = $this->MediaRecords->read(null, $id);
        if (empty($admin)) {
            $this->Session->setFlash('Invalid Media Query Id', 'admin_flash_bad');
             $this->redirect($this->referer());
        }

        if ($this->MediaRecords->delete($id)) {
 
            $this->MediaRecords->deleteAll(array('MediaRecords.id'=>$id));
            $this->Session->setFlash('Media Query has been deleted successfully', 'admin_flash_good');
            $this->redirect($this->referer());
        }
        $this->Session->setFlash('Media Query has not been deleted', 'admin_flash_bad');
         $this->redirect($this->referer());
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }




        function share_question() {


		
             if(!empty($this->data)) {

			$this->User->Behaviors->attach('Containable');
			$user = $this->User->findById($this->Auth->user('id'));

    	      


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = $user['User']['username'];
                           $fromName = $user['UserReference']['first_name']." ".$user['UserReference']['last_name'];
                           $websiteurl = SITE_URL;
                           $to = $this->data[EAddress];
                           $link = SITE_URL;
                           $subject = $this->data[ESubject];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("share_question");
                           $data .= "&merge_fname=".urlencode(nl2br($this->data[FName]));
                           $data .= "&merge_emailcontent=".urlencode($this->data[EText]);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
                          $this->redirect($this->referer());
                     }
                



	}


        function share_answer() {


		
             if(!empty($this->data)) {

			$this->User->Behaviors->attach('Containable');
			$user = $this->User->findById($this->Auth->user('id'));

    	      


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = $user['User']['username'];
                           $fromName = $user['UserReference']['first_name']." ".$user['UserReference']['last_name'];
                           $websiteurl = SITE_URL;
                           $to = $this->data[EAddress];
                           $link = SITE_URL;
                           $subject = $this->data[ESubject];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("share_answer");
                           $data .= "&merge_fname=".urlencode(nl2br($this->data[FName]));
                           $data .= "&merge_emailcontent=".urlencode($this->data[EText]);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
                          $this->redirect($this->referer());
                     }
                



	}



	
}   
?>