<?php
/**
 * Roundtable Controller
 *
 * PHP version 5
 *
 * @category Controller
 */
//require('../webroot/mandrill/Mandrill.php');

class RoundtableController extends AppController{
	/**
	* Controller name
	*
	* @var string
	* @access public
	*/
	var $name = 'Roundtable';
	var $uses = array('StaticPage', 'Roundtable','Prospect','User','UserReference');
	
	var $helpers = array('General', 'Form', 'Ajax', 'javascript', 'Paginator');
	var $components = array("Session", "Email", "Auth", "RequestHandler" ,"Upload");
	
	/**
	 * Models used by the Controller
	 *
	 * @var array
	 * @access public
	*/	 
       function beforeFilter(){
		parent::beforeFilter();	
		$this->Auth->allow('roundtable','client_registration','registered_request','thanks');
	}


         function registered_request(){
    	if ($this->RequestHandler->isAjax()) {
    		Configure::write('debug', 0);
    			
    		$loggedUserId = $this->request->data['loggedUserId'];

                $user = $this->User->find('first', array('conditions' => array('User.id' => $loggedUserId)));

		$prospect['Prospect']['firstname'] = $user['UserReference']['first_name'];
		$prospect['Prospect']['lastname'] = $user['UserReference']['last_name'];
		$prospect['Prospect']['email'] = $user['User']['username'];
		$prospect['Prospect']['company'] = "GUILD Member";
		$prospect['Prospect']['phone'] = "xxxxx";

		$prospect['Prospect']['prospect_type'] = "EXECUTIVE ROUNDTABLE";
		
		$this->Prospect->save($prospect);
              
                $organization = "GUILD Member";


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $to = "iqbal@guild.im";
                           $subject = "New Executive Roundtable request";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("executive_roundtable_request");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_userlname=".urlencode($user['UserReference']['last_name']);
                           $data .= "&merge_useremail=".urlencode($user['User']['username']);
                           $data .= "&merge_userphone=".urlencode("xxxxx");
                           $data .= "&merge_userorganization=".urlencode($organization);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End




			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $user['User']['username'];
                           $subject = "Your request for an Executive Roundtable has been submitted";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("client_roundtable_request_confirmatiom");
                           $data .= "&merge_visitorfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

		echo json_encode($loggedUserId);
    		$this->render(false);
    		exit();


            }


         }
    
       function roundtable()
        {


		$this->Session->write('popup', 1);
              $this->set("title_for_layout","Executive Roundtable");

	    
	   

         
              if (!empty($this->data)) {
	       
		$prospect['Prospect']['firstname'] = $this->data['Mentorship']['firstname'];
		$prospect['Prospect']['lastname'] = $this->data['Mentorship']['lastname'];
		$prospect['Prospect']['email'] = $this->data['Mentorship']['email'];
		$prospect['Prospect']['company'] = $this->data['Mentorship']['company'];
		$prospect['Prospect']['phone'] = $this->data['Mentorship']['phone'];

		$prospect['Prospect']['prospect_type'] = "EXECUTIVE ROUNDTABLE";
		
		$this->Prospect->save($prospect);
               
               

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $to = "iqbal@guild.im";
                           $subject = "New Executive Roundtable request";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("executive_roundtable_request");
                           $data .= "&merge_userfname=".urlencode($this->data['Mentorship']['firstname']);
                           $data .= "&merge_userlname=".urlencode($this->data['Mentorship']['lastname']);
                           $data .= "&merge_useremail=".urlencode($this->data['Mentorship']['email']);
                           $data .= "&merge_userphone=".urlencode($this->data['Mentorship']['phone']);
                           $data .= "&merge_userorganization=".urlencode($this->data['Mentorship']['company']);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

                $url = SITE_URL."roundtable/client_registration";


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $this->data['Mentorship']['email'];
                           $subject = "Your request for an Executive Roundtable has been submitted";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("visitor_roundtable_request_confirmatiom");
                           $data .= "&merge_visitorfname=".urlencode($this->data['Mentorship']['firstname']);
                           $data .= "&merge_registerlink=".urlencode($url);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

              $sending = SITE_URL."thanks";
		$this->Session->setFlash(__("Your request for an Executive Roundtable has been submitted.", true), 'default', array('class' => 'notclass'));    				 
              echo "<script type='text/javascript'>window.location.href='".$sending."';</script>";
		die;
                }  
   	}



       function client_registration(){

               $this->redirect(array('controller'=>'fronts','action'=>'index','OpenPlanPopup:'.true));
     
              }
         
     function thanks(){

	      $this->Session->write('popup', 1);
              $this->set("title_for_layout","Thanks");


        }
             
}
?>