<?php
/**
 * Cities Controller
 *
 * PHP version 5
 *
 * @category Controller
 */
     use Elasticsearch\ClientBuilder;
 

    require 'vendor/autoload.php';


class FrontsController extends AppController{
	/**
     * Controller name
     *
     * @var string
     * @access public
     */
	var $name	=	'Fronts';
	/**
     * Models used by the Controller
     *
     * @var array
     * @access public
     */	 
	var $uses =array('User','StaticPage','Mentorship','Question','City','Mentorship','Answer','FeaturedMentor',
			'UserReference','TempMentorship', 'Keywords','Topic','DirectoryUser','DirectoryConsultation',
			'Feedback','MediaRecords','Communication','Testimonial','QnaQuestion',
			'QnaAnswer','Prospect', 'Project','QnaQuestionCategory','Takeaway_prospect','TakeawayData','IndustryCategory','MemberIndustryCategory','Synonym','ConsultationRequest','Emailprospect');
	var $helpers = array('General','Form','Ajax','Javascript','Html','Image');
	var $components = array("Session", "Email","Auth","RequestHandler","General");
	protected $_searchSession;
   /**
	 * beforeFilter
	 * @return void
	 */	 
	function beforeFilter(){
		parent::beforeFilter();	 
		$this->Auth->allow('index','auto_zipcode','getStateCode','privacy','terms','search', 
				'searchKeywords','guarantee','consult', 'thanks_for_consulting','thanks_for_feedback','give_feedback','claim_profile',
				'consult_iframe','feedback_iframe','directory', 'loadCategories','consultation_request_ajax','directory_result','consultation_request',
				'visitor_consultation_request','consultation_request_iframe','cosultation_request_login', 'new_consultation_request',
				'new_add_question', 'new_project_request','admin_prospective','admin_present_prospective','media_response',
				'admin_takeaway_prospective_delete','admin_prospective_delete','admin_takeaway_prospective_approve',
				'admin_prospective_approve','test','get_elastic_data','elastic_search','esearch','elastic_update', 
				'elastic_search_distance', 'elastic_search_quality','search_result','index_member','elastic_delete','schedule_call','expertise_search','thanks_for_request','emailsession','admin_visitoremailaddress','callapi','thanks_for_interest','admin_homepage_prospective');
		if (in_array($this->params['action'], array('admin_process'))){
			$this->Security->validatePost = false;						
		}
      $this->searchSession = session_id();
		
	}

	/*
	* Front page 
	*/



	function elastic_delete () {
		
		$client = ClientBuilder::create()->build();
		
					$params = array(
				    'index' => 'guild_members',
				    'type' => 'guildlive_new1',
				    'id' => 3559
				);
				
				$response = $client->delete($params);
				
				pr($response);
				
				die();
				
	}

    function test(){
		
		$client = ClientBuilder::create()->build();

		$users = $this->User->find('all',
					array(
							'contain'=>array(
									'UserReference'=>array(
											'fields'=>array('first_name','last_name','zipcode','background_summary','desire_mentee_profile','categories_tags','linkedin_headline')
									),
									
							),
							'conditions'=>array('role_id'=>2,'status'=>1, 'access_specifier'=>'publish')
					)
			);
			
		$inc = 0;
		foreach ($users as $key => $userData) {
			
			if($inc >= 1000 && $inc < 1100) {
				
				$full_name = $userData['UserReference']['first_name'] . " " . $userData['UserReference']['last_name'] ;

				$city = $this->_getCityData($userData['UserReference']['zipcode']);
				
				//Topics Begin
				
				$topic1 = $userData['MemberIndustryCategory']['topic1'];
				$topic2 = $userData['MemberIndustryCategory']['topic2'];
				$topic3 = $userData['MemberIndustryCategory']['topic3'];
				$topic4 = $userData['MemberIndustryCategory']['topic4'];
				$topic5 = $userData['MemberIndustryCategory']['topic5'];
				$topic_synonym = "";

				if($topic1 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic1)));
					$topic_first = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_first ='';
                                }

				if($topic2 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic2)));
					$topic_second = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_second  ='';
                                }

				if($topic3 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic3)));
					$topic_third = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_third ='';
                                }

				if($topic4 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic4)));
					$topic_fourth = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_fourth ='';
                                }

				if($topic5 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic5)));
					$topic_fifth = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_fifth ='';
                                }
				//Topics End
				
				$ind_obj1 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['industry1'])));
				$cat_obj1 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['category1'])));
				
				$ind_obj2 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['industry2'])));
				$cat_obj2 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['category2'])));
				
				$ind_obj3 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['industry3'])));
				$cat_obj3 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['category3'])));


				if(empty($ind_obj1) == false && empty($cat_obj1) == false) {
					
                                $industry1 = $ind_obj1['IndustryCategory']['category'];
				$category1 = $cat_obj1['IndustryCategory']['category'];	

				}else{
                                 
                                $industry1 ='';
                                $category1 ='';
                                }   
		
				if(empty($ind_obj2) == false && empty($cat_obj2) == false) {
					
                                $industry2 = $ind_obj2['IndustryCategory']['category'];
				$category2 = $cat_obj2['IndustryCategory']['category'];	
					

				}else{
                                 
                                $industry2 ='';
                                $category2 ='';
                                } 
		
				if(empty($ind_obj3) == false && empty($cat_obj3) == false) {
					
                                $industry3 = $ind_obj3['IndustryCategory']['category'];
				$category3 = $cat_obj3['IndustryCategory']['category'];	
					

				}else{
                                 
                                $industry3 ='';
                                $category3 ='';
                                } 
				

				$location = $city['latitude'] . ", ". $city['longitude'];
				

						
				$params = array(
			    'index' => 'guild_members',
			    'type' => 'guildlive_new1',
			    'id' => $userData['User']['id'],
			    'body' => array(
			        
			        	'first_name' => $userData['UserReference']['first_name'],
					    'last_name' => $userData['UserReference']['last_name'],
					    'extension' => $userData['User']['extension'],
					    'url_key' => $userData['User']['url_key'],
					    'user_image' => $userData['UserImage'][0]['image_name'],
					    'city_name' => $city['city_name'],
					    'state' => $city['state'],
					    'industry1' => $industry1,
					    'industry2' => $industry2,
					    'industry3' => $industry3,
					    'category1' => $category1,
					    'category2' => $category2,
					    'category3' => $category3,
					    'topic1' => $topic_first,
					    'topic2' => $topic_second,
					    'topic3' => $topic_third,
					    'topic4' => $topic_fourth,
					    'topic5' => $topic_fifth,
                                            'topic_synonyms' => $topic_synonym,
					    'background_summary' => $userData['UserReference']['background_summary'],
					   'quality' => $userData['MemberIndustryCategory']['quality'],
					    'location' => $location,
						'name' => $full_name,
						'headline' => $userData['UserReference']['linkedin_headline']
			        
			    )
			);
				
				

			pr("Pankaj");
				// Update doc at /my_index/my_type/my_id
				$response = $client->index($params);

				pr($inc);
				
				pr($response);
			}
			$inc++;
		}
            
		pr("Pankaj Done"); 
		die;  
					   
	}
	
	function _getCityData($zipcode){
		$this->loadModel('City');
		$mentorCityArr = $this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($zipcode))));
		return $mentorCityArr['City'];
	}	
		
    function index_member($userId){
		
		
		$client = ClientBuilder::create()->build();

		$userData = $this->User->find('first',
					array(
							'contain'=>array(
									'UserReference'=>array(
											'fields'=>array('first_name','last_name','zipcode','background_summary','area_of_expertise','desire_mentee_profile','categories_tags','linkedin_headline')
									),
									
							),
							'conditions'=>array('User.id'=>$userId)
					)
			);
					
				$full_name = $userData['UserReference']['first_name'] . " " . $userData['UserReference']['last_name'] ;

				$city = $this->_getCityData($userData['UserReference']['zipcode']);
				
				//Topics Begin
				
				$topic1 = $userData['MemberIndustryCategory']['topic1'];
				$topic2 = $userData['MemberIndustryCategory']['topic2'];
				$topic3 = $userData['MemberIndustryCategory']['topic3'];
				$topic4 = $userData['MemberIndustryCategory']['topic4'];
				$topic5 = $userData['MemberIndustryCategory']['topic5'];
				$topic_synonym = "";

				if($topic1 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic1)));
					$topic_first = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_first ='';
                                }

				if($topic2 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic2)));
					$topic_second = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_second  ='';
                                }

				if($topic3 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic3)));
					$topic_third = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_third ='';
                                }

				if($topic4 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic4)));
					$topic_fourth = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_fourth ='';
                                }

				if($topic5 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic5)));
					$topic_fifth = $match['Topic'][autocomplete_text];
                                        $synonym_match = $this->Synonym->find('first', array('conditions'=> array('Synonym.original_word'=>$match['Topic'][autocomplete_text])));
                                        $topic_synonym =  $topic_synonym.", ".$synonym_match['Synonym']['synonym_word'];
				}else{
                                 $topic_fifth ='';
                                }
				//Topics End
				
				$ind_obj1 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['industry1'])));
				$cat_obj1 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['category1'])));
				
				$ind_obj2 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['industry2'])));
				$cat_obj2 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['category2'])));
				
				$ind_obj3 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['industry3'])));
				$cat_obj3 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['category3'])));


				if(empty($ind_obj1) == false && empty($cat_obj1) == false) {
					
                                $industry1 = $ind_obj1['IndustryCategory']['category'];
				$category1 = $cat_obj1['IndustryCategory']['category'];	

				}else{
                                 
                                $industry1 ='';
                                $category1 ='';
                                }   
		
				if(empty($ind_obj2) == false && empty($cat_obj2) == false) {
					
                                $industry2 = $ind_obj2['IndustryCategory']['category'];
				$category2 = $cat_obj2['IndustryCategory']['category'];	
					

				}else{
                                 
                                $industry2 ='';
                                $category2 ='';
                                } 
		
				if(empty($ind_obj3) == false && empty($cat_obj3) == false) {
					
                                $industry3 = $ind_obj3['IndustryCategory']['category'];
				$category3 = $cat_obj3['IndustryCategory']['category'];	
					

				}else{
                                 
                                $industry3 ='';
                                $category3 ='';
                                } 
				

				$location = $city['latitude'] . ", ". $city['longitude'];
				
		$params = array(
		    'index' => 'guild_members',
		    'type' => 'guildlive_new1',
		    'id' => $userData['User']['id'],
		    'body' => array(
			        	'first_name' => $userData['UserReference']['first_name'],
					    'last_name' => $userData['UserReference']['last_name'],
					    'extension' => $userData['User']['extension'],
					    'url_key' => $userData['User']['url_key'],
					    'user_image' => $userData['UserImage'][0]['image_name'],
					    'city_name' => $city['city_name'],
					    'state' => $city['state'],
					    'industry1' => $industry1,
					    'industry2' => $industry2,
					    'industry3' => $industry3,
					    'category1' => $category1,
					    'category2' => $category2,
					    'category3' => $category3,
					    'topic1' => $topic_first,
					    'topic2' => $topic_second,
					    'topic3' => $topic_third,
					    'topic4' => $topic_fourth,
					    'topic5' => $topic_fifth,
                                            'topic_synonyms' => $topic_synonym,
					    'background_summary' => $userData['UserReference']['background_summary'],
					   'quality' => $userData['MemberIndustryCategory']['quality'],
					    'location' => $location,
						'name' => $full_name,
						'headline' => $userData['UserReference']['linkedin_headline']
			)
		);
				
		$response = $client->index($params);
		
		pr($response);
		die;  
	}

	function elastic_update(){
		
		$client = ClientBuilder::create()->build();

		$users = $this->User->find('all',
					array(
							'contain'=>array(
									'UserReference'=>array(
											'fields'=>array('first_name','last_name','zipcode','background_summary','area_of_expertise','desire_mentee_profile','categories_tags','linkedin_headline')
									),
									
							),
							'conditions'=>array('role_id'=>2,'status'=>1, 'access_specifier'=>'publish')
					)
			);
			
		$inc = 0;
		foreach ($users as $key => $userData) {
			
			if($inc >= 700 && $inc < 800) {
				
				$full_name = $userData['UserReference']['first_name'] . " " . $userData['UserReference']['last_name'] ;

				$city = $this->_getCityData($userData['UserReference']['zipcode']);
				
				//Topics Begin
				
				$topic1 = $userData['MemberIndustryCategory']['topic1'];
				$topic2 = $userData['MemberIndustryCategory']['topic2'];
				$topic3 = $userData['MemberIndustryCategory']['topic3'];
				$topic4 = $userData['MemberIndustryCategory']['topic4'];
				$topic5 = $userData['MemberIndustryCategory']['topic5'];
				
				$topic_list = "";
				if($topic1 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic1)));
					$topic_list = $match['Topic'][autocomplete_text];
				}
				if($topic2 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic2)));
					$topic_list = $topic_list . ", " . $match['Topic'][autocomplete_text];
				}
				if($topic3 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic3)));
					$topic_list = $topic_list . ", " . $match['Topic'][autocomplete_text];
				}
				if($topic4 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic4)));
					$topic_list = $topic_list . ", " . $match['Topic'][autocomplete_text];
				}
				if($topic5 != -1) {
					$match = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topic5)));
					$topic_list = $topic_list . ", " . $match['Topic'][autocomplete_text];
				}
				//Topics End
				
				$ind_obj1 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['industry1'])));
				$cat_obj1 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['category1'])));
				
				$ind_obj2 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['industry2'])));
				$cat_obj2 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['category2'])));
				
				$ind_obj3 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['industry3'])));
				$cat_obj3 = $this->IndustryCategory->find('first', array('conditions' => array('IndustryCategory.id' => $userData['MemberIndustryCategory']['category3'])));
				
				$industry_string = '';
				
				if(empty($ind_obj1) == false && empty($cat_obj1) == false) {
					
					if($cat_obj1['IndustryCategory']['category'] == "ALL CATEGORIES")
						$industry_string = $industry_string . $ind_obj1['IndustryCategory']['category'];
					else
						$industry_string = $industry_string . $ind_obj1['IndustryCategory']['category'] . ":  " . $cat_obj1['IndustryCategory']['category'];
					
					$industry_string = $industry_string . "|";
				}
		
				if(empty($ind_obj2) == false && empty($cat_obj2) == false) {
					
					if($cat_obj2['IndustryCategory']['category'] == "ALL CATEGORIES")
						$industry_string = $industry_string . $ind_obj2['IndustryCategory']['category'];
					else
						$industry_string = $industry_string . $ind_obj2['IndustryCategory']['category'] . ": " . $cat_obj2['IndustryCategory']['category'];
					
					$industry_string = $industry_string . "|";
				}
		
				if(empty($ind_obj3) == false && empty($cat_obj3) == false) {
					
					if($cat_obj3['IndustryCategory']['category'] == "ALL CATEGORIES")
						$industry_string = $industry_string . $ind_obj3['IndustryCategory']['category'];
					else
						$industry_string = $industry_string . $ind_obj3['IndustryCategory']['category'] . ": " . $cat_obj3['IndustryCategory']['category'];
					
					$industry_string = $industry_string . "|";
				}
				
				$cat_list = $industry_string;
				$location = $city['latitude'] . ", ". $city['longitude'];
				
				pr($full_name);
				pr($userData['User']['id']);
				pr($userData['UserReference']['linkedin_headline']);
				pr($location);
				pr($full_name);
				pr($topic_list);
						
				$params = array(
			    'index' => 'guild_members',
			    'type' => 'guild_live',
			    'id' => $userData['User']['id'],
			    'body' => array(
			        'doc' => array(
			        	'first_name' => $userData['UserReference']['first_name'],
					    'last_name' => $userData['UserReference']['last_name'],
					    'extension' => $userData['User']['extension'],
					    'url_key' => $userData['User']['url_key'],
					    'user_image' => $userData['UserImage'][0]['image_name'],
					    'city_name' => $city['city_name'],
					    'state' => $city['state'],
					    'industry_category' => $cat_list,
					    'background_summary' => $userData['UserReference']['background_summary'],
					    'area_of_expertise' => $userData['UserReference']['area_of_expertise'],
					   'quality' => $userData['MemberIndustryCategory']['quality'],
					    'categories_tags' => $topic_list,
						'location' => $location,
						'name' => $full_name,
						'headline' => $userData['UserReference']['linkedin_headline']
			        )
			    )
			);
			
				
				

			pr("Pankaj");
				// Update doc at /my_index/my_type/my_id
				$response = $client->update($params);

				pr($inc);
				
				pr($response);
			}
			$inc++;
		}
            
		pr("Pankaj Done"); 
		die;  
					   
	}
	
	function get_elastic_data () {
		
		$client = ClientBuilder::create()->build();
		
					$params = array(
				    'index' => 'guild_members',
				    'type' => 'guild',
				    'id' => 1,
				    'body' => array(
					    'first_name' => "Pankaj",
					    'last_name' => "Nirwan",
					    'extension' => "111",
					    'url_key' => "pankaj.nirwan",
					    'user_image' => "",
					    'city_name' => "Delhi",
					    'state' => "India",
					    'industry_category' => "cat_list",
					    'background_summary' => "summary",
					    'area_of_expertise' => "Expertise",
					    'categories_tags' => "Tags",
					    'quality' => 1,
						'location' => array(10, 10)
					)
				);
				
				$response = $client->index($params);
				
				pr($response);
				
				die();
				
	}

	function elastic_search_distance($term){
		
				
		$client = ClientBuilder::create()->build();
		
		$params = array(
		    'index' => 'guild_members',
		    'type' => 'guild',
		    'size' => 10,
		    'explain'=> true,
		    'body' => array(
		    			'query' => [
							'function_score' => [
								'query' => [
						            'bool' => [
						                'should' => [
						                    [ 'match' => ['area_of_expertise'=>[ 'query' => $term , 'boost'=> 1]]],
						                    [ 'match' => ['background_summary'=>[ 'query' => $term, 'boost'=> 0.5]]],
						                    [ 'match' => ['categories_tags'=>[ 'query' => $term, 'boost' => 3]]],
						                    [ 'match' => ['industry_category'=>[ 'query' => $term, 'boost' => 3]]]
						                ]
						               ]
						              ],
								'functions' => [
											[
												'exp' => [
											 		'location' => [
										              'origin' => "33.6, -117.71",
										              'offset' => "300km",
										              'scale' =>  "150km"
										            ]
										          ],
										          'weight' => 1
					        			]
					        		]
					        	]
					        	]
		    )
		);
		
				
		$response = $client->search($params);
		
		pr($response);
		die();
		
	}
	
	function elastic_search($term){
		
		$client = ClientBuilder::create()->build();

		/*	$params = array(
		    'index' => 'guild_members',
		    'type' => 'guild_test_final',
		    'size' => 500,
		    'body' => array(
						'query' => [
						            'bool' => [
						                'should' => [
						                    [ 'match' => ['area_of_expertise'=>[ 'query' => $term , 'boost'=> 1]]],
						                    [ 'match' => ['background_summary'=>[ 'query' => $term, 'boost'=> 0.5]]],
						                    [ 'match' => ['categories_tags'=>[ 'query' => $term, 'boost' => 3]]],
						                    [ 'match' => ['industry_category'=>[ 'query' => $term, 'boost' => 3]]]
						                ],
						                'filter' => [
						                  'geo_distance' => [
						                    'distance' => '200km',
						                    	'location' => "36,-117.71"
						                  ]
						                ]
						            ]
						        ]
		    )
		);
		*/
		
		
		$params = array(
		    'index' => 'guild_members',
		    'type' => 'guild',
		    'size' => 500,
		    'body' => array(
		    			'query' => [
							'function_score' => [
								'query' => [
						            'bool' => [
						                'should' => [
						                    [ 'match' => ['area_of_expertise'=>[ 'query' => $term , 'boost'=> 1]]],
						                    [ 'match' => ['background_summary'=>[ 'query' => $term, 'boost'=> 0.5]]],
						                    [ 'match' => ['categories_tags'=>[ 'query' => $term, 'boost' => 3]]],
						                    [ 'match' => ['industry_category'=>[ 'query' => $term, 'boost' => 3]]]
						                ]
						               ]
						              ]
								
					        	]
					        	]
		    )
		);
		
				
		$response = $client->search($params);
		
		pr($response);
		die();
	}
		 
		 
		function elastic_search_quality($term){
		
		$client = ClientBuilder::create()->build();
		
		$params = array(
		    'index' => 'guild_members',
		    'type' => 'guild',
		    'size' => 500,
		    'body' => array(
		    			'query' => [
							'function_score' => [
								'query' => [
						            'bool' => [
						                'should' => [
						                    [ 'match' => ['area_of_expertise'=>[ 'query' => $term , 'boost'=> 1]]],
						                    [ 'match' => ['background_summary'=>[ 'query' => $term, 'boost'=> 0.5]]],
						                    [ 'match' => ['categories_tags'=>[ 'query' => $term, 'boost' => 3]]],
						                    [ 'match' => ['industry_category'=>[ 'query' => $term, 'boost' => 3]]]
						                ]
						               ]
						              ],
								'functions' => [
											[
												'exp' => [
											 		'location' => [
										              'origin' => "33.6, -117.71",
										              'offset' => "300km",
										              'scale' =>  "150km"
										            ]
										          ],
										          'weight' => 1
					        			],
					        			[
					        			"script_score" => [
											"script" => "doc['quality'].value"
											],
											'weight' => 2
										]
					        		]
					        	]
					        	]
		    )
		);
		
				
		$response = $client->search($params);
		
		pr($response);
		die();
	}
		
		
	function _getLatLong($cityStateArr){
		//prd($cityStateArr);
		$this->loadModel('City');
		if(isset($cityStateArr['city']) && isset($cityStateArr['state'])){
			$cityArr = $this->City->find('first',array('conditions'=>array('City.city_name'=>trim($cityStateArr['city']),'City.state'=>trim($cityStateArr['state']))));
		}elseif(isset($cityStateArr['city'])){
			$cityArr = $this->City->find('first',array('conditions'=>array('City.city_name'=>trim($cityStateArr['city']))));
		}elseif(isset($cityStateArr['state'])){
			$cityArr = $this->City->find('first',array('conditions'=>array('City.city_name'=>trim($cityStateArr['state']))));
		}else{
			$cityArr = array();
		}
		return $cityArr;
	}
		
		
	function search_result($adIndustry = null, $adSubIndustry = null,$adKeyword = null, $adstate = null, $adCity = null){
	
	                
	        if(empty($adCity)){

                $adCity ="new-york";

                 }

                if(empty($adstate)){

                $adstate ="Ny";

                }


             $this->set('login_industry',$adIndustry);
	     $this->set('login_subindustry',$adSubIndustry);	 
             $this->set('login_keyword',$adKeyword);
             $this->set('login_city',$adCity);
             $this->set('login_state',$adstate);

             $this->Session->write('login_industry',$adIndustry);
             $this->Session->write('login_subindustry',$adSubIndustry);
             $this->Session->write('login_keyword',$adKeyword);
             $this->Session->write('login_city',$adCity);
             $this->Session->write('login_state',$adstate);

	
                $this->set('cache_industry', $adIndustry);
                $this->set('cache_subindustry', $adSubIndustry);
		$this->set('cache_keyword', $adKeyword);
		$this->set('cache_adCity', $adCity);
		$this->set('cache_adstate', $adstate);

		if(!empty($adCity) && $adCity != '')
		{
			$adCity = str_replace("-"," ",$adCity);
		}
		
		if(!empty($adKeyword) && $adKeyword != '')
		{
			$adKeyword = str_replace("-"," ",$adKeyword);
		}
		if(!empty($adIndustry) && $adIndustry != '')
		{
			$adIndustry = str_replace("-"," ",$adIndustry);
		}

		if(!empty($adSubIndustry) && $adSubIndustry != '')
		{
			$adSubIndustry = str_replace("-"," ",$adSubIndustry);
		}
                
                if(!empty($adIndustry) && $adIndustry == 'non profit' || $adIndustry == 'Non profit')
                {
                   $adIndustry = "Non-profit";
                 }
                if(!empty($adSubIndustry) && $adSubIndustry == 'fund raising' || $adSubIndustry == 'Fund raising')
                {
                   $adSubIndustry = "Fund-Raising";
                 } 
		$this->layout = 'search';
		
	       if(false !== stripos($adKeyword,"_slash"))
        	$adKeyword = str_replace("_slash","/",$adKeyword);
               if(false !== stripos($adKeyword,"_or"))
        	$adKeyword = str_replace("_or","-",$adKeyword);
               if(false !== stripos($adKeyword,"_and"))
         	$adKeyword = str_replace("_and","&",$adKeyword);
         

		  	if(!empty($adKeyword) && $adKeyword != '' && $adKeyword != 'browse all')
		    {
				
                $this->set("title_for_layout",$adKeyword." | Search Result");
                $mentorsdata = array();

		$location = array('city'=>$adCity,'state'=>$adstate);
		
		$userLocation = $this->_getLatLong($location);
		
		$userLocationString =  $userLocation['City']['latitude'] . ", ". $userLocation['City']['longitude'];
		
		$ind_cat_str = $adIndustry . " " . $adSubIndustry;

		$client = ClientBuilder::create()->build();

		$params = array(
		    'index' => 'guild_members',
		    'type' => 'guild',
		    'size' => 50,
		    'body' => array(
		    			'query' => [
							'function_score' => [
								'query' => [
						            'bool' => [
						                'should' => [
						                    [ 'match' => ['area_of_expertise'=>[ 'query' => $adKeyword , 'boost'=> 1]]],
						                    [ 'match' => ['background_summary'=>[ 'query' => $adKeyword, 'boost'=> 0.5]]],
						                    [ 'match' => ['categories_tags'=>[ 'query' => $adKeyword, 'boost' => 3]]],
						                    [ 'match' => ['industry_category'=>[ 'query' => $ind_cat_str, 'boost' => 3]]]
						                ]
						               ]
						              ],
								'functions' => [
											[
												'exp' => [
											 		'location' => [
										              'origin' => $userLocationString,
										              'offset' => "300km",
										              'scale' =>  "150km"
										            ]
										          ],
										          'weight' => 1
					        			],
					        			[
					        			"script_score" => [
											"script" => "doc['quality'].value"
											],
											'weight' => 2
										]
					        		]
					        	]
					        	]
		    )
		);
		

		$response = $client->search($params);
		
					
			    $final_mentorsdata = array();
				$inc = 0;
					
				foreach($response['hits']['hits'] as $value)
				{
						$final_mentorsdata[$inc]['User']['id'] = $value['_id'];
						$final_mentorsdata[$inc]['User']['extension'] = $value['_source']['extension'];
						$final_mentorsdata[$inc]['User']['url_key'] = $value['_source']['url_key'];
						$final_mentorsdata[$inc]['UserReference']['first_name'] = $value['_source']['first_name'];
						$final_mentorsdata[$inc]['UserReference']['last_name'] = $value['_source']['last_name'];
						$final_mentorsdata[$inc]['UserReference']['background_summary'] = $value['_source']['background_summary'];
						$final_mentorsdata[$inc]['UserReference']['area_of_expertise'] = $value['_source']['area_of_expertise'];
						$final_mentorsdata[$inc]['UserReference']['City']['city_name'] = $value['_source']['city_name'];
						$final_mentorsdata[$inc]['UserReference']['City']['state'] = $value['_source']['state'];
						$final_mentorsdata[$inc]['UserImage'][0]['image_name']= $value['_source']['user_image'];
						$inc = $inc + 1;
				 }

				//sorting 
				$data = $final_mentorsdata;
				$this->set('adSubIndustry', $adSubIndustry);
				
				$this->set(compact('data','keyword','adCity','adstate','adIndustry'));
				
				//Finding directory members
				/*$dUsers = $this->DirectoryUser->find('all',array('conditions'=>array('DirectoryUser.hasPublished'=>0), 'order'=>array('DirectoryUser.last_name'=>'asc')));
				$dUsersFinal = array();
				$inc = 0;
				foreach ($dUsers as $key=>$value) 
				{
					$dValue = 0;
					if($keyword != '')
					{
						foreach ($keyArray as $keyw) 
						{
							if (0 == strcasecmp($keyw, $value['DirectoryUser']['first_name']) || 0 == strcasecmp($keyw, $value['DirectoryUser']['first_name'])) 
							{
								$dValue++;
							}
                                                        if (0 == strcasecmp($keyw, $value['DirectoryUser']['last_name']) || 0 == strcasecmp($keyw, $value['DirectoryUser']['last_name'])) 
						        {
								$dValue++;
							}
						 }
	
						 if (false !== stripos($value['DirectoryUser']['area_of_expertise'], $keyword)) 
						 {
							$dValue++;
						 }
						       
						 if($dValue > 0) 
						 {
							$dUsersFinal[$inc++] = $value;
						 }
                         } else 
                        {
							$dUsersFinal[$inc++] = $value;
				        }
			       }*/
				$dUsersFinalSorted = Set::sort($dUsersFinal, '{n}.DirectoryUser.quality', 'asc');
				$this->set('dUsersFinal', $dUsersFinalSorted);
		}

}

	function esearch($adKeyword = null, $adstate = null, $adCity = null){

                
	        if(empty($adCity)){

                $adCity ="new-york";

                 }

                if(empty($adstate)){

                $adstate ="Ny";

                }


             $this->set('login_industry',$adIndustry);
	     $this->set('login_subindustry',$adSubIndustry);	 
             $this->set('login_keyword',$adKeyword);
             $this->set('login_city',$adCity);
             $this->set('login_state',$adstate);

             $this->Session->write('login_industry',$adIndustry);
             $this->Session->write('login_subindustry',$adSubIndustry);
             $this->Session->write('login_keyword',$adKeyword);
             $this->Session->write('login_city',$adCity);
             $this->Session->write('login_state',$adstate);

	
                $this->set('cache_industry', $adIndustry);
                $this->set('cache_subindustry', $adSubIndustry);
		$this->set('cache_keyword', $adKeyword);
		$this->set('cache_adCity', $adCity);
		$this->set('cache_adstate', $adstate);

		if(!empty($adCity) && $adCity != '')
		{
			$adCity = str_replace("-"," ",$adCity);
		}
		
		if(!empty($adKeyword) && $adKeyword != '')
		{
			$adKeyword = str_replace("-"," ",$adKeyword);
		}
		if(!empty($adIndustry) && $adIndustry != '')
		{
			$adIndustry = str_replace("-"," ",$adIndustry);
		}

		if(!empty($adSubIndustry) && $adSubIndustry != '')
		{
			$adSubIndustry = str_replace("-"," ",$adSubIndustry);
		}
                
                if(!empty($adIndustry) && $adIndustry == 'non profit' || $adIndustry == 'Non profit')
                {
                   $adIndustry = "Non-profit";
                 }
                if(!empty($adSubIndustry) && $adSubIndustry == 'fund raising' || $adSubIndustry == 'Fund raising')
                {
                   $adSubIndustry = "Fund-Raising";
                 } 
		$this->layout = 'search';
		
	       if(false !== stripos($adKeyword,"_slash"))
        	$adKeyword = str_replace("_slash","/",$adKeyword);
               if(false !== stripos($adKeyword,"_or"))
        	$adKeyword = str_replace("_or","-",$adKeyword);
               if(false !== stripos($adKeyword,"_and"))
         	$adKeyword = str_replace("_and","&",$adKeyword);
         

		  	if(!empty($adKeyword) && $adKeyword != '' && $adKeyword != 'browse all')
		    {
				
                $this->set("title_for_layout",$adKeyword." | Search Result");
                $mentorsdata = array();

		$location = array('city'=>$adCity,'state'=>$adstate);
		
		$userLocation = $this->_getLatLong($location);
		
		$userLocationString =  $userLocation['City']['latitude'] . ", ". $userLocation['City']['longitude'];

		$client = ClientBuilder::create()->build();

		$params = array(
		    'index' => 'guild_members',
		    'type' => 'guild',
		    'size' => 500,
		    'body' => array(
		    			'query' => [
							'function_score' => [
								'query' => [
						            'bool' => [
						                'should' => [
						                    [ 'match' => ['area_of_expertise'=>[ 'query' => $adKeyword , 'boost'=> 1]]],
						                    [ 'match' => ['background_summary'=>[ 'query' => $adKeyword, 'boost'=> 0.5]]],
						                    [ 'match' => ['categories_tags'=>[ 'query' => $adKeyword, 'boost' => 3]]],
						                    [ 'match' => ['industry_category'=>[ 'query' => $adKeyword, 'boost' => 3]]]
						                ]
						               ]
						              ],
								'functions' => [
											[
												'exp' => [
											 		'location' => [
										              'origin' => $userLocationString,
										              'offset' => "300km",
										              'scale' =>  "150km"
										            ]
										          ],
										          'weight' => 1
					        			],
					        			[
					        			"script_score" => [
											"script" => "doc['quality'].value"
											],
											'weight' => 2
										]
					        		]
					        	]
					        	]
		    )
		);
		

		$response = $client->search($params);
		
					
			    $final_mentorsdata = array();
				$inc = 0;
					
				foreach($response['hits']['hits'] as $value)
				{
						$final_mentorsdata[$inc]['User']['id'] = $value['_id'];
						$final_mentorsdata[$inc]['User']['extension'] = $value['_source']['extension'];
						$final_mentorsdata[$inc]['User']['url_key'] = $value['_source']['url_key'];
						$final_mentorsdata[$inc]['UserReference']['first_name'] = $value['_source']['first_name'];
						$final_mentorsdata[$inc]['UserReference']['last_name'] = $value['_source']['last_name'];
						$final_mentorsdata[$inc]['UserReference']['background_summary'] = $value['_source']['background_summary'];
						$final_mentorsdata[$inc]['UserReference']['area_of_expertise'] = $value['_source']['area_of_expertise'];
						$final_mentorsdata[$inc]['UserReference']['City']['city_name'] = $value['_source']['city_name'];
						$final_mentorsdata[$inc]['UserReference']['City']['state'] = $value['_source']['state'];
						$final_mentorsdata[$inc]['UserImage'][0]['image_name']= $value['_source']['user_image'];
						$inc = $inc + 1;
				 }

				//sorting 
				$data = $final_mentorsdata;
				$this->set('adSubIndustry', $adSubIndustry);
				
				$this->set(compact('data','keyword','adCity','adstate','adIndustry'));
				
				//Finding directory members
				/*$dUsers = $this->DirectoryUser->find('all',array('conditions'=>array('DirectoryUser.hasPublished'=>0), 'order'=>array('DirectoryUser.last_name'=>'asc')));
				$dUsersFinal = array();
				$inc = 0;
				foreach ($dUsers as $key=>$value) 
				{
					$dValue = 0;
					if($keyword != '')
					{
						foreach ($keyArray as $keyw) 
						{
							if (0 == strcasecmp($keyw, $value['DirectoryUser']['first_name']) || 0 == strcasecmp($keyw, $value['DirectoryUser']['first_name'])) 
							{
								$dValue++;
							}
                                                        if (0 == strcasecmp($keyw, $value['DirectoryUser']['last_name']) || 0 == strcasecmp($keyw, $value['DirectoryUser']['last_name'])) 
						        {
								$dValue++;
							}
						 }
	
						 if (false !== stripos($value['DirectoryUser']['area_of_expertise'], $keyword)) 
						 {
							$dValue++;
						 }
						       
						 if($dValue > 0) 
						 {
							$dUsersFinal[$inc++] = $value;
						 }
                         } else 
                        {
							$dUsersFinal[$inc++] = $value;
				        }
			       }*/
				$dUsersFinalSorted = Set::sort($dUsersFinal, '{n}.DirectoryUser.quality', 'asc');
				$this->set('dUsersFinal', $dUsersFinalSorted);
		}
      }//function end
      
      
         
	function admin_prospective() {
    	if($this->Auth->user('id') == 1){
		
		$this->layout = 'admin';  
		$prospectives = $this->Prospect->find('all');
		
		$this->set('data', $prospectives);	 
		$this->set('title_for_layout', 'Client');
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}


       function admin_present_prospective() {
    	if($this->Auth->user('id') == 1){
		
		$this->layout = 'admin';  
		$prospectives = $this->Takeaway_prospect->find('all');
		
		$this->set('data', $prospectives);	 
		$this->set('title_for_layout', 'Client');
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}


	function admin_takeaway_prospective_delete($id = null) {
    	if($this->Auth->user('id') == 1){
		
		$this->layout = 'admin';  
		if ($this->Takeaway_prospect->delete($id)) {
            $this->Session->setFlash('Prospective client has been deleted successfully', 'admin_flash_good');
            $this->redirect($this->referer());
        } else {
        	$this->redirect($this->referer());
        }
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}

	function admin_takeaway_prospective_approve($id = null) {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		if(id != null) {
			$takeaway_prospect = $this->Takeaway_prospect->find('first',array('conditions'=>array('Takeaway_prospect.id'=>$id)));
                     $takeaway_detail = $this->TakeawayData->find('first',array('conditions'=>array('TakeawayData.takeaway_link'=>$takeaway_prospect['Takeaway_prospect']['takeaway_link'])));
			if ($takeaway_detail != null && !empty($takeaway_detail)) {
				


				$userData = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name')
											),
											
									),
									'conditions'=>array('User.id'=>$takeaway_detail['TakeawayData']['user_id'])
							)
					
					);
                              


			//Elastic Email Integration - Start

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $userData['User']['username'];
                           $subject = "Prospect email";
                           $consultantfname = $userData['UserReference']['first_name'];
                           $visitoremail = $takeaway_prospect['Takeaway_prospect']['prospect_email'];
                           $visitortakeaway  = $takeaway_prospect['Takeaway_prospect']['takeaway_input'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("takeaway_prospect_email");
                           $data .= "&merge_firstname=".urlencode($consultantfname);
                           $data .= "&merge_email=".urlencode($visitoremail);
                           $data .= "&merge_title=".urlencode($visitortakeaway);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


			//Elastic Email Integration - End

				$takeaway_prospect['Takeaway_prospect']['isApproved'] = 1;
				
				$this->Takeaway_prospect->save($takeaway_prospect);
				
	            $this->Session->setFlash('Prospective client has been approved successfully', 'admin_flash_good');
	            $this->redirect($this->referer());
	        } else {
	        	$this->redirect($this->referer());
	        }
		}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}


	function admin_prospective_delete($id = null) {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		if ($this->Prospect->delete($id)) {
            $this->Session->setFlash('Prospective client has been deleted successfully', 'admin_flash_good');
            $this->redirect($this->referer());
        } else {
        	$this->redirect($this->referer());
        }
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}

	function admin_prospective_approve($id = null) {
    	if($this->Auth->user('id') == 1){
		$this->layout = 'admin';  
		if(id != null) {
			$client = $this->Prospect->find('first',array('conditions'=>array('Prospect.id'=>$id)));
			if ($client != null && !empty($client)) {
				
				if($client['Prospect']['prospect_type'] == "CONSULTATION"){

				$userData = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name')
											),
											
									),
									'conditions'=>array('User.id'=>$client['Prospect']['member_id'])
							)
					
					);
                              

                           


                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $userData['User']['username'];
                           $subject = "A new consultation request";
                           $consultantfname = $userData['UserReference']['first_name'];
                           $visitorfname = $client['Prospect']['firstname'];
                           $visitorlname = $client['Prospect']['lastname'];
                           $visitoremail = $client['Prospect']['email'];
                           $visitorphone = $client['Prospect']['phone'];


                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("visitor_consultation_email_for_consultant");
                           $data .= "&merge_consultantfname=".urlencode($consultantfname);
                           $data .= "&merge_visitorfname=".urlencode($visitorfname);
                           $data .= "&merge_visitorlname=".urlencode($visitorlname);
                           $data .= "&merge_visitoremail=".urlencode($visitoremail);
                           $data .= "&merge_visitorphone=".urlencode($visitorphone);
                           $data .= "&merge_visitorneed=".urlencode($client['Prospect']['answer']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }
                           


                   }


				if($client['Prospect']['prospect_type'] == "DIRECTORY CONSULTATION"){

				$userData = $this->DirectoryUser->find('first',array('conditions'=>array('DirectoryUser.id'=>$client['Prospect']['member_id'])));



                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $userData['DirectoryUser']['username'];
                           $subject = "Consultation request";


                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("visitor_consultation_request");
                           $data .= "&merge_consultantfname=".urlencode($userData['DirectoryUser']['first_name']);
                           $data .= "&merge_clientname=".urlencode($client['Prospect']['firstname']." ". $client['Prospect']['lastname']);
                           $data .= "&merge_clientemail=".urlencode($client['Prospect']['email']);
                           $data .= "&merge_clientphone=".urlencode($client['Prospect']['phone']);
                           $data .= "&merge_clientneed=".urlencode(trim($client['Prospect']['answer']));
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                   }


				$client['Prospect']['isApproved'] = 1;
				
				$this->Prospect->save($client);
				
	            $this->Session->setFlash('Prospective client has been approved successfully', 'admin_flash_good');
	            $this->redirect($this->referer());
	        } else {
	        	$this->redirect($this->referer());
	        }
		}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}

        function directory_result(){

        $this->layout= 'defaultnew';
	$this->Session->write('popup', 1);
	
         $keyword = $this->params['url']['keyword'];
         $id = $this->params['url']['id'];
        
                     $keyword = str_replace("-"," ",$keyword);
                     if(false !== stripos($keyword,"_slash"))
                     $keyword = str_replace("_slash","/",$keyword);
                     if(false !== stripos($keyword,"_or"))
                     $keyword = str_replace("_or","-",$keyword);
                     if(false !== stripos($keyword,"_and"))
                     $keyword = str_replace("_and","&",$keyword);

                if(!empty($keyword) && $keyword == 'Myers Briggs Type Indicator (MBTI)' || $keyword == 'Myers Briggs Type Indicator')
                 {
                   $keyword = "Myers-Briggs Type Indicator (MBTI)";
                 }
                if(!empty($keyword) && $keyword == 'non profit' || $keyword == 'Non profit')
                 {
                   $keyword = "Non-profit";
                 }
                     
         
         $this->set('keyword',$keyword);
         $this->set("title_for_layout","$keyword");

	  $dUsers = $this->DirectoryUser->find('all',array('conditions'=>array('OR' => array(array('DirectoryUser.area_of_expertise LIKE'=>'%'.$keyword.'%'),array('DirectoryUser.first_name LIKE'=>'%'.$keyword.'%'),array('DirectoryUser.last_name LIKE'=>'%'.$keyword.'%')),'AND' =>array('DirectoryUser.hasPublished'=>0)), 'order'=>array('DirectoryUser.last_name'=>'asc')));
		$dUsersFinal = array();
		$inc = 0;
		foreach ($dUsers as $key=>$value) {
			$dUsersFinal[$inc++] = $value;
		}
			
		//$dUsersFinalSorted = Set::sort($dUsersFinal, '{n}.DirectoryUser.quality', 'asc');
		$this->set('dUsersFinal', $dUsersFinal);
               $particulardmember = $this->DirectoryUser->find('first',array('conditions'=>array(array('DirectoryUser.id'=>$id))));

               $this->set('particulardmember', $particulardmember);
 }
       

	function index() {


              $this->layout= 'defaultnew';
		//Delete all registration popup	
	
                if($this->Session->check('menteePopup')){
                $this->Session->delete('menteePopup');
                }

                if($this->Session->check('projectId')){
                $this->Session->delete('projectId');
                } 
                if($this->Session->check('questionId')){
                $this->Session->delete('questionId');
                } 

                if($this->Session->check('duserid')){
                $this->Session->delete('duserid');
                } 
                if($this->Session->check('login_industry')){
                $this->Session->delete('login_industry');
                } 
                if($this->Session->check('mentor')){
                $this->Session->delete('mentor');
                } 
                if($this->Session->check('mentorshipId')){
                $this->Session->delete('mentorshipId');
                } 
                if($this->Session->check('consultantPopup')){
                $this->Session->delete('consultantPopup');
                }
                if($this->Session->check('directoryclaim')){
                $this->Session->delete('directoryclaim');
                } 

		$this->set("title_for_layout","GUILD: Expertise On Demand");
		$this->loadModel('User');
		$this->loadModel('FeaturedMentor');

	        $featureData = $this->FeaturedMentor->find('all',array('conditions'=>array('not' => array('FeaturedMentor.order'=>null)),'order' => 'FeaturedMentor.order asc'));
 		
 		$this->User->Behaviors->attach('Containable');
 		foreach ($featureData as $featuredMentor){
 			$featuredMentorArray[$featuredMentor['FeaturedMentor']['user_id']] = $this->User->find('first',
 				array(
 					'contain'=>array(
 						'UserReference'=>array(
 							'fields'=>array('zipcode','first_name','last_name','area_of_expertise')
 						),
 						'UserImage',
                                           'Testimonial'			
 					),
 					'conditions'=>array('role_id'=>2,'User.id'=>$featuredMentor['FeaturedMentor']['user_id'],'status'=>1)		
 				)
 		
 			);
 		}
 		$this->set('featuredMentorArray', $featuredMentorArray);

		//get city data
		$this->loadModel('City');
		if(isset($this->data['UserReference']['zipcode']) && $this->data['UserReference']['zipcode'] !=''){
			$cityData = $this->City->find('first',array('conditions'=>array('City.zip_code'=>$this->data['UserReference']['zipcode'])));
			$this->set(compact('cityData'));
		}
		if(isset($this->params['named']['backreg']) && $this->params['named']['backreg'] !=''){
			$this->set('backreg',1);
		}
		if(isset($this->params['named']['back_creation']) && $this->params['named']['back_creation'] !=''){
			$this->set('back_creation',1);
		}
		if(isset($this->params['named']['openloginpopup']) && $this->params['named']['openloginpopup'] !=''){
			$this->set('openloginpopup',1);
		}
		if(isset($this->params['named']['OpenRegisterPopup']) && $this->params['named']['OpenRegisterPopup'] !='' && $this->Session->read('ab') != '1'){
                  $this->set('OpenRegisterPopup',1);
              }
              if(isset($this->params['named']['OpenPlanPopup']) && $this->params['named']['OpenPlanPopup'] !=''){
              	$this->set('OpenPlanPopup',1);
              }   

		if(isset($this->params['named']['question_url']) && $this->params['named']['question_url'] !=''){
			$this->set('question_url',$this->params['named']['question_url']);
		}
		
		if($this->Session->read('Auth.User.id') != '' && $this->Session->read('Auth.User.id') != 1) {
			$loggedInUser = $this->User->find('first',
				array(
					'contain'=>array(
						'UserReference'=>array(
							'fields'=>array('zipcode','first_name','last_name','background_summary','area_of_expertise')
						),
						'UserImage'			
					),
					'conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'),'status'=>Configure::read('App.Status.active'))		
				)
			);
			if($loggedInUser['UserReference']['zipcode'] !=''){
				$logg = "Azeez";
				$this->set('logg', $logg);
				$loggedInUsercityData = $this->City->find('first',array('conditions'=>array('City.zip_code'=>$loggedInUser['UserReference']['zipcode'])));
				
				if( $loggedInUsercityData == ''){
					$loggedInUsercityData['City']['city_name'] = 'New York';
					$loggedInUsercityData['City']['state'] = 'NY';
				}
				$this->set(compact('loggedInUsercityData'));
			}
		}	

		$keywords = $this->Topic->find('all',array(
				'conditions'=>array('Topic.topic_subset'=>'1'),
				'fields' => array('DISTINCT (Topic.autocomplete_text) AS autocomplete_text'),
		));
		$key = array();
		$cn = 0;
		foreach($keywords as $value){
			$key[$cn++] = $value['Topic']['autocomplete_text'];
		}
		$this->set('allKeywords', $key);
	}
	
	function loadCategories(){
		
	}
	
	function auto_zipcode(){
			$this->loadModel('City');
			$zip_code = $_REQUEST['data'];
			$citiesArr = $this->City->find('list',array('conditions'=>array('City.zip_code like'=>'%'.$zip_code.'%'),'fields'=>array('id','zip_code'),'limit'=>10));
			echo '<ul class="list">';
			if(count($citiesArr) >0){
				foreach($citiesArr as $key=>$value){
					echo '<li><a href=\'javascript:void(0);\'>'.$value.'</a></li>';
				}				
			}else{
				echo '<li><a href=\'javascript:void(0);\'>No Match</a></li>'; 
			}		
		
			echo "</ul>";
           $this->layout = '';
           $this->render(false);
           exit();			
			
	}
	function terms()
	{
	    $this->set("title_for_layout","Terms of service");
		$this->data = $this->StaticPage->find('first',array('conditions'=>array('StaticPage.slug'=>'terms-and-conditions')));
	}
      
    
	function privacy()
	{


	       $this->set("title_for_layout","Privacy Policy");
		$this->data = $this->StaticPage->find('first',array('conditions'=>array('StaticPage.slug'=>'privacy-policy')));
            
	}	
	function search(){
		if ($this->RequestHandler->isAjax() ) {
			$this->loadModel('City');
   			Configure::write ( 'debug',0);
   			$this->autoRender=false;
			
			$city_name=$this->City->find('all',array(
										'conditions'=>array('City.city_state LIKE'=>$_GET['term'].'%'),
										'group'=>array('city_name','state'),
										'limit'=>5
										));
				$i=0;
				if(!empty($city_name)){
						foreach($city_name as $valueproductname){
							$len = strlen($_GET['term']);					
							//echo $pos;
							$keyvalue= "<strong>".substr($valueproductname['City']['city_name'],0,$len)."</strong>".substr($valueproductname['City']['city_name'],$len).', '.$valueproductname['City']['state'];
						
						

							$response[$i]['value']=$valueproductname['City']['city_name'].', '.$valueproductname['City']['state'];
							$response[$i]['label']="<span class=\"username\">".$keyvalue."</span>";
							//$response[$i]['label']=
						$i++;
						}
						echo json_encode($response);
				}else{
						// echo json_encode(array("Your enter city name is incorrect"));
				}
		}	
		
		
		
	}

	function searchKeywords(){
			
		if ($this->RequestHandler->isAjax() ) {
			$this->loadModel('Topic');
			Configure::write ( 'debug',0);
			$this->autoRender=false;
		
			$expertise=$this->Topic->find('all',array(
					'conditions'=>array('Topic.autocomplete_text LIKE'=>'%'.$_GET['term'].'%'), 
					'fields' => array('DISTINCT (Topic.autocomplete_text) AS autocomplete_text'),
					'limit'=>5
			));
			$i=0;
			if(!empty($expertise)){
				$len = strlen($_GET['term']);
				foreach($expertise as $valueproductname){
					$pos = stripos($valueproductname['Topic']['autocomplete_text'],$_GET['term']);
					$keyvalue = "";

					if($pos == 0) {
						$keyvalue= "<strong>".substr($valueproductname['Topic']['autocomplete_text'],$pos,$len)."</strong>"
								.substr($valueproductname['Topic']['autocomplete_text'],$len);
					}else {
						$keyvalue= substr($valueproductname['Topic']['autocomplete_text'],0,$pos)."<strong>"
								.substr($valueproductname['Topic']['autocomplete_text'],$pos,$len)."</strong>"
								.substr($valueproductname['Topic']['autocomplete_text'],$pos+$len);
					}
		
		
					$response[$i]['value']=$valueproductname['Topic']['autocomplete_text'];
					$response[$i]['label']="<span class=\"username\">".$keyvalue."</span>";
					$i++;
					
				}
				echo json_encode($response);
			}else{
			}
	}
	}
	
	function thanks(){
		$this->set("title_for_layout","Paypal Payment Confirmation");
	}
	
	function decline(){
		$this->set("title_for_layout","Paypal Payment Error");
	}
	
       function cosultation_request_login($mentorship_id = null)

       {
         $id = $mentorship_id;
         
         $this->Session->write('membershipId',$id);

         
	 $this->redirect(array('controller'=>'fronts','action'=>'index','openloginpopup:'.true));
          
       }

	function consultation_request_ajax() {


                if($this->Session->check('menteePopup')){
                $this->Session->delete('menteePopup');
                }
                if($this->Session->check('projectId')){
                $this->Session->delete('projectId');
                } 
                if($this->Session->check('questionId')){
                $this->Session->delete('questionId');
                } 
                if($this->Session->check('consultantPopup')){
                $this->Session->delete('consultantPopup');
                }
                if($this->Session->check('login_industry')){
                $this->Session->delete('login_industry');
                } 
                if($this->Session->check('mentor')){
                $this->Session->delete('mentor');
                } 
                if($this->Session->check('directoryclaim')){
                $this->Session->delete('directoryclaim');
                }  
	
		if ($this->RequestHandler->isAjax()) {
			Configure::write('debug', 0);
			 

		                          $req['ConsultationRequest']['req_message'] = $_REQUEST['mentorshipneed'];
		                          $req['ConsultationRequest']['consultant_id'] = $_REQUEST['mentor_id'];

                                          $req['ConsultationRequest']['req_status'] = "1";		
		                          $this->ConsultationRequest->save($req);
                                          $conid = $this->ConsultationRequest->id;

                                          $clength = strlen($_REQUEST['mentorshipneed']);

                                          if($clength > 100 && !($clength <= 130)){
                                             $title = substr($_REQUEST['mentorshipneed'], 0, strpos(wordwrap($_REQUEST['mentorshipneed'], 120), "\n"));
                                             $projecttitle = $title;
                                          
                                          }else{
                                           $projecttitle = $_REQUEST['mentorshipneed'];
                                           }
                                          $project['Project']['title'] = $projecttitle;
                                          $project['Project']['details'] = trim($_REQUEST['mentorshipneed']);
                                          $project['Project']['budget'] = "$10K to $50K";
                                          $project['Project']['start_date'] = "Within 1 month";
                                          $project['Project']['status'] = "1";
                                          $this->Project->save($project);
                                          $id = $this->Project->id;

  	              $urlKey = preg_replace('/\PL/u', '-', $projecttitle);
		       $urlKey = $urlKey.'-'.$id; 
	    	       $this->Project->updateAll(array('Project.project_url'=>"'".$urlKey."'"),array('Project.id'=>$id));

	                $this->Session->write('mentorshipId',$id);
                        $this->Session->write('consultationId',$conid);
			echo json_encode($id);
			$this->render(false);
			exit();
		}
	}
	
	function new_consultation_request($mentor_url = null)
	{
		$mentor = $this->User->find('first', array('conditions' => array('User.url_key' => $mentor_url)));
                $mentor_id = $mentor['User']['id'];
                $this->Session->write('popup', 1); 


              if(!(empty($this->data))){
		
		$prospect['Prospect']['firstname'] = $this->data['Mentorship']['firstname'];
		$prospect['Prospect']['lastname'] = $this->data['Mentorship']['lastname'];
		$prospect['Prospect']['email'] = $this->data['Mentorship']['email'];
		$prospect['Prospect']['company'] = $this->data['Mentorship']['company'];
		$prospect['Prospect']['phone'] = $this->data['Mentorship']['phone'];
		$prospect['Prospect']['answer'] = trim($this->data['Mentorship']['mentee_need']);
		$prospect['Prospect']['member_id'] = $mentor['User']['id'];
		$prospect['Prospect']['prospect_type'] = "CONSULTATION";
                $date = date('Y-m-d H:i:s');
		$prospect['Prospect']['submitted'] = $date;
		
		$this->Prospect->save($prospect);






	



		                          $req['ConsultationRequest']['req_message'] = trim($this->data['Mentorship']['mentee_need']);
		                          $req['ConsultationRequest']['consultant_id'] = $mentor['User']['id'];
                                          //$req['ConsultationRequest']['client_id'] = $id;
                                          $req['ConsultationRequest']['req_status'] = "1";		
		                          $this->ConsultationRequest->save($req);
                        
                                             $clength = strlen($this->data['Mentorship']['mentee_need']);

                                          if($clength > 100 && !($clength <= 130)){
                                             $title = substr($this->data['Mentorship']['mentee_need'], 0, strpos(wordwrap($this->data['Mentorship']['mentee_need'], 80), "\n"));
                                             $projecttitle = $title;
                                          
                                          }else{
                                           $projecttitle = $this->data['Mentorship']['mentee_need'];
                                           }
                                          $project['Project']['user_id'] = $id;
                                          $project['Project']['title'] = $projecttitle;
                                          $project['Project']['details'] = trim($this->data['Mentorship']['mentee_need']);
                                          $project['Project']['budget'] = "$10K to $50K";
                                          $project['Project']['start_date'] = "Within 1 month";
                                          $project['Project']['status'] = "1";
                                          $this->Project->save($project);


  	              $urlKey = preg_replace('/\PL/u', '-', $projecttitle);
		       $urlKey = $urlKey.'-'.$this->Project->id; 
                       $this->Project->updateAll(array('Project.url'=>"'".$this->data['Mentorship']['email']."'"),array('Project.id'=>$this->Project->id)); 
	    	       $this->Project->updateAll(array('Project.project_url'=>"'".$urlKey."'"),array('Project.id'=>$this->Project->id));

                                          $this->Session->write('FirstName',$this->data['Mentorship']['firstname']);
                                          $this->Session->write('LastName',$this->data['Mentorship']['lastname']);
                                          $this->Session->write('Email',$this->data['Mentorship']['email']);
		 
                         	         $project = $this->Project->find('first', array('conditions' => array('Project.id' => $this->Project->id)));

			//EasticEmail Integration - Begin




                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $this->data['Mentorship']['email'];
                           $subject = "Your requirement has been submitted for review";
                           $projectlink = SITE_URL.'project/index/'.$project['Project']['project_url'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("visitor_consultation_project_submit_confirmation");
                           $data .= "&merge_userfname=".urlencode($this->data['Mentorship']['firstname']);
                           $data .= "&merge_clientemail=".urlencode($this->data['Mentorship']['email']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           $data .= "&merge_projectlink=".urlencode($projectlink);

                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }

                           //EasticEmail Integration - End


                          $this->redirect(array('controller' => 'project', 'action' => 'schedule_call', $this->Project->id));





               }

		
		
	}
	
	function new_add_question($mentor_url = null)
	{
		if($this->data['Mentorship']['email'] !='' && $this->Session->read('Question_context') !=''){		
		$Question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.id' =>$this->Session->read('Question_id'))));
		$Question['QnaQuestion']['question_text'] = $this->Session->read('Question_text');
		$Question['QnaQuestion']['question_context'] = $this->Session->read('Question_context');
		$Question['QnaQuestion']['isdraft'] = 1;
                $Question['QnaQuestion']['user_id'] = $this->Session->read('Auth.User.id');
		
		$this->QnaQuestion->save($Question);
		
		$this->QnaQuestion->createUrlKey($this->QnaQuestion->id);
		
		if($this->Session->read('Question_text') != '')
			$this->Session->delete('Question_text');
		if($this->Session->read('Question_context') != '')
			$this->Session->delete('Question_context');
		if($this->Session->read('Question_id') != '')
			$this->Session->delete('Question_id');	
			
		$checkBoxes=$this->Session->read('checkBoxes');
		$cat = explode(',',$checkBoxes);
				
		for($i=0;$i<count($cat);$i++){
		
			if(isset($cat[$i]) && $cat[$i] != '') {
				
				$topic = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$cat[$i])));
		               if(empty($topic)) {
			
                                $this->Topic->create( );
				    $this->Topic->save(array('autocomplete_text'=>$cat[$i]));

                                   $topicid = $this->Topic->getLastInsertID();
                                   $this->Topic->updateAll(array('Topic.topic_subset' => 0),array('Topic.id ' => $topicid));
                                   $topic = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topicid)));
				    
				}
				
				$Question_category['qna_question_id'] = $this->QnaQuestion->id;
				$Question_category['topic_id'] = $topic['Topic']['id'];
		
				$this->QnaQuestionCategory->save($Question_category);
				$this->QnaQuestionCategory->id = '';
				$this->Topic->id = '';
			}
		}
	
		$prospect['Prospect']['firstname'] = $this->data['Mentorship']['firstname'];
		$prospect['Prospect']['lastname'] = $this->data['Mentorship']['lastname'];
		$prospect['Prospect']['email'] = $this->data['Mentorship']['email'];
		$prospect['Prospect']['company'] = '';
		$prospect['Prospect']['phone'] = $this->data['Mentorship']['phone'];
		$prospect['Prospect']['answer'] = 'Question Id : ' . $this->QnaQuestion->id;
		$prospect['Prospect']['prospect_type'] = "QUESTION";
                $date = date('Y-m-d H:i:s');
		$prospect['Prospect']['submitted'] = $date;
		
		$this->Prospect->save($prospect);
	

					$activationKey = substr(md5(uniqid()), 0, 20);
                                        $array=array();
                                        $array['User']['username'] = $this->data['Mentorship']['email'];
					$array['User']['role_id'] = 3;
					$array['User']['status'] = 1;
					$array['User']['password'] = $this->data['Mentorship']['password'];//Security::hash($this->data['User']['password2'], null, true);
                                        $array['User']['password2'] = Security::hash($this->data['Mentorship']['password'], null, true);
					$array['User']['plan_type'] = "Basic";
					$array['User']['isPlanAdmin'] = 0;

					$array['User']['activation_key'] = $activationKey;
					

						
					$array['User']['access_specifier'] = 'publish';
					$array['User']['is_approved'] = 1;
					$array['User']['email_send'] = '1';
					
						

				        $this->User->create();
				        $this->User->save($array,false);

					$id = $this->User->getLastInsertID();
                              
                                        $userarray = array();
                                        $userarray['UserReference']['user_id'] = $id;
					$userarray['UserReference']['first_name'] = $this->data['Mentorship']['firstname'];
                                        $userarray['UserReference']['last_name'] = $this->data['Mentorship']['lastname'];
                                        $userarray['UserReference']['client_companycontact'] = $this->data['Mentorship']['phone'];
                                        $userarray['UserReference']['phone'] = $this->data['Mentorship']['phone'];
                                        $userarray['UserReference']['client_companycontactname'] = $this->data['Mentorship']['firstname']." ".$this->data['Mentorship']['lastname'];
                                        $userarray['UserReference']['client_companyaddress'] = $this->data['Mentorship']['email'];
                                        $this->UserReference->create();
                                        $this->UserReference->save($userarray,false);



	

                                          $data = $this->User->read(null,$id);
					
					  $this->Session->write('Auth', $data);
						
                                          $this->Session->write('FirstName',$this->data['Mentorship']['firstname']);
                                          $this->Session->write('LastName',$this->data['Mentorship']['lastname']);
                                          $this->Session->write('Email',$this->data['Mentorship']['email']);


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "iqbal@guild.im";
                           $subject = "A new question has been submitted by a visitor";


                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("visitor_question_submit");
                           $data .= "&merge_visitorfname=".urlencode($this->data['Mentorship']['firstname']);
                           $data .= "&merge_visitorlname=".urlencode($this->data['Mentorship']['lastname']);
                           $data .= "&merge_visitoremail=".urlencode($this->data['Mentorship']['email']);
                           $data .= "&merge_visitorphone=".urlencode($this->data['Mentorship']['phone']);
                           $data .= "&merge_questionid=".urlencode($this->QnaQuestion->id);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $this->data['Mentorship']['email'];
                           $subject = "Your question has been submitted";


                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("question_submitted");
                           $data .= "&merge_profilelink=".urlencode($link);
                           $data .= "&merge_userfname=".urlencode($this->data['Mentorship']['firstname']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


		$this->redirect(array('controller'=>'qna','action' => 'thanks_for_asking'));
	
            }
	
	}
	
	function new_project_request(){
		
	            $project = $this->Project->find('first', array('conditions' => array('Project.id' => $this->data['Project']['id'])));
		    $project['Project']['phone_number'] = $this->data['Project']['phone_number'];
		    $project['Project']['status'] = 1; // Project saved with number
 		    $this->Project->save($project);   		
			
			$prospect['Prospect']['firstname'] = $this->data['Mentorship']['firstname'];
			$prospect['Prospect']['lastname'] = $this->data['Mentorship']['lastname'];
			$prospect['Prospect']['email'] = $this->data['Mentorship']['email'];
			$prospect['Prospect']['company'] = '';
			$prospect['Prospect']['answer'] = 'Project Id : '. $this->data['Project']['id'];
			$prospect['Prospect']['member_id'] = '';
			$prospect['Prospect']['prospect_type'] = "PROJECT";
                        $date = date('Y-m-d H:i:s');
		        $prospect['Prospect']['submitted'] = $date;
			
			$this->Prospect->save($prospect);
			$project = $this->Project->find('first', array('conditions' => array('Project.id' => $this->data['Project']['id'])));
                        $title = $project['Project']['title'];

                        $this->Project->updateAll(array('Project.url'=>"'".$this->data['Mentorship']['email']."'"),array('Project.id'=>$this->data['Project']['id']));



                                    $user = $this->User->find('first', array('conditions' => array('User.username' => $this->data['Mentorship']['email'])));
                                    if($user['User']['id'] ==''){


					  	
                                          $this->Session->write('FirstName',$this->data['Mentorship']['firstname']);
                                          $this->Session->write('LastName',$this->data['Mentorship']['lastname']);
                                          $this->Session->write('Email',$this->data['Mentorship']['email']);
 






			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $this->data['Mentorship']['email'];
                           $subject = "Project created: ".$title;
                           $projectlink = SITE_URL.'project/index/'.$project['Project']['project_url'];
 

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("visitor_project_submit_confirmation");
                           $data .= "&merge_userfname=".urlencode($this->data['Mentorship']['firstname']);
                           $data .= "&merge_clientemail=".urlencode($this->data['Mentorship']['email']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           $data .= "&merge_projecttitle=".urlencode($title);
                           $data .= "&merge_projectlink=".urlencode($projectlink);

                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

                        $this->redirect(array('controller' => 'project', 'action' => 'schedule_call', $project['Project']['id']));


                          } else {



                                          $this->Session->write('FirstName',$this->data['Mentorship']['firstname']);
                                          $this->Session->write('LastName',$this->data['Mentorship']['lastname']);
                                          $this->Session->write('Email',$this->data['Mentorship']['email']);
					  $this->Project->updateAll(array('Project.user_id'=>"'".$user['User']['id']."'"),array('Project.id'=>$this->data['Project']['id']));






			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $this->data['Mentorship']['email'];
                           $subject = "Project created: ".$title;
                           $projectlink = SITE_URL.'project/index/'.$project['Project']['project_url'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("visitor_guilduser_project_submit_confirmation");
                           $data .= "&merge_userfname=".urlencode($this->data['Mentorship']['firstname']);
                           $data .= "&merge_clientemail=".urlencode($this->data['Mentorship']['email']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           $data .= "&merge_projecttitle=".urlencode($title);
                           $data .= "&merge_projectlink=".urlencode($projectlink);

                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

                              $this->redirect(array('controller' => 'project', 'action' => 'schedule_call', $project['Project']['id']));

                         }
                        



		          
				
	}



          function consultation_request($mentor_url = null){

          $this->layout= 'defaultnew';
          $mentor = $this->User->find('first', array('conditions' => array('User.url_key' => $mentor_url)));
          $mentor_id = $mentor['User']['id'];

		if(!empty($this->data))
		{

	
			                     $req['ConsultationRequest']['req_message'] = trim($this->data['Mentorship']['mentee_need']);
		                             $req['ConsultationRequest']['consultant_id'] = $mentor['User']['id'];
                                             $req['ConsultationRequest']['client_id'] = $this->Auth->user('id');
                                             $req['ConsultationRequest']['req_status'] = "1";		
		                             $this->ConsultationRequest->save($req);
                                             $id = $this->Auth->user('id');

                                             $clength = strlen($this->data['Mentorship']['mentee_need']);

                                          if($clength > 100 && !($clength <= 130)){
                                             $title = substr($this->data['Mentorship']['mentee_need'], 0, strpos(wordwrap($this->data['Mentorship']['mentee_need'], 80), "\n"));
                                             $projecttitle = $title;
                                          
                                          }else{
                                           $projecttitle = $this->data['Mentorship']['mentee_need'];
                                           }
                                          $project['Project']['user_id'] = $id;
                                          $project['Project']['title'] = $projecttitle;
                                          $project['Project']['details'] = trim($this->data['Mentorship']['mentee_need']);
                                          $project['Project']['budget'] = "$10K to $50K";
                                          $project['Project']['start_date'] = "Within 1 month";
                                          $project['Project']['status'] = "1";
                                          $this->Project->save($project);

  	              $urlKey = preg_replace('/\PL/u', '-', $projecttitle);
		       $urlKey = $urlKey.'-'.$this->Project->id; 
                       $this->Project->updateAll(array('Project.url'=>"'".$this->Auth->user('username')."'"),array('Project.id'=>$this->Project->id));
	    	       $this->Project->updateAll(array('Project.project_url'=>"'".$urlKey."'"),array('Project.id'=>$this->Project->id));
									  
				                      $menteeData = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
				                      $mentorData = $this->User->find('first',array('conditions'=>array('User.id'=>$mentor_id)));


                    $project = $this->Project->find('first', array('conditions' => array('Project.id' => $this->Project->id)));
                    $user = $this->User->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));

                                          $this->Session->write('FirstName',$user['UserReference']['first_name']);
                                          $this->Session->write('LastName',$user['UserReference']['last_name']);
                                          $this->Session->write('Email',$user['User']['username']);

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $user['User']['username'];
                           $subject = "Your requirement has been submitted for review";
                           $projectlink = SITE_URL.'project/index/'.$project['Project']['project_url'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("consultation_project_submit_confirmation");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_clientemail=".urlencode($user['User']['username']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           $data .= "&merge_projecttitle=".urlencode($project['Project']['title']);
                           $data .= "&merge_projectlink=".urlencode($projectlink);

                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

									  
									   
			//EasticEmail Integration - Begin




                           //EasticEmail Integration - End	


				//send email to mentor

				if($menteeData != ''){


		               $prospect['Prospect']['firstname'] = $menteeData['UserReference']['first_name'];
		               $prospect['Prospect']['lastname'] = $menteeData['UserReference']['last_name'];
		               $prospect['Prospect']['email'] = $menteeData['User']['username'];
		               $prospect['Prospect']['company'] = "GUILD";
		               $prospect['Prospect']['phone'] = " ";
		               $prospect['Prospect']['answer'] = trim($this->data['Mentorship']['mentee_need']);
		               $prospect['Prospect']['member_id'] = $mentorData['User']['id'];
		               $prospect['Prospect']['prospect_type'] = "CONSULTATION";
                               $date = date('Y-m-d H:i:s');
		               $prospect['Prospect']['submitted'] = $date;                            
		
		               $this->Prospect->save($prospect);
		



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "iqbal@guild.im";
                           $subject = "A new consultation request has been submitted by a logged in User";


                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("visitor_consultation_submit");
                           $data .= "&merge_consultantfname=".urlencode($mentorData['UserReference']['first_name']);
                           $data .= "&merge_consultantlname=".urlencode($mentorData['UserReference']['last_name']);
                           $data .= "&merge_visitorfname=".urlencode($menteeData['UserReference']['first_name']);
                           $data .= "&merge_visitorlname=".urlencode($menteeData['UserReference']['last_name']);
                           $data .= "&merge_visitoremail=".urlencode($menteeData['User']['username']);
                           $data .= "&merge_visitorphone=".urlencode(" ");
                           $data .= "&merge_visitorneed=".urlencode(trim($this->data['Mentorship']['mentee_need']));
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End



                     }	

                      $this->redirect(array('controller' => 'project', 'action' => 'schedule_call', $this->Project->id));							  

        }else{
			
			$this->set("title_for_layout","Request a free initial consultation");	
			$this->User->Behaviors->attach('Containable');
			//$this->data = $this->User->find('first',array('contain'=>array('UserReference'=>array('fields'=>array('zipcode','first_name','last_name','background_summary','area_of_expertise','application_5_fee')),'UserImage'),'conditions'=>array('role_id'=>2,'User.id'=>$mentor_id,'status'=>"2)));
              

                     $this->data =  $this->User->find('first',
				array(
					'contain'=>array(
						'UserReference'=>array(
							'fields'=>array('zipcode','first_name','last_name','area_of_expertise','application_5_fee','linkedin_headline')
						),
						'UserImage',
                                                'Communication',
                                                'MemberIndustryCategory',
                                          			
					),
					'conditions'=>array('role_id'=>2,'User.id'=>$mentor_id,'status'=>1)		
				)
		              
			);


			//get city data
			if(isset($this->data['UserReference']['zipcode']) && $this->data['UserReference']['zipcode'] !='')
			{
				$cityData = $this->City->find('first',array('conditions'=>array('City.zip_code'=>$this->data['UserReference']['zipcode'])));
				$this->set(compact('cityData'));
			}
			if(empty($this->data))
				$this->redirect(array('action'=>'index'));			
			
		}
		
		
       }




	function getStateCode(){

		$this->loadModel('StateCode');
		$name = trim($this->request->data['state_name']);
		$city = trim($this->request->data['city']);
		$country_code = trim($this->request->data['country_code']);
		$code = $this->StateCode->find('first',array('conditions'=>array('state_name'=>$name)));
		if(!empty($code['StateCode']['state_code'])){
			$state_code = $code['StateCode']['state_code'];
			if($country_code =='US'){
				$this->Session->write("Location",$city.', '.$state_code);
			} else {
				$this->Session->write("Location","International");
			}
		}else{
			$state_code = '';
			if($country_code =='US'){
				$this->Session->write("Location",$city.', '.$name);
			} else {
				$this->Session->write("Location","International");
			}
		}

		echo json_encode(array('value'=>$state_code));
	   $this->layout = '';
	   $this->render(false);
	   exit();
	}


	


	function guarantee(){
             $cachefile = WWW_ROOT."cache/serviceguarantee.html";
             if (file_exists($cachefile)) {

              $this->set("title_for_layout","Client Satisfaction Guarantee");

              $this->set('Cached', true);
              
             } else {
	       $this->set("title_for_layout","Client Satisfaction Guarantee");

		$this->data = $this->StaticPage->find('first',array('conditions'=>array('StaticPage.slug'=>'service_guarantee')));
            }
	}
	
	function consult_iframe($dUserId = null){
	
		if($dUserId != null) {
			$this->set('id', $dUserId);
			$this->layout = 'ajax';
		} 
	}
	
	function consultation_request_iframe($memberId = null){
	
		if($memberId != null) {
			$this->set('id', $memberId);
			$this->layout = 'ajax';
		} 
	}

	function consult($dUserId = null){

                 $this->layout= 'defaultnew';
		
		if($dUserId != null) {
			$dUser = $this->DirectoryUser->find('first',array('conditions'=>array('DirectoryUser.id'=>$dUserId)));
			$this->set('dUSer', $dUser);
			$title  = $dUser['DirectoryUser']['first_name'].' '.$dUser['DirectoryUser']['last_name']. " 's consult request page";

                          $this->set('title_for_layout', $title);
		} else if ($this->RequestHandler->isAjax()) {
			Configure::write('debug', 0);
			
			$message = $_REQUEST['message'];
			$hidId = $_REQUEST['hidId'];
			
			$directory_consultations['message'] = $message;
			$directory_consultations['d_user_id'] = $hidId;
			
			$this->DirectoryConsultation->save($directory_consultations);
			
			$id = $this->DirectoryConsultation->id;
			
			echo json_encode($id);
			$this->render(false);
			exit();
		} else if(!empty($this->data)){
			
			$directory_consultations['message'] = trim($this->data['Mentorship']['mentee_need']);
			$directory_consultations['d_user_id'] = $this->data['DirectoryUser']['id'];
			$directory_consultations['client_id'] = $this->Session->read('Auth.User.id');
			$directory_consultations['random_key'] = substr(md5(uniqid()), 0, 20);
			
			$this->DirectoryConsultation->save($directory_consultations);
			
			//Send Email to Directory user
			$dUser = $this->DirectoryUser->find('first', array('conditions' => array('DirectoryUser.id' => $this->data['DirectoryUser']['id'])));
			 
			$menteeData = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
			
		      $prospect['Prospect']['firstname'] = $menteeData['UserReference']['first_name'];
		      $prospect['Prospect']['lastname'] = $menteeData['UserReference']['last_name'];
		      $prospect['Prospect']['email'] = $menteeData['UserReference']['username'];
		      $prospect['Prospect']['company'] = "GUILD Member";
		      $prospect['Prospect']['phone'] = "";
		      $prospect['Prospect']['answer'] = trim($this->data['Mentorship']['mentee_need']);
		      $prospect['Prospect']['member_id'] = $this->data['DirectoryUser']['id'];
		      $prospect['Prospect']['prospect_type'] = "DIRECTORY CONSULTATION";
                      $date = date('Y-m-d H:i:s');
		      $prospect['Prospect']['submitted'] = $date;
		
		      $this->Prospect->save($prospect);	


			//EasticEmail Integration - Begin

                           /**$res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $dUser['DirectoryUser']['username'];
                           $subject = "Request for Initial Consultation";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("directory_user_consultation");
                           $data .= "&merge_duserfname=".urlencode($dUser['DirectoryUser']['first_name']);                           
                           $data .= "&merge_clientfname=".urlencode($menteeData['UserReference']['first_name']);
                           $data .= "&merge_clientlname=".urlencode($menteeData['UserReference']['last_name']);
                           $data .= "&merge_clientemail=".urlencode($menteeData['User']['username']);
                           $data .= "&merge_clientneed=".urlencode($directory_consultations['message']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }**/


                           //EasticEmail Integration - End

			

                          // Send Email to client		


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $menteeData['User']['username'];
                           $subject = "Your request for an initial consultation";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_application_request_sent");
                           $data .= "&merge_consultantfname=".urlencode($dUser['DirectoryUser']['first_name']);  
                           $data .= "&merge_consultantlname=".urlencode($dUser['DirectoryUser']['last_name']);                         
                           $data .= "&merge_clientfname=".urlencode($menteeData['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

			$this->redirect(array('action'=>'thanks_for_request'));
			
		}
	}
	
	function visitor_consultation_request($memberId = null){

                             
                if(!empty($this->data)){
		
			$member =  $this->DirectoryUser->find('first',array('conditions'=>array('DirectoryUser.id'=>$memberId)));
                     

			
			$member_id = $this->data[DirectoryUser][id];


		      $prospect['Prospect']['firstname'] = $this->data['Mentorship']['firstname'];
		      $prospect['Prospect']['lastname'] = $this->data['Mentorship']['lastname'];
		      $prospect['Prospect']['email'] = $this->data['Mentorship']['email'];
		      $prospect['Prospect']['company'] = "Visitor";
		      $prospect['Prospect']['phone'] = $this->data['Mentorship']['phone'];
		      $prospect['Prospect']['answer'] = trim($this->data['Mentorship']['mentee_need']);
		      $prospect['Prospect']['member_id'] = $member['DirectoryUser']['id'];
		      $prospect['Prospect']['prospect_type'] = "DIRECTORY CONSULTATION";
                      $date = date('Y-m-d H:i:s');
		      $prospect['Prospect']['submitted'] = $date;
		
		      $this->Prospect->save($prospect);			
                     
			$clientname = $this->data['Mentorship']['firstname']." ".$this->data['Mentorship']['lastname'];
                     $message = trim($this->data['Mentorship']['mentee_need']);
                     $clientemail = $this->data['Mentorship']['email'];
                     $clientphone = $this->data['Mentorship']['phone'];

                     // Send Email to Admin


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "ankit@guild.im";
                           $subject = "A new consultation request for Directory Member has been submitted by a visitor";


                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("visitor_consultation_submit");
                           $data .= "&merge_consultantfname=".urlencode($member['DirectoryUser']['first_name']);
                           $data .= "&merge_consultantlname=".urlencode($member['DirectoryUser']['last_name']);
                           $data .= "&merge_visitorfname=".urlencode($this->data['Mentorship']['firstname']);
                           $data .= "&merge_visitorlname=".urlencode($this->data['Mentorship']['lastname']);
                           $data .= "&merge_visitoremail=".urlencode($this->data['Mentorship']['email']);
                           $data .= "&merge_visitorphone=".urlencode($this->data['Mentorship']['phone']);
                           $data .= "&merge_visitorneed=".urlencode(trim($this->data['Mentorship']['mentee_need']));
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


			//Send Email to Directory user


			//EasticEmail Integration - Begin

                           /**$res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $member['DirectoryUser']['username'];
                           $subject = "Consultation request";


                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("visitor_consultation_request");
                           $data .= "&merge_consultantfname=".urlencode($member['DirectoryUser']['first_name']);
                           $data .= "&merge_clientname=".urlencode($clientname);
                           $data .= "&merge_clientemail=".urlencode($clientemail);
                           $data .= "&merge_clientphone=".urlencode($clientphone);
                           $data .= "&merge_clientneed=".urlencode(trim($message));
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }**/


                           //EasticEmail Integration - End

			
                   // Send Email to Visitor

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $this->data['Mentorship']['email'];
                           $subject = "Your request for an initial consultation";


                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("visitor_application_request_sent");
                           $data .= "&merge_consultantfname=".urlencode($member['DirectoryUser']['first_name']);
                           $data .= "&merge_consultantlname=".urlencode($member['DirectoryUser']['last_name']);
                           $data .= "&merge_visitorfname=".urlencode($this->data['Mentorship']['firstname']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

			$this->redirect(array('action'=>'thanks_for_request'));


		
                } 
         
		
	}

	function thanks_for_consulting(){


                $this->layout= 'defaultnew';
	
                $consultationreqid = $this->Session->read('consultationreqid');
                $this->Session->delete('consultationreqid');
                $this->ConsultationRequest->updateAll(array('ConsultationRequest.req_status'=>"2"),array('ConsultationRequest.id'=>$consultationreqid));
    
                $consultationreq = $this->Project->find('first', array('conditions' => array('Project.id' => $consultationreqid)));
                $user = $this->User->find('first', array('conditions' => array('User.id' => $consultationreq['ConsultationRequest']['client_id'])));

                           //EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $user['User']['username'];
                           $subject = $user['UserReference']['first_name'].", your upcoming call with GUILD Client Partner";
                          

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("clientpartner_callscheduled");
                           $data .= "&merge_clientfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }

                         //EasticEmail Integration - End

	}
	
	function feedback_iframe($dUserId = null){
	
		if($dUserId != null) {
			$this->set('id', $dUserId);
			$this->layout = 'ajax';
		}
	}
	

	function give_feedback($dUserId = null){
	
		if($dUserId != null) {
			$dUser = $this->DirectoryUser->find('first',array('conditions'=>array('DirectoryUser.id'=>$dUserId)));
			$this->set('dUSer', $dUser);
			$this->layout = 'iframe';
		} else if ($this->RequestHandler->isAjax()) {
			Configure::write('debug', 0);
				
			$message = $_REQUEST['message'];
			$hidId = $_REQUEST['hidId'];
			$feedback_val = $_REQUEST['feedback'];
				
			if($feedback_val == 1) //y
				$feedback['recommended'] = 2;
			else if($feedback_val == 2) //n
				$feedback['recommended'] = 0;
			else if($feedback_val == 3) //mb
				$feedback['recommended'] = 1;
			$feedback['description'] = $message;
			$feedback['d_user_id'] = $hidId;
				
			$this->Feedback->save($feedback);
				
			$id = $this->Feedback->id;
				
			echo json_encode($id);
			$this->render(false);
			exit();
		} else if($this->data){
			
			if($this->data['Feedback']['recommended'] == 1) //y
				$feedback['recommended'] = 2;
			else if($this->data['Feedback']['recommended'] == 2) //n
				$feedback['recommended'] = 0;
			else if($this->data['Feedback']['recommended'] == 3) //mb
				$feedback['recommended'] = 1;
			
			$feedback['description'] = $this->data['Feedback']['description'];
			$feedback['d_user_id'] = $this->data['DirectoryUser']['id'];
			$feedback['client_id'] = $this->Session->read('Auth.User.id');
			
			$this->Feedback->save($feedback);
			
			$this->redirect(array('action'=>'thanks_for_feedback'));
			
		}
	}
	function thanks_for_feedback(){
		$this->layout = 'iframe';
	}
	function claim_profile($dUserId){
	
                 $this->layout= 'defaultnew';

                if($this->Session->check('menteePopup')){
                $this->Session->delete('menteePopup');
                }
                if($this->Session->check('projectId')){
                $this->Session->delete('projectId');
                } 
                if($this->Session->check('questionId')){
                $this->Session->delete('questionId');
                } 
                if($this->Session->check('consultantPopup')){
                $this->Session->delete('consultantPopup');
                }
                if($this->Session->check('login_industry')){
                $this->Session->delete('login_industry');
                } 
                if($this->Session->check('mentor')){
                $this->Session->delete('mentor');
                } 
                if($this->Session->check('directoryclaim')){
                $this->Session->delete('directoryclaim');
                } 

                $this->Session->write('directoryclaim',$dUserId);
                
		if($this->Session->read('Auth.User.id') != ''){
			
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("Sorry! You are unauthorized user. Contact us at <a href='mailto:help@guild.im' class='delete'>help@guild.im</a>", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
			die;
			
		}
		if($dUserId != null) {
			$dUser = $this->DirectoryUser->find('first',array('conditions'=>array('DirectoryUser.id'=>$dUserId)));
			$this->set('dUSer', $dUser);

		} 
                $title  = $dUser['DirectoryUser']['first_name'].' '.$dUser['DirectoryUser']['last_name']. " 's claim profile page";

                $this->set('title_for_layout', $title);
	}
	
	function directory(){
		
		//Finding directory members
		$dUsers = $this->DirectoryUser->find('all',array('conditions'=>array('DirectoryUser.hasPublished'=>0), 'order'=>array('DirectoryUser.last_name'=>'asc')));
		$dUsersFinal = array();
		$inc = 0;
		foreach ($dUsers as $key=>$value) {
			$dUsersFinal[$inc++] = $value;
		}
			
		$dUsersFinalSorted = Set::sort($dUsersFinal, '{n}.DirectoryUser.quality', 'asc');
		$this->set('dUsersFinal', $dUsersFinalSorted);
		
	}


        function media_response() {
                     
                        
                     if(!empty($this->data)) {
			$this->User->Behaviors->attach('Containable');
			$user = $this->User->findById($this->Auth->user('id'));

                     $refer = $user['User']['url_key'];


					$socialCount=0;
					
                                   $twitterURL ='';

					while(count($user['Social'])>$socialCount)
					{
					
					if(($pos =strpos($user['Social'][$socialCount]['social_name'],'http'))!==false)
						{
							$link=$user['Social'][$socialCount]['social_name'];
						}
						else
						{
							$link="https://".$user['Social'][$socialCount]['social_name'];
						}
						
						
                                          if(strpos($link, "twitter")!== false)
							$twitterURL = $link;

						$socialCount++;
						}

                                      
                     $image_link = MENTORS_IMAGE_PATH.DS.$user['User']['id'].DS.$user['UserImage'][0]['image_name']; 
                     
                     $communication = $this->Communication->find('first', array('conditions' => array('AND' => array(array('Communication.user_id' => $user['User']['id']),array('Communication.mode_type' => 'phone')))));
                     $reporter = $this->MediaRecords->find('first', array('conditions' => array('MediaRecords.id' => $this->data['QueryId'])));
                     $find_question = $this->QnaQuestion->find('first', array('conditions' => array('QnaQuestion.media_query_id' => $this->data['QueryId'])));
                     if($find_question !='' && $user['User']['access_specifier'] == "publish"){                     
                     $question_answer['question_id']= $find_question['QnaQuestion']['id'];
                     $answer = $this->data['EmailBody'];
                     $question_answer['answer_text']= trim($answer);
                     $question_answer['member_id']= $user['User']['id'];
                     $this->QnaAnswer->save($question_answer);
                     }

				


		   //EasticEmail Integration - Begin

                           $res = "";
                           $websiteurl = SITE_URL;
                           $from = $user['User']['username'];
                           $fromName = $user['UserReference']['first_name']." ".$user['UserReference']['last_name'];
                           $to = "ankit@guild.im";
                           $subject = $reporter['MediaRecords']['subject'];


                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("media_reply");
                           $data .= "&merge_reportername=".urlencode($reporter['MediaRecords']['reporter_name']);
                           $data .= "&merge_reporteremail=".urlencode($reporter['MediaRecords']['reporter_email']);
                           $data .= "&merge_senderfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_senderlname=".urlencode($user['UserReference']['last_name']);
                           $data .= "&merge_senderemail=".urlencode($user['User']['username']);
                           $data .= "&merge_senderphone=".urlencode($communication['Communication']['mode']);
                           $data .= "&merge_senderheadline=".urlencode($user['UserReference']['linkedin_headline']);
                           $data .= "&merge_senderreply=".urlencode(nl2br(trim($this->data['EmailBody'])));
                           $data .= "&merge_sendertwiter=".urlencode($twitterURL);
                           $data .= "&merge_senderimage=".urlencode($image_link);
                           $data .= "&merge_senderurlkey=".urlencode($refer);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
                    

                        if($this->Session->read('Auth.User.mentor_type') != 'Premium Member')
                          {              

				$this->redirect(array('controller'=>'members','action'=>'pr', 'OpenSubscribePopup:'.true));
                        }else{
                         $this->redirect(array('controller'=>'members','action'=>'pr'));
                        }

		} else {

			$this->User->Behaviors->attach('Containable');
			$user = $this->User->findById($this->Auth->user('id'));
			$this->layout = 'ajax';
			$this->set("fname", $user['UserReference']['first_name']);
                     $this->set("refer", $user['User']['url_key']);
                     $media = $this->MediaRecords->find('first', array('conditions' => array('MediaRecords.id' => $subject_id)));
                     $this->set("subject", $media['MediaRecords']['subject']);
                     $this->set("queryid", $media['MediaRecords']['id']);
		}
                 

		
	}

    function schedule_call($consultationreqid){

          $this->layout= 'defaultnew';
          $this->set("title_for_layout","Consultation Request Schedule Call");


                        $consultationreq = $this->ConsultationRequest->find('first', array('conditions' => array('ConsultationRequest.id' => $consultationreqid)));
                        if($consultationreq['ConsultationRequest']['client_id'] =='')
                        {
                        $this->ConsultationRequest->updateAll(array('ConsultationRequest.client_id'=>"'".$this->Auth->user('id')."'"),array('ConsultationRequest.id'=>$consultationreqid));

                        
                        }      
                        $this->Session->write('consultationreqid',$consultationreqid);

                        $user = $this->User->find('first', array('conditions' => array('User.id' => $consultationreq['ConsultationRequest']['client_id'])));

                        $this->set("fname",$user['UserReference']['first_name']);
                        $this->set("lname",$user['UserReference']['last_name']);
                        $this->set("email",$user['User']['username']);
                         
       }

   function expertise_search(){

           $this->layout= 'defaultnew';
           $this->set("title_for_layout","Search For Experts");

		//get city data
		$this->loadModel('City');
		if(isset($this->data['UserReference']['zipcode']) && $this->data['UserReference']['zipcode'] !=''){
			$cityData = $this->City->find('first',array('conditions'=>array('City.zip_code'=>$this->data['UserReference']['zipcode'])));
			$this->set(compact('cityData'));
		}

		if($this->Session->read('Auth.User.id') != '' && $this->Session->read('Auth.User.id') != 1) {
			$loggedInUser = $this->User->find('first',
				array(
					'contain'=>array(
						'UserReference'=>array(
							'fields'=>array('zipcode','first_name','last_name','background_summary','area_of_expertise')
						),
						'UserImage'			
					),
					'conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'),'status'=>Configure::read('App.Status.active'))		
				)
			);
			if($loggedInUser['UserReference']['zipcode'] !=''){
				$logg = "Azeez";
				$this->set('logg', $logg);
				$loggedInUsercityData = $this->City->find('first',array('conditions'=>array('City.zip_code'=>$loggedInUser['UserReference']['zipcode'])));
				
				if( $loggedInUsercityData == ''){
					$loggedInUsercityData['City']['city_name'] = 'New York';
					$loggedInUsercityData['City']['state'] = 'NY';
				}
				$this->set(compact('loggedInUsercityData'));
			}
		}

		$keywords = $this->Topic->find('all',array(
				'conditions'=>array('Topic.topic_subset'=>'1'),
				'fields' => array('DISTINCT (Topic.autocomplete_text) AS autocomplete_text'),
		));
		$key = array();
		$cn = 0;
		foreach($keywords as $value){
			$key[$cn++] = $value['Topic']['autocomplete_text'];
		}
		$this->set('allKeywords', $key);

        }

    function thanks_for_request(){
          $this->layout= 'defaultnew';
          $this->set("title_for_layout","Request has submitted");

   }

    function emailsession() {
    	 
    	$emailvalue= trim($this->request->data['emailvalue']);
    	 
    	if (!empty($emailvalue)) {
                      $this->Session->delete('visitoremail');

                 $_SESSION['myapp']['user_start'] = time();
                 $this->Session->write('visitoremail',$emailvalue);

                 $feed['visitoremailid'] = $emailvalue;
                 $this->Emailprospect->save($feed);


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "iqbal@guild.im";
                           $subject = "A new visitor has shown interest in GET Started";


                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("vistior_interest_email_to_admin");

                           $data .= "&merge_visitoremail=".urlencode($emailvalue);

                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

    		  
 		 
    		echo json_encode(array('id'=>1));
    		$this->layout = '';
    		$this->render(false);
    		exit();
    
    	}
    }

	function admin_visitoremailaddress() {
    	if($this->Auth->user('id') == 1){
		
		$this->layout = 'admin';  
		$prospectives = $this->Emailprospect->find('all');
		
		$this->set('data', $prospectives);	 
		$this->set('title_for_layout', 'Client');
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
	}



function __callapi($method, $url, $data){
   $curl = curl_init();


         if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
   

$header = array(
    'Accept: application/json',
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Basic '. base64_encode("guild-admin:APblgTqi8j6oVnNoyA==")
);

   // OPTIONS:
   curl_setopt($curl, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

   // EXECUTE:
   $result = curl_exec($curl);
   if(!$result){die("Connection Failure");}
   curl_close($curl);
   return $result;
}


     function thanks_for_interest(){

          $this->layout= 'defaultnew';
          $this->set("title_for_layout","Thanks for interest!!");


       }


  function admin_homepage_prospective(){

       	if($this->Auth->user('id') == 1){
		
		$this->layout = 'admin'; 

      $username = "guild-admin";
      $password = "APblgTqi8j6oVnNoyA==";
      $baseUrl = "https://api-test.guild.im";
      $url = "$baseUrl/schedulecall";
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      $result = curl_exec($ch);
      curl_close($ch);
      $json = json_decode($result, true);

       $this->set('data', $json['items']);

     }		


     
    }

		
}

?>