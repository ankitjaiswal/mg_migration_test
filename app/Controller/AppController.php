<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    /**
     * Components
     *
     * @var array
     * @access public
     */    
    var $components = array(    
        'Security',          
        'Auth',  
		'Session',	
        'RequestHandler',       
        'Email',
        'General',
        'Breadcrumb',
		'Cookie',		
    );
    
    /**
     * Helpers
     *
     * @var array
     * @access public
     */
    var $helpers = array(
        'Html',
        'Form',
        'Text',
        'Javascript',
        'Time',
        'Layout',
		'General',
		'Ajax',
		'ExPaginator',
		'Fck',
		'Admin',
		'Thickbox'
    );
    /**
     * Models
     *
     * @var array
     * @access public
     */
    var $uses = array();
    /**
     * Cache pagination results
     *
     * @var boolean
     * @access public
     */
    //var $usePaginationCache = true;
    /**
     * View
     *
     * @var string
     * @access public
     */
   // var $view = 'Theme';
    /**
     * Theme
     *
     * @var string
     * @access public
     */
  //  var $theme;
    /**
     * Set Pagination setting
     * */
//   var $paginate = array('limit'=>'4');

    /**
     * beforeFilter
     *
     * @return void
     */
    function beforeFilter() {
		
		$this->Auth->authenticate = array('Form' => array('passwordHasher' => 'Blowfish'));
		
		$this->Security->blackHoleCallback = '__securityError'; 	
		$this->disableCache();
		if($this->request['controller'] == 'fronts' && $this->request['action'] == 'search_result'){
			$this->Session->write('back_hack',1);
		}else{
			if($this->request['controller'] != 'fronts' && $this->request['action'] != 'index'){
				$this->Session->delete('back_hack');
			}
		}
			if(isset($this->request['admin'])) {
				$this->layout = 'admin';             	     
				$this->Auth->userModel = 'User';
				$this->Auth->userScope = array('User.status' =>1,'User.role_id'=>1);
				$this->Auth->loginError = "Login failed. Invalid Email Address or password";
				$this->Auth->fields = array('username'=>'username', 'password'=>'password');
				$this->Auth->loginAction = array('admin' => true, 'controller' => 'users', 'action' => 'login');
				$this->Auth->loginRedirect = array('admin' => true, 'controller' => 'users', 'action' => 'dashboard');
				$this->Auth->authError = 'You must login to view this information.';
				$this->Auth->authorize 		= 'controller';
				$this->Auth->autoRedirect = true;
				//$this->Auth->allow('login'); 		
			}else{
				$this->Auth->userModel 		=	'User';
				$this->Auth->userScope 		= 	array('User.status' =>1);
				$this->Auth->loginError 	=	"Login failed. Invalid Email Address or password";
				$this->Auth->loginAction 	=   array('controller' => 'users', 'action' => 'login');
				$this->Auth->fields			=	array('username' => 'username', 'password' => 'password');	
				$this->Auth->loginRedirect 	=	array('admin'=>false,'controller' => 'users', 'action' => 'my_account');	
				$this->Auth->logoutRedirect = "https://test.guild.im";
				$this->Auth->authorize 		= 'controller';				
				$this->Auth->autoRedirect 	= 	false;				
				//$this->Auth->allowedActions	= 	array('logout', 'login','signup');
							
			}
			$this->loadModel('Setting');
			$this->Setting->getSetting();				
			$this->isAuthorized();
			
    }   
	function isAuthorized() {
		 if(isset($this->request['admin'])) {
			 if($this->Auth->user()){
				if($this->Auth->user('role_id') != 1){
				   $this->cakeError('accessDenied');					
			   }
			}
		 }
        return true;
    }
	 function __securityError() {
        //$this->cakeError('securityError');
    }
     function appError($error) {
       //$this->redirect(array('controller'=>'staticPages','action'=>'page_not_found'));
    }
		/**
     * undocumented function
     *
     * @param string $model
     * @return void
     * @access public
     */
	function pageForPagination($model) {
			$page = 1;
			$sameModel = isset($this->request['named']['model']) && $this->request['named']['model'] == $model;
			$pageInUrl = isset($this->request['named']['page']);
			if ($sameModel && $pageInUrl) {
					$page = $this->request['named']['page'];
			}
		 
			$this->passedArgs['page'] = $page;
			
			return $page;
	}
	function __sendMail($To, $Subject, $message, $From,$template, $smtp="0") {
         
			$this->Email->to      = $To;
			$this->Email->from    = "cellsolo.com<".$From.">";
			$this->Email->subject = $Subject;           
			$this->Email->sendAs = 'both';			
			$this->Email->template = $template;
			$this->Email->layout = 'default';			
			if($smtp == 1)
			{
					$this->Email->smtpOptions = array(
					'port' => '25',
					'timeout' => '30',
					'host' => '',
					'username' => '',
					'password' => '',
					'client' => ''
					);
					$this->Email->delivery = 'smtp';
			}           
			$this->set('message',$message);
			if($this->Email->send($message))
			{
					return true;
			}
			else
			{
					return false;
			}
		}
		function displaySqlDump(){
		 if (!class_exists('ConnectionManager') || Configure::read('debug') < 2) {
				return false;
			}
			$noLogs = !isset($logs);
			if ($noLogs):
				$sources = ConnectionManager::sourceList();

				$logs = array();
				foreach ($sources as $source):
					$db =& ConnectionManager::getDataSource($source);
					if (!$db->isInterfaceSupported('getLog')):
						continue;
					endif;
					$logs[$source] = $db->getLog();
				endforeach;
			endif;

			if ($noLogs || isset($_forced_from_dbo_)):
				foreach ($logs as $source => $logInfo):
					$text = $logInfo['count'] > 1 ? 'queries' : 'query';
					printf(
						'<table class="cake-sql-log" id="cakeSqlLog_%s" summary="Cake SQL Log" cellspacing="0" border = "0">',
						preg_replace('/[^A-Za-z0-9_]/', '_', uniqid(time(), true))
					);
					printf('<caption>(%s) %s %s took %s ms</caption>', $source, $logInfo['count'], $text, $logInfo['time']);
				?>
				<thead>
					<tr><th>Nr</th><th>Query</th><th>Error</th><th>Affected</th><th>Num. rows</th><th>Took (ms)</th></tr>
				</thead>
				<tbody>
				<?php
					foreach ($logInfo['log'] as $k => $i) :
						echo "<tr><td>" . ($k + 1) . "</td><td>" . h($i['query']) . "</td><td>{$i['error']}</td><td style = \"text-align: right\">{$i['affected']}</td><td style = \"text-align: right\">{$i['numRows']}</td><td style = \"text-align: right\">{$i['took']}</td></tr>\n";
					endforeach;
				?>
				</tbody></table>
				<?php 
				endforeach;
			else:
				echo '<p>Encountered unexpected $logs cannot generate SQL log</p>';
			endif;	
		}
/* 		function displayCategoryTree($pid,$level)
		{
			global $res;
			$res[0] = "-Root-";
			$blank = "";	
			
			for($i=0; $i< $level; $i++)
			$blank   .=  "-";
			
			$parents = $this->find('all' , array('conditions' => array('Category.parent_id' => $pid)));
			
			if(!empty($parents))
			$level++;
			
			foreach($parents as $value)
			{		
				unset($value['ParentCategory']);
				unset($value['Product']);
				$res[$value['Category']['id']]	= $blank.$value['Category']['name'];			
				//pr($res);
				$this->displayCategoryTree($value['Category']['id'],$level);
			}
			return $res;
		} */
		/* =========================================== */
		/* 
		 *added at 30-04-12
		 *added by S.R.
		 * @param $keyword is search value
		 * @param $op an fields array to apply key value		
		 * @return array An array of model conditions
		 * @access public		
			
		*/
		/* =========================================== */
		function _keyConditions($keyword= null,$op = null){

			if ($op === null || !is_array($op) || empty($keyword)) {
				return null;
			}
			$arrayOp = array();
				
			foreach ($op as $model => $field){
				if(is_array($field)){
					foreach($field as $title){
						$arrayOp[$model][$title] = $keyword;
					}
				}else{
					$arrayOp[$model][$field] = $keyword;
				}
			}		
			return $arrayOp;
			
		}
		
		function checkWithoutActiveuser()
		{
			if($this->Session->read('Auth.User.id')!='' && $this->Session->read('Auth.User.role_id')== Configure::read('App.Role.Mentee'))
			{				
				if($this->Auth->user('email_send') =='1' && $this->Auth->user('is_approved') =='0')
				{
					$this->Session->delete();
   					$this->redirect(array('controller'=>'fronts','action'=>'index')); exit;
				}	
			}	
		}
}
?>
