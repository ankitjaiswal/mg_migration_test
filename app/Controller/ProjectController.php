<?php


require('../webroot/stripe-php-1.17.2/lib/Stripe.php');
class ProjectController extends AppController {

    var $name	=	'Project';	
    var $uses = array('Project', 'User', 'UserReference','Topic', 'ProjectMember','ProjectMemberConfirm',
    				'NewsLetter','Social', 'UserImage', 'Mentorship', 'City','Member','Prospect','IndustryCategory','Proposal','ProposalTopic','ProjectInvoice');

	var $helpers = array('General', 'Form', 'Ajax', 'javascript', 'Paginator', 'Html', 'Image','Ical');
	var $components = array("Session", "Email", "Auth", "RequestHandler" ,"Upload", "General",'Paginator');

    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('create','add_project', 'view', 'finalise',  'profile', 'preview','view_project','postproject', 'index','thanks_for_posting',
				'password_setting_popup','projectAuth','projectlinkedinAuth','showInterest','consultation_request_ajax','consultation_request',
				'consultant_view','project_creator','project_consultation','project_details','home',
				'admin_review','projectlistclient','projectlistconsultant','admin_approveproject','projectlistmember','admin_disapproveproject',
				'admin_deleteproject','proposal','proposal_create','proposal_preview','add_proposal','finalize_proposal','project_proposals','proposal_edit',
				'projectproposal','proposal_pdf_preview','preview_pdf','preview_pdf1','project_adminedit','admin_proposalreview','project_adminview','proposal_adminedit',
				'admin_deleteproposal','schedule_call','stripecheckout','stripe_payment','projectedit','client_approveproject','project_view','thanks_for_payment',
				'invoice_create','project_invoice','proposal_accept_ajax','invoice_detail','invoice_preview','stripecheckout1','admin_changeprojectstatus',
				'admin_changeproposalstatus','invoicepdf1','invoicepdf2','invoicepdf3','invoicepdf4','invoice_pdf','sendinvoice','thanks_for_invoicepayment','show_interest','index1','projectinterestlinkedinAuth','confirm_interest','proposal_ignore_ajax','project_start','project_close','client_auth_popup');
    }
    
    

    function add_project() {
    	 
    	if ($this->RequestHandler->isAjax()) {
    		Configure::write('debug', 0);
    			
    		$project['title'] = $this->request->data['title'];
    		$project['details'] = $this->request->data['details'];
   
    		$project['status'] = 0;
              
			$this->Project->save($project);
			$id = $this->Project->id;

                   


		echo json_encode($id);
    		$this->render(false);
    		exit();
    	}
    }
    
    function create($projectId = null) {

        $this->set("title_for_layout","Post A Project");

        $this->layout= 'defaultnew';
	$this->Session->write('popup', 1);  

        $industry_categories = $this->IndustryCategory->find('all',array('order'=>'IndustryCategory.id asc'));
        $this->set('industry_categories', $industry_categories);  	
    	if($projectId != null){
    		
    		$project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectId)));
    		


    		$this->data = $project;
    		
    	} else if(!(empty($this->data))){
             
                                   if($this->data['Project']['title']==''){

                                          $clength = strlen($this->data['Project']['details']);

                                          if($clength > 100 && !($clength <= 130)){
                                             $title = substr($this->data['Project']['details'], 0, strpos(wordwrap($this->data['Project']['details'], 80), "\n"));
                                             $this->request->data['Project']['title'] = $title;
                                          
                                          }else{
                                           $this->request->data['Project']['title'] = $this->data['Project']['details'];
                                           }

                                          }

    		$this->request->data['Project']['user_id'] = $this->Auth->user('id');
                $this->request->data['Project']['url'] = $this->Auth->user('username');
    		$this->request->data['Project']['status'] = 0;
                
                if($this->data['Resume1']['resume_location']['name']['name'] !=''){
                $this->request->data['Project']['filepath'] = $this->data['Resume1']['resume_location']['name']['name'];
                }
              
              $this->Project->save($this->request->data['Project']);





                   if ($this->data['Resume1']['resume_location']['name'] != '') {
                       

    			$destination = IMAGES . MENTEES_PROJECT_PATH . DS . $this->Project->id;
                     
    			if (!is_dir($destination)) {
                          
    				mkdir($destination, 0777);
                    
    			}
                    
    			$destination .= DS . $this->data['Resume1']['resume_location']['name']['name'];
    			
    			move_uploaded_file($this->data['Resume1']['resume_location']['name']['tmp_name'], $destination);
    			
    			
    		}


    		
    		$this->redirect(array('action' => 'view', $this->Project->id));
    	}
    }

    function view($projectId){


                $this->layout= 'defaultnew';
                if($this->Session->check('menteePopup')){
                $this->Session->delete('menteePopup');
                }
                if($this->Session->check('projectId')){
                $this->Session->delete('projectId');
                } 
                if($this->Session->check('questionId')){
                $this->Session->delete('questionId');
                } 
                if($this->Session->check('login_industry')){
                $this->Session->delete('login_industry');
                } 
                if($this->Session->check('mentor')){
                $this->Session->delete('mentor');
                }
                if($this->Session->check('duserid')){
                $this->Session->delete('duserid');
                } 
                if($this->Session->check('mentorshipId')){
                $this->Session->delete('mentorshipId');
                }
                if($this->Session->check('consultantPopup')){
                $this->Session->delete('consultantPopup');
                }
                if($this->Session->check('directoryclaim')){
                $this->Session->delete('directoryclaim');
                } 
    	

	    	if($projectId != null){
	    	
	    		$project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectId)));

  	              $urlKey = preg_replace('/\PL/u', '-', $project['Project']['title']);
		       $urlKey = $urlKey.'-'.$project['Project']['id']; 
	    	       $this->Project->updateAll(array('Project.project_url'=>"'".$urlKey."'"),array('Project.id'=>$project['Project']['id']));



	    		$this->data = $project;

			$this->Session->write('projectId',$projectId);
	    	}

    }
    
    function finalise() {
    	
    	if(!(empty($this->data))){
    		
              	$project = $this->Project->find('first', array('conditions' => array('Project.id' => $this->data['Project']['id'])));
    		    $project['Project']['phone_number'] = $this->data['Project']['phone_number'];
                    if($this->Session->read('Auth.User.role_id')!=1){
    		    $project['Project']['status'] = 1; // Project saved with number
                    }



                    if($project['Project']['user_id'] =='' && $this->Auth->user('id')!='' && $this->Session->read('Auth.User.role_id')!=1){
                    $project['Project']['user_id'] = $this->Auth->user('id');
                    $project['Project']['url'] = $this->Auth->user('username');
                    }

     		    $this->Project->save($project);   
                    $project = $this->Project->find('first', array('conditions' => array('Project.id' => $this->data['Project']['id'])));
                    $user = $this->User->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));



                                          $this->Session->write('FirstName',$user['UserReference']['first_name']);
                                          $this->Session->write('LastName',$user['UserReference']['last_name']);
                                          $this->Session->write('Email',$user['User']['username']);

                       if($this->Session->read('Auth.User.role_id')!=1){
			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $user['User']['username'];
                           $subject = "Project created: ".$project['Project']['title'];
                           $link = SITE_URL.'clients/my_account/'.$user['User']['id'];
                           $projectlink = SITE_URL.'project/index/'.$project['Project']['project_url'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("project_submit_confirmation");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           $data .= "&merge_clientemail=".urlencode($user['User']['username']);
                           $data .= "&merge_projecttitle=".urlencode($project['Project']['title']);
                           $data .= "&merge_profilelink=".urlencode($link);
                           $data .= "&merge_projectlink=".urlencode($projectlink);

                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


		
		    		
               	    $this->redirect(array('action' => 'schedule_call', $this->Project->id));
                     }
                   else{
                        $project = $this->Project->find('first', array('conditions' => array('Project.id' => $this->data['Project']['id'])));
                        $user = $this->User->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));
                        


                           //EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $user['User']['username'];
                           $projectlink = SITE_URL.'project/index/'.$project['Project']['project_url'];
                           $subject = $user['UserReference']['first_name'].", Please approve project details";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("clientpartner_editproject");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_projecttitle=".urlencode($project['Project']['title']);
                           $data .= "&merge_projectlink=".urlencode($projectlink);
                           $data .= "&merge_clientemail=".urlencode($user['User']['username']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }

                         //EasticEmail Integration - End
                          $this->redirect(array('action' => 'admin_review'));
                     }
                   
 

    	} else {
    		$this->redirect(array('controller'=>'fronts','action'=>'index'));
    	}
    }
    
     function thanks_for_posting(){

                $this->layout= 'defaultnew';
	
                $projectid = $this->Session->read('projectId');
                $this->Session->delete('projectId');
                $this->Project->updateAll(array('Project.status'=>"2"),array('Project.id'=>$projectid));
    
                $project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectid)));
                $user = $this->User->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));



               if($user['User']['id'] == '')
               {

                        $clientfname =  $this->Session->read('FirstName');
                        $clientlname =  $this->Session->read('LastName');
                        $clientemail =  $this->Session->read('Email');
                        $this->Session->delete('FirstName');
                        $this->Session->delete('LastName');
                        $this->Session->delete('Email');

                 }

                else{

                     $clientemail = $user['User']['username'];
                     $clientfname = $user['UserReference']['first_name'];

                    }
               

           
            

                           //EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $clientemail;
                           $subject = $clientfname.", your upcoming call with GUILD Client Partner";
                          

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("clientpartner_callscheduled");
                           $data .= "&merge_clientfname=".urlencode($clientfname);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }

                         //EasticEmail Integration - End


              
	}

          function view_project($activation_key){


                 $project = $this->Project->find('first',array('conditions'=>array('Project.activation_key'=>$activation_key)));

    	      if($this->Session->read('Auth.User.role_id') == 3 || $this->Session->read('Auth.User.role_id') == 2) 
            {

		     $user = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
                   
                    if($user['User']['username'] == $project['Project']['url'])
                    {
  			$project['Project']['activation_key'] = '-1';   
	               $this->Project->save($project);

                
                     $this->redirect(array('action' => 'index',$project['Project']['project_url']));
                   
                     }
			else{
			$this->Session->setFlash(__("Sorry, the project is assigned to another user account. Please contact us at <a href='mailto:help@guild.im' class='delete'>help@guild.im</a>.", true), 'flash_bad');
			$this->Session->write('RedirectTo','Front');
                     $this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
			}

		} else{

                       /**if($project['Project']['status']== 5){
			$this->Session->setFlash(__("Sorry, the project is not approved. Please contact <a href='mailto:help@guild.im' class='delete'>help@guild.im</a>.", true), 'flash_bad');
			$this->Session->write('RedirectTo','Front');
                       $this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
                       }
                      else{**/

				$this->Session->write('Project.Submit.Visitor',$activation_key);

				$this->redirect(array('action'=>'thanks_for_posting','OpenProjectPopup:'.true));
                     //}
             }

}


    

   


    
    function admin_approveproject($projectId = null){
    	if($this->Auth->user('id') == 1){    	
		$this->layout = 'admin';  
    	if($this->Auth->User('id') != '' && $this->Auth->User('role_id') == 1) {
	    	
    		if($projectId) {
	    		
		    	$ProjectMembers = $this->ProjectMember->find('all', array('conditions' => array('ProjectMember.project_id' => $projectId)));
		    	
		    	$project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectId)));


	

		    	foreach ($ProjectMembers as $members) {
		    		

		    		$this->project_interest($members['ProjectMember']['member_id'], $projectId);

                          }
		    	
		    	$project['Project']['status'] = 4; //Project approved by admin
		    	 
		    	$this->Project->save($project);
		    	
		    	$this->Session->setFlash('Project has been approved', 'admin_flash_good');
		    	
		    	$this->redirect(array('action' => 'admin_review'));
	    	}
    } else {
    		$this->redirect(array('controller'=>'fronts','action'=>'index'));
    	}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }
    
    function project_interest($member_id, $projectId)

    {


       	      $mentorData = $this->User->find('first',array('conditions'=>array('User.id'=>$member_id)));

		    	$project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectId)));


                           //EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $mentorData['User']['username'];
                           $subject = "A New Project has been Submitted";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("new_project_link");
                           $data .= "&merge_consultantfname=".urlencode($mentorData['UserReference']['first_name']);
                           $data .= "&merge_consultantemail=".urlencode($mentorData['User']['username']);
                           $data .= "&merge_projecttitle=".urlencode($project['Project']['project_url']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }

                         //EasticEmail Integration - End

       }


     function consultant_view($email = null, $project_url = null){

          if($this->Auth->User('id') != '') {
	    	            
                 $this->redirect(array('action' => 'index', $project_url));

           } else{

                 if($email != null && $project_url != null) {

                      $this->set('project_url',$project_url);
                      $this->Session->write('project_url',$project_url);
                      $this->Session->write('myaccount',$email);
                      $this->Session->write('menteeActEmail',$email);

                      $this->redirect(array('controller'=>'fronts','action' => 'index'));

                  
  		   } else {
			$this->Session->setFlash(__("Sorry, the project URL is  not match with this link. Please contact us at <a href='mailto:help@guild.im' class='delete'>help@guild.im.</a>", true), 'flash_bad');
			$this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));

                 } 

         }

    }		




		

    function admin_disapproveproject($projectId = null){
     	if($this->Auth->user('id') == 1){   	 
		 $this->layout = 'admin';  
    	if($this->Auth->User('id') != '' && $this->Auth->User('role_id') == 1) {
    
    		if($projectId) {
    	   
    			$project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectId)));
    		  
    			$project['Project']['status'] = 5; 
    
    			$this->Project->save($project);
    			
    			$condionArr['User.id'] = $project['Project']['user_id'];
    			$createdBy = $this->User->find('first',
    					array(
    							'contain'=>array(
    									'UserReference'=>array(
    											'fields'=>array('first_name','last_name')
    									),
    									'UserImage'
    							),
    							'conditions'=>$condionArr
    					)
    			);
    			

    			
                           //EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $createdBy['User']['username'];
                           $subject = "Project disapproved";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("project_disapproved");
                           $data .= "&merge_userfname=".urlencode($createdBy['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }

                         //EasticEmail Integration - End
    		  

    			$this->Session->setFlash('Project has been disapproved', 'admin_flash_good');
    		  
    			$this->redirect(array('action' => 'admin_review'));
    		}
    	} else {
    		$this->redirect(array('controller'=>'fronts','action'=>'index'));
    }
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }
    
    
    function index($projectUrl = null, $email = null, $prospect = null){

               if($this->Session->check('projectId')){
                $this->Session->delete('projectId');
                } 


        $this->layout= 'defaultnew';

                if($email != null && $prospect =="register"){
                 $user = $this->Prospect->find('first',array('conditions'=>array('Prospect.email'=>$email)));
                 $this->set('firstname', $user['Prospect']['firstname']);
                 $this->set('lastname', $user['Prospect']['lastname']);
                 $this->set('useremail', $user['Prospect']['email']);
                 $this->Session->write('projectUrl', $projectUrl);
                 $this->set('prospect', $prospect);
                 $this->set('username', $email);                 
            
                 }

                if($email != null && $prospect =="login"){
                 $this->set('username', $email);
                 $this->set('prospect', $prospect);
                 }
    

              
	    	if($projectUrl!= null) {

                     
	    		$project = $this->Project->find('first', array('conditions' => array('Project.project_url' => $projectUrl)));
                       
                      
                      if($project != null){

                        $user = $this->User->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));

                                          $this->Session->write('FirstName',$user['UserReference']['first_name']);
                                          $this->Session->write('LastName',$user['UserReference']['last_name']);
                                          $this->Session->write('Email',$user['User']['username']);
                                          $this->Session->write('projectId', $project['Project']['id']);
	    		
	    		$ProjectMembers = $this->ProjectMemberConfirm->find('all', array('conditions' => array('ProjectMemberConfirm.project_id' => $project['Project']['id'])));

	    		$ProposalMembers = $this->Proposal->find('all', array('conditions' => array('Proposal.projectid' => $project['Project']['id'])));

                     $assigned_members = $this->ProjectMember->find('all', array('conditions' => array('ProjectMember.project_id' => $project['Project']['id'])));
                     
                     $assignedmentorsArray = array();

		    	foreach ($assigned_members as $value) {
		    
		    		$condionArr['User.id'] = $value['ProjectMember']['member_id'];
		    		$assignedmentorsArray[$value['ProjectMember']['member_id']] = $this->User->find('first',
		    				array(
		    						'contain'=>array(
		    								'UserReference'=>array(
		    										'fields'=>array('first_name','last_name')
		    								),
		    								'UserImage'
		    						),
		    							
		    						'conditions'=>$condionArr
		    				)
		    					
		    		);
		    
		    	}

                     $this->set('assignedmentorsArray', $assignedmentorsArray);

		    	$mentorsArray = array();
		    	 
		    	foreach ($ProjectMembers as $value) {
		    
		    		$condionArr['User.id'] = $value['ProjectMemberConfirm']['member_id'];
		    		$mentorsArray[$value['ProjectMemberConfirm']['member_id']] = $this->User->find('first',
		    				array(
		    						'contain'=>array(
		    								'UserReference'=>array(
		    										'fields'=>array('first_name','last_name','client_linkedinurl')
		    								),
		    								'UserImage'
		    						),
		    							
		    						'conditions'=>$condionArr
		    				)
		    					
		    		);
		    
		    	}
		    		 
		    	$this->data = $project;
		    	 
		    	$this->set('mentorsArray', $mentorsArray);


                     
                     $count = 0; 
                     foreach($ProjectMembers as $member){

                     if($member['ProjectMemberConfirm']['member_id'] ==  $this->Session->read('Auth.User.id'))
                     $count++;
                     
                     }
		    	$this->set('count', $count);		    	
		    	$createdBy = $this->User->find('first',
		    			array(
		    					'contain'=>array(
		    							'UserReference'=>array(
		    									'fields'=>array('first_name','last_name')
		    							),
		    							'UserImage'
		    					),
		    					'conditions'=>array('User.id'=>$project['Project']['user_id'])
		    			)
		    	
		    	);
		    	
		    	$this->set('createdBy', $createdBy);

                     $visitor_project = "Project id : ".$project['Project']['id'];
	    	     $visitor_name = $this->Prospect->find('first', array('conditions' => array('Prospect.answer' => $visitor_project)));

                     $invoice = $this->ProjectInvoice->find('all', array('conditions' => array('ProjectInvoice.projectid' => $project['Project']['id']),'order'=>'ProjectInvoice.id DESC'));
                     $invoicecount = $this->ProjectInvoice->find('count', array('conditions' => array('ProjectInvoice.projectid' => $project['Project']['id'])));


                     if($invoicecount != ''){
                     $this->set('invoicedata', $invoice);
                     }
                     $this->set('visitor_name', $visitor_name);
                     $project_title = $project['Project']['title'];
                     $this->set('invoice_title',$project_title);
                     $this->Session->write('invoice_title',$project_title);
                     $fromproject = "true";
                     $this->set('fromproject',$fromproject);
                     $this->Session->write('fromproject',$fromproject);
                  
                     }else{
                             $this->redirect(array('controller'=>'fronts','action'=>'index'));
                     
                    }
                      
                 
		    }	



      
    }
    


 

       function project_details($projectUrl = null){


                 $this->layout= 'defaultnew';
    	

	    	if($projectUrl != null) {


	    		$project = $this->Project->find('first', array('conditions' => array('Project.project_url' => $projectUrl)));
	    		



		    		 
		    	$this->data = $project;
		    	 



                     

		    			    	
		    	$createdBy = $this->User->find('first',
		    			array(
		    					'contain'=>array(
		    							'UserReference'=>array(
		    									'fields'=>array('first_name','last_name')
		    							),
		    							'UserImage'
		    					),
		    					'conditions'=>array('User.id'=>$project['Project']['user_id'])
		    			)
		    	
		    	);
		    	
		    	$this->set('createdBy', $createdBy);
                     $visitor_project = "Project id : ".$project['Project']['id'];
	    	     $visitor_name = $this->Prospect->find('first', array('conditions' => array('Prospect.answer' => $visitor_project)));

                     $this->set('visitor_name', $visitor_name);

                 
		    }	


    }  


 

    	function consultation_request_ajax() {
	
		if ($this->RequestHandler->isAjax()) {
			Configure::write('debug', 0);
			 

                    $confirm_id = trim($this->request->data['confirm_id']);

			
	    		  $project = $this->ProjectMemberConfirm->find('first', array('conditions' => array('ProjectMemberConfirm.id' =>$confirm_id)));
                       $mentor = $project['ProjectMemberConfirm']['member_id'];
                       $this->ProjectMemberConfirm->updateAll(array('status'=>1), array('ProjectMemberConfirm.id'=> $confirm_id));
	              $this->layout = '';
	    		$this->render(false);		    		 
	
		       //echo json_encode($mentor);
                      echo json_encode($mentor);
	    		exit();
         }
	}

    	function proposal_accept_ajax() {
	
		if ($this->RequestHandler->isAjax()) {
			Configure::write('debug', 0);
			 

                    $confirm_id = trim($this->request->data['confirm_id']);

			
	    		  $project = $this->ProjectMemberConfirm->find('first', array('conditions' => array('ProjectMemberConfirm.id' =>$confirm_id)));
                          $allproposals = $this->ProjectMemberConfirm->find('all', array('conditions' => array('ProjectMemberConfirm.project_id' =>$project['ProjectMemberConfirm']['project_id'])));
                       
                         $status = "7";
                         $this->Project->updateAll(array('Project.status'=>"'".$status."'"),array('Project.id'=>$project['ProjectMemberConfirm']['project_id']));

                      foreach($allproposals as $proposals){
                        $this->ProjectMemberConfirm->updateAll(array('status'=>-1), array('ProjectMemberConfirm.id'=> $proposals['ProjectMemberConfirm']['id']));
                        }
                        

                       $mentor = $project['ProjectMemberConfirm']['member_id'];
                       $this->ProjectMemberConfirm->updateAll(array('status'=>1), array('ProjectMemberConfirm.id'=> $confirm_id));
                       $user = $this->User->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $user['User']['username'];
                           $subject = $user['UserReference']['first_name'].", your project is ready to start";
                           $link = SITE_URL.'project/index/'.$project['Project']['project_url'];
                           $projectlink = SITE_URL.'project/index/'.$project['Project']['project_url'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("project_status_change");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           $data .= "&merge_clientemail=".urlencode($user['User']['username']);
                           $data .= "&merge_projecttitle=".urlencode($project['Project']['title']);
                           $data .= "&merge_projectlink=".urlencode($projectlink);

                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }

                           //EasticEmail Integration - End

	              $this->layout = '';
	    		$this->render(false);		    		 
	
		       //echo json_encode($mentor);
                      echo json_encode($mentor);
	    		exit();
         }
	}



    	function proposal_ignore_ajax() {
	
		if ($this->RequestHandler->isAjax()) {
			Configure::write('debug', 0);
			 

                    $confirm_id = trim($this->request->data['confirm_id']);

			
	    		  $project = $this->ProjectMemberConfirm->find('first', array('conditions' => array('ProjectMemberConfirm.id' =>$confirm_id)));
                       
                        
                       $mentor = $project['ProjectMemberConfirm']['member_id'];
                       $this->ProjectMemberConfirm->updateAll(array('status'=>-1), array('ProjectMemberConfirm.id'=> $confirm_id));
                       $user = $this->User->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));



	              $this->layout = '';
	    		$this->render(false);		    		 
	
		       //echo json_encode($mentor);
                      echo json_encode($mentor);
	    		exit();
         }
	}



    	function project_start() {
	
		if ($this->RequestHandler->isAjax()) {
			Configure::write('debug', 0);
			 

                    $confirm_id = trim($this->request->data['confirm_id']);


                       
                         $status = "8";

                         $this->Project->updateAll(array('Project.status'=>"'".$status."'"),array('Project.id'=>$confirm_id));

                         $project = $this->Project->find('first', array('conditions' => array('Project.id' => $confirm_id)));
                         $user = $this->User->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $user['User']['username'];
                           $subject = "Congratulations ".$user['UserReference']['first_name'].", your project has started";
                           $link = SITE_URL.'project/index/'.$project['Project']['project_url'];
                           $projectlink = SITE_URL.'project/index/'.$project['Project']['project_url'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("project_status_change");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           $data .= "&merge_clientemail=".urlencode($user['User']['username']);
                           $data .= "&merge_projecttitle=".urlencode($project['Project']['title']);
                           $data .= "&merge_projectlink=".urlencode($projectlink);

                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }

                           //EasticEmail Integration - End

	              $this->layout = '';
                        echo json_encode($link);
	    		$this->render(false);
		    		 

	    		exit();
         }
	}


    	function project_close() {
	
		if ($this->RequestHandler->isAjax()) {
			Configure::write('debug', 0);
			 

                    $confirm_id = trim($this->request->data['confirm_id']);


                       
                         $status = "9";

                         $this->Project->updateAll(array('Project.status'=>"'".$status."'"),array('Project.id'=>$confirm_id));

                         $project = $this->Project->find('first', array('conditions' => array('Project.id' => $confirm_id)));
                         $user = $this->User->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $user['User']['username'];
                           $subject = $user['UserReference']['first_name'].", your project has been completed";
                           $link = SITE_URL.'project/index/'.$project['Project']['project_url'];
                           $projectlink = SITE_URL.'project/index/'.$project['Project']['project_url'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("project_status_change");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           $data .= "&merge_clientemail=".urlencode($user['User']['username']);
                           $data .= "&merge_projecttitle=".urlencode($project['Project']['title']);
                           $data .= "&merge_projectlink=".urlencode($projectlink);

                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }

                           //EasticEmail Integration - End

	              $this->layout = '';
                        echo json_encode($link);
	    		$this->render(false);
		    		 

	    		exit();
         }
	}




    function consultation_request($mentee_id = null, $mentor_id = null, $projectId = null){

      
        $project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectId)));


    	$mentorData = $this->Mentorship->find('count',array('conditions'=>array('Mentorship.is_deleted'=>0,'Mentorship.mentor_id'=>$mentor_id,'Mentorship.mentee_id'=>$mentee_id)));
    	if(isset($mentorData) && ($mentorData)>0)
    	{
        	$this->Session->setFlash(__('You already have an open consultation with this member.', true), 'default', array('class' => 'success'));
       	$this->redirect(array('action'=>'index',$project['Project']['project_url']));

    	}
    	if(!$mentor_id)
    	{
    		return;
    		//$this->redirect(array('action'=>'index'));
    	}

	
    	$menteeNeed = "Project: ".SITE_URL."project/project_consultation/".$project['Project']['project_url'];
    
    	$mentorship = array('mentee_id'=>$mentee_id,'mentor_id'=>$mentor_id,'mentee_need'=>$menteeNeed);
    	$this->Mentorship->save($mentorship);
    	$id = $this->Mentorship->getLastInsertID();
    
    	unset($this->Mentorship->id);



    	//send email to mentee
    	$menteeData = $this->User->find('first',array('conditions'=>array('User.id'=>$mentee_id)));
    	$mentorData = $this->User->find('first',array('conditions'=>array('User.id'=>$mentor_id)));
    	$this->set(compact('menteeData','mentorData'));
    	$this->set('mentorship_id',$id);
    

    
			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $menteeData['User']['username'];
                           $subject = "Your request for an initial consultation";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_application_request_sent");
                           $data .= "&merge_consultantfname=".urlencode($mentorData['UserReference']['first_name']);
                           $data .= "&merge_consultantlname=".urlencode($mentorData['UserReference']['last_name']);
                           $data .= "&merge_clientfname=".urlencode($menteeData['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

    
    	//send email to mentor
	if($menteeData != ''){    



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $mentorData['User']['username'];
                           $subject = "Consultation request received";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentors_application_request");
                           $data .= "&merge_consultantfname=".urlencode($mentorData['UserReference']['first_name']);
                           $data .= "&merge_clientfname=".urlencode($menteeData['UserReference']['first_name']);
                           $data .= "&merge_clientlname=".urlencode($menteeData['UserReference']['last_name']);
                           $data .= "&merge_consultantemail=".urlencode($mentorData['User']['username']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


                          }
                         else{



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "iqbal@guild.im";
                           $subject = "Consultation request received without client Deatils";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentors_application_request");
                           $data .= "&merge_consultantfname=".urlencode($mentorData['UserReference']['first_name']);
                           $data .= "&merge_clientfname=".urlencode($menteeData['UserReference']['first_name']);
                           $data .= "&merge_clientlname=".urlencode($menteeData['UserReference']['last_name']);
                           $data .= "&merge_consultantemail=".urlencode($mentorData['User']['username']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


                           }

        	$this->Session->setFlash(__('You have requested a free initial consultation.', true), 'default', array('class' => 'success'));
       	$this->redirect(array('action'=>'index',$project['Project']['project_url']));

    }
    
    function projectlistclient(){


       $this->layout= 'defaultnew';
    	
    	if($this->Auth->User('id') != '' && $this->Auth->User('role_id') == 3) {
	    	$mentee_id = $this->Session->read('Auth.User.id');
	    	$role_id = $this->Session->read('Auth.User.role_id');
	    	if($mentee_id=='' || $role_id!='3')
	    	{
	    		$this->redirect(array('controller'=>'fronts','action'=>'index'));
	    	}
	    	// echo $type; die;
	    	$this->Paginator->settings = array('conditions'=>array('Project.user_id'=>$mentee_id),'order'=>'Project.id DESC','limit'=>'5');
	    	
	    	$data = $this->Paginator->paginate('Project');
	    	$this->set(compact('data'));
    	} else {
    		$this->redirect(array('controller'=>'fronts','action'=>'index'));
    	}
    }
      function projectlistconsultant(){

       $this->layout= 'defaultnew';
    	
    	if($this->Auth->User('id') != '' && $this->Auth->User('role_id') == 2) {
	    	$mentee_id = $this->Session->read('Auth.User.id');
	    	$role_id = $this->Session->read('Auth.User.role_id');
	    	if($mentee_id=='' || $role_id!='2')
	    	{
	    		$this->redirect(array('controller'=>'fronts','action'=>'index'));
	    	}
	    	// echo $type; die;
	    	$this->Paginator->settings = array('conditions'=>array('Project.user_id'=>$mentee_id),'order'=>'Project.id DESC');
	    	
	    	$data = $this->Paginator->paginate('Project');
	    	$this->set(compact('data'));
    	} else {
    		$this->redirect(array('controller'=>'fronts','action'=>'index'));
    	}
    }
  
    function projectlistmember(){

         $this->layout= 'defaultnew';
    	
    	if($this->Auth->User('id') != '' && $this->Auth->User('role_id') == 2) {
	    	
    		$mentor_id = $this->Session->read('Auth.User.id');
	    	$role_id = $this->Session->read('Auth.User.role_id');
              
              $shown_interest = $this->ProjectMemberConfirm->find('all', array('conditions' => array('AND' => array(array('ProjectMemberConfirm.member_id' => $mentor_id)))));

                     $project = array();
  			foreach($shown_interest as $key=>$value)
			{

                     $project[$key] = $this->Project->find('first', array('conditions' => array('Project.id' => $value['ProjectMemberConfirm']['project_id'])));			           
                     }
                  
                  
              
	    	  $subcontract = $this->Project->find('all',array('conditions'=>array('Project.user_id'=>$mentor_id)));

                 $all_project = array_merge($subcontract,$project);

                  

				$data = Set::sort($all_project, '{n}.Project.id', 'DESC');
				$this->set('data', $data);
	    	
    	} else {
    		$this->redirect(array('controller'=>'fronts','action'=>'index'));
    	}
    }
    

    

    
    
    function admin_review(){
      	if($this->Auth->user('id') == 1){  
		$this->layout = 'admin';  
    	$projects = $this->Project->find('all', array('order'=>array('Project.id'=>'DESC')));
    	
    	$this->set('projects', $projects);
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }
    
    function admin_deleteproject($id = null){
    	if($this->Auth->user('id') == 1){    
		$this->layout = 'admin';  
    	if($this->Project->delete($id)) {
    
    		$ProjectMembers = $this->ProjectMember->find('all', array('conditions' => array('ProjectMember.project_id' => $id)));
    		 
    		foreach ($ProjectMembers as $members) {
    		
    			$this->ProjectMember->delete($members['ProjectMember']['id']);
    		}
    		
    		$this->Session->setFlash('Project has been deleted', 'admin_flash_good');
    		$this->redirect(array('action' => 'admin_review'));
    	} else {
    		$this->Session->setFlash('Project could not be deleted', 'admin_flash_bad');
    	}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }
   
	function projectAuth() {
	
		if(isset($_REQUEST['oid']) && $_REQUEST['oid']!=''&& $_REQUEST['email']!='')
		{
			/* Read linkedin data from text file  */
			$current = file_get_contents(SITE_URL.$_REQUEST['oid'].".txt");
			$linkData = explode("<br/>",$current);
			unlink($_REQUEST['oid'].".txt");
			$this->__subscribenewsletter($linkData[4]);
	
			if($linkData[4] == '' || $linkData[1] == '') {
	
				$linkData[0] = $_REQUEST['oid'];
				$linkData[4] = $_REQUEST['email'];
			}
	
			$existEmail = $this->User->find('first',array('conditions'=>array('User.username'=>$linkData[4])));
			if(empty($existEmail)) //Email Id was not found in DB -- Register as a client
			{
			    
                         $project = $this->Project->find('first',array('conditions'=>array('Project.activation_key'=>$this->Session->read('Project.Submit.Visitor'))));

 

                         $id = $this->__registerUsingLinkedin($_REQUEST['oid'], $linkData);
                         $project['Project']['user_id'] = $id;
			    $project['Project']['activation_key'] = '-1';
                         $this->Project->save($project);

                         $this->redirect(array('controller'=>'clients','action'=>'edit_account'));			
                          }
                   
			else //Email Id found in DB -- Login
			{
				if($existEmail['User']['role_id']==3 || $existEmail['User']['role_id']==2)
				{
					$id = $existEmail['User']['id'];

                                  $project = $this->Project->find('first',array('conditions'=>array('Project.activation_key'=>$this->Session->read('Project.Submit.Visitor'))));                          
                                  $activationKey = $project['Project']['activation_key'];
	
					$this->__loginUsingLinkedin($_REQUEST['oid'], $existEmail, $linkData, $activationKey);


				}
			}
	


					

			$project['Project']['user_id'] = $id;

			$this->Project->save($project);					

	              

			//$this->redirect(array('action' => 'index', $project['Project']['id']));
		}
		else
		{
			$this->Session->write('RedirectTo','Front');
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("The email address does not match. Please contact <a href='mailto:help@guild.im' class='delete'>help@guild.im</a>.", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
			die;
		}
	} 
    

	function projectlinkedinAuth($oid, $email) {
	
		if(isset($oid) && $oid!=''&&  $email!='')
		{
			/* Read linkedin data from text file  */
			$current = file_get_contents(SITE_URL.$oid.".txt");
			$linkData = explode("<br/>",$current);
			unlink($oid.".txt");
			$this->__subscribenewsletter($linkData[4]);
	
			if($linkData[4] == '' || $linkData[1] == '') {
	
				$linkData[0] = $_REQUEST['oid'];
				$linkData[4] = $_REQUEST['email'];
			}
	
			$existEmail = $this->User->find('first',array('conditions'=>array('User.username'=>$linkData[4])));
			if(empty($existEmail)) //Email Id was not found in DB -- Register as a client
			{
				//$projectId = $linkData[9];
				$projectId = $this->Session->read('projectId');
				
				$project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectId)));
                              if($project['Project']['status'] == 0){
    		              $project['Project']['status'] = 1; // Project saved with number
				}
                            $id = $this->__registerUsingLinkedin($oid, $linkData);
                 
                            $project['Project']['user_id'] = $id;
                            $project['Project']['url'] = $linkData[4];
			    $project['Project']['activation_key'] = '-1';
                            $this->Project->save($project);
                            
                                if($project['Project']['status'] == 0){   

				$this->redirect(array('controller'=>'project','action'=>'view',$project['Project']['id']));

                               }else{
                                     
                                  $this->redirect(array('controller'=>'project','action'=>'index',$project['Project']['project_url']));

                                }
                              

                 }
			else //Email Id found in DB -- Login
			{
				if($existEmail['User']['role_id'] == 3 || $existEmail['User']['role_id']==2)
				{
					$projectId = $this->Session->read('projectId');
					$id = $existEmail['User']['id'];

					$project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectId)));
    		    	              $project['Project']['status'] = 1; // Project saved with number
                	              $project['Project']['user_id'] = $id;
                                   $project['Project']['url'] = $existEmail['User']['username'];
			    	       $project['Project']['activation_key'] = '-1';
                	              $this->Project->save($project);

					$this->__loginUsingLink($oid, $existEmail, $linkData, $project['Project']['project_url']);
					
					

				}
			}
	


					$this->Session->delete('projectId');

			              $project['Project']['user_id'] = $id;

			              $this->Project->save($project);					

	              

			//$this->redirect(array('action' => 'index', $project['Project']['id']));
		}
		else
		{
			$this->Session->write('RedirectTo','Front');
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("The email address does not match. Please contact <a href='mailto:help@guild.im' class='delete'>help@guild.im</a>.", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
			die;
		}
	}






	function __subscribenewsletter($email=null)
	{
		if($email!="")
		{
			$newsletterData = $this->NewsLetter->find('first',array('conditions'=>array('NewsLetter.email'=>$email)));
			if(empty($newsletterData))
			{
				$newsData['NewsLetter']['email'] = $email;
				$newsData['NewsLetter']['status'] = '1';
				$this->NewsLetter->save($newsData);
			}
		}
	}
	

	function __registerUsingLinkedin($socialId, $linkData){
	
		$saveData['User']['status'] = '1';
		$saveData['User']['activation_key'] = substr(md5(uniqid()), 0, 20);
	
		$this->User->create();
		$saveData['User']['access_specifier'] = 'publish';
		$saveData['User']['is_approved'] = '1';
		$saveData['User']['role_id'] = 3;
		$saveData['User']['username'] = $linkData[4];
		$saveData['User']['socialId'] = $socialId;
		$password = $this->General->randamPassword();
		$saveData['User']['password'] = $password;//Security::hash($password, null, true);
                $saveData['User']['password2'] = Security::hash($password, null, true);
		
		$saveData['User']['plan_type'] = 'Basic';
		$saveData['User']['isPlanAdmin'] = 0;
		
		if($this->User->save($saveData,false))
		{
			$id = $this->User->id;
	
			if($linkData[5]!='' || $linkData[7]!='')
			{
				$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
				if(empty($socialLink))
				{
					$socialData['Social']['user_id'] = $id;
					$socialData['Social']['created_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				else
				{
					$socialData['Social']['id'] = $socialLink['Social']['id'];
					$socialData['Social']['modified_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				$this->Social->save($socialData);
				$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
				if($linkData[7]!='')
				{
					$twittLink = explode(',',$linkData[7]);
					for($i=0;$i<count($twittLink);$i++)
					{
					$this->Social->create();
					$twitData['Social']['user_id'] = $id;

					$twitData['Social']['created_by'] = $id;
					$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
					$this->Social->save($twitData);
					}
					}
			}
	
					$this->UserReference->create();
					$refData['UserReference']['first_name'] = $linkData[1];
					$refData['UserReference']['last_name'] = $linkData[2];
					$refData['UserReference']['user_id'] = $id;
					$refData['UserReference']['background_summary']  =  $linkData[6];
                                        $refData['UserReference']['client_linkedinurl']  =  $linkData[5];
                                        $refData['UserReference']['headline']  =  $linkData[8];
                                        $refData['UserReference']['client_companyname']  =  $linkData[9];
                                        $refData['UserReference']['client_companydesignation']  =  $linkData[10];
                                        $refData['UserReference']['client_companycontactname']  =  $linkData[1]." ".$linkData[2];
                                        $refData['UserReference']['client_companyaddress']  =  $linkData[4];
				
				$this->UserReference->save($refData);
				
				if(isset($linkData[3]) && $linkData[3]!='')
				{
				$db_imgname = "user_".time().".jpeg";
				$file_name = IMAGES . MENTEES_IMAGE_PATH . DS . $id;
	
					$destination = IMAGES . MENTEES_IMAGE_PATH . DS . $id;
					$imagePath = $linkData[3];
					$des1 = $file_name.DS.'uploaded';
					$des2 = $file_name.DS.'small/';
					if (!is_dir($destination)) {
					mkdir($destination, 0777);
						mkdir($destination.DS.'small', 0777);
						mkdir($destination.DS.'uploaded', 0777);
					}
					if(!copy($imagePath,$file_name.DS.'uploaded'.DS.$db_imgname))
					{
							$linkData[3]="";
					}
						$cropdata = array();
						$cropdata['x'] = 0; $cropdata['y'] = 0; $cropdata['w'] = 40; $cropdata['h'] = 40; $cropdata['name'] = $db_imgname;
						$rr = $this->imageCrop($des1.DS,$cropdata,$des2,40,40);
						$rr = $this->imageCrop($des1.DS,$cropdata,$destination,200,200);
						$image_data['UserImage']['user_id'] = $id;
								$image_data['UserImage']['image_name'] = $db_imgname;
										$this->UserImage->save($image_data);
					}
					
					$this->User->createUrlKey($id);
					$user = $this->User->findById($id);
					
					if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
						$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
					else
					$this->Session->write('User.image','');
	
			$this->Session->write('Auth',$user);
	

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $linkData[4];
                           $subject = "Welcome to GUILD";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_register_linkedin");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_useremail=".urlencode($linkData[4]);
                           $data .= "&merge_userpassword=".urlencode($password);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


									
		return $id;
	}
	}
 
	function __loginUsingLinkedin($socialId, $existEmail, $linkData, $activationKey){
	
		$saveData['User']['status'] = '1';
		$saveData['User']['activation_key'] = substr(md5(uniqid()), 0, 20);
	
		$id = $existEmail['User']['id'];
		$saveData['User']['id'] = $existEmail['User']['id'];
		$saveData['User']['socialId'] = $socialId;

              $project = $this->Project->find('first',array('conditions'=>array('Project.activation_key'=>$activationKey)));  
              $project['Project']['activation_key'] = '-1';
	       $project['Project']['user_id'] = $id;

	       $this->Project->save($project);
		
		if($this->User->save($saveData,false))
		{
			if($linkData[5]!='' || $linkData[7]!='')
			{
				$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
				if(empty($socialLink))
				{
					$socialData['Social']['user_id'] = $id;
					$socialData['Social']['created_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				else
				{
					$socialData['Social']['id'] = $socialLink['Social']['id'];
					$socialData['Social']['modified_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				$this->Social->save($socialData);
		
				if($linkData[7]!='')
				{
					$twittLink = explode(',',$linkData[7]);
					$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
					for($i=0;$i<count($twittLink);$i++)
					{
						$this->Social->create();
						$twitData['Social']['user_id'] = $id;
						$twitData['Social']['created_by'] = $id;
						$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
						$this->Social->save($twitData);
					}
				}
			}
	              
			$user = $this->User->findById($id);

			if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
				$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
			else
				$this->Session->write('User.image','');

				$this->Session->write('Auth.User',$user['User']);
                            $this->redirect(array('controller'=>'clients','action'=>'my_account'));
			}
		}

             function __loginUsingLink($socialId, $existEmail, $linkData, $pageUrl){
	
		$saveData['User']['status'] = '1';
		$saveData['User']['activation_key'] = substr(md5(uniqid()), 0, 20);
	
		$id = $existEmail['User']['id'];
		$saveData['User']['id'] = $existEmail['User']['id'];
		$saveData['User']['socialId'] = $socialId;

              $project = $this->Project->find('first',array('conditions'=>array('Project.project_url'=>$pageUrl)));
		if($this->User->save($saveData,false))
		{
			if($linkData[5]!='' || $linkData[7]!='')
			{
				$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
				if(empty($socialLink))
				{
					$socialData['Social']['user_id'] = $id;
					$socialData['Social']['created_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				else
				{
					$socialData['Social']['id'] = $socialLink['Social']['id'];
					$socialData['Social']['modified_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				$this->Social->save($socialData);
		
				if($linkData[7]!='')
				{
					$twittLink = explode(',',$linkData[7]);
					$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
					for($i=0;$i<count($twittLink);$i++)
					{
						$this->Social->create();
						$twitData['Social']['user_id'] = $id;
						$twitData['Social']['created_by'] = $id;
						$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
						$this->Social->save($twitData);
					}
				}
			}
	              
			$user = $this->User->findById($id);


			if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
				$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
			else
				$this->Session->write('User.image','');

				$this->Session->write('Auth.User',$user['User']);


                            $this->redirect(array('controller'=>'project','action'=>'view',$project['Project']['id']));
			}
		}


		function imageCrop($src,$cropdata,$dest,$targ_w =300,$targ_h=250)
		{
			$Imgpath = $src;
			$srcPath = str_replace("\\","/",$Imgpath.$cropdata['name']);
			$dest = str_replace("\\","/",$dest);
			$dest = $dest.DS.$cropdata['name'];
			$jpeg_quality = 100;
			$img_r = imagecreatefromjpeg($srcPath);
			$maxW = $targ_w;
			$maxH = $targ_h;
			$uploadSize = getimagesize($srcPath);
			$uploadWidth = $uploadSize[0];
			$uploadHeight = $uploadSize[1];
			$uploadType = $uploadSize[2];
		
			if ($uploadType != 1 && $uploadType != 2 && $uploadType != 3) {
				$this->error("File type must be GIF, PNG, or JPG to resize");
			}
		
			switch ($uploadType) {
				case 1: $srcImg = imagecreatefromgif($srcPath);
				break;
				case 2: $srcImg = imagecreatefromjpeg($srcPath);
				break;
				case 3: $srcImg = imagecreatefrompng($srcPath);
				break;
				default: $this->error("File type must be GIF, PNG, or JPG to resize");
			}
			/**$ratioX = $maxW / $uploadWidth;
			$ratioY = $maxH / $uploadHeight;
		
			#figure out new dimensions
			if (($uploadWidth == $maxW) && ($uploadHeight == $maxH)) {
				$newX = $uploadWidth;
				$newY = $uploadHeight;
			} else if (($ratioX * $uploadHeight) > $maxH) {
				$newX = $maxW;
				$newY = ceil($ratioX * $uploadHeight);
			} else {
				$newX = ceil($ratioY * $uploadWidth);
				$newY = $maxH;
			}
			//imagefilter($srcImg, IMG_FILTER_GRAYSCALE);
			$dstImg = imagecreatetruecolor($newX, $newY);
			imagecopyresampled($dstImg, $srcImg, 0, 0, 0, 0, $newX, $newY, $uploadWidth, $uploadHeight);**/


                $ratioX = $maxW / $uploadWidth;
                $ratioY = $maxH / $uploadHeight;

                if ($ratioX < $ratioY) {
                    $newX = round(($uploadWidth - ($maxW / $ratioY)) / 2);
                    $newY = 0;
                    $uploadWidth = round($maxW / $ratioY);
                    $uploadHeight = $uploadHeight;
                } else {
                    $newX = 0;
                    $newY = round(($uploadHeight - ($maxH / $ratioX)) / 2);
                    $uploadWidth = $uploadWidth;
                    $uploadHeight = round($maxH / $ratioX);
                }
		//imagefilter($srcImg, IMG_FILTER_GRAYSCALE);
                $dstImg = imagecreatetruecolor($maxW, $maxH);
                $white = imagecolorallocate($dstImg, 255, 255, 255);
                imagefill($dstImg, 0, 0, $white);
                imagecolortransparent($dstImg,$white);
                imagecopyresampled($dstImg, $srcImg, 0, 0, $newX, $newY, $maxW, $maxH, $uploadWidth, $uploadHeight);

			$write =imagejpeg($dstImg,$dest,$jpeg_quality);
			return $write;
		}

             function home(){

                                 $this->layout= 'defaultnew';
	                         $this->Session->write('popup', 1);   
                   		    $this->set("title_for_layout","Project");

                  }

           function proposal($proposalUrl = null){
                     
          $this->layout= 'defaultnew';
	  $fromproject = $this->Session->read('fromproject');
          $this->Session->delete('fromproject');
          $this->set("fromproject",$fromproject);


			//GetProposal topics

			$result = $this->Topic->find('all');
			$qcategories = array();
			foreach($result as $value){
				$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
			}
			$this->set("categories",$qcategories);


                     $proposal = $this->Proposal->find('first', array('conditions' => array('Proposal.proposal_url' => $proposalUrl)));
                if($proposal['Proposal']['id'] != ''){
                        $proposal['Proposal']['proposal_title'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_title']);
                        $proposal['Proposal']['proposal_details'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_details']);
                        $proposal['Proposal']['proposal_approach'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_approach']);
                        $proposal['Proposal']['proposal_success'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_success']);                    
                     $user = $this->User->findById($proposal['Proposal']['user_id']);
                     $this->set('proposal',$proposal);
                     $this->set('user',$user);

                     if($this->Auth->user('id') == $proposal['Proposal']['user_id'] || $this->Auth->user('role_id') ==1){
                     $edit ="yes";
                     $this->set('edit',$edit);
                     }

		$this->loadModel('City');
		$dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($user['UserReference']['zipcode']))));
		$this->set("city",$dataZip['City']['city_name']);
		$this->set("state",$dataZip['City']['state']);


                     $this->set("title_for_layout",$proposal['Proposal']['proposal_title']);

    	} else {
    		$this->redirect(array('controller'=>'project','action'=>'project_proposals'));
    	}
          }


        function proposal_create($id = null){

          if($this->Auth->user('id') != "" && $this->Auth->user('role_id') == 2 && $this->Auth->user('access_specifier') =='publish'){

          $this->layout= 'defaultnew';
          $this->set("title_for_layout","Project proposal create");
	  $this->Session->write('popup', 1); 
	    	if($id != null){
	    	
	    		$proposal = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id)));
                        $proposal['Proposal']['proposal_title'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_title']);
                        $proposal['Proposal']['proposal_details'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_details']);
                        $proposal['Proposal']['proposal_approach'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_approach']);
                        $proposal['Proposal']['proposal_success'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_success']);
    		        $this->data = $proposal;

                        $titlelength = strlen($proposal['Proposal']['proposal_title']);
                        $detailslength = strlen($proposal['Proposal']['proposal_details']);  
                        $approachlength = strlen($proposal['Proposal']['proposal_approach']);
                        $successlength = strlen($proposal['Proposal']['proposal_success']);

                        $title =  $proposal['Proposal']['proposal_title'];
                        $details =  $proposal['Proposal']['proposal_details'];
                        $approach =  $proposal['Proposal']['proposal_approach'];
                        $success =  $proposal['Proposal']['proposal_success'];

                        if($titlelength > 128)
                        {
                        $title = substr($proposal['Proposal']['proposal_title'], 0, $titlelength); 
                        }
                        if($detailslength > 1000)
                        {
                        $details = substr($proposal['Proposal']['proposal_details'], 0, $detailslength); 
                        }
                        if($approachlength > 1000)
                        {
                        $approach = substr($proposal['Proposal']['proposal_approach'], 0, $approachlength); 
                        }
                        if($successlength > 500)
                        {
                        $success = substr($proposal['Proposal']['proposal_success'], 0, $successlength); 
                        } 

                        $this->set("title",$title);
                        $this->set("details",$details);
                        $this->set("approach",$approach);
                        $this->set("success",$success);
                 }


 	
              else if(!(empty($this->data))){
             


              $this->Proposal->create();             
              $proposalarray['Proposal']['user_id'] = $this->Auth->user('id');

    	      $proposalarray['Proposal']['proposal_title'] = $this->data['Proposal']['proposal_title'];
    	      $proposalarray['Proposal']['proposal_details'] = $this->data['Proposal']['proposal_details'];
   	      $proposalarray['Proposal']['proposal_approach'] = $this->data['Proposal']['proposal_approach'];
    	      $proposalarray['Proposal']['proposal_success'] = $this->data['Proposal']['proposal_success'];


              
              $this->Proposal->save($proposalarray,false);
              $id = $this->Proposal->getLastInsertId();

             $this->redirect(array('action' => 'proposal_preview', $id));
    	}

    	} else {
    		$this->redirect(array('controller'=>'project','action'=>'project_proposals'));
    	} 


      }


        function projectproposal($id = null){

        if($this->Auth->user('id') != "" && $this->Auth->user('role_id') == 2 && $this->Auth->user('access_specifier') =='publish'){

           $this->layout= 'defaultnew';

          $this->set("title_for_layout","Project proposal create");
	  $this->Session->write('popup', 1); 


			//GetProposal topics
			$result = $this->ProposalTopic->find('all');
			$ptopics = array();
			foreach($result as $value){
				$ptopics[$value['ProposalTopic']['id']] = $value['ProposalTopic']['proposal_topic'];
			}
			$this->set("ptopics",$ptopics);


             $projectid = $id;

	    	if($id != null){
	    	
    		$project = $this->Project->find('first', array('conditions' => array('Project.id' => $id)));
    		
    		$this->data = $project;


                        $this->set("title",$project['Project']['title']);
                        $this->set("projectid",$project['Project']['id']);

                 }


 	
              else if(!(empty($this->data))){
             


              $this->Proposal->create();             
              $proposalarray['Proposal']['user_id'] = $this->Auth->user('id');

    	      $proposalarray['Proposal']['proposal_title'] = $this->data['Proposal']['proposal_title'];
    	      $proposalarray['Proposal']['proposal_details'] = $this->data['Proposal']['proposal_details'];
   	      $proposalarray['Proposal']['proposal_approach'] = $this->data['Proposal']['proposal_approach'];
    	      $proposalarray['Proposal']['proposal_success'] = $this->data['Proposal']['proposal_success'];
    	      $proposalarray['Proposal']['projectid'] = $this->data['Proposal']['projectid'];


              
              $this->Proposal->save($proposalarray,false);
              $id = $this->Proposal->getLastInsertId();
          
                  $project = $this->ProjectMemberConfirm->find('first', array('conditions' => array('ProjectMemberConfirm.project_id' => $this->data['Proposal']['projectid'])));
                  if($this->data['Proposal']['projectid'] !='' && $project['ProjectMemberConfirm']['member_id'] !=  $this->Auth->user('id')){
                  $project['ProjectMemberConfirm']['project_id'] = $this->data['Proposal']['projectid'];
                  $project['ProjectMemberConfirm']['member_id'] =  $this->Auth->user('id');
                  $this->ProjectMemberConfirm->save($project);

                  } 

             $this->redirect(array('action' => 'proposal_preview', $id));
    	}

    	} else {
    		$this->redirect(array('controller'=>'project','action'=>'project_proposals'));
    	} 

      }



        function proposal_edit($id = null){

        if($this->Auth->user('id') != "" && $this->Auth->user('role_id') == 2 && $this->Auth->user('access_specifier') =='publish'){

          $this->layout= 'defaultnew';
          $this->set("title_for_layout","Project proposal create");
	  $this->Session->write('popup', 1); 
	    	if($id != null){
	    	
	    		$proposal = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id)));
                        $proposal['Proposal']['proposal_title'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_title']);
                        $proposal['Proposal']['proposal_details'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_details']);
                        $proposal['Proposal']['proposal_approach'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_approach']);
                        $proposal['Proposal']['proposal_success'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_success']);
    		        $this->data = $proposal;

                        $titlelength = strlen($proposal['Proposal']['proposal_title']);
                        $detailslength = strlen($proposal['Proposal']['proposal_details']);  
                        $approachlength = strlen($proposal['Proposal']['proposal_approach']);
                        $successlength = strlen($proposal['Proposal']['proposal_success']);

                        $title =  $proposal['Proposal']['proposal_title'];
                        $details =  $proposal['Proposal']['proposal_details'];
                        $approach =  $proposal['Proposal']['proposal_approach'];
                        $success =  $proposal['Proposal']['proposal_success'];

                        if($titlelength > 128)
                        {
                        $title = substr($proposal['Proposal']['proposal_title'], 0, $titlelength); 
                        }
                        if($detailslength > 1000)
                        {
                        $details = substr($proposal['Proposal']['proposal_details'], 0, $detailslength); 
                        }
                        if($approachlength > 1000)
                        {
                        $approach = substr($proposal['Proposal']['proposal_approach'], 0, $approachlength); 
                        }
                        if($successlength > 500)
                        {
                        $success = substr($proposal['Proposal']['proposal_success'], 0, $successlength); 
                        } 

                        $this->set("title",$title);
                        $this->set("details",$details);
                        $this->set("approach",$approach);
                        $this->set("success",$success);
                 }


 	
              else if(!(empty($this->data))){

              $edit_time = date("Y-m-d H:i:s");
                
              $id = $this->data['Proposal']['id'];
    	      $title = $bodytag = str_replace("'", "_a_", $this->data['Proposal']['proposal_title']);
    	      $details = $bodytag = str_replace("'", "_a_", $this->data['Proposal']['proposal_details']);
   	      $approach = $bodytag = str_replace("'", "_a_", $this->data['Proposal']['proposal_approach']);;
    	      $success = $bodytag = str_replace("'", "_a_", $this->data['Proposal']['proposal_success']);

	     $this->Proposal->updateAll(array('Proposal.proposal_title'=>"'".$title."'"),array('Proposal.id'=>$id));
	     $this->Proposal->updateAll(array('Proposal.proposal_details'=>"'".$details."'"),array('Proposal.id'=>$id));
	     $this->Proposal->updateAll(array('Proposal.proposal_approach'=>"'".$approach."'"),array('Proposal.id'=>$id));
	     $this->Proposal->updateAll(array('Proposal.proposal_success'=>"'".$success."'"),array('Proposal.id'=>$id));
	     $this->Proposal->updateAll(array('Proposal.modified'=>"'".$edit_time."'"),array('Proposal.id'=>$id));


             $this->redirect(array('action' => 'proposal_preview', $id));
    	}

    	} else {
    		$this->redirect(array('controller'=>'project','action'=>'project_proposals'));
    	}

    }

       function proposal_preview($proposalId){

        if($this->Auth->user('id') != "" && $this->Auth->user('role_id') == 2 && $this->Auth->user('access_specifier') =='publish'){

               $this->layout= 'defaultnew';
               $this->set("title_for_layout","Project proposal preview");


			$result = $this->Topic->find('all');
			$qcategories = array();
			foreach($result as $value){
				$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
			}
			$this->set("categories",$qcategories);

	    	if($proposalId != null){

		$keywords = $this->Topic->find('all',array(
				'fields' => array('DISTINCT (Topic.autocomplete_text) AS autocomplete_text'),
				'conditions'=>array('Topic.isName'=>0)
		));

		$this->set('keywords',$keywords);
            
		
		$key = array();
		$cn = 0;
		foreach($keywords as $value){
			$key[$cn++] = $value['Topic']['autocomplete_text'];
		}
		$this->set('allKeywords', $key);
	    	
	    		$proposal = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $proposalId)));
 
                        $proposal['Proposal']['proposal_title'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_title']);
                        $proposal['Proposal']['proposal_details'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_details']);
                        $proposal['Proposal']['proposal_approach'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_approach']);
                        $proposal['Proposal']['proposal_success'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_success']);


                       if($proposal['Proposal']['proposal_url']==''){

  	               $urlKey = preg_replace('/\PL/u', '-', $proposal['Proposal']['proposal_title']);
		       $urlKey = $urlKey.'-'.$proposal['Proposal']['id']; 

	    	       $this->Proposal->updateAll(array('Proposal.proposal_url'=>"'".$urlKey."'"),array('Proposal.id'=>$proposal['Proposal']['id']));
                       }

                   
                     $user = $this->User->findById($proposal['Proposal']['user_id']);
                     $this->set('proposal',$proposal);
                     $this->set('user',$user);

		$this->loadModel('City');
		$dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($user['UserReference']['zipcode']))));
		$this->set("city",$dataZip['City']['city_name']);
		$this->set("state",$dataZip['City']['state']);
	    	}

    	} else {
    		$this->redirect(array('controller'=>'project','action'=>'project_proposals'));
    	}


      } 

    function add_proposal() {
    	 
    	if ($this->RequestHandler->isAjax()) {
    		Configure::write('debug', 0);
    		$proposal['user_id'] = $this->Auth->user('id');	
    		$proposal['proposal_title'] = $this->request->data['title'];
    		$proposal['proposal_details'] = $this->request->data['details'];
    		$proposal['proposal_approach'] = $this->request->data['approach'];   
    		$proposal['proposal_success'] = $this->request->data['success'];
              
			$this->Proposal->save($proposal);
			$id = $this->Proposal->getLastInsertId();

                   


		echo json_encode($id);
    		$this->render(false);
    		exit();
    	}
    }


    function finalize_proposal($id = null){

    	if(!(empty($this->data))){
    		
              	    $proposal = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id)));


                        $value = "Y";
	    	        $this->Proposal->updateAll(array('Proposal.haspublished'=>"'".$value."'"),array('Proposal.id'=>$proposal['Proposal']['id']));
                        $this->Proposal->updateAll(array('Proposal.ispublic'=>"'".$value."'"),array('Proposal.id'=>$proposal['Proposal']['id']));
                        $this->ProposalTopic->deleteAll(array('ProposalTopic.proposal_id'=>$proposal['Proposal']['id']));
			$checkBoxes = $this->data['Proposal']['select'];


			$cat = explode(',',$checkBoxes);
                        $cat = array_unique($cat);

			for($i=0;$i<3;$i++){
			
				if(isset($cat[$i]) && $cat[$i] != '' && $cat[$i] != -1) {
					
			       $topic = $this->Topic->find('first', array('conditions'=> array('Topic.autocomplete_text'=>$cat[$i])));
		               if(empty($topic)) {
			
                                $this->Topic->create( );
				    $this->Topic->save(array('autocomplete_text'=>$cat[$i]));

                                   $topicid = $this->Topic->getLastInsertID();
                                   $this->Topic->updateAll(array('Topic.topic_subset' => 0),array('Topic.id ' => $topicid));
                                   $topic = $this->Topic->find('first', array('conditions'=> array('Topic.id'=>$topicid)));
				    
				}
                                 
					

					$proposal_topic['proposal_id'] = $proposal['Proposal']['id'];
					$proposal_topic['topic_id'] = $topic['Topic']['id'];
					
					$this->ProposalTopic->save($proposal_topic);
					$this->ProposalTopic->id = '';
					$this->Topic->id = '';
				}
			}

		    if($this->Auth->user('role_id') == 1){

                    $this->Session->setFlash('Proposal has been updated', 'admin_flash_good');
                    $this->redirect(array('action' => 'admin_proposalreview'));

                        
                    }else{

			
		        if($this->Auth->user('role_id') != 1){

                        $user = $this->User->find('first', array('conditions' => array('User.id' => $proposal['Proposal']['user_id'])));

                        

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $link = SITE_URL."project/proposal/".$proposal['Proposal']['proposal_url'];
                           $to = $user['User']['username'];
                           $subject = "Your project proposal has been submitted";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("proposal_submit_confirmation");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_proposallink=".urlencode($link);
                           $data .= "&merge_proposaltitle=".urlencode($proposal['Proposal']['proposal_title']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

                   
                         }

                 if($proposal['Proposal']['projectid'] !=''){

 

    	          $project = $this->Project->find('first', array('conditions' => array('Project.id' => $proposal['Proposal']['projectid'])));

                        $value = "N";
                        $this->Proposal->updateAll(array('Proposal.ispublic'=>"'".$value."'"),array('Proposal.id'=>$proposal['Proposal']['id']));


   
                    $client =  $this->User->find('first',array('conditions'=>array('User.id'=>$project['Project']['user_id'])));
                    $id = $this->Session->read('Auth.User.id');
                    $mentorData = $this->User->find('first',array('conditions'=>array('User.id'=>$id)));

    	

                          $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "ankit@guild.im";
                           $subject = "Consultant has shown interest in client project.";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("consultant_interest_email_to_admin");
                           $data .= "&merge_consultantfname=".urlencode($mentorData['UserReference']['first_name']);
                           $data .= "&merge_consultantlname=".urlencode($mentorData['UserReference']['last_name']);
                           $data .= "&merge_clientfname=".urlencode($client['UserReference']['first_name']);
			   $data .= "&merge_clientlname=".urlencode($client['UserReference']['last_name']);
                           $data .= "&merge_clientemail=".urlencode($client['User']['username']);
                           $data .= "&merge_projecttitle=".urlencode($project['Project']['title']);
                           $data .= "&merge_projectlink=".urlencode($project['Project']['project_url']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }
						   
						  						   
						  						   
						  
                    }



                      if($proposal['Proposal']['projectid'] !=''){

 

    	                       $project = $this->Project->find('first', array('conditions' => array('Project.id' => $proposal['Proposal']['projectid'])));

                                $urlkey = "project/index/".$project['Project']['project_url'];


				$sending = SITE_URL.$urlkey;

 
				echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
				die;

                         }else{

                                $urlkey = $user['User']['url_key'].'#project_proposals';


				$sending = SITE_URL.$urlkey;

 
				echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
				die;

                            }
				
                     }// admin proposal edit else condtion end     


    	} else {
    		$this->redirect(array('controller'=>'fronts','action'=>'index'));
    	}



   }


         function project_proposals($category = null){

                     $this->set("title_for_layout","Project proposals");
                     $this->layout= 'defaultnew';
			//GetProposal topics


			$result = $this->Topic->find('all');
			$qcategories = array();
			foreach($result as $value){
				$qcategories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
			}
			$this->set("categories",$qcategories);



                        $proposal_topics = $this->ProposalTopic->find('all', array('fields'=>'DISTINCT topic_id'));

                        
                        $ptopiclist = array();

			foreach($proposal_topics as $key=>$aValue){
				
				$ptopiclist[$key] = $qcategories[$aValue['ProposalTopic']['topic_id']];
			}

                        $this->set("topiclist",$ptopiclist);

                       if($category == null){

                       $proposalData = $this->Proposal->find('all',array('conditions'=>array('AND' => array(array('Proposal.haspublished'=>"Y"),array('Proposal.ispublic'=>"Y"))),'order'=>'Proposal.modified desc'));

                        $proposalArray = array();
                        $count = 0;
                        $total1 = count($proposalData);
                        $qCount1 = 0;
                        $this->loadModel('City');

			$this->User->Behaviors->attach('Containable');
			foreach($proposalData as $aValue){
					if($qCount1 < $total1){

			
					$userData = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name','linkedin_headline','zipcode'),
											),
											'UserImage' ,
									),
									'conditions'=>array('User.id'=>$aValue['Proposal']['user_id'])
							)
			
					);

		                        $dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($userData['UserReference']['zipcode']))));
						
					if(empty($userData['UserImage']) == false)
				        $aValue['User']['user_image'] = $userData['UserImage'][0]['image_name'];
					$aValue['User']['first_name'] = $userData['UserReference']['first_name'];
					$aValue['User']['last_name'] = $userData['UserReference']['last_name'];
                                        $aValue['User']['headline'] = $userData['UserReference']['linkedin_headline'];
					$aValue['User']['url_key'] = $userData['User']['url_key'];
					$aValue['User']['city'] = $dataZip['City']['city_name'];
					$aValue['User']['state'] = $dataZip['City']['state'];
				        $qCount1++;
					$proposalArray[$count++] = $aValue;
			}
                   }

                                  $this->set("proposals", $proposalArray);
                                  $title = 'All Project Proposals';
                                  $this->set('queryString',$title );



                      }else{

                           $keyword = $category;
                           if(false !== stripos($category,"_and"))
                           $keyword = str_replace("_and","&",$category);

                           if(false !== stripos($category,"_slash"))
                           $keyword = str_replace("_slash","/",$category);

                           $keyword = str_replace("-"," ",$keyword);

                           if(false !== stripos($keyword,"Non profit"))
                           $keyword = str_replace("Non profit","Non-profit",$keyword);

                           if(false !== stripos($keyword,"Cross cultural practices"))
                           $keyword = str_replace("Cross cultural practices","Cross-cultural practices",$keyword);

                           if(false !== stripos($keyword,"Post merger integration (PMI)"))
                           $keyword = str_replace("Post merger integration (PMI)","Post-merger integration (PMI)",$keyword);

		           $topic = $this->Topic->find('first', array('conditions' => array('Topic.autocomplete_text' => $keyword)));
                    
                           $id = $topic['Topic']['id'];

			   $Proposallist = $this->ProposalTopic->find('all', array('conditions' => array('ProposalTopic.topic_id' => $id), 'order'=>'ProposalTopic.id desc'));

                          $total = count($Proposallist);
                                
			  $proposalArray = array();
                          $datetime = array();

                          $cCount = 0;
                          $count = 0;
                          $this->loadModel('City');

			  foreach($Proposallist as $aValue){
                          if($cCount < $total){

                           $proposals = $this->Proposal->find('first',array('conditions'=>array('AND' => array(array('Proposal.id' => $aValue['ProposalTopic']['proposal_id']),array('Proposal.ispublic'=>"Y"))),'order'=>'Proposal.modified desc'));
                          //$proposals = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $aValue['ProposalTopic']['proposal_id']), 'order'=>'Proposal.modified desc'));
                           if($proposals['Proposal']['haspublished'] =="Y"){
					$userData = $this->User->find('first',
							array(
									'contain'=>array(
											'UserReference'=>array(
													'fields'=>array('first_name','last_name','linkedin_headline','zipcode'),
											),
											'UserImage' ,
									),
									'conditions'=>array('User.id'=>$proposals['Proposal']['user_id'])
							)
			
					);

		                        $dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($userData['UserReference']['zipcode']))));
						
					if(empty($userData['UserImage']) == false)
				        $proposals['User']['user_image'] = $userData['UserImage'][0]['image_name'];
					$proposals['User']['first_name'] = $userData['UserReference']['first_name'];
					$proposals['User']['last_name'] = $userData['UserReference']['last_name'];
                                        $proposals['User']['headline'] = $userData['UserReference']['linkedin_headline'];
					$proposals['User']['url_key'] = $userData['User']['url_key'];
					$proposals['User']['city'] = $dataZip['City']['city_name'];
					$proposals['User']['state'] = $dataZip['City']['state'];
                                        $proposals['User']['time'] = strtotime($proposals['Proposal']['modified']);
                                        $cCount++;
                                        $datetime[$count++] = strtotime($proposals['Proposal']['modified']);
                                        $proposalArray[$count++] = $proposals;
                                       

                        }
                                 
                       }
                        
                       }

                                  $data = Set::sort($proposalArray, '{n}.User.time', 'desc');
			          $this->set("proposals", $data);
                                  $title = 'Project Proposals for "'.$keyword.'"';
                                  $this->set('queryString',$title );


 
         }
           

        }






        function proposal_pdf_preview($proposalid)
        {

                        $this->layout= 'defaultnew';

          
                        $proposal = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $proposalid)));

                        $proposal['Proposal']['proposal_title'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_title']);
                        $proposal['Proposal']['proposal_details'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_details']);
                        $proposal['Proposal']['proposal_approach'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_approach']);
                        $proposal['Proposal']['proposal_success'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_success']); 

                        $pos = strrpos($proposal['Proposal']['proposal_success'], '.');
                        $proposal['Proposal']['proposal_success'] = substr($proposal['Proposal']['proposal_success'], 0, $pos). '.';  


                     $user = $this->User->findById($proposal['Proposal']['user_id']);
                     $this->set('proposal',$proposal);
                     $this->set('user',$user);



		$this->loadModel('City');
		$dataZip=$this->City->find('first',array('conditions'=>array('City.zip_code'=>trim($user['UserReference']['zipcode']))));
		$this->set("city",$dataZip['City']['city_name']);
		$this->set("state",$dataZip['City']['state']);

                $this->set('pdf_preview_data', $proposal);
           	$this->Session->write('pdf_preview_data', $proposal);
           	$this->Session->write('user', $user);
           	$this->Session->write('city', $dataZip['City']['city_name']);
           	$this->Session->write('state', $dataZip['City']['state']);

        }
        
        function preview_pdf($data = null)
        {
        	$this->layout = 'pdf';
        	$pdf_preview_data = $this->Session->read('pdf_preview_data');

        	$user = $this->Session->read('user');
        	$city = $this->Session->read('city');
        	$state = $this->Session->read('state');


			$this->response->header(array('Content-type: application/pdf')); 
			$this->response->type('pdf'); 
        	$this->set('proposal',$pdf_preview_data);
        	$this->set('user',$user);
        	$this->set('city',$city);
        	$this->set('state',$state);
        }

        function preview_pdf1($data = null)
        {
        	$this->layout = 'pdf';
        	$pdf_preview_data = $this->Session->read('pdf_preview_data');

        	$user = $this->Session->read('user');
        	$city = $this->Session->read('city');
        	$state = $this->Session->read('state');


			$this->response->header(array('Content-type: application/pdf')); 
			$this->response->type('pdf'); 
        	$this->set('proposal',$pdf_preview_data);
        	$this->set('user',$user);
        	$this->set('city',$city);
        	$this->set('state',$state);
        }


        function project_adminedit($id = null)

       {

        $this->layout= 'defaultnew';
        $industry_categories = $this->IndustryCategory->find('all',array('order'=>'IndustryCategory.id asc'));
        $this->set('industry_categories', $industry_categories);


    	if($id != null){
    		
    		$project = $this->Project->find('first', array('conditions' => array('Project.id' => $id)));
    		


    		$this->data = $project;
    		
    	} else if(!(empty($this->data))){
             

                if($this->Session->read('Auth.User.role_id')!=1){
    		$this->request->data['Project']['user_id'] = $this->Auth->user('id');
                $this->request->data['Project']['url'] = $this->Auth->user('username');
    		$this->request->data['Project']['status'] = 1;
                }
                if($this->data['Resume1']['resume_location']['name']['name'] !=''){
                $this->request->data['Project']['filepath'] = $this->data['Resume1']['resume_location']['name']['name'];
                }
                $this->request->data['Project']['status'] = 3;
                $this->Project->save($this->request->data['Project']);





                   if ($this->data['Resume1']['resume_location']['name'] != '') {
                       

    			$destination = IMAGES . MENTEES_PROJECT_PATH . DS . $this->Project->id;
                     
    			if (!is_dir($destination)) {
                          
    				mkdir($destination, 0777);
                    
    			}
                    
    			$destination .= DS . $this->data['Resume1']['resume_location']['name']['name'];
    			
    			move_uploaded_file($this->data['Resume1']['resume_location']['name']['tmp_name'], $destination);
    			
    			
    		}


    		
    		$this->redirect(array('action' => 'project_adminview', $this->Project->id));
    	}


     }

    function project_adminview($projectId){
    	
                 $this->layout= 'defaultnew';
	    	if($projectId != null){
	    	
	    		$project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectId)));

  	              $urlKey = preg_replace('/\PL/u', '-', $project['Project']['title']);
		       $urlKey = $urlKey.'-'.$project['Project']['id']; 
	    	       $this->Project->updateAll(array('Project.project_url'=>"'".$urlKey."'"),array('Project.id'=>$project['Project']['id']));

	    		$this->data = $project;

			$this->Session->write('projectId',$projectId);


	    	}

    }

    function admin_proposalreview(){

      	if($this->Auth->user('id') == 1){  
	$this->layout = 'admin';  
    	$proposals = $this->Proposal->find('all',array('order'=>'Proposal.modified desc'));

    	$this->set('proposals', $proposals);
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }

    function proposal_adminedit($id = null){


          $this->layout= 'defaultnew';
          $this->set("title_for_layout","Project proposal create");
	  $this->Session->write('popup', 1); 
	    	if($id != null){
	    	
	    		$proposal = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id)));
                        $proposal['Proposal']['proposal_title'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_title']);
                        $proposal['Proposal']['proposal_details'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_details']);
                        $proposal['Proposal']['proposal_approach'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_approach']);
                        $proposal['Proposal']['proposal_success'] = $bodytag = str_replace("_a_", "'", $proposal['Proposal']['proposal_success']);
    		        $this->data = $proposal;

                        $titlelength = strlen($proposal['Proposal']['proposal_title']);
                        $detailslength = strlen($proposal['Proposal']['proposal_details']);  
                        $approachlength = strlen($proposal['Proposal']['proposal_approach']);
                        $successlength = strlen($proposal['Proposal']['proposal_success']);

                        $title =  $proposal['Proposal']['proposal_title'];
                        $details =  $proposal['Proposal']['proposal_details'];
                        $approach =  $proposal['Proposal']['proposal_approach'];
                        $success =  $proposal['Proposal']['proposal_success'];

                        if($titlelength > 128)
                        {
                        $title = substr($proposal['Proposal']['proposal_title'], 0, $titlelength); 
                        }
                        if($detailslength > 1000)
                        {
                        $details = substr($proposal['Proposal']['proposal_details'], 0, $detailslength); 
                        }
                        if($approachlength > 1000)
                        {
                        $approach = substr($proposal['Proposal']['proposal_approach'], 0, $approachlength); 
                        }
                        if($successlength > 500)
                        {
                        $success = substr($proposal['Proposal']['proposal_success'], 0, $successlength); 
                        } 

                        $this->set("title",$title);
                        $this->set("details",$details);
                        $this->set("approach",$approach);
                        $this->set("success",$success);
                 }


 	
              else if(!(empty($this->data))){
             


              $edit_time = date("Y-m-d H:i:s");
                
              $id = $this->data['Proposal']['id'];
    	      $title = $bodytag = str_replace("'", "_a_", $this->data['Proposal']['proposal_title']);
    	      $details = $bodytag = str_replace("'", "_a_", $this->data['Proposal']['proposal_details']);
   	      $approach = $bodytag = str_replace("'", "_a_", $this->data['Proposal']['proposal_approach']);;
    	      $success = $bodytag = str_replace("'", "_a_", $this->data['Proposal']['proposal_success']);

	     $this->Proposal->updateAll(array('Proposal.proposal_title'=>"'".$title."'"),array('Proposal.id'=>$id));
	     $this->Proposal->updateAll(array('Proposal.proposal_details'=>"'".$details."'"),array('Proposal.id'=>$id));
	     $this->Proposal->updateAll(array('Proposal.proposal_approach'=>"'".$approach."'"),array('Proposal.id'=>$id));
	     $this->Proposal->updateAll(array('Proposal.proposal_success'=>"'".$success."'"),array('Proposal.id'=>$id));
	     $this->Proposal->updateAll(array('Proposal.modified'=>"'".$edit_time."'"),array('Proposal.id'=>$id));


             $this->redirect(array('action' => 'proposal_preview', $id));
    	}




     }
       



    function admin_deleteproposal($id = null){
    	if($this->Auth->user('id') == 1){    
		$this->layout = 'admin';  
    	if($this->Proposal->delete($id)) {
    
    		$proposalTopic = $this->ProposalTopic->find('all', array('conditions' => array('ProposalTopic.proposal_id' => $id)));
    		 
    		foreach ($proposalTopic as $topics) {
    		
    			$this->ProposalTopic->delete($topics['ProposalTopic']['id']);
    		}
    		
    		$this->Session->setFlash('Proposal has been deleted', 'admin_flash_good');
    		$this->redirect(array('action' => 'admin_proposalreview'));
    	} else {
    		$this->Session->setFlash('Proposal could not be deleted', 'admin_flash_bad');
    	}
        }else{
          $this->redirect(array('controller' => 'fronts', 'action' => 'index'));
        }
    }



    function schedule_call($projectid){


                if($this->Session->check('projectId')){
                $this->Session->delete('projectId');
                } 

          $this->layout= 'defaultnew';
          $this->set("title_for_layout","Project Schedule Call");
          $this->Session->write('projectId',$projectid);



                         
       }





         function stripe_payment($projectid){

          $this->layout= 'defaultnew';
          $this->set("title_for_layout","Stripe Payment");
          $this->set("projectid",$projectid);    

         }

       function thanks_for_payment(){

 

                $this->layout= 'defaultnew';
	
                $projectid = $this->Session->read('projectId');

    		$project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectid)));

                        $user = $this->User->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));




                $this->Session->delete('projectId');
                

         }




        function projectedit($id)

       {


        $this->layout= 'defaultnew';
        $industry_categories = $this->IndustryCategory->find('all',array('order'=>'IndustryCategory.id asc'));
        $this->set('industry_categories', $industry_categories);


    	if($id != null){
    		
    		$project = $this->Project->find('first', array('conditions' => array('Project.id' => $id)));
    		


    		$this->data = $project;
    		
    	} else if(!(empty($this->data))){
             


                if($this->data['Resume1']['resume_location']['name']['name'] !=''){
                $this->request->data['Project']['filepath'] = $this->data['Resume1']['resume_location']['name']['name'];
                }

                $this->Project->save($this->request->data['Project']);





                   if ($this->data['Resume1']['resume_location']['name'] != '') {
                       

    			$destination = IMAGES . MENTEES_PROJECT_PATH . DS . $this->Project->id;
                     
    			if (!is_dir($destination)) {
                          
    				mkdir($destination, 0777);
                    
    			}
                    
    			$destination .= DS . $this->data['Resume1']['resume_location']['name']['name'];
    			
    			move_uploaded_file($this->data['Resume1']['resume_location']['name']['tmp_name'], $destination);
    			
    			
    		}
                 
                 $this->redirect(array('action' => 'project_view', $this->Project->id));
    		
    		//$this->redirect(array('action' => 'index/'.$project['Project']['id']));
    	}


     }


    function client_approveproject($projectId = null){
  
    	if($this->Auth->User('id') != '' || $this->Auth->User('role_id') == 1) {
	    	
    		if($projectId) {
	    		
		    	$ProjectMembers = $this->ProjectMember->find('all', array('conditions' => array('ProjectMember.project_id' => $projectId)));
		    	
		    	$project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectId)));


	

		    	foreach ($ProjectMembers as $members) {
		    		

		    		$this->project_interest($members['ProjectMember']['member_id'], $projectId);

                          }
		    	
		    	$project['Project']['status'] = 4; //Project approved by client
		    	 
		    	$this->Project->save($project);

                        $user = $this->User->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));


		    	
		    	
		    	$this->redirect(array('action' => 'stripe_payment',$this->Project->id));
	    	}
    } else {
    		$this->redirect(array('controller'=>'fronts','action'=>'index'));
    	}

    }

    function project_view($projectId){
    	
                 $this->layout= 'defaultnew';
	    	if($projectId != null){
	    	
	    		$project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectId)));

  	              $urlKey = preg_replace('/\PL/u', '-', $project['Project']['title']);
		       $urlKey = $urlKey.'-'.$project['Project']['id']; 
	    	       $this->Project->updateAll(array('Project.project_url'=>"'".$urlKey."'"),array('Project.id'=>$project['Project']['id']));

	    		$this->data = $project;

			$this->Session->write('projectId',$projectId);


	    	}

    }

         function stripecheckout(){
    	  if ($this->RequestHandler->isAjax()) {

    			
    		$invoiceId = $this->request->data['invoiceId'];
                $amount = $this->request->data['price'];
                $token = $this->request->data['token'];
                $email = $this->request->data['email'];

                Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");
                


			try {
				$charge = Stripe_Charge::create(array(
						"amount" => $amount, // amount in cents, again
						"currency" => "usd",
						"card" => $token,
						"receipt_email"=> $email,
						"description" => "Project Invoice Payment")
                        
				);


		    	$projectinvoice = $this->ProjectInvoice->find('first', array('conditions' => array('ProjectInvoice.id' => $invoiceId)));

		    	
		    	$projectinvoice['ProjectInvoice']['invoicestatus'] = "PAID"; //Project Invoice payment done by client
		    	 
		    	$this->ProjectInvoice->save($projectinvoice);
                 
		echo json_encode($invoiceId);
    		$this->render(false);
    		exit();
                 
                } catch(Stripe_CardError $e) {
              			//pr("There is some error");
				//pr($e);
                                //die;

               }


            }


         }


		function admin_changeprojectstatus($id = null){

			$this->layout = 'admin';


			$this->set('title_for_layout', 'Project Status');     

			if(!empty($this->data)){


                       if($this->data['Project']['status'] =="0. Project created by Client")
                       $status = "0";
                       if($this->data['Project']['status'] =="1. Project posted by Client")
                       $status = "1";
                       if($this->data['Project']['status'] =="2. Client Partner call scheduled")
                       $status = "2";
                       if($this->data['Project']['status'] =="3. Project submitted for Client approval")
                       $status = "3";
                       if($this->data['Project']['status'] =="4. Project approved by Client")
                       $status = "4";
                       if($this->data['Project']['status'] =="5. $500 Payment Done")
                       $status = "5";
                       if($this->data['Project']['status'] =="6. Proposal submitted"){
                       $status = "6";   

                    $project = $this->Project->find('first', array('conditions' => array('Project.id' => $id)));
                    $user = $this->User->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));  

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $user['User']['username'];
                           $subject = $user['UserReference']['first_name'].", your project has a proposal";
                           $link = SITE_URL.'clients/my_account/'.$user['User']['id'];
                           $projectlink = SITE_URL.'project/index/'.$project['Project']['project_url'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("project_status_change");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           $data .= "&merge_clientemail=".urlencode($user['User']['username']);
                           $data .= "&merge_projecttitle=".urlencode($project['Project']['title']);
                           $data .= "&merge_profilelink=".urlencode($link);
                           $data .= "&merge_projectlink=".urlencode($projectlink);

                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

                       } 
                 
                       if($this->data['Project']['status'] =="7. Proposal approved by Client")
                       $status = "7";
                       if($this->data['Project']['status'] =="8. Project started")
                       $status = "8";
                       if($this->data['Project']['status'] =="9. Project completed")
                       $status = "9";


                       $this->Project->updateAll(array('Project.status'=>"'".$status."'"),array('Project.id'=>$id));

					$this->Session->setFlash('Project Status has been changed successfully', 'admin_flash_good');
					$this->redirect(array( 'action'=>'admin_review'));
				
			}else{
				$project = $this->Project->find('first', array('conditions' => array('Project.id' => $id)));
				$this->data = $project;
			}   
    

		}

		function admin_changeproposalstatus($id = null){

			$this->layout = 'admin';  

			$this->set('title_for_layout', 'Proposal Status');     

			if(!empty($this->data)){

                       if($this->data['Proposal']['ispublic'] =="Public")
                       $status = "Y";
                       if($this->data['Project']['ispublic'] =="Private")
                       $status = "N";

                       $this->Proposal->updateAll(array('Proposal.ispublic'=>"'".$status."'"),array('Proposal.id'=>$id));

					$this->Session->setFlash('Proposal Status has been changed successfully', 'admin_flash_good');
					$this->redirect(array( 'action'=>'admin_proposalreview'));
				
			}else{
				$project = $this->Proposal->find('first', array('conditions' => array('Proposal.id' => $id)));
				$this->data = $project;
			}   
    

		}





	function invoice_create($projectId)
		{

                        $this->layout= 'defaultnew';

                        $project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectId)));

                        $Client = $this->User->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));
                        $this->set('project',$project);

                        $this->set('clientData',$Client);

                                $invoicecount = $this->ProjectInvoice->find('count', array('conditions' => array('ProjectInvoice.projectid' => $projectId)));
                                $invoicecount = $invoicecount + 1;
                                $invoiceno     = $projectId."-0".$invoicecount;
                                $this->set('invoiceno',$invoiceno);
                                $this->set('projectid',$projectId);




				
				//Get Member user
				$this->User->Behaviors->attach('Containable');
				$member = $this->User->find('first',
						array(
								'contain'=>array(
										'UserReference'=>array(
												'fields'=>array('first_name','last_name','fee_first_hour','fee_regular_session','regular_session_per'),
										),
								),
								'conditions'=>array('User.id'=>$this->Auth->user('id'))
						)
				
				);
				
				

				$this->set('memberData',$member);
			
		}








     function invoice_detail($id = null){

          $this->layout= 'defaultnew';

        $invoice = $this->ProjectInvoice->find('first', array('conditions' => array('ProjectInvoice.invoiceno' => $id)));
        if($invoice['ProjectInvoice']['invoiceno'] !=''){

        $project = $this->Project->find('first', array('conditions' => array('Project.id' => $invoice['ProjectInvoice']['projectid'])));
        $this->set('invoicedata',$invoice);
        $this->set('project',$project);
        $invoicelink = SITE_URL."project/invoice_detail/".$invoice['ProjectInvoice']['invoiceno'];

        $this->Session->write('invoicepaypage', $invoicelink);

    	} else {
    		$this->redirect(array('controller'=>'fronts','action'=>'index'));
    	}
           
     }



        function invoice_preview()
        {


                     $this->set("title_for_layout","Invoice Preview");
                     $this->layout= 'defaultnew';

                                $invno = $this->data['ProjectInvoice']['invoiceno'];

                                $projectId = $this->data['ProjectInvoice']['projectid'];

                                $filename = "GUILD Invoice-".$invno.".pdf";
                                $filepath = "/var/www/html/guild/app/webroot/img/clients/projects/".$projectId."/".$filename;

                                $this->set('invoiceno',$invno);
                                $this->set('projectid',$projectId);
                                 
    			        if (is_dir($filepath)) {                            
                                
    				 unlink($filepath);
                    
    			       }
                             
                              
                                 

        	if(empty($this->data)){



        		$pdf_preview_data = $this->Session->read('pdf_preview_data');

        		if($pdf_preview_data == null && empty($pdf_preview_data)) {
        			$this->redirect(array('controller'=>'fronts','action'=>'index'));
        		} else {
        			$this->request->data = $pdf_preview_data;
        		}
        	}


                $userId = $this->Session->read('Auth.User.id'); 
          	if($userId==''){
            	$this->redirect(array('controller'=>'fronts','action'=>'index'));
          	}
         
           	$this->Session->write('pdf_preview_data1', $this->data);
                $this->Session->write('pdf_preview_data3', $this->data);
                $this->Session->write('pdf_preview_data4', $this->data);
                $this->Session->write('sendinvoice', $this->data);

        }
        







     function invoicepdf1()
        {
        	$this->layout = 'pdf';
        	$pdf_preview_data1 = $this->Session->read('pdf_preview_data1');
                $this->Session->delete('pdf_preview_data1');

			$this->response->header(array('Content-type: application/pdf')); 
			$this->response->type('pdf'); 
        	        $this->set('data',$pdf_preview_data1);

            }




     function invoicepdf3()
        {
        	$this->layout = 'pdf';
        	$pdf_preview_data3 = $this->Session->read('pdf_preview_data3');
                 $this->Session->delete('pdf_preview_data3');

			$this->response->header(array('Content-type: application/pdf')); 
			$this->response->type('pdf'); 
        	        $this->set('data',$pdf_preview_data3);

            }


        function invoicepdf4()
        {


        	$this->layout = 'pdf';
        	$pdf_preview_data4 = $this->Session->read('pdf_preview_data4');
                     $this->Session->delete('pdf_preview_data4');
			$this->response->header(array('Content-type: application/pdf')); 
			$this->response->type('pdf'); 
        	        $this->set('data',$pdf_preview_data4);

        }

        function invoice_pdf($data = null)
        {

           
        	$this->layout = 'pdf';
                $invoice = $this->ProjectInvoice->find('first', array('conditions' => array('ProjectInvoice.invoiceno' => $data)));

        	$pdf_preview_data = $invoice;
        	
			$this->response->header(array('Content-type: application/pdf')); 
			$this->response->type('pdf'); 
        	        $this->set('data',$pdf_preview_data);
        }


       function sendinvoice(){


		if ($this->RequestHandler->isAjax()) {
    		Configure::write('debug', 0);
    	

	    	 

	              
	              $this->layout = '';
	    		$this->render(false);		    		 
	

	    	
                                
                                $pdf_data = $this->Session->read('sendinvoice');
                                $this->Session->delete('sendinvoice');

                                $projectId = $pdf_data['ProjectInvoice']['projectid'];

                                $project = $this->Project->find('first', array('conditions' => array('Project.id' => $projectId))); 
                                $invoicecount = $this->ProjectInvoice->find('count', array('conditions' => array('ProjectInvoice.projectid' => $projectId)));


                                $invoicecount = $invoicecount + 1;
                                $invoiceno     = $projectId."-0".$invoicecount;


                                $this->ProjectInvoice->create();
				$userId = $this->Session->read('Auth.User.id');
				

                                $invoicearray['ProjectInvoice']['inv_mentor_id'] = $userId;
                                $invoicearray['ProjectInvoice']['inv_client_id'] = $project['Project']['user_id'];
                                $invoicearray['ProjectInvoice']['title'] = $pdf_data['ProjectInvoice']['inv_title'];
				$invoicearray['ProjectInvoice']['inv_description'] = $pdf_data['ProjectInvoice']['inv_description'];                                
                                $invoicearray['ProjectInvoice']['inv_client_name'] = $pdf_data['ProjectInvoice']['client_name'];				
				$invoicearray['ProjectInvoice']['inv_client_email'] = $pdf_data['ProjectInvoice']['client_email'];
				$invoicearray['ProjectInvoice']['inv_from'] = $pdf_data['ProjectInvoice']['inv_from'];
				$invoicearray['ProjectInvoice']['amount']     = $pdf_data['ProjectInvoice']['inv_amount'];
				$invoicearray['ProjectInvoice']['discount']  = $pdf_data['ProjectInvoice']['inv_discount'];
				$invoicearray['ProjectInvoice']['subtotal']  = $pdf_data['ProjectInvoice']['inv_amount'] - $pdf_data['ProjectInvoice']['inv_discount'];
				$invoicearray['ProjectInvoice']['tax']       = $pdf_data['ProjectInvoice']['inv_tax'];
				$invoicearray['ProjectInvoice']['total']     = $pdf_data['ProjectInvoice']['inv_subtotal'] + $pdf_data['ProjectInvoice']['inv_tax'];
                                $invoicearray['ProjectInvoice']['projectid']     = $projectId;

                                $invoicearray['ProjectInvoice']['invoiceno']     = $invoiceno;
                                $invoicearray['ProjectInvoice']['invoicestatus']     = "OPEN";
                               
		
				$this->ProjectInvoice->save($invoicearray);



	                        $invoice = $this->ProjectInvoice->find('first', array('conditions' => array('ProjectInvoice.invoiceno' => $invoiceno)));
						

                                 $url = 'https://api.elasticemail.com/v2/email/send';
                                 $filename = "GUILD Invoice-".$invoiceno.".pdf";
                                 $filepath = "/var/www/html/guild/app/webroot/img/clients/projects/".$projectId."/".$filename;

                                 $filetype = "pdf"; // Change correspondingly to the file type
                                 $invoicelink = SITE_URL."project/invoice_detail/".$invoiceno;

                                 

                                 $bodyhtml ='<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                                 <html xmlns="http://www.w3.org/1999/xhtml">
                                  <head>    
                                   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                                   </head>
                                     <body>    
                                      <table>
                                        <tr>
                                         <td>'.$invoice['ProjectInvoice']['inv_client_name'].',</td>
                                        </tr>
                                        <tr><td></td></tr>
										<tr><td></td></tr>            
                                        <tr>
                                          <td>
                                           <p>Thanks for your business. Your invoice is attached.</p>
                                           <p><a href='.$invoicelink.' target="_blank" style="color:#CF3135;">Review invoice</a></p>
                                                Please contact us if you have questions.
												<p><img src="https://www.guild.im/img/email-final.png" alt="logo" style="padding-top:5px;"></p>
                                                <p>Phone : 1-866-511-1898<br>eMail :&nbsp;<a href="mailto:help@guild.im" target="_blank" style="color:#CF3135;">help@guild.im</a> 
                                                </p>
										 </td>
                                        </tr>
      	                              </table>
									</body>
									</html>';

                           try{
                                $copyemail = "ankitsumit1990@gmail.com";
                                $post = array('from' => 'iqbal@guild.im',
                                'fromName' => 'GUILD',
			                    'to' => $invoice['ProjectInvoice']['inv_client_email'].";".$copyemail,
                                'apikey' => '3194ea3c-1324-45b7-8e3e-a2283c144674',
                                'subject' => 'Invoice '.$invoice['ProjectInvoice']['invoiceno'].' from GUILD',
                                'bodyHtml' => $bodyhtml,
                                'bodyText' => 'Text Body',
                                'isTransactional' => false,
                                'file_1' => new CurlFile($filepath, $filetype, $filename));
        
                                 $ch = curl_init();
            
                                 curl_setopt_array($ch, array(
                                 CURLOPT_URL => $url,
                                 CURLOPT_POST => true,
                                 CURLOPT_POSTFIELDS => $post,
                                 CURLOPT_RETURNTRANSFER => true,
                                 CURLOPT_HEADER => false,
                                 CURLOPT_SSL_VERIFYPEER => false
                                 ));
                                 $result=curl_exec ($ch);
                                 curl_close ($ch);
                                 //echo $result; 
                               }
                                catch(Exception $ex){
                                 echo $ex->getMessage();
                                                   }



                                $urlkey = "project/index/".$project['Project']['project_url'];


				$sending = SITE_URL.$urlkey;
 


		                echo json_encode($sending);
    		                $this->render(false);
    		                exit();

                      }


       }


       function thanks_for_invoicepayment(){

 

                $this->layout= 'defaultnew';                

         }

       function show_interest() {
    	 
		if ($this->RequestHandler->isAjax()) {
    		Configure::write('debug', 0);
    	
	    	$confirm_id = trim($this->request->data['confirm_id']);

	    	 
	    	if (!empty($confirm_id)) {
	    


                  $project['ProjectMemberConfirm']['project_id'] = $confirm_id;
                  $project['ProjectMemberConfirm']['member_id'] =  $this->Auth->user('id');
                  $this->ProjectMemberConfirm->save($project);
	              
	              $this->layout = '';
	    		$this->render(false);		    		 
	
		       echo json_encode('1');
	    		exit();
	    	}
		}
    }


    function index1($projectUrl = null, $email = null){


             if($this->Auth->user('id') == "" ){


                $this->layout= 'defaultnew';

                if($email != null){
                 $this->set('username', $email);
                 }
    

              
	    	if($projectUrl!= null) {

                     
	    		$project = $this->Project->find('first', array('conditions' => array('Project.project_url' => $projectUrl)));

                        $user = $this->User->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));

       

	    		
	    		$ProjectMembers = $this->ProjectMemberConfirm->find('all', array('conditions' => array('ProjectMemberConfirm.project_id' => $project['Project']['id'])));

	    		$ProposalMembers = $this->Proposal->find('all', array('conditions' => array('Proposal.projectid' => $project['Project']['id'])));

                     $assigned_members = $this->ProjectMember->find('all', array('conditions' => array('ProjectMember.project_id' => $project['Project']['id'])));
                     
                     $assignedmentorsArray = array();

		    	foreach ($assigned_members as $value) {
		    
		    		$condionArr['User.id'] = $value['ProjectMember']['member_id'];
		    		$assignedmentorsArray[$value['ProjectMember']['member_id']] = $this->User->find('first',
		    				array(
		    						'contain'=>array(
		    								'UserReference'=>array(
		    										'fields'=>array('first_name','last_name')
		    								),
		    								'UserImage'
		    						),
		    							
		    						'conditions'=>$condionArr
		    				)
		    					
		    		);
		    
		    	}

                     $this->set('assignedmentorsArray', $assignedmentorsArray);

		    	$mentorsArray = array();
		    	 
		    	foreach ($ProjectMembers as $value) {
		    
		    		$condionArr['User.id'] = $value['ProjectMemberConfirm']['member_id'];
		    		$mentorsArray[$value['ProjectMemberConfirm']['member_id']] = $this->User->find('first',
		    				array(
		    						'contain'=>array(
		    								'UserReference'=>array(
		    										'fields'=>array('first_name','last_name')
		    								),
		    								'UserImage'
		    						),
		    							
		    						'conditions'=>$condionArr
		    				)
		    					
		    		);
		    
		    	}
		    		 
		    	$this->data = $project;
		    	 
		    	$this->set('mentorsArray', $mentorsArray);


                     
                     $count = 0; 
                     foreach($ProjectMembers as $member){

                     if($member['ProjectMemberConfirm']['member_id'] ==  $this->Session->read('Auth.User.id'))
                     $count++;
                     
                     }
		    	$this->set('count', $count);		    	
		    	$createdBy = $this->User->find('first',
		    			array(
		    					'contain'=>array(
		    							'UserReference'=>array(
		    									'fields'=>array('first_name','last_name')
		    							),
		    							'UserImage'
		    					),
		    					'conditions'=>array('User.id'=>$project['Project']['user_id'])
		    			)
		    	
		    	);
		    	
		    	$this->set('createdBy', $createdBy);

                     $visitor_project = "Project id : ".$project['Project']['id'];
	    	     $visitor_name = $this->Prospect->find('first', array('conditions' => array('Prospect.answer' => $visitor_project)));

                     $invoice = $this->ProjectInvoice->find('all', array('conditions' => array('ProjectInvoice.projectid' => $project['Project']['id']),'order'=>'ProjectInvoice.id DESC'));
                     $invoicecount = $this->ProjectInvoice->find('count', array('conditions' => array('ProjectInvoice.projectid' => $project['Project']['id'])));


                     if($invoicecount != ''){
                     $this->set('invoicedata', $invoice);
                     }
                     $this->set('visitor_name', $visitor_name);
                     $project_title = $project['Project']['title'];
                     $this->set('invoice_title',$project_title);
                     $this->Session->write('invoice_title',$project_title);
                     $fromproject = "true";
                     $this->set('fromproject',$fromproject);
                     $this->Session->write('fromproject',$fromproject);
                 
		    }	

                  }else{            	

                       $this->redirect(array('controller'=>'fronts','action'=>'index'));
          	}


      
    }


 function confirm_interest($id=null) {

               if($this->Session->check('menteePopup')){
                $this->Session->delete('menteePopup');
                }
                if($this->Session->check('projectId')){
                $this->Session->delete('projectId');
                } 
                if($this->Session->check('questionId')){
                $this->Session->delete('questionId');
                } 
                if($this->Session->check('login_industry')){
                $this->Session->delete('login_industry');
                } 
                if($this->Session->check('mentor')){
                $this->Session->delete('mentor');
                }
                if($this->Session->check('duserid')){
                $this->Session->delete('duserid');
                } 
                if($this->Session->check('mentorshipId')){
                $this->Session->delete('mentorshipId');
                }
                if($this->Session->check('consultantPopup')){
                $this->Session->delete('consultantPopup');
                }
                if($this->Session->check('directoryclaim')){
                $this->Session->delete('directoryclaim');
                } 



                        $this->layout= 'defaultnew';

	    		$project = $this->Project->find('first', array('conditions' => array('Project.id' => $id)));

                        $user = $this->User->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));

                                          $this->Session->write('FirstName',$user['UserReference']['first_name']);
                                          $this->Session->write('LastName',$user['UserReference']['last_name']);
                                          $this->Session->write('Email',$user['User']['username']);
                                          $this->Session->write('projectindexUrl',$project['Project']['project_url']);


              }




	function projectinterestlinkedinAuth($oid, $email) {
	
		if(isset($oid) && $oid!=''&&  $email!='')
		{
			/* Read linkedin data from text file  */
			$current = file_get_contents(SITE_URL.$oid.".txt");
			$linkData = explode("<br/>",$current);
			unlink($oid.".txt");
			$this->__subscribenewsletter($linkData[4]);
	
			if($linkData[4] == '' || $linkData[1] == '') {
	
				$linkData[0] = $_REQUEST['oid'];
				$linkData[4] = $_REQUEST['email'];
			}
	
			$existEmail = $this->User->find('first',array('conditions'=>array('User.username'=>$linkData[4])));
			if(empty($existEmail)) //Email Id was not found in DB -- Register as a client
			{

				$projectindexUrl = $this->Session->read('projectindexUrl');
				$this->Session->delete('projectindexUrl');
				$project = $this->Project->find('first', array('conditions' => array('Project.project_url' => $projectindexUrl)));


					$this->User->create();
					$saveData['User']['access_specifier'] = 'draft';
					$saveData['User']['is_approved'] = '1';
					$saveData['User']['role_id'] = '2';
                                        $saveData['User']['status'] = '1';
					$saveData['User']['username'] = $linkData[4];
					$saveData['User']['socialId'] = $oid;
					$password = $this->General->randamPassword();
					$saveData['User']['password'] = $password;//Security::hash($password, null, true);
					$saveData['User']['password2'] = Security::hash($password, null, true);

					if($this->User->save($saveData,false))
					{
						$id = $this->User->id;
						if($linkData[5]!='' || $linkData[7]!='')
						{
							$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
							if(empty($socialLink))
							{
								$socialData['Social']['user_id'] = $id;
								$socialData['Social']['created_by'] = $id;
								$socialData['Social']['social_name'] = $linkData[5];
							}
							else
							{
								$socialData['Social']['id'] = $socialLink['Social']['id'];
								$socialData['Social']['modified_by'] = $id;
								$socialData['Social']['social_name'] = $linkData[5];
							}
							$this->Social->save($socialData);
							$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
							if($linkData[7]!='')
							{
								$twittLink = explode(',',$linkData[7]);
								for($i=0;$i<count($twittLink);$i++)
								{
									$this->Social->create();
									$twitData['Social']['user_id'] = $id;
									$twitData['Social']['created_by'] = $id;
									$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
									$this->Social->save($twitData);
								}
							}
						}
						 
						$this->UserReference->create();
						$refData['UserReference']['first_name'] = $linkData[1];
						$refData['UserReference']['last_name'] = $linkData[2];
						$refData['UserReference']['user_id'] = $id;
						$refData['UserReference']['background_summary']  =  $linkData[6];
                                                $refData['UserReference']['client_linkedinurl']  =  $linkData[5];
                                                $refData['UserReference']['linkedin_headline']  = $linkData[8];
						 
						$this->UserReference->save($refData);
						$this->User->createUrlKey($id);
						if(isset($linkData[3]) && $linkData[3]!='')
						{
							$db_imgname = $linkData[1]."-".$linkData[2]."-GUILD.jpeg";
							$file_name = IMAGES . MENTORS_IMAGE_PATH . DS . $id;
							 
							$destination = IMAGES . MENTORS_IMAGE_PATH . DS . $id;
							$imagePath = $linkData[3];
							$des1 = $file_name.DS.'uploaded';
							$des2 = $file_name.DS.'small/';
							if (!is_dir($destination)) {
								mkdir($destination, 0777);
								mkdir($destination.DS.'small', 0777);
								mkdir($destination.DS.'uploaded', 0777);
							}
							if(!copy($imagePath,$file_name.DS.'uploaded'.DS.$db_imgname))
							{
								$linkData[3]="";
							}
							$cropdata = array();
							$cropdata['x'] = 0; $cropdata['y'] = 0; $cropdata['w'] = 40; $cropdata['h'] = 40; $cropdata['name'] = $db_imgname;
							$rr = $this->imageCrop($des1.DS,$cropdata,$des2,40,40);
							$rr = $this->imageCrop($des1.DS,$cropdata,$destination,200,200);
							$image_data['UserImage']['user_id'] = $id;
							$image_data['UserImage']['image_name'] = $db_imgname;
							$this->UserImage->save($image_data);
						}
						 
						$user = $this->User->findById($id);
							

						 
						if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
							$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
						else
							$this->Session->write('User.image','');
						$this->Session->write('Auth.User',$user['User']);

                                                $match = $this->Member->find('first', array('conditions'=> array('Member.member_id'=>$id)));
  		                                if(empty($match) && $id !='') 
  		                                {
  			                        $member['member_id'] = $member_id;
  			                        $this->Member->save($member);
  		                                }

				
                            //$id = $this->__registerUsingLinkedin($oid, $linkData);
                 
                            $project['ProjectMemberConfirm']['project_id'] = $project['Project']['id'];
                            $project['ProjectMemberConfirm']['member_id'] =  $id;
                            $this->ProjectMemberConfirm->save($project);

                            $member['member_id'] = $id;
  			    $this->Member->save($member);


                          //EasticEmail Integration - Begin

                           $res = "";
                           $projecttitle = $project['Project']['title'];
                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $this->Auth->user('username');
                           $subject = "Welcome to GUILD";
                           $urlkey = $user['User']['url_key'];

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("project_interest_shown_byoutsider");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_userurlkey=".urlencode($urlkey);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           $data .= "&merge_projecttitle=".urlencode($projecttitle);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End



                            


				$this->redirect(array('controller'=>'project','action'=>'index',$projectindexUrl));

                         }
                 }
			else //Email Id found in DB -- Login
			{
				if($existEmail['User']['role_id'] == 3 || $existEmail['User']['role_id']==2)
				{
					$projectindexUrl = $this->Session->read('projectindexUrl');
                                        $this->Session->delete('projectindexUrl');
					$id = $existEmail['User']['id'];

				       $project = $this->Project->find('first', array('conditions' => array('Project.project_url' => $projectindexUrl)));
                                        $project['ProjectMemberConfirm']['project_id'] = $project['Project']['id'];
                                        $project['ProjectMemberConfirm']['member_id'] =  $id;
                                        $this->ProjectMemberConfirm->save($project);

					$this->__loginUsingLink($oid, $existEmail, $linkData, $project['Project']['project_url']);
                                       $this->redirect(array('controller'=>'project','action'=>'index',$projectindexUrl));
					
					

				}
			}
	


					$this->Session->delete('projectindexUrl');

				

	              

			$this->redirect(array('controller'=>'project','action'=>'index',$projectindexUrl));
		}
		else
		{
			$this->Session->write('RedirectTo','Front');
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("The email address does not match. Please contact <a href='mailto:help@guild.im' class='delete'>help@guild.im</a>.", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
			die;
		}
	}



	function client_auth_popup(){

	
		$this->layout = 'ajax';
		$this->Session->write('popup', 1);

                if($this->Session->check('menteePopup')){
                $this->Session->delete('menteePopup');
                }

                if($this->Session->check('projectId')){
                $this->Session->delete('projectId');
                } 
                if($this->Session->check('questionId')){
                $this->Session->delete('questionId');
                } 

                if($this->Session->check('duserid')){
                $this->Session->delete('duserid');
                } 
                if($this->Session->check('login_industry')){
                $this->Session->delete('login_industry');
                } 
                if($this->Session->check('mentor')){
                $this->Session->delete('mentor');
                } 
                if($this->Session->check('mentorshipId')){
                $this->Session->delete('mentorshipId');
                } 
                if($this->Session->check('consultantPopup')){
                $this->Session->delete('consultantPopup');
                }
                if($this->Session->check('directoryclaim')){
                $this->Session->delete('directoryclaim');
                } 


                $this->Session->write('menteePopup', 1);
                $this->Session->write('planID', 1);
		if(!empty($this->data)){ //popup submit

			
				$this->__subscribenewsletter($this->data['User']['username']);
	
				$plan_id = "1";
				$projectUrl = $this->Session->read('projectUrl');

				
				$user = $this->User->find('first',array('conditions'=>array('User.username'=>$this->data['User']['username'])));
				
				if(!empty($user)) { // Already a user. 
					
					$this->Session->write('Auth', $user);
					
		                        $this->redirect(array('controller'=>'project','action'=>'index',$projectUrl));
					
				} else { // Register as a new user
					
					$activationKey = substr(md5(uniqid()), 0, 20);
   
					$this->request->data['User']['role_id'] = 3;
					$this->request->data['User']['status'] = 1;
					$this->request->data['User']['password'] = $this->data['User']['password2'];//Security::hash($this->data['User']['password2'], null, true);
                                        $this->request->data['User']['password2'] = Security::hash($this->data['User']['password2'], null, true);
					$this->request->data['User']['plan_type'] = "Basic";
					$this->request->data['User']['isPlanAdmin'] = 0;

					$this->request->data['User']['activation_key'] = $activationKey;
					$this->request->data['User']['access_specifier'] = 'publish';
				        $this->request->data['User']['is_approved'] = 1;
				        $this->request->data['User']['email_send'] = '1';
					
						
					$this->User->saveAll($this->request->data);

					if(!isset($this->data['User']['id'])){
						$id = $this->User->getLastInsertID();
					}else{
						$id = $this->data['User']['id'];
					}

                                        $refData = $this->UserReference->find('first',array('conditions'=>array('UserReference.user_id'=>$id)));

                                        $refData['UserReference']['client_companycontactname']  =  $this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'];
                                        $refData['UserReference']['client_companyaddress']  =  $this->data['User']['username'];
				        $this->User->createUrlKey($id);
				        $this->UserReference->save($refData);
					

					


						


                                          $data = $this->User->read(null,$id);
					
					       $this->Session->write('Auth', $data);						
				


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $this->data['User']['username'];
                           $subject = "Welcome to GUILD";
                           $link = SITE_URL.'clients/my_account/'.$id;

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_register");
                           $data .= "&merge_userfname=".urlencode($this->data['UserReference']['first_name']);
                           $data .= "&merge_useremail=".urlencode($this->data['User']['username']);
                           $data .= "&merge_profilelink=".urlencode($link);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End



                        $this->Project->updateAll(array('Project.user_id'=>"'".$id."'"),array('Project.project_url'=>$projectUrl));

		        $this->redirect(array('controller'=>'project','action'=>'index',$projectUrl));


				}
			}

	}




}

?>