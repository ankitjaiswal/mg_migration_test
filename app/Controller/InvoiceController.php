<?php
	/**
	* NewsLetters Controller
	*
	* PHP version 5
	*
	*/
//require('../webroot/mandrill/Mandrill.php');
require('../webroot/stripe-php-1.17.2/lib/Stripe.php');
	class InvoiceController extends AppController{
		/**
		* Controller name
		* @var string
		* @access public
		*/
		var $name = 'Invoice';
		/**
		* Models used by the Controller
		* @var array
		* @access public
		*/	
		var $uses = array('User','Mentorship','Invoice','City','UserReference','TmpInvoice','Timezone', 'EmailJobs','ReminderJobs','Plan', 'PlanUser');
		var $helpers = array('General', 'Form', 'Ajax', 'javascript', 'Paginator','Ical');
		var $components = array("Session", "Email", "Auth", "RequestHandler" ,"Upload","Cookie",'Paginator');
		
		function beforeFilter(){			
			parent::beforeFilter();	 
			$this->Auth->allow('member_create','mentee_invoice','edit_invoice','order_confirm','schedule_next','send_reminder',
					'ical','invoicelistmember','invoicelistclient','invoice_view','requestreschedule','view_pdf','invoice_create', 
					'member_invoice_preview', 'preview_pdf','send_pending_emails','send_reminder_emails','report_issue','complete_session','deletementorship','stripecheckout','thanks');
		}
		/* generate invoice number */
        
        function generateInvoice($mentor_id=null,$mentee_id=null,$mentorshipId=null)
        {
            $prgId = str_pad($mentorshipId,4,0,STR_PAD_LEFT);
            $mentshipCount = $this->Mentorship->find('count',array('conditions'=>array('Mentorship.mentor_id'=>$mentor_id,'Mentorship.mentee_id'=>$mentee_id,'Mentorship.is_deleted!=1')));
            if($mentshipCount==1)
            {
                $invNo = "IA".$prgId."-00";
            }
            if($mentshipCount>1)
            {
                $countId =  str_pad(($mentshipCount-1),2,0,STR_PAD_LEFT);
                $invNo = "IA".$prgId."-".$countId;
            }
            return $invNo;
        }
        
        
        function generateInvoiceNumber($mentorshipId=null)
        {
        	$prgId = str_pad($mentorshipId,4,0,STR_PAD_LEFT);
        	$newInvoiceCount = $this->Invoice->find('count',array('conditions'=>array('Invoice.inv_date'=>null,'Invoice.mentorship_id'=>$mentorshipId)));
        	$prgCount = str_pad($newInvoiceCount,2,0,STR_PAD_LEFT);
        	
        	$invNo = "IN".$prgId."-".$prgCount;
        	return $invNo;
        }
       
		/* Function for create new invoice from mentor side */
        function member_create($mentorship_id = null)

		{
		   $invOldData = $this->Invoice->find('first',array('conditions'=>array('Invoice.mentorship_id'=>$mentorship_id)));
           if(!empty($invOldData))
           {
               $this->redirect(array('controller'=>'users','action'=>'my_account'));
           }           
		    $userId = $this->Session->read('Auth.User.id');
			if(!empty($this->data))
			{
			    $invNo = $this->generateInvoice($userId,$this->data['Invoice']['mentee_id'],$this->data['Invoice']['mentorship_id']);
			    //$invNo = 'Invoice-'.$userId.'-'.$this->data['Invoice']['mentee_id'].'-'.$this->data['Invoice']['mentorship_id'];
			    //$this->data['Invoice']['title']  = "Initial consultation";
			    $this->request->data['Invoice']['inv_time']  = $this->data['Invoice']['time_hh'].':'.$this->data['Invoice']['time_mm'].':'.$this->data['Invoice']['time_AM_PM'];
				
				if($this->data['Invoice']['inv_datey']!="" && $this->data['Invoice']['time_hh1']!="" && $this->data['Invoice']['time_mm1']!="")
                {
                    $this->request->data['Invoice']['inv_time1']  = $this->data['Invoice']['time_hh1'].':'.$this->data['Invoice']['time_mm1'].':'.$this->data['Invoice']['time_AM_PM1'];
                    $this->request->data['Invoice']['inv_date1']  = strtotime($this->data['Invoice']['inv_datey']);
                }
                if($this->data['Invoice']['inv_dateZ']!="" && $this->data['Invoice']['time_hh2']!="" && $this->data['Invoice']['time_mm2']!="")
                {
                    $this->request->data['Invoice']['inv_time2']  = $this->data['Invoice']['time_hh2'].':'.$this->data['Invoice']['time_mm2'].':'.$this->data['Invoice']['time_AM_PM2'];
                    $this->request->data['Invoice']['inv_date2']  = strtotime($this->data['Invoice']['inv_dateZ']);
                }
                
				//$this->data['Invoice']['inv_date']  = strtotime($this->data['Invoice']['inv_date']);
                              $this->request->data['Invoice']['inv_date']  = strtotime($this->data['Invoice']['inv_datex']);
                $this->request->data['Invoice']['inv_timezone']  = $this->data['Invoice']['timezone'];
                
                $this->request->data['Invoice']['inv_no']     = $invNo;
                
                if($this->data['Invoice']['pay_in_advance'] == 1) {
	                
                	$this->request->data['Invoice']['discount']  = $this->data['Invoice']['ment_discounts'];
	                $this->request->data['Invoice']['tax']       = $this->data['Invoice']['ment_taxes'];
	                
	                $this->request->data['Invoice']['price']     = $this->data['Invoice']['ment_price'];
	                $this->request->data['Invoice']['total']     = (($this->data['Invoice']['ment_price']-$this->data['Invoice']['ment_discounts'])+$this->data['Invoice']['ment_taxes']);
	                
	                if($this->data['Invoice']['price']!=0)
	                    $this->request->data['Invoice']['rate']      = $this->data['Invoice']['ment_rate'];
	                else
	                    $this->request->data['Invoice']['rate']      = 0;
                    
                }
                $this->request->data['Invoice']['inv_client_id'] = $this->data['Invoice']['mentee_id'];
                
                $this->Invoice->save($this->request->data);
                
                //Updating Current credit for plan
                $plan_user = $this->PlanUser->find('first',array('conditions'=>array('PlanUser.user_id'=>$this->data['Invoice']['mentee_id'])));
                
                if(!empty($plan_user))
                	$plan = $this->Plan->find('first',array('conditions'=>array('Plan.id'=>$plan_user['PlanUser']['plan_id'])));
                
                $plan['Plan']['current_credit'] = $plan['Plan']['current_credit'] - $this->data['Invoice']['ment_discounts'];
                
                $this->Plan->save($plan['Plan']);
                //Plan Updated
                
                /* update time zone save for user */ 
                $this->__updatetimezone($this->data['Invoice']['timezone']);
                
                $this->Mentorship->bindModel(
                        array(
                        'belongsTo' => array(
                            'User' => array('className' => 'User','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=User.id'),'fields'=>array('User.username')),
                            'Mentor' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=Mentor.user_id'),'fields'=>array('Mentor.first_name','Mentor.last_name')),
                            'Mentee' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),'fields'=>array('Mentee.first_name','Mentee.last_name')),
                            'Invoice' => array('className' => 'Invoice','foreignKey' => false,'conditions'=>array('Mentorship.id=Invoice.mentorship_id'),'fields'=>array('Invoice.id')),
                            )));
                $this->Mentorship->recursive = '0';
                $mentorData = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.id'=>$this->data['Invoice']['mentorship_id'])));

                /* Acceptance mail for mentee from mentor */
               
                /*$this->Email->to = $mentorData['User']['username'];
                $this->Email->from = $this->Session->read('Auth.User.username');
                $this->set('data',$mentorData);
                $this->Email->subject = 'Mentorship application accepted';
                $this->Email->template = 'mentor_accept';
                $this->Email->sendAs = 'html';
                $this->Email->send();*/
                


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $mentorData['User']['username'];
                           $subject = "Consultation request accepted";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentor_accept");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_mentorlname=".urlencode($mentorData['Mentor']['last_name']);
                           $data .= "&merge_invoiceid=".urlencode($mentorData['Invoice']['id']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

                
                $this->Mentorship->id = $this->data['Invoice']['mentorship_id'];
                $this->Mentorship->saveField('applicationType', '3');
                $this->Session->setFlash(__("Appointment proposed", true), 'default', array('class' => 'success'));
                $this->redirect(array('controller'=>'users','action'=>'my_account'));
			}
			else {
					$this->Mentorship->recursive = '0';
					$mentorshipData = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.id'=>$mentorship_id,'Mentorship.mentor_id'=>$userId)));
					if(empty($mentorshipData))
					{
					    $this->Session->setFlash(__("You are trying to access invalid data.", true), 'default', array('class' => 'success'));
						$this->redirect(array('controller'=>'users','action'=>'my_account'));
					}
					
					$this->Invoice->bindModel(
						array(
						'belongsTo' => array(
							'Mentorship' => array('className' => 'Mentorship','foreignKey' => 'mentorship_id'),
							'UserReference' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=UserReference.user_id')),
							'City' => array('className' => 'City','foreignKey' => false,'conditions'=>array('UserReference.zipcode=City.zip_code'),
						 	))));
					$fields = array('Invoice.*','Mentorship.mentor_id','Mentorship.mentee_id','UserReference.user_id','UserReference.first_name','UserReference.last_name','UserReference.zipcode','UserReference.fee_regular_session','UserReference.fee_first_hour','City.city_name','City.state');
					$invoiceData = $this->Invoice->find('first',array('conditions'=>array('Invoice.mentorship_id'=>$mentorship_id),'fields'=>$fields));
					$this->set('invoiceData',$invoiceData);					
					$this->set('mentorship_id',$mentorship_id);
                    $timezoneData = $this->Timezone->find('list',array('order'=>'id asc'));
                    $this->set('timezoneData',$timezoneData);
                    
                    //Getting discount values
                  /*  $plan_user = $this->PlanUser->find('first',array('conditions'=>array('PlanUser.user_id'=>$mentorshipData['Mentorship']['mentee_id'])));
                    	
                    if(!empty($plan_user))
                    	$plan = $this->Plan->find('first',array('conditions'=>array('Plan.id'=>$plan_user['PlanUser']['plan_id'])));
                    	
                    if(empty($plan))
                    	$discount_val = 0;
                    else {
                    		
                    	$this->User->Behaviors->attach('Containable');
                    	$client = $this->User->find('first',
                    			array(
                    					'contain'=>array(
                    							'UserReference'=>array(
                    									'fields'=>array('first_name','last_name'),
                    							),
                    					),
                    					'conditions'=>array('User.id'=>$mentorshipData['Mentorship']['mentee_id'])
                    			)
                    	
                    	);
                    	
                    	if( $client['User']['plan_type'] == 'Basic')
                    		$discount_val = 0;
                    	else if( $client['User']['plan_type'] == 'Pro')
                    		$discount_val = 30 * $invoiceData['UserReference']['fee_regular_session'] / 100;
                    	else
                    		$discount_val = 50 * $invoiceData['UserReference']['fee_regular_session'] / 100;
                    		
                    	if ($discount_val > $plan['Plan']['current_credit'])
                    		$discount_val = $plan['Plan']['current_credit'];
                    }
                    $this->set('discount_val',$discount_val);*/
			}
		}
		
		function invoice_create($mentorship_id = null,$client_id = null)
		{
			if(!empty($this->data))
			{
				$userId = $this->Session->read('Auth.User.id');
				
				$invNo = $this->generateInvoiceNumber($this->data['Invoice']['mentorship_id']);
				
				$this->request->data['Invoice']['inv_client_id'] = $this->data['Invoice']['mentee_id'];
				
				$this->request->data['Invoice']['discount']  = $this->data['Invoice']['ment_discount'];
				$this->request->data['Invoice']['tax']       = $this->data['Invoice']['ment_tax'];
		
				$this->request->data['Invoice']['price']     = $this->data['Invoice']['ment_price'];
				$this->request->data['Invoice']['total']     = (($this->data['Invoice']['ment_price']-$this->data['Invoice']['ment_discount'])+$this->data['Invoice']['ment_tax']);
				$this->request->data['Invoice']['inv_no']     = $invNo;
				if($this->data['Invoice']['price']!=0)
					$this->request->data['Invoice']['rate']      = $this->data['Invoice']['ment_rate'];
				else
					$this->request->data['Invoice']['rate']      = 0;
		
				$this->Invoice->save($this->request->data);
				
				//Updating Current credit for plan
				$plan_user = $this->PlanUser->find('first',array('conditions'=>array('PlanUser.user_id'=>$this->data['Invoice']['mentee_id'])));
				
				if(!empty($plan_user))
					$plan = $this->Plan->find('first',array('conditions'=>array('Plan.id'=>$plan_user['PlanUser']['plan_id'])));
				
				$plan['Plan']['current_credit'] = $plan['Plan']['current_credit'] - $this->data['Invoice']['ment_discount'];
				
				$this->Plan->save($plan['Plan']);
				//Plan Updated
				
				$invoice_id = $this->Invoice->id;
		
				$this->Mentorship->bindModel(
						array(
								'belongsTo' => array(
										'User' => array('className' => 'User','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=User.id'),'fields'=>array('User.username')),
										'Mentor' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=Mentor.user_id'),'fields'=>array('Mentor.first_name','Mentor.last_name')),
										'Mentee' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),'fields'=>array('Mentee.first_name','Mentee.last_name')),
										'Invoice' => array('className' => 'Invoice','foreignKey' => false,'conditions'=>array('Mentorship.id=Invoice.mentorship_id'),'fields'=>array('Invoice.id')),
								)));
				$this->Mentorship->recursive = '0';
				$mentorData = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.id'=>$this->data['Invoice']['mentorship_id'])));
				
				


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $mentorData['User']['username'];
                           $subject = "Invoice received";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("invoice_sent");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_mentorlname=".urlencode($mentorData['Mentor']['last_name']);
                           $data .= "&merge_invoiceid=".urlencode($invoice_id);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
		
				$this->Session->setFlash(__("Invoice sent", true), 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'users','action'=>'my_account'));
			}
			else {
				if($client_id != null) {
					//Get client user
					$this->User->Behaviors->attach('Containable');
					$client = $this->User->find('first',
						array(
								'contain'=>array(
										'UserReference'=>array(
												'fields'=>array('first_name','last_name'),
										),
								),
								'conditions'=>array('User.id'=>$client_id)
						)
				
				);
				}
				
				//Get Member user
				$this->User->Behaviors->attach('Containable');
				$member = $this->User->find('first',
						array(
								'contain'=>array(
										'UserReference'=>array(
												'fields'=>array('first_name','last_name','fee_first_hour','fee_regular_session','regular_session_per'),
										),
								),
								'conditions'=>array('User.id'=>$this->Auth->user('id'))
						)
				
				);
				
				
				//Getting discount values
				/*$plan_user = $this->PlanUser->find('first',array('conditions'=>array('PlanUser.user_id'=>$client_id)));
				
				if(!empty($plan_user))
					$plan = $this->Plan->find('first',array('conditions'=>array('Plan.id'=>$plan_user['PlanUser']['plan_id'])));
				
				if(empty($plan))
					$discount_val = 0;
				else {
				
					if( $client['User']['plan_type'] == 'Basic')
						$discount_val = 0;
					else if( $client['User']['plan_type'] == 'Pro')
						$discount_val = 30 * $member['UserReference']['fee_regular_session'] / 100;
					else
						$discount_val = 50 * $member['UserReference']['fee_regular_session'] / 100;
					
					if ($discount_val > $plan['Plan']['current_credit'])
						$discount_val = $plan['Plan']['current_credit'];
				}
				
				$this->set('discount_val',$discount_val);*/
				$this->set('clientData',$client);
				$this->set('memberData',$member);
				$this->set('mentorship_id', $mentorship_id);
			}
		}
		
        /* Function for view ,comment & reschedule invoice on mentee side */
		function mentee_invoice($invoiceId = null){
		    $userId = $this->Session->read('Auth.User.id');
		    if(!empty($this->data))
            {
               $this->Invoice->id = $this->data['Invoice']['id'];
               $this->Invoice->saveField('comment', $this->data['Invoice']['comment']);     
               if($this->data['Invoice']['clickValue']=='rechedule')
               {
                 $this->Mentorship->id = $this->data['Invoice']['mentorship_id'];
                 $this->Mentorship->saveField('applicationType', '4');
                 
                 $this->Mentorship->bindModel(
                        array(
                        'belongsTo' => array(
                            'User' => array('className' => 'User','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=User.id'),'fields'=>array('User.username')),
                            'Mentor' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=Mentor.user_id'),'fields'=>array('Mentor.first_name','Mentor.last_name')),
                            'Mentee' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),'fields'=>array('Mentee.first_name','Mentee.last_name')),
                            )));
                $this->Mentorship->recursive = '0';
                $mentorData = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.id'=>$this->data['Invoice']['mentorship_id'])));
                
                /* Reschedule request mail for mentor */
               
                /*$this->Email->to = $mentorData['User']['username'];
                $this->Email->from = $this->Session->read('Auth.User.username');
                $this->set('data',$mentorData);
                $this->Email->subject = 'Request to reschedule';
                $this->Email->template = 'mentee_reschedule';
                $this->Email->sendAs = 'html';
                $this->Email->send();*/
                

                
                 
			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $mentorData['User']['username'];
                           $subject = "Request to reschedule";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_reschedule");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_clientlname=".urlencode($mentorData['Mentee']['last_name']);
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_mentorshipid=".urlencode($mentorData['Invoice']['id']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
   
                $this->redirect(array('controller'=>'clients','action'=>'my_account'));    
               }
               if($this->data['Invoice']['clickValue']=='confirm')
               {
                  $this->redirect(array('action'=>'order_confirm/'.$this->data['Invoice']['id']));  
               }
            }
            $this->Invoice->bindModel(
                        array(
                        'belongsTo' => array(
                            'Mentorship' => array(
                                'className' => 'Mentorship',
                                'foreignKey' => 'mentorship_id',
                                'fields'=>array('Mentorship.mentor_id','Mentorship.mentee_id'),
                            ),
                            'Mentee' => array(
                                'className' => 'UserReference',
                                'foreignKey' => false,
                                'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),
                                'fields'=>array('Mentee.first_name','Mentee.last_name'),
                            ))));
                            
            $data= $this->Invoice->find('first',array('conditions'=>array('Invoice.id'=>$invoiceId,'Mentorship.mentee_id'=>$userId,'Mentorship.is_deleted'=>'0')));
            if(empty($data))
              $this->redirect(array('controller'=>'fronts','action' => 'index'));
            else
               $this->set('data',$data);
		}

        /* Function for edit invoice from mentor side */
        function edit_invoice($invoiceId=null)
        {
           $userId = $this->Session->read('Auth.User.id');
           if(!empty($this->data))
           {
             
                if($this->data['Invoice']['clickValue']=='edit')
                {
                    $this->request->data['Invoice']['inv_time']  = $this->data['Invoice']['time_hh'].':'.$this->data['Invoice']['time_mm'].':'.$this->data['Invoice']['time_AM_PM'];
                   if($this->data['Invoice']['inv_datey']!="" && $this->data['Invoice']['time_hh1']!="" && $this->data['Invoice']['time_mm1']!="")
                    {
                        $this->request->data['Invoice']['inv_time1']  = $this->data['Invoice']['time_hh1'].':'.$this->data['Invoice']['time_mm1'].':'.$this->data['Invoice']['time_AM_PM1'];
                        $this->request->data['Invoice']['inv_date1']  = strtotime($this->data['Invoice']['inv_datey']);
                    }
                    if($this->data['Invoice']['inv_datez']!="" && $this->data['Invoice']['time_hh2']!="" && $this->data['Invoice']['time_mm2']!="")
                    {
                        $this->request->data['Invoice']['inv_time2']  = $this->data['Invoice']['time_hh2'].':'.$this->data['Invoice']['time_mm2'].':'.$this->data['Invoice']['time_AM_PM2'];
                        $this->request->data['Invoice']['inv_date2']  = strtotime($this->data['Invoice']['inv_datez']);
                    }                   
                    $this->request->data['Invoice']['inv_date']  = strtotime($this->data['Invoice']['inv_datex']);
                    $this->request->data['Invoice']['inv_timezone']  = $this->data['Invoice']['timezone'];
                    
                    if($this->data['Invoice']['pay_in_advance'] == 1) {
	                    $this->request->data['Invoice']['discount']  = $this->data['Invoice']['ment_discounts'];
	                    $this->request->data['Invoice']['tax']       = $this->data['Invoice']['ment_taxes'];
	                    $this->request->data['Invoice']['rate']      = $this->data['Invoice']['ment_rate'];
	                    $this->request->data['Invoice']['price']     = $this->data['Invoice']['ment_price'];
	                    $this->request->data['Invoice']['total']     = (($this->data['Invoice']['ment_price']-$this->data['Invoice']['ment_discounts'])+$this->data['Invoice']['ment_taxes']);
	                    
	                    if($this->data['Invoice']['price']!=0)
	                        $this->request->data['Invoice']['rate']      = $this->data['Invoice']['ment_rate'];
	                    else
	                        $this->request->data['Invoice']['rate']      = 0;
                    }
                    $this->Invoice->save($this->request->data);
                    
                    //Updating Current credit for plan
                    $plan_user = $this->PlanUser->find('first',array('conditions'=>array('PlanUser.user_id'=>$this->data['Invoice']['mentee_id'])));
                    
                    if(!empty($plan_user))
                    	$plan = $this->Plan->find('first',array('conditions'=>array('Plan.id'=>$plan_user['PlanUser']['plan_id'])));
                    
                    $plan['Plan']['current_credit'] = $plan['Plan']['current_credit'] - $this->data['Invoice']['ment_discounts'];
                    
                    $this->Plan->save($plan['Plan']);
                    //Plan Updated
                    
                    
                    /* update time zone save for user */ 
                    $this->__updatetimezone($this->data['Invoice']['timezone']);
                                       
                    $this->Session->setFlash(__("Appointment proposed", true), 'default', array('class' => 'success'));
                }
                $this->Mentorship->bindModel(
                        array(
                        'belongsTo' => array(
                            'User' => array('className' => 'User','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=User.id'),'fields'=>array('User.username')),
                            'Mentor' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=Mentor.user_id'),'fields'=>array('Mentor.first_name','Mentor.last_name')),
                            'Mentee' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),'fields'=>array('Mentee.first_name','Mentee.last_name')),
                            'Invoice' => array('className' => 'Invoice','foreignKey' => false,'conditions'=>array('Mentorship.id=Invoice.mentorship_id'),'fields'=>array('Invoice.id')),
                            )));
                $this->Mentorship->recursive = '0';
                $mentorData = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.id'=>$this->data['Invoice']['mentorship_id'])));
                
                if($this->data['Invoice']['clickValue']=='deny')
                {
                    /* Reschedule request denied from mentor side */
               
                    /*$this->Email->to = $mentorData['User']['username'];
                    $this->Email->from = $this->Session->read('Auth.User.username');
                    $this->set('data',$mentorData);
                    $this->Email->subject = 'Reschedule request denied';
                    $this->Email->template = 'deny_reschedule_request';
                    $this->Email->sendAs = 'html';
                    $this->Email->send();*/
                    

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $mentorData['User']['username'];
                           $subject = "Request to reschedule";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("deny_reschedule_request");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_mentorlname=".urlencode($mentorData['Mentor']['last_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
                    
                     $this->Session->setFlash(__("Appointment proposed", true), 'default', array('class' => 'success'));
                }
                
               
                /* Reschedule request send for mentee */
           
                /*$this->Email->to = $mentorData['User']['username'];
                $this->Email->from = $this->Session->read('Auth.User.username');
                $this->set('data',$mentorData);
                $this->Email->subject = 'Mentorship session scheduled';
                $this->Email->template = 'schedule_next';
                $this->Email->sendAs = 'html';
                $this->Email->send(); */


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $mentorData['User']['username'];
                           $subject = "Consultation proposed";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("schedule_next");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_mentorlname=".urlencode($mentorData['Mentor']['last_name']);
                           $data .= "&merge_invoiceid=".urlencode($mentorData['Invoice']['id']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
                
                
                $this->Mentorship->id = $this->data['Invoice']['mentorship_id'];
                $this->Mentorship->saveField('applicationType', '3');
                $this->redirect(array('controller'=>'users','action'=>'my_account'));
           }
           $this->Invoice->bindModel(
                        array(
                        'belongsTo' => array(
                            'Mentorship' => array(
                                'className' => 'Mentorship',
                                'foreignKey' => 'mentorship_id',
                                'fields'=>array('Mentorship.mentor_id','Mentorship.mentee_id'),
                            ),
                            'Mentee' => array(
                                'className' => 'UserReference',
                                'foreignKey' => false,
                                'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),
                                'fields'=>array('Mentee.first_name','Mentee.last_name'),
                            ))));
                            
            $data= $this->Invoice->find('first',array('conditions'=>array('Invoice.id'=>$invoiceId,'Mentorship.mentor_id'=>$userId,'Mentorship.is_deleted'=>'0','Invoice.inv_create_status!=1')));
            if(empty($data))
              $this->redirect(array('controller'=>'fronts','action' => 'index'));
            else
            {
               $this->set('data',$data);
               $timezoneData = $this->Timezone->find('list',array('order'=>'name asc'));
               $this->set('timezoneData',$timezoneData);
            }  
        }

        function order_confirm($invoiceId=null)
        {
           
           $inv = $this->Invoice->find('first',array('conditions'=>array('Invoice.id'=>$invoiceId)));
           $userId = $inv['Invoice']['inv_client_id'];
           $client = $this->User->find('first',array('conditions'=>array('User.id'=>$userId)));
           $email = $client['User']['username'];
           if($this->Auth->user('id') == ""){
           $this->Session->write('myaccount',$email);
           $this->Session->write('menteeActEmail',$email);
           $this->set('invoice_Id',$invoiceId);
           $this->Session->write('invoice_Id',$invoiceId);
		   $this->redirect(array('controller'=>'fronts','action' => 'index'));
		       }
		   
		            if(!empty($this->data))
           {
               $this->Invoice->id = $this->data['Invoice']['id'];
               if($this->data['Invoice']['clickValue']=='rechedule')
               {
               	$this->Invoice->saveField('comment', $this->data['Invoice']['comment']);
                 $this->Mentorship->id = $this->data['Invoice']['mentorship_id'];
                 $this->Mentorship->saveField('applicationType', '4');
                 
                 $this->Mentorship->bindModel(
                        array(
                        'belongsTo' => array(
                            'User' => array('className' => 'User','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=User.id'),'fields'=>array('User.username')),
                            'Mentor' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=Mentor.user_id'),'fields'=>array('Mentor.first_name','Mentor.last_name')),
                            'Mentee' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),'fields'=>array('Mentee.first_name','Mentee.last_name')),
                            'Invoice' => array('className' => 'Invoice','foreignKey' => false,'conditions'=>array('Mentorship.id=Invoice.mentorship_id'),'fields'=>array('Invoice.id')),
                            )));
                $this->Mentorship->recursive = '0';
                $mentorData = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.id'=>$this->data['Invoice']['mentorship_id'])));
                
                         /* Email to consultant to reschedule meeting*/

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $mentorData['User']['username'];
                           $subject = "Request to reschedule appointment";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_reschedule");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_clientlname=".urlencode($mentorData['Mentee']['last_name']);
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_mentorshipid=".urlencode($mentorData['Invoice']['id']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
                
                $this->Session->setFlash(__("Reschedule request sent", true), 'default', array('class' => 'success'));
				if($this->Session->read('Auth.User.role_id') == 2)
                	$this->redirect(array('controller'=>'users','action'=>'my_account'));  
				else  
					$this->redirect(array('controller'=>'clients','action'=>'my_account'));
               }  
               if($this->data['Invoice']['clickValue']=='complete')
               {
               		$this->Invoice->saveField('comment', $this->data['Invoice']['comment']);
                   $invTimeData = $this->Invoice->find('first',array('conditions'=>array('Invoice.id'=>$this->data['Invoice']['id']),'fields'=>array('Invoice.inv_time','Invoice.inv_time1','Invoice.inv_time2','Invoice.inv_date1','Invoice.inv_date2')));
                   $this->Invoice->id = $this->data['Invoice']['id']; 
                   if($this->data['Invoice']['inv_time_choose']=='2')
                   {
                       //$this->Invoice->saveField(array('inv_time'=>$invTimeData['Invoice']['inv_time1'],'inv_date'=>$invTimeData['Invoice']['inv_date1']));
                      $this->Invoice->saveField('inv_time', $invTimeData['Invoice']['inv_time1']);   
                      $this->Invoice->saveField('inv_date', $invTimeData['Invoice']['inv_date1']);   
                   }
                   if($this->data['Invoice']['inv_time_choose']=='3')
                   {
                       $this->Invoice->saveField('inv_time', $invTimeData['Invoice']['inv_time2']);
                       $this->Invoice->saveField('inv_date', $invTimeData['Invoice']['inv_date2']);     
                   } 
                    $this->Mentorship->id = $this->data['Invoice']['mentorship_id'];
                    $this->Mentorship->saveField('applicationType', '7');
                    $this->Invoice->id = $this->data['Invoice']['id'];
                    $this->Invoice->saveField('inv_create_status', '1');
                     
                    $this->Mentorship->bindModel(
                            array(
                            'belongsTo' => array(
                                'User' => array('className' => 'User','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=User.id'),'fields'=>array('User.username')),
                                'Mentor' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=Mentor.user_id'),'fields'=>array('Mentor.first_name','Mentor.last_name')),
                                'Mentee' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),'fields'=>array('Mentee.first_name','Mentee.last_name')),
                                'Invoice' => array('className' => 'Invoice','foreignKey' => false,'conditions'=>array('Mentorship.id=Invoice.mentorship_id')),
                                )));
                    $this->Mentorship->recursive = '0';
                    $mentorData = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.id'=>$this->data['Invoice']['mentorship_id'])));
                    /* Orientation confirmation mail for mentor */
                   
                    $this->save_pending_emails($mentorData);
                    $this->save_reminder_emails($mentorData);

                    
                    $timeData = $this->General->timeformatchange($mentorData['Invoice']['inv_time'], $mentorData['Invoice']['duration_hours'], $mentorData['Invoice']['duration_minutes']);
                    $timeZoneValue = $this->General->getTimeZoneById($mentorData['Invoice']['inv_timezone']);
                    
                    if($mentorData['Invoice']['inv_mode']=='1')
                    	$mode = 'Face to face';
                    if($mentorData['Invoice']['inv_mode']=='2')
                    	$mode = 'Online';
                    if($mentorData['Invoice']['inv_mode']=='3')
                    	$mode = 'Phone';
                     

                     
                    if($mentorData['Invoice']['inv_mode']=='1')
                    {


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $date = date('m/d/Y',$mentorData['Invoice']['inv_date'])."(".date('l',$mentorData['Invoice']['inv_date']).")";
                           $to = $mentorData['User']['username'];
                           $subject = "Consultation confirmed";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("orientation_confirm");
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_invoiceid=".urlencode($mentorData['Invoice']['id']);
                           $data .= "&merge_invoicedate=".urlencode($date);
                           $data .= "&merge_invoicetime=".urlencode($timeData);
                           $data .= "&merge_modemethod=".urlencode($mode);
                           $data .= "&merge_modevalue=".urlencode($mentorData['Invoice']['mode_text']);
                           $data .= "&merge_timezone=".urlencode($timeZoneValue);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


                    } else{

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $date = date('m/d/Y',$mentorData['Invoice']['inv_date'])."(".date('l',$mentorData['Invoice']['inv_date']).")";
                           $to = $mentorData['User']['username'];
                           $subject = "Consultation confirmed";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("orientation_confirm_no_map");
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_invoiceid=".urlencode($mentorData['Invoice']['id']);
                           $data .= "&merge_invoicedate=".urlencode($date);
                           $data .= "&merge_invoicetime=".urlencode($timeData);
                           $data .= "&merge_modemethod=".urlencode($mode);
                           $data .= "&merge_modevalue=".urlencode($mentorData['Invoice']['mode_text']);
                           $data .= "&merge_timezone=".urlencode($timeZoneValue);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


                    }

                    
                    $this->Session->setFlash(__("Order confirmed", true), 'default', array('class' => 'success'));
                    
                     $timeZoneValue = $this->General->getTimeZoneById($mentorData['Invoice']['inv_timezone']);
                    

                     
                    if($mentorData['Invoice']['inv_mode']=='1')
                    {

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $date = date('m/d/Y',$mentorData['Invoice']['inv_date'])."(".date('l',$mentorData['Invoice']['inv_date']).")";
                           $to = $this->Session->read('Auth.User.username');
                           $subject = "Consultation confirmed";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("orientation_confirm_mentee");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_invoiceid=".urlencode($mentorData['Invoice']['id']);
                           $data .= "&merge_invoicedate=".urlencode($date);
                           $data .= "&merge_invoicetime=".urlencode($timeData);
                           $data .= "&merge_modemethod=".urlencode($mode);
                           $data .= "&merge_modevalue=".urlencode($mentorData['Invoice']['mode_text']);
                           $data .= "&merge_timezone=".urlencode($timeZoneValue);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


                    } else{

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $date = date('m/d/Y',$mentorData['Invoice']['inv_date'])."(".date('l',$mentorData['Invoice']['inv_date']).")";
                           $to = $this->Session->read('Auth.User.username');
                           $subject = "Consultation confirmed";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("orientation_confirm_mentee_no_map");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_invoiceid=".urlencode($mentorData['Invoice']['id']);
                           $data .= "&merge_invoicedate=".urlencode($date);
                           $data .= "&merge_invoicetime=".urlencode($timeData);
                           $data .= "&merge_modemethod=".urlencode($mode);
                           $data .= "&merge_modevalue=".urlencode($mentorData['Invoice']['mode_text']);
                           $data .= "&merge_timezone=".urlencode($timeZoneValue);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


                    }

                    
                    $this->Session->setFlash(__("Order confirmed", true), 'default', array('class' => 'success'));
                    if($this->Session->read('Auth.User.role_id') == 2)
	                	$this->redirect(array('controller'=>'users','action'=>'my_account'));  
					else  
						$this->redirect(array('controller'=>'clients','action'=>'my_account'));  
               }  


           }
           $this->Invoice->bindModel(
                        array(
                        'belongsTo' => array(
                            'Mentorship' => array(
                                'className' => 'Mentorship',
                                'foreignKey' => 'mentorship_id',
                                'fields'=>array('Mentorship.mentor_id','Mentorship.mentee_id',),
                            ),
                            'Mentee' => array(
                                'className' => 'UserReference',
                                'foreignKey' => false,
                                'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),
                                'fields'=>array('Mentee.first_name','Mentee.last_name'),
                            ),
                             'Mentor' => array(
                                'className' => 'UserReference',
                                'foreignKey' => false,
                                'conditions'=>array('Mentorship.mentor_id=Mentor.user_id'),
                                'fields'=>array('Mentor.first_name','Mentor.last_name'),
                            ),
                            'User' => array(
                                'className' => 'User',
                                'foreignKey' => false,
                                'conditions'=>array('Mentorship.mentor_id=User.id'),
                                'fields'=>array('User.timezone'),
                            ))));
                            
            $data= $this->Invoice->find('first',array('conditions'=>array('Invoice.id'=>$invoiceId,'Mentorship.mentee_id'=>$userId,'Mentorship.is_deleted'=>'0','Invoice.inv_create_status'=>'0')));
            if(empty($data))
              $this->redirect(array('controller'=>'fronts','action' => 'index'));
            else
               $this->set('data',$data);
        }

        /* function for send reminder from mentee or mentor user */
        function send_reminder($mentorship_id=null,$from=null,$email_type=null)
        {
            $userId = $this->Session->read('Auth.User.id');
            if(empty($mentorship_id) ||empty($from))
            {
                $this->redirect(array('controller'=>'fronts','action'=>'index'));
            }
            else
            {
                if($from=='client')
                {
                    /* send reminder mail from mentee for mentor*/
                    $this->Mentorship->bindModel(
                            array(
                            'belongsTo' => array(
                                'User' => array('className' => 'User','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=User.id'),'fields'=>array('User.username')),
                                'Mentor' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=Mentor.user_id'),'fields'=>array('Mentor.first_name','Mentor.last_name')),
                                'Mentee' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),'fields'=>array('Mentee.first_name','Mentee.last_name')),
                                )));
                    $this->Mentorship->recursive = '0';
                    $mentorData = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.id'=>$mentorship_id,'Mentorship.mentee_id'=>$userId,'Mentorship.is_deleted!=1')));
                    /*if($email_type=='application'){$this->set('email_type','application');}
                     if($email_type=='reschedule'){$this->set('email_type','reschedule request');}
                    $this->Email->to = $mentorData['User']['username'];
                    $this->Email->from = $this->Session->read('Auth.User.username');
                    $this->set('data',$mentorData);
                    $this->Email->subject = 'Reminder';
                    $this->Email->template = 'send_reminder_mentor';
                    $this->Email->sendAs = 'html';
                    $this->Email->send();*/
                    
                    
                    if($email_type=='reschedule'){



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $mentorData['User']['username'];
                           $subject = "Reminder: Request to reschedule";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("send_reminder_mentor_reshedule_request");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_clientlname=".urlencode($mentorData['Mentee']['last_name']);
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


                    }
                    
                    if($email_type=='application'){
  

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $mentorData['User']['username'];
                           $subject = "Reminder: Session proposed";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("send_reminder_mentor_orientation");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_clientlname=".urlencode($mentorData['Mentee']['last_name']);
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
                    
                    }
                    //Mandrill Integration - End
                    $this->Session->write('hide_reminder_link',$mentorship_id);
                    $this->Session->setFlash(__("Reminder sent", true), 'default', array('class' => 'success'));
                    $this->redirect($this->referer());
					
				}   
                if($from=='member')
                {
                    /* send reminder mail from mentor for mentee*/
                    $this->Mentorship->bindModel(
                            array(
                            'belongsTo' => array(
                                'User' => array('className' => 'User','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=User.id'),'fields'=>array('User.username')),
                                'Mentor' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=Mentor.user_id'),'fields'=>array('Mentor.first_name','Mentor.last_name')),
                                'Mentee' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),'fields'=>array('Mentee.first_name','Mentee.last_name')),
                                )));
                    $this->Mentorship->recursive = '0';
                    $mentorData = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.id'=>$mentorship_id,'Mentorship.mentor_id'=>$userId,'Mentorship.is_deleted!=1')));
                    /*if($email_type=='orientation'){$this->set('email_type','orientation');}
                    
                    $this->Email->to = $mentorData['User']['username'];
                    $this->Email->from = $this->Session->read('Auth.User.username');
                    $this->set('data',$mentorData);
                    $this->Email->subject = 'Reminder';
                    $this->Email->template = 'send_reminder_mentee';
                    $this->Email->sendAs = 'html';
                    $this->Email->send();*/
                    

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $mentorData['User']['username'];
                           $subject = "Reminder: Session proposed";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("send_reminder_mentee");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_mentorlname=".urlencode($mentorData['Mentor']['last_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
                     
                    //Mandrill Integration - End
                    $this->Session->write('hide_reminder_link',$mentorship_id);
                    $this->Session->setFlash(__("Reminder sent", true), 'default', array('class' => 'success'));
                   // $this->redirect(array('controller'=>'users','action'=>'my_account'));
                   $this->redirect($this->referer()); 
					
				} 
            }
    
            
        }

  /* function for report issue from mentee or mentor user */
        function report_issue($mentorship_id=null,$from=null)
        {
			$this->layout = 'ajax';
            if(empty($mentorship_id) ||empty($from))
            {
                $this->redirect(array('controller'=>'fronts','action'=>'index'));
            }
            if(!empty($this->data))
            {
                 /* report issue from mentee or mentor*/
                $this->Mentorship->bindModel(
                        array(
                        'belongsTo' => array(
                            'User' => array('className' => 'User','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=User.id'),'fields'=>array('User.username')),
                            'Mentor' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=Mentor.user_id'),'fields'=>array('Mentor.first_name','Mentor.last_name')),
                            'Mentee' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),'fields'=>array('Mentee.first_name','Mentee.last_name')),
                            )));
                $this->Mentorship->recursive = '0';
                $mentorData = $this->Mentorship->findById($mentorship_id);
                /*$this->Email->to = Configure::read('Site.email');
                $this->Email->from = $this->Session->read('Auth.User.username');
                $this->set('from',$from);
                $this->set('data',$mentorData);
				$this->set('comment',$this->data['Invoice']['comment']);
                $this->Email->subject = 'Report issue';
                $this->Email->template = 'report_issue';
                $this->Email->sendAs = 'html';
                $this->Email->send();*/
                

                
                if($from=='mentor')
                {


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "iqbal@guild.im";
                           $subject = "Report issue";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentor_report_issue");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_clientlname=".urlencode($mentorData['Mentee']['last_name']);
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_mentorlname=".urlencode($mentorData['Mentor']['last_name']);
                           $data .= "&merge_comment=".urlencode($this->data['Invoice']['comment']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

                }
                
                if($from=='mentee')
                {


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "iqbal@guild.im";
                           $subject = "Report issue";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_report_issue");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_clientlname=".urlencode($mentorData['Mentee']['last_name']);
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_mentorlname=".urlencode($mentorData['Mentor']['last_name']);
                           $data .= "&merge_comment=".urlencode($this->data['Invoice']['comment']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
                }
                
                //Mandrill Integration - End
                
				
				$this->Session->write('hide_reportissue_link',$mentorship_id);
				$this->Session->setFlash(__("Issue reported", true), 'default', array('class' => 'success'));
                if($from=='mentor')
                    $this->redirect(array('controller'=>'users','action'=>'my_account'));
                else
                   $this->redirect(array('controller'=>'clients','action'=>'my_account'));
            }
			$this->set('mentorship_id',$mentorship_id);
			$this->set('from',$from);
        }
        
        /* function for complete the session from mentor side*/
        function complete_session($mentorship_id=null)
        {
            if(!$mentorship_id)
            {
                $this->redirect(array('controller'=>'fronts','action' => 'index')); 
            }
            $this->Mentorship->id = $mentorship_id;
            $this->Mentorship->saveField('applicationType', '5'); 
            $this->Mentorship->bindModel(
                        array(
                        'belongsTo' => array(
                            'User' => array('className' => 'User','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=User.id'),'fields'=>array('User.username')),
                            'Mentor' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=Mentor.user_id'),'fields'=>array('Mentor.first_name','Mentor.last_name')),
                            'Mentee' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),'fields'=>array('Mentee.first_name','Mentee.last_name')),
                            'Invoice' => array('className' => 'Invoice','foreignKey' => false,'conditions'=>array('Mentorship.id=Invoice.mentorship_id'),'fields'=>array('Invoice.id','Invoice.title')),
                            )));
            $this->Mentorship->recursive = '0';
            $mentorData = $this->Mentorship->findById($mentorship_id);
            if($mentorData['Invoice']['title']=='Initial consultation')
                 $msgSubject = "Initial consultation completed";
            else
                 $msgSubject = "Consultation completed";

            if($mentorData['User']['role_id'] == '3')
           {

           

             
			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $mentorData['User']['username'];
                           $subject = $msgSubject;

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("session_completed");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_mentorlname=".urlencode($mentorData['Mentor']['last_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
             
            
           } 

           else{


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $mentorData['User']['username'];
                           $subject = $msgSubject;

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("subcontract_session_completed");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_mentorlname=".urlencode($mentorData['Mentor']['last_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
             
            
           }
            
            $this->Session->setFlash(__("Session completed", true), 'default', array('class' => 'success')); 
            $this->redirect(array('controller'=>'users','action' => 'my_account')); 
        }
        
        function generateSessionName($mentor_id=null,$mentee_id=null,$mentorshipId=null)
        {
            $mentshipCount = $this->Mentorship->find('count',array('conditions'=>array('Mentorship.mentor_id'=>$mentor_id,'Mentorship.mentee_id'=>$mentee_id,'Mentorship.is_deleted!=1')));
            if($mentshipCount>1)
            {
                $sessname = "Session ".($mentshipCount-1);
            }
            return $sessname ;
        }
        /* function for schedule the next new session from mentor side*/
        function schedule_next($mentorship_id=null)
        {       
            $userId = $this->Session->read('Auth.User.id');
            if(!empty($this->data))
            {
               /* generate new mentorship row for new invoice in next schedule*/
                $parent_id = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.mentor_id'=>$userId,'Mentorship.mentee_id'=>$this->data['Invoice']['mentee_id'],'Mentorship.is_deleted!=1','Mentorship.parent_id=0'),'fields'=>'Mentorship.id','recursive'=>'0'));
                $mentData['Mentorship']['parent_id'] = $parent_id['Mentorship']['id'];
                $mentData['Mentorship']['mentee_id'] = $this->data['Invoice']['mentee_id'];
                $mentData['Mentorship']['mentor_id'] = $userId;
                $mentData['Mentorship']['applicationType'] = '3';
                $mentData['Mentorship']['mentee_need'] = 'Schedule next new session';
                $this->Mentorship->save($mentData);
                $mentorship_id = $this->Mentorship->id;
                
                //$SessionName = $this->generateSessionName($userId,$this->data['Invoice']['mentee_id'],$mentorship_id); 
                $invNo = $this->generateInvoice($userId,$this->data['Invoice']['mentee_id'],$mentorship_id);   
                //$invNo = 'Invoice-'.$userId.'-'.$this->data['Invoice']['mentee_id'].'-'.$mentorship_id;
                $this->request->data['Invoice']['inv_time']      = $this->data['Invoice']['time_hh'].':'.$this->data['Invoice']['time_mm'].':'.$this->data['Invoice']['time_AM_PM'];
                
                if($this->data['Invoice']['inv_datey']!="" && $this->data['Invoice']['time_hh1']!="" && $this->data['Invoice']['time_mm1']!="")
                {
                    $this->request->data['Invoice']['inv_time1']  = $this->data['Invoice']['time_hh1'].':'.$this->data['Invoice']['time_mm1'].':'.$this->data['Invoice']['time_AM_PM1'];
                    $this->request->data['Invoice']['inv_date1']  = strtotime($this->data['Invoice']['inv_datey']);
                }
                if($this->data['Invoice']['inv_datez']!="" && $this->data['Invoice']['time_hh2']!="" && $this->data['Invoice']['time_mm2']!="")
                {
                    $this->request->data['Invoice']['inv_time2']  = $this->data['Invoice']['time_hh2'].':'.$this->data['Invoice']['time_mm2'].':'.$this->data['Invoice']['time_AM_PM2'];
                    $this->request->data['Invoice']['inv_date2']  = strtotime($this->data['Invoice']['inv_datez']);
                }
                
                
                $this->request->data['Invoice']['inv_timezone']  = $this->data['Invoice']['timezone'];
                //$this->data['Invoice']['title']         = $SessionName;
                $this->request->data['Invoice']['inv_date']      = strtotime($this->data['Invoice']['inv_datex']);
                
                if($this->data['Invoice']['pay_in_advance'] == 1) {
	                $this->request->data['Invoice']['discount']      = $this->data['Invoice']['ment_discounts'];
	                $this->request->data['Invoice']['tax']           = $this->data['Invoice']['ment_taxes'];
	                $this->request->data['Invoice']['rate']          = $this->data['Invoice']['ment_rate'];
	                $this->request->data['Invoice']['price']         = $this->data['Invoice']['ment_price'];
	                $this->request->data['Invoice']['total']         = (($this->data['Invoice']['ment_price']-$this->data['Invoice']['ment_discounts'])+$this->data['Invoice']['ment_taxes']);
	                
	                if($this->data['Invoice']['price']!=0)
	                	$this->request->data['Invoice']['rate']      = $this->data['Invoice']['ment_rate'];
	                else
	                	$this->request->data['Invoice']['rate']      = 0;
                }
                
                $this->request->data['Invoice']['inv_no']        = $invNo;
                $this->request->data['Invoice']['mentorship_id'] = $mentorship_id;
                $this->request->data['Invoice']['inv_client_id'] = $this->data['Invoice']['mentee_id'];
              
                $this->Invoice->save($this->request->data);
                
                //Updating Current credit for plan
                $plan_user = $this->PlanUser->find('first',array('conditions'=>array('PlanUser.user_id'=>$this->data['Invoice']['mentee_id'])));
                
                if(!empty($plan_user))
                	$plan = $this->Plan->find('first',array('conditions'=>array('Plan.id'=>$plan_user['PlanUser']['plan_id'])));
                
                $plan['Plan']['current_credit'] = $plan['Plan']['current_credit'] - $this->data['Invoice']['ment_discounts'];
                
                $this->Plan->save($plan['Plan']);
                //Plan Updated
                
                 /* update time zone save for user */ 
                $this->__updatetimezone($this->data['Invoice']['timezone']);
            
                $this->Mentorship->bindModel(
                        array(
                        'belongsTo' => array(
                            'User' => array('className' => 'User','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=User.id'),'fields'=>array('User.username')),
                            'Mentor' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=Mentor.user_id'),'fields'=>array('Mentor.first_name','Mentor.last_name')),
                            'Mentee' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),'fields'=>array('Mentee.first_name','Mentee.last_name')),
                            'Invoice' => array('className' => 'Invoice','foreignKey' => false,'conditions'=>array('Mentorship.id=Invoice.mentorship_id'),'fields'=>array('Invoice.id')),
                            )));
                $this->Mentorship->recursive = '0';
                $mentorData = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.id'=>$mentorship_id)));
                
                /* Acceptance mail for mentee from mentor */
               
               /* $this->Email->to = $mentorData['User']['username'];
                 $this->Email->from = $this->Session->read('Auth.User.username');
                $this->set('data',$mentorData);
                $this->Email->subject = 'Mentorship session scheduled';
                $this->Email->template = 'schedule_next';
                $this->Email->sendAs = 'html';
                $this->Email->send();
                $this->Session->setFlash(__("Invoice sent", true), 'default', array('class' => 'success')); */
          


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $mentorData['User']['username'];
                           $subject = "Consultation proposed";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("schedule_next");
                           $data .= "&merge_clientfname=".urlencode($mentorData['Mentee']['first_name']);
                           $data .= "&merge_mentorfname=".urlencode($mentorData['Mentor']['first_name']);
                           $data .= "&merge_mentorlname=".urlencode($mentorData['Mentor']['last_name']);
                           $data .= "&merge_invoiceid=".urlencode($mentorData['Invoice']['id']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

                $this->Session->setFlash(__("Appointment proposed", true), 'default', array('class' => 'success')); 
                $this->redirect(array('controller'=>'users','action'=>'my_account'));
            }
            $this->Mentorship->recursive = '0';
            $mentorshipData = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.id'=>$mentorship_id,'Mentorship.mentor_id'=>$userId)));
            if(empty($mentorshipData))
            {
                $this->redirect(array('controller'=>'users','action'=>'my_account'));
            }
            $this->Invoice->bindModel(
                array(
                'belongsTo' => array(
                    'Mentorship' => array('className' => 'Mentorship','foreignKey' => 'mentorship_id'),
                    'UserReference' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=UserReference.user_id')),
                    'City' => array('className' => 'City','foreignKey' => false,'conditions'=>array('UserReference.zipcode=City.zip_code'),
                    ))));
            $fields = array('Invoice.*','Mentorship.mentor_id','Mentorship.mentee_id','UserReference.user_id','UserReference.first_name','UserReference.last_name','UserReference.zipcode','UserReference.fee_regular_session','UserReference.fee_first_hour','City.city_name','City.state');
            $invoiceData = $this->Invoice->find('first',array('conditions'=>array('Invoice.mentorship_id'=>$mentorship_id),'fields'=>$fields));
            $timezoneData = $this->Timezone->find('list',array('order'=>'name asc'));
            $this->set('timezoneData',$timezoneData);
            $this->set('invoiceData',$invoiceData);                 
            $this->set('mentorship_id',$mentorship_id); 
            
            //Getting discount values
          /*  $plan_user = $this->PlanUser->find('first',array('conditions'=>array('PlanUser.user_id'=>$mentorshipData['Mentorship']['mentee_id'])));
            
            if(!empty($plan_user))
            	$plan = $this->Plan->find('first',array('conditions'=>array('Plan.id'=>$plan_user['PlanUser']['plan_id'])));
            
            if(empty($plan))
            	$discount_val = 0;
            else {
            
            	$this->User->Behaviors->attach('Containable');
            	$client = $this->User->find('first',
            			array(
            					'contain'=>array(
            							'UserReference'=>array(
            									'fields'=>array('first_name','last_name'),
            							),
            					),
            					'conditions'=>array('User.id'=>$mentorshipData['Mentorship']['mentee_id'])
            			)
            			 
            	);
            	
            	if( $client['User']['plan_type'] == 'Basic')
            		$discount_val = 0;
            	else if( $client['User']['plan_type'] == 'Pro')
            		$discount_val = 30 * $invoiceData['UserReference']['fee_regular_session'] / 100;
            	else
            		$discount_val = 50 * $invoiceData['UserReference']['fee_regular_session'] / 100;
            		
            	if ($discount_val > $plan['Plan']['current_credit'])
            		$discount_val = $plan['Plan']['current_credit'];
            }
            
            $this->set('discount_val',$discount_val);*/
            
        }

        function ical($id = null) 
        { $this->layout = false;
            $id=$this->params['pass'][0];
            
            if (!$id || !$this->Invoice->read(null,$id)) 
            {
              $this->Session->setFlash('Invalid id for Orientation.');
              $this->redirect(array('controller' => 'clients', 'action' => 'my_account'));
              exit();
            }
            $this->Invoice->bindModel(
                array(
                'belongsTo' => array(
                    'Mentorship' => array('className' => 'Mentorship','foreignKey' => 'mentorship_id'),
                    'Mentor' => array('className'=>'UserReference','foreignKey'=>false,'conditions'=>array('Mentor.user_id=Mentorship.mentor_id')),
                    'Mentee' => array('className'=>'UserReference','foreignKey'=>false,'conditions'=>array('Mentee.user_id=Mentorship.mentee_id')),
                    'User' => array('className'=>'User','foreignKey'=>false,'conditions'=>array('User.id=Mentorship.mentor_id')),
                    )));
            $fields = array('Invoice.*','Mentorship.mentor_id','Mentorship.mentee_id','Mentor.first_name','Mentor.last_name','Mentee.first_name','Mentee.last_name','User.timezone');
            $invoiceData = $this->Invoice->find('first',array('conditions'=>array('Invoice.id'=>$id),'fields'=>$fields));
            
            $this->set('invoiceData', $invoiceData);
            $this->render('vcs'); 
        }

        function deletementorship()
        {
            if(isset($_REQUEST['id']) && $_REQUEST['id']!='')
            {
                $mentorshipData = $this->Mentorship->findById($_REQUEST['id']);
                $this->Mentorship->updateAll(array('Mentorship.is_deleted'=>'1'), array('Mentorship.mentor_id'=>$mentorshipData['Mentorship']['mentor_id'],'Mentorship.mentee_id'=>$mentorshipData['Mentorship']['mentee_id']));
                echo json_encode(array('value' => '1'));
                exit();  
            }
        }
        
        /* function for all mentor invoice listing in front end */
        
        function invoicelistmember()
        {

            $this->layout= 'defaultnew';
            $mentor_id = $this->Session->read('Auth.User.id');
            $role_id = $this->Session->read('Auth.User.role_id');
            if($mentor_id=='' || $role_id!='2')
            {
                $this->redirect(array('controller'=>'fronts','action'=>'index'));
            }
           // echo $type; die;
            $this->Invoice->bindModel(
                    array(
                    'belongsTo' => array(
                        'Mentorship' => array('className' => 'Mentorship','foreignKey' => 'mentorship_id'),
                        'Mentee' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=Mentee.user_id')),
                        //'Mentor' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=Mentor.user_id'))
                        )),false);
            $fields = array('Invoice.*','Mentee.first_name','Mentee.last_name'/*,'Mentor.first_name','Mentor.last_name'*/);
            
            $mentorData = $this->UserReference->find('first',array('conditions'=>array('UserReference.user_id'=>$mentor_id),'fields'=>array('first_name','last_name')));

            $this->Paginator->settings = array('conditions'=>array('OR'=> array('Mentorship.mentee_id'=>$mentor_id,'Mentorship.mentor_id'=>$mentor_id)),'fields' => $fields,'order'=>'Invoice.id DESC','limit'=>'10');
          
            $data = $this->Paginator->paginate('Invoice');
            $this->set(compact('data'));
            $this->set('mentorData',$mentorData);
            $this->set('mentor_id',$mentor_id);
        } 
        
        function invoicelistclient()
        {

            $this->layout= 'defaultnew';
            $mentee_id = $this->Session->read('Auth.User.id');
            $role_id = $this->Session->read('Auth.User.role_id');
            if($mentee_id=='' || $role_id!='3')
            {
                $this->redirect(array('controller'=>'fronts','action'=>'index'));
            }
           // echo $type; die;
            $this->Invoice->bindModel(
                    array(
                    'belongsTo' => array(
                        'Mentorship' => array('className' => 'Mentorship','foreignKey' => 'mentorship_id'),
                        'Mentor' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentor_id=Mentor.user_id')),
                        //'Mentor' => array('className' => 'UserReference','foreignKey' => false,'conditions'=>array('Mentorship.mentee_id=Mentor.user_id'))
                        )),false);
             
            $fields = array('Invoice.*','Mentor.first_name','Mentor.last_name'/*,'Mentor.first_name','Mentor.last_name'*/);
            
            $mentorData = $this->UserReference->find('first',array('conditions'=>array('UserReference.user_id'=>$mentee_id),'fields'=>array('first_name','last_name')));
            $this->Paginator->settings = array('conditions'=>array('Mentorship.mentee_id'=>$mentee_id),'fields' => $fields,'order'=>'Invoice.id DESC','limit'=>'5');
          
            $data = $this->Paginator->paginate('Invoice');
           // pr($data);
         //   die;
            $this->set(compact('data'));
            $this->set('mentorData',$mentorData);
            $this->set('mentee_id',$mentee_id);
        }
        
        function invoice_view($invoiceId=null)
        {
          $userId = $this->Session->read('Auth.User.id'); 
          if($userId==''){

            $this->redirect(array('controller'=>'fronts','action'=>'index'));
          }
          if($this->Session->read('Auth.User.role_id')=='2'){

           	$inv = $this->Invoice->find('first',array('conditions'=>array('Invoice.id'=>$invoiceId)));   
              if($inv['Invoice']['inv_client_id'] ==  $this->Session->read('Auth.User.id'))
              $cond['Mentorship.mentee_id'] = $inv['Invoice']['inv_client_id'];
              else                       
          	$cond['Mentorship.mentor_id'] = $userId;
         } else if($this->Session->read('Auth.User.role_id')=='3') {
          	$inv = $this->Invoice->find('first',array('conditions'=>array('Invoice.id'=>$invoiceId)));
             $cond['Mentorship.mentee_id'] = $inv['Invoice']['inv_client_id'];
           }else
          	$this->redirect(array('controller'=>'fronts','action' => 'index'));
          
          $this->Invoice->bindModel(
                    array(
                    'belongsTo' => array(
                        'Mentorship' => array(
                            'className' => 'Mentorship',
                            'foreignKey' => 'mentorship_id',
                            'fields'=>array('Mentorship.mentor_id','Mentorship.mentee_id',),
                        ),
                        'Mentee' => array(
                            'className' => 'UserReference',
                            'foreignKey' => false,
                            'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),
                            'fields'=>array('Mentee.first_name','Mentee.last_name'),
                        ),
                         'Mentor' => array(
                            'className' => 'UserReference',
                            'foreignKey' => false,
                            'conditions'=>array('Mentorship.mentor_id=Mentor.user_id'),
                            'fields'=>array('Mentor.first_name','Mentor.last_name'),
                        ),
                        'User' => array(
                            'className' => 'User',
                            'foreignKey' => false,
                            'conditions'=>array('Mentorship.mentor_id=User.id'),
                            'fields'=>array('User.timezone'),
                        
                        ))));
              
            $data= $this->Invoice->find('first',array('conditions'=>array('Invoice.id'=>$invoiceId,$cond)));
            $ab = $this->Invoice->find('first',array('conditions'=>array('Invoice.id'=>$invoiceId)));
            $plan = $this->User->find('first',array('conditions'=>array('User.id'=>$ab['Invoice']['inv_client_id'])));
            $this->set("plan",$plan['User']['plan_type']);

             if(empty($data))
              $this->redirect(array('controller'=>'fronts','action' => 'index'));
            else
               $this->set('data',$data);

        }

        function requestreschedule()
        {
            if($this->Session->read('Auth.User.id')=='')
            {
                $sending = SITE_URL."fronts/index";
                $this->Session->write('reqReschdule','user');
                echo "<script type='text/javascript'>window.location.href='".$sending."';loginpopup();</script>";
                exit;
            }
            else
            {
                if($this->Session->read('Auth.User.role_id')=='3')
                   $this->redirect(array('controller'=>'clients','action'=>'my_account'));
                if($this->Session->read('Auth.User.role_id')=='2')
                    $this->redirect(array('controller'=>'users','action'=>'my_account'));
                else
                    $this->redirect(array('controller'=>'fronts','action'=>'index'));
            }
        }

        function view_pdf($invoiceId=null) 
        {
            $this->layout = 'pdf';
            $userId = $this->Session->read('Auth.User.id'); 

          if($userId==''){
            $this->redirect(array('controller'=>'fronts','action'=>'index'));
          }
          
          if($this->Session->read('Auth.User.role_id')=='2')
          	$cond['Mentorship.mentor_id'] = $userId;
          else if($this->Session->read('Auth.User.role_id')=='3') {
          	$inv = $this->Invoice->find('first',array('conditions'=>array('Invoice.id'=>$invoiceId)));
          	$cond['Mentorship.mentee_id'] = $inv['Invoice']['inv_client_id'];
          }else
          	$this->redirect(array('controller'=>'fronts','action' => 'index'));
          
          $this->Invoice->bindModel(
                    array(
                    'belongsTo' => array(
                        'Mentorship' => array(
                            'className' => 'Mentorship',
                            'foreignKey' => 'mentorship_id',
                            'fields'=>array('Mentorship.mentor_id','Mentorship.mentee_id',),
                        ),
                        'Mentee' => array(
                            'className' => 'UserReference',
                            'foreignKey' => false,
                            'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),
                            'fields'=>array('Mentee.first_name','Mentee.last_name'),
                        ),
                         'Mentor' => array(
                            'className' => 'UserReference',
                            'foreignKey' => false,
                            'conditions'=>array('Mentorship.mentor_id=Mentor.user_id'),
                            'fields'=>array('Mentor.first_name','Mentor.last_name'),
                         ),
                         'User' => array(
                            'className' => 'User',
                            'foreignKey' => false,
                            'conditions'=>array('User.id=Mentorship.mentee_id'),
                            'fields'=>array('User.plan_type'),
                        ))));
               $data= $this->Invoice->find('first',array('conditions'=>array('Invoice.id'=>$invoiceId,$cond,'Mentorship.is_deleted'=>'0')));
            if(empty($data))
              $this->redirect(array('controller'=>'fronts','action' => 'index'));
            else
               $this->set('data',$data);
			$this->response->header(array('Content-type: application/pdf')); 
			$this->response->type('pdf'); 
           $this->render();
        } 
 
        function mail_invoice_view() 
        {
          $userId = $this->Session->read('Auth.User.id'); 
          if($userId==''){
            $this->redirect(array('controller'=>'fronts','action'=>'index'));
          }
          $this->Invoice->bindModel(
                    array(
                    'belongsTo' => array(
                        'Mentorship' => array(
                            'className' => 'Mentorship',
                            'foreignKey' => 'mentorship_id',
                            'fields'=>array('Mentorship.mentor_id','Mentorship.mentee_id',),
                        ),
                        'Mentee' => array(
                            'className' => 'UserReference',
                            'foreignKey' => false,
                            'conditions'=>array('Mentorship.mentee_id=Mentee.user_id'),
                            'fields'=>array('Mentee.first_name','Mentee.last_name'),
                        ),
                         'Mentor' => array(
                            'className' => 'UserReference',
                            'foreignKey' => false,
                            'conditions'=>array('Mentorship.mentor_id=Mentor.user_id'),
                            'fields'=>array('Mentor.first_name','Mentor.last_name'),
                        ))));
            if($this->Session->read('Auth.User.role_id')=='2')
                $cond['Mentorship.mentor_id'] = $userId;
            else if($this->Session->read('Auth.User.role_id')=='3')
                $cond['Mentorship.mentee_id'] = $userId;  
            else
                $this->redirect(array('controller'=>'fronts','action' => 'index'));           
            $data= $this->Invoice->find('first',array('conditions'=>array('Invoice.id'=>$invoiceId,$cond,'Mentorship.is_deleted'=>'0')));
            if(empty($data))
              $this->redirect(array('controller'=>'fronts','action' => 'index'));
            else
               $this->set('data',$data);
        } 

        function __updatetimezone($timezone)
        {
            $this->User->id = $this->Session->read('Auth.User.id');
            $this->User->saveField('timezone', $timezone);
        }    
        
        /* Function for view ,comment & reschedule invoice on member_incoice_preview side */
        function member_invoice_preview()
        {

        	if(empty($this->data)){
        		$pdf_preview_data = $this->Session->read('pdf_preview_data');

        		if($pdf_preview_data == null && empty($pdf_preview_data)) {
        			$this->redirect(array('controller'=>'fronts','action'=>'index'));
        		} else {
        			$this->request->data = $pdf_preview_data;
        		}
        	}
            $userId = $this->Session->read('Auth.User.id'); 
          	if($userId==''){
            	$this->redirect(array('controller'=>'fronts','action'=>'index'));
          	}
          
          	$this->User->Behaviors->attach('Containable');
          	$client = $this->User->find('first',
          			array(
          					'contain'=>array(
          							'UserReference'=>array(
          									'fields'=>array('first_name','last_name'),
          							),
          					),
          					'conditions'=>array('User.id'=>$this->data['Invoice']['mentee_id'])
          			));
          	
          	$this->User->Behaviors->attach('Containable');
          	$member = $this->User->find('first',
          			array(
          					'contain'=>array(
          							'UserReference'=>array(
          									'fields'=>array('first_name','last_name'),
          							),
          					),
          					'conditions'=>array('User.id'=>$userId)
          			));
          			
          	$this->request->data['Mentor']['first_name'] = $member['UserReference']['first_name'];
          	$this->request->data['Mentor']['last_name'] = $member['UserReference']['last_name'];
          	$this->request->data['Mentee']['first_name'] = $client['UserReference']['first_name'];
          	$this->request->data['Mentee']['last_name'] = $client['UserReference']['last_name'];
          	
          	
          	
          	if($this->data['Invoice']['pay_in_advance'] < 2){
          		$this->request->data['Invoice']['inv_to'] = '';
          		$this->request->data['Invoice']['inv_from'] = '';
          		$this->request->data['Invoice']['inv_description'] = '';
          		
          		$invNo = $this->generateInvoice($userId,$this->data['Invoice']['mentee_id'],$this->data['Invoice']['mentorship_id']);
          	} else {

          		$invNo = $this->generateInvoiceNumber($this->data['Invoice']['mentorship_id']);
          	}
          	$this->request->data['Invoice']['inv_no'] = $invNo;
           	//$this->set('data',$this->data);
           	$this->Session->write('pdf_preview_data', $this->data);
           	//pr($this->data);
           	//$this->render();
        }
        
        function preview_pdf($data = null)
        {
        	$this->layout = 'pdf';
        	$pdf_preview_data = $this->Session->read('pdf_preview_data');
        	//prd($pdf_preview_data);
			$this->response->header(array('Content-type: application/pdf')); 
			$this->response->type('pdf'); 
        	$this->set('data',$pdf_preview_data);
        }
        
        function save_pending_emails($mentorData)
        {
        	$mentor = $this->User->find('first',array('conditions'=>array('User.id'=>$mentorData['Mentorship']['mentor_id'])));
        	
        	$emailJobs['email'] = $mentor['User']['username'];
        	$emailJobs['first_name'] = $mentorData['Mentor']['first_name'];
        	$emailJobs['last_name'] = $mentorData['Mentor']['last_name'];
        	$emailJobs['inv_time'] = $mentorData['Invoice']['inv_time'];
        	$emailJobs['duration_hours'] = $mentorData['Invoice']['duration_hours'];
        	$emailJobs['duration_minutes'] = $mentorData['Invoice']['duration_minutes'];
        	$emailJobs['inv_timezone'] = $mentorData['Invoice']['inv_timezone'];
        	$emailJobs['inv_date'] = $mentorData['Invoice']['inv_date'];
        	
        	$this->EmailJobs->save($emailJobs);
        }
        
        function send_pending_emails()
        {
        	$pendingEmail = $this->EmailJobs->find('all',array('conditions'=>array('EmailJobs.send'=>0)));
        	
        	foreach ($pendingEmail as $value){
        		
        		$time = $this->General->getTimeValue($value['EmailJobs']['inv_time']);
        		
        		$timeOfConsultation = $value['EmailJobs']['inv_date'] + $time;
        		
        		$currentTime = strtotime("now");
        		
        		$twoHours = 2 * 60 * 60;
        		
        		if(($currentTime - $timeOfConsultation) > $twoHours){
        		
        		$timeData = $this->General->timeformatchange($value['EmailJobs']['inv_time'], $value['EmailJobs']['duration_hours'], $value['EmailJobs']['duration_minutes']);
        		$timeZoneValue = $this->General->getTimeZoneById($value['EmailJobs']['inv_timezone']);
        		

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $date = date('m/d/Y',$value['EmailJobs']['inv_date'])."(".date('l',$value['EmailJobs']['inv_date']).")";
                           $to = $value['EmailJobs']['email'];
                           $subject = "Did you have a good consultation?";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("complete_consultation");
                           $data .= "&merge_mentorfname=".urlencode($value['EmailJobs']['first_name']);
                           $data .= "&merge_invoicedate=".urlencode($date);
                           $data .= "&merge_invoicetime=".urlencode($timeData);
                           $data .= "&merge_timezone=".urlencode($timeZoneValue);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End
        		
        		$value['EmailJobs']['send'] = 1;
        		
        		$this->EmailJobs->save($value['EmailJobs']);
        		unset($this->EmailJobs->id);
        		}
        	}
        	$this->render(false);
        	die;
        }

                function save_reminder_emails($mentorData)
        {
        	$mentor = $this->User->find('first',array('conditions'=>array('User.id'=>$mentorData['Mentorship']['mentor_id'])));
        	$mentee = $this->User->find('first',array('conditions'=>array('User.id'=>$mentorData['Mentorship']['mentee_id'])));
			
        	$emailJobs['mentor_email'] = $mentor['User']['username'];
		$emailJobs['mentee_email'] = $mentee['User']['username'];
        	$emailJobs['mentor_fname'] = $mentorData['Mentor']['first_name'];
        	$emailJobs['mentor_lname'] = $mentorData['Mentor']['last_name'];
		$emailJobs['mentee_fname'] = $mentorData['Mentee']['first_name'];
        	$emailJobs['mentee_lname'] = $mentorData['Mentee']['last_name'];
        	$emailJobs['inv_time'] = $mentorData['Invoice']['inv_time'];
        	$emailJobs['duration_hours'] = $mentorData['Invoice']['duration_hours'];
        	$emailJobs['duration_minutes'] = $mentorData['Invoice']['duration_minutes'];
        	$emailJobs['inv_timezone'] = $mentorData['Invoice']['inv_timezone'];
        	$emailJobs['inv_date'] = $mentorData['Invoice']['inv_date'];
        	
        	$this->ReminderJobs->save($emailJobs);
        }    
	
                /* function for saving email before consulation time*/		
         function send_reminder_emails()
        {
        	$reminderEmail = $this->ReminderJobs->find('all',array('conditions'=>array('ReminderJobs.send'=>0)));
        	
        	foreach ($reminderEmail as $value){
        		
        		$time = $this->General->getTimeValue($value['ReminderJobs']['inv_time']);
        		
        		$timeOfConsultation = $value['ReminderJobs']['inv_date'] + $time;
        		
        		$currentTime = strtotime("now");
        		
        		$twelveHours = 12 * 60 * 60;
        		
        		if(($timeOfConsultation - $currentTime) < $twelveHours){
        		
        		$timeData = $this->General->timeformatchange($value['ReminderJobs']['inv_time'], $value['ReminderJobs']['duration_hours'], $value['ReminderJobs']['duration_minutes']);
        		$timeZoneValue = $this->General->getTimeZoneById($value['ReminderJobs']['inv_timezone']);
        	



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $date = date('m/d/Y',$value['ReminderJobs']['inv_date'])."(".date('l',$value['ReminderJobs']['inv_date']).")";
                           $to = $value['ReminderJobs']['mentor_email'];
                           $subject = "Consultation reminder";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentor_consultation_reminder");
                           $data .= "&merge_mentorfname=".urlencode($value['ReminderJobs']['mentor_fname']);
                           $data .= "&merge_invoicedate=".urlencode($date);
                           $data .= "&merge_invoicetime=".urlencode($timeData);
                           $data .= "&merge_timezone=".urlencode($timeZoneValue);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $date = date('m/d/Y',$value['ReminderJobs']['inv_date'])."(".date('l',$value['ReminderJobs']['inv_date']).")";
                           $to = $value['ReminderJobs']['mentor_email'];
                           $subject = "Consultation reminder";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_consultation_reminder");
                           $data .= "&merge_clientfname=".urlencode($value['ReminderJobs']['mentee_fname']);
                           $data .= "&merge_invoicedate=".urlencode($date);
                           $data .= "&merge_invoicetime=".urlencode($timeData);
                           $data .= "&merge_timezone=".urlencode($timeZoneValue);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

        		$value['ReminderJobs']['send'] = 1;
        		
        		$this->ReminderJobs->save($value['ReminderJobs']);
        		unset($this->ReminderJobs->id);        		        		
        		}
        	}
        	$this->render(false);
        	die;
        }

         function stripecheckout(){
    	  if ($this->RequestHandler->isAjax()) {
    		
    			
    		$invoiceId = $this->request->data['invoiceId'];
                $amount = $this->request->data['price'];
                $token = $this->request->data['token'];
                $email = $this->request->data['email'];
                Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");
                
 

			try {
				$charge = Stripe_Charge::create(array(
						"amount" => $amount, // amount in cents, again
						"currency" => "usd",
						"card" => $token,
                                                "receipt_email"=> $email,
						"description" => "Invoice Payment")
				);

                $invoice = $this->Invoice->find('first', array('conditions' => array('Invoice.id' => $invoiceId)));
                $mentorData = $this->Mentorship->find('first',array('conditions'=>array('Mentorship.id'=>$invoice['Invoice']['mentorship_id'])));

		
		$this->Invoice->updateAll(array('inv_create_status'=>1), array('Invoice.id'=> $invoiceId));
		echo json_encode($invoiceId);
    		$this->render(false);
    		exit();
                 
                } catch(Stripe_CardError $e) {
              			//pr("There is some error");
				//pr($e);
                                //die;
               }


            }


         }

         function thanks(){

	      $this->Session->write('popup', 1);
              $this->set("title_for_layout","Thanks");

           }

	}
	?>