<?php
/**
 * Users Controller
 *
 * PHP version 5
 *
 * @category Controller
 */
//require('../webroot/mandrill/Mandrill.php');
require('../webroot/stripe-php-1.17.2/lib/Stripe.php');
class PricingController extends AppController{
	/**
	* Controller name
	*
	* @var string
	* @access public
	*/
	var $name = 'Pricing';
	var $uses = array('User','StaticPage', 'UserReference', 'Social', 'UserImage', 'NewsLetter', 'Plan', 'PlanUser',
			'Mentorship', 'Pricing_temp');
	
	var $helpers = array('General', 'Form', 'Ajax', 'javascript', 'Paginator');
	var $components = array("Session", "Email", "Auth", "RequestHandler" ,"Upload");
	

	/**
	 * Models used by the Controller
	 *
	 * @var array
	 * @access public
	*/	 
	function beforeFilter(){
		parent::beforeFilter();	
		$this->Auth->allow('PricingAuth',  'password_setting_popup','PricingInviteAuth','pricing_auth_popup','stripecheckout', 'striperecurr', 'striperecurr1','striperecurrcancel','striperecurrcancel1','cancel_plan','cancel_plan1');
	}
	
function striperecurrcancel(){
		
		Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");
		
		$user = $this->User->find('first',array('conditions'=>array('User.username'=>$this->Auth->user('username'))));
		
		$customer = Stripe_Customer::retrieve($user['User']['paypalRecurringId']);
		
		$subscription = $customer->subscriptions;
		
		$subscription['data'][0]->cancel();
		
		$user['User']['mentor_type'] = "Regular Member";
		$user['User']['paypalRecurringId'] = '';
		
		$this->User->save($user);
		
              //  sending  mail to client after publish profile 
  
             $mailData = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
                        



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $daniellemail = "danielle@guild.im";
                           $to = $this->Auth->user('username').";".$daniellemail;
                           $subject = "Your Premium Membership has been canceled";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("cancel_premium_membership");
                           $data .= "&merge_consultantfname=".urlencode($mailData['UserReference']['first_name']);
                           $data .= "&merge_link=".urlencode("members/premium");
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End



		$this->Session->write('Auth', $user);
		$this->Session->setFlash(__("You are now a Regular Member.", true), 'default', array('class' => 'success'));
		$this->redirect(array('controller'=>'members','action'=>'account_setting'));
		
	}

  function striperecurrcancel1(){
		
		Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");
		
		$user = $this->User->find('first',array('conditions'=>array('User.username'=>$this->Auth->user('username'))));
		
		$customer = Stripe_Customer::retrieve($user['User']['paypalRecurringId']);
		
		$subscription = $customer->subscriptions;
		
		$subscription['data'][0]->cancel();
		
		$user['User']['mentor_type'] = "Regular Member";
		$user['User']['paypalRecurringId'] = '';
		
		$this->User->save($user);
		
              //  sending  mail to client after publish profile 
  
             $mailData = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
                        



			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $daniellemail = "danielle@guild.im";
                           $to = $this->Auth->user('username').";".$daniellemail;
                           $subject = "Your Master Membership has been canceled";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("cancel_master_membership");
                           $data .= "&merge_consultantfname=".urlencode($mailData['UserReference']['first_name']);
                           $data .= "&merge_link=".urlencode("members/master");
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End



		$this->Session->write('Auth', $user);
		$this->Session->setFlash(__("You are now a Regular Member.", true), 'default', array('class' => 'success'));
		$this->redirect(array('controller'=>'members','action'=>'account_setting'));
		
	}
	
	function striperecurr() {

               $this->layout= 'defaultnew';
	
		if(!empty($_POST['stripeToken'])) {
	
			// Set your secret key: remember to change this to your live secret key in production
			// See your keys here https://dashboard.stripe.com/account
			Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");
	
			// Get the credit card details submitted by the form
			$token = $_POST['stripeToken'];
				
			// Create the charge on Stripe's servers - this will charge the user's card
			try {
				$customer = Stripe_Customer::create(array(
					"card" => $token,
					"plan" => "mg_premium_membership_99",
					"email" => $this->Auth->user('username'))
				);
	
				$user = $this->User->find('first',array('conditions'=>array('User.username'=>$this->Auth->user('username'))));
				
				$user['User']['mentor_type'] = "Premium Member";
				$user['User']['paypalRecurringId'] = $customer['id'];
				
				$this->User->save($user);
                         
                            //  sending  mail to client after publish profile   
                        $mailData = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
                        


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $daniellemail = "danielle@guild.im";
                           $to = $this->Auth->user('username').";".$daniellemail;
                           $subject = "Welcome to Premium Membership!";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("premium_membership");
                           $data .= "&merge_consultantfname=".urlencode($mailData['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

				
				$this->Session->write('Auth', $user);
				$this->Session->setFlash(__("You are now a Premium Member.", true), 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'members','action'=>'account_setting'));
	
			} catch(Stripe_CardError $e) {
				pr("There is some error");
				prd($e);
				// The card has been declined
			}
		} else {
				
			$this->set("title_for_layout","Upgrade to premium membership");
			$this->User->Behaviors->attach('Containable');
			$this->data = $this->User->find('first',array('contain'=>array('UserReference'=>array('fields'=>array('zipcode','first_name','last_name','background_summary','area_of_expertise','application_5_fee')),'UserImage'),'conditions'=>array('role_id'=>2,'User.id'=>$this->Session->read('Auth.User.id'),'status'=>1)));			
		}
	}

	function striperecurr1() {
	
                $this->layout= 'defaultnew';

		if(!empty($_POST['stripeToken'])) {
	
			// Set your secret key: remember to change this to your live secret key in production
			// See your keys here https://dashboard.stripe.com/account
			Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");
	
			// Get the credit card details submitted by the form
			$token = $_POST['stripeToken'];
				
			// Create the charge on Stripe's servers - this will charge the user's card
			try {
				$customer = Stripe_Customer::create(array(
					"card" => $token,
					"plan" => "mg_master_membership_999",
					"email" => $this->Auth->user('username'))
				);
	
				$user = $this->User->find('first',array('conditions'=>array('User.username'=>$this->Auth->user('username'))));
				
				$user['User']['mentor_type'] = "Master Member";
				$user['User']['paypalRecurringId'] = $customer['id'];
				
				$this->User->save($user);
                         
                            //  sending  mail to client after publish profile   
                        $mailData = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
                        


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $daniellemail = "danielle@guild.im";
                           $to = $this->Auth->user('username').";".$daniellemail;
                           $subject = "Welcome to GUILD Master Membership!";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("master_membership");
                           $data .= "&merge_consultantfname=".urlencode($mailData['UserReference']['first_name']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End

				
				$this->Session->write('Auth', $user);
				$this->Session->setFlash(__("You are now a master Member.", true), 'default', array('class' => 'success'));
				$this->redirect(array('controller'=>'members','action'=>'account_setting'));
	
			} catch(Stripe_CardError $e) {
				pr("There is some error");
				prd($e);
				// The card has been declined
			}
		} else {
				
			$this->set("title_for_layout","Upgrade to master membership");
			$this->User->Behaviors->attach('Containable');
			$this->data = $this->User->find('first',array('contain'=>array('UserReference'=>array('fields'=>array('zipcode','first_name','last_name','background_summary','area_of_expertise','application_5_fee')),'UserImage'),'conditions'=>array('role_id'=>2,'User.id'=>$this->Session->read('Auth.User.id'),'status'=>1)));			
		}
	}

	
	function stripecheckout($fee = null, $message = null) {
		
		if(!empty($_POST['stripeToken'])) {
		
			// Set your secret key: remember to change this to your live secret key in production
			// See your keys here https://dashboard.stripe.com/account
			Stripe::setApiKey("sk_live_TZC7VfUJSsoHya3yafuqhsZf");
		
			// Get the credit card details submitted by the form
			$token = $_POST['stripeToken'];
			$amount = $_POST['FEE'];

			// Create the charge on Stripe's servers - this will charge the user's card
			try {
				$charge = Stripe_Charge::create(array(
						"amount" => $amount, // amount in cents, again
						"currency" => "usd",
						"card" => $token,
						"description" => $this->Auth->user('email'))
				);
				
				//pr("Charged");
				//prd($charge);
				
				$this->redirect(array('controller'=>'pricing','action'=>'actionAfterPaymentSuccess'));
				
			} catch(Stripe_CardError $e) {
				pr("There is some error");
				prd($e);
				// The card has been declined
			}
		} else {
			
			$centFee = $fee * 100;
			
			$this->set('centFee', $centFee);
			$this->set('fee', $fee);
			$this->set('message', $message);
			
		}
		//$this->layout=false;
	}

      function cancel_plan(){
				
			$this->set("title_for_layout","Cancel premium membership");
			$this->User->Behaviors->attach('Containable');
			$this->data = $this->User->find('first',array('contain'=>array('UserReference'=>array('fields'=>array('zipcode','first_name','last_name','background_summary')),'UserImage'),'conditions'=>array('role_id'=>2,'User.id'=>$this->Session->read('Auth.User.id'),'status'=>1)));			
		     }

      function cancel_plan1(){
				
			$this->set("title_for_layout","Cancel master membership");
			$this->User->Behaviors->attach('Containable');
			$this->data = $this->User->find('first',array('contain'=>array('UserReference'=>array('fields'=>array('zipcode','first_name','last_name','background_summary')),'UserImage'),'conditions'=>array('role_id'=>2,'User.id'=>$this->Session->read('Auth.User.id'),'status'=>1)));			
		     }


	function actionAfterPaymentSuccess(){
		
		$action = $this->Session->read('ActionAfterPaymentSuccess');
		
		if($action == 'Linkedin_Non_Basic') {
			
			//Saving Plan
			$plan['Plan']['admin_id'] = $this->Auth->user('id');
			$plan['Plan']['plan_type'] = $this->Session->read('plan_type');
			$plan['Plan']['discount'] = $this->Session->read('discount');
			$plan['Plan']['initial_credit'] = $this->Session->read('initial_credit');
			$plan['Plan']['current_credit'] = $this->Session->read('initial_credit');
			$plan['Plan']['start_date'] = strtotime(date('Y-m-d'));
			
			$this->Plan->save($plan['Plan']);
			$plantype = $this->Session->read('plan_type');
			//Saving Plan User
			$plan_user['PlanUser']['plan_id'] = $this->Plan->id;
			$plan_user['PlanUser']['user_id'] = $this->Auth->user('id');
			$plan_user['PlanUser']['activation_key'] = '-1';
			$plan_user['PlanUser']['first_name'] = $this->Session->read('first_name');
			$plan_user['PlanUser']['last_name'] = $this->Session->read('last_name');
			
			$this->PlanUser->save($plan_user['PlanUser']);
			
			$user = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
			$user['User']['plan_type'] = $this->Session->read('plan_type');
			$user['User']['isPlanAdmin'] = 1;
			
			$this->User->save($user);
			
			$this->Session->write('Auth',$user);
			
			$this->Session->delete('ActionAfterPaymentSuccess');
			$this->Session->delete('plan_type');
			$this->Session->delete('first_name');
			$this->Session->delete('last_name');
			$this->Session->delete('discount');
			$this->Session->delete('initial_credit');
			
			$this->Session->setFlash(__("You are now enrolled to the $plantype plan.", true), 'default', array('class' => 'success'));
			$this->redirect(array('controller' => 'clients', 'action' => 'edit_account'));
		
		} else if($action == 'Basic_to_Non_Basic') {

			$user = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
			
			$plan['Plan']['admin_id'] = $user['User']['id'];
			$plan['Plan']['plan_type'] = $this->Session->read('plan_type');
			$plan['Plan']['discount'] = $this->Session->read('discount');
			$plan['Plan']['initial_credit'] = $this->Session->read('initial_credit');
			$plan['Plan']['current_credit'] = $this->Session->read('initial_credit');
			$plan['Plan']['start_date'] = strtotime(date('Y-m-d'));
			 
			$this->Plan->save($plan['Plan']);
			
			$user['User']['plan_type'] = $this->Session->read('plan_type');
			$user['User']['isPlanAdmin'] = 1;
			$this->User->save($user);
			
			$this->Session->write('Auth', $user);
			 
			//Saving Plan User
			$plan_user['PlanUser']['plan_id'] = $this->Plan->id;
			$plan_user['PlanUser']['user_id'] = $user['User']['id'];
			$plan_user['PlanUser']['activation_key'] = '-1';
			$plan_user['PlanUser']['first_name'] = $user['UserReference']['first_name'];
			$plan_user['PlanUser']['last_name'] = $user['UserReference']['last_name'];
				
			$this->PlanUser->save($plan_user);
			
			$this->Session->delete('ActionAfterPaymentSuccess');
			$this->Session->delete('plan_type');
			$this->Session->delete('first_name');
			$this->Session->delete('last_name');
			$this->Session->delete('discount');
			$this->Session->delete('initial_credit');
			
			$this->Session->setFlash(__("Plan updated. Add members to your plan.", true), 'default', array('class' => 'success'));
			$this->redirect(array('controller' => 'pricing','action' => 'dashboard'));
			
		} else if($action == 'Admin_upgrade'){
			
			$user = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
			
			$plan = $this->Plan->find('first',array('conditions'=>array('Plan.admin_id'=>$user['User']['id'])));
			 
			$plan_users = $this->PlanUser->find('all',array('conditions'=>array('PlanUser.plan_id'=>$plan['Plan']['id'])));
			
			$plan['Plan']['admin_id'] = $user['User']['id'];
			$plan['Plan']['plan_type'] = $this->Session->read('plan_type');
			$plan['Plan']['discount'] = $this->Session->read('discount');
			$plan['Plan']['initial_credit'] = $this->Session->read('initial_credit');
			$plan['Plan']['current_credit'] = $this->Session->read('initial_credit');
			 
			$this->Plan->save($plan['Plan']);
			
			$user['User']['plan_type'] = $this->Session->read('plan_type');
			$user['User']['isPlanAdmin'] = 1;
			$this->User->save($user);
			
			$this->Session->write('Auth', $user);
			
			$this->User->id == null;
			
			foreach ($plan_users as $pUser) { //Deleting all users
					
				$userp = $this->User->find('first',array('conditions'=>array('User.id'=>$pUser['PlanUser']['user_id'])));
					
				if($userp['User']['id'] != $user['User']['id']) {
			
					$userp['User']['plan_type'] = 'Basic';
						
					$this->User->save($userp);
						
					$this->User->id == null;
						
					$this->PlanUser->delete($pUser['PlanUser']['id']);
						
					$this->PlanUser->id == null;
				}
					
			}
			
			$this->Session->delete('ActionAfterPaymentSuccess');
			$this->Session->delete('plan_type');
			$this->Session->delete('first_name');
			$this->Session->delete('last_name');
			$this->Session->delete('discount');
			$this->Session->delete('initial_credit');
			
			$this->Session->setFlash(__("Plan updated. Add members to your plan.", true), 'default', array('class' => 'success'));
			$this->redirect(array('controller' => 'pricing','action' => 'dashboard'));
			
		} else if ($action == 'Admin_downgrade') {
			
			$user = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
			
			$plan = $this->Plan->find('first',array('conditions'=>array('Plan.admin_id'=>$user['User']['id'])));
			
			$plan_users = $this->PlanUser->find('all',array('conditions'=>array('PlanUser.plan_id'=>$plan['Plan']['id'])));
			 
			$plan['Plan']['admin_id'] = $user['User']['id'];
			$plan['Plan']['plan_type'] = $this->Session->read('plan_type');
			$plan['Plan']['discount'] = $this->Session->read('discount');
			$plan['Plan']['initial_credit'] = $this->Session->read('initial_credit');
			$plan['Plan']['current_credit'] = $this->Session->read('initial_credit');
			
			$this->Plan->save($plan['Plan']); //Plan updated
			 
			$user['User']['plan_type'] = $this->Session->read('plan_type');
			$user['User']['isPlanAdmin'] = 1;
			$this->User->save($user); //User updated
			 
			$this->Session->write('Auth', $user);
			 
			$this->User->id == null;
			 
			foreach ($plan_users as $pUser) { //Deleting all users
				 
				$userp = $this->User->find('first',array('conditions'=>array('User.id'=>$pUser['PlanUser']['user_id'])));
					
				if($userp['User']['id'] != $user['User']['id']) {
						
					$userp['User']['plan_type'] = 'Basic';
			
					$this->User->save($userp);
			
					$this->User->id == null;
			
					$this->PlanUser->delete($pUser['PlanUser']['id']);
			
					$this->PlanUser->id == null;
				}
					
			}
			 
			$this->Session->delete('ActionAfterPaymentSuccess');
			$this->Session->delete('plan_type');
			$this->Session->delete('first_name');
			$this->Session->delete('last_name');
			$this->Session->delete('discount');
			$this->Session->delete('initial_credit');
			
			$this->Session->setFlash(__("Plan updated. Add members to your plan.", true), 'default', array('class' => 'success'));
			$this->redirect(array('controller'=>'pricing','action'=>'dashboard'));
		} else if($action == 'Manual_New'){
			
			$user = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Auth->user('id'))));
			
			//Saving Plan
			$plan['Plan']['admin_id'] = $user['User']['id'];
			$plan['Plan']['plan_type'] = $this->Session->read('plan_type');
			$plan['Plan']['discount'] = $this->Session->read('discount');
			$plan['Plan']['initial_credit'] = $this->Session->read('initial_credit');
			$plan['Plan']['current_credit'] = $this->Session->read('initial_credit');
			$plan['Plan']['start_date'] = strtotime(date('Y-m-d'));
		
			$this->Plan->save($plan['Plan']);
		
			//Saving Plan User
			$plan_user['PlanUser']['plan_id'] = $this->Plan->id;
			$plan_user['PlanUser']['user_id'] = $user['User']['id'];
			$plan_user['PlanUser']['activation_key'] = '-1';
			$plan_user['PlanUser']['first_name'] = $user['UserReference']['first_name'];
			$plan_user['PlanUser']['last_name'] = $user['UserReference']['last_name'];
		
			$this->PlanUser->save($plan_user['PlanUser']);
					
			$user['User']['plan_type'] = $this->Session->read('plan_type');
			$user['User']['isPlanAdmin'] = 1;
			$this->User->save($user); //User updated
			
			$this->Session->write('Auth', $user);
			
			$this->User->id == null;
			
			$this->Session->delete('ActionAfterPaymentSuccess');
			$this->Session->delete('plan_type');
			$this->Session->delete('first_name');
			$this->Session->delete('last_name');
			$this->Session->delete('discount');
			$this->Session->delete('initial_credit');
			
			$this->redirect(array('controller'=>'clients','action'=>'edit_account'));
			
		}
	}
	
	
	
    function login(){

    	$this->Session->write('popup', 1);
        
        $this->layout = 'ajax';
        $this->set('title_for_layout', 'User Login'); 
        
        if(isset($this->request['named']['plan_id_for_login'])){
        	$this->data['User']['plan_id_for_login'] = $this->request['named']['plan_id_for_login'];
        }
        
        /* Remember me functionality */
        //if(isset($this->data['User']['rememberme']) && $this->data['User']['rememberme']==1)
          if($this->Session->read('Auth.User.id')!='')
        {
            $this->Cookie->write('password',$_REQUEST['data']['User']['password'],$encrypt = false, 3600);
            $this->Cookie->write('email', $this->data['User']['username'], $encrypt = false, 3600);
        }
        
        if(isset($this->data['User']['username']) && $this->data['User']['username'] != '') {
	        $user = $this->User->find('first',array('conditions'=>array('User.username'=>$this->data['User']['username'])));
	
	        if(!empty($user)) {
	        	
	        	$this->Session->write('Auth', $user);
	        	
	        	$UserData = $this->User->read('password',$this->Auth->user('id'));
	        	
	        	//set image logined user
	        	if(isset($UserData['UserImage'][0]['image_name']) &&  count($UserData['UserImage'])>0){
	        		$this->Session->write('User.image',$UserData['UserImage'][0]['image_name']);
	        	}
	        	else
	        	{
	        		$this->Session->write('User.image','');
	        	}
	        	
	        	if($this->Auth->user('role_id') == 3) {
	        		 
	        		////////////Plan URL/////////////////////
	        		if(isset($this->data['User']['plan_id_for_login']) && $this->data['User']['plan_id_for_login'] !=''){
	        			 
	        			//$this->redirect(array('controller'=>'pricing','action'=>'pricing', $this->data['User']['plan_id_for_login']));
                                         $this->redirect(array('controller'=>'fronts','action'=>'index'));
	        		}
	        		/////////////////////////////////
	        		 
	        	} else {
	        		 
	        		/*$this->Session->setFlash(__('Consultant member is required to delete the current profile to join us as a client. Email help@guild.im for support.', true), 'flash_bad');*/
	        		//$this->redirect(array('controller' => 'pricing', 'action' => 'pricing'));
                                  $this->redirect(array('controller'=>'fronts','action'=>'index'));
	        	}
	        	
	        } else { //Not a user
	        	
	        	$this->Session->setFlash(__('You are not a registered user. Contact <a href="mailto:help@guild.im">help@guild.im</a> for support.', true), 'flash_bad');
	        	$this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
	        }
        }
    }
    
	function loginfromregister() {
		 
		$this->redirect(array('controller'=>'pricing','action'=>'pricing','plan_id_for_login:'.$this->Session->read('plan_id')));
	}
	
	
	function confirmation() {
		
	}
       
    function pricing()
    {
	    $this->Session->write('popup', 1);

           $this->set("title_for_layout","Subscriber Plan");
	    $this->data = $this->StaticPage->find('first',array('conditions'=>array('StaticPage.slug'=>'pricing')));
	    
	    if(isset($this->request['named']['plan_id_for_login']) && $this->request['named']['plan_id_for_login'] !=''){
	    	$this->set('plan_id_for_login',$this->request['named']['plan_id_for_login']);
	    }
	    
   	}
   	
    function change_plan($plan_id = null, $isConfirmed = null) {
    	
    	if($this->Session->read('Auth.User.role_id') == 2) {
    		
    		$this->Session->setFlash(__('Consultants are required to delete the current profile to join us as Clients. Contact <a href="mailto:help@guild.im">help@guild.im</a> for support.', true), 'flash_bad');
    		$this->Session->write('RedirectTo','Pricing');
    		$this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
    	}
    	
    	if($plan_id ==''){
    		
    		$plan_id = $this->request['named']['plan_id'];
    	}
    	$user_id = $this->Session->read('Auth.User.id');
    	
    	$user = $this->User->find('first',array('conditions'=>array('User.id'=>$user_id)));
    	
    	$old_plan_id = 1;
    	
    	if($user['User']['plan_type'] == 'Basic')
    		$old_plan_id = 1;
    	else if ($user['User']['plan_type'] == 'Business')
    		$old_plan_id = 2;
    	else
    		$old_plan_id = 3;
    	
    	if($plan_id == 1){
    		$plan_type = 'Basic';
    	}else if($plan_id == 2) {
			$plan_type = 'Business';
			$discount = 30;
			$initial_credit = 300;
			$fee = 1950;
			$message = "Business PLAN";
		}
		else {
			$plan_type = 'Enterprise';
			$discount = 50;
			$initial_credit = 500;
			$fee = 2950;
			$message = "ENTERPRISE PLAN";
		}
    	
    	if($plan_type == $user['User']['plan_type']) //User already has same plan
    	{
    		 $this->Session->setFlash(__('You are already registered as a '.$plan_type.' user', true), 'default', array('class' => 'success'));
   			 $this->redirect(array('controller' => 'pricing','action' => 'dashboard'));
    	}  
    	
    	if($user['User']['plan_type'] == 'Basic') { //Basic user changing to Pro or Enterprise
    		
    		$this->Session->write('ActionAfterPaymentSuccess', 'Basic_to_Non_Basic');
    		$this->Session->write('plan_type',$plan_type);
    		$this->Session->write('discount',$discount);
    		$this->Session->write('initial_credit',$initial_credit);
    		
    		$init_amt = $fee;
    		$current_date = date('Y-m-d');
    		$profile_start_date = strtotime($current_date . ' + 1 year');
    		
    		//$this->redirect(array('controller' => 'pricing', 'action' => 'getRecurringToken', $fee, $init_amt, $message, $profile_start_date));


    	} else if ($user['User']['isPlanAdmin'] == 1) { //User is admin of a Pro or enterprise plan
    		
    		if($isConfirmed == 0) {
    			
    			$this->Session->write('plan_id', $plan_id);
    			$this->Session->setFlash(__('Are you sure you want to change to <span style="font-weight:bold">'.$plan_type . '</span> plan?', true), 'flash_bad');
    			$this->Session->write('RedirectTo','Pricing_dashboard');
                $this->redirect(array('controller' => 'pricing', 'action' => 'confirmation'));
    			
    		} else { // Confirmed
	    		
	    		if($old_plan_id < $plan_id) { //upgrade
	    			
	    			$plan = $this->Plan->find('first',array('conditions'=>array('Plan.admin_id'=>$user_id)));
	    			$start_date = $plan['Plan']['start_date'];
	    			
	    			$this->Session->write('ActionAfterPaymentSuccess', 'Admin_upgrade');
	    			$this->Session->write('plan_type',$plan_type);
	    			$this->Session->write('discount',$discount);
	    			$this->Session->write('initial_credit',$initial_credit);
	    			$this->Session->write('start_date',$start_date);
	    			
	    			//$this->cancelRecurringAccount();
	    			
	    			$profile_start_date = 0;
	    				
	    			$current_date = strtotime(date('Y-m-d'));
	    			
    				$i = 1;
    					
    				while ($profile_start_date < $current_date) {
    			
    					$add_time = $i * (365 * 60 * 60 * 24);
    					$profile_start_date = $start_date + $add_time;
    					$i++;
    				}
    					
    				$days_remaining = ($profile_start_date - $current_date) / (60 * 60 * 24);
    			       
    				$init_amt = (1000* $days_remaining) / 364;
    				
	    			//$this->redirect(array('controller' => 'pricing', 'action' => 'getRecurringToken', $fee, $init_amt, $message, $profile_start_date));
	    			
	    		} else { //Downgrade
	    			
	    			if($plan_id == 1) { //Changing to Basic
	    				
	    				$plan = $this->Plan->find('first',array('conditions'=>array('Plan.admin_id'=>$user_id)));
	    				 
	    				$plan_users = $this->PlanUser->find('all',array('conditions'=>array('PlanUser.plan_id'=>$plan['Plan']['id'])));
	    				
	    				$this->Plan->delete($plan['Plan']['id']); //Deleting Plan
	    				
	    				foreach ($plan_users as $pUser) { //Deleting all users
	    					 
	    					$userp = $this->User->find('first',array('conditions'=>array('User.id'=>$pUser['PlanUser']['user_id'])));
	    					
	    					$userp['User']['plan_type'] = 'Basic';
	    					
	    					$this->User->save($userp);
	    					
	    					$this->User->id == null;
	    					
	    					$this->PlanUser->delete($pUser['PlanUser']['id']);
	    					
	    					$this->PlanUser->id == null;
	    					
	    				}
	    				
	    				$user['User']['plan_type'] = $plan_type; //Changing user plan
	    				$user['User']['isPlanAdmin'] = 0;
	    				$this->User->save($user);
	    				
	    				$this->Session->write('Auth', $user);
	    				
	    				//$this->cancelRecurringAccount();
	    				
	    				$this->Session->setFlash(__("Your membership plan has been updated.", true), 'default', array('class' => 'success'));
	    				$this->redirect(array('controller'=>'clients','action'=>'my_account'));
	    				
	    			} else { // Changing to Business
	    				
	    				$plan = $this->Plan->find('first',array('conditions'=>array('Plan.admin_id'=>$user_id)));
	    				$start_date = $plan['Plan']['start_date'];
	    				
	    				$this->Session->write('ActionAfterPaymentSuccess', 'Admin_downgrade');
	    				$this->Session->write('plan_type',$plan_type);
	    				$this->Session->write('discount',$discount);
	    				$this->Session->write('initial_credit',$initial_credit);
	    				$this->Session->write('start_date',$start_date);
	    				
	    				//$this->cancelRecurringAccount();
	    				
	    				$init_amt = 1;
	    				
	    				$profile_start_date = 0;
	    					
	    				$current_date = strtotime(date('Y-m-d'));
	    				
    					$i = 1;
    						
    					while ($profile_start_date < $current_date) {
    				
    						$add_time = $i * (365 * 60 * 60 * 24);
    						$profile_start_date = $start_date + $add_time;
    						$i++;
    					}
    						
    					$init_amt = 1; // Plan change fees
	    						
	    				//$this->redirect(array('controller' => 'pricing', 'action' => 'getRecurringToken', $fee, $init_amt, $message, $profile_start_date));
	    				
	    			}
	    		}
    		}
    	} else { //Business and Enterprise non admin
    		
    		if($plan_id == 1) { //Changing to Basic
    			
    			if($isConfirmed == 0) {
    				
    				$this->Session->write('plan_id', $plan_id);
    				$this->Session->setFlash(__('Are you sure you want to end your Plan membership?', true), 'flash_bad');
    				$this->Session->write('RedirectTo','Client_my_account');
                    $this->redirect(array('controller' => 'Pricing', 'action' => 'confirmation'));
    				 
    			} else {
	    			$plan_user = $this->PlanUser->find('first',array('conditions'=>array('PlanUser.user_id'=>$user_id)));
	    			
	    			$this->PlanUser->delete($plan_user['PlanUser']['id']);
	    			
	    			$user['User']['plan_type'] = 'Basic'; //Changing user plan
	    			$user['User']['isPlanAdmin'] = 0;
	    			$this->User->save($user);
	    			
	    			$this->Session->write('Auth', $user);
	    			
    				$this->Session->setFlash(__("Your membership plan has been updated.", true), 'default', array('class' => 'success'));
    				$this->redirect(array('controller'=>'clients','action'=>'my_account'));
    			}
    			
    		} else {
    			
    			$this->Session->setFlash(__('Please contact your plan administrator to change your plan.', true), 'flash_bad');
    			$this->Session->write('RedirectTo','Client_profile');
    			$this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
    		}
    	}
    }
	function make_admin($clientId = null){
		
		if($clientId != null) {
			
			$new_Admin = $this->User->find('first',array('conditions'=>array('User.id'=>$clientId)));
			
			$current_Admin = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
			
			$new_Admin['User']['isPlanAdmin'] = 1;
			$current_Admin['User']['isPlanAdmin'] = 0;
			
			$this->User->save($new_Admin['User']);
			$this->User->id = null;
			
			$this->User->save($current_Admin['User']);
			
			$this->Session->write('Auth.User',$current_Admin['User']);
			
			$plan = $this->Plan->find('first',array('conditions'=>array('Plan.admin_id'=>$this->Session->read('Auth.User.id'))));
			
			$plan['Plan']['admin_id'] = $clientId;
			
			$this->Plan->save($plan);
			
			/**
                        //Mandrill Integration - Begin
			$mandrill = new Mandrill();
				
			$template_content = array(
						
			);
			
			$params = array(
					'subject' => 'You have been made admin',
					'from_email' => Configure::read('Site.email'),
					'from_name' => "GUILD",
					'to' => array(
							array('email' => $new_Admin['User']['username'])
					),
					'global_merge_vars' => array(
							array('name' => 'NEW_FNAME', 'content' => $new_Admin['UserReference']['first_name']),
							array('name' => 'NEW_LNAME', 'content' => $new_Admin['UserReference']['last_name']),
							array('name' => 'OLD_FNAME', 'content' => $current_Admin['UserReference']['first_name']),
							array('name' => 'OLD_LNAME', 'content' => $current_Admin['UserReference']['last_name']),
							array('name' => 'SITE_URL', 'content' => SITE_URL)
					)
			);
				
			$result = $mandrill->messages->sendtemplate('admin_change', $template_content, $params);
			//Mandrill Integration - End
			**/
			
		}
		
		$this->redirect(array('controller' => 'clients', 'action' => 'my_account'));
	}
	
	function round_table(){
		
		$user = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
		

		

			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = "iqbal@guild.im";
                           $subject = "Request for Executive Roundtale";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("roundtable_request");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_userlname=".urlencode($user['UserReference']['last_name']);
                           $data .= "&merge_userid=".urlencode($user['User']['id']);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


		$this->Session->setFlash(__('Your request for an Executive Roundtable has been sent', true), 'default', array('class' => 'success'));
		$this->redirect(array('controller' => 'pricing', 'action' => 'dashboard'));
		
	}
	
	function open_invoices() {
	
		$plan = $this->Plan->find('first',array('conditions'=>array('Plan.admin_id'=>$this->Session->read('Auth.User.id'))));
	
		$plan_users = $this->PlanUser->find('all',array('conditions'=>array('PlanUser.plan_id'=>$plan['Plan']['id'])));
	


	}
	
	function past_invoices() {
		
		$plan = $this->Plan->find('first',array('conditions'=>array('Plan.admin_id'=>$this->Session->read('Auth.User.id'))));
		
		$plan_users = $this->PlanUser->find('all',array('conditions'=>array('PlanUser.plan_id'=>$plan['Plan']['id'])));
		


	}

	
	function pricing_auth_popup(){

	
		$this->layout = 'ajax';
		$this->Session->write('popup', 1);

                if($this->Session->check('menteePopup')){
                $this->Session->delete('menteePopup');
                }

                if($this->Session->check('projectId')){
                $this->Session->delete('projectId');
                } 
                if($this->Session->check('questionId')){
                $this->Session->delete('questionId');
                } 

                if($this->Session->check('duserid')){
                $this->Session->delete('duserid');
                } 
                if($this->Session->check('login_industry')){
                $this->Session->delete('login_industry');
                } 
                if($this->Session->check('mentor')){
                $this->Session->delete('mentor');
                } 
                if($this->Session->check('mentorshipId')){
                $this->Session->delete('mentorshipId');
                } 
                if($this->Session->check('consultantPopup')){
                $this->Session->delete('consultantPopup');
                }
                if($this->Session->check('directoryclaim')){
                $this->Session->delete('directoryclaim');
                } 


                $this->Session->write('menteePopup', 1);
                $this->Session->write('planID', 1);
		if(!empty($this->data)){ //popup submit

			
				$this->__subscribenewsletter($this->data['User']['username']);
	
				$plan_id = "1";
				

				
				$user = $this->User->find('first',array('conditions'=>array('User.username'=>$this->data['User']['username'])));
				
				if(!empty($user)) { // Already a user. 
					
					$this->Session->write('Auth', $user);
					
					$this->redirect(array('controller' => 'clients', 'action' => 'my_account'));
					
				} else { // Register as a new user
					
					$activationKey = substr(md5(uniqid()), 0, 20);
   
					$this->request->data['User']['role_id'] = 3;
					$this->request->data['User']['status'] = 1;
					$this->request->data['User']['password'] = $this->data['User']['password2'];//Security::hash($this->data['User']['password2'], null, true);
                                        $this->request->data['User']['password2'] = Security::hash($this->data['User']['password2'], null, true);
					$this->request->data['User']['plan_type'] = "Basic";
					$this->request->data['User']['isPlanAdmin'] = 0;

					$this->request->data['User']['activation_key'] = $activationKey;
					$this->request->data['User']['access_specifier'] = 'publish';
				        $this->request->data['User']['is_approved'] = 1;
				        $this->request->data['User']['email_send'] = '1';
					
						
					$this->User->saveAll($this->request->data);

					if(!isset($this->data['User']['id'])){
						$id = $this->User->getLastInsertID();
					}else{
						$id = $this->data['User']['id'];
					}

                                        $refData = $this->UserReference->find('first',array('conditions'=>array('UserReference.user_id'=>$id)));

                                        $refData['UserReference']['client_companycontactname']  =  $this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'];
                                        $refData['UserReference']['client_companyaddress']  =  $this->data['User']['username'];
				        $this->User->createUrlKey($id);
				        $this->UserReference->save($refData);
					

					


						


                                          $data = $this->User->read(null,$id);
					
					       $this->Session->write('Auth', $data);						
				


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $this->data['User']['username'];
                           $subject = "Welcome to GUILD";
                           $link = SITE_URL.'clients/my_account/'.$id;

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_register");
                           $data .= "&merge_userfname=".urlencode($this->data['UserReference']['first_name']);
                           $data .= "&merge_useremail=".urlencode($this->data['User']['username']);
                           $data .= "&merge_profilelink=".urlencode($link);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End



                                    $this->redirect(array('controller' => 'clients', 'action' => 'my_account'));
				}
			}

	}

	
	function password_setting_popup(){
		
		$this->layout = 'ajax';
		$this->Session->write('popup', 1);
		if($this->Session->check('Pricing.Plan.AddUser')){
				
			if (isset($_POST['_method'])) {
				$this->__subscribenewsletter($this->data['User']['username']);
				
				$plan_user = $this->PlanUser->find('first',array('conditions'=>array('PlanUser.activation_key'=>$this->Session->read('Pricing.Plan.AddUser'))));
				
				if( !empty($plan_user) && $plan_user['PlanUser']['activation_key'] != '-1') {
					
					$plan = $this->Plan->find('first',array('conditions'=>array('Plan.id'=>$plan_user['PlanUser']['plan_id'])));
					
					$activationKey = substr(md5(uniqid()), 0, 20);
					$this->request->data['User']['role_id'] = 3;
					$this->request->data['User']['status'] = 1;
					$this->request->data['User']['access_specifier'] = 'draft';
					$this->request->data['User']['is_approved'] = 1;
					$this->request->data['User']['password'] = $this->data['User']['password2'];//Security::hash($this->data['User']['password2'], null, true);
					$this->request->data['User']['password2'] = Security::hash($this->data['User']['password2'], null, true);
					$this->request->data['User']['plan_type'] = $plan['Plan']['plan_type'];
					$this->request->data['User']['activation_key'] = $activationKey;
					
					$this->User->saveAll($this->request->data);
					
					
					$menteeArray['password'] = $this->data['User']['password2'];
					$menteeArray['username'] = $this->data['User']['username'];
					$this->Session->write('menteeTempData',$menteeArray);
					
					if(!isset($this->data['User']['id'])){
						$id = $this->User->getLastInsertID();
					}else{
						$id = $this->data['User']['id'];
					}
					$data = $this->User->read(null,$id);

					$this->Session->write('Auth', $data);
					
					$plan_user['PlanUser']['user_id'] = $id;
					$plan_user['PlanUser']['activation_key'] = '-1';
					$plan_user['PlanUser']['username'] = $this->data['User']['username'];
					$plan_user['PlanUser']['first_name'] = $this->data['UserReference']['first_name'];
                    $plan_user['PlanUser']['last_name'] = $this->data['UserReference']['last_name'];

					$this->PlanUser->save($plan_user);
					
					$this->redirect(array('controller'=>'users','action'=>'registration_step2'));
				} else {
					$this->Session->setFlash(__('Invalid activation link, please contact site administrator.', true), 'flash_bad');
					$this->Session->write('RedirectTo','Front');
					$this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
				}
			}
		}else{
			$this->redirect(array('controller'=>'fronts','action'=>'index'));
		}
	}
	
	function adduser($activation_key) {
		
		$plan_user = $this->PlanUser->find('first',array('conditions'=>array('PlanUser.activation_key'=>$activation_key)));
		
		if(empty($plan_user)) { //User already used this link
			
			$this->Session->setFlash(__('Invalid activation link, please contact site administrator.', true), 'flash_bad');
			$this->Session->write('RedirectTo','Front');
            $this->redirect(array('controller' => 'static_pages', 'action' => 'thanks'));
			
		} else {
			
			$user = $this->User->find('first',array('conditions'=>array('User.username'=>$plan_user['PlanUser']['email'])));
		
               if(!empty($user) && $user['User']['isPlanAdmin'] == 1){// admin can't join any other admin plan
                      
                        $this->Session->write('Auth',$user);
                       $this->Session->setFlash(__("To join a different plan, first reassign your admin role to another user.", true), 'default', array('class' => 'success'));
                      $this->redirect(array('controller' => 'pricing', 'action' => 'dashboard'));
                         }
                    
               else{
                      
                       if(!empty($user)) { //User exists
			
				/*if($user['User']['plan_type'] == 'Business' || $user['User']['plan_type'] == 'Enterprise')  {// one client for two place
                           
                            $plan_user = $this->PlanUser->find('first',array('conditions'=>array('PlanUser.email'=>$user['User']['username'])));
	    		      $this->PlanUser->delete($plan_user['PlanUser']['id']);
                           
                           $plan = $this->Plan->find('first',array('conditions'=>array('Plan.id'=>$plan_user['PlanUser']['plan_id'])));

				$user['User']['plan_type'] = $plan['Plan']['plan_type'];
				
				$this->User->save($user['User']);
				
				$plan_user['PlanUser']['user_id'] = $user['User']['id'];
				$plan_user['PlanUser']['activation_key'] = '-1';
				
                             $this->PlanUser->save($plan_user);
				
				$this->Session->write('Auth',$user);
				
				$this->redirect(array('controller'=>'clients','action'=>'my_account'));
				
			     }

                       else {*/
                        
                              $plan = $this->Plan->find('first',array('conditions'=>array('Plan.id'=>$plan_user['PlanUser']['plan_id'])));

				$user['User']['plan_type'] = $plan['Plan']['plan_type'];
				
				$this->User->save($user['User']);
				
				$plan_user['PlanUser']['user_id'] = $user['User']['id'];
				$plan_user['PlanUser']['activation_key'] = '-1';
				
                $this->PlanUser->save($plan_user);
				
				$this->Session->write('Auth',$user);
				
				$this->redirect(array('controller'=>'clients','action'=>'my_account'));

                    // }
                      

                     } else {
			
				$this->Session->write('Pricing.Plan.AddUser',$activation_key);
				$this->redirect(array('controller'=>'fronts','action'=>'index','OpenPlanPopup:'.true));
			}
		}
	}
}
	
	function remove_user($plan_user_id) {
		
		$plan_user = $this->PlanUser->find('first',array('conditions'=>array('PlanUser.id'=>$plan_user_id)));
		
		$user = $this->User->findById($plan_user['PlanUser']['user_id']);
		
		if(!empty($user)) {
		
			$user['User']['plan_type'] = 'Basic';
			
			$this->User->save($user['User']);
		}
		
		$this->PlanUser->delete($plan_user_id);
		
		$this->redirect(array('controller'=>'pricing','action'=>'dashboard'));
	}
	
	function plan_invitation() {
	
		if(!empty($this->data)) {
	
			$this->User->Behaviors->attach('Containable');
			$user = $this->User->findById($this->Auth->user('id'));
	
			$emailIds = explode(',',$this->data['EmailId']);
			$count = count($emailIds);
	
			$plan = $this->Plan->find('first',array('conditions'=>array('Plan.admin_id'=>$this->Session->read('Auth.User.id'))));
			
			for($i=0;$i<$count;$i++)
			{
				$activation_key = substr(md5(uniqid()), 0, 20);
				$plan_user['PlanUser']['plan_id'] = $plan['Plan']['id'];
				$plan_user['PlanUser']['email'] = $emailIds[$i];
				$plan_user['PlanUser']['activation_key'] = $activation_key;
				
				$this->PlanUser->save($plan_user);
				$this->PlanUser->id = '';
						
				/**$mandrill = new Mandrill();
				
				$template_content = array(
					
				);
					
				$params = array(
						'subject' => $this->data['EmailSubject'],
						'from_email' => $user['User']['username'],
						'from_name' => $user['UserReference']['first_name']." ".$user['UserReference']['last_name'],
						'to' => array(
									array('email' => $emailIds[$i])
									),
	    				'global_merge_vars' => array(
		    							array('name' => 'EMAIL_BODY', 'content' => nl2br($this->data['EmailBody'])),
		    							array('name' => 'URL', 'content' => "https://www.guild.im/pricing/adduser/".$activation_key),
		    							array('name' => 'SENDER_FNAME', 'content' => $user['UserReference']['first_name'])
		    						)
		    						);
	
    				$result = $mandrill->messages->sendtemplate('send_invitation', $template_content, $params);**/
	
			}
	
			$this->redirect(array('controller'=>'pricing','action'=>'dashboard'));
	
			} else {
	
			$this->User->Behaviors->attach('Containable');
    		$user = $this->User->findById($this->Auth->user('id'));
	    	$this->layout = 'ajax';
	    	$this->set("fname", $user['UserReference']['first_name']);
		}
	}
	    				
	function dashboard() {
		
		if($this->Session->read('Auth.User.id') != '') {
			
			$user = $this->User->find('first',array('conditions'=>array('User.id'=>$this->Session->read('Auth.User.id'))));
			
			if($user['User']['isPlanAdmin'] == 1) {
				
				$plan = $this->Plan->find('first',array('conditions'=>array('Plan.admin_id'=>$this->Session->read('Auth.User.id'))));
				
				$plan_users = $this->PlanUser->find('all',array('conditions'=>array('PlanUser.plan_id'=>$plan['Plan']['id'])));
				
				$count = 0;
				foreach ($plan_users as $pUsers) {
					
					
					$user = $this->User->find('first',array('conditions'=>array('User.id'=>$pUsers['PlanUser']['user_id'])));
					
					if(!empty($user)) {
						
						$plan_users[$count]['PlanUser']['username'] = $user['User']['username'];
						$plan_users[$count]['PlanUser']['is_approved'] = $user['User']['is_approved'];
						$plan_users[$count]['PlanUser']['access_specifier'] = $user['User']['access_specifier'];
						$plan_users[$count]['PlanUser']['first_name'] = $user['UserReference']['first_name'];
						$plan_users[$count]['PlanUser']['last_name'] = $user['UserReference']['last_name'];
					} else {
						$plan_users[$count]['PlanUser']['username'] = $pUsers['PlanUser']['email'];
						$plan_users[$count]['PlanUser']['is_approved'] = '0';
						$plan_users[$count]['PlanUser']['access_specifier'] = 'draft';
					}
					
					$count++;
				}
				
				$this->set('plan', $plan);
				$this->set('plan_users', $plan_users);
			} else {
				
				$this->redirect(array('controller'=>'fronts','action'=>'index'));
			}
		} else {
			
			$this->redirect(array('controller'=>'fronts','action'=>'index'));
		}
	}
	
	function PricingInviteAuth() {
	
		if(isset($_REQUEST['oid']) && $_REQUEST['oid']!=''&& $_REQUEST['email']!='')
		{



			/* Read linkedin data from text file  */
			$current = file_get_contents(SITE_URL.$_REQUEST['oid'].".txt");
			$linkData = explode("<br/>",$current);
			unlink($_REQUEST['oid'].".txt");
			$this->__subscribenewsletter($linkData[4]);
	
			if($linkData[4] == '' || $linkData[1] == '') {
	
				$linkData[0] = $_REQUEST['oid'];
				$linkData[4] = $_REQUEST['email'];
			}
	
			$existEmail = $this->User->find('first',array('conditions'=>array('User.username'=>$linkData[4])));
			if(empty($existEmail)) //Email Id was not found in DB -- Register as a client
			{
			    
                         $plan_user = $this->PlanUser->find('first',array('conditions'=>array('PlanUser.activation_key'=>$this->Session->read('Pricing.Plan.AddUser'))));
                         $plan = $this->Plan->find('first',array('conditions'=>array('Plan.id'=>$plan_user['PlanUser']['plan_id'])));
                         $plan_user['PlanUser']['user_id'] = $id;
                         $plan_user['PlanUser']['username'] = $linkData[4];
			             $plan_user['PlanUser']['first_name'] = $this->data['UserReference']['first_name'];
                         $plan_user['PlanUser']['last_name'] = $this->data['UserReference']['last_name'];

			                $this->PlanUser->save($plan_user); 
                            $id = $this->__registerUsingLinkedin($_REQUEST['oid'], $linkData, 0);
			
                          }
                   
			else //Email Id found in DB -- Login
			{
				if($existEmail['User']['role_id']==3 || $existEmail['User']['role_id']==2)
				{
					$id = $existEmail['User']['id'];
	
					$this->__loginUsingLinkedin($_REQUEST['oid'], $existEmail, $linkData, 0);
				}
			}
	
			$plan_user = $this->PlanUser->find('first',array('conditions'=>array('PlanUser.email'=>$linkData[4])));
					
			$plan_user['PlanUser']['activation_key'] = '-1';
			$plan_user['PlanUser']['user_id'] = $id;
			$plan_user['PlanUser']['first_name'] = $linkData[1];
            $plan_user['PlanUser']['last_name'] = $linkData[2];	
			$this->PlanUser->save($plan_user);
	
			$this->redirect(array('controller'=>'clients','action'=>'my_account'));
		}
		else
		{
			$this->Session->write('RedirectTo','Front');
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("Sorry! You are unauthorized user.", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
			die;
		}
	}
	
	function PricingAuth($oid, $email) {

		if(isset($oid) && $oid!=''&& $email!='')
		{

			/* Read linkedin data from text file  */
			$current = file_get_contents(SITE_URL.$oid.".txt");
			$linkData = explode("<br/>",$current);
   
			unlink($oid.".txt");


			$this->__subscribenewsletter($linkData[4]);
			if($linkData[4] == '' || $linkData[1] == '') {
	
				$linkData[0] = $oid;
				$linkData[4] = $email;
			}
	
			$existEmail = $this->User->find('first',array('conditions'=>array('User.username'=>$linkData[4])));
			if(empty($existEmail)) //Email Id was not found in DB -- Register as a client
			{      //pr($linkData);
                            //die;
				$id = $this->__registerUsingLinkedin($oid, $linkData, 1);
				
                                $planid = $this->Session->read('planID');
                                $this->Session->delete('planID');
                                $this->Session->delete('menteePopup');
				
				if($planid == 2) {
					
                               
                                   //$id = 1;
					$this->redirect(array('controller' => 'clients', 'action' => 'striperecurr2',$planid));
				
				}
                            if($planid == 3) {
                                   //$id = 1;
					$this->redirect(array('controller' => 'clients', 'action' => 'striperecurr3',$planid));
				
				}

				$this->redirect(array('controller' => 'clients', 'action' => 'my_account'));
			}
			else //Email Id found in DB -- Login
			{
				echo "3";
				if($existEmail['User']['role_id']==3 || $existEmail['User']['role_id']==2)
				{
					$id = $existEmail['User']['id'];
						
					$this->__loginUsingLinkedin($oid, $existEmail, $linkData, 1);
					
					//$this->redirect(array('action'=>'pricing'));
                                        $this->redirect(array('controller'=>'fronts','action'=>'index'));
				}
			}
		}
		else
		{
                    
			echo "else";
			$this->Session->write('RedirectTo','Front');
			$sending = SITE_URL."thanks";
			$this->Session->setFlash(__("Sorry! You are unauthorized user.", true), 'default', array('class' => 'notclass'));
			echo "<script type='text/javascript'> window.location.href='".$sending."';</script>";
			die;
		}
    }
	
	function __subscribenewsletter($email=null)
	{
		if($email!="")
		{
			$newsletterData = $this->NewsLetter->find('first',array('conditions'=>array('NewsLetter.email'=>$email)));
			if(empty($newsletterData))
			{
				$newsData['NewsLetter']['email'] = $email;
				$newsData['NewsLetter']['status'] = '1';
				$this->NewsLetter->save($newsData);
			}
		}
	}
	
	function __registerUsingLinkedin($socialId, $linkData, $isPlanAdmin){
	
		$saveData['User']['status'] = '1';
		$saveData['User']['activation_key'] = substr(md5(uniqid()), 0, 20);
	
		$this->User->create();
		$saveData['User']['access_specifier'] = 'publish';
		$saveData['User']['is_approved'] = '1';
		$saveData['User']['role_id'] = 3;
		$saveData['User']['username'] = $linkData[4];
		$saveData['User']['socialId'] = $socialId;
		$password = $this->General->randamPassword();
		$saveData['User']['password'] = $password;//Security::hash($password, null, true);
		$saveData['User']['password2'] = Security::hash($password, null, true);		
		$saveData['User']['plan_type'] = 'Basic';
		$saveData['User']['isPlanAdmin'] = 0;
		
		if($this->User->save($saveData,false))
		{
			$id = $this->User->id;
	
			if($linkData[5]!='' || $linkData[7]!='')
			{
				$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
				if(empty($socialLink))
				{
					$socialData['Social']['user_id'] = $id;
					$socialData['Social']['created_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				else
				{
					$socialData['Social']['id'] = $socialLink['Social']['id'];
					$socialData['Social']['modified_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				$this->Social->save($socialData);
				$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
				if($linkData[7]!='')
				{
					$twittLink = explode(',',$linkData[7]);
					for($i=0;$i<count($twittLink);$i++)
					{
					$this->Social->create();
					$twitData['Social']['user_id'] = $id;
					$twitData['Social']['created_by'] = $id;
					$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
					$this->Social->save($twitData);
					}
					}
					}
	
					$this->UserReference->create();
					$refData['UserReference']['first_name'] = $linkData[1];
					$refData['UserReference']['last_name'] = $linkData[2];
					$refData['UserReference']['user_id'] = $id;
			                $refData['UserReference']['background_summary']  =  $linkData[6];
                                        $refData['UserReference']['client_linkedinurl']  =  $linkData[5];
				$refData['UserReference']['headline']  =  $linkData[8];
                                $refData['UserReference']['client_companyname']  =  $linkData[9];
                                $refData['UserReference']['client_companydesignation']  =  $linkData[10];
                                $refData['UserReference']['client_companycontactname']  =  $linkData[1]." ".$linkData[2];
                                $refData['UserReference']['client_companyaddress']  =  $linkData[4];
				$this->User->createUrlKey($id);
				$this->UserReference->save($refData);
	
				if(isset($linkData[3]) && $linkData[3]!='')
				{
				$db_imgname = "user_".time().".jpeg";
				$file_name = IMAGES . MENTEES_IMAGE_PATH . DS . $id;
	
					$destination = IMAGES . MENTEES_IMAGE_PATH . DS . $id;
					$imagePath = $linkData[3];
					$des1 = $file_name.DS.'uploaded';
					$des2 = $file_name.DS.'small/';
					if (!is_dir($destination)) {
					mkdir($destination, 0777);
						mkdir($destination.DS.'small', 0777);
						mkdir($destination.DS.'uploaded', 0777);
					}
					if(!copy($imagePath,$file_name.DS.'uploaded'.DS.$db_imgname))
					{
							$linkData[3]="";
					}
						$cropdata = array();
						$cropdata['x'] = 0; $cropdata['y'] = 0; $cropdata['w'] = 40; $cropdata['h'] = 40; $cropdata['name'] = $db_imgname;
						$rr = $this->imageCrop($des1.DS,$cropdata,$des2,40,40);
						$rr = $this->imageCrop($des1.DS,$cropdata,$destination,200,200);
						$image_data['UserImage']['user_id'] = $id;
								$image_data['UserImage']['image_name'] = $db_imgname;
										$this->UserImage->save($image_data);
					}
	
					$user = $this->User->findById($id);
					if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
						$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
					else
					$this->Session->write('User.image','');
	
			$this->Session->write('Auth',$user);
	


			//EasticEmail Integration - Begin

                           $res = "";

                           $from = "help@guild.im";
                           $fromName = "GUILD";
                           $websiteurl = SITE_URL;
                           $to = $linkData[4];
                           $subject = "Welcome to GUILD";

                           $data = "username=".urlencode("iqbal@guild.im");
                           $data .= "&api_key=".urlencode("3194ea3c-1324-45b7-8e3e-a2283c144674");
                           $data .= "&from=".urlencode($from);
                           $data .= "&from_name=".urlencode($fromName);
                           $data .= "&to=".urlencode($to);
                           $data .= "&subject=".urlencode($subject);
                           $data .= "&template=".urlencode("mentee_register_linkedin");
                           $data .= "&merge_userfname=".urlencode($user['UserReference']['first_name']);
                           $data .= "&merge_useremail=".urlencode($linkData[4]);
                           $data .= "&merge_websiteurl=".urlencode($websiteurl);
                           if($body_html)
                            $data .= "&body_html=".urlencode($body_html);
                           if($body_text)
                            $data .= "&body_text=".urlencode($body_text);
    

                           $header = "POST /mailer/send HTTP/1.0\r\n";
                           $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                           $header .= "Content-Length: " . strlen($data) . "\r\n\r\n";
                           $fp = fsockopen('ssl://api.elasticemail.com', 443, $errno, $errstr, 30);

                           if(!$fp)
                            return "ERROR. Could not open connection";
                           else {
                            fputs ($fp, $header.$data);
                           while (!feof($fp)) {
                            $res .= fread ($fp, 1024);
                           }
                            fclose($fp);
                           }


                           //EasticEmail Integration - End


									
		return $id;
	}
	}
	
	function __loginUsingLinkedin($socialId, $existEmail, $linkData, $isPlanAdmin){
	
		$saveData['User']['status'] = '1';
		$saveData['User']['activation_key'] = substr(md5(uniqid()), 0, 20);
	
		$id = $existEmail['User']['id'];
		$saveData['User']['id'] = $existEmail['User']['id'];
		$saveData['User']['socialId'] = $socialId;
		
		if($this->User->save($saveData,false))
		{
			if($linkData[5]!='' || $linkData[7]!='')
			{
				$socialLink = $this->Social->find('first',array('conditions'=>array('user_id'=>$id,'social_name like'=>'%linkedin%')));
				if(empty($socialLink))
				{
					$socialData['Social']['user_id'] = $id;
					$socialData['Social']['created_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				else
				{
					$socialData['Social']['id'] = $socialLink['Social']['id'];
					$socialData['Social']['modified_by'] = $id;
					$socialData['Social']['social_name'] = $linkData[5];
				}
				$this->Social->save($socialData);
		
				if($linkData[7]!='')
				{
					$twittLink = explode(',',$linkData[7]);
					$this->Social->deleteAll(array('Social.user_id' => $id,'social_name like'=>'%twitter%'),false);
					for($i=0;$i<count($twittLink);$i++)
					{
						$this->Social->create();
						$twitData['Social']['user_id'] = $id;
						$twitData['Social']['created_by'] = $id;
						$twitData['Social']['social_name'] = "http://www.twitter.com/".$twittLink[$i];
						$this->Social->save($twitData);
					}
				}
			}
	
			$user = $this->User->findById($id);
			if(isset($user['UserImage'][0]['image_name']) &&  count($user['UserImage'])>0)
				$this->Session->write('User.image',$user['UserImage'][0]['image_name']);
			else
				$this->Session->write('User.image','');
	
				$this->Session->write('Auth.User',$user['User']);
			}
		}
		
		function imageCrop($src,$cropdata,$dest,$targ_w =300,$targ_h=250)
		{
			$Imgpath = $src;
			$srcPath = str_replace("\\","/",$Imgpath.$cropdata['name']);
			$dest = str_replace("\\","/",$dest);
			$dest = $dest.DS.$cropdata['name'];
			$jpeg_quality = 100;
			$img_r = imagecreatefromjpeg($srcPath);
			$maxW = $targ_w;
			$maxH = $targ_h;
			$uploadSize = getimagesize($srcPath);
			$uploadWidth = $uploadSize[0];
			$uploadHeight = $uploadSize[1];
			$uploadType = $uploadSize[2];
		
			if ($uploadType != 1 && $uploadType != 2 && $uploadType != 3) {
				$this->error("File type must be GIF, PNG, or JPG to resize");
			}
		
			switch ($uploadType) {
				case 1: $srcImg = imagecreatefromgif($srcPath);
				break;
				case 2: $srcImg = imagecreatefromjpeg($srcPath);
				break;
				case 3: $srcImg = imagecreatefrompng($srcPath);
				break;
				default: $this->error("File type must be GIF, PNG, or JPG to resize");
			}
			/**$ratioX = $maxW / $uploadWidth;
			$ratioY = $maxH / $uploadHeight;
		
			#figure out new dimensions
			if (($uploadWidth == $maxW) && ($uploadHeight == $maxH)) {
				$newX = $uploadWidth;
				$newY = $uploadHeight;
			} else if (($ratioX * $uploadHeight) > $maxH) {
				$newX = $maxW;
				$newY = ceil($ratioX * $uploadHeight);
			} else {
				$newX = ceil($ratioY * $uploadWidth);
				$newY = $maxH;
			}
			imagefilter($srcImg, IMG_FILTER_GRAYSCALE);
			$dstImg = imagecreatetruecolor($newX, $newY);
			imagecopyresampled($dstImg, $srcImg, 0, 0, 0, 0, $newX, $newY, $uploadWidth, $uploadHeight);**/


                $ratioX = $maxW / $uploadWidth;
                $ratioY = $maxH / $uploadHeight;

                if ($ratioX < $ratioY) {
                    $newX = round(($uploadWidth - ($maxW / $ratioY)) / 2);
                    $newY = 0;
                    $uploadWidth = round($maxW / $ratioY);
                    $uploadHeight = $uploadHeight;
                } else {
                    $newX = 0;
                    $newY = round(($uploadHeight - ($maxH / $ratioX)) / 2);
                    $uploadWidth = $uploadWidth;
                    $uploadHeight = round($maxH / $ratioX);
                }
		imagefilter($srcImg, IMG_FILTER_GRAYSCALE);
                $dstImg = imagecreatetruecolor($maxW, $maxH);
                $white = imagecolorallocate($dstImg, 255, 255, 255);
                imagefill($dstImg, 0, 0, $white);
                imagecolortransparent($dstImg,$white);
                imagecopyresampled($dstImg, $srcImg, 0, 0, $newX, $newY, $maxW, $maxH, $uploadWidth, $uploadHeight);


			$write =imagejpeg($dstImg,$dest,$jpeg_quality);
			return $write;
		}
		
}   
?>