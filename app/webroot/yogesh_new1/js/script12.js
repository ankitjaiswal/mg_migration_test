// banner height 
$(function(){
	var DEBUG = false;
	if(!DEBUG){
	    if(!window.console) window.console = {};
	    var methods = ["log", "debug", "warn", "info"];
	    for(var i=0;i<methods.length;i++){
	        console[methods[i]] = function(){};
	    }
	}
	var windowHeight = $(window).height()-90;
	$("#banner-section").css("height",windowHeight)
});

// talk us toggle
$("#tell-more").click(function() {
	if(loggedUserId == ''){

		$(this).next(".form-interest").toggleClass("hide");
	}else{

		var purpose = $('#purpose').val();
		$(".form-group.form-interest").addClass("hide");

		$.ajax({
			url:SITE_URL+'members/loggedin_service_request',
			type:'post',
			dataType:'json',
			data: 'loggedUserId='+loggedUserId + '&purpose=' + purpose,
			success:function(loggedUserId){
				$(".form-success").removeClass("hide");
			}
		});	
		return false;
	}
});

//know more click to scroll down
$('#banner-section .get-started').click(function(){
	$(".form-interest").removeClass("hide");
	$('html, body').animate({
		scrollTop: $("#interested-section").offset().top -100
	}, 1000);
	return false;
});


// validation form getstarted
$("#contact-form").validate({
	rules: {
		name: {
			required: true
		},
		email: {
			required: true,
			email:  true
		},
		contact_no: {
			required: true,
			minlength: 10,
			//maxlength: 10,
			//number: true
		}
	},
	messages:{
		name: {
			required: 'Please Enter Name', 
		},
		email:{
			required:   "Please Enter Email",
			email:      "Please Enter Valid Email",
		},
		contact_no: {
			required:   "Please Enter Mobile Number",
			minlength:  "Number Should Not less then 10 Digits",
			//maxlength:  "Number Should Not Exceed 10 Digits",
			//number:     "Only Numbers Allowed"
		}
	},
	submitHandler: function() {
		var name = $('#name').val();
		var phone = $('#contact_no').val();
		var email = $('#email_id').val();
		var purpose = $('#purpose').val();
		var data = {'email':email,'phone':phone,'name':name,'purpose':purpose};

		$.ajax({
			url:SITE_URL+'members/service_request',
			type:'post',
			dataType:'json',
			data: data,
			success:function(loggedUserId){
				$(".form-group.form-interest").addClass("hide");
				$(".form-success").removeClass("hide");
			}
		});	
		return false;
	}
});

$("#loginButtonclick").click(function(){


	var mailformat=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var flag=0;

	if($.trim($('#password').val())=='')
	{
		$('#password').css('border-color','#F00');
		flag++;
	}else{
		$('#password').css('border-color','');
	}
	if($.trim($('#email').val())=='')
	{
		$('#LoginEmailErr').hide();
		$('#LoginPassErr').hide();
		$('#email').css('border-color','#F00');
		flag++;
	}else if(!$('#email').val().match(mailformat))
	{
		$('#email').css('border-color','#F00');
		$('#LoginEmailErr').html('Invalid Email Id');
		$('#LoginEmailErr').show();
		flag++;
	}else{
		var email=$('#email').val();
		var pass=$('#password').val();
		$.ajax({
			url:SITE_URL+'/users/checkEmailPassword',
			type:'post',
                        dataType:'json',
			data:'email='+email+'&pass='+pass,
			success:function(res)
			{
				if(res.value==0)
				{
					if(res.pass=='email')
					{
						$('#LoginEmailErr').html('Invalid Username & password');
						$('#email').css('border-color','#F00');
						$('#password').css('border-color','#F00');
						$('#LoginPassErr').hide();
						$('#LoginEmailErr').show();
						flag = 1;
					}
					if(res.pass=='wrong')
					{
						$('#email').css('border-color','');
						$('#password').css('border-color','#F00');
						$('#LoginPassErr').html('Invalid Password');
						$('#LoginPassErr').show();
						$('#LoginEmailErr').hide();
						flag = 1;
					}
				}
				if(flag>0)
				{
					return false;
				}else{

 
		                  $.ajax({
					url:SITE_URL+'users/login',
					type:'post',
					dataType:'json',
					data: 'email='+email+'&pass='+pass,
					success:function(id){
                                                
                                                $(".modal loginModal fade").modal('hide');
                                                location.reload();
						//alert("login successed");
					}
				   });	
		       
		   		   return false;
				}
			}
			});
		}
	return false;
});


$("#linkedinclick").click(function(){
var url = location.href;

$.ajax({
			url:SITE_URL+'/users/sessionlocation',
			type:'post',
                        dataType:'json',
			data:'url='+url,
			success:function(res)
			{
                          
                        }

});

//alert(url);
});


// ForgotPassword Validation
$("#forgotbutton").click(function(){
        var email=$('#forgetProceess #email').val();

	var mailformat=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var flag=0;
	
	if((email)=='')
	{    
		$('#ForgotEmailErr').hide();
		$('#forgetProceess #email').css('border-color','#F00');
		flag++;
	}else if(!email.match(mailformat))
	{   
		$('#forgetProceess #email').css('border-color','#F00');
		$('#ForgotEmailErr').html('Invalid Email Id');
		$('#ForgotEmailErr').show();
		flag++;
	}else{
		

		
		$.ajax({
			url:SITE_URL+'/users/checkEmailForgot',
			type:'post',
            dataType:'json',
			data:'email='+email,
			success:function(res)
			{
			if(res.value){
				if(res.status=='inactive'){
					$('#ForgotEmailErr').html('Your account has been deactivated please contact site administrator');
					$('#forgetProceess #email').css('border-color','#F00');
					flag = 1;
				}
				else{
					$('#ForgotEmailErr').html('');
					$('#forgetProceess #email').css('border-color','');
				}
			}
			else{
				$('#ForgotEmailErr').html('Email ID does not exist');
				$('#forgetProceess #email').css('border-color','#F00');
				flag = 1;
			}
			    if(flag>0)
				{

					return false;
				}else{
					
					$("#forgetProceess").submit();
				}
			}
			
			
		  });
		 }
	return false;

});


// signup for client & member Validation

$("#registerbutton").click(function(){

	
	        var fname = $('#signupProceess #fname').val();
		var lname = $('#signupProceess #lname').val();
		var email = $('#signupProceess #email').val();
		var pass = $('#signupProceess #password').val();
		var zipcode = $('#signupProceess #zipcode').val();

		
		var mailformat=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		var onlyLetters=/^[a-zA-Z _!@#"$%\^\-:.<>?=&'()*\+,\/;\[\\\]\^_`{|}~]+$/;
		var onlyNumbers=/^[0-9]*$/;
	        var flag= 0;
		
		if((fname)=='' || (!fname.match(onlyLetters))){
			$('#signupProceess #fname').css('border-color','#F00');
			flag = 1;
		}else{
			$('#signupProceess #fname').css('border-color','');
		}

		if((lname)=='' || (!fname.match(onlyLetters))){
			$('#signupProceess #lname').css('border-color','#F00');
			flag = 1;
		}else{
			$('#signupProceess #lname').css('border-color','');
		}		
	
		if((pass)==''){
			$('#signupProceess #password').css('border-color','#F00');
			$('#PassLengtherr').hide();
			flag = 1;
		}else{
			if(pass.length<6 || pass.length>20){
				$('#signupProceess #password').css('border-color','#F00');
				$('#PassLengtherr').html('Password should be between 6 and 20 characters in length');
				$('#PassLengtherr').show();
				flag = 1;
			}
			else{
			$('#signupProceess #password').css('border-color','');
			$('#PassLengtherr').hide();
		    }
		}

		if((zipcode)==''){
			$('#signupProceess #zipcode').css('border-color','#F00');
			flag = 1;
		}else{
			if(zipcode.length<4 || (!zipcode.match(onlyNumbers))){
				$('#signupProceess #zipcode').css('border-color','#F00');
				$('#zipError').html('Invalid ZIP');
				$('#zipError').show();
				flag = 1;
			}
			else{			
			$('#signupProceess #zipcode').css('border-color','');
			$('#signupProceess #zipcode').html('');
			}
		}		
		
		if((email)==''){
			$('#signupProceess #email').css('border-color','#F00');
			$('#UserEmailErr').hide();
			flag = 1;
		}else{
			if(!email.match(mailformat)){
			$('#signupProceess #email').css('border-color','#F00');
                        $('#UserEmailErr').html('Invalid Email Id');
			$('#UserEmailErr').show();	
                        flag = 1;			
			}

			else{

			var id=jQuery('#popupId').val();

			$.ajax({
			url:SITE_URL+'/members/checkEmail',
			type:'post',
                        dataType:'json',
			data:'email='+email+'&id='+id,
			success:function(res)
			 {
				if(res.value){
					$('#signupProceess #email').css('border-color','#F00');
					$('#UserEmailErr').html('Existing user. Please login.');
					$('#UserEmailErr').show();
					flag = 1;
				}
				else{
					$('#UserEmailErr').html('');
					$('#signupProceess #email').css('border-color','');
					$('#UserEmailErr').hide();
				}

				if(flag>0)
                                        {
					return false;
					}
				else{
					jQuery("#signupProceess").submit();
                                        return true;
					}
			 }

			});	
			}
               }
	return false;		
});	
	


// signup for member Validation

         $("#memberregisterbutton").click(function(){

	        var fname = $('#userfname').val();
		var lname = $('#userlname').val();
		var email = $('#useremail').val();
		var pass = $('#userpassword').val();
		var zipcode = $('#userzipcode').val();

		var mailformat=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		var onlyLetters=/^[a-zA-Z _!@#"$%\^\-:.<>?=&'()*\+,\/;\[\\\]\^_`{|}~]+$/;
		var onlyNumbers=/^[0-9]*$/;
	        var flag= 0;
		
		if((fname)=='' || (!fname.match(onlyLetters))){
			$('#userfname').css('border-color','#F00');
			flag = 1;
		}else{
			$('#userfname').css('border-color','');
		}

		if((lname)=='' || (!fname.match(onlyLetters))){
			$('#userlname').css('border-color','#F00');
			flag = 1;
		}else{
			$('#userlname').css('border-color','');
		}		
	
		if((pass)==''){
			$('#userpassword').css('border-color','#F00');
			$('#UpassLengtherr').hide();
			flag = 1;
		}else{
			if(pass.length<6 || pass.length>20){
				$('#userpassword').css('border-color','#F00');
				$('#UpassLengtherr').html('Password should be between 6 and 20 characters in length');
				$('#UpassLengtherr').show();
				flag = 1;
			}
			else{
			$('#userpassword').css('border-color','');
			$('#UpassLengtherr').hide();
		    }
		}

		if((zipcode)==''){
			$('#userzipcode').css('border-color','#F00');
			flag = 1;
		}else{
			if(zipcode.length<4 || (!zipcode.match(onlyNumbers))){
				$('#userzipcode').css('border-color','#F00');
				$('#UzipError').html('Invalid ZIP');
				$('#UzipError').show();
				flag = 1;
			}
			else{			
			$('#userzipcode').css('border-color','');
			$('#userzipcode').html('');
			}
		}		
		
		if((email)==''){
			$('#useremail').css('border-color','#F00');
			$('#EmailErr').hide();
			flag = 1;
		}else{
			if(!email.match(mailformat)){
			$('#useremail').css('border-color','#F00');
                        $('#EmailErr').html('Invalid Email Id');
			$('#EmailErr').show();	
                        flag = 1;			
			}

			else{

			var id=jQuery('#popupId').val();
                        alert(id);
			$.ajax({
			url:SITE_URL+'/members/checkEmail',
			type:'post',
                        dataType:'json',
			data:'email='+email+'&id='+id,
			success:function(res)
			 {
				if(res.value){
					$('#useremail').css('border-color','#F00');
					$('#EmailErr').html('Existing user. Please login.');
					$('#EmailErr').show();
					flag = 1;
				}
				else{
					$('#EmailErr').html('');
					$('#useremail').css('border-color','');
					$('#EmailErr').hide();
				}

				if(flag>0)
                                        {
					return false;
					}
				else{
					jQuery("#msignupProceess").submit();
                                        return true;
					}
			 }

			});	
			}
               }
	return false;		
});



// hover show model
$('.profile-dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(300);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(300);
});
