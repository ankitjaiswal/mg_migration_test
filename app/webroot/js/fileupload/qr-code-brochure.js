jQuery(document).ready(function(){		

	var hash = jQuery("#UserHash").val();
	var logouploader = new qq.FileUploader({

		element: document.getElementById('vertical_logo'),

		action: SITE_URL+'/members/file_upload/'+hash,

		debug: true,

		multiple: false,				

		allowedExtensions:['jpg','jpeg','png','gif'],

		params: {

			tp: 'vlogo'					

		},

		onSubmit: function(id, fileName){			
			return;
			//jQuery(".qq-upload-list").append(fileName+" Image cancel, You can upload only 5 images <br/>");
			//return false;					
			$('#qq-upload-list-vertical_logo').html('');
			

		},

		onProgress: function(id, fileName, loaded, total){

		

		},

		onComplete: function(id, fileName, responseJSON){								
			
			if(responseJSON.success == true){
				jQuery(".qq-upload-list").html('');
				var inputStr = "<input type='hidden' name='data[UserImage][image_name]' value='"+responseJSON.filename+"'/>";
				//jQuery("#userImageList").append(inputStr);				
				//console.log('<img src='+'"'+SITE_URL+'img/mentors/profile_images/'+hash+'/'+responseJSON.filename+'"'+'>');
				//jQuery("#profileImage").html('<img src="'+SITE_URL+'/img/mentors/profile_images/'+hash+'/'+responseJSON.filename+'"'+'>');
				if(mentee_url=='mentee'){
					jQuery("#profileImage img").attr('src',SITE_URL+'/img/clients/profile_images/'+hash+'/'+responseJSON.filename);
					jQuery('#inner-nav-list a img').attr('src',SITE_URL+'/img/clients/profile_images/'+hash+'/'+responseJSON.filename);
					document.getElementById('image_name').value = 'data[UserImage][image_name]';
				}
				else
				{
					jQuery("#profileImage img").attr('src',SITE_URL+'/img/members/profile_images/'+hash+'/'+responseJSON.filename);
					jQuery('#inner-nav-list a img').attr('src',SITE_URL+'/img/members/profile_images/'+hash+'/'+responseJSON.filename);
					document.getElementById('image_name').value = 'data[UserImage][image_name]';
				}
				if(jQuery("#UserImageImageTempName").length > 0){
					jQuery("#UserImageImageTempName").val(responseJSON.filename);
				}
			}

		}

	});

});