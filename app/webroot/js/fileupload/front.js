/* under development alert */
jQuery("[href='#']").click(function(){
	alert('Under Development');
});

function invitation_popup() {
	var path=SITE_URL+'/users/invitation/';
	TINY.box.show({url:path,width:595,height:580});
}

function client_invitation_popup() {
	var path=SITE_URL+'/users/client_invitation/';
	TINY.box.show({url:path,width:595,height:580});
}

function loginpopup() {
	request = {};
     url = SITE_URL+'users/checkUser';
	 jQuery.post(url,function(request)
	  {
		  if(request==0)
		  {
			  var path=SITE_URL+'/users/login/';
			  TINY.box.show({url:path,width:512,height:430});
		  }
		  else
		  {
		  	window.location.reload(true);
		  }
	  }
	  );	
}

function pricingloginpopup(plan_id) {
	request = {};
     url = SITE_URL+'users/checkUser';
	 jQuery.post(url,function(request)
	  {
		  if(request==0)
		  {
			  var path=SITE_URL+'/pricing/login/plan_id_for_login:'+plan_id;
			  TINY.box.show({url:path,width:512,height:430});
		  }
		  else
		  {
		  	window.location.reload(true);
		  }
	  }
	  );	
}

function loginpopup_question(q_url) {
	request = {};
    url = SITE_URL+'users/checkUser';
	 jQuery.post(url,function(request)
	  {
		  if(request==0)
		  {
			  var path=SITE_URL+'/users/login/question_url:'+q_url;
			  TINY.box.show({url:path,width:512,height:430});
		  }
		  else
		  {
		  	window.location.reload(true);
		  }
	  }
	  );	
}

function register_popup(){
	var path=SITE_URL+'users/register/';
	//TINY.box.show({url:SITE_URL+'/users/register',width:500,fixed:true});
	//TINY.box.show({url:SITE_URL+'/users/register',width:500,height:470});/* VL 27/12 change height*/
	TINY.box.show({url:path,width:780,height:390})
}
function plan_register_popup(){
	var path=SITE_URL+'users/plan_register/';
	//TINY.box.show({url:SITE_URL+'/users/register',width:500,fixed:true});
	//TINY.box.show({url:SITE_URL+'/users/register',width:500,height:470});/* VL 27/12 change height*/
	TINY.box.show({url:path,width:512,height:480})
}

function register_step2(){
	window.location.href=SITE_URL+'users/registration_step2';
}

function public_feedback_popup(id){
    var path=SITE_URL+'/feedback/feedbackpublic/';
    //TINY.box.show({url:SITE_URL+'/users/register',width:500,fixed:true});
    TINY.box.show({url:SITE_URL+'/feedback/feedbackpublic/'+id,width:500,height:400});/* VL 27/12 change height*/
}

function other_testimonials(id){

	TINY.box.show({url:SITE_URL+'/users/other_testimonials/'+id,width:1000,height:550});
}

function feedback_popup(){
	var path=SITE_URL+'/users/feedback/';
	//TINY.box.show({url:SITE_URL+'/users/register',width:500,fixed:true});
	TINY.box.show({url:SITE_URL+'/users/feedback',width:500,height:425});/* VL 27/12 change height*/
}

function feedbackform_popup(mentor_id,mentorship_id){
	TINY.box.show({url:SITE_URL+'/clients/give_feedback/'+mentor_id+'/'+mentorship_id,width:500,height:400});
}

function reportissue_popup(mentorship_id,from){
	TINY.box.show({url:SITE_URL+'invoice/report_issue/'+mentorship_id+'/'+from,width:500,height:340});
}

function feedbackdetail_popup(){
	var path=SITE_URL+'/users/feedbackdetail/';
	//TINY.box.show({url:SITE_URL+'/users/register',width:500,fixed:true});
	TINY.box.show({url:SITE_URL+'/users/feedbackdetail',width:500,height:399});/* VL 27/12 change height*/
}
/* apply for mentorship loginpopup */
function applyloginpopup(mentor_id){
	var path=SITE_URL+'/users/login/mentor_id:'+mentor_id;
	TINY.box.show({url:path,width:512,height:403})
	
}
function applyregisterpopup(){
	register_popup();
	
}
/* password_setting_popup */
function passwordSettingPopup(){
	//alert('hiiiii');
	var path=SITE_URL+'/members/password_setting_popup';
	OPENTINY.box.show({url:path,width:500,height:512,close:true})
	
	
}


function pricingAuthPopSettingPopup(plan_id){
	
	var path = SITE_URL+'pricing/pricing_auth_popup/'+plan_id;
	TINY.box.show({url:path,width:500,height:512})
	
	
}

function forgotPopup(){
	var path=SITE_URL+'/users/forgot/';
	//TINY.box.show({url:SITE_URL+'/users/register',width:500,fixed:true});
	TINY.box.show({url:SITE_URL+'/users/forgot',width:509,height:240});/* VL 27/12 change height*/
}
/*
function forgot_thanks()
{
	TINY.box.show({url:SITE_URL+'/users/forgot_thanks',width:509,height:237});	
}*/
function validateReset()
{
	var flag = 0;
	var passlength = 0;
	var pass1 = jQuery.trim(jQuery('#newPass').val());
	var pass2 = jQuery.trim(jQuery('#confPass').val())
	if(pass1=='' || pass2=='')
	{
		if(pass1=='')
		{
			jQuery('#newPass').css('border-color','#F00');
			jQuery('#errorMessage').hide();
			flag++;	
		}
		if(pass2=='')
		{
			jQuery('#confPass').css('border-color','#F00');
			jQuery('#errorMessage').hide();
			flag++;	
		}
	}
	else if((pass1.length<6 || pass1.length>20) || (pass2.length<6 || pass2.length>20))
		 {
		 	if(pass1.length<6 || pass1.length>20)
			{
				jQuery('#newPass').css('border-color','#F00');
				jQuery('#errorMessage').html('Password should be between 6 and 20 characters in length');
				jQuery('#errorMessage').show();
				flag++;	
			}
			if(pass2.length<6 || pass2.length>20)
			{
				jQuery('#confPass').css('border-color','#F00');
				jQuery('#errorMessage').html('Password should be between 6 and 20 characters in length');
				jQuery('#errorMessage').show();
				flag++;	
			}
		 }
	if(pass1!=pass2)
	{
		jQuery('#errorMessage').html('Passwords do not match');
		jQuery('#errorMessage').show();
		flag++;		
	}
	if(flag > 0) 
	{						
		return false;
	} else {
		jQuery("#resetPassForm").submit();
	}	
}
function validateforgot()
{
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	var errorflag = 0;

	if(jQuery.trim(jQuery('#forgotEmail').val()) == '') {
		jQuery('#ForgotEmailErr').hide();	
		jQuery('#forgotEmail').css('border-color','#F00');			
		errorflag++;
	} else if(!jQuery('#forgotEmail').val().match(mailformat)) {
		jQuery('#forgotEmail').css('border-color','#F00');
		jQuery('#ForgotEmailErr').html('Invalid Email Id');
		jQuery('#ForgotEmailErr').show();
		errorflag++;
	}else{
		var email = jQuery('#forgotEmail').val();	
		jQuery.ajax({
			url:SITE_URL+'/users/checkEmailForgot',
			type:'post',
			dataType:'json',
			data:'email='+email,
			success:function(res){
				if(res.value){
				    if(res.status=='inactive')
				    {
				       jQuery('#ForgotEmailErr').html('Your account has been deactivated please contact site administrator');   
                       jQuery('#forgotEmail').css('border-color','#F00');
                       errorflag++; 
				    }
				    else
				    {
				        jQuery('#ForgotEmailErr').html('');
                        jQuery('#forgotEmail').css('border-color','');
				    }
					
				}else{
					jQuery('#ForgotEmailErr').html('Email ID does not exist');	
					jQuery('#forgotEmail').css('border-color','#F00');
					errorflag++;
				}
				if(errorflag > 0) {						
					return false;
				} else {
					jQuery("#forgotForm").submit();
				}					
			}
		});	
				
	}
	return false;
	
}
function validateLogin(){
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	var flag = 0;

	if(jQuery.trim(jQuery('#UserPassword').val()) == '') {
		jQuery('#UserPassword').css('border-color','#F00');			
		flag++;
	} else {
		jQuery('#UserPassword').css('border-color','');	
	}

	if(jQuery.trim(jQuery('#loginEmail').val()) == '') {
		jQuery('#LoginEmailErr').hide();	
		jQuery('#LoginPassErr').hide();	
		jQuery('#loginEmail').css('border-color','#F00');			
		flag++;
	} else if(!jQuery('#loginEmail').val().match(mailformat)) {
		jQuery('#loginEmail').css('border-color','#F00');
		jQuery('#LoginEmailErr').html('Invalid Email Id');
		jQuery('#LoginEmailErr').show();
		flag++;
	}else{
		var email = jQuery('#loginEmail').val();
		var pass = jQuery('#UserPassword').val();		
		jQuery.ajax({
			url:SITE_URL+'/users/checkEmailPassword',
			type:'post',
			dataType:'json',
			data:'email='+email+'&pass='+pass,
			success:function(res){
				if(res.value==0)
				{
					if(res.pass=='email')
					{
						jQuery('#LoginEmailErr').html('Invalid Username & password');
						jQuery('#loginEmail').css('border-color','#F00');
						jQuery('#UserPassword').css('border-color','#F00');
						jQuery('#LoginPassErr').hide();	
						jQuery('#LoginEmailErr').show();	
						flag++;
					}
					if(res.pass=='wrong')
					{
						jQuery('#loginEmail').css('border-color','');
						jQuery('#UserPassword').css('border-color','#F00');
						jQuery('#LoginPassErr').html('Invalid Password');	
						jQuery('#LoginPassErr').show();	
						jQuery('#LoginEmailErr').hide();	
						flag++;
					}
				}
				if(flag > 0) {	
					//jQuery('#LoginEmailErr').show();
					//jQuery('#LoginPassErr').show();
					return false;
				} else {
					jQuery("#loginForm").submit();
				}					
			}
		});	
				
	}
	return false;

}
function validateRegistration() {

	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var pass = jQuery.trim(jQuery('#popupPassword').val());
	var zipValue = jQuery.trim(jQuery('#popupZipcode').val());
	var onlyNumbers = /^[0-9]*$/.test(jQuery.trim(jQuery("#popupZipcode").val()));
	
	var filter = /^[a-zA-Z _!@#"$%\^\-:.<>?=&'()*\+,\/;\[\\\]\^_`{|}~]+$/;
	

	var flag = 0;

	if(jQuery.trim(jQuery('#popupFirstName').val()) == '' || filter.test(jQuery.trim(jQuery('#popupFirstName').val())) == false) {
		jQuery('#popupFirstName').css('border-color','#F00');			
		flag++;
	} else {
		jQuery('#popupFirstName').css('border-color','');	
	}

	if(jQuery.trim(jQuery('#popupLastName').val()) == '' || filter.test(jQuery.trim(jQuery('#popupLastName').val())) == false) {
		jQuery('#popupLastName').css('border-color','#F00');			
		flag++;
	} else {
		jQuery('#popupLastName').css('border-color','');	
	}

	if(jQuery.trim(jQuery('#popupPassword').val()) == '') {
		jQuery('#popupPassword').css('border-color','#F00');
		jQuery('#PassLengtherr').hide();
		flag++;
	} else {
			if(pass.length<6 || pass.length>20) 
			{
				jQuery('#popupPassword').css('border-color','#F00');
				jQuery('#PassLengtherr').html('Password should be between 6 and 20 characters in length');
				jQuery('#PassLengtherr').show();
				flag++;
			}
			else
			{ 	jQuery('#popupPassword').css('border-color','');
				jQuery('#PassLengtherr').hide();
			}
	}
	
	if(jQuery.trim(jQuery('#popupZipcode').val()) == '') {
		jQuery('#popupZipcode').css('border-color','#F00');			
		flag++;
	} else /*if(!onlyNumbers || zipValue.length<4){
				jQuery('#popupZipcode').css('border-color','#F00');
				jQuery('#popupzipError').html('Invalid ZIP');
				jQuery('#popupZipcode').focus();
				flag++;
			}else */{
				jQuery('#popupZipcode').css('border-color','');
				jQuery('#popupzipError').html('');	
			}			
	
	if(jQuery.trim(jQuery('#popupUserName').val()) == '') {
		jQuery('#UserEmailErr').hide();	
		jQuery('#popupUserName').css('border-color','#F00');			
		flag++;
	} else if(!jQuery('#popupUserName').val().match(mailformat)) {
		jQuery('#popupUserName').css('border-color','#F00');
		jQuery('#UserEmailErr').html('Invalid Email Id');
		jQuery('#UserEmailErr').show();
		flag++;
	}  else {
		var email = jQuery('#popupUserName').val();
		var URL = SITE_URL+'/users/checkEmail';
		jQuery.ajax({
			url:URL,
			type:'post',
			dataType:'json',
			data:'email='+email,
			success:function(res){
				
				if(parseInt(res.value)>0){
					
					jQuery('#popupUserName').css('border-color','#F00');
					jQuery('#UserEmailErr').html('Existing user. Please login.');
					jQuery('#UserEmailErr').show();
					flag++;
				}else{
					jQuery('#UserEmailErr').html('');
					jQuery('#popupUserName').css('border-color','');
					jQuery('#UserEmailErr').hide();
					
				}
				if(flag > 0) {
					TINY.box.resize();
					return false;
				} else {
					
					jQuery("#planregisterForm").submit();
					return true;
				}					
				
			}
		});
		
	}
	return false;
	
}

function passwordSettingPopupValidate(){
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var pass = jQuery.trim(jQuery('#UserPassword2').val());
	var zipValue = jQuery.trim(jQuery('#zipcode').val());
	var onlyNumbers = /^[0-9]*$/.test(jQuery.trim(jQuery("#zipcode").val()));
	var onlyLetters = /^[a-zA-Z _!@#"$%\^\-:.<>?=&'()*\+,\/;\[\\\]\^_`{|}~]+$/;
	var flag = 0;

	if(jQuery.trim(jQuery('#fname').val()) == '' || !jQuery('#fname').val().match(onlyLetters)) {
		jQuery('#fname').css('border-color','#F00');			
		flag++;
	} else {
		jQuery('#fname').css('border-color','');	
	}

	if(jQuery.trim(jQuery('#lname').val()) == '' || !jQuery('#lname').val().match(onlyLetters)) {
		jQuery('#lname').css('border-color','#F00');			
		flag++;
	} else {
		jQuery('#lname').css('border-color','');	
	}

	if(jQuery.trim(jQuery('#UserPassword2').val()) == '') {
		jQuery('#UserPassword2').css('border-color','#F00');	
		jQuery('#PassLengtherr').hide();
		flag++;
	}else {
			if(pass.length<6 || pass.length>20) 
			{
				jQuery('#UserPassword2').css('border-color','#F00');
				jQuery('#PassLengtherr').html('Password should be between 6 and 20 characters in length');
				jQuery('#PassLengtherr').show();
				flag++;
			}
			else
			{ 	jQuery('#UserPassword2').css('border-color','');
				jQuery('#PassLengtherr').hide();
			}
	}
	if(jQuery.trim(jQuery('#zipcode').val()) == '') {
		
		jQuery('#zipcode').css('border-color','#F00');			
		flag++;
	}else if(!onlyNumbers || zipValue.length<4 ){
				jQuery('#zipcode').css('border-color','#F00');
				jQuery('#zipError').html('Invalid ZIP');
				jQuery('#zipcode').focus();
				flag++;
			}else {
				jQuery('#zipcode').css('border-color','');
				jQuery('#zipError').html('');	
			}
	
	if(jQuery.trim(jQuery('#email').val()) == '') {
		jQuery('#UserEmailErr').hide();	
		jQuery('#email').css('border-color','#F00');			
		flag++;
	} else if(!jQuery('#email').val().match(mailformat)) {
		jQuery('#email').css('border-color','#F00');
		jQuery('#UserEmailErr').html('Invalid Email Id');
		jQuery('#UserEmailErr').show();
		flag++;
	}  else {
		var email = jQuery('#email').val();
		var id = jQuery('#popupId').val();
		var URL = SITE_URL+'/members/checkEmail';
		jQuery.ajax({
			url:URL,
			type:'post',
			dataType:'json',
			data:'email='+email+'&id='+id,
			success:function(res){
				
				if(parseInt(res.value)>0){
					
					jQuery('#email').css('border-color','#F00');
					jQuery('#UserEmailErr').html('Existing user. Please login.');
					jQuery('#UserEmailErr').show();
					flag++;
				}else{
					jQuery('#UserEmailErr').html('');
					jQuery('#email').css('border-color','');
					jQuery('#UserEmailErr').hide();
					
				}
				if(flag > 0) {					
					return false;
				} else {					
					jQuery("#passwordSettingPopupForm").submit();
					return true;
				}					
				
			}
		});
		
	}
	return false;

}

function subscribe()
{
	var path=SITE_URL+'news_letters/subscribe/';
	if(loggedUserId!='')
	{
		TINY.box.show({url:SITE_URL+'news_letters/subscribe',width:390,height:114,closejs:function(){window.location.reload(true);}});
	}
	else
	{
		TINY.box.show({url:SITE_URL+'news_letters/subscribe',width:523,height:219,closejs:function(){window.location.reload(true);}});
	}
	/* VL 27/12 change height*/	
}
function validatesubscribe()
{
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	var flag = 0;

	if(jQuery.trim(jQuery('#subscribeEmail').val()) == '') {
		jQuery('#SubscribeEmailErr').hide();	
		jQuery('#subscribeEmail').css('border-color','#F00');			
		flag++;
	} else if(!jQuery('#subscribeEmail').val().match(mailformat)) {
		jQuery('#subscribeEmail').css('border-color','#F00');
		jQuery('#SubscribeEmailErr').html('Invalid Email Id');
		jQuery('#SubscribeEmailErr').show();
		flag++;
	}else{
		var email = jQuery('#subscribeEmail').val();	
		jQuery.ajax({
			url:SITE_URL+'/news_letters/checkEmail',
			type:'post',
			dataType:'json',
			data:'email='+email,
			success:function(res){
				if(res.value){
					jQuery('#SubscribeEmailErr').html('You have already subscribed');			
					flag++;
										
				}
				if(flag > 0) {						
					return false;
				} else {
					jQuery.ajax({
						url:SITE_URL+'/news_letters/subscribe',
						type:'post',
						dataType:'json',
						data:'email='+email,
						success:function(res){
							if(parseInt(res.value)=='1')
							{
								var path=SITE_URL+'news_letters/unsubmsg/';
								//TINY.box.show({url:SITE_URL+'news_letters/unsubmsg/1',width:390,height:114,closejs:function(){window.location.reload(true);}});
								jQuery('#registerationFm').hide();
								jQuery('.tinner').css('height','114');
								jQuery('.tinner').css('width','390');
								jQuery('#succmsg').show();
							}
							
						}
					});	
				}					
			}
		});	
				
	}
	return false;
	
}

function unsubscribe()
{
	jQuery.ajax({
			url:SITE_URL+'/news_letters/unsubscribe',
			type:'post',
			dataType:'json',
			data:'',
			success:function(res){
				var path=SITE_URL+'news_letters/unsubmsg/';
				TINY.box.show({url:SITE_URL+'news_letters/unsubmsg/2',width:390,height:114,closejs:function(){window.location.reload(true);}});
			}
		});	
}

function delAccount()
{
	if(confirm('Are you sure you want to delete your account?')==true)
	{
		jQuery.ajax({
			url:SITE_URL+'/members/status_change',
			type:'post',
			dataType:'json',
			data:'',
			success:function(res){
				var path=SITE_URL+'members/delAccount/';
				TINY.box.show({url:SITE_URL+'members/delaccount',width:387,height:77,closejs:function(){window.location.href = SITE_URL;}});
			}
		});	
	}
}

function validAccountSetting()
{
    var flag = 0;
	var pass = jQuery.trim(jQuery('#UserNewpass').val());
	jQuery('#UserOldpass').css('border-color','');
	jQuery('#OldNotMatch').css('display','none');
	if(jQuery.trim(jQuery('#UserOldpass').val()) != '' || jQuery.trim(jQuery('#UserNewpass').val()) != '' || jQuery.trim(jQuery('#UserConfpass').val()) != '') 
	{
		if(jQuery.trim(jQuery('#UserOldpass').val()) == '')	
		{
			jQuery('#UserOldpass').css('border-color','#F00');
			flag++;
		}
		else
			jQuery('#UserPassword').css('border-color','');
			
		if(jQuery.trim(jQuery('#UserNewpass').val()) == '')	
		{
			jQuery('#UserNewpass').css('border-color','#F00');
			jQuery('#commonerror').hide();
			flag++;
		}
		else
		{
			if(pass.length<6 || pass.length>20) 
			{
				jQuery('#UserNewpass').css('border-color','#F00');
				jQuery('#commonerror').html('Password should be between 6 and 20 characters in length');
				jQuery('#commonerror').show();
				flag++;
			}
			else
			{ 	jQuery('#UserNewpass').css('border-color','');
				jQuery('#commonerror').hide();
			}	
		}
		
		if(jQuery.trim(jQuery('#UserConfpass').val()) == '')	
		{
			jQuery('#UserConfpass').css('border-color','#F00');
			flag++;
		}
		else
			jQuery('#UserConfpass').css('border-color','');	
		if(jQuery.trim(jQuery('#UserNewpass').val()) != jQuery.trim(jQuery('#UserConfpass').val()) != '')
		{
			jQuery('#commonerror').html('New & confirm passwords do not match');
			jQuery('#UserNewpass').css('border-color','#F00');
			jQuery('#UserConfpass').css('border-color','#F00');
			jQuery('#commonerror').show();
			flag++;
		}
		if(flag>0)
		{
			return false;	
		}
		else
		{
			jQuery("#accountSettingForm").submit();	
		}
	}
	if(flag==0)
	{
		jQuery("#accountSettingForm").submit();	
	}
	return true;
}

function redirect_q() {

    var qId = jQuery('#question_url').val();

    window.location.href = SITE_URL+'linkedin/signuplinkclientpopup.php?questionURL='+qId;
}
 
function linkedin(type,id)
{	
	if(typeof type == "undefined")
	{
		window.location.href = SITE_URL+'linkedin/signuplink.php';
		return true;
	}
	if(type=='mentor')
	{
		window.location.href = SITE_URL+'linkedin/signuplinkmember.php';
		return true;	
	}	
	if(type=='mentorPopup')
	{
		window.location.href = SITE_URL+'linkedin/signuplinkmemberpopup.php?userId='+id;
		return true;		
	}
	if(type=='menteePopup')
	{
		window.location.href = SITE_URL+'linkedin/signuplinkclientpopup.php?userId='+id;
		return true;		
	}
	if(type=='pricingPopUp')
	{
		window.location.href = SITE_URL+'linkedin/signuplinkclientpopup.php?planUserId='+id;
		return true;		
	}
       if(type=='sresult')
       {

              window.location.href = SITE_URL+'linkedin/signuplinkclientpopup.php?sresult='+id;
		return true;
       }
	if(type == 'pricingAuthPopUp')
	{
		window.location.href = SITE_URL+'linkedin/signuplinkclientpopup.php?plan_id='+id;
		return true;
	}
}

function unlink_resource(source_id){
	if(confirm("Are you sure you want to remove this resource?")){
		unlink_resource_from_db(source_id);
	}
}

function unlink_resource_from_db(source_id){
	already_add  = jQuery('input[name="data[UserReference][resource_id][]"]').length;
	jQuery.ajax({
		url:SITE_URL+'/users/unlink_resource/',
		type:'post',
		data:'source_id='+source_id,
		success:function(res){
			if(res == 1 || res == "1"){			
				jQuery("#my_uploded_resource_"+source_id).hide(500,function(){
					if(already_add > 1){
						if_first = jQuery("#my_uploded_resource_"+source_id).find('#add').length ;
					}
					jQuery("#my_uploded_resource_"+source_id).remove();
					add_count= jQuery('input[name="data[UserReference][resource_location][]"]').length;
					if(already_add > 1){
						if(if_first == 1){
							jQuery('.plus_symbol:first').each(function() {
								jQuery(this).html('');
								jQuery(this).append('<a id="add" style="cursor:pointer"><img src="'+SiteUrl+'img/media/add.png" style="cursor:pointer" height="19" width="24" alt="" /></a>');
								//jQuery(this).parent().find('#remove').remove();
							});
						}
						
					}else {
						if(add_count == 0)
						{
						jQuery('<div id="dynamic-Resources'+res+'"> <br/><input name="data[UserReference][resource_title][]" type="text" id="resource_title[]" class="forminput" value="" style="width:228px;" maxLength="200" placeholder="" /> &nbsp; &nbsp; &nbsp; &nbsp; <input type="file" name="data[UserReference][resource_location][]" id="resource_location[]" />&nbsp; &nbsp;<a id="add" style="cursor:pointer"><img src="'+SiteUrl+'img/media/add.png" style="cursor:pointer" height="19" width="24" alt="" /></a> </div>').fadeIn('slow').prependTo('#divAdd');
						res++;
						}else{
							jQuery('.plus_symbol:first').each(function() {
								jQuery(this).html('');
								jQuery(this).append('<a id="add" style="cursor:pointer"><img src="'+SiteUrl+'img/media/add.png" style="cursor:pointer" height="19" width="24" alt="" /></a>');
								jQuery(this).parent().find('#remove').remove();
							});
						}
					}
				});
			}else{
				alert("There are some error occured. Please try again.");
			}
		}
	});	
}

function unlink_resource_draft(source_id){
	if(confirm("Are you sure you want to remove this resource?")){
		unlink_resource_draft_from_db(source_id);
	}
}

function unlink_resource_draft_from_db(source_id){
	jQuery.ajax({
		url:SITE_URL+'/users/unlink_resource/',
		type:'post',
		data:'source_id='+source_id,
		success:function(res){
			if(res == 1 || res == "1"){
				jQuery("#my_uploded_resource_"+source_id).hide(500,function(){
					jQuery("#my_uploded_resource_"+source_id).remove();
				});
			}else{
				alert("There are some error occured. Please try again.");
			}
		}
	});	
}

function validateInvitation()
{
	jQuery('#subject').css('border-color', '#DDDDDD');
	jQuery('#emailbody').css('border-color', '#DDDDDD');
	jQuery('#email').css('border-color', '#DDDDDD');
	
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	var flag = 0;

	if(jQuery.trim(jQuery('#subject').val()) == '') {
		jQuery('#subject').css('border-color','#F00');
		flag++;	
	}

	if(jQuery.trim(jQuery('#emailbody').val()) == '') {
		jQuery('#emailbody').css('border-color','#F00');
		flag++;	
	}

	var str = jQuery.trim(jQuery('#email').val());
	if(str == ''){
		jQuery('#email').css('border-color','#F00');
		flag++;	
	}else{
		if(str.indexOf(",") != -1){ // checking comma
			var expertArr = str.split(",");
			var length = expertArr.length;
			for(i = 0;i<length;i++){
					if(expertArr[i] == '' || !((jQuery.trim(expertArr[i])).match(mailformat))){
						jQuery('#email').css('border-color','#F00');
						flag++;	
					}					
				}
			} else {
				if(!(jQuery.trim(str).match(mailformat))){
					jQuery('#email').css('border-color','#F00');
					flag++;	
				}
			}
		}

	if(flag == 0){
		jQuery("#Submitform").submit();
	}
	else{
	    return false;
	}
	
}

function validateMenteeRegister()
{
	//var imageName = jQuery.trim(jQuery('#image_name').val());
	var fname = jQuery.trim(jQuery('#UserReferenceFirstName').val());
	var lname = jQuery.trim(jQuery('#UserReferenceLastName').val());
	var zip = jQuery.trim(jQuery('#register_zipcode').val());
	var summary = jQuery.trim(jQuery('#text-area').val());
	var onlyNumbers = /^[0-9]*$/.test(jQuery.trim(jQuery("#register_zipcode").val()));
	var filter = /^[a-zA-Z _!@#"$%\^\-:.<>?=&'()*\+,\/;\[\\\]\^_`{|}~]+$/;
	/*var URLReg = new RegExp();
    URLReg.compile("^[A-Za-z]+://[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]+$");*/
    var url_reg  = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
	var flag = 0;
	var goingID  = '';

	/*if(imageName == 'profile.png')
	{ 
		jQuery('#profileImage').css('border-color','#F00');
		if(goingID==''){ goingID = 'profileImage'; }
		jQuery("#profileImage").opentip("Upload photo", {
			 style: "myErrorStyle" 
			});
		flag++;
	}
	else
	{
		jQuery('#profileImage').css('border-color','rgb(221, 221, 221)');
	}*/
	if(fname==''  || filter.test(fname) == false)
	{ 
		jQuery('#UserReferenceFirstName').css('border-color','#F00');
		if(goingID==''){ goingID = 'UserReferenceFirstName'; }
		jQuery("#UserReferenceFirstName").opentip("Do not leave blank", {
			 style: "myErrorStyle" 
			});
		flag++;
	}
	else
	{
		jQuery('#UserReferenceFirstName').css('border-color','');
	}
	if(lname=='' || filter.test(lname) == false)
	{ 
		jQuery('#UserReferenceLastName').css('border-color','#F00');
		if(goingID==''){ goingID = 'UserReferenceLastName'; }
		jQuery("#UserReferenceLastName").opentip("Do not leave blank", {
			 style: "myErrorStyle" 
			});
		flag++;
	}
	else
	{
		jQuery('#UserReferenceLastName').css('border-color','');
	}
	if(zip=='')
	{ 
		jQuery('#register_zipcode').css('border-color','#F00');
		if(goingID==''){ goingID = 'register_zipcode'; }
		jQuery("#register_zipcode").opentip("Do not leave blank", {
			 style: "myErrorStyle" 
			});
		flag++;
	}
	/*else if(!onlyNumbers || zip.length<4)
		{
			jQuery('#zipcode').css('border-color','#F00');
			if(goingID==''){ goingID = 'zipcode'; }
			flag++;
		}*/
		else
		{	jQuery('#register_zipcode').css('border-color','');
		}
	if(summary=='')
	{ 
		jQuery('#text-area').css('border-color','#F00');
		if(goingID==''){ goingID = 'text-area'; }
		jQuery("#text-area").opentip("Do not leave blank", {
			 style: "myErrorStyle" 
			});
		flag++;
	}
	else
	{
	    words = countWords('text-area');
         if(words>400 || jQuery("#text-area").val().length>2000 )
         {
            jQuery('#text-area').css('border-color','#F00');
            if(goingID==''){ goingID = 'text-area'; }
            jQuery('#text-area').focus();   
            jQuery("#text-area").opentip("400 words limit for this section", {
   			 style: "myErrorStyle" 
   			});
            flag++;  
         }
         else
         {
             jQuery('#text-area').css('border-color','');     
         }
	}
	
	jQuery("#p_social").find(":text").each(function ()
	 {
        if(jQuery(this).val()!='')
        {
            if (!url_reg.test(jQuery(this).val())) 
            {
                jQuery(this).css('border-color','#F00');
                flag++;
            }
            else{  jQuery(this).css('border-color',''); }
        }   
     }); 
	
	if(flag>0)
	{
	    if(goingID != ""){
            jQuery('#'+goingID).focus();
        }
		return false;	
	}
	else	
		return true;
}

function requestMentorship(id)
{
	jQuery.ajax({
		url:SITE_URL+'/clients/requestMentor',
		type:'post',
		dataType:'json',
		data:'id='+id,
		success:function(res){
			if(parseInt(res.value)=='1')
			{
				var path=SITE_URL+'clients/request_message';
				TINY.box.show({url:SITE_URL+'clients/request_message',width:387,height:77,closejs:function(){window.location.reload(true);}});
			}
		}
	});
}

function mentorshipStatusChange(request,id)
{
	jQuery.ajax({
		url:SITE_URL+'/clients/mentorshipApply',
		type:'post',
		dataType:'json',
		data:'action='+request+'&id='+id,
		beforeSend:function(){jQuery('#loadImage').show();},
		success:function(res){
			if(parseInt(res.value)=='1' || parseInt(res.value)=='2')
			{	
				window.location.reload(true);
			}
		}
	});
}
/*
jQuery(function() {
	if (!jQuery.support.placeholder) {
		jQuery(":text").each(function(){
			if (jQuery(this).attr('placeholder') != ''){
				jQuery(this).val(jQuery(this).attr('placeholder'));
			}	
		});
		jQuery(":text").live('keypress',function(){
			if (jQuery(this).attr('placeholder') != ''){
				if(jQuery(this).val() ==jQuery(this).attr('placeholder'))
				{
					jQuery(this).val('');
				}
			}
		});
		jQuery(":text").live('focus',function(){
			if (jQuery(this).attr('placeholder') != ''){
				if(jQuery(this).val() ==jQuery(this).attr('placeholder'))
				{
					jQuery(this).selectionStart = 0;
					jQuery(this).selectionEnd = 0;
					//jQuery(this).focus();
				}
			}
		});
		jQuery(":text").live('blur',function(){
			if (jQuery(this).attr('placeholder') != ''){
				if(jQuery(this).val() == '')
				{
					jQuery(this).val(jQuery(this).attr('placeholder'));
				}
			}
		});
	}	
});
*/
 function change_feedback_status(id,type)
    {
        jQuery.ajax({
            url:SITE_URL+'/feedback/typechange',
            type:'post',
            dataType:'json',
            beforeSend:function(){jQuery('#loadImage'+id).show();},
            data:'id='+id+'&type='+type,
            success:function(res){
                       jQuery('#loadImage'+id).hide();    
            }
        }); 
    }
  
function removedSession(id)
{
   if(confirm('Do you want to delete this engagement? This action can not be undone.')==true && id!='')
    {
        jQuery.ajax({
            url:SITE_URL+'/invoice/deletementorship',
            type:'post',
            dataType:'json',
            beforeSend:function(){jQuery('#loadImage'+id).show();},
            data:'id='+id,
            success:function(res){
                jQuery('#edit_row_delete'+id).remove();
            }
        }); 
    }
}

function invoice_detail(id){
    var path=SITE_URL+'/invoice/popupinvoice/';
    //TINY.box.show({url:SITE_URL+'/users/register',width:500,fixed:true});
    TINY.box.show({url:SITE_URL+'/invoice/popupinvoice/'+id,width:240,height:103});/* VL 27/12 change height*/
}

function countWords(id)
{ 
    s = jQuery("#"+id).val();
    s = s.replace(/(^\s*)|(\s*$)/gi,"");
    s = s.replace(/[ ]{2,}/gi," ");
    s = s.replace(/\n /,"\n");
    words = s.split(' ').length;
    return words;
}
function appendzero(data)
{
    value = /^[0-9.]*$/.test(jQuery(data).val());
    if(jQuery(data).val()<=9 && value==true && jQuery(data).val().length<2)
    {
        jQuery(data).val("0"+jQuery(data).val());
    }
   /* if(!value)
    {
      jQuery(data).css('border-color','#F00');
    }
    else
    {
        jQuery(data).css('border-color',''); 
    }*/
}

/* for generation map */
 function genMap()
 {
    var txtValue = jQuery('#mode_text').val();
    if(jQuery.trim(txtValue)=='')
    {
       jQuery('#mode_text').css('border-color','#F00');
       jQuery('#mapDiv').html("");
       flag++;
    }
    else
    { 
        jQuery('#mode_text').css('border-color','');
        jQuery('#generateButton').css('border-color','');
        jQuery('#hideMapText').show();
        jQuery('#mapDiv').html("<iframe width='400' height='200' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0' src='https://maps.google.com/maps?&amp;q="+encodeURIComponent(txtValue)+"&amp;output=embed&amp;iwloc=near'></iframe>");
        jQuery('#mapDiv').show();
     }
   
 }
 function hideMap()
 {
     jQuery('#hideMapText').hide();
     jQuery('#mapDiv').hide();             
 }

function chkUrl(submit_button)
{
    var url_reg = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    urlValue = jQuery('#MessageLinkLinkText').val();
    flag=0;
	
    if(jQuery.trim(urlValue)!='')
    {
        if (!url_reg.test(urlValue)) 
        {
            jQuery('#MessageLinkLinkText').css('border-color','#F00');
            flag++;
        }
        else
            jQuery('#MessageLinkLinkText').css('border-color','');
    }
    else
        jQuery('#MessageLinkLinkText').css('border-color','');
        
    if(flag>0){
		jQuery('#MessageLinkLinkText').focus();
        return false;
	}else{
    	    jQuery("#mentorshipForm").submit();
    		submit_button.disabled=true;
	}	
}

function changeEditInvoice()
{
    jQuery('#inv_date').attr('disabled',false);
    jQuery('#inv_date').attr('readonly',true);
    jQuery('#time_hh').attr('readonly',false);
    jQuery('#time_mm').attr('readonly',false);
    jQuery('#time_AM_PM').attr('disabled',false);
    jQuery('#time_zone').attr('disabled',false);
    
    jQuery('#inv_date1').attr('disabled',false);
    jQuery('#time_hh1').attr('readonly',false);
    jQuery('#time_mm1').attr('readonly',false);
    jQuery('#time_AM_PM1').attr('disabled',false);
    jQuery('#inv_date2').attr('disabled',false);
    jQuery('#time_hh2').attr('readonly',false);
    jQuery('#time_mm2').attr('readonly',false);
    jQuery('#time_AM_PM2').attr('disabled',false);
    
    jQuery('#a1').attr('disabled',false);
    jQuery('#a2').attr('disabled',false);
    jQuery('#a3').attr('disabled',false);
    jQuery('#mode_text').attr('readonly',false);
    jQuery('#text-area').attr('readonly',false);
    price = jQuery('#price').val();
    if(price!='0.00')
    { 
        jQuery('#discount').attr('readonly',false);
        jQuery('#tax').attr('readonly',false);
    }
}

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

function validateComment(){
	var txtValue = jQuery('#report_comment').val();
	jQuery('#report_comment').css('border-color','');
	if(jQuery.trim(txtValue) ==''){
		jQuery('#report_comment').css('border-color','#F00');
		jQuery('#report_comment').focus();
		return false;
	}
}

jQuery(function(){
    jQuery(".send_reminder_hide").click(function(){
        jQuery(this).hide();
        
    });
});


jQuery(function(){
    jQuery("a[class^='report_issue_hide_']").click(function(){
        var id = jQuery(this).attr("class");
		id = id.replace('report_issue_hide_','');
		if (jQuery("#pipe_"+id).length > 0){
			jQuery("#pipe_"+id).hide();
		}
		jQuery(this).hide();
    });
});

function noBack() { 
	window.history.forward(); 
}


function checkValidFile(data)
{
    var ext = ["doc", "docx", "ppt", "xlsx", "pdf", "jpg", "JPG" ,"jpeg", "png",  "gif", "pptx", "xls"];
        
    var fileExt=jQuery(data).val(); 
    
    //alert('This file size is: ' + size + "MB");
    //alert(fileExt);
    //return false ;
    if(fileExt!="")
    {
        size=data.files[0].size/1024/1024;
        if(size>2)
        {
            $(data).css('border-color','#F00');
            //$(this).focus();
            return 0;
        }
        var resumeArr = fileExt.split(".");
        if(jQuery.inArray(resumeArr[1], ext)==-1)
        {
            jQuery(data).css('border-color','#F00');
            //jQuery(this).focus();
            return 0;
        }
        return 1;
    }
    else
    {
        return 1;
    }
}

/*validfile = checkValidFile(this);
 if(validfile==1)
 {
   jQuery(this).css('border-color','');
 }
 else
 {
     jQuery(this).css('border-color','#F00');
     jQuery(this).focus();
 }*/

function isNumberKey(evt)

{

    var charCode = (evt.which) ? evt.which : evt.keyCode

    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) return false;            

    return true;

}     

function validfeedback()
{
    var txtValue = jQuery('#feedbackText').val();
    jQuery('#feedbackText').css('border-color','');
    if(jQuery.trim(txtValue) ==''){
        jQuery('#feedbackText').css('border-color','#F00');
        jQuery('#feedbackText').focus();
        return false;
    }
}

function toggle(counter, length){
	
	for(var i = 1; i <= length; i++){
		if(i == counter){
			var f1=document.getElementById("head"+i);
			if(document.getElementById("sub"+i).style.display == "block") {
				f1.style.color="#FF0000";
				document.getElementById("sub"+i).style.display="none";
				document.getElementById("tm"+i).style.display="none";
				document.getElementById("img"+i).setAttribute('src',SITE_URL+'/img/media/dnarrow.png');
			} else {
				f1.style.color="#292929";
				document.getElementById("sub"+i).style.display="block";
				document.getElementById("tm"+i).style.display="block";
				document.getElementById("img"+i).setAttribute('src',SITE_URL+'/img/media/uparrow.png');
			}
		} else {
			var f1=document.getElementById("head"+i);
			f1.style.color="#FF0000";
			document.getElementById("sub"+i).style.display="none";
			document.getElementById("tm"+i).style.display="none";
			document.getElementById("img"+i).setAttribute('src',SITE_URL+'/img/media/dnarrow.png');
		}
	}
}

	// -----------for progile and case studies button--------------------------
	function tog1(){
		var i=document.getElementById('prof');
		var j=document.getElementById('divParent');
		var k=document.getElementById('bg1');
		var l=document.getElementById('bg2');
		var m=document.getElementById('answersDiv');
		var n=document.getElementById('bg3');
		i.style.display="block";
		j.style.display="none";
		if(m)
		{
			m.style.display="none";
			n.style.color="#5A5A5B";
			n.style.backgroundColor="#D0D0D0";
		}
		k.style.color="white";
		k.style.backgroundColor="#5A5A5B";
		l.style.color="#5A5A5B";
		l.style.backgroundColor="#D0D0D0";
		
		document.getElementById('rmHIDE').style.display="none";
		document.getElementById('mentor-detail-right').style.display="block";
		document.getElementById('answersRight').style.display="none";
		document.getElementById('profileLinkTab').style.display="block";
		document.getElementById('answersLinkTab').style.display="none";
	}
	function tog2(){
		var i=document.getElementById('prof');
		var j=document.getElementById('divParent');
		var k=document.getElementById('bg1');
		var l=document.getElementById('bg2');
		var m=document.getElementById('answersDiv');
		var n=document.getElementById('bg3');
		i.style.display="none";
		j.style.display="block";
		if(m)
		{
			m.style.display="none";
			n.style.color="#5A5A5B";
			n.style.backgroundColor="#D0D0D0";
		}
		k.style.color="#5A5A5B";
		k.style.backgroundColor="#D0D0D0";
		l.style.color="white";
		l.style.backgroundColor="#5A5A5B";
		
		document.getElementById('rmHIDE').style.display="block";
		document.getElementById('mentor-detail-right').style.display="none";
		document.getElementById('answersRight').style.display="none";
	}

	function tog3(){
		var i=document.getElementById('prof');
		var j=document.getElementById('divParent');
		var k=document.getElementById('bg1');
		var l=document.getElementById('bg2');
		var m=document.getElementById('answersDiv');
		var n=document.getElementById('bg3');
		i.style.display="none";
		j.style.display="none";
		m.style.display="block";
		k.style.color="#5A5A5B";
		k.style.backgroundColor="#D0D0D0";
		l.style.color="#5A5A5B";
		l.style.backgroundColor="#D0D0D0";
		n.style.color="#FFF";
		n.style.backgroundColor="#5A5A5B";
		document.getElementById('rmHIDE').style.display="none";
		document.getElementById('mentor-detail-right').style.display="none";
		document.getElementById('answersRight').style.display="block";
		document.getElementById('profileLinkTab').style.display="none";
		document.getElementById('answersLinkTab').style.display="block";
	}

	
	function delCaseStudy(id)
	{
		if(confirm('Are you sure you want to delete this Case Study?')==true)
		{
			window.location.href = SITE_URL+'members/delete_casestudy/'+id;
		}
	}

	function addQuestion(q_id){
		var path=SITE_URL+'/users/register/question_id:'+q_id;
		TINY.box.show({url:path,width:890,height:500})
		
	}
	
	function openProfilePopup(u_id) {
		var path=SITE_URL+'/project/profile/'+u_id;
		TINY.box.show({url:path,width:1040,height:555})
	}
	
	function showInterest(member_id, project_id) {
		var path=SITE_URL+'project/showInterest/'+member_id+'/'+project_id;
		window.location.href = path;
	}
	function dir_consult(member_id) {
		TINY.box.show({url:SITE_URL+'fronts/consult_iframe/'+member_id,width:1000,height:550});
	}
	function dir_feedback(member_id) {
		TINY.box.show({url:SITE_URL+'fronts/feedback_iframe/'+member_id,width:1000,height:550});
	}
	function changePlanAfterCheckingUser(plan_id) {
		var path=SITE_URL+'pricing/change_plan/'+plan_id;
		window.location.href = path;
	}
