/*
   Script name  : Ajax Auto Suggest
   File Name 	: script.js
   Developed By : Amit Patil (India)
   Email Id 	: amitpatil321@gmail.com
   last Updated : 9 June 2009
         This program is freeware.There is no any fucking copyright and bla bla bla.
   You can use it for your personal use.You can also make any changes to this script.
   But before using this script i would appericiate your mail.That will encourage me a lot.
   Any suggestions are always welcome.
         Have a fun with programming.   
*/
jQuery(document).ready(function(){
	jQuery(document).click(function(){
		jQuery("#ajax_response").fadeOut('slow');
	});
	jQuery("#keyword").focus();
	var offset = jQuery("#keyword").offset();
	var width = jQuery("#keyword").width()-2;
	jQuery("#ajax_response").css("left",offset.left); 
	jQuery("#ajax_response").css("width",width);
	jQuery("#keyword").keyup(function(event){
		
		 var keyword = jQuery("#keyword").val();
		 if(keyword.length)
		 {
			 if(event.keyCode != 40 && event.keyCode != 38 && event.keyCode != 13)
			 {
				
				 jQuery("#loading").css("visibility","visible");
				 jQuery.ajax({
				   type: "POST",
				   url: SITE_URL+'fronts/auto_zipcode',
				   data: "data="+keyword,
				   success: function(msg){
					 if(msg != 0)
					  jQuery("#ajax_response").fadeIn("slow").html(msg);
					else
					{
					  jQuery("#ajax_response").fadeIn("slow");	
					  jQuery("#ajax_response").html('<div style="text-align:left;">No Matches Found</div>');
					}
					jQuery("#loading").css("visibility","hidden");
				   }
				 });
			 }
			 else
			 {
				switch (event.keyCode)
				{
				 case 40:
				 {
					  found = 0;
					  jQuery("li").each(function(){
						 if(jQuery(this).attr("class") == "selected")
							found = 1;
					  });
					  if(found == 1)
					  {
						var sel = jQuery("li[class='selected']");
						sel.next().addClass("selected");
						sel.removeClass("selected");
					  }
					  else
						jQuery("li:first").addClass("selected");
					 }
				 break;
				 case 38:
				 {
					  found = 0;
					  jQuery("li").each(function(){
						 if(jQuery(this).attr("class") == "selected")
							found = 1;
					  });
					  if(found == 1)
					  {
						var sel = jQuery("li[class='selected']");
						sel.prev().addClass("selected");
						sel.removeClass("selected");
					  }
					  else
						jQuery("li:last").addClass("selected");
				 }
				 break;
				 case 13:
					jQuery("#ajax_response").fadeOut("slow");
					jQuery("#keyword").val(jQuery("li[class='selected'] a").text());
				 break;
				}
			 }
		 }
		 else
			jQuery("#ajax_response").fadeOut("slow");
	});
	jQuery("#ajax_response").mouseover(function(){
		jQuery(this).find("li a:first-child").mouseover(function () {
			  jQuery(this).addClass("selected");
		});
		jQuery(this).find("li a:first-child").mouseout(function () {
			  jQuery(this).removeClass("selected");
		});
		jQuery(this).find("li a:first-child").click(function () {
			  jQuery("#keyword").val(jQuery(this).text());
			  jQuery("#ajax_response").fadeOut("slow");
		});
	});
});