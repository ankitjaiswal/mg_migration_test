<?php
ob_start();
function oauth_session_exists() {
  if((is_array($_SESSION)) && (array_key_exists('oauth', $_SESSION))) {
    return TRUE;
  } else {
    return FALSE;
  }
}

try {
  // include the LinkedIn class
  require_once('linkedin_3.2.0.class.php');
  // start the session

  if(!session_start()) {
    throw new LinkedInException('This script requires session support, which appears to be disabled according to session_start().');
  }
  
  // display constants
  $API_CONFIG = array(
    'appKey'       => '754xt9rqmuhn7z',
	  'appSecret'    => 'TC1UcUzbCEtBjaja',
	  'callbackUrl'  => NULL 
  );
  define('DEMO_GROUP', '4010474');
  define('DEMO_GROUP_NAME', 'Simple LI Demo');
  define('PORT_HTTP', '80');
  define('PORT_HTTP_SSL', '443');
  // set index
  if(!isset($_REQUEST['lType'])){
  	$_REQUEST['lType'] = 'initiate';
  }
  if(isset($_REQUEST['oauth_problem']) && $_REQUEST['oauth_problem']=='user_refused')
  {     
      header("Location:../users/cancellinkedin");
      exit;
  }
  if(isset($_REQUEST['return']) && $_REQUEST['return']=='true')
  {
  	 if($_SESSION['oauth']['linkedin']['authorized'] === TRUE) {
            $OBJ_linkedin = new LinkedIn($API_CONFIG);
            $OBJ_linkedin->setTokenAccess($_SESSION['oauth']['linkedin']['access']);
          	$OBJ_linkedin->setResponseFormat(LINKEDIN::_RESPONSE_XML);
            ?>
            
            <?php
          } 
		//  '~:(id,first-name,last-name,picture-urls::(original),email-address,location:(name,country:(code)),public-profile-url,twitter-accounts,summary)');
	 	$response = $OBJ_linkedin->profile('~:(id,first-name,last-name,picture-urls::(original),email-address,public-profile-url,summary)');
		if($response['success'] === TRUE) {
		  $response['linkedin'] = new SimpleXMLElement($response['linkedin']);
		  $userData = array('linkedin_id'=>'','first-name'=>'','last-name'=>'','picture-url'=>'','email-address'=>'','public_url'=>'','summary'=>'');
		  foreach($response['linkedin'] as $key=>$value)
		  {
		  	if($key=='id')
				$userData['linkedin_id'] = (string)$value;
			if($key=='first-name')
				$userData['first-name'] = (string)$value;
			if($key=='last-name')
				$userData['last-name'] = (string)$value;
			if($key=='picture-urls')
			{
				$val = json_encode($value); 
				$val1 = json_decode($val,true);
				$userData['picture-url'] = $val1['picture-url'];
			}
			if($key=='email-address')
				$userData['email-address'] = (string)$value;	
			if($key=='public-profile-url')
				$userData['public_url'] = (string)$value;	
			if($key=='summary')
				$userData['summary'] = (string)$value;	
		  }
		  $userRequiredDetails = "";
		  $linkedinMenter_array =array('oid'=> $userData['linkedin_id']."<br/>",'first_name'=>$userData['first-name']."<br/>",'last_name'=>$userData['last-name']."<br/>",'image'=>$userData['picture-url']."<br/>",'email'=>$userData['email-address']."<br/>",'public_url'=>$userData['public_url']."<br/>",'summary'=>$userData['summary']);
		  
		  /* for data write in file */
		  $file = "../".$userData['linkedin_id'].".txt";
		  file_put_contents($file, $linkedinMenter_array);
		  
		  $userRequiredDetails.="?oid=".$userData['linkedin_id']."&email=".$userData['email-address'];
		  header("Location:../../../users/signuplinkedinMentor".$userRequiredDetails);
		  exit;
 		}
  }
  

  $_REQUEST[LINKEDIN::_GET_TYPE] = (isset($_REQUEST[LINKEDIN::_GET_TYPE])) ? $_REQUEST[LINKEDIN::_GET_TYPE] : '';
  switch($_REQUEST[LINKEDIN::_GET_TYPE]) {
    case 'initiate':
      /**
       * Handle user initiated LinkedIn connection, create the LinkedIn object.
       */
     
      // check for the correct http protocol (i.e. is this script being served via http or https)
      if($_SERVER['HTTPS'] == 'on') {
        $protocol = 'https';
      } else {
        $protocol = 'http';
      }
      
      // set the callback url
      $API_CONFIG['callbackUrl'] = $protocol . '://' . $_SERVER['SERVER_NAME'] . ((($_SERVER['SERVER_PORT'] != PORT_HTTP) || ($_SERVER['SERVER_PORT'] != PORT_HTTP_SSL)) ? ':' . $_SERVER['SERVER_PORT'] : '') . $_SERVER['PHP_SELF'] . '?' . LINKEDIN::_GET_TYPE . '=initiate&' . LINKEDIN::_GET_RESPONSE . '=1';
     
	  $OBJ_linkedin = new LinkedIn($API_CONFIG);
	
      // check for response from LinkedIn
      $_GET[LINKEDIN::_GET_RESPONSE] = (isset($_GET[LINKEDIN::_GET_RESPONSE])) ? $_GET[LINKEDIN::_GET_RESPONSE] : '';
      if(!$_GET[LINKEDIN::_GET_RESPONSE]) {
        // LinkedIn hasn't sent us a response, the user is initiating the connection
        
        // send a request for a LinkedIn access token
        $response = $OBJ_linkedin->retrieveTokenRequest();
        if($response['success'] === TRUE) {
          // store the request token
          $_SESSION['oauth']['linkedin']['request'] = $response['linkedin'];
          // redirect the user to the LinkedIn authentication/authorisation page to initiate validation.
          header('Location: ' . LINKEDIN::_URL_AUTH . $response['linkedin']['oauth_token']);
        } else {
        	exit;
          // bad token request
          //echo "Request token retrieval failed:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response, TRUE) . "</pre><br /><br />LINKEDIN OBJ:<br /><br /><pre>" . print_r($OBJ_linkedin, TRUE) . "</pre>";
        }
      } else {
        // LinkedIn has sent a response, user has granted permission, take the temp access token, the user's secret and the verifier to request the user's real secret key
        $response = $OBJ_linkedin->retrieveTokenAccess($_SESSION['oauth']['linkedin']['request']['oauth_token'], $_SESSION['oauth']['linkedin']['request']['oauth_token_secret'], $_GET['oauth_verifier']);
        if($response['success'] === TRUE) {
          // the request went through without an error, gather user's 'access' tokens
          $_SESSION['oauth']['linkedin']['access'] = $response['linkedin'];
          
          // set the user as authorized for future quick reference
          $_SESSION['oauth']['linkedin']['authorized'] = TRUE;
            
          // redirect the user back to the demo page
          header('Location: ' . $_SERVER['PHP_SELF']."?return=true");
        } else {
        	exit;
          // bad token access
          //echo "Access token retrieval failed:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response, TRUE) . "</pre><br /><br />LINKEDIN OBJ:<br /><br /><pre>" . print_r($OBJ_linkedin, TRUE) . "</pre>";
        }
      }
      break;

    case 'revoke':
      /**
       * Handle authorization revocation.
       */
                    
      // check the session
      if(!oauth_session_exists()) {
        throw new LinkedInException('This script requires session support, which doesn\'t appear to be working correctly.');
      }
      
      $OBJ_linkedin = new LinkedIn($API_CONFIG);
      $OBJ_linkedin->setTokenAccess($_SESSION['oauth']['linkedin']['access']);
      $response = $OBJ_linkedin->revoke();
      if($response['success'] === TRUE) {
        // revocation successful, clear session
        session_unset();
        $_SESSION = array();
        if(session_destroy()) {
          // session destroyed
          header('Location: ' . $_SERVER['PHP_SELF']);
        } else {
          // session not destroyed
          echo "Error clearing user's session";
        }
      } else {
		exit;
        // revocation failed
        //echo "Error revoking user's token:<br /><br />RESPONSE:<br /><br /><pre>" . print_r($response, TRUE) . "</pre><br /><br />LINKEDIN OBJ:<br /><br /><pre>" . print_r($OBJ_linkedin, TRUE) . "</pre>";
      }
      break;
    default:
      // nothing being passed back, display demo page
      
      // check PHP version
      if(version_compare(PHP_VERSION, '5.0.0', '<')) {
        throw new LinkedInException('You must be running version 5.x or greater of PHP to use this library.'); 
      } 
      
      // check for cURL
      if(extension_loaded('curl')) {
        $curl_version = curl_version();
        $curl_version = $curl_version['version'];
      } else {
        throw new LinkedInException('You must load the cURL extension to use this library.'); 
      }
     
      break;
  }
} catch(LinkedInException $e) {
  // exception raised by library call
  echo $e->getMessage();
}

?>