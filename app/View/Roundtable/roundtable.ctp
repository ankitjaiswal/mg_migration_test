

<?php echo($this->Html->css(array('plans'))); ?>
        <?php echo($this->Html->css(array('plans'))); ?>
	<div id="inner-content-wrapper">
	  <div id="inner-content-box" class="pagewidth"> 
	    <div id="user-account">
	      <div class="account-form">
		  	<div class="onbanner">
		          <h1>                 
		            <div id="spec1" style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;">From <span style="text-decoration: underline;">Executive Coaching</span> to <span style="text-decoration: underline;">New Product Launch</span></div>
					<div id="spec2" style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;display:none;">From <span style="text-decoration: underline;">Business Strategy</span> to <span style="text-decoration: underline;">Succession Planning</span></div>
					<div id="spec3" style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;display:none;">From <span style="text-decoration: underline;">Workplace Conflict</span> to <span style="text-decoration: underline;">Customer Service</span></div>
					<div id="spec4" style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;display:none;">From <span style="text-decoration: underline;">Mergers &amp; Acquisitions</span> to <span style="text-decoration: underline;">Change Management</span></div>
					<div id="spec5" style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;display:none;">From <span style="text-decoration: underline;">Employee Engagement</span> to <span style="text-decoration: underline;">Lean Six Sigma</span></div>  
		          </h1>
				  <div style="position:relative;top:80px;"><p><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal;font-size: 18px;">Expert perspectives for a better plan of action.</span></div>
			</div>

                    <script>
		var $body = jQuery('body'),
		    cycle;
			(cycle = function() {
			jQuery('#spec1').delay(2000).fadeOut('slow');
			
            jQuery('#spec2').delay(2750).fadeIn('slow');
			jQuery('#spec2').delay(2000).fadeOut('fast');
					
			jQuery('#spec3').delay(5500).fadeIn('slow');
		    jQuery('#spec3').delay(2000).fadeOut('fast');
					
			jQuery('#spec4').delay(8250).fadeIn('slow');
		    jQuery('#spec4').delay(2000).fadeOut('fast');

			jQuery('#spec5').delay(11000).fadeIn('slow');
		    jQuery('#spec5').delay(2000).fadeOut('fast');
				
			jQuery('#spec1').delay(11750).fadeIn('slow',cycle);	
									
			})();
		</script>
    <div id="content-submit">
	
<div class="exec-rt-container">
	
                      <div class="exec-rt"><h1 style="display:none;"><span style ="font-weight:bold;font-size:22px;color:#333333;">Executive Roundtable</span></h1></div>

<div id="text-content" style="font-weight:normal;font-size:16px;font-family: Ubuntu;line-height: 150%;"><!--VL 24/12-->
<span style="font-weight:bold;color:#444444;">Is your organization at a turning point?</span> Will your team benefit from a conversation with a leading expert who specializes in dealing with your current challenge? But, you don&#39;t want an on-site or long-term consulting engagement?
<br/>
<br/>
<div id="text-content" style="font-weight:normal;font-size:16px;font-size:16px;font-family: Ubuntu;">
Such situations are perfect for an <span style="font-weight:bold;color:#444444;">Executive Roundtable</span>.
<br/>
<br/>
The roundtable is a focused hour-long discussion between your executive team, an outside expert and our facilitator. It provides practical insights to power through your challenges.
<br/>
<br/>
<br/>
</div>

<img src="img/media/discuss_icon.png" class="discuss"><div class="rt_head">Successful discussions include:</div>
<div id="text-content" style="font-weight:normal;font-size:16px;font-family: Ubuntu;padding-top: 25px;">
<ul type="disc" class="a">
<li style="margin-left:55px;">Guidance on a reorganization</li>
<li style="margin-left:55px;">Feedback on a product launch</li>
<li style="margin-left:55px;">Best practices in your industry</li>
<li style="margin-left:55px;">Due diligence during M&A</li>
</ul>
</div>

<div class="datagrid"><table>
<thead><tr><th colspan="3"><div class="steps">3 simple steps</div></th></tr></thead>
<tfoot><tr><td colspan="3"></tr></tfoot>
<tbody>

    <tr><td>
    
    <div class="numberCircle">1</div></br>
    
    </td><td>
    
    <div class="numberCircle">2</div></br>
    
    </td><td>
    
    <div class="numberCircle">3</div></br>
    
    </td></tr>
    
        <tr><td>
    
    <div class="steps_text">Request a preliminary call to discuss your need</div>
    
    </td><td>
    
    <div id="arrow"></div>
    <div class="steps_text">We&#39;ll help select the right expert</div>
        
    </td><td>
    
   <div id="arrow"></div>
    <div class="steps_text">GUILD will facilitate the Roundtable</div>
        
    </td></tr>
    
    <tr class="alt"><td colspan="3"><div class="steps_footer"></div></tr>

</tbody>
</table></div>

<div id="text-content" style="font-weight:normal;font-size:16px;font-size:16px;font-family: Ubuntu;">
Sound like a service that could empower your team to take your business to the next level? You bet it is.
<br/>
<br/>
<span style="font-weight:bold;color:#444444;">So, what can you really achieve in an Executive Roundtable?</span>
<br/>
<br/>
You won&#39;t solve a complex organizational challenge during a one-hour call. However, as our clients tell us, you&#39;ll get &mdash; a fresh perspective, a way to test your assumptions and a few <i>Aha! Moments</i>.
<br/>
<br/>
You&#39;ll likely save a great deal of time and make a wiser decision.
<br/>
<br/>
</div>
<div class="included">
<img src="img/media/executive_roundtable-thumb.png" class="report">
<div class="rt_head" style="left:-18px;top:-5px;position:relative;">What's included?</div>
<div id="text-content" style="font-weight:normal;font-size:16px;font-family: Ubuntu;padding-top: 25px;">
<ul type="disc" class="a">
<li style="margin-left:55px;"><a href="<?php echo(SITE_URL);?>Confidentiality-Agreement.html" target="_blank">Confidentiality agreement</a></li>
<li style="margin-left:55px;">Preliminary call to understand your need</li>
<li style="margin-left:55px;">Expert shortlist for your final selection</li>
<li style="margin-left:55px;">Focused Roundtable discussion</li>
</ul>
</div>
</div>


 <div class="" style="padding-top: 30px;">
<input class="expButton" id="onclick" style="font-weight:normal;font-family:ubuntu;width:100%;height:50px;font-size:x-large;" value="I&#39;m interested &mdash; Tell me more!" type="button">
<p style="text-align:center;"><span style ="font-weight:normal;font-size:16px;color:#333333;font-family: Ubuntu;">Or call us at <span style ="color:#D03135;font-weight:normal;font-size:16px;font-family: Ubuntu;">+1-866-511-1898 </span> <span style ="font-weight:normal;font-size:16px;color:#333333;font-family: Ubuntu;"> / email us at </span><a href="mailto: roundtable@guild.im"><span style ="font-weight:normal;font-size:16px;font-family: Ubuntu;">roundtable@guild.im</span></a>.</span>
</p>

</div>
 
<div id="formbox" style="margin-top:-40px;display:none;">
<?php echo $this->Form->create(null,array('url'=>array('controller'=>'roundtable','action'=>'roundtable'),'id'=>'roundtableForm'));?>
<div id="name-box">                   
                    <div id="country" style="width:780px;">
    <div style="margin-left:150px;font-family:ubuntu;">
                    
	      							<table>
	      								<tbody>
	      									<tr>
	      										<td style="float: right; margin-top: 16px; margin-right: 10px;">Name</td>
	      										<td>
	      											<input name="data[Mentorship][firstname]" type="text" class="forminput" id="userfname" value="" maxlength="120" placeholder="First" style="width:153px;">
													<input name="data[Mentorship][lastname]" type="text" class="forminput" id="userlname" value="" maxlength="120" placeholder="Last" style="width:155px;margin-left:-10px;">
	      										</td>
	      									</tr>
	      									<tr>
	      										<td style="float: right; margin-top: 10px; margin-right: 10px;">Work email</td>
	      										<td>
													<input name="data[Mentorship][email]" type="text" class="forminput" placeholder = "email@yourcompany.com" id="useremail" value="" maxlength="250" style="width:332px;">      										
												</td>
	      									</tr>
	      									<tr>
	      										<td style="float: right; margin-top: 16px; margin-right: 10px;">Organization</td>
	      										<td>
													<input name="data[Mentorship][company]" type="text" class="forminput"  id="usercompany" value="" maxlength="250" style="width:332px;">      										
												</td>
	      									</tr>
	      									<tr>
	      										<td style="float: right; margin-top: 16px; margin-right: 10px;">Phone</td>
	      										<td>
													<input name="data[Mentorship][phone]" type="text" class="forminput" placeholder = "(xxx) xxx-xxxx" id="userphone" value="" maxlength="250" style="width:332px;">      										
												</td>
	      									</tr>
                                                                      <tr>
                                                                        				<td style="float: right;margin-top: 25px;"></td>

			                                 <td>   <input id="sendButton" type="submit" value="Submit" class="mgButton" style="margin-bottom: 20px; float: right;margin-right: 12px;">
						                             </td>
	      								</tbody>
	      							</table>


</div>



</div>

                 
</div>
</form>
</div>
</div>
     </div>
    
    </div>
    
    
</div>
</div>
</div>
	<script src="https://code.jquery.com/jquery-1.7.2.js"></script>
	<script>
	$(document).ready(function(){
	  $(function(){
	  var from=['Business Strategy','Workplace Conflict','Mergers &amp; Acquisition','Employee Engagement','Executive Coaching'], i=1; // i for counting
	      setInterval(function(){
	          $('#from').fadeOut(function(){ //fadeout text
	          $(this).html(from[i=(i+1)%from.length]).fadeIn(); //update, count and fadeIn
	          });
	      }, 3500 ); //2s

	  var to=['Succession Planning','Customer Service','Change Management','Lean Six Sigma','New Product Launch'], j=1; // j for counting
	      setInterval(function(){
	          $('#to').fadeOut(function(){ //fadeout text
	          $(this).html(to[j=(j+1)%to.length]).fadeIn(); //update, count and fadeIn
	          });
	      }, 3500 ); //2s

	  });
	});

	
</script>
<style type="text/css">
ul.a li{
position:relative;
top:-15px;

}
ol.a li{
position:relative;
top:-14px;

}
#user-account .account-form input[type=text], #user-account .account-form input.input{border:#dcdcdc solid 1px;font-family:'arial';color:#000000;margin-bottom:10px;}

</style>
<script type="text/javascript">
	//validation
	
	jQuery(document).ready(function(){
         		jQuery("#sendButton").click(function() {
                            
                var flag = 0;
				
		      if(jQuery.trim(jQuery("#userfname").val()) == '') {
                    jQuery('#userfname').css('border-color','#F00');
                    jQuery('#userfname').focus();                
                    flag++;
                    }else {
                    jQuery('#userfname').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#userlname").val()) == '') {
                    jQuery('#userlname').css('border-color','#F00');
                    jQuery('#userlname').focus();                
                    flag++;
                    }else {
                    jQuery('#userlname').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#useremail").val()) == '') {
                    jQuery('#useremail').css('border-color','#F00');
                    jQuery('#useremail').focus();                
                    flag++;
                    }else {
                    jQuery('#useremail').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#usercompany").val()) == '') {
                    jQuery('#usercompany').css('border-color','#F00');
                    jQuery('#usercompany').focus();                
                    flag++;
                    }else {
                    jQuery('#usercompany').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#userphone").val()) == '') {
                    jQuery('#userphone').css('border-color','#F00');
                    jQuery('#userphone').focus();                
                    flag++;
                    }else {
                    jQuery('#userphone').css('border-color','');                 
                    }
    			if(flag > 0) {
    				return false;
    			} else {

                           jQuery("#roundtableForm").submit();
                     }
    		});  
 });
</script>
 <script type="text/javascript">
    jQuery(document).ready(function(){
       jQuery("#onclick").click(function() {
       if(loggedUserId == ''){
        document.getElementById('formbox').style.display="block";
         }else{
		       jQuery.ajax({
					url:SITE_URL+'roundtable/registered_request',
					type:'post',
					dataType:'json',
					data: 'loggedUserId='+loggedUserId,
					success:function(loggedUserId){
						window.location.href = SITE_URL+'roundtable/thanks';
					}
				});	
		       
		   		return false;
		}
                   });
    });

</script>
<script type="text/javascript">
jQuery(document).ready(function(){
jQuery("#onclick").click(function() {
		jQuery('html, body').animate({
	        scrollTop: jQuery("#reqform").offset().top
	    }, 1000);
	    return false;
	});
});
</script>
