<div class="fieldset">
    <h3 class="legend">
		Member-Client Log
        <div class="total" style="float:right">
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Program ID</td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Member name</td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Client name</td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Action</td>
                    </tr>
                    <?php 
                    if(!empty($data) && count($data)>0)
                    { 
                        foreach($data as $msg)
                        {    
                            $prgId = $this->General->fetchProgramId($msg['Message']['mentor_id'],$msg['Message']['mentee_id']);
                        ?>
                        <tr>
                            <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo $prgId; ?></td>
                            <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo ucfirst(strtolower($msg['Mentor']['first_name']))." ".ucfirst(strtolower($msg['Mentor']['last_name'])); ?></td>
                            <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo ucfirst(strtolower($msg['Mentee']['first_name']))." ".ucfirst(strtolower($msg['Mentee']['last_name'])); ?></td>
                            <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $html->link("view history", array('controller' => 'reports', 'action' => 'loghistory/'.$msg['Message']['mentor_id'].'/'.$msg['Message']['mentee_id']), array("title" => "", "escape" => false)); ?></td>           
                        </tr>	
                   <?php
                        }
                    }else{ ?>
                   <tr>
                        <td colspan="4" width="15%" align="center" valign="middle" class="Bdrrightbot" style="padding-left:9px;height:30px;">Record not found</td>           
                    </tr>
                   <?php } ?>   
                </table>
            </div>
            <?php
     //   }
        ?>

    </div>
</div>
<div class="clr"></div>
<?php //echo $this->element('admin/admin_paging', array("paging_model_name" => "NewsLetter", "total_title" => "NewsLetter")); ?>	 