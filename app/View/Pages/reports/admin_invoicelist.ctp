<div class="fieldset">
    <h3 class="legend">
        Invoice list
        <div class="total" style="float:right">
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <input type="hidden" name="pageAction" id="pageAction"/>     
            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table"> 
                    <tr class="head">
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Member ID</td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Member type/Account type</td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Client ID</td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Invoice ID</td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Invoice date</td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Invoice Status</td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Amount</td>
                    </tr>
               <?php if(!empty($data) && count($data)>0)
                     {
                            $exPaginator->options = array('url' => $this->passedArgs);
                            $paginator->options(array('url' => $this->passedArgs));
                            foreach($data as $invoice)
                            {
                                $mtr = "M".str_pad($invoice['Mentorship']['mentor_id'],4,0,STR_PAD_LEFT);
                                $mte = "S".str_pad($invoice['Mentorship']['mentee_id'],4,0,STR_PAD_LEFT);
                                
                                $userLinkMentor = $html->link($mtr,array('controller' => 'members', 'action' => 'edit/'.$invoice['Mentorship']['mentor_id']) , array("title" => "", "escape" => false));
                                $userLinkMentee = $html->link($mte,array('controller' => 'clients', 'action' => 'edit/'.$invoice['Mentorship']['mentee_id']) , array("title" => "", "escape" => false));
               ?>
                    <tr>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php echo ucfirst(strtolower($invoice['Mentor']['first_name'])).' '.ucfirst(strtolower($invoice['Mentor']['last_name'])).' ('.$userLinkMentor.')'; ?>
                        </td>
                        <td width="15%" align="center" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                            <?php
                                echo $invoice['User']['mentor_type'];
                            ?>
                        </td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">
                            <?php echo ucfirst(strtolower($invoice['Mentee']['first_name'])).' '.ucfirst(strtolower($invoice['Mentee']['last_name'])).' ('.$userLinkMentee.')'; ?>
                        </td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $invoice['Invoice']['inv_no']; ?></td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $invoice['Invoice']['created'];  ?></td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">
                                <?php if($invoice['Invoice']['inv_create_status']==1): echo "Paid"; else: echo "Unpaid"; endif; ?>                            
                        </td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">$<?php echo number_format($invoice['Invoice']['total'],2); ?></td>
                    </tr> 
               <?php }
               }else{ ?>
                   <tr>
                        <td width="15%" colspan="5" align="center" style="height: 30px;" valign="middle" class="Bdrrightbot" >No Record found</td>
             <?php  }
               ?>  
                   
                </table>
            </div>
            <?php
     //   }
        ?>

    </div>
</div>
<div class="clr"></div>
<?php echo $this->element('admin/admin_paging', array("paging_model_name" => "Invoice", "total_title" => "")); ?>  