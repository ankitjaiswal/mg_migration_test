<div class="fieldset">
    <h3 class="legend">
		Search Details
        <div class="total" style="float:right">
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td width="5%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Id</td>
                        <td  width="40%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Referring user</td>
                        <td  width="40%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Referred</td>
                        <td  width="15%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Role</td>
                    </tr>
                    <?php 
                    if(!empty($data) && count($data)>0)
                    { 
                        foreach($data as $msg)
                        {    
                        
                        ?>
                        <tr>
                            <td width="5%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo $msg['Referrals']['id']; ?></td>
                            <td  width="40%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $msg['Referrals']['mentorEmailId']; ?></td>
                            <td  width="40%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $msg['Referrals']['ReferalEmailId']; ?>  </td>  
                            <td  width="15%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $msg['Referrals']['role']; ?>  </td>
                        </tr>	
                   <?php
                        }
                    }else{ ?>
                   <tr>
                        <td colspan="3" width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Record not found</td>           
                    </tr>
                   <?php } ?>   
                </table>
            </div>
            <?php
     //   }
        ?>

    </div>
</div>
<div class="clr"></div>
<?php //echo $this->element('admin/admin_paging', array("paging_model_name" => "NewsLetter", "total_title" => "NewsLetter")); ?>	 