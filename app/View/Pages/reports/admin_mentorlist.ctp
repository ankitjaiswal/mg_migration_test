<?php e($javascript->link(array('fancybox/jquery.fancybox-1.3.4.pack', 'fancybox/jquery.mousewheel-3.0.4.pack'), false)); ?>
<?php e($html->css(array('jquery.fancybox-1.3.4')), false); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.addAttribute').fancybox();
    });     
</script>

<div class="fieldset">
    <h3 class="legend">
        Member users list
        <div class="total" style="float:right"> Total users : <?php e($this->params["paging"]['User']["count"]); ?>
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <?php e($form->create('User', array('name' => 'Admin', 'url' => array('controller' => 'members', 'action' => 'process')))); ?>    
        <input type="hidden" name="pageAction" id="pageAction"/>     
        <?php
        if (!empty($data)) {
            $exPaginator->options = array('url' => $this->passedArgs);
            $paginator->options(array('url' => $this->passedArgs));
            ?> 

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="2" cellspacing="0" class="Admin2Table"> 
                    <tr class="head">
                        <td width="5%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php e($exPaginator->sort('User ID', 'User.id')) ?></td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php e($exPaginator->sort('User name', 'UserReference.first_name')) ?></td>
                       <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php e($exPaginator->sort('Start Date', 'User.created')) ?></td>
                       <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Location</td>
                       <td width="20%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Mode</td>
                       <td width="8%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Accepting application</td>
                        <td width="8%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Application Fee</td>
                        <td width="8%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Initial Consultation</td>
                        <td width="8%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Session Fee</td>
                      <?php /*  <td width="8%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Member statics</td> */ ?>
                    </tr>   
                    <?php
                    foreach ($data as $value) {
                        ?>
                        <tr>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                             <?php
                             $formatIDMentor = "M".str_pad($value['User']['id'],4,0,STR_PAD_LEFT);
                             
                             $userLinkMentor = $html->link($formatIDMentor,array('controller' => 'members', 'action' => 'edit/'.$value['User']['id']) , array("title" => "", "escape" => false));
                             echo $userLinkMentor; ?></td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php e(ucfirst(strtolower($value['UserReference']['first_name'])) . ' ' . ucfirst(strtolower($value['UserReference']['last_name']))); ?>
                           </td>
                           <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php e(date('M, d Y',strtotime($value['User']['created']))); ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                              <?php //echo $this->General->getCountry($value['UserReference']['zipcode']); ?>
                              <?php echo $value['UserReference']['zipcode']; ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                            <?php 
                            if(!empty($value['Communication']) && count($value['Communication'])>0)
                                    {
                                        foreach($value['Communication'] as $comm)
                                        {
                                            if($comm['mode_type']!=''):
                                            echo $comm['mode_type']." : ".$comm['mode']."<br/>";
                                            endif;
                                        }    
                                } else{
                                            echo "None";
                            }?>
                             </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php 
                                    if($value['UserReference']['accept_application']=='Y'):
                                        echo 'Yes';
                                    else:
                                        echo 'No';
                                    endif;
                                ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                $<?php if($value['UserReference']['application_5_fee']!=0): echo '5'; else: echo '0'; endif;?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                $<?php echo $value['UserReference']['fee_first_hour'];?>
                            </td>
                             <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                $<?php echo $value['UserReference']['fee_regular_session'];?>
                            </td>
                        <?php /*        <td align="left" valign="middle" class="Bdrrightbot">
                                <?php echo $html->link("view all", array('controller' => 'reports', 'action' => 'mentor_static/'.$value['User']['id']), array("title" => "", "escape" => false)); ?>
                            </td> */ ?>
                        </tr>   
                        <?php
                    }
                    ?>
                </table>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<div class="clr"></div>
<?php echo $this->element('admin/admin_paging', array("paging_model_name" => "User", "total_title" => "User")); ?>   