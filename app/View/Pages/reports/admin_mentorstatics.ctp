<?php e($html->css(array('jquery/jquery-ui'))); ?>
<?php e($javascript->link(array('caljs/jquery-1.9.1','caljs/jquery-ui')));?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery( "#startdate" ).datepicker();
        jQuery( "#enddate" ).datepicker();
        jQuery('#total_paging').hide();
    });		
</script>
<?php $st= ''; $en='';
if(isset($firstTime) && $firstTime=='enter')
{
    $st = $start;
    $en = $end;
}
if(isset($this->params['pass'][0])){
    $st = str_replace("_","/",$this->params['pass'][0]);
}
if(isset($this->params['pass'][1])){
    $en = str_replace("_","/",$this->params['pass'][1]);
}
    
?>
<div class="adminrightinner">
    <div class="tablewapper2 AdminForm">
        <h3 class="legend1">Search</h3>
        <?php e($form->create('Mentor', array('url' => array('admin' => true, 'controller' => 'reports', 'action' => 'mentorstatics'),'onsubmit'=>'return chkDate();'))); ?>
        <table border="0" width="50%" class="Admin2Table">
            <tbody>
                <tr>
                    <td width="18%" valign="middle" class="Padleft26">Start date :</td>
                    <td><?php e($form->input('start', array('id'=>'startdate','value'=>$st,'label' => false,'readonly'=>true, 'div' => false, 'class' => 'InputBox'))); ?>  </td>
                    <td width="18%" valign="middle" class="Padleft26">End date :</td>
                    <td><?php e($form->input('end', array('id'=>'enddate','label' => false,'value'=>$en,'readonly'=>true, 'div' => false, 'class' => 'InputBox'))); ?>  </td>
                </tr>
                <tr>
                    <td valign="middle" class="Padleft26">&nbsp;</td>
                    <td align="left"> 

                        <?php e($form->submit('Search', array('div' => false, 'class' => 'submit_button'))); ?>                 
                    </td>
                </tr>              
            </tbody>
        </table> 
        <?php e($form->end()); ?>    
        <div style="clear: both;"></div>
    </div>
</div>

<div class="fieldset">
    <h3 class="legend">
		Member statistics
        <div class="total" style="float:right">
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <input type="hidden" name="pageAction" id="pageAction"/>
            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">User ID</td>
                        <td width="15%" align="center" valign="middle" class="Bdrrightbot" style="padding-left:9px;">No. Of clients</td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">No. Of Sessions</td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Member Earnings</td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Mentors Guild Earnings</td>
                       <?php /* <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Earnings this week </td> */ ?>
                    </tr>
                 <?php if(count($data)>0)
                       {
                           $send_start = str_replace("/", '_', $start);
                           $send_end = str_replace("/", '_', $end);
                           $exPaginator->options = array('url' => $this->passedArgs);
                           $paginator->options(array('url' => array($send_start,$send_end)));
                           foreach($data as $staticData)
                           {
                               $mtr = "M".str_pad($staticData['Mentorship']['mentor_id'],4,0,STR_PAD_LEFT);
                               
                               $userLinkMentor = $html->link($mtr,array('controller' => 'members', 'action' => 'edit/'.$staticData['Mentorship']['mentor_id']) , array("title" => "", "escape" => false));
                               $userName = $this->General->getUserReferenceData($staticData['Mentorship']['mentor_id']);
                               $menterName = ucfirst(strtolower($userName['UserReference']['first_name']))." ".ucfirst(strtolower($userName['UserReference']['last_name']))."(".$userLinkMentor.")";
                               $mentstaticData = $this->General->getMentorStaticAdmin($staticData['Mentorship']['mentor_id'],$start,$end);
                               ?>   
                                <tr>
                                    <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo $menterName; ?></td>
                                    <td width="15%" align="center" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo $mentstaticData['menteeCount']; ?></td>
                                    <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $mentstaticData['sessionCount']; ?></td>
                                    <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">$<?php echo number_format($mentstaticData['amount'],2); ?></td>
                                    <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">$<?php echo number_format($mentstaticData['commission'],2); ?></td>
                                 <?php /*  <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">$<?php echo number_format($data['weekearning'],2); ?></td> */ ?>
                                </tr>
               <?php       }
                        }
                        else { ?>
                               <tr height="30">
                                    <td width="100%" align="center" colspan="5" valign="middle" class="Bdrrightbot">Record not found</td>
                               </tr>            	
               <?php          } ?>
                   
                </table>
            </div>
            <?php
     //   }
        ?>

    </div>
</div>
<div class="clr"></div>
<?php echo $this->element('admin/admin_paging', array("paging_model_name" => "Mentorship", "total_title" => "")); ?>   
<script type="text/javascript">

    function chkDate()
    {
        var strt = jQuery('#startdate').val();
        var end = jQuery('#enddate').val();
        if(strt=='' || end=='')
        {
            if(strt=='')
            {
                jQuery('#startdate').css('border-color','#F00');
            }
            else{
                 jQuery('#startdate').css('border-color','');
            }
            
            if(end=='')
            {
                jQuery('#enddate').css('border-color','#F00');
            }
            else{
                 jQuery('#enddate').css('border-color','');
            }
            return false;
        }
        else
        {
            return true;   
        }
    }    
</script>	 