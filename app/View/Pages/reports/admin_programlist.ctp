<?php e($html->css(array('jquery/jquery-ui'))); ?>
<?php e($javascript->link(array('caljs/jquery-1.9.1','caljs/jquery-ui')));?> 
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery( "#startdate" ).datepicker();
        jQuery( "#enddate" ).datepicker();
    });     
</script>

<div class="adminrightinner">
    <div class="tablewapper2 AdminForm">
        <h3 class="legend1">Search</h3>
        <?php e($form->create('Member', array('url' => array('admin' => true, 'controller' => 'reports', 'action' => 'programlist'),'onsubmit'=>'return chkDate();'))); ?>
        <table border="0" width="50%" class="Admin2Table">
            <tbody>
                <tr>
                    <td width="18%" valign="middle" class="Padleft26">Start date :</td>
                    <td><?php e($form->input('start', array('id'=>'startdate','label' => false,'readonly'=>true, 'div' => false, 'class' => 'InputBox'))); ?>  </td>
                    <td width="18%" valign="middle" class="Padleft26">End date :</td>
                    <td><?php e($form->input('end', array('id'=>'enddate','label' => false,'readonly'=>true, 'div' => false, 'class' => 'InputBox'))); ?>  </td>
                </tr>
                <tr>
                    <td valign="middle" class="Padleft26">&nbsp;</td>
                    <td align="left"> 

                        <?php e($form->submit('Search', array('div' => false, 'class' => 'submit_button'))); ?>                 
                    </td>
                </tr>              
            </tbody>
        </table> 
        <?php e($form->end()); ?>    
        <div style="clear: both;"></div>
    </div>
</div>

<div class="fieldset">
    <h3 class="legend">
        Program list
        <div class="total" style="float:right">
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <input type="hidden" name="pageAction" id="pageAction"/>     
            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table"> 
                    <tr class="head">
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Program ID</td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Member ID</td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Mentee ID</td>
                      <?php /*  <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Program Status</td> */ ?>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Payment Total</td>
                      <?php /*  <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Payment This week</td> */ ?>
                    </tr>
               <?php if(!empty($data) && count($data)>0)
                     {
                        $col3=''; $i=0;
                            foreach($data as $apply)
                            {
                                $totalAmount = $apply[0]['fee'] + $apply[0]['total'];
                                $userInfoMentor = $this->General->getUserReferenceData($apply['mentor_id']);
                                $userInfoMentee = $this->General->getUserReferenceData($apply['mentee_id']);
                                $prgId = str_pad($apply['id'],4,0,STR_PAD_LEFT);
                                
                                $mtr = "M".str_pad($apply['mentor_id'],4,0,STR_PAD_LEFT);
                                $mte = "S".str_pad($apply['mentee_id'],4,0,STR_PAD_LEFT);
                                $userLinkMentor = $html->link($mtr,array('controller' => 'members', 'action' => 'edit/'.$apply['mentor_id']) , array("title" => "", "escape" => false));
                                $userLinkMentee = $html->link($mte,array('controller' => 'clients', 'action' => 'edit/'.$apply['mentee_id']) , array("title" => "", "escape" => false));
                               
                                
               ?>
                    <tr>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">P<?php echo $prgId; ?></td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo ucfirst(strtolower($userInfoMentor['UserReference']['first_name'])).' '.ucfirst(strtolower($userInfoMentor['UserReference']['last_name'])).'('.$userLinkMentor.')'; ?></td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo ucfirst(strtolower($userInfoMentee['UserReference']['first_name'])).' '.ucfirst(strtolower($userInfoMentee['UserReference']['last_name'])).'('.$userLinkMentee.')'; ?></td>
                      <?php /*  <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $col3; ?></td> */ ?>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">$<?php echo number_format($totalAmount,2); ?></td>
                     <?php /*  <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">$<?php echo number_format($invTotal,2); ?></td> */ ?>
                    </tr> 
               <?php 
                    }
               }else{ ?>
                   <tr>
                        <td width="15%" colspan="5" align="center" style="height: 30px;" valign="middle" class="Bdrrightbot" >No Record found</td>
             <?php  }
               ?>  
                   
                </table>
            </div>
            <?php
     //   }
        ?>

    </div>
</div>
<div class="clr"></div>
<script>
    function chkDate()
    {
        var strt = jQuery('#startdate').val();
        var end = jQuery('#enddate').val();
        if(strt=='' || end=='')
        {
            if(strt=='')
            {
                jQuery('#startdate').css('border-color','#F00');
            }
            else{
                 jQuery('#startdate').css('border-color','');
            }
            
            if(end=='')
            {
                jQuery('#enddate').css('border-color','#F00');
            }
            else{
                 jQuery('#enddate').css('border-color','');
            }
            return false;
        }
        else
        {
            return true;   
        }
    }    
</script>    