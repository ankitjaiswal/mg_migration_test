<?php e($html->css(array('jquery/jquery-ui'))); ?>
<?php e($javascript->link(array('caljs/jquery-1.9.1','caljs/jquery-ui')));?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery( "#startdate" ).datepicker();
        jQuery( "#enddate" ).datepicker();
    });     
</script>
<?php 
$st = ''; $en = '';
if(isset($start) && isset($end))
{
    $st = $start;
    $en = $end;
} ?>
<div class="adminrightinner">
    <div class="tablewapper2 AdminForm">
        <h3 class="legend1">Search</h3>
        <?php e($form->create('Member', array('url' => array('admin' => true, 'controller' => 'reports', 'action' => 'weeklyprogramlist'),'onsubmit'=>'return chkDate();'))); ?>
        <table border="0" width="50%" class="Admin2Table">
            <tbody>
                <tr>
                    <td width="18%" valign="middle" class="Padleft26">Start date :</td>
                    <td><?php e($form->input('start', array('id'=>'startdate','vlaue'=>$st,'label' => false,'readonly'=>true, 'div' => false, 'class' => 'InputBox'))); ?>  </td>
                    <td width="18%" valign="middle" class="Padleft26">End date :</td>
                    <td><?php e($form->input('end', array('id'=>'enddate','value'=>$en,'label' => false,'readonly'=>true, 'div' => false, 'class' => 'InputBox'))); ?>  </td>
                </tr>
                <tr>
                    <td valign="middle" class="Padleft26">&nbsp;</td>
                    <td align="left"> 

                        <?php e($form->submit('Search', array('div' => false, 'class' => 'submit_button'))); ?>                 
                    </td>
                </tr>              
            </tbody>
        </table> 
        <?php e($form->end()); ?>    
        <div style="clear: both;"></div>
    </div>
    </div>
<div class="fieldset">
    <h3 class="legend">
		Program Status
        <div class="total" style="float:right">
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <input type="hidden" name="pageAction" id="pageAction"/>
            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Application Sent</td>
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Application Accepted</td>
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Order Confirmed</td>
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Initial Consultations</td>
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Session 1</td>
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Session 2</td>
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Other Session</td>
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Disputes</td>
                    </tr>
                    <tr>
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $appSent; ?></td>
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $appAccept; ?></td>
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $orderConfirm; ?></td>
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $intialConultation; ?></td>
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $session1; ?></td>
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $session2; ?></td>
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $otherSession; ?></td>
                        <td  width="13%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $disputes; ?></td>
                    </tr>	
                   
                </table>
            </div>
            <?php
     //   }
        ?>

    </div>
</div>
<div class="clr"></div>
<script type="text/javascript">
    function chkDate()
    {
        var strt = jQuery('#startdate').val();
        var end = jQuery('#enddate').val();
        if(strt=='' || end=='')
        {
            if(strt=='')
            {
                jQuery('#startdate').css('border-color','#F00');
            }
            else{
                 jQuery('#startdate').css('border-color','');
            }
            
            if(end=='')
            {
                jQuery('#enddate').css('border-color','#F00');
            }
            else{
                 jQuery('#enddate').css('border-color','');
            }
            return false;
        }
        else
        {
            return true;   
        }
    }    
</script>