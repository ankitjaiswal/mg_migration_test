<?php e($html->css(array('regist_popup'))); ?>
<?php if($this->Session->read('Auth.User.id')==''):?>
<div id="registerationFm" style="width:100%;">
	<h1 style="margin-bottom:14px; padding-bottom:8px;">Subscribe to newsletter</h1>
		<?php e($form->create('NewsLetter',array('url'=>array('controller'=>'news_letters','action'=>'subscribe'),'id'=>'subscribeForm'))); ?>
		
		<table style="padding:0px;padding-top:10px;border:0px solid red;width:100%;" id="subNewsLetter">
			
			<tr>
				<td colspan="2">
				<div id="errorMessage" style="padding-bottom:3px;" class="errormsg"></div>
				<div class="input_box">
				<?php 
				
				e($form->input('email',array('class'=>'forminput','id'=>'subscribeEmail','label'=>false,'div'=>false,'placeholder'=>'Email')));
				?>	
				<div id="SubscribeEmailErr"  class="errormsg"></div></div>			
				</td>
			</tr>
			<tr>
				<td style="float:left;">
				<?php e($form->submit('Submit',array('class'=>'btn','onclick'=>'return validatesubscribe();')));?>
				
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<?php e($form->end()); ?>
			<div class="floatL"></div>
			</td>
			</tr>
		</table>
</div>
<div style="clear:both;"></div>
		<?php endif;?>
<?php if(isset($status) && $status==1):?>
<div id="registerationFm" style="width:88%; margin:40px 0 0 25px;">
		<table style="padding:0px;padding-top:50px 0 0 40px;border:0px solid red;width:100%;">
			<tr>
				<td style="float:left;">
				You have subscribed to Mentors Guild newsletter.			
				</td>
			</tr>
		</table>
</div>
<?php endif;?>
<div id="succmsg" style="display:none; margin:40px 0 0 25px;">
<table style="padding:0px;padding-top:50px 0 0 40px;border:0px solid red;width:100%;">
			<tr>
				<td style="float:left;">
				You have subscribed to Mentors Guild newsletter.			
				</td>
			</tr>
</table>
</div>