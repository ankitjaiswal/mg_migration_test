<?php e($javascript->link(array('fancybox/jquery.fancybox-1.3.4.pack', 'fancybox/jquery.mousewheel-3.0.4.pack'), false)); ?>
<?php e($html->css(array('jquery.fancybox-1.3.4')), false); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.addAttribute').fancybox();
    });		
</script>

<div class="adminrightinner">
    <div class="tablewapper2 AdminForm">
        <h3 class="legend1">Search</h3>
        <?php e($form->create('NewsLetter', array('url' => array('admin' => true, 'controller' => 'news_letters', 'action' => 'index')))); ?>
        <table border="0" width="100%" class="Admin2Table">
            <tbody>
                <tr>
                    <td width="18%" valign="middle" class="Padleft26">Subscriber's email :</td>
                    <td><?php e($form->input('email', array('label' => false, 'div' => false, 'class' => 'InputBox'))); ?>  </td>
                </tr>
                <tr>
                    <td valign="middle" class="Padleft26">&nbsp;</td>
                    <td align="left"> 

                        <?php e($form->submit('Search', array('div' => false, 'class' => 'submit_button'))); ?>                 
                    </td>
                </tr>              
            </tbody>
        </table> 
        <?php e($form->end()); ?>    
        <div style="clear: both;"></div>
    </div>
</div>

<div class="fieldset">
    <h3 class="legend">
		Newsletters Subscribers
        <div class="total" style="float:right"> Total Subscriber's member : <?php e($this->params["paging"]['NewsLetter']["count"]); ?>
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <?php e($form->create('NewsLetter', array('name' => 'Admin', 'url' => array('controller' => 'news_letters', 'action' => 'process')))); ?>    
        <input type="hidden" name="pageAction" id="pageAction"/>	 
        <?php
        if (!empty($data)) {
            $exPaginator->options = array('url' => $this->passedArgs);
            $paginator->options(array('url' => $this->passedArgs));
            ?> 

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td width="5%" align="center" valign="middle" class="Bdrrightbot Padtopbot6">
                            <input name="chkbox_n" id="chkbox_id" type="checkbox" value="" class="Chkbox" onclick="javascript:check_uncheck('Admin')" />
                        </td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php e($exPaginator->sort('Email', 'NewsLetter.email')) ?></td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php e($exPaginator->sort('Created Date', 'NewsLetter.created')) ?></td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Action</td>
                    </tr>	
                    <?php
                    foreach ($data as $value) {
                        ?>
                        <tr>
                            <td align="center" valign="middle" class="Bdrrightbot Padtopbot6">
        <?php e($form->checkbox('NewsLetter.' . $value['NewsLetter']['id'], array("class" => "Chkbox", 'value' => $value['NewsLetter']['id']))) ?>
                            </td>	
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php e($value['NewsLetter']['email']); ?></td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
        <?php e(date('m-d-Y h:i:s',strtotime($value['NewsLetter']['created']))); ?>
                            </td>						
                            <td align="center" valign="middle" class="Bdrbot ActionIcon">
        <?php e($admin->getActionImage(array('delete' => array('controller' => 'news_letters', 'action' => 'delete', 'token' => $this->params['_Token']['key'])), $value['NewsLetter']['id'])); ?>
                            </td>
                        </tr>	
                        <?php
                    }
                    ?>
                </table>
            </div>
            <?php
        }
        ?>
<?php if (!empty($data)) { ?>
            <div class="buttonwapper">
                <?php e($form->submit('Delete', array('name' => 'delete', 'class' => 'cancel_button', "type" => "button", "onclick" => "javascript:return validateChk('Admin', 'delete');"))); ?>
            </div>
            <?php
            e($form->end());
        } else {
            ?>
            <div style="color:blue, font-size:20; padding-bottom:30px;padding-top:30px;padding-left:280px" ><strong>No Records Found.</strong></div>
            <div class="Addnew_button">
            </div>
        <?php } ?>

    </div>
</div>
<div class="clr"></div>
<?php echo $this->element('admin/admin_paging', array("paging_model_name" => "NewsLetter", "total_title" => "NewsLetter")); ?>	 