<?php e($html->css(array('regist_popup'))); ?>
<div id="registerationFm">
	<h1 style="font-size:18px;">Feedback for <?php echo ucfirst(strtolower($mentorData['UserReference']['first_name'])).' '.ucfirst(strtolower($mentorData['UserReference']['last_name']));?>
	</h1>
	<!-- VL 27/12-->
	<?php 
	$feedCount = 0;
	if(isset($data) && count($data)>0){ ?>
	<div class="feedbackcontent" style="margin-bottom: -17px; min-height:150px;">
		<?php 
		$feedCount = count($data);
        $i=0;
        foreach($data as $feed)
		{
		     $userUrl = $this->General->getUserReferenceData($feed['Feedback']['client_id']); 
             if($userUrl['User']['role_id'] == Configure::read('App.Role.Mentee'))
             	$menteeUrl = SITE_URL."clients/my_account/".strtolower($userUrl['UserReference']['url_key']);
             else
             	$menteeUrl = SITE_URL.strtolower($userUrl['UserReference']['url_key']);
             
		      if($i<2){
        ?>
		<p align="justify" style="padding-bottom:5px !important;padding-top:5px !important;line-height:20px;"><a href="<?php echo $menteeUrl; ?>" target="_blank"><?php echo ucfirst(strtolower($feed['Mentee']['first_name'])).' '.ucfirst(strtolower($feed['Mentee']['last_name']));?></a> <?php echo date('jS F, Y',$feed['Feedback']['created']); ?><br />		
	       <div style="padding-top: 4px; padding-bottom: 4px; text-align: justify; text-indent: 20px;">
	           <?php 
	               $feedLength = strlen($feed['Feedback']['description']);
                   echo substr(nl2br(strip_tags($feed['Feedback']['description'])),0,227);
                   if($feedLength>227): echo "..."; endif;
               ?>
	       </div>
	    </p>
	<?php }
          $i++; 
        }?>
	</div>
	<?php }else{ ?>
	<div class="feedbackcontent" >
        <p style="padding-top: 20px;">No feedback yet from Mentors Guild clients</p>     
	</div>
	
    </div>
    <?php } 
    if($feedCount!=0){
    ?>
    	<?php if(isset($mentorData['UserReference']['feedback_link'])  && $mentorData['UserReference']['feedback_link'] != ''){ ?>
    	<div style="position: absolute; bottom: 40px; right: 30px;">
    		<span >
	    		<a class="registeration" alt="Registration" href="javascript:other_testimonials('<?php echo $id?>');">Other testimonials</a>
	    		<span style="padding:0px 15px;">|</span>
    			<a href="<?php echo SITE_URL."feedback/feedbackall/".$id?>">Read more feedback</a>
    		</span>
		</div>
		<?php } else {?>
		
		<div style="position: absolute; bottom: 40px; right: 30px;">
    		<span >
    			<a href="<?php echo SITE_URL."feedback/feedbackall/".$id?>">Read more feedback</a>
    		</span>
		</div>
		
		<?php }?>
		
    <?php } ?>
	<?php e($form->end()); ?>	

<style>
    .feedbackcontent{
        padding-top:0px!important;
    }
</style>