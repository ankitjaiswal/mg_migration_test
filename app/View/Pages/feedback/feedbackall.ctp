<style>
#pagination-flickr span{ font-size:15px; padding:5px; }
</style>
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
<?php e($form->create('Feedback',array('url'=>array('controller'=>'feedback','action'=>'feedbackall'),'id'=>'invoiceForm'))); ?>
     <div id="registerationFm" style="width:550px;min-height: 450px;">
        <?php //$MenterData = $this->General->getUserInfo($this->Session->read('Auth.User.id')); ?>
        <h1 style="font-size:18px; margin-top: 15px;">Feedback for <?php echo ucfirst(strtolower($mentorData['UserReference']['first_name'])).' '.ucfirst(strtolower($mentorData['UserReference']['last_name']));?></h1>
        <!-- VL 27/12-->
        <?php if(isset($data) && count($data)>0){ ?>
        <div class="feedbackcontent">
            <?php 
              $feedCount = count($data);
              $i=1;
              foreach($data as $feed)
              {
                  $userUrl = $this->General->getUserReferenceData($feed['Feedback']['client_id']); 
                  if($userUrl['User']['role_id'] == Configure::read('App.Role.Mentee'))
                  	$menteeUrl = SITE_URL."clients/my_account/".strtolower($userUrl['UserReference']['url_key']); 
                  else
                  	$menteeUrl = SITE_URL.strtolower($userUrl['UserReference']['url_key']);
                  $prv=''; $pub=''; 
                  if($feed['Feedback']['recommended']=='0')
                    $recom = 'No';
                  if($feed['Feedback']['recommended']=='1')
                    $recom = 'Maybe';
                  if($feed['Feedback']['recommended']=='2')
                    $recom = 'Yes';
                   
                  if($feed['Feedback']['type']=='0')
                    $prv='checked="checked"'; 
                  if($feed['Feedback']['type']=='1')
                        $pub='checked="checked"'; 
            ?>
            
            
            <?php if($this->Session->read('Auth.User.id')==$id){?>
                <p align="justify" style="padding-bottom:5px !important;"><a href="<?php echo $menteeUrl; ?>" target="_blank"><?php echo ucfirst(strtolower($feed['Mentee']['first_name'])).' '.ucfirst(strtolower($feed['Mentee']['last_name']));?></a> <?php echo date('jS F, Y',$feed['Feedback']['created']); ?><br />     
                    <div style="padding-top: 4px; padding-bottom: 4px; text-indent: 20px; text-align: justify;"><?php e(nl2br(strip_tags($feed['Feedback']['description'])));?></div>
                </p>
                <span>Recommended:&nbsp;</span><?php echo $recom; ?>
                <div class="styled-form feedback-form" style="margin-bottom:2px;">
                    <input onchange="" type="radio" <?php echo $prv; ?> id="inv_mode<?php echo $feed['Feedback']['id'];?>_p1" name="data[Feedback][inv_mode<?php echo $feed['Feedback']['id'];?>]" value="0">
                    <label for="inv_mode<?php echo $feed['Feedback']['id'];?>_p1" style="font-family:'Ubuntu';">Private</label>
                    
                    <input onchange=""  type="radio" <?php echo $pub; ?> id="inv_mode<?php echo $feed['Feedback']['id'];?>_p2" name="data[Feedback][inv_mode<?php echo $feed['Feedback']['id'];?>]" value="1">
                    <label for="inv_mode<?php echo $feed['Feedback']['id'];?>_p2" style="font-family:'Ubuntu';">Public</label>
                    <?php echo $html->image('loading.gif',array('style'=>'display:none;','id'=>'loadImage'.$feed['Feedback']['id'])); ?>
               </div><br/>
        <?php
                    echo $form->hidden('feedId'.$i,array('value'=>"feedId_".$feed['Feedback']['id']));
                    echo $form->hidden('feedbackCount',array('value'=>$i));
                }
                else { 
                        if($feed['Feedback']['type']=='1'){
                    ?>
                        <p align="justify" style="padding-bottom:5px !important;"><a href="<?php echo $menteeUrl; ?>" target="_blank"><?php echo ucfirst(strtolower($feed['Mentee']['first_name'])).' '.ucfirst(strtolower($feed['Mentee']['last_name']));?></a> <?php echo date('jS F, Y',$feed['Feedback']['created']); ?><br />     
                            <div style="padding-top: 4px; padding-bottom: 4px; text-align: justify;"><?php e(nl2br(strip_tags($feed['Feedback']['description'])));?></div>
                        </p><br />
   <?php            }
                }
                $i++;
           } ?>
  <?php    if($this->params['paging']['Feedback']['pageCount']>1)
           { ?>
              <div id="pagination-flickr" style="margin-bottom: 5px;">
                <?php
                $paginator->options(array('update'=>'CustomerPaging','url'=>array('controller'=>'feedback', 'action'=>'feedbackall/'.$id),'indicator' => 'LoadingDiv'));
                echo $paginator->prev('<< Previous', array('class' => 'previous-off'), null);
                echo $paginator->numbers(array('separator'=>'','class'=>'pagingnum')); 
                echo $paginator->next('Next >>', array('class' => 'next next-off'), null);
                ?>
            </div>
      <?php }
        }
        else{ ?>
                 <div class="feedbackcontent" style="padding-top: 12px;"><p align="justify"><strong>Record not found</strong></p></div>
  <?php } ?>
        </div>
         </div>
      <?php if($this->Session->read('Auth.User.id')==$id){?>
        <div id="Apply" role="Apply" style="float: right;clear:both; padding-top:5px; margin-bottom:10px;">
            <input id="apply" value="Save" class="submitProfile" type="submit">
            <?php //echo $html->image('Loading_Animation.gif',array('height'=>100,'style'=>'display:none;','id'=>'loadImage')); ?>
      </div>
      <?php } ?>
        <?php e($form->end()); ?> 
    </div>
</div>