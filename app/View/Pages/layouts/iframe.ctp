<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="google-site-verification" content="UroIkuqOwc9A-KngERWfg2Mam9Fw_0UmVECjjujMYMY" />
        <title>
            <?php if ($title_for_layout != ''){
            			echo $title_for_layout; ?> | <?php echo Configure::read('Site.title'); 
            	  } else {
						echo Configure::read('Site.title');
				}?>
        </title>
        <link rel="icon" href="<?php echo $this->webroot; ?>favicon.ico?v=2" type="image/x-icon" />
        <link rel="shortcut icon" href="<?php echo $this->webroot; ?>favicon.ico?v=2" type="image/x-icon" />

        <?php
        //if (isset($meta_description) || isset($meta_keywords)) {
            if (isset($meta_description) && !empty($meta_description)) {
                ?>
                <meta name="description" content="<?php e($meta_description); ?>" />
                <?php
            } else {?>
				<meta name="description" content="Expert guidance for business owners and high-potential professionals" />
				<?php
			}
            if (isset($meta_keywords) && !empty($meta_keywords)) {
                ?>
                <meta name="keywords" content="<?php e($meta_keywords); ?>" /> 
                <?php
            } else { ?>
				<meta name="keywords" content="executive coaching, executive coach, business coaching, business advice, business consulting, business expertise, CEO coach, executive development, leadership coaching" />
			<?php }
      //  } else {
           // echo $layout->meta();
        //}
        ?>  
        <?php
        e($html->css(array('front','autocomplete','image','opentip')));
        e($html->meta('icon'));
        ?>
	
	<?php e($javascript->link(array('jquery/jquery','ui/jquery-ui-1.8.2.custom.min','Placeholders','opentip-jquery')));?>
	
        <script type="text/javascript">
            jQuery.noConflict();
            var SiteUrl = "<?php echo SITE_URL; ?>";
            var SITE_URL = "<?php echo SITE_URL; ?>";	
			var loggedUserId = '';
			var mentee_url = '';
        </script>
        <!--[if lte IE 6]><style>
                img { behavior: url("<?php echo SITE_URL; ?>/css/iepngfix.htc") }
        </style><![endif]-->
        <?php e($scripts_for_layout); ?>    
        
           
    </head>
    <body>
	<div id="main-container">
	</div>
	   <script type="text/javascript">
        jQuery(document).ready(function(){
            msgClassSuccess = ''; action='';
            msgClassSuccess = jQuery("#flashMessage").attr("class");
            action = '<?php echo $this->params['action']; ?>';
            if(msgClassSuccess=='success' && action!='thanks')
            {
                <?php  $msgFlag = 1; ?>
            }
        });
    </script>
        <?php if($msgFlag==1){ echo $session->flash(); $msgFlag=0; } ?> 
        <?php e($content_for_layout); ?>
		<?php e($javascript->link(array('tiny/tinybox')));?> 
		<?php e($html->css(array('tiny/style')));?> 
		<?php e($javascript->link(array('opentiny/opentinybox')));?> 
    </body>
</html>