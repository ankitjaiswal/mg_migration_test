<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      
      

             <link rel="icon" href="<?php echo $this->webroot; ?>favicon.ico?v=2" type="image/x-icon" />
        <link rel="shortcut icon" href="<?php echo $this->webroot; ?>favicon.ico?v=2" type="image/x-icon" />
     <?php e($javascript->link(array('jquery/jquery','ui/jquery-ui-1.8.2.custom.min','Placeholders')));?>
      

     <script src="//cdn.optimizely.com/js/2182070636.js"></script>
      <?php 
     
          if((strpos($this->params['url']['url'], "executive-coaching")!== false) || (strpos($this->params['url']['url'], "marketing-management")!== false) || (strpos($this->params['url']['url'], "mergers-and-acquisitions")!== false) ||(strpos($this->params['url']['url'], "leadership-development")!== false) && (strpos($this->params['url']['url'], "qna/question")!== false)){
             
            $url = $this->params['url']['url'];
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $last = end($path); // get the value of the last element
            $last = str_replace("--","-",$last); 
            $last = str_replace("-"," ",$last);
            
            $pattern = "/(\d+)/";
            $text =  preg_split($pattern, $last, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
            $menu = ClassRegistry::init('Qna_question');  //for class load in view
            $Question = $menu->find('first', array('conditions' => array('Qna_question.id' => $text[1]))); 
            $combined = $Question['Qna_question']['question_text'] . ' ' . $Question['Qna_question']['question_context'];  
             
            $length = strlen($combined);
            if($length >= 160)
            $result = preg_replace('/\s+?(\S+)?$/', '', substr($combined, 0, 160));         
            else
            $result = $combined;
             ?>   
                
           <meta property="og:title" content="<?php e($Question['Qna_question']['question_text']);?>" />
           <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, mentors guild, management consultant" />
           <meta name="description"  property="og:description" content="<?php e($result);?>..." />

           <?php } elseif((strpos($this->params['url']['url'], "executive-coaching")!== false) || (strpos($this->params['url']['url'], "business-consulting")!== false) || (strpos($this->params['url']['url'], "marketing-management")!== false) || (strpos($this->params['url']['url'], "mergers-and-acquisitions")!== false) ||(strpos($this->params['url']['url'], "leadership-development")!== false)){
        
	    
	    $url = $this->params['url']['url'];
           //prd($url);
           $keys = parse_url($url); // parse the url
           $path = explode("/", $keys['path']); // splitting the path
           $last = end($path); // get the value of the last element
           $city = explode("-", $last); // splitting the city

	      if (strpos($url, "executive-coaching")!== false){?>
		<meta property="og:title" content="Executive Coaching Group <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?> - Mentors Guild" />
              <meta name="keywords" content="Executive Coaching <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?>" /> 
		<meta name="description"  property="og:description" content="Executive coaching can stop derailing behavior and develop leaders. Schedule free intial consultations to find the right coach."/>
        
	    <?php } elseif (strpos($url, "business-coaching")!== false){?>
		
		<meta property="og:title" content="Business Consultants <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?> - Mentors Guild" />
              <meta name="keywords" content="Business Consulting <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?>" /> 
		<meta name="description"  property="og:description" content="Business Consulting <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?> provides management consulting, business development, Strategy,planning and market research and new ventures capital services."/>
        
	   <?php } elseif (strpos($url, "marketing-management")!== false){?>
		
		<meta property="og:title" content="Marketing Management <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?> - Mentors Guild" />
              <meta name="keywords" content="Marketing Management <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?>" /> 
		<meta name="description"  property="og:description" content="Marketing management consultants in <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?> provide expertise in  customer experience, branding, customer loyalty, social media, etc. Schedule free intial consultations to find the right consultant."/>
       
         <?php } elseif (strpos($url, "mergers-and-acquisitions")!== false){?>
		
		<meta property="og:title" content="Mergers and Acquisitions (M&A) - Mentors Guild" />
              <meta name="keywords" content="Mergers and Acquisitions (M&A)" /> 
		<meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
	  

             
	 <?php } else {?>
		
		<meta property="og:title" content="Leadership Development <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?> - Mentors Guild" />
              <meta name="keywords" content="Leadership Development <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?>" /> 
		<meta name="description"  property="og:description" content="Leadership Development <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?> helps organizations improve individual leader effectiveness, organizational alignment, succession planning and the overall quality of leadership."/>
           <?php }?>

     <?php  } else{
	     $url = $this->params['url']['url'];
             

               if(strpos($url, ".")!== false && strpos($url, "fronts")!==false){
                //prd($url);
               $length = strlen($this->data['UserReference']['area_of_expertise']);
               if($length >= 120){
               $string = substr($this->data['UserReference']['area_of_expertise'], 0, strpos(wordwrap($this->data['UserReference']['area_of_expertise'], 120), "\n"));}
               else{
               $string = $this->data['UserReference']['area_of_expertise'];} 
              $city = ClassRegistry::init('City');
              
              $dataZip=$city->find('first',array('conditions'=>array('City.zip_code'=>trim($this->data['UserReference']['zipcode']))));?>
		<meta property="og:title" content="<?php e(ucfirst($this->data['UserReference']['first_name']).' '.$this->data['UserReference']['last_name']); ?> | Business Consulting | Mentors Guild" />
              <meta property="og:jobtitle" content="Consultant at Mentors Guild">
              <meta name="keywords" content="Expert in <?php e($this->data['UserReference']['categories_tags']) ?>" />
	       <meta name="description"  property="og:description" content="Send a free consultation request to <?php e(ucfirst($this->data['UserReference']['first_name']).' '.$this->data['UserReference']['last_name']); ?> (<?php echo e($dataZip['City']['city_name']) ;?>, <?php echo  e($dataZip['City']['state']) ;?>)." />

                <?php } elseif(strpos($url, ".")!== false){
               $length = strlen($this->data['UserReference']['area_of_expertise']);
               if($length >= 120){
               $string = substr($this->data['UserReference']['area_of_expertise'], 0, strpos(wordwrap($this->data['UserReference']['area_of_expertise'], 120), "\n"));}
               else{
               $string = $this->data['UserReference']['area_of_expertise'];} 
              $city = ClassRegistry::init('City');
              
              $dataZip=$city->find('first',array('conditions'=>array('City.zip_code'=>trim($this->data['UserReference']['zipcode']))));?>
		<meta property="og:title" content="<?php e(ucfirst($this->data['UserReference']['first_name']).' '.$this->data['UserReference']['last_name']); ?> | Business Consulting | Mentors Guild" />
              <meta property="og:jobtitle" content="Consultant at Mentors Guild">
              <meta name="keywords" content="Expert in <?php e($this->data['UserReference']['categories_tags']) ?>" />
	       <meta name="description"  property="og:description" content="<?php e(ucfirst($this->data['UserReference']['first_name']).' '.$this->data['UserReference']['last_name']); ?> is a leading expert in <?php e($string); ?> (<?php echo e($dataZip['City']['city_name']) ;?>, <?php echo  e($dataZip['City']['state']) ;?>). Click to view <?php e(ucfirst($this->data['UserReference']['first_name']));?>'s bio, or request a free consultation." />
	  

	    <?php } elseif(strpos($url, "pricing")!== false){?>
             <meta property="og:title" content="Subscriber Plan | Business Consulting | Mentors Guild" />
             <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, mentors guild, management consultant" />
	      <meta name="description"  property="og:description" content="Client membership allows your organization an on-demand access to outside expertise. Basic membership is free and ideal for a 1-time engagement. Pro and Enterprise memberships..." />
	  

           <?php } elseif(strpos($url, "qanda")!== false){?>
             <meta property="og:title" content="Qna | Business Consulting | Mentors Guild" />
             <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, mentors guild, management consultant" />
	      <meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
	  
        
           <?php } elseif(strpos($url, "roundtable")!==false){?>
            <meta property="og:title" content="Executive Roundtable | Business Consulting | Mentors Guild" />
            <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, mentors guild, management consultant" />
	     <meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
	  
		
	    <?php } elseif(strpos($url,'qna/category') !== false){
             
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $last = end($path); // get the value of the last element 
            ?>
           <meta property="og:title" content="<?php e($last);?> | Qna Search | Business Consulting | Mentors Guild" />
           <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, mentors guild, management consultant" />
	    <meta name="description"  content="Questions & Answers on <?php e($last);?>." />
		
	    <?php } elseif(strpos($url,'question') !== false){
       
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $last = end($path); // get the value of the last element 
            $last = str_replace("--","-",$last);
            $last = str_replace("-"," ",$last);
            $pattern = "/(\d+)/";
            $text =  preg_split($pattern, $last, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
            $menu = ClassRegistry::init('Qna_question');  //for class load in view
            $Question = $menu->find('first', array('conditions' => array('Qna_question.id' => $text[1]))); 
            $combined = $Question['Qna_question']['question_text'] . ' ' . $Question['Qna_question']['question_context'];  
             
            $length = strlen($combined);
            if($length >= 160)
            $result = preg_replace('/\s+?(\S+)?$/', '', substr($combined, 0, 160));         
            else
            $result = $combined;
             ?>   
                
           <meta property="og:title" content="<?php e($Question['Qna_question']['question_text']);?>" />
           <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, mentors guild, management consultant" />
	    <meta name="description"  property="og:description" content="<?php e($result);?>..." />
	     
            <?php } elseif(strpos($url,'insight') !== false){
             
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $last = end($path); // get the value of the last element
            $last = str_replace("--","-",$last);
            $last = str_replace("-"," ",$last);
            $pattern = "/(\d+)/";
            $text =  preg_split($pattern, $last, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
            $menu = ClassRegistry::init('Insight');  //for class load in view 
            $insight = $menu->find('first', array('conditions' => array('Insight.id' => $text[1]))); 
            $combined = $insight['Insight']['title'] . ' ' . $insight['Insight']['insight'];

            $length = strlen($combined);
            if($length >= 160)
            $result = preg_replace('/\s+?(\S+)?$/', '', substr($combined, 0, 160));         
            else
            $result = $combined;
            ?>
           <meta property="og:title" content="<?php e($insight['Insight']['title']);?>" />
           <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, mentors guild, management consultant" />
	    <meta name="description"  property="og:description" content="<?php e($result);?>..." />
	  

          <?php } elseif(strpos($url,'browse/category') !== false){
             
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $last = end($path); // get the value of the last element
            ?>
           <meta property="og:title" content="<?php e($last);?> | Business Consulting | Mentors Guild" />
           <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, mentors guild, management consultant" />
	    <meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
	  
            
           <?php } elseif(strpos($url,'browse/search_result') !== false){
             
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $last = end($path); // get the value of the last element
            ?>
           <meta property="og:title" content="<?php e(ucfirst($path[2]));?> - Mentors Guild" />
           <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, mentors guild, management consultant" />
	    <meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
	  

          <?php } elseif($url != '' && strpos($url, "/")!=false){
          
          $keys = parse_url($url); // parse the url
          
          $path = explode("/", $keys['path']); // splitting the path
          $keyword = explode("-", $path[2]); // splitting the city

          $last = end($path); // get the value of the last element 
          $city = explode("-", $last); // splitting the city
          ?>
         <meta property="og:title" content="<?php e(ucfirst($keyword[0]));?> <?php e(ucfirst($keyword[1]));?> <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?> | Business Consulting | Mentors Guild" />
         <meta name="keywords" content="<?php e(ucfirst($keyword[0]));?> <?php e(ucfirst($keyword[1]));?>  <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?>" />
	  <meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
	  
         <?php } else{?>
         <meta property="og:title" content="Business Consulting | Mentors Guild" />
         <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, mentors guild, management consultant" />
	  <meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
	  
	  <?php }}?>	


        <meta property="og:image" content="<?php e(SITE_URL);?>img/media/Final-Logo.png" />	
	 <meta property="og:url" content="<?php e(SITE_URL);?><?php e($this->params['url']['url']);?>" />
        <meta property="og:site_name" content="Mentors Guild"/>
	 <meta property="og:type" content="website" />   
        <meta name="language" content="english"/>

        <meta name="DC.Title" content="Business Consulting | Mentors Guild" />
        <meta name="DC.Creator" content="Mentors Guild" />
        <meta name="DC.Subject" content="Business Consulting" />
        <meta name="DC.Description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
        <meta name="DC.Publisher" content="Mentors Guild" />
        <meta name="DC.Date" content="2015-04-22" />
        <meta name="DC.Type" content="Text" />
        <meta name="DC.Language" content="en-US" />
        <meta name="DC.Rights" content="Mentors Guild" />  

     
	     <title><?php if(strpos($this->params['url']['url'], "qna/question")!== false && $title_for_layout != '')
                        { echo $title_for_layout; ?> | Mentors Guild
                  <?php }else if ($title_for_layout == 'Mentors Guild: Business Consulting, Made Easy'){
            			echo $title_for_layout; 
                  }else if ($title_for_layout != ''){
            			echo $title_for_layout; ?> | <?php echo Configure::read('Site.title'); 
            	    }else {
				echo Configure::read('Site.title');}?></title>
		



          <?php
        e($html->css(array('front')));
        e($html->meta('icon'));
        ?>
	
	
	  
         <script type="text/javascript">
//IPInfoDB javascript JSON query example
//Tested with FF 3.5, Opera 10, Chome 5 and IE 8
//Geolocation data is stored as serialized JSON in a cookie
//Bug reports : http://forum.ipinfodb.com/viewforum.php?f=7
function geolocate(timezone, cityPrecision, objectVar) {
 
  var api = (cityPrecision) ? "ip-city" : "ip-country";
  var domain = 'api.ipinfodb.com';
  var url = "http://" + domain + "/v3/" + api + "/?key=75c52efd0ce60f4a2da885d0696500e98f6db4c72ae8eee3ef05277d661d715a&format=json" + "&callback=" + objectVar + ".setGeoCookie";
  var geodata;
  var callbackFunc;
  var JSON = JSON || {};
 
  // implement JSON.stringify serialization
  JSON.stringify = JSON.stringify || function (obj) {
    var t = typeof (obj);
    if (t != "object" || obj === null) {
      // simple data type
      if (t == "string") obj = '"'+obj+'"';
        return String(obj);
    } else {
    // recurse array or object
      var n, v, json = [], arr = (obj && obj.constructor == Array);
      for (n in obj) {
        v = obj[n]; t = typeof(v);
        if (t == "string") v = '"'+v+'"';
        else if (t == "object" && v !== null) v = JSON.stringify(v);
        json.push((arr ? "" : '"' + n + '":') + String(v));
      }
      return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
  };
 
  // implement JSON.parse de-serialization
  JSON.parse = JSON.parse || function (str) {
    if (str === "") str = '""';
      eval("var p=" + str + ";");
      return p;
  };
 
  //Check if cookie already exist. If not, query IPInfoDB
  this.checkcookie = function(callback) {
    geolocationCookie = getCookie('geolocation');
    callbackFunc = callback;
    if (!geolocationCookie) {
      getGeolocation();
    } else {
      geodata = JSON.parse(geolocationCookie);
      callbackFunc();
    }
  }
 
  //API callback function that sets the cookie with the serialized JSON answer
  this.setGeoCookie = function(answer) {
    if (answer['statusCode'] == 'OK') {
      JSONString = JSON.stringify(answer);
      setCookie('geolocation', JSONString, 365);
      geodata = answer;
      callbackFunc();
    }
  }
 
  //Return a geolocation field
  this.getField = function(field) {
    try {
      return geodata[field];
    } catch(err) {}
  }
 
  //Request to IPInfoDB
  function getGeolocation() {
    try {
      script = document.createElement('script');
      script.src = url;
      document.body.appendChild(script);
    } catch(err) {}
  }
 
  //Set the cookie
  function setCookie(c_name, value, expire) {
    var exdate=new Date();
    exdate.setDate(exdate.getDate()+expire);
    document.cookie = c_name+ "=" +escape(value) + ((expire==null) ? "" : ";expires="+exdate.toGMTString());
  }
 
  //Get the cookie content
  function getCookie(c_name) {
    if (document.cookie.length > 0 ) {
      c_start=document.cookie.indexOf(c_name + "=");
      if (c_start != -1){
        c_start=c_start + c_name.length+1;
        c_end=document.cookie.indexOf(";",c_start);
        if (c_end == -1) {
          c_end=document.cookie.length;
        }
        return unescape(document.cookie.substring(c_start,c_end));
      }
    }
    return '';
  }
}
</script>

        <script type="text/javascript">
            jQuery.noConflict();
            var SiteUrl = "<?php echo SITE_URL; ?>";
            var SITE_URL = "<?php echo SITE_URL; ?>";	
			var loggedUserId = '';
			var mentee_url = '';
        </script>
        <!--[if lte IE 6]>
       <style type="text/css">
                img { behavior: url("<?php echo SITE_URL; ?>/css/iepngfix.htc") }
        </style><![endif]-->
        <?php e($scripts_for_layout); ?>    

<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-38592072-1', 'auto');
  ga('require', 'linkid', 'linkid.js');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>  
    <script type="application/ld+json">
{ "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "Mentors Guild",
  "url" : "http://www.mentorsguild.com",
  
  "sameAs" : [ "https://www.linkedin.com/company/2966858",
    "http://www.twitter.com/mentorsguild",
    "https://www.facebook.com/US.MentorsGuild",
    "https://plus.google.com/u/0/+MentorsguildLive"] 
}
</script>   
    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "url": "http://www.mentorsguild.com",
      "logo": "http://www.mentorsguild.com/imgs/Mentors_Guild_logo.jpeg"
    }
    </script>  
    <script type="application/ld+json">
    {  "@context" : "http://schema.org",
       "@type" : "Organization",
       "name" : "Mentors Guild",
       "alternateName" : "mentorsguild.com",
       "url" : "http://www.mentorsguild.com"
    }
    </script> 
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "Mentors Guild",
  "description": "Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results.",
  "telephone": "+1 866-511-1898"
  
}
</script> 
<!-- start Mixpanel --><script type="text/javascript">(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
mixpanel.init("fedf4dac58a94c6471db80e45a78713e");</script><!-- end Mixpanel -->
    </head>
    <body>
	<div id="main-container">
		<div id="header-wrapper">
		   <?php 
		   $msgFlag = 0;
			e($this->element('front/header'));   			
			if($this->params['controller']=='fronts' && $this->params['action']=='index'){
				e($this->element('front/home_search')); 
			}?>		
		</div>
	</div>
	   <script type="text/javascript">
        jQuery(document).ready(function(){
            msgClassSuccess = ''; action='';
            msgClassSuccess = jQuery("#flashMessage").attr("class");
            action = '<?php echo $this->params['action']; ?>';
            if(msgClassSuccess=='success' && action!='thanks')
            {
                <?php  $msgFlag = 1; ?>
            }
        });
    </script>
        <?php if($msgFlag==1){ echo $session->flash(); $msgFlag=0; } ?> 
        <?php e($content_for_layout); ?>
        <?php e($this->element("front/footer")); ?> 
        <?php //e($this->element('sql_dump'));?>
		<?php e($javascript->link(array('tiny/tinybox')));?> 
		<?php e($html->css(array('tiny/style')));?> 
		<?php e($javascript->link(array('opentiny/opentinybox')));?> 
    </body>
</html>