<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>
            <?php echo $title_for_layout; ?> | <?php echo Configure::read('Site.title'); ?>
        </title>
        <link rel="icon" href="<?php echo $this->webroot; ?>favicon.ico?v=2" type="image/x-icon" />
        <link rel="shortcut icon" href="<?php echo $this->webroot; ?>favicon.ico?v=2" type="image/x-icon" />

        <?php
        if (isset($meta_description) || isset($meta_keywords)) {
            if (isset($meta_description) && !empty($meta_description)) {
                ?>
                <meta name="description" content="<?php e($meta_description); ?>" />
                <?php
            }
            if (isset($meta_keywords) && !empty($meta_keywords)) {
                ?>
                <meta name="keywords" content="<?php e($meta_keywords); ?>" /> 
                <?php
            }
        } else {
            echo $layout->meta();
        }
        ?>  
        <?php
        e($html->css(array('front')));
        e($html->meta('icon'));
        e($html->css(array('jquery/jquery.alerts')));
        ?>
        <?php
        e($javascript->link(array(
                    'jquery/jquery',
                    'jquery/jquery.alerts',
                    'ddaccordion',
                    'front'
                )));
        ?>
        <?php e($javascript->link(array('fancybox/jquery.fancybox-1.3.4.pack', 'fancybox/jquery.mousewheel-3.0.4.pack'))); ?>
        <?php e($html->css(array('jquery.fancybox-1.3.4')), false); ?>
        <script type="text/javascript">
            jQuery.noConflict();
            var SITE_URL = "<?php echo SITE_URL; ?>";	
            var SiteUrl = "<?php echo SITE_URL; ?>";
        </script>
        <!--[if lte IE 6]><style>
                img { behavior: url("<?php echo SITE_URL; ?>/css/iepngfix.htc") }
        </style><![endif]-->
        <?php e($scripts_for_layout); ?>
        <?php //echo $html->css("/popup/css/default_theme"); ?>
    </head>
    <body>
        <div id="Wraper">
            <div id="Header">
                <?php e($this->element('front/header')); ?>
            </div>
            <div id="Middle">
                <div class="MiddleConts">
                    <div class="MidTop">
                        <div class="MidTopImgs"><?php e($html->image('images/mid_pics.png', array('alt' => 'Img'))); ?></div>
                        <div class="MidTopSrh">
                            <?php e($this->element('front/form_normal_search')); ?>				
                        </div>
                    </div>
                    <div class="MidBtm">			
                        <div class="MidTopBg">
                            <h2 class="InnerHd"><?php e($this->element('inner_head_title')); ?></h2>
                            <div class="InnerMidCnt">
                                <?php e($this->element('left')); ?>
                                <?php e($content_for_layout); ?>
                            </div>
                        </div>
                        <div class="MidBtmBg"></div>			
                    </div>
                    <div class="Clear"></div>			
                </div>
            </div>
            <div id="Footer">
                <?php e($this->element('front/footer')); ?>
            </div>
        </div>
        <?php //e($this->element('sql_dump')); ?>
    </body>
</html>