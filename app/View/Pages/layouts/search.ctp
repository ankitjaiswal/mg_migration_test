<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      
      <meta name="google-site-verification" content="UroIkuqOwc9A-KngERWfg2Mam9Fw_0UmVECjjujMYMY" />
     
        <link rel="icon" href="<?php echo $this->webroot; ?>favicon.ico?v=2" type="image/x-icon" />
        <link rel="shortcut icon" href="<?php echo $this->webroot; ?>favicon.ico?v=2" type="image/x-icon" />
      <?php if((strpos($this->params['url']['url'], "executive-coaching")!== false) || (strpos($this->params['url']['url'], "mergers-and-acquisitions")!== false) || (strpos($this->params['url']['url'], "marketing-management")!== false) || (strpos($this->params['url']['url'], "mergers-and-acquisitions")!== false) ||(strpos($this->params['url']['url'], "leadership-development")!== false)){
        
	    $url = $this->params['url']['url'];
           //prd($url);
           $keys = parse_url($url); // parse the url
           $path = explode("/", $keys['path']); // splitting the path
           $last = end($path); // get the value of the last element
           $city = explode("-", $last); // splitting the city

	      if (strpos($url, "executive-coaching")!== false){?>
		<meta property="og:title" content="Executive Coaching <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?> - Mentors Guild" />
              <meta name="keywords" content="Executive Coaching <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?>" /> 
		<meta name="description"  property="og:description" content="Executive coaching can stop derailing behavior and develop leaders. Schedule free intial consultations to find the right coach."/>
        
        
	   <?php } elseif (strpos($url, "marketing-management")!== false){?>
		
		<meta property="og:title" content="Marketing Management <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?> - Mentors Guild" />
              <meta name="keywords" content="Marketing Management <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?>" /> 
		<meta name="description"  property="og:description" content="Marketing management consultants in <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?> provide expertise in  customer experience, branding, customer loyalty, social media, etc. Schedule free intial consultations to find the right consultant."/>
       
         <?php } elseif (strpos($url, "mergers-and-acquisitions")!== false){?>
		
		<meta property="og:title" content="Mergers and Acquisitions (M&A) - Mentors Guild" />
              <meta name="keywords" content="Mergers and Acquisitions (M&A)" /> 
		<meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
	  

             
	 <?php } else {?>
		
		<meta property="og:title" content="Leadership Development <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?> - Mentors Guild" />
              <meta name="keywords" content="Leadership Development <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?>" /> 
		<meta name="description"  property="og:description" content="Leadership Development <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?> helps organizations improve individual leader effectiveness, organizational alignment, succession planning and the overall quality of leadership."/>
           <?php }?>

     <?php  } else{
	     $url = $this->params['url']['url'];
             
              if(strpos($url, ".")!== false){
               $length = strlen($this->data['UserReference']['categories_tags']);
               if($length >= 100){
                $tags = $this->data['UserReference']['categories_tags'];
               $string = preg_replace('/\s+?(\S+)?$/', '', substr($tags, 0, 110));         }
               
               else{
               $string = $this->data['UserReference']['categories_tags'];
                       } ?>
		<meta property="og:title" content="<?php e(ucfirst($this->data['UserReference']['first_name']).' '.$this->data['UserReference']['last_name']); ?> | Business Consulting | Mentors Guild" />
              <meta property="og:jobtitle" content="Consultant at Mentors Guild">
              <meta name="keywords" content="Expert in <?php e($this->data['UserReference']['categories_tags']) ?>" />
	       <meta name="description"  property="og:description" content="<?php e(ucfirst($this->data['UserReference']['first_name']).' '.$this->data['UserReference']['last_name']); ?> is an expert in <?php e($string); ?>" />
	  

	    <?php } elseif(strpos($url, "pricing")!== false){?>
             <meta property="og:title" content="Subscriber Plan | Business Consulting | Mentors Guild" />
             <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting,top coaches, top consultants, mentors guild, management consultant" />
	      <meta name="description"  property="og:description" content="Mentors Guild's client pricing for smart businesses. Consult leading experts to save time and drive results." />
	  

           <?php } elseif(strpos($url, "qanda")!== false){?>
             <meta property="og:title" content="Qna | Business Consulting | Mentors Guild" />
             <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting,top coaches, top consultants, mentors guild, management consultant" />
	      <meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
	  
        
           <?php } elseif(strpos($url, "roundtable")!==false){?>
            <meta property="og:title" content="Executive Roundtable | Business Consulting | Mentors Guild" />
            <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting,top coaches, top consultants, mentors guild, management consultant" />
	     <meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
	  
		
	    <?php } elseif(strpos($url,'qna') !== false){
       
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $last = end($path); // get the value of the last element 
            $last = str_replace("-"," ",$last);
            $pattern = "/(\d+)/";
            $text =  preg_split($pattern, $last, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
           // prd($text[0]); 
           //$this->loadModel('Qna_question'); 
           $menu = ClassRegistry::init('Qna_question');
            $Question = $menu->find('first', array('conditions' => array('Qna_question.id' => $text[1]))); 
           //$Question = $this->data['Qna_question']['question_text'];
           //prd($Question[Qna_question][question_context]);        
                ?>
           <meta property="og:title" content="<?php e($text[0]);?> | Business Consulting | Mentors Guild" />
           <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting,top coaches, top consultants, mentors guild, management consultant" />
	    <meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
	     
            <?php } elseif(strpos($url,'insight') !== false){
             
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $last = end($path); // get the value of the last element 
            ?>
           <meta property="og:title" content="<?php e($last);?> | Business Consulting | Mentors Guild" />
           <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting,top coaches, top consultants, mentors guild, management consultant" />
	    <meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
	  

          <?php } elseif(strpos($url,'browse/category') !== false){
             
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $last = end($path); // get the value of the last element
            ?>
           <meta property="og:title" content="<?php e($last);?> | Business Consulting | Mentors Guild" />
           <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting,top coaches, top consultants, mentors guild, management consultant" />
	    <meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
	  
         <?php } elseif(strpos($url,'browse/search_result') !== false){
             
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $keyword = explode("-", $path[2]);
            $last = end($path); // get the value of the last element
            $city = explode("-", $last); // splitting the city
            ?>
           <meta property="og:title" content="<?php e(ucfirst($keyword[0]));?> <?php e(ucfirst($keyword[1]));?> <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?> - Mentors Guild" />
           <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting,top coaches, top consultants, mentors guild, management consultant" />
	    <meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
	  

          <?php } elseif($url != '' && strpos($url, "/")!=false){
          
          $keys = parse_url($url); // parse the url
          
          $path = explode("/", $keys['path']); // splitting the path
          $keyword = explode("-", $path[2]); // splitting the city

          $last = end($path); // get the value of the last element 
          $city = explode("-", $last); // splitting the city
          ?>
         <meta property="og:title" content="<?php e(ucfirst($keyword[0]));?> <?php e(ucfirst($keyword[1]));?> <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?> | Business Consulting | Mentors Guild" />
         <meta name="keywords" content="<?php e(ucfirst($keyword[0]));?> <?php e(ucfirst($keyword[1]));?>  <?php e(ucfirst($city[0]));?> <?php e(ucfirst($city[1]));?>" />
	  <meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />
	  
         <?php } else{?>
         <meta property="og:title" content="Business Consulting | Executive Coaching | Mentors Guild" />
         <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting,top coaches, top consultants, mentors guild, management consultant" />
	  <meta name="description"  property="og:description" content="Mentors Guild provides financia Marketing management consulting,Executive Coaching, Business Consultants, planning and market research." />
	  
	  <?php }}?>	


        <meta property="og:image" content="<?php e(SITE_URL);?>img/media/Final-Logo.png" />	
	 <meta property="og:url" content="<?php e(SITE_URL);?><?php e($this->params['url']['url']);?>" />
        <meta property="og:site_name" content="Mentors Guild"/>
	 <meta property="og:type" content="website" />   
        <meta name="language" content="english"/>
	  <title>
            <?php if ($title_for_layout != ''){
            			echo $title_for_layout; ?> | <?php echo Configure::read('Site.title'); 
            	  } else {
						echo Configure::read('Site.title');
				}?>
        </title>



          <?php
        e($html->css(array('front')));
        e($html->meta('icon'));
        ?>

        <script type="text/javascript">
            var SiteUrl = "<?php echo SITE_URL; ?>";
            var SITE_URL = "<?php echo SITE_URL; ?>";	
			var loggedUserId = '';
			var mentee_url = '';
        </script>
        <?php e($scripts_for_layout); ?>    
           
    </head>
    <body>
	<div id="main-container">
		<div id="header-wrapper">
		   <?php 
		   $msgFlag = 0;
			e($this->element('front/header'));   			
			?>		
		</div>
	</div>
        <?php if($msgFlag==1){ echo $session->flash(); $msgFlag=0; } ?> 
        <?php e($content_for_layout); ?>
        <?php e($this->element("front/footer")); ?> 
			<?php e($javascript->link(array('tiny/tinybox')));?> 
		<?php e($html->css(array('tiny/style')));?> 
		<?php e($javascript->link(array('opentiny/opentinybox')));?> 
    </body>
</html>