<div id="inner-content-wrapper">
	<div id="inner-content-box" class="pagewidth">
		<div id="financial-management">
			<p>Oops!</p>
		</div>
<div id="mentors-content">
	<div class="box1">
	 <div class="content-right-text">
		<div class="expertise" style="margin-top:20px;width:950px;">
		<h1><span style="color: #292929;text-decoration:none">We can't find the page you're looking for...</span></h1>
               </div>
            <div style="color: #292929; margin-top:50px;font-size:14px;height:150px;width:950px;">

              <div style="float: left;margin-left: 0;width:300px;">
               <h2 style="color: #292929;font-size:16px;">What do you want to do?</h2>
                <ul class="premium-list"style="margin-top:20px;">
                  <li>Check your spelling</li>
                  <li>Try more general words</li>
                  <li>Try different words that mean the same thing</li>
                  <li><a href="<?php echo SITE_URL?>qanda" class="internal">Ask a question</a> and get answers from Experts</li>
                 </ul>
               </div>
              <div style="float: right;width:500px;margin-left:120px;">
               <h2 style="color: #292929;font-size:16px;">Top Searches</h2>

                <div style="float: left;width:140px;margin-left:0px;">
                 <ul style="list-style-type: none;margin-top:20px;">
                  <li>1. <a href="<?php echo SITE_URL?>john.baldoni">John Baldoni</a></li>
                  <li>2. <a href="<?php echo SITE_URL?>mark.palmer">Mark Palmer</a></li>
                  <li>3. <a href="<?php echo SITE_URL?>bill.fotsch">Bill Fotsch</a></li>
                  <li>4. <a href="<?php echo SITE_URL?>judy.bardwick">Judy Bardwick</a></li>
                  <li>5. <a href="<?php echo SITE_URL?>ivan.rosenberg">Ivan Rosenberg</a></li>
                 </ul>
                </div>
                <div style="float: right;width:350px;">
                <div style="float: left; width:170px;">
                    <ul style="list-style-type: none;margin-top:20px;">
                     <li>6. <a href="<?php echo SITE_URL?>browse/category/Leadership">Leadership</a></li>
                     <li>7. <a href="<?php echo SITE_URL?>browse/category/Customers">Customers</a></li>
                     <li>8. <a href="<?php echo SITE_URL?>browse/category/Interim Management">Interim Management</a></li>
                     <li>9. <a href="<?php echo SITE_URL?>browse/category/Strategy">Strategy</a></li>
                     <li>10. <a href="<?php echo SITE_URL?>browse/category/Healthcare">Healthcare</a></li>
                  </ul>
                </div>

                <div style="float: right;width:170px; margin-right:0px;">
                    <ul style="list-style-type: none;margin-top:20px;">
                     <li>11. <a href="<?php echo SITE_URL?>browse/search_result/executive-coaching/NY/new-york">Executive Coaching</a></li>
                     <li>12. <a href="<?php echo SITE_URL?>browse/category/Retail">Retail</a></li>
                     <li>13. <a href="<?php echo SITE_URL?>browse/category/Innovation">Innovation</a></li>
                     <li>14. <a href="<?php echo SITE_URL?>browse/search_result/browse-all/CA/los-angeles">Los Angeles</a></li>
                     <li>15. <a href="<?php echo SITE_URL?>browse/search_result/browse-all/NY/new-york">New York</a></li>
                 </ul>
                </div>
               </div>
              </div>

             </div>   
                  		
	 </div>
	 </div>
</div>
</div>
</div>