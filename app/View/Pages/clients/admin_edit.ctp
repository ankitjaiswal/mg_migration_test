<?php e($javascript->link(array('autocomplete/jquery.coolautosuggest')));?>
<?php e($html->css(array('autocomplete/jquery.coolautosuggest')));?>	
	<div class="adminrightinner">
		<?php 
		e($form->create('User', array('url' => array('controller' => 'clients', 'action' => 'edit'),'type'=>'file')));
		e($form->hidden('User.id'));
		e($form->hidden('UserReference.id'));
		e($form->hidden('UserImage.0.id'));
		e($form->hidden('UserImage.0.image_name'));		
		?>    
		<div class="tablewapper2 AdminForm">
			<h3 class="legend1">Client's Detail  </h3>
			<?php e($this->element('menteesuser/form'));?>
		</div>
		<div class="buttonwapper">
			<div>
				<input type="submit" value="Submit" class="submit_button" />
			</div>
			<div class="cancel_button">
				<?php echo $html->link("Cancel", "/admin/clients/index/", array("title"=>"", "escape"=>false)); ?>
			</div>
		</div>
		<?php e($form->end()); ?>
		
	</div>

