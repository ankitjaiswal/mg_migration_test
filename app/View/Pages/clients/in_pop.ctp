<?php
$error = '';
if(isset($_SESSION['pass_error']))
{
	$error  = $_SESSION['pass_error'];
	unset($_SESSION['pass_error']);
}?>
<?php 
e($html->css(array('jquery/jquery.alerts')));
e($javascript->link(array('jquery/jquery.min','jquery/jquery.alerts')));
?>

<div id="" class="tmask" style="height: 1906px; width: 1349px; opacity: 0.7; display: block;"></div>

<div class="tbox" style="position: fixed; top: 50px; left: 225.5px; opacity: 1; display: block;">
	<div id="" class="tinner" style="height:100px; width: 597px; background-image: none; padding-left:80px; padding-right:80px;">
		<div id="inner-content-wrapper">
		  <div id="inner-content-box" class="pagewidth"> 
	      <table width="100%">
					<tbody>
						<tr>
							<td>
								<div id="registerationFm">
									<div class="infobar" style="display:block;height:52px;">
										<p class="redarrow" style="float:left; margin-top:24px; font-weight:normal; padding-left:12px; color:#000;">
											 Only registered users can submit questions. Register or login using<br>
											<small> Mentors Guild will never post to your Linkedin account without your permission.</small>
										</p>
										<p style="float:left; padding-left:10px"><?php e($html->image('linkedin.png',array('alt'=>'Linkedin',"onclick"=>"linkedin('menteePopup');",'style'=>'cursor:pointer;margin-top:8px;'))); ?></p>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
		  </div>
			<?php e($form->end()); ?>
		</div>
		<div class="tclose"></div>
	</div>
</div>

<script src="http://code.jquery.com/jquery-1.7.2.js"></script>
<script>
$(document).ready(function(){
  $(function(){
  var from=['Business Strategy','Workplace Conflict','Mergers &amp; Acquisition','Employee Engagement','Executive Coaching'], i=1; // i for counting
      setInterval(function(){
          $('#from').fadeOut(function(){ //fadeout text
          $(this).html(from[i=(i+1)%from.length]).fadeIn(); //update, count and fadeIn
          });
      }, 3500 ); //2s
  
  var to=['Succession Planning','Customer Service','Change Management','Lean Six Sigma','New Product Launch'], j=1; // j for counting
      setInterval(function(){
          $('#to').fadeOut(function(){ //fadeout text
          $(this).html(to[j=(j+1)%to.length]).fadeIn(); //update, count and fadeIn
          });
      }, 3500 ); //2s

  });
});
</script>