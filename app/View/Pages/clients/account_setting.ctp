<?php
$error = '';
if(isset($_SESSION['pass_error']))
{
	$error  = $_SESSION['pass_error'];
	unset($_SESSION['pass_error']);
}?>
<?php 
e($html->css(array('jquery/jquery.alerts')));
e($javascript->link(array('jquery/jquery.min','jquery/jquery.alerts')));
?>
 <div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
    
      <div id="user-account">
      <?php e($form->create('User', array('url' => array('controller' => 'clients', 'action' => 'account_setting'),'id'=>'accountSettingForm')));?>     
	  <?php e($form->hidden('UserReference.id',array('value'=>$userData['UserReference']['id']))); ?>    
        <div class="account-form">
		<div class="heading">
              <h1>Account Settings</h1>
            </div>
		<!--<div class="errormsg" id="passError"></div>-->
          <div class="column first">
           
            <div class="clear"></div>
            <div> 
       
            </div>
            <div class="clear"> </div>
            <div>
			
				<span id="passwordError"></span>
                
                 
                <div class="clear"> </div>              
            </div>
			<div class="clear"> </div>
            <div class="subheading">
              <h2>Change password</h2>
            </div>
            <div class="clear"> </div>
            <div>
				<span id="passwordError"></span>
                <div>
                 <?php e($form->password('oldpass', array('maxlength'=>20,'div'=>false, 'label'=>false, "class" => "input", "placeholder" => "Old password")));?>
				  <span id="OldNotMatch" class="errormsg" style="display:inline; float:left; display:none;"><?php echo $error; ?></span>
                  <br />
				  <?php e($form->password('newpass', array('maxlength'=>20,'div'=>false, 'label'=>false, "class" => "input", "placeholder" => "New password")));?>
                  <br />
                 <?php e($form->password('confpass', array('maxlength'=>20,'div'=>false, 'label'=>false, "class" => "input", "placeholder" => "Confirm password"))); ?>
				  <span id="commonerror" class="errormsg" style="display:inline; float:left; display:none;"></span>
				</div>
                 
                <div class="clear"> </div>              
            </div>
            <div class="clear"> </div>
          </div>
          <div class="column last">
            <div class="clear"> </div>
            <div class="subheading">
              <h2>Email notifications</h2>
            </div>
            <div class="clear"> </div>
            <div class="styled-form">               
				  <?php 
				  $options = array('W'=>'Weekly','N'=>'None');
				  e($form->radio('UserReference.email_notification',$options,array('legend'=>false,'div'=>false,'label'=>true,'default'=>$userData['UserReference']['email_notification'])));
				  ?>               
                
             <?php e($form->end()); ?>
              <div class="clear"> </div>
            </div>
            <div class="clear"> </div>
            <div class="subheading">
              <h2>Account type</h2>
            </div>
            
          <?php if($this->Session->read('Auth.User.id')!='' && $this->Session->read('Auth.User.role_id')=='3' && $this->Session->read('Auth.User.isPlanAdmin') =='1'){ ?>
        
 <p>
				<?php echo $this->Session->read('Auth.User.plan_type');?> Client (Admin)
			</p>
<?php } else{?>

          <p>
				<?php echo $this->Session->read('Auth.User.plan_type');?> Client 
			</p>
<?php } ?>
            <div class="del-acc"><?php e($html->link('My invoices',array('controller'=>'invoice','action'=>'invoicelistclient'),array('class'=>'delete'))); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php e($html->link('Delete my account','javascript:delAccount();',array('class'=>'delete'))); ?></div>
          </div>
          <div class="button-set">
                  <input name="" type="submit" id="submitAccount" onclick="return validAccountSetting();"  class="button" value="Save">
				  <?php e($html->link('Cancel','javascript:void(0);',array('class'=>'reset','style'=>""))); ?>
 
                </div>
        </div>
	 <?php e($form->end()); ?>
      </div>
    </div>
  </div>
  <script type="text/javascript">
	jQuery(document).ready(function(){
	jQuery('#OldNotMatch').css('display','none');
	<?php if($error!=''):?>
		jQuery('#UserOldpass').css('border-color','#F00');
		jQuery('#OldNotMatch').css('display','block');
	<?php endif; ?>
		/*jQuery(".delete").click(function(){
			   jConfirm('Are you sure you want to delete your MentorsGuild account?','Mentorsguild Confirm', function(r) {
					  if(r){  
						document.location.href= SiteUrl+'/members/delete';
					  }
					  return false;
				  }
				);
		});*/
		//validation cancel
		jQuery(".reset").click(function(){
			document.location.href= SiteUrl;
		});
		//form validation
		/*jQuery("#submitAccount").click(function(){		
			var flag = 0;			
			//check password validation
			if(jQuery.trim(jQuery('#UserPassword2').val()) != '' || jQuery.trim(jQuery('#UserRepeat').val()) != '') {
				
				if(jQuery.trim(jQuery('#UserPassword2').val()) != jQuery.trim(jQuery('#UserRepeat').val())){
					jQuery('#passwordError').show();	
					jQuery('#UserPassword2').css('border-color','#F00');
					jQuery('#passwordError').css('color','#F00');	
					jQuery('#passwordError').html('No match new password and reset password');
					jQuery('#UserRepeat').css('border-color','#F00');						
					flag++;					
				}else{
					jQuery('#passwordError').hide();	
					jQuery('#UserPassword2').css('border-color','');	
					jQuery('#UserRepeat').css('border-color','');				
				}				

			} else{
				jQuery('#passwordError').hide();	
				jQuery('#UserPassword2').css('border-color','');	
				jQuery('#UserRepeat').css('border-color','');				
			}			
			if(flag == 0){
				jQuery("#accountSettingForm").submit();			
			}
			return false;
		

		
		});*/

		
	});
  </script>
  
  