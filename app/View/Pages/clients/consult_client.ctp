<?php
$error = '';
if(isset($_SESSION['pass_error']))
{
	$error  = $_SESSION['pass_error'];
	unset($_SESSION['pass_error']);
}?>
<?php 
e($html->css(array('jquery/jquery.alerts')));
e($javascript->link(array('jquery/jquery.min','jquery/jquery.alerts')));
?>

<div id="" class="tmask" style="height: 1906px; width: 1349px; opacity: 0.7; display: block;"></div>

<div class="tbox" style="position: fixed; top: 50px; left: 225.5px; opacity: 1; display: block;">
	<div id="" class="tinner" style="height:380px; width: 740px; background-image: none; padding-left:40px; padding-right:40px;">
		<div id="inner-content-wrapper">
		  <div id="inner-content-box"> 
				<div class="logotextdot">
					<p>
						<b>Mentors Guild</b><span>.</span>
						<strong>is building a trusted community for facilitating speedier resolution of today's most complex business challenges.</strong>
						<br><br>
						<strong>Our membership is comprised of two interactive cimmunities: Consultants and Clients.</strong>
					</p>
					<div class="consultantbox">
						<span>Consultants</span>
						<p>
							Our consultants are seasoned experts from various fields. Consultants work with our clients over various media or in person.
							<br><br>
							All engagements are at the consultant's discretion.
						</p>
						<input class="btn" onclick="" type="submit" value="Apply Now">
					</div>
					<div class="clientbox">
						<span>Clients</span>
						<p>
							Our clients are executive leaders from various organizations.
							<br><br>
							They join us to solve their most critical operational and developmental challenges.
						</p>
						<input class="btn" onclick="" type="submit" value="Get Started">
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<div class="tclose"></div>
		</div>
	</div>
</div>

<script src="http://code.jquery.com/jquery-1.7.2.js"></script>
<script>
$(document).ready(function(){
  $(function(){
  var from=['Business Strategy','Workplace Conflict','Mergers &amp; Acquisition','Employee Engagement','Executive Coaching'], i=1; // i for counting
      setInterval(function(){
          $('#from').fadeOut(function(){ //fadeout text
          $(this).html(from[i=(i+1)%from.length]).fadeIn(); //update, count and fadeIn
          });
      }, 3500 ); //2s
  
  var to=['Succession Planning','Customer Service','Change Management','Lean Six Sigma','New Product Launch'], j=1; // j for counting
      setInterval(function(){
          $('#to').fadeOut(function(){ //fadeout text
          $(this).html(to[j=(j+1)%to.length]).fadeIn(); //update, count and fadeIn
          });
      }, 3500 ); //2s

  });
});
</script>