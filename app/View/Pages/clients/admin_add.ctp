<?php e($javascript->link(array('autocomplete/jquery.coolautosuggest')));?>
<?php e($html->css(array('autocomplete/jquery.coolautosuggest')));?>
<div class="adminrightinner">
	<?php e($form->create(Null, array('url' => array('controller' => 'clients', 'action' => 'add'),'type'=>'file')));?>     
	<div class="tablewapper2 AdminForm">
		<h3 class="legend1">Add Client</h3>
		<?php e($this->element('menteesuser/form'));?>
	</div>
	<div class="buttonwapper">
		<div><input type="submit" value="Submit" class="submit_button" /></div>
		<div class="cancel_button"><?php echo $html->link("Cancel", "/admin/clients/index/", array("title"=>"", "escape"=>false)); ?>
		</div>
	</div>
	<?php e($form->end()); ?>	
</div>

