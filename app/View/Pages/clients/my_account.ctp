<style>
.expertise p {
    text-align: left !important;
}
.mentor_details
{
    float:none !important;
}
</style>
<script>
var clickTrack = 0;
function doSomethingMouseOver(){
	if(clickTrack == 0){
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
		vis.visibility='visible';
		vis.position='relative';
	}
}

function doSomethingMouseOut(){
	if(clickTrack == 0){
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
		vis.visibility='hidden';
		vis.position='absolute';
	}
}

function doSomething()
{
		
		//alert(clickTrack);	
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
	if ( (vis.visibility=='visible' || vis.visibility=='hidden') && clickTrack == 0)
	{
		clickTrack = 1;
		vis.visibility='visible';
		vis.position='relative';
	}
	else if (vis.visibility=='visible' && clickTrack == 1)
	{
		clickTrack = 0;
		vis.visibility='hidden';
		vis.position='absolute';
		
	}
}
</script>
<div  align="center">
	<b>
		<?php 
			if(isset($ERROR) && $ERROR != ''){
				e($ERROR); 
			}
		?>
	</b> 
</div>
<div id="inner-content-wrapper">
	<div id="inner-content-box" class="pagewidth">
	<div class="Profile_page_container"><!-- VL 22/12-->
		<div class="figure-left align-left-botttom">
        <div class="profileimg">
			<?php 
			if(isset($this->data['UserImage'][0]['image_name']) && $this->data['UserImage'][0]['image_name'] !=''){
				if($this->data['User']['role_id']==Configure::read('App.Role.Mentor')){
					$image_path = MENTORS_IMAGE_PATH.DS.$this->data['User']['id'].DS.$this->data['UserImage'][0]['image_name'];
					e($html->image($image_path,array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'], 'height'=>'200','width'=>'200' ,'class'=>'desaturate')));	
				}else{
					$image_path = MENTEES_IMAGE_PATH.DS.$this->data['User']['id'].DS.$this->data['UserImage'][0]['image_name'];
					e($html->image($image_path,array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'] ,'height'=>'200', 'width'=>'200' ,'class'=>'desaturate')));	
					
				}
				
			}else{
				e($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])));
			}?>
			</div>		
			<div class="figcaption" style="bottom:-5px;width:455px;">
			
				<h1><span><?php e($this->data['UserReference']['first_name'].' '.$this->data['UserReference']['last_name']); ?></span></h1>
				<?php e($this->data['UserReference']['headline']); ?>
				<p><?php
				if($city != '') {
					echo e($city) ;?>, <?php echo  e($state) ;
				} else {
					echo e($this->data['UserReference']['zipcode']);
				}?><br />
				<?php
				//$disp="www.mentorsguild.com/".strtolower($this->data['User']['url_key']);
				//$displink=SITE_URL."mentees/my_account/".strtolower($this->data['User']['url_key']);
				?>
				<?php //e($html->link($disp,$displink,array('escape'=>false , 'target'=>'_blank'))); ?> </p>
				<div class="socials">
				<?php
					$socialCount=0;
					while(count($this->data['Social'])>$socialCount)
					{
					
					if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'http'))!==false)
						{
							$link=$this->data['Social'][$socialCount]['social_name'];
						}
						else
						{
							$link="https://".$this->data['Social'][$socialCount]['social_name'];
						}
						if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'linkedin'))!==false)
						{
						 e($html->link($html->image('media/linkedin.png',array('alt'=>'twitter' , 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						}
						else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'twitter'))!==false)
						{
							 e($html->link($html->image('media/twitter.png',array('alt'=>'twitter' , 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						}
						else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'facebook'))!==false)
						{
							 e($html->link($html->image('media/facebook.png',array('alt'=>'twitter' , 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						}
						else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'google'))!==false)
						{
							 e($html->link($html->image('media/Google+.png',array('alt'=>'twitter' , 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						}
						else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'blogspot'))!==false)
						{
							 e($html->link($html->image('media/blogger.png',array('alt'=>'blogger' , 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						}
						else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'wordpress'))!==false)
						{
							 e($html->link($html->image('media/wordpress.png',array('alt'=>'wordpress' , 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						}
						else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'youtube'))!==false)
						{
							 e($html->link($html->image('media/youtube.png',array('alt'=>'youtube' , 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						}
						else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'yahoo'))!==false)
						{
							 e($html->link($html->image('media/yahoo.png',array('alt'=>'yahoo' , 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						}
						else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'delicious'))!==false)
						{
							 e($html->link($html->image('media/delicious.png',array('alt'=>'delicious' , 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						}
						else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'digg'))!==false)
						{
							 e($html->link($html->image('media/digg.png',array('alt'=>'digg' , 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						}
						else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'orkut'))!==false)
						{
							 e($html->link($html->image('media/orkut.png',array('alt'=>'orkut' , 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						}


						else 
						{
							 e($html->link($html->image('media/share-icon-1.png',array('alt'=>'twitter' , 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						}
						
						$socialCount++;
						}
				?>
				
				</div> 
			</div>
			<div id="like-btn">
				<?php // e($html->link($html->image('media/Untitled1.png',array('alt'=>'google-plus')),'#',array('escape'=>false)));?>
				<?php // e($html->link($html->image('media/Untitled2.png',array('alt'=>'linkedin')),'#',array('escape'=>false)));?>
				<?php // e($html->link($html->image('media/Untitled3.png',array('alt'=>'facebook')),'#',array('escape'=>false)));?>
				<?php // e($html->link($html->image('media/Untitled4.png',array('alt'=>'twitter')),'#',array('escape'=>false)));?>
			</div>
		</div>
		<div class="mentor_details">
		<?php if($this->Session->read('Auth.User.role_id') == Configure::read('App.Role.Mentor') || $this->Session->read('Auth.User.id')=='' || $this->Session->read('Auth.User.role_id')=='1') {
			
			$mentorshipData = $this->General->getMentorshipSessionsWithClient($this->data['User']['id']);
			
			if (empty($mentorshipData) == false && $mentorshipData['Mentorship']['applicationType'] != 1) {
		?>
			<div id="mentor-detail-left1">
					<div id="area-box">
						<?php
							if($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==Configure::read('App.Role.Mentee'))
							{
						?>
						<div class="invoce-creation" style="padding-top:15px;">			
								<?php e($this->element('menteesuser/mentee_dash')); ?>
							</div>
						<?php
							}
						?>
						<div class="expertise" style="padding-top:15px;">
							<h1>Professional summary</h1>
							<p><?php e(nl2br(strip_tags($this->data['UserReference']['background_summary']))); ?></p>
						</div>
					</div>
			</div>
			<div id="mentor-detail-right" style="margin-bottom: 20px; display: block;">
				<h1></h1>
				<div class="apply-button-search" style="float: left; margin-bottom: 5px;">
                      <input class="profilebtn" type="button" style="width:211px !important; margin-top: 10px;" value="Send Invoice" onclick="window.location.href='<?php echo SITE_URL."invoice/invoice_create/".$mentorshipData['Mentorship']['id']."/".$this->data['User']['id']; ?>'; ">
                </div> 
			</div>
		<?php } else {?>
		<div id="mentor-detail-left1">
				<div id="area-box">
					<?php
						if($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==Configure::read('App.Role.Mentee'))
						{
					?>
					<div class="invoce-creation" style="padding-top:15px;">			
							<?php e($this->element('menteesuser/mentee_dash')); ?>
						</div>
					<?php
						}
					?>
					<div class="expertise" style="padding-top:15px;">
						<h1>Professional summary</h1>
						<p><?php e(nl2br(strip_tags($this->data['UserReference']['background_summary']))); ?></p>
					</div>
				</div>
		</div>
		<?php }
		}?>
		<?php if($this->Session->read('Auth.User.role_id') == Configure::read('App.Role.Mentee')) {
			
			$unpaidInvoiceCount = $this->General->getUnpaidInvoices();
				
			if ($unpaidInvoiceCount > 0) {
		?>
			<div id="mentor-detail-left1">
					<div id="area-box">
						<?php
							if($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==Configure::read('App.Role.Mentee'))
							{
						?>
						<div class="invoce-creation" style="padding-top:15px;">			
								<?php e($this->element('menteesuser/mentee_dash')); ?>
							</div>
						<?php
							}
						?>
						<div class="expertise" style="padding-top:15px;">
							<h1>Professional summary</h1>
							<p><?php e(nl2br(strip_tags($this->data['UserReference']['background_summary']))); ?></p>
						</div>
					</div>
			</div>
			<div id="mentor-detail-right" style="margin-bottom: 20px; display: block;">
				<h1></h1>
				<div class="apply-button-search" style="float: left; margin-bottom: 5px;">
                      <input class="profilebtn" type="button" style="width:211px !important; margin-top: 10px;" value="My invoices" onclick="window.location.href='<?php echo SITE_URL."invoice/invoicelistclient" ?>'; ">
                </div> 
               
                <?php if($numberOfProjects == '1') {?>
	                <div style="margin-top: 10px;">
	                	<img src="<?php e(SITE_URL);?>img/media/arrow.png" style="width: 14px; height: 14px;"></img>
	                	<a class="delete" href="<?php e(SITE_URL);?>project/projectlistclient">My projects</a>
	                </div>
                <?php }?>
			</div>
		<?php } else {?>
			<div id="mentor-detail-left1">
				<div id="area-box">
					<?php $showBlock = 0;
						if($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==Configure::read('App.Role.Mentee'))
						{ $showBlock = 1;
					?>
					<div class="invoce-creation" style="padding-top:15px;">			
							<?php e($this->element('menteesuser/mentee_dash')); ?>
						</div>
					<?php
						}
					?>
					<div class="expertise" style="padding-top:15px;">
					<h1>Professional summary</h1>
					<p><?php e(nl2br(strip_tags($this->data['UserReference']['background_summary']))); ?></p>
				</div>
				</div>
			</div>
			<?php if($this->Session->read('Auth.User.id') == $this->data['User']['id']) {?>
				<?php if($showBlock > 0) {?>
				<div id="mentor-detail-right" style="margin-bottom: 20px; display: block;">
					<h1></h1>
	                <?php if($numberOfInvoices == '1') {?>
		                <?php if($showBlock == 2){?>
		                	<div style="margin-top: 30px;">
		                <?php } else if($showBlock == 1){?>
		                	<div style="margin-top: 65px;">
		                <?php }?>
			                	<img src="<?php e(SITE_URL);?>img/media/arrow.png" style="width: 14px; height: 14px;"></img>
			                	<a class="delete" href="<?php e(SITE_URL);?>invoice/invoicelistclient">My invoices</a>
		                	</div>
	                <?php }?>
	                <?php if($numberOfProjects == '1') {?>
	                	<?php if($numberOfInvoices == '1') {?>
		                	<div style="margin-top: 10px;">
		                <?php } else {?>
		                	 <?php if($showBlock == 2){?>
		                	<div style="margin-top: 30px;">
		                <?php } else if($showBlock == 1){?>
		                	<div style="margin-top: 65px;">
		                <?php }?>
		                <?php }?>
		                	<img src="<?php e(SITE_URL);?>img/media/arrow.png" style="width: 14px; height: 14px;"></img>
		                	<a class="delete" href="<?php e(SITE_URL);?>project/projectlistclient">My projects</a>
		                </div>
	                <?php }?>
				</div>
				<?php }?>
			<?php }?>
		<?php }
		}?>
		</div><!-- VL 22/12-->	
</div>
</div>
</div><!-- VL 22/12-->
<script type="text/javascript" src="//s3.amazonaws.com/scripts.hellobar.com/f876b1b3d2e2502ac0853ca4e83d0aa530c8f3b3.js"></script>