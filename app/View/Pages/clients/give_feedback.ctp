<?php e($html->css(array('regist_popup'))); ?>
<div id="registerationFm">
	<h1>Feedback</h1>
	<?php 
		e($form->create('Client',array('url'=>array('controller'=>'clients','action'=>'give_feedback'),'id'=>'feedbackForm'))); 
		echo $form->hidden('Feedback.id',array('value'=>$data['Feedback']['id']));
		echo $form->hidden('Feedback.client_id',array('value'=>$client_id));
		echo $form->hidden('Feedback.member_id',array('value'=>$member_id));
	?>	
	<div class="feedbackcontent">
		<div class="form_row">
			<p>Would you recommend this member to others?</p>
			<div class="styled-form feedback-form">
				 <?php 
				 $y='';$n='';$m='';
				 if(isset($data['Feedback']['recommended']) && $data['Feedback']['recommended']==0): 
				 			$n='checked="checked"';  
				 elseif(isset($data['Feedback']['recommended']) && $data['Feedback']['recommended']==2):
					     $y='checked="checked"'; 
					 elseif(isset($data['Feedback']['recommended']) && $data['Feedback']['recommended']==1):
					     $m='checked="checked"';  
					 else:
						 $y='checked="checked"';
					 endif; ?>
				<input type="radio" <?php echo $y;?> value="2" id="feedback_y" name="data[Feedback][recommended]"><label for="feedback_y" style="font-family:'Ubuntu';">Yes</label>
				<input type="radio" <?php echo $n;?> value="0" id="feedback_n" name="data[Feedback][recommended]"><label for="feedback_n" style="font-family:'Ubuntu';">No</label>  
				<input type="radio" <?php echo $m;?> value="1" id="feedback_mb" name="data[Feedback][recommended]"><label for="feedback_mb" style="font-family:'Ubuntu';">Maybe</label>
				<div class="clear"> </div>
			</div>
		</div>
		<div class="form_row">
			<p>How has the member helped you?</p>
			<textarea  style="height:161px;" id="feedbackText"  maxlength="1200" div="1" rows="9" name="data[Feedback][description]"><?php if(isset($data['Feedback']['description'])): echo $data['Feedback']['description']; endif;?></textarea>           
		</div>
	</div>
	<div class="submit_bar">
		<div class="submit_bar_right" style="float:right;">
			<?php e($form->submit('Submit',array('class'=>'btn','onclick'=>'return validfeedback();'))); ?> 
		</div>
	</div>
	<?php 
		e($form->end()); 
	?>	
</div>
</div>
</div>