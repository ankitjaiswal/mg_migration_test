<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
    	<div id="content-submit">
        <div class="mentor_details"><!-- VL 22/12-->
			<div id="mentor-detail-left1" style="width: 100%;">
				<form action="<?php echo SITE_URL ?>insight/preview" method="post" id="AddformIn">
				<div id="area-box" style="width: 100%;">
					<?php if(isset($Insight_id)){?>
	                	<input type="hidden" name="data[Insight][id]" value="<?php e($Insight_id);?>"/>
	                <?php }?>
					<div class="expertise" style="padding-top:15px;">
						<h1>Share your insight<sub style="font-size: 11px; font-weight: normal; font-family: Ubuntu; margin-left: 5px;">Advice for business leaders</sub></h1>
						<br/>
							
						<span>Title</span> <span style="float:right;">(<span id="noOfCharIn"><?php e(128 - strlen($Insight_title))?></span>  characters remaining)</span>
                        <div class="inputRELATIVE">
                        	<p>
                        		<input id="titleTextBox" name="data[Insight][title]" type="text" style="width:651px;" placeholder="Be creative and clear"  value="<?php e(htmlspecialchars($Insight_title))?>" />
                        	</p>
                        </div>
                    </div>
                    <div class="expertise">
						<span>Your insight</span> <span style="float:right;"id="noSpan" >(Minimum <span id="noOfCharInsight"><?php e(1000 - strlen($Insight_insight))?></span> more characters)</span>
						<div class="inputRELATIVE">
	                        <p>
	                        	<textarea id="InsightBox" class="forminput" name="data[Insight][insight]" style="width:651px; padding:10px; height:150px;" placeholder="Short words, short sentences, short paragraphs" ><?php e(htmlspecialchars($Insight_insight))?></textarea> 
	                        </p>
	                        <div class="showHINT" style="height: 230px; width: 250px; right: -290px;">
                        		<span></span><!--for left arrow show on hover-->
                        		<p style="text-align: left;">
                        			1. Be specific. <br/><br/>
									2. Respect reader's time and intelligence. <br/><br/>
									3. Draw on your past experience.<br/><br/>
									4. Your post needs to stand on its own feet.<br/>
										(You may use 1 external link for attribution)
                        		</p>
                    		</div>
                    	</div> 
                    </div>

                    <div id="Apply" style="float: right;" role="Apply">
						<a class="reset" style="float: left; margin-top: 10px; margin-right: 30px;" href="<?php e(SITE_URL)?>qanda">Cancel</a>
						<input id="applyPreviewIn" class="mgButton" type="submit" value="Preview" disabled="disabled">
					</div>
					<br/><br/>
				</div>
				</form>
    		</div>
    	</div>	
    	</div>
    </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){

	var qText = jQuery("#InsightBox").val();
	if(qText.length > 1000) {
		 jQuery("#noSpan").hide();
         document.getElementById('applyPreviewIn').disabled = false;
	}
	
	 function updateCountIn()
	    {
	        var qText = jQuery("#titleTextBox").val();

	       if(qText.length < 128) {
	           jQuery("#noOfCharIn").html(128 - qText.length);
	       } else {
	           jQuery("#noOfCharIn").html(0);
	           jQuery("#titleTextBox").val(qText.substring(0,128));
	       }
	    }

	    jQuery("#titleTextBox").keyup(function () {
	        updateCountIn();
	    });
	    jQuery("#titleTextBox").keypress(function () {
	        updateCountIn();
	    });
	    jQuery("#titleTextBox").keydown(function () {
	        updateCountIn();
	    });
	    

	    function updateCountInsight()
	    {
	        var qText = jQuery("#InsightBox").val();

	       if(qText.length < 1000) {
	    	   jQuery("#noOfCharInsight").html(1000 - qText.length);
	    	   jQuery("#noSpan").show();
	    	   document.getElementById('applyPreviewIn').disabled = true;
	       } else {
	           jQuery("#noSpan").hide();
	           document.getElementById('applyPreviewIn').disabled = false;
	       }
	    }

	    jQuery("#InsightBox").keyup(function () {
	    	updateCountInsight();
	    });
	    jQuery("#InsightBox").keypress(function () {
	    	updateCountInsight();
	    });
	    jQuery("#InsightBox").keydown(function () {
	    	updateCountInsight();
	    });

	    
	    jQuery("#applyPreviewIn").click(function(){

	    	var flag = 0;
	    	if(jQuery.trim(jQuery("#titleTextBox").val()) == '') {
				jQuery('#titleTextBox').css('border-color','#F00');
				jQuery('#titleTextBox').focus();
				flag++;
			} else {
				jQuery('#titleTextBox').css('border-color',''); 
			}

	    	var context = jQuery.trim(jQuery("#InsightBox").val());
	    	var words = countWords('InsightBox');
	    	if(context == '' /*|| words < 50*/) {
				jQuery('#InsightBox').css('border-color','#F00');
				jQuery('#InsightBox').focus();
				flag++;
			} else {
				jQuery('#InsightBox').css('border-color','');
			}

			if(flag > 0) {
				return false;
			} else {
				jQuery("#AddformIn").submit();
				return true;
			}
			
	    });
});
</script>

	
