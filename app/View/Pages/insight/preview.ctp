<form action="<?php echo SITE_URL ?>insight/add_insight" method="post" id="Addform">
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div id="content-submit">
        	<div class="mentor_details" style="width: 960px;"><!-- VL 22/12-->
        		<input type="hidden" name="data[Insight][title]" value="<?php e($Insight_title);?>"/>
                <input type="hidden" name="data[Insight][insight]" value="<?php e($Insight_insight);?>"/>
                <input type="hidden" name="data[Insight][id]" id="qId" value="<?php e($Insight_id);?>"/>
				<div id="mentor-detail-left1" style="min-width: 600px;">
					<div id="area-box">
						<div class="expertise" style="padding-top:15px;">
							<h1>Share your insight<sub style="font-size: 11px; font-weight: normal; font-family: Ubuntu; margin-left: 5px;">Advice for business leaders</sub></h1>
							<br/>
							<h5><?php e($Insight_title) ?></h5>
							<p style="text-align: left;">
								<?php echo nl2br($Insight_insight);?>
								<br/><br/>
								<a href="<?php e(SITE_URL.'insight/edit/')?>" style="float: right;">Edit</a>
							</p>
						</div>
					</div>
    			</div>

    			<div id="mentor-detail-right" style="margin-bottom: 20px;"><!-- VL 22/12 start-->
					<div class="rates labelSET">
						<h1>Choose Category</h1>
						<br/>
						<h5>Functions</h5>
						<?php $countC = 0; foreach($fcategories as $fvalue) {?>
						<label><input class="group1" type="checkbox" name="catCheck[]" value="<?php e($fvalue['Qna_category']['id']);?>"></input><?php e($fvalue['Qna_category']['category']);?></label><br/>
						<?php }?>
						<br/><br/>
						<h5>Domains</h5>
						<?php foreach($dcategories as $dvalue) {?>
						<label><input class="group1" type="checkbox" name="catCheck[]" value="<?php e($dvalue['Qna_category']['id']);?>"></input><?php e($dvalue['Qna_category']['category']);?></label><br/>
						<?php $countC++;}?>
					</div>
				</div>

				<div class="clear"></div>
				<div class="HRLine"></div>
				<div id="Apply" style="float: right; margin-bottom:80px; margin-right: 50px;" >
					<input id="apply" class="submitProfile LTSpreview" type="submit" value="Submit">
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>
</form>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".submitProfile").click(function(){

    	var flag = 0;
    	if(jQuery.trim(jQuery("#titleTextBox").val()) == '') {
			jQuery('#titleTextBox').css('border-color','#F00');
			jQuery('#titleTextBox').focus();
			flag++;
		} else {
			jQuery('#titleTextBox').css('border-color',''); 
		}

    	var context = jQuery.trim(jQuery("#InsightBox").val());
    	var words = countWords('InsightBox');
    	if(context == '' /*|| words < 50*/) {
			jQuery('#InsightBox').css('border-color','#F00');
			jQuery('#InsightBox').focus();
			flag++;
		} else {
			jQuery('#InsightBox').css('border-color','');
		}

		if(flag > 0) {
			return false;
		} else {
			jQuery("#Addform").submit();
			return true;
		}
    });
});
</script>