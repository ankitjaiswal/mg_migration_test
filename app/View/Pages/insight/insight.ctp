<?php /*
 $completelink = SITE_URL.'insight/insight/'.$insight['Insight']['url_key']; 
 $compTitle = $insight['Insight']['title'];*/?>

<?php $completelink = SITE_URL."insight/insight/".($insight['Insight']['url_key']);?>
<?php $tinyUrl = $this->General->get_tiny_url(urlencode($completelink));?>
<?php $atMentorsGuild = "@MentorsGuild: "?>
<?php $MentorsGuild = "Mentors Guild: "?>
<?php $compTitle = $insight['Insight']['title'];?>
<?php $linkTitle = $insight['Insight']['title'];?>

<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div id="content-submit">
       		<h1> <?php e($insight['Insight']['title']); ?></h1>
				<div id="text-content">
					<div class="QnAbutton" style="margin-top: 10px;">
						<?php 
			    		foreach ($insight['Insight_category'] as $catValue) {

                                                           $str = ($categories[$catValue['category_id']]);
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"&"))
                                                            $str = str_replace("&","_and",($categories[$catValue['category_id']]));
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"/"))
                                                            $str = str_replace("/","_slash",($categories[$catValue['category_id']]));
                                                            $str = str_replace(" ","-",$str);

            		             e($html->link($categories[$catValue['category_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false)));
			    		}?>
						<br/><br/>
					</div>
					
				<br/><br/>
				</div>
				
			<div id="mentors-content" style="padding-bottom: 0px;">
				<table id="results" style="width:100% !important;">
					<tr class="set_0?>">
						<td>
							<div class="box1">
						 		<div class="content-left-img QnAconsult" style="width: 130px;"> 
						 			<?php if(isset($userData['UserImage'][0]['image_name']) && $userData['UserImage'][0]['image_name'] !=''){
					            			$image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$userData['User']['id'].DS.$userData['UserImage'][0]['image_name'];
					            		} else {
					            			$image_path = SITE_URL.'img/media/profile.png';
					            		}?>
						  				<a href="<?php e(SITE_URL.strtolower($userData['User']['url_key']));?>" style="border: 0px;">
						  					<img class="desaturate" src="<?php e($image_path);?>" alt="<?php e($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?>" style="width:100px; height:100px;" />
						  				</a><br/>
						  					<input class="mgButton" type="button" value="Contact" style="margin-top: 5px; width: 100px; font-weight: normal" onclick="window.location.href='<?php e(SITE_URL."fronts/consultation_request/".$userData['User']['url_key']);?>'">
						  			</div>
					
								<div class="content-right-text" style="width: 824px;">
                     				<h1>
                     					<span>
											<a href="<?php e(SITE_URL.strtolower($userData['User']['url_key']));?>"><?php e($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?></a>
										</span> 

										<br/>
										<?php 
										$disp=SITE_URL.strtolower($userData['User']['url_key'])."#answers";
										if($userData['ans_count'] >1)
											e($html->link($userData['ans_count']." answers",$disp,array('escape'=>false, 'style'=>'font-size: 14px;')));
										else if($userData['ans_count'] == 1)
											e($html->link($userData['ans_count']." answer",$disp,array('escape'=>false, 'style'=>'font-size: 14px;')));
										else { ?>
											<span style="font-size: 14px;">&nbsp;</span>
										<?php }?>
                     					
                     					<div class="share_icons" onmouseout="doSomethingMouseOut();"  onmouseover="doSomethingMouseOver()" style="width: 220px; float: right; margin-top: -10px;">
											<div style="float:right;" >
                                                                             <input class="create_btn" type="button" style="width:77px !important;height:25px;background: none repeat scroll 0 0 #F32C33;color: #FFFFFF;" value="Share" onclick='javascript:doSomething();',onmouseover = 'doSomethingMouseOver('.$key.');'>
												<?php //e($html->link($html->image('images/share_icon.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'77')),'javascript:doSomething()',array('escape'=>false,'onmouseover' => 'doSomethingMouseOver()', 'style'=>'padding-right: 0px;')));?>
											</div>
											<?php if($userData['User']['id'] == $this->Session->read('Auth.User.id')) {?>
											<div id="shareicon" style="float:right; visibility:visible; width: 130px;">
											<?php }else {?>
						<div id="shareicon" style="float:right; visibility:hidden; width: 130px;">
						<?php }?>
							&nbsp; 
							<a href="javascript:window.open('https://plus.google.com/share?url=<?php e($completelink);?>&title=<?php e($atMentorsGuild. $userData['UserReference']['first_name']." ".$userData['UserReference']['last_name'] .$compTitle);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
							   <?php e($html->image('images/g_plus_icon.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'27'))); ?></a>
							

							<a title="Share this post/page"href="javascript:window.open('http://www.facebook.com/sharer.php?s=100&p[url]=<?php echo $completelink; ?>&p[title]=<?php e($atMentorsGuild. $userData['UserReference']['first_name']." ".$userData['UserReference']['last_name'] ." ".$compTitle);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
							      <?php e($html->image('images/fb_icon.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'27'))); ?></a>
							
                                                 <?php 
								 $linkdinUrl="http://www.linkedin.com/shareArticle?mini=true&url=".$completelink."&title=".$MentorsGuild.$userData['UserReference']['first_name']." ". $userData['UserReference']['last_name']." " .$linkTitle;
								 $tweet="http://twitter.com/intent/tweet?text=".$atMentorsGuild. $userData['UserReference']['first_name']." ". $userData['UserReference']['last_name'] ." ".$compTitle." " .$tinyUrl;
							?>	
                                               <a href="javascript:window.open('<?php echo ($linkdinUrl);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')"><?php e($html->image('images/logo_linkedin.png',array('alt'=>'LinkedIn' , 'height'=>'25', 'width'=>'27', 'id'=>'linkedin_Share'))); ?></a>
                                               <a href="javascript:window.open('<?php echo ($tweet);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')"><?php e($html->image('images/twit_icon.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'27', 'id'=>'twitter_Share'))); ?></a>

							 
						 </div>
						<!-- Share on social media end -->
									</div>
								</h1>
							<div class="text-content-box">
						  		<p> 
						  			<?php $answer = str_replace('  ', '&nbsp;&nbsp;', $insight['Insight']['insight']); 
									echo $this->General->make_links(nl2br($answer));?>
						  		</p>
							</div>
						</td>
					</tr>
				</table>
			</div>	
	    </div>
    </div>
</div>

<script type="text/javascript">

var clickTrack = 0;
									
function doSomethingMouseOver(){
	if(clickTrack == 0){
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
		vis.visibility='visible';
		vis.position='relative';
	}
}

function doSomethingMouseOut(){
	if(clickTrack == 0){
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
		vis.visibility='hidden';
		vis.position='absolute';
	}
}

function doSomething()
{
	var id_div=document.getElementById("shareicon");
	var vis = id_div.style;
	if ( (vis.visibility=='visible' || vis.visibility=='hidden') && clickTrack == 0)
	{
		clickTrack = 1;
		vis.visibility='visible';
		vis.position='relative';
	}
	else if (vis.visibility=='visible' && clickTrack == 1)
	{
		clickTrack = 0;
		vis.visibility='hidden';
		vis.position='absolute';
		
	}
}

function nl2br (str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br/>' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

</script>
