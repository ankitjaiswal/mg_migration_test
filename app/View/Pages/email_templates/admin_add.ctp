 <?php echo $this->Html->script('tinymce/jscripts/tiny_mce/tiny_mce');?>
<script type="text/javascript">
tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		skin : "o2k7",
		height:400,
		width :600
});
</script>
<div class="adminrightinner">
     <?php e($form->create('EmailTemplate', array('url' => array('controller' => 'email_templates', 'action' => 'add'))));?>    
	 <div class="tablewapper2 AdminForm">
	  <h3 class="legend1">Edit Page </h3>
		<table border="0" class="Admin2Table" width="100%">				   
			  <tr>
				 <td valign="middle" class="Padleft26">Title <span class="input_required">*</span></td>
				 <td><?php e($form->input('name', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
                 <?php e($form->input('id'));?>
			  </tr>
               <tr>
				 <td valign="middle" class="Padleft26">Subject <span class="input_required">*</span></td>
				 <td><?php e($form->input('subject', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
			  </tr>
               <tr>
				 <td valign="middle" class="Padleft26">Slug <span class="input_required">*</span></td>
				 <td><?php e($form->input('slug', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
			  </tr>
			   <tr>
				 <td valign="middle" class="Padleft26">Content <span class="input_required">*</span></td>
				 <td><?php e($form->textarea('description', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
			  </tr>
			   <tr>
				
			 		 
			 
		</table>
      </div>
      <div class="buttonwapper">
				<div><input type="submit" value="Submit" class="submit_button" /></div>
				<div class="cancel_button"><?php echo $html->link("Cancel", "/admin/email_templates/index/", array("title"=>"", "escape"=>false)); ?></div>
		</div>
		<?php
		   e($form->end());
		?>	
</div>


         