<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
     
        <div class="Profile_page_container">
        <div id="pro-creation">
                <h1>Mentorship log<span class="mentorname"><strong>Robert Werner (Mentor)</strong> - Michael Dang (Mentee)</span></h1>
            </div>
          <div class="logsearch">
<div class="formrow">
                        <p><b>Shared documents</b></p>
            <input type="text" name="data[UserReference][area_of_expertise]" size="30" maxlength="150" placeholder="Enter link to Google drive, Dropbox, etc" id="UserReferenceAreaOfExpertise">
          </div>
</div>
<div id="memberlog">
<div class="content">
<div class="row">
<div class="date">Fri, 1/28/2011</div>
<div class="description"><strong>I am open tomorrow at 6 p.m. for your orientation session. Please bring your calculator and be ready to take notes.</strong></div>
<div class="clear"></div>
</div>
<div class="row">
<div class="date">Sat, 1/29/2011</div>
<div class="description">
  <p>It was an amazing session. Thanks for participation . I will like you to buy the book Reading Financial Reports by  Lita Epstein (link). It will be good for your continued reference.</p>
  <p>It was great meeting today. Is there any homework before we meet Tuesday?</p>
</div>
<div class="clear"></div>
</div>
<div class="row">
<div class="date">Sun, 1/30/2011</div>
<div class="description"><strong>Michael Relating to the question that you asked me yesterday, I think it is enough if you read the Balance Sheet of your employer to figure out the Quick Ratio.</strong></div>
<div class="clear"></div>
</div>
<div class="row">
<div class="date">Mon, 1/31/2011</div>
<div class="description"><strong>Assignment 1: What are the key controls to lower your cost of capital? We can discuss tomorrow.</strong></div>
<div class="clear"></div>
</div>
<div class="row">
<div class="date">Mon, 1/31/2011</div>
<div class="description"><strong>Assignment 1: What are the key controls to lower your cost of capital? We can discuss tomorrow.</strong></div>
<div class="clear"></div>
</div>
<div class="row">
<div class="date">Mon, 1/31/2011</div>
<div class="description"><strong>Assignment 1: What are the key controls to lower your cost of capital? We can discuss tomorrow.</strong></div>
<div class="clear"></div>
</div>
<div class="row">
<div class="date">Mon, 1/31/2011</div>
<div class="description"><strong>Assignment 1: What are the key controls to lower your cost of capital? We can discuss tomorrow.</strong></div>
<div class="clear"></div>
</div>

</div>
<div class="row">
  <textarea rows="6" cols="30" placeholder="Enter your comment here" style="width:906px; padding:10px;" maxlength="2000" name="data[UserReference][background_summary]"></textarea>
<div class="clear"></div>
</div>
</div>
        
      
		  
		  
        </div>
      
    </div>
  </div>