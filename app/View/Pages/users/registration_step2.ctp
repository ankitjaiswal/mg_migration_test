<?php //e($javascript->link(array('autocomplete/jquery.coolautosuggest')));?>
<?php //e($html->css(array('autocomplete/jquery.coolautosuggest')));
//pr($this->Session->read('Auth')); die;
?>
<?php e($html->css(array('opentip'))); ?>
<?php e($javascript->link(array('opentip-jquery')));?>
<div id="inner-content-wrapper">
  <div id="inner-content-box" class="pagewidth">
    <div id="user">
      <div id="pro-creation">
        <h1>Profile Creation</h1>
      </div>
      <?php  e($form->create('User',array('url'=>array('controller'=>'users','action'=>'registration_step2'),'type'=>'file'))); ?>
      <?php  //e($form->hidden('User.username'));?>
      <?php  //e($form->hidden('User.password2'));?>
       <?php //e($form->hidden('UserImage.image_temp_name'));?>
       <?php //e($form->hidden('User.hash',array('value'=>'temp'.time()."_mentee"))); ?>
      <div id="name-box">
        <div id="profileImage" style="background: none repeat scroll 0 0 white;border: 1px solid #DDDDDD;float: left;margin-bottom: 20px;margin-right: 15px;overflow: hidden;text-align: center; width:200px;">
       <?php
          $imageName = $this->General->FindUserImage($this->Session->read('Auth.User.id'));
          if(isset($imageName) && $imageName!='')
          {
              $image_path = MENTEES_IMAGE_PATH.DS.$this->Session->read('Auth.User.id').DS.$imageName;
              e($html->image($image_path,array('alt'=>$this->Session->read('Auth.UserReference.first_name')." ".$this->Session->read('Auth.UserReference.last_name'),'style' => 'padding-bottom:0px;')));
              e($form->hidden($imageName,array('value'=>$imageName,'id'=>'image_name')));
          }
          else
          {
            e($html->image('media/profile.png',array('alt'=>'user-image','style' => 'padding-bottom:0px;'))); 
            e($form->hidden('profile.png',array('value'=>'profile.png','id'=>'image_name')));
        } ?>
          <div><?php e($this->element('menteesuser/step2_add_image')); ?></div>
        </div>
        <div id="input-names">
          <?php e($form->input('UserReference.first_name',array('maxlength'=>'20','div'=>false,'label'=>false,'class'=>'forminput','placeholder'=>'First Name','value'=>$this->Session->read('Auth.UserReference.first_name'))));?>
          <?php e($form->input('UserReference.last_name',array('maxlength'=>'20','div'=>false,'label'=>false,'class'=>'forminput','placeholder'=>'Last Name','value'=>$this->Session->read('Auth.UserReference.last_name'))));?>
          <?php e($form->input('UserReference.headline',array('div'=>false,'label'=>false,'class'=>'forminput','placeholder'=>'Professional Headline','value'=>$this->Session->read('Auth.UserReference.headline'))));?>
          <?php e($form->input('UserReference.zipcode',array('id'=>'register_zipcode','div'=>false,'label'=>false,'class'=>'forminput','placeholder'=>'ZIP (if US based) OR Country','value'=>$this->Session->read('Auth.UserReference.zipcode'))));?>
        </div>
        <?php e($form->input('UserReference.country',array('type'=>'hidden','div'=>false,'label'=>false,'class'=>'forminput1','placeholder'=>'Country','value'=>'US','readonly'=>true)));?>
       <div id="country">
      <?php /*     <?php e($form->input('UserReference.country',array('div'=>false,'label'=>false,'class'=>'forminput1','placeholder'=>'Country','value'=>'US','readonly'=>true)));?>
          <?php e($form->input('UserReference.zipcode',array('id'=>'zipcode','div'=>false,'maxlength'=>'5','label'=>false,'class'=>'forminput','placeholder'=>'ZIP (if US based) OR Country','value'=>$this->Session->read('Auth.UserReference.zipcode'))));?>
          <?php //e($html->image('loading.gif',array('id'=>'loading','style'=>'float:right')));?>
          <span id="ajax_response" style="margin-top:-20px"></span>  */?></div>
        <div id="upload-resume">
          <p> <label>Background summary</label></p>
          <?php e($form->textarea('UserReference.background_summary',array('div'=>false,'label'=>false,'id'=>'text-area','value'=>$this->Session->read('Auth.UserReference.background_summary'))));?>
        </div>
        <div id="links">
          <p>
            <label> Profile links</label>
          </p>
          <div id="p_social">
           <?php 
              $ss=0;
              $userLinks = $this->General->FindSocialLink($this->Session->read('Auth.User.id'));
              //pr($userLinks); die;
              if(!empty($userLinks) && count($userLinks)>0)
              {
                $cc = count($userLinks);
                foreach($userLinks as $social):
                    if(trim($social['Social']['social_name'])!=''):
                        echo "<p><label>";
                        e($form->input('Social.'.$ss.'.social_name',array('div'=>false,'label'=>false,'placeholder'=>'Twitter','value'=>$social['Social']['social_name'])));
                        if($ss==0):
                            echo "</label><span class='addmore' style='cursor:pointer' id='addSocial'>&nbsp[ + ]</span></p>";
                        else:
                            echo "</label><span class='remSocial addmore' style='cursor:pointer'>&nbsp[ - ]</span></p>";
                        endif;
                        $ss++;
                    endif;
                endforeach;
              } 
             else { 
             ?>
            <p style="margin-bottom: 10px;">
             <?php  e($form->input('Social.0.social_name',array('div'=>false,'label'=>false,'placeholder'=>'Twitter')));
                    echo '<span id="addSocial" style="cursor:pointer" class="addmore">[ + ]</span>';                 ?>
               </p>
        <?php } ?>
          </div>
          <div><span style="float: left;color:#888888; font-size: 12px;">e.g. http://www.domain.com</span></div>
        </div>
        <div id ="app-button">
          <?php e($form->submit('Submit',array('id'=>'submit','onclick'=>'return validateMenteeRegister();', 'style' =>'margin-right: 50px; margin-bottom: 20px;'))); ?>
        </div>
      </div>
      <?php e($form->end()); ?>
    </div>
  </div>
</div>
<script>
jQuery(document).ready(function(){
<?php
if(isset($popup) && $this->Session->read('Auth.User.id') ==''){?>
    register_popup();
<?php
}?>
    jQuery(function() {
            var socialDiv = jQuery('#p_social');
            var i = jQuery('#p_social p').size() + 1;
            
            jQuery('#addSocial').live('click', function() {
                if(i<=5)
                {
                    var title = '<label>';
                        title +='<input type="text" id="Social'+i+'SocialName" size="97" name="data[Social]['+i+'][social_name]" value="" placeholder="Twitter" />';
                        title +='</label>';
                    var remove = '<span class="remSocial addmore" style="cursor:pointer">&nbsp;[ - ]</span>';
                    jQuery('<p>'+title+remove+'</p>').appendTo(socialDiv);
                    i++;
                    return false;
                }
                else
                {
                    alert("You can enter up to 5 profile links");
                }
            });
                        
            jQuery('.remSocial').live('click', function() { 
                    if( i > 2 ) {
                            jQuery(this).parents('p').remove();
                            i--;
                    }
                    return false;
            });           
    });

});
</script>