<div  align="center">
	<b>
		<?php 
			if(isset($ERROR) && $ERROR != ''){
				e($ERROR); 
			}
		?>
	</b> 
</div>

<div class="profile-header">
    <div id="inner-content-wrapper">
	<div id="inner-content-box" class="pagewidth">
	    <div class="Profile_page_container">
		<div class="profile-pic">
		      <?php 
			if(isset($this->data['UserImage'][0]['image_name']) && $this->data['UserImage'][0]['image_name'] !=''){
				if($this->data['User']['role_id']==Configure::read('App.Role.Mentor')){
					$image_path = MENTORS_IMAGE_PATH.DS.$this->data['User']['id'].DS.$this->data['UserImage'][0]['image_name'];
					e($html->image($image_path,array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'], 'height'=>'200','width'=>'200' ,'class'=>'desaturate')));	
				}else{
					$image_path = MENTEES_IMAGE_PATH.DS.$this->data['User']['id'].DS.$this->data['UserImage'][0]['image_name'];
					e($html->image($image_path,array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'] ,'height'=>'200', 'width'=>'200' ,'class'=>'desaturate')));	
					
					
				}
				
			}else{
				e($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])));
			}?>
		</div>
 
             
		<div class="profile-bio" itemscope itemtype="http://schema.org/Person">
		    <div>
		    <div class="profile-name"><h1 style="color:#dedede;"><span class="fn"><span itemprop="name"><span itemprop="givenName"><?php e(ucfirst($this->data['UserReference']['first_name']));?></span> <span itemprop="familyName"><?php e($this->data['UserReference']['last_name']); ?></span></span></span></h1>
                    <div class="profile-guarantee"><?php if($this->data['UserReference']['guarantee'] == 'Y') {
				e($html->link($html->image('media/gurantee_shield28.png',array('alt'=>'Client Satisfaction Guaranteed','title'=>'Client Satisfaction Guaranteed','style'=>'width:20px; height:20px;')),SITE_URL.'fronts/service_guarantee',array('escape'=>false)));} ?>
					
                         </div>
                   </div>


		    <div class="profile-city" itemscope itemtype="http://schema.org/PostalAddress">
                     <span itemprop="addressRegion">
                            <span class="adr"><span class="locality"><?php if($city != '') {
                                         $citylink = str_replace(" ","-",$city);
                                         $keyword = "browse-all";?>
					<a href="<?php e(SITE_URL.'browse/search_result/'.$keyword.'/'.$state.'/'.$citylink);?>" style="color:white; "><?php echo e($city) ;?>, <?php echo  e($state) ;?></a>
				<?php } else {
					echo e($this->data['UserReference']['zipcode']);
				}?></span></span></span>
                    </div>

                   <?php
				$disp="mentorsguild.com/".strtolower($this->data['User']['url_key']);
				$completelink=SITE_URL.strtolower($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($this->data['User']['url_key']);
				?> 

                  <div class="profile-url"><span class="url"><span id="profileLinkTab" itemprop="url"><a href = "#profile" onclick="tog1();" style="color:white;"><?php e($disp); ?></a></span></span></div>


		    <div class="profile-headline" style="color:#dedede;"><?php e(ucfirst($this->data['UserReference']['linkedin_headline']));?></div>
		    
                    
		    </div>

		    	 <?php 
                    $data='';
                    if($this->Session->read('Auth.User.role_id')=='3' ||$this->Session->read('Auth.User.role_id')==''):
                        $data = $this->General->checkMentorApply($this->data['User']['id']);
                    
                    if($this->data['UserReference']['accept_application'] == 'Y')
                    {
                        if(empty($data)):
                     ?>                      
                          <div class="apply-button-search"style="float: left;" >
		    <input class="profilebtn" type="button"style="margin-right: 20px;margin-top:34px;font-weight:normal;margin-bottom:0px;"
	           id="onclick" value="Request a Free Consultation" type="button" onclick="window.location.href='<?php e(SITE_URL."fronts/consultation_request/".$this->data['User']['url_key']);?>'">
                   </div>
                     <div class="profile-connect-link" style="margin-top:57px;" ><a href="#footer-logo">See how you're connected</a>
                  </div> 
                   <?php   else: ?>
                        <div class="apply-button-search" style="float:left; height: 30px;">
                            <span style="color:#FFF;">Already applied</span>
                        </div>      
                <?php   endif;
                    }else{
                            $tempData = $this->General->checkMentorRequest($this->data['User']['id']);
                            if(empty($tempData)):
                ?>  
                            <div class="apply-button-search" id="reqbutton" style="float: left; ">
                                <input class="profilebtn" type="button"style="margin-right: 20px;margin-top:26px;font-weight:normal;margin-bottom:0px;"
	           id="onclick" value="Request" onclick="javascript:apply('<?php echo SITE_URL."/fronts/request_for_mentership/"; ?>','<?php e($this->data['User']['id']); ?>','request');" title="Be notified when Mentor becomes available."/>
                            </div>  
                <?php       else: ?>
                            <div class="apply-button-search" style="float:left; height: 30px;">
                                <span>Already requested</span>
                            </div>
                <?php       endif;
                        }
                endif;
                ?>
                    <?php
				if($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==Configure::read('App.Role.Mentor'))
				{?>
                     <div class="apply-button-search"style="float: left;" >
		    <input class="profilebtn" type="button"style="margin-right: 20px;margin-top:26px;font-weight:normal;margin-bottom:0px;"
	           id="onclick" value="Edit Profile" type="button" onclick="window.location.href='<?php e(SITE_URL."users/edit_account");?>'">
                   </div>
                  <?php 	}
			?>	
			<?php
				if($this->Session->read('Auth.User.id') != $this->data['User']['id'] && $this->Session->read('Auth.User.role_id')==Configure::read('App.Role.Mentor'))
				{?>
                       <div class="apply-button-search"style="float: left;" >
		    <input class="profilebtn" type="button"style="margin-right: 20px;margin-top:26px;font-weight:normal;margin-bottom:0px;"
	           id="onclick"  type="button" value="Endorse <?php e(ucfirst($this->data['UserReference']['first_name']));?>" onclick="javascript:feedbackform_popup('<?php echo $this->data['User']['id']; ?>', '<?php echo $this->Session->read('Auth.User.id'); ?>');">
                   </div>
                 <?php 	}
			?>
	</div>
		



                  <?php if(count($this->data['Testimonial']) > 0){?>
                    <div class="profile-testimonial" id="testimonial" style="display:none;">

		     <div class="profile-testimonial-content" id="slider-code"  >
                   <div class="viewport">
                        <ul class="overview">
                      <?php 
                	       foreach(($this->data['Testimonial']) as $testimonial) {	?>
                           <li>
			      <div class="profile-testimonial-quote" style="margin-top:25px;">
                        
			       <a href = "#testimonials" onclick="tog5();" style="color:white;">  	
			        
                         <?php  { $length = strlen($testimonial['testimonial_detail']);
                             if($length >= 100){
                            $string = ltrim($testimonial['testimonial_detail']);
                            $string = substr($string, 0, strpos(wordwrap($string, 100), "\n"));
                            
                              echo ($string);} 
                           else{
                              echo (ltrim($testimonial['testimonial_detail']));} 
                          }?>..
                          </a>
 
                      </div>
                     
			<div class="profile-testimonial-name" style="font-family: Ubuntu,arial,vardana;font-size: 14px;"><?php echo $testimonial['client_name']?></div>
                     <?php if($testimonial['client_designation'] != ''){?>
                     <div class="profile-testimonial-org" style="font-family: Ubuntu,arial,vardana;font-size: 12px;"><?php echo $testimonial['client_designation']?></div>
                     <?php }?>
			<div class="profile-testimonial-org" style="font-family: Ubuntu,arial,vardana;font-size: 12px;"><?php echo $testimonial['client_organization']?></div>
                      </li>
		    <?php }?>
                   </ul>
                </div>
		</div>
                </div>
                <?php }?>



	    </div>
	</div>
    </div>
</div>


<div class="profile-navbar">
    <div id="inner-content-wrapper">
	<div id="inner-content-box" class="pagewidth">
	    <div class="Profile_page_container">
		
                <div class="profile-nav">


                                  <?php
					$socialCount=0;
					$linkedinURL = '';
                                   $twitterURL ='';
                                   $googleURL ='';
                                   $facebookURL ='';
                                   $youtubeURL ='';
					while(count($this->data['Social'])>$socialCount)
					{
					
					if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'http'))!==false)
						{
							$link=$this->data['Social'][$socialCount]['social_name'];
						}
						else
						{
							$link="https://".$this->data['Social'][$socialCount]['social_name'];
						}
						
						$icon = $this->General->getIcon($this->data['Social'][$socialCount]['social_name']);
						if($icon == 'media/linkedin.png')
							$linkedinURL = $link;
                                          if($icon == 'media/twitter.png')
							$twitterURL = $link;
                                          if($icon == 'media/Google+.png')
							$googleURL = $link;
                                          if($icon == 'media/facebook.png')
							$facebookURL = $link;
						if($icon == 'media/youtube.png')
							$youtubeURL = $link;
						$socialCount++;
						}
				     ?>
                      
                  <?php $count = 1; 
                                      foreach ($answeredQs as $qaValue) {
                                       $count++;}
                                     $answeredQCount = $count - 1;?>
                  <?php $count = 1; 
                                      foreach ($questions as $qaValue) {
                                       $count++;}
                                     $unansweredQCount = $count - 1;?>
                  <?php $count = 1; 
                                      foreach ($insightData as $qaValue) {
                                       $count++;}
                                     $insightQCount = $count - 1;
                                     $sum =($answeredQCount + $insightQCount); ?>
			<?php
				$published_case_study = 0;
				foreach($this->data['Case_Study'] as $case_study) {
				
					if($case_study['specifier'] == 'Publish'){			
						$published_case_study++;
					}
				}
				
				$isTabs = 0;
				$answer_tab = 0;
				$feed_tab = 0;
                            $testimonial_tab = 0;
				
				if(count($this->data['Feed']) > 0)
					$feed_tab++;
                            if(count($this->data['Testimonial']) > 0)
					$testimonial_tab++;
                           if($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==Configure::read('App.Role.Mentor')){
				if(count($answeredQs) > 0 || (count($insightData) > 0 || count($unansweredQCount) > 0))
					$answer_tab++;
				}else{
				if(count($answeredQs) > 0 || count($insightData) > 0)
					$answer_tab++;
				}
				
				
				if($published_case_study > 0)
					$isTabs++;
				if($answer_tab > 0)
					$isTabs++;
				if($feed_tab > 0)
					$isTabs++;
				if($testimonial_tab > 0)
					$isTabs++;
				?>

                   <?php if($isTabs > 0){?>


		<ul>
		     <li id="bg1" class="profile-nav-current"><a href="#profile"  onclick="tog1();">Profile</a></li>
		    <li id="bg2" class="profile-hover-current"><a href="#case_studies" onclick="tog2();"style="<?php if($published_case_study == 0) {?>
					display: none;
					<?php }?>">Case Studies</a>
                 </li>
		    <li id="bg3" class="profile-hover-current"><a href="#answers"  onclick="tog3();" style="<?php if($answer_tab == 0){?>
					display: none;
					<?php }?>">Answers<sup style="font-size:11px;"><?php echo ($sum);?></sup></a>
                 </li>
		    <li id="bg4" class="profile-hover-current"><a href="#articles"  onclick="tog4();" style="<?php if($feed_tab == 0){?>
					display: none;
					<?php }?>">Articles</a>
                </li>
		    <li id="bg5" class="profile-hover-current"><a href="#testimonials"  onclick="tog5();" style="<?php if($testimonial_tab == 0){?>
					display: none;
					<?php }?>">Testimonials</a>
               </li>
		</ul>

              <?php }?>


       </div>
 
	<div class="profile-socialmedia">
		<ul><?php if($linkedinURL !=''){
                   if(($pos =strpos($linkedinURL,'http'))!==false)
						{
							$link=$linkedinURL;
						}
						else
						{
							$link="https://".$linkedinURL;
						}?>
		    <li><a href="<?php e($link);?>" target="_blank"><div class="sm-linkedin" style="margin-top:4px;"></div></a></li>
                  <?php }?>
                  <?php if($twitterURL !=''){?>
		    <li><a href="<?php e($twitterURL);?>" target="_blank"><div class="sm-twitter" style="margin-bottom:-2px;"></div></a></li>
                   <?php }?>
                  <?php if($googleURL !=''){?>
		    <li><a href="<?php e($googleURL);?>" target="_blank"><div class="sm-google" style="margin-top:5px;"></div></a></li>
                  <?php }?>
                  <?php if($facebookURL !=''){?>
		    <li><a href="<?php e($facebookURL);?>" target="_blank"><div class="sm-facebook" style="margin-top:5px;"></div></a></li>
                  <?php }?>
                   <?php if($youtubeURL !=''){?>
		    <li><a href="<?php e($youtubeURL);?>" target="_blank"><div class="sm-youtube" style="margin-top:5px;"></div></a></li>
                  <?php }?>


                  <?php if(($this->data['UserReference']['business_website']) !=''){
                     $link ='';
                     $url ='';
                       if(($pos =strpos($this->data['UserReference']['business_website'],'http'))!==false)
						{
							$link=$this->data['UserReference']['business_website'];
						}
						else
						{
							$link="https://".$this->data['UserReference']['business_website'];
						}
                                         ?>
                              
                                     

		    <li class="profile-socialmedia-url"><a href="<?php e($link);?>" target="_blank">Website</a></li>
                 
                   <?php }?>
		</ul>

	  </div>

	    </div>
	</div>
    </div>
</div>



    <div id="inner-content-wrapper">
	<div id="inner-content-box" class="pagewidth">
	    <div class="Profile_page_container">
		<div class="profile-sidebar">

		    <div class="profile-sidebar-shim"></div>
		    <div class="profile-sidebar-border"></div>
		    
                 <div class="profile-sidebar-icons">
		   <div class="socials"> 
                             <?php
                                    $url ='';
					$socialCount=0;
                                    
                               
					
					while(count($this->data['Social'])>$socialCount)
					{
					
					if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'http'))!==false)
						{
							$link=$this->data['Social'][$socialCount]['social_name'];
						}
						else
						{
							$link="https://".$this->data['Social'][$socialCount]['social_name'];
						}
						
						$icon = $this->General->getIcon($this->data['Social'][$socialCount]['social_name']);
                                          if(($pos =strpos($this->data['UserReference']['business_website'],'http'))!==false)
						{
							$url=$this->data['UserReference']['business_website'];
						}
						else
						{
							$url="https://".$this->data['UserReference']['business_website'];
						}
                                          
						if($icon != 'media/linkedin.png' && $icon != 'media/linkedin.png' && $icon != 'media/twitter.png' && $icon != 'media/Google+.png' && $icon != 'media/facebook.png' && $icon != 'media/youtube.png'  && $url != $link)
						{	
						e($html->link($html->image($icon,array('alt'=>$this->data['Social'][$socialCount]['social_name'] , 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						}
						$socialCount++;
						}
				?>
		    </div>
                  </div>


		    <div class="profile-sidebar-links">
			<ul>
                                  <?php if(($this->data['UserReference']['intro_link']) !=''){  
                                  if(false !== stripos($this->data['UserReference']['intro_link'],"youtube")) 
                                    {
                                     $pos = strpos($this->data['UserReference']['intro_link'], '//');
                                     $poss = strpos($this->data['UserReference']['intro_link'], "frameborder");
                                     $length = $poss - $pos;
                                     $url = substr($this->data['UserReference']['intro_link'],$pos+2,$length-4);
                                     //echo($url);
                                       }
                                    else if(false !== stripos($this->data['UserReference']['intro_link'],"vimeo"))  
                                      {                                 
                                      $pos = strpos($this->data['UserReference']['intro_link'], '//');
                                      $poss = strpos($this->data['UserReference']['intro_link'], '?');
                                      $length = $poss - $pos;
                                     $url = substr($this->data['UserReference']['intro_link'],$pos+2,$length-1);
                                     //echo($url);
                                     }?>
                                  <li><div class="sb-video"></div><a href="#" style="font-weight:bold;" onclick="TINY.box.show({iframe:'//<?php echo($url);?>',width:860,height:460,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">Play intro video</a></li>
                           <?php }?>
                       <?php if($this->Session->read('Auth.User.id') != '' && $this->Session->read('Auth.User.id') != $this->data['User']['id']){?>
			  <li><div class="sb-email"></div>

                        <span style="font-weight:bold;"><?php e($html->link("Refer to a colleague",'javascript:refer_member_popup();',array('alt'=>'Refer to a colleague')));}?></li></span>
                        
                         <?php if($this->Session->read('Auth.User.id') == ''){
                              $fname = $this->data['UserReference']['first_name'];
                              $lname = $this->data['UserReference']['last_name'];
                              $url = $this->data['User']['url_key'];
                              $links = "http://mentorsguild.com/$url";
                              
                              $link = preg_replace("#http://([A-z0-9./-]+)#", '$0', $links);
                              $body = "Hi, I saw this consultant's profile and thought you might be interested $link"; ?>
                              <li><div class="sb-email"></div>
                              <a href="mailto:?subject=An expert for your reference&body=<?php echo ($body);?>" style="font-weight:bold;">Refer to a colleague</a><?php }?></li>
                       

                                
                          
		      
         

                           
			    <li><div class="sb-share"></div>
                         <div class="share_icons" onmouseout="doSomethingMouseOut();"  onmouseover="doSomethingMouseOver()">
                        <span style="font-weight:bold;"><?php e($html->link(" Share on social media",'javascript:doSomething()',array('escape'=>false,'onmouseover' => 'doSomethingMouseOver()')));?></span>
                         
				
                           


<!-- Social share start -->
<div id="shareicon" style="margin-left:20px;visibility:hidden">
&nbsp; 
	<a href="javascript:window.open('https://plus.google.com/share?url=<?php e($completelink);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
		<?php e($html->image('images/g_plus_icon.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'27'))); ?></a>

							<a title="Share this post/page"href="javascript:window.open('http://www.facebook.com/sharer.php?s=100&p[url]=<?php echo $completelink; ?>&p[title]=<?php e($this->data['UserReference']['first_name'].' '.$this->data['UserReference']['last_name'].' is on Mentors Guild');?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
							      <?php e($html->image('images/fb_icon.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'27'))); ?></a>
<?php 

 $linkdinUrl="http://www.linkedin.com/shareArticle?mini=true&url=".$completelink."&title=".$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name']." is on Mentors Guild&summary=".$completelink." Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results.&source=http://mentorsguild.com";
if($twitterURL != ''){
$path = explode("/", $twitterURL);
$twitter_handle = end($path);
$tweet="http://twitter.com/intent/tweet?text=".$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name']." @".$twitter_handle." is on Mentors Guild ".$completelink." ";
}
else{
$tweet="http://twitter.com/intent/tweet?text=".$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name']." is on Mentors Guild ".$completelink." ";
}
 ?>
                                               <a href="javascript:window.open('<?php echo ($linkdinUrl);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')"><?php e($html->image('images/logo_linkedin.png',array('alt'=>'LinkedIn' , 'height'=>'25', 'width'=>'27', 'id'=>'linkedin_Share'))); ?></a>
                                               <a href="javascript:window.open('<?php echo ($tweet);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')"><?php e($html->image('images/twit_icon.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'27', 'id'=>'twitter_Share'))); ?></a>
						
						

			</div>
<!-- Social share end -->
                </div>	
             </li>	

         </ul>
     </div>



		    <div class="profile-sidebar-comm">
			
                        <h5>Communication</h5>
			<?php 
			  $modeTypeFaceToFace=$modeTypePhone=$modeTypeVideo='FALSE' ;
			  $modeValue1=$modeValue2=$modeValue3='';
			  if(isset($this->data['Communication'][0]['mode_type']) && $this->data['Communication'][0]['mode_type'] != '')
			  {
			  		
					if($this->data['Communication'][0]['mode_type']=='face_to_face')
				{
						$modeTypeFaceToFace='TRUE';
			  			$modeValue1=$this->data['Communication'][0]['mode'];
					}
					else if($this->data['Communication'][0]['mode_type']=='phone')
						{
						  $modeTypePhone='TRUE';
			  			  $modeValue2=$this->data['Communication'][0]['mode'];
			  			}
			  			
			  			else
			  			{
			  			   $modeTypeVideo='TRUE';
			  			   $modeValue3=$this->data['Communication'][0]['mode'];
					 }
					
	
				
			}
			  if(isset($this->data['Communication'][1]['mode_type']) && $this->data['Communication'][1]['mode_type'] != '')
			  {
			  		
					if($this->data['Communication'][1]['mode_type']=='face_to_face')
				{
						$modeTypeFaceToFace='TRUE';
			  			$modeValue1=$this->data['Communication'][1]['mode'];
					}
					else if($this->data['Communication'][1]['mode_type']=='phone')
						{
						   $modeTypePhone='TRUE';
			  		       $modeValue2=$this->data['Communication'][1]['mode'];
			  			}
			  			
			  			else
			  			{
			  			   $modeTypeVideo='TRUE';
			  			   $modeValue3=$this->data['Communication'][1]['mode'];
					 }
					
		
				
			}
			 if(isset($this->data['Communication'][2]['mode_type']) && $this->data['Communication'][2]['mode_type'] != '')
			  {
			  		if($this->data['Communication'][2]['mode_type']=='face_to_face')
					{
						$modeTypeFaceToFace='TRUE';
			  			$modeValue1=$this->data['Communication'][2]['mode'];
					}
						else if($this->data['Communication'][2]['mode_type']=='phone')
						{
						   $modeTypePhone='TRUE';
			  		       $modeValue2=$this->data['Communication'][2]['mode'];
			  			}
			  			
			  			else
			  			{
			  			   $modeTypeVideo='TRUE';
			  			   $modeValue3=$this->data['Communication'][2]['mode'];
					 }
					
					
			  }
			
			
				 ?>

         
                     <?php if($modeTypeFaceToFace=='TRUE') {?>
						<?php if(trim($modeValue1)!='') : ?>
			<div class="sb-locator-pin"></div><p>In person: <?php echo $modeValue1; ?><?php endif; } ?></p>
			
                    <?php if($modeTypeVideo=='TRUE') {?>
		                           <?php if(trim($modeValue3)!='') : ?>
                     <div class="sb-computer"></div><p><?php echo $modeValue3; ?><?php endif; }?></p>
			
                    <?php if($modeTypePhone=='TRUE' && ($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==Configure::read('App.Role.Mentor') || $this->data['User']['extension'] == NULL)) {
					 if(trim($modeValue2)!=''){  ?>
                   
                    <div class="sb-phone" ></div><p><?php echo $modeValue2; ?><?php }?></p>
		      <?php }else{
                    			 if(trim($modeValue2)!='') { ?>
                   
                    <div class="sb-phone" ></div><p>1-866-511-1898 <span style="font-weight:bold;">x<?php echo $this->data['User']['extension']; ?></span><?php } ?></p>
                    <?php }?>
               </div>
             

		 <div class="profile-sidebar-comm">
			<h5>Availability</h5>
			<div class="sb-clock"></div><p><?php echo($this->data['Availablity']['day_time']); ?></p>
		 </div>

                
		 <div class="profile-sidebar-logos">

			<?php if (($this->data['Showcase_client']['first_client']) !=''){?>
                     <div class="profile-sidebar-logo">
                    <img src="https://logo.clearbit.com/<?php e($this->data['Showcase_client']['first_client']);?>?size=60">
                   </div>
		     <?php }?>

 
                  <?php if (($this->data['Showcase_client']['second_client']) !=''){?>
                  <div class="profile-sidebar-logo">
                  <img src="https://logo.clearbit.com/<?php e($this->data['Showcase_client']['second_client']);?>?size=60">
                  </div>
		    <?php }?>


                  <?php if (($this->data['Showcase_client']['third_client']) !=''){?>
                  <div class="profile-sidebar-logo">
                  <img src="https://logo.clearbit.com/<?php e($this->data['Showcase_client']['third_client']);?>?size=60">
                  </div>
		    <?php }?>
                  <?php if (($this->data['Showcase_client']['fourth_client']) !=''){?>
                  <div class="profile-sidebar-logo">
                  <img src="https://logo.clearbit.com/<?php e($this->data['Showcase_client']['fourth_client']);?>?size=60">
                  </div>
		    <?php }?>
                  <?php if (($this->data['Showcase_client']['fifth_client']) !=''){?>
                  <div class="profile-sidebar-logo">
                  <img src="https://logo.clearbit.com/<?php e($this->data['Showcase_client']['fifth_client']);?>?size=60">
                  </div>
		    <?php }?>
                  <?php if (($this->data['Showcase_client']['sixth_client']) !=''){?>
                  <div class="profile-sidebar-logo">
                  <img src="https://logo.clearbit.com/<?php e($this->data['Showcase_client']['sixth_client']);?>?size=60" onerror="this.src='<?php e(SITE_URL)?>/imgs/share-icon-1.png'" style="width:60px;height:60px;">
                  </div>
		    <?php }?>
                  <?php if (($this->data['Showcase_client']['seventh_client']) !=''){?>
                  <div class="profile-sidebar-logo">
                  <img src="https://logo.clearbit.com/<?php e($this->data['Showcase_client']['seventh_client']);?>?size=60">
                  </div>
		    <?php }?>


            </div>
              
                     <?php if(($this->data['UserReference']['categories_tags']) !=''){?>
		    <div class="profile-sidebar-categories">
			<?php 
                         $stringExp = $this->data['UserReference']['categories_tags'];
				$Exp3 = explode(',',$stringExp);
				$allExp = array();
				$count = 0;
					
				for($i=0;$i<count($Exp3);$i++){
				
					if(isset($Exp3[$i]) && $Exp3[$i] != '')
						$allExp[$count++] = ucfirst(trim($Exp3[$i]));
				}
					
				$uniqueAry = array_unique($allExp);
				
		        foreach($uniqueAry as $catValue){
                      if((false !== stripos($catValue,"/")) || (false !== stripos($catValue,"&")) || (false !== stripos($catValue,"-")))
                      {
                     if(false !== stripos($catValue,"/")){
                     $catValues = str_replace("/","_slash",$catValue);
                      $catValues =  strtolower($catValues);}

                      if(false !== stripos($catValue,"&")){
                     $catValues = str_replace("&","_and",$catValue);
                      $catValues =  strtolower($catValues);}

                      if(false !== stripos($catValue,"-")){
                     $catValues = str_replace("-","_or",$catValue);
                        $catValues =  strtolower($catValues);}
                      ?>
			<span class="profile-sidebar-cat"> <?php e($html->link($catValue,SITE_URL.'browse/search_result/'.$catValues,array('escape'=>false))); ?>
			   </span><br/>
                     <?php } else{?>
                     <span class="profile-sidebar-cat"> <?php e($html->link($catValue,SITE_URL.'browse/search_result/'.$catValue,array('escape'=>false))); ?>
			   </span><br/><?php }}?>
		    </div>
                  <?php }?>
      
                  
           
                 

	  </div> <?php //profile sidebar end ?>

             
		
	    <div class="profile-body" id="profilebody"style="width: 674px; margin-bottom: 30px; display: block;">

               <?php if($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==Configure::read('App.Role.Mentor'))
				{echo $this->element('mentorsuser/mentor_dash');}?>
					  
                   					
				
                   					
				
               
	       <div class="mg-profile-expertise"></div><h2>Areas of expertise</h2>
	       <p><?php e($this->data['UserReference']['area_of_expertise']); ?></p>
 
	    	<div class="mg-profile-profile"></div><h2>Desired client profile</h2>
		<p><?php e($this->data['UserReference']['desire_mentee_profile']); ?></p>

		<div class="mg-profile-summary"></div><h2>Professional summary</h2>
		<p><?php e(nl2br(strip_tags($this->data['UserReference']['background_summary']))); ?></p>

		<div class="mg-profile-engagement"></div><h2>Engagement overview</h2>
		<p>
              <?php if($this->data['UserReference']['feeNSession']) {?>
		<?php $result = str_replace('  ', '&nbsp;&nbsp;', $this->data['UserReference']['feeNSession']); 
			 echo nl2br($result);?>			
               <?php } else {?>
			 Duration and cost of an engagement depends on the scope of work, identified during the initial consultation.
		<?php }?>
             </p>

             <?php if(isset($this->data['UserReference']['past_clients']) && $this->data['UserReference']['past_clients'] != '') {?>
				
		<div class="mg-profile-clients" id="clients"></div><h2>Clients</h2>
		<p><?php $answer = str_replace('  ', '&nbsp;&nbsp;', $this->data['UserReference']['past_clients']); 
						echo $this->General->make_links(nl2br($answer));?>
              </p>
                 <?php }?>
	   </div>
             
               <?php// code for different tab ?>

               <?php //if($published_case_study > 0) {?>
		<div id="divParent" style="width: 674px;height: 100%; margin-bottom: 30px; display: none;">	
			<?php 
			$counter = 1; 
			$length = count($this->data['Case_Study']);
			foreach(array_reverse($this->data['Case_Study']) as $case_study)
					{	if($case_study['specifier'] == 'Publish'){?>		
			<div class="collaborative" style="color:#2a2a2a;" onclick="toggle(<?php echo $counter;?>,<?php echo $length;?>)" id="head<?php echo $counter;?>">
				<?php echo $case_study['title'];?>
				<span><img id="img<?php echo $counter;?>" src="<?php echo SITE_URL;?>img/media/dnarrow.png"></span>
			</div>

			<div class="textSH" id="sub<?php echo $counter;?>" style='display:none;'>
				<h1>Client</h1>
				<p>
					<?php 
					if(isset($case_study['url']) && $case_study['url'] != '') {
					$link = $case_study['url'];
					
					$icon = $this->General->getIcon($case_study['url']);
					e($html->link($html->image($icon,array('alt'=>$link, 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						
					}
					echo $case_study['client_details'];?>
				</p>

				<h1>Situation</h1>
				<p>
					<?php $situation = str_replace('  ', '&nbsp;&nbsp;', $case_study['situation']); 
					echo nl2br($situation);?>
				</p>

				<h1>Actions</h1>
				<p>
					<?php $actions = str_replace('  ', '&nbsp;&nbsp;', $case_study['actions']); 
					echo nl2br($actions);?>
				</p>

				<h1>Results</h1>
				<p>
					<?php $result = str_replace('  ', '&nbsp;&nbsp;', $case_study['result']); 
					echo nl2br($result);?>
				</p>
				<?php
				$completelinkCS=SITE_URL.strtolower($this->data['User']['url_key'])."#case_studies";
				?>
				<?php 
					$linkdinUrl="http://www.linkedin.com/shareArticle?mini=true&url=".$completelinkCS."&title=".$case_study['title'];
					$tweet="http://twitter.com/intent/tweet?text=".$case_study['title']." ".$this->General->get_tiny_url(urlencode($completelinkCS))." ";
				?>
						
				<div class="share_icons" onmouseout="doSomethingMouseOutCS(<?php e($counter);?>);"  onmouseover="doSomethingMouseOverCS(<?php e($counter)?>)" style="margin-top: 30px; width: 220px;">
					<div style="float:left;margin-bottom: 0px;" >
						<?php e($html->link($html->image('images/share_icon.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'77')),'javascript:doSomethingCS('.$counter.')',array('escape'=>false,'onmouseover' => 'doSomethingMouseOverCS('.$counter.')')));?>
					</div>
					<!-- Google Start -->
					<div id="shareiconCS_<?php e($counter);?>" style="float:left;visibility:hidden;">
									
						<a href="https://plus.google.com/share?url=<?php echo $completelinkCS; ?>" 
						target="_blank"><?php e($html->image('images/g_plus_icon.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'27'))); ?></a>
						<!-- Google complete -->
						
						<a title="Share this post/page"
						href="http://www.facebook.com/sharer.php?
						s=100
						&p[url]=<?php echo $completelinkCS; ?>
						&p[title]=<?php e($case_study['title']);?>
						&p[summary]=Mentors Guild is a community of experts, eager to share their expertise with others."
						target="_blank">
						<?php e($html->image('images/fb_icon.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'27'))); ?>
						</a>			
						
						<?php e($html->link($html->image('images/logo_linkedin.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'27')),$linkdinUrl,array('escape'=>false , 'target'=>'_blank')));?>
						<?php e($html->link($html->image('images/twit_icon.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'27')),$tweet,array('escape'=>false , 'target'=>'_blank')));?>
						
					</div>		
				</div>	
			</div>
			<?php $counter++; }}?>
		    </div>
		    <?php //}?>




                  <?php //if($answer_tab > 0) {?>
		    <div id="answersDiv" style="width: 674px;height: 100%; margin-bottom: 30px; display: none">
		    	<div id="answeredQ" style="display: block;">

                              <?php foreach($insightData as $insight){?>
					
                                       <div class="TXT-LHight" style="padding-top:15px;">	   
                                        <p>
			            	<a href="<?php e(SITE_URL.'insight/insight/'.$insight['Insight']['url_key'])?>" class="questionA"><span style="font-weight:bold;font-size:15px;color:#2a2a2a;"><?php e($insight['Insight']['title'])?></span></a>
			                   </p>
                                  
                                  <p style="font-weight: normal;">
			            	 <?php
							$strdata = strlen ($insight['Insight']['insight']);
							if($strdata > 400){
								echo nl2br($this->Text->truncate($insight['Insight']['insight'], 400, array('ending' => '...', 'exact' => false)));
							}else{
								echo nl2br($insight['Insight']['insight']);
							}
							e($html->link("Read more",SITE_URL.'insight/insight/'.$insight['Insight']['url_key'],array('escape'=>false,'class'=>'readMore')));?>
			            </p>
			           
                                       <div class="QnAbutton">
			          	<?php foreach ($insight['Insight_category'] as $catValue) {
			    		
                                                           
                                                            $str = ($categories[$catValue['category_id']]);
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"&"))
                                                            $str = str_replace("&","_and",($categories[$catValue['category_id']]));
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"/"))
                                                            $str = str_replace("/","_slash",($categories[$catValue['category_id']]));

            		             e($html->link($categories[$catValue['category_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false)));
			    		}?>
						<br/><br/>
			            </div>
                                   
	            		        <div class="HRLine" style="margin-top: 5px;"></div>
                                </div>
                          <?php }?>

				    <?php $count = 1; foreach ($answeredQs as $qaValue) {?>
        		              <div class="TXT-LHight" style="padding-top:15px;">
			            <p>
			            	<a href="<?php e(SITE_URL.'qna/question/'.$qaValue['Qna_question']['url_key'])?>" class="questionA"><span style="font-weight:bold;font-size:15px;color:#2a2a2a;"><?php e($qaValue['Qna_question']['question_text'])?></span></a>
			            </p>
			            <p style="font-weight: normal;">
			            	 <?php
							$strdata = strlen ($qaValue['answer_text']);
							if($strdata > 400){
								echo nl2br($this->Text->truncate($qaValue['answer_text'], 400, array('ending' => '...', 'exact' => false)));
							}else{
								echo nl2br($qaValue['answer_text']);
							}
							e($html->link("Read more",SITE_URL.'qna/question/'.$qaValue['Qna_question']['url_key'],array('escape'=>false,'class'=>'readMore')));?>
			            </p>
			            <div class="QnAbutton">
			            		<?php foreach ($qaValue['Qna_question_category'] as $catValue) {
                                                            $str = ($categories[$catValue['category_id']]);
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"&"))
                                                            $str = str_replace("&","_and",($categories[$catValue['category_id']]));
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"/"))
                                                            $str = str_replace("/","_slash",($categories[$catValue['category_id']]));

            		                e($html->link($categories[$catValue['category_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false)));
                                              }?>
			                <br/><br/>
			            </div>
	            		<div class="HRLine" style="margin-top: 5px;"></div>
        		</div>
				<?php $count++;} $answeredQCount = $count - 1;?>
		   </div>
		    	<?php  if($this->Session->read('Auth.User.id')==$this->data['User']['id'] && $unansweredQCount != 0) {?>
                      <br/><br/>
                    <div class="expertise" style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal;font-size: large;color: #2a2a2a; padding-bottom: 5px;border-bottom: 1px solid #bcbcbc;">Unanswered Questions</div>
		    	<div id="unAnsweredQ">
				    <?php $count = 1; foreach ($questions as $qaValue) {?>
        			<div class="TXT-LHight" style="padding-top:15px;" id="unansweredQ<?php echo ($qaValue['Qna_question']['id'])?>">
			            <p>
			                <a href="<?php e(SITE_URL.'qna/question/'.$qaValue['Qna_question']['url_key'])?>" class="questionA"><?php e($qaValue['Qna_question']['question_text'])?> </a>
			            </p>
			            <p style="font-weight: normal;" id="unansweredQtext<?php echo ($qaValue['Qna_question']['id'])?>">
			            	 <?php
							$strdata = strlen (nl2br($qaValue['Qna_question']['question_context']));
							if($strdata > 400){
								echo nl2br($this->Text->truncate($qaValue['Qna_question']['question_context'], 400, array('ending' => '...', 'exact' => false)));
							}else{
								echo nl2br($qaValue['Qna_question']['question_context']);
							}
						 
			            	e($html->link("Read more",SITE_URL.'qna/question/'.$qaValue['Qna_question']['url_key'],array('escape'=>false,'class'=>'readMore')));?>
			            </p>
			            <div class="QnAbutton" id="unansweredQCat<?php echo ($qaValue['Qna_question']['id'])?>">
			            		<?php foreach ($qaValue['Qna_question_category'] as $catValue) {
                                                            $str = ($categories[$catValue['category_id']]);
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"&"))
                                                            $str = str_replace("&","_and",($categories[$catValue['category_id']]));
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"/"))
                                                            $str = str_replace("/","_slash",($categories[$catValue['category_id']]));

            		                e($html->link($categories[$catValue['category_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false)));
                                       }?>
			                <br/><br/>
			            </div>
			            <div style="float: right;" id="unansweredQButton<?php echo ($qaValue['Qna_question']['id'])?>">
                                          <a href="javascript:delQuestion(<?php echo $qaValue['Qna_question']['id']?>)">Ignore</a>
				            
					    	<input id="applyPreview" style="float: none; margin-left: 15px;" value="Answer" type="submit" onclick="window.location.href='<?php echo SITE_URL."qna/question/".$qaValue['Qna_question']['url_key']; ?>'; ">
			            </div>
	            		<div class="HRLine" style="margin-top: 45px;"></div>
        		   </div>
				<?php $count++; }?>
		     </div>
		    	<?php  } $unAnsweredQCount = $count - 1;?>
		</div>
              <?php //}?> 

         <?php //if($feed_tab > 0) {?> 
        <div id="feedDiv" style="width: 674px;height: 100%; display:none; padding-top: 15px;">	
		<div id="allFeeds">
                	<?php 
                	foreach(array_reverse($this->data['Feed']) as $feed)
					{	   
                                    	
					?>
                                  <?php if (strpos($feed['url'], "www")!=false){
                                     $pos = strpos($feed['url'], '.');
                                   
                                      $pos1 = strpos($feed['url'], '/');
                                      $pos2 = strpos($feed['url'],'/', $pos1 + strlen('/'));
                                      $pos3 = strpos($feed['url'],'/', $pos2 + strlen('/'));
                                      $final =$pos3 - $pos;
                                      $url = substr($feed['url'],$pos+1,$final-1);
                                       }
                                      else{
                                          $pos1 = strpos($feed['url'], '/');
                                         $pos2 = strpos($feed['url'],'/', $pos1 + strlen('/'));
                                        $pos3 = strpos($feed['url'],'/', $pos2 + strlen('/'));
                                        $final =$pos3 - $pos2;
                                      $url = substr($feed['url'],$pos2+1,$final-1);
                                      }
                                    ?> 
                                     <span style ="color:#929292; font-size:10px;font-weight:normal;"><?php echo(strtoupper($url));?></span>
					<p id="feed<?php echo $feed['id']?>" style="margin-top: 7px">
                                   
					<a href="<?php echo $feed['url']?>" target="_blank"style="text-decoration: none"><span style="font-weight:bold;font-size:15px;color:#2a2a2a;"><?php echo $feed['title'];
					?></span></a>
                                   <?php $length = strlen($feed['summary']);
                                             $string = substr($feed['summary'],0,240);
                                            
                                        if($length >= 240){?>
                                   
                                  
                                   
                                   <p id="summary<?php echo $feed['id']?>" >
					<?php echo $string;
					?>... <a href="<?php echo $feed['url']?>" target="_blank">read more</a></p>
                                  <?php }?>
                                     <?php $length = strlen($feed['summary']);
                                             $string = substr($feed['summary'],0,240);
                                        if($length <= 240){?>
					<p id="summary<?php echo $feed['id']?>" >
					<?php echo $feed['summary'];
					?></p>
                                     <?php }?>
					
					
					<br/>
					<?php }?>
				</div>
			</div>

                <?php //}?>


           <?php //if($testimonial_tab > 0) {?> 
            <div id="testimonialDiv" style="width: 674px;height: 100%;display:none; padding-top:20px;">	
		<div id="allTestimonials">
                                 
                	<?php 
                	foreach(array_reverse($this->data['Testimonial']) as $testimonial)
					{	   
                                    	
					?>
                                  
                                 <div class="expertise" style="margin-bottom:50px;width:674px;border-bottom:1px solid #bcbcbc;float:right;">  
                         
					
                              
					<p id="testimonial<?php echo $testimonial['id']?>" style="margin-top:-20px;">
					<?php echo $testimonial['testimonial_detail'];?>
					</p>
                                     <p id="testimonial_client<?php echo $testimonial['id']?>" style="font-size:14px;font-weight:normal;" >
                                    
                                   <?php if ($testimonial['client_name'] == '' && $testimonial['client_organization'] != ''){?>
                                   
                                      <span style="float:right;"><?php echo $testimonial['client_organization']?></span>

                                   <?php } elseif ($testimonial['client_name'] == '' && $testimonial['client_organization'] == ''){?>
                                   
                                      <span style="float:right;"><?php echo $testimonial['client_designation']?></span>
                                 
                                    <?php } elseif ($testimonial['client_designation'] != '' && $testimonial['client_organization'] == ''){?>
                                  
                                 <span style="float:right;"><?php echo $testimonial['client_name']?>,&nbsp;<?php echo $testimonial['client_designation']?></span>
                                 
                                 <?php } else if( $testimonial['client_organization'] != '' && $testimonial['client_designation'] == ''){?>
                                  
                                 <span style="float:right;"><?php echo $testimonial['client_name']?></span><br><span style="float:right;"><?php echo $testimonial['client_organization']?></span>
                                  
                                 <?php }else if($testimonial['client_organization'] == '' && $testimonial['client_designation'] == ''){?>
                                    <span style="float:right;"><?php echo $testimonial['client_name']?></span>

                                  <?php }else if($testimonial['client_organization'] == "MG-Consultant"){
                                           $urlkey = str_replace(' ', '.', $testimonial['client_name']);
                                           $link = SITE_URL.$urlkey;
                                           ?>
                                      <span style="float:right;"> <a href="<?php e($link);?>" target="_blank"><?php echo $testimonial['client_name']?></a>,&nbsp;<?php echo $testimonial['client_designation']?></span>

                                   <?php }else if($testimonial['client_organization'] == "MG-Client"){
                                           $urlkey = str_replace(' ', '.', $testimonial['client_name']);
                                           $link = SITE_URL."clients/my_account/".$urlkey;
                                           ?>
                                      <span style="float:right;"> <a href="<?php e($link);?>" target="_blank"><?php echo $testimonial['client_name']?></a>,&nbsp;<?php echo $testimonial['client_designation']?></span>
                                 <?php }else{?>
                                      <span style="float:right;"> <?php echo $testimonial['client_name']?>,&nbsp;<?php echo $testimonial['client_designation']?></span><br><span style="float:right;"><?php echo $testimonial['client_organization']?></span>
                              
                                    <?php }?>
                                 
                                     </p>
                               
                             </div>
					
					
					<?php }?>
		    </div>
	     </div>
                     <?php //}?>



 </div>     

  <?php if($linkedinURL != '') {?>
  <div style="padding-bottom: 50px;">
  <script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
  <script type="IN/MemberProfile" data-id="<?php e($linkedinURL);?>" data-format="inline" data-width="674"></script>
  </div>
  <?php }?>
	    
	
</div>
</div>
<script type="text/javascript">
function delQuestion(ques_id){

		jQuery(document).ready(function(){
			jQuery.ajax({
				url:SITE_URL+'qna/ignore_question',
				type:'post',
				dataType:'json',
				data:'ques_id='+ques_id,
				success:function(res){
					if(res.id !=''){
                                          var Question = document.getElementById('unansweredQ'+res.id).style.display="none"; 

					}else{
						alert("Failure!!!");
					}
				}
			});
		});
    }

</script>
<script type="text/javascript">
jQuery(document).ready(function(){
    window.onload=my_init();
});
function my_init(){

	var a = document.createElement('a');
	a.href = document.URL;
	if(a.hash=='#profile' || a.hash=='')
	{
        tog1(); 
	}
      
	else if(a.hash=='#case_studies')
	{
		tog2(); 
	}
	else if(a.hash=='#answers')
	{
		tog3(); 
		giveAnswers();
	}

    else if(a.hash=='#articles')
	{
		tog4(); 
	}
   else if(a.hash=='#testimonials')
	{
		tog5(); 
	}
   else if(a.hash=='#clients')
	{
		document.getElementById("#clients").scrollIntoView(); 
	}
	else
	{
		tog5();
	}
}
</script>
<script type="text/javascript">


/*

	var noOfCaseStudies = document.getElementById("noOfCaseStudies").value - 1;
	
	var f1=document.getElementById("head"+noOfCaseStudies);
	f1.style.color="#292929";
	document.getElementById("sub"+noOfCaseStudies).style.display="block";
	
	document.getElementById("img"+noOfCaseStudies).setAttribute('src',SITE_URL+'/img/media/uparrow.png');
	
}
*/

function apply(url,mentor_id,ClickType){
        var role_id = "<?php echo $this->Session->read('Auth.User.role_id'); ?>";
        var email_send = "<?php echo $this->Session->read('Auth.User.email_send'); ?>";
        var is_approved = "<?php echo $this->Session->read('Auth.User.is_approved'); ?>";
        if(role_id==''){
        	//applyloginpopup(mentor_id);
			applyregisterpopup();
            
        }else if(role_id==2){
            alert("Sorry, mentors are currently not allowed to apply for mentorship from other mentors.");
        }else{
            if(is_approved=='0')
            {
                location.href = SITE_URL+"users/logout";
            }
            else
            {
                if(ClickType == "request"){
                    requestMentorship(mentor_id);
                }
                else{
                    location.href = url+mentor_id;
                }
            }
        }
    }

function giveAnswers(){
	document.getElementById("toggleBtnAns").style.display="none";
	document.getElementById("answeredQ").style.display="none";
	document.getElementById("toggleBtnUnAns").style.display="block";
	document.getElementById("unAnsweredQ").style.display="block";
}

function myAnswers(){
	document.getElementById("toggleBtnAns").style.display="block";
	document.getElementById("answeredQ").style.display="block";
	document.getElementById("toggleBtnUnAns").style.display="none";
	document.getElementById("unAnsweredQ").style.display="none";
}
 </script>
<script>
var clickTrack = 0;
function doSomethingMouseOverCS(key){
	if(clickTrack == 0){
		var id_div=document.getElementById("shareiconCS_"+key);
		var vis = id_div.style;
		vis.visibility='visible';
		vis.position='relative';
	}
}

function doSomethingMouseOutCS(key){
	if(clickTrack == 0){
		var id_div=document.getElementById("shareiconCS_"+key);
		var vis = id_div.style;
		vis.visibility='hidden';
		vis.position='absolute';
	}
}

function doSomethingCS(key)
{
		
		//alert(clickTrack);	
		var id_div=document.getElementById("shareiconCS_"+key);
		var vis = id_div.style;
	if ( (vis.visibility=='visible' || vis.visibility=='hidden') && clickTrack == 0)
	{
		clickTrack = 1;
		vis.visibility='visible';
		vis.position='relative';
	}
	else if (vis.visibility=='visible' && clickTrack == 1)
	{
		clickTrack = 0;
		vis.visibility='hidden';
		vis.position='absolute';
		
	}
}
</script>
<script>
var clickTrack = 0;
function doSomethingMouseOver(){
	if(clickTrack == 0){
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
		vis.visibility='visible';
		vis.position='relative';
	}
}

function doSomethingMouseOut(){
	if(clickTrack == 0){
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
		vis.visibility='hidden';
		vis.position='absolute';
	}
}

function doSomething()
{
		
		//alert(clickTrack);	
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
	if ( (vis.visibility=='visible' || vis.visibility=='hidden') && clickTrack == 0)
	{
		clickTrack = 1;
		vis.visibility='visible';
		vis.position='relative';
	}
	else if (vis.visibility=='visible' && clickTrack == 1)
	{
		clickTrack = 0;
		vis.visibility='hidden';
		vis.position='absolute';
		
	}
}
</script>

<style>
.tbox {position:absolute; display:none; padding:14px 17px; z-index:900}
.tinner {padding:15px; -moz-border-radius:0px; border-radius:0px; background: #212121 url(/mg/img/preload.gif) no-repeat 50% 50%; border-right:1px solid #212121; border-bottom:1px solid #212121}
.tmask {position:absolute; display:none; top:0px; left:0px; height:100%; width:100%; background:#000; z-index:800}
.tclose {position:absolute; top:0px; right:0px; width:30px; height:30px; cursor:pointer; background:url(/mg/img/close.png) no-repeat}
.tclose:hover {background-position:0 -30px}

#error {background:#ff6969; color:#fff; text-shadow:1px 1px #cf5454; border-right:1px solid #000; border-bottom:1px solid #000; padding:0}
#error .tcontent {padding:10px 14px 11px; border:1px solid #ffb8b8; -moz-border-radius:5px; border-radius:5px}
#success {background:#2ea125; color:#fff; text-shadow:1px 1px #1b6116; border-right:1px solid #000; border-bottom:1px solid #000; padding:10; -moz-border-radius:0; border-radius:0}
#blackmask {background:#000}
#frameless {padding:0}
#frameless .tclose {left:6px}
</style>
<script type="text/javascript" src="//s3.amazonaws.com/scripts.hellobar.com/f876b1b3d2e2502ac0853ca4e83d0aa530c8f3b3.js"></script>
<script type="text/javascript" src="<?php e(SITE_URL)?>js/jquery.tinycarousel.min.js"></script>
	<script type="text/javascript">         
    jQuery(document).ready(function(){               
       setTimeout(function(){jQuery("#testimonial").css('display','inline'); }, 3000);     
    	jQuery('#slider-code').tinycarousel({ pager: true, interval: true, intervaltime: 6000, duration: 500    });
        
    });
</script>



<style>
.profile-testimonial{
width:auto;
}
.profile-sidebar-logo
{
  background-color: #f9f8f8;
}
#slider-code{height:200px;overflow:hidden;}
#slider-code .viewport{float:left;width:240px;height:200px;overflow:hidden;position:relative;}
#slider-code .overview{list-style:none;padding:0;margin:0;position:absolute;left:0;top:0;}
#slider-code .overview li{float:left;margin:0 5px 0 0;padding:1px;height:200px;width:240px;}
</style>