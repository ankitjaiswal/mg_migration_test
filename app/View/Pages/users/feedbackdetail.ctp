<?php e($html->css(array('regist_popup'))); ?>
<div id="registerationFm">
	<h1>Feedback for Robert Wong</h1>
	<!-- VL 27/12-->
	
	<?php e($form->create('User',array('url'=>array('controller'=>'users','action'=>'registration_step2'),'id'=>'registerForm'))); ?>	
	<div class="feedbackcontent">
	<p><strong>Michael Dang</strong> 8th Sep, 2012<br />		
    Robert is very knowledgeable and patient as a 
    mentor. He is also very open about things he 
    does not know.</p>
	<br />	
    <p><strong>Alan Kay</strong> 8th Sep, 2012<br />		
    I look forward to every session with Robert. He 
    supported me every step of the way in getting 
    a large bank loan...saving me several thousand 
    dollars in CPA fees. This is just one example of 
    his practical mentorship.</p>
    <br />	
    <p><strong>Steve Krug</strong> 8th Sep, 2012<br />		
    Robert is good for learning the basics of finance. 
	His advice though limited, is very practical.</p>
    <br />	
    <p><strong>Michael Dang</strong> 8th Sep, 2012<br />		
    Robert is very knowledgeable and patient as a 
    does not know.</p>
	</div>
	<div class="feedback-link"><a target="_blank" href="#">Read more ></a></div>
	<?php e($form->end()); ?>	
	</div>
</div>
</div>
