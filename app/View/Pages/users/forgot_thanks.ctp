<div id="inner-content-wrapper" style="height: 400px">
    <div id="inner-content-box" class="pagewidth">
        <div id="user" style="margin-left:53px;">
            <h1 style="color: #000;font-size:15px;">
                Help is on the way.
            </h1>
        </div>
        <div style="padding: 20px 50px;color:grey;font-size:15px;">
            <h1 style="font-size:15px;color:#000;">
                We've sent you an email with instructions for changing your password.
            </h1>
        </div>
        <div id ="app-button">		
            <?php e($form->create('User', array('url' => '/','method'=>'link')));?>     
                <input type="submit" value="Done" id="submit"/>
            <?php e($form->end()); ?>
        </div>
    </div>
</div>