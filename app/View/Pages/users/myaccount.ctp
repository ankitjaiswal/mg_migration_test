<h1 class="heading">My Account</h1>
<div class="productContainer">
	<div class="contents">
		<p align="justify">
			<table align="left" cellpadding="2" cellspacing="2" width="100%">				
				
				<tr>
					<td align="left" valign="top" colspan="2">Congratulations <?php e($session->read('Auth.User.screen_name')) ;?></td>
				</tr>			
				<tr>
					<td align="left" valign="top" colspan="2">&nbsp;</td>					
				</tr>
				<tr>
					<td align="left" valign="top" colspan="2">
					<p>
					You have successfully signed up as a <?=SITE_NAME;?> member. <br />
					Your registration information has been sent to the email address you provided.
					<br /><br />
					Lots of skilled professionals are ready to give you immediate advice or assistance on any conceivable topic, 
					live via chat or email. It's fast, affordable, and you'll wonder how you've ever managed without it.
					</p>
					</td>
				</tr>
				<tr>
					<td align="left" valign="top" colspan="2">&nbsp;</td>					
				</tr>
				<tr>
					<td align="left" valign="top" colspan="2">
						<? echo $form->create('User', array('url' => '#')); ?>
						<table align="left" cellpadding="2" cellspacing="2" width="100%">
							<tr>							
								<td align="left" valign="top" width="18%">Search for an expert:</td>
								<td align="left" valign="top" width="22%"><? echo $form->input("User.username",array('label'=>false,'class'=>'InputClass'))?></td>
							  <td align="left" valign="top" width="60%"><input class="loginButton" type="submit" value="Go" /></td>
							</tr>						
							<tr>
								<td align="left" valign="top" colspan="2">Go to <? echo $html->link('homepage','/',array('class'=>'gen_link')); ?></td>					
							</tr>
						</table>
						<? echo $form->end();?>
					</td>					
				</tr>
				<tr>
					<td align="left" valign="top" colspan="2">&nbsp;</td>					
				</tr>
				<tr>
					<td align="left" valign="top" colspan="2">
						Are you an Expert in your field? Share your knowledge with our users - and get paid. <? echo $html->link('Apply now','/users/expert_register',array('class'=>'gen_link')); ?>!
					</td>					
				</tr>
				<tr>
					<td align="left" valign="top" colspan="2">&nbsp;</td>					
				</tr>
			</table>	
		</p>
	</div>
</div>