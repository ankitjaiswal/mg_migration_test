<div align="center">
<table>
	<tbody>
		<tr>
			<td style="font-weight: bold; width: 300px;"> Emailid </td> 
			<td style="font-weight: bold; width: 150px;"> First Name </td> 
			<td style="font-weight: bold; width: 150px;"> Last Name</td>
			<td style="font-weight: bold; width: 100px;"> Role</td>
		</tr>
		<?php foreach ($userslist as $vars){?>
		<tr>
			<td><?php e($vars['User']['username'])?></td>
			<td><?php e($vars['UserReference']['first_name'])?></td>
			<td><?php e($vars['UserReference']['last_name'])?></td>
			<td>
				<?php 
					 if($vars['User']['role_id'] == 1)
					 	 e("Admin");
					 else if ($vars['User']['role_id'] == 2)
					 	 e("Member");
					 else if ($vars['User']['role_id'] == 3)
					 	 e("Client");
					 else if ($vars['User']['role_id'] == 4)
					 	 e("Reader");
				?>
			</td>
            <td>
				<?php 
					 if($vars['User']['status'] == 1)
					 	 e("Active");
					 else if ($vars['User']['status'] == 0)
					 	 e("Deactive");
					 
				?>
			</td>
                      <td>
				<?php 
					 if($vars['User']['access_specifier'] == "publish")
					 	 e("Publish");
					 else if ($vars['User']['access_specifier'] == "draft")
					 	 e("Draft");
					 
				?>
			</td>
		</tr>
		<?php }?>
	</tbody>
</table>
</div>