<?php e($html->css(array('regist_popup'))); ?>
<table>
	<tbody>
		<tr>
			<td>
				<div id="registerationFm" style="width: 512px;">
				<h1>Register as a client</h1>
	        <div class="infobar"><!-- VL 27/12--><!-- VL 2/1/2013-->
		             <p style="float:left; margin-top:12px; font-weight:normal;">To register use </p><p style="float:left; padding-left:10px"><?php e($html->image('linkedin.png',array('alt'=>'Linkedin',"onclick"=>"linkedin('menteePopup');",'style'=>'cursor:pointer;'))); ?></p>
	             </div><!-- VL 27/12-->
	             <div class="infobar noborder"><!-- VL 2/1/2013-->
		          <span>Or register using email id</span>
	              </div>
	             	
					
					<?php e($form->create('User',array('url'=>array('controller'=>'users','action'=>'registration_step2'),'id'=>'planregisterForm'))); ?>	
					<div class="form_row">
						
						<div class="floatL">
							<?php 
							e($form->input('UserReference.first_name',array('class'=>'forminput1','id'=>'popupFirstName','size'=>30,'label'=>false,'div'=>false,'placeholder'=>'First Name','maxlength' => 20)));
							?>		
							<span class="errormsg" id="firstNameErr"></span>
						</div>
						<div class="floatL mr-lt20">
							<?php 
							e($form->input('UserReference.last_name',array('class'=>'forminput1','id'=>'popupLastName','size'=>30,'label'=>false,'div'=>false,'placeholder'=>'Last Name','maxlength' => 20)));
							?>
						</div>
						<div class="clear"></div>
					</div>
					<div class="form_row">
						<?php 
						e($form->input('User.username',array('class'=>'forminput Big','id'=>'popupUserName','label'=>false,'div'=>false,'placeholder'=>'Email','maxlength' => 150)));
						?>
						<div id="UserEmailErr" class="errormsg"></div>
					</div>
					<div class="form_row">
					<?php 
					e($form->password('User.password2',array('class'=>'forminput Big','id'=>'popupPassword','label'=>false,'div'=>false,'placeholder'=>'Password','style'=>'font-family:verdana;')));
					?>	
					<div id="PassLengtherr" class="errormsg"></div>			
					</div>
					<div class="form_row">
					<?php 
					e($form->input('UserReference.zipcode',array('class'=>'forminput Big','id'=>'popupZipcode','label'=>false,'div'=>false,'placeholder'=>'ZIP (if US based) OR Country')));/*VL 2/1/2013*/
					?>
					<div id="popupzipError" class="errormsg"></div>
					</div>
					<div class="submit_bar" style="margin-right:13px;"><!--class name is changed VL 2/1/2013-->
						
					<div class="floatR" style="padding:8px 12px 0 0 !important;">
							<input class="btn" type="submit" value="Register" style="margin-top: 20px; position: relative;" onclick="return validateRegistration();"></input>
						</div>
					</div>
				
					<?php e($form->end()); ?>	
					
				</div>
			</td>
		
		</tr>
	</tbody>
</table>
