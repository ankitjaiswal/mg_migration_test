<?php e($html->css(array('opentip'))); ?>
<?php e($javascript->link(array('opentip-jquery')));?>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script> 


<link href="css/front.css" type="text/css" rel="stylesheet" />
 <?php e($form->create('User', array('url' => array('controller' => 'users', 'action' => 'edit_account'),'type'=>'file','id'=>'EditAccountform' , "name"=>'EditAccountform')));?>     
<div class="profile-header" style="height:280px;">
    <div id="inner-content-wrapper">
	<div id="inner-content-box" class="pagewidth">
	    <div class="Profile_page_container">
		<div  id="profileImage" class="profile-pic" >
      
                <?php if(isset($this->data['UserImage'][0]['image_name']) && $this->data['UserImage'][0]['image_name'] !=''){
                if($this->data['User']['role_id']==Configure::read('App.Role.Mentor'))
                {
                    $image_path = MENTORS_IMAGE_PATH.DS.$this->data['User']['id'].DS.$this->data['UserImage'][0]['image_name'];
                ?>
               <?php e($html->image($image_path,array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'], 'height'=>'200','width'=>'200')));?>
               <?php                   
                }
                else
                {
                    $image_path = MENTEES_IMAGE_PATH.DS.$this->data['User']['id'].DS.$this->data['UserImage'][0]['image_name']; ?>
                
                <?php e($html->image($image_path,array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'] ,'height'=>'200', 'width'=>'200'))); ?>
                <?php                   
                }
                 e($form->hidden($this->data['UserImage'][0]['image_name'],array('value'=>$this->data['UserImage'][0]['image_name'],'id'=>'image_name')));
                }
               else
               {
                e($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'],'style' => 'padding-bottom:51px;')));
                e($form->hidden('profile.png',array('value'=>'profile.png','id'=>'image_name')));
            }?>	    
           
	     </div>

		<div style="padding-top:200px;"><?php e($this->element('mentorsuser/add_image')); ?></div>

              <div class="profile-bio">
		<div>
		   <div class="profile-name">
                <?php e($form->input('UserReference.first_name', array('div'=>false, 'label'=>false, "class" => "forminput1", "style"=>"margin-bottom:1px;" , "placeholder" => "First name")));?> &nbsp; 
                <?php e($form->input('UserReference.last_name', array('div'=>false, 'label'=>false, "class" => "forminput1", "style"=>"margin-bottom:1px; clear:both;" , "placeholder" => "Last name")));?>
                 </div>
              
		    <div class="profile-city">
                 <?php e($form->input('UserReference.zipcode', array('div'=>false,'maxlength'=>5, 'label'=>false, "class" => "forminput1", "style"=>"margin-bottom:1px;" , "placeholder" => "ZIP")));?>
                  </div>
                  
                  
		     <div class="profile-headline"><span style="margin-left:270px; color:#fff; font-size: 10px; font-weight: normal;">(<span id="headlinenoOfChars" style="padding-left: 0px; color: #fff; font-size: 10px; font-weight: normal;"><?php e(140 - strlen($this->data['UserReference']['linkedin_headline']))?></span> characters remaining)</span>
                  <?php e($form->input('UserReference.linkedin_headline', array('div'=>false, 'label'=>false,"id"=>"link-headline", "class" => "forminput1", "style"=>"width:430px;margin-top:1px;" , "placeholder" => "Your professional headline")));?>
                   </div>
		    
		 </div>
               </div>
          
           </div>
	</div>
    </div>
</div>


<div class="profile-navbar">
    <div id="inner-content-wrapper">
	<div id="inner-content-box" class="pagewidth">
	    <div class="Profile_page_container">
		<div class="profile-nav">

                         <?php
					$socialCount=0;
					$linkedinURL = '';
                                   $twitterURL ='';
                                   $googleURL ='';
                                   $facebookURL ='';
                                   $youtubeURL ='';
					while(count($this->data['Social'])>$socialCount)
					{
					
					if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'http'))!==false)
						{
							$link=$this->data['Social'][$socialCount]['social_name'];
						}
						else
						{
							$link="https://".$this->data['Social'][$socialCount]['social_name'];
						}
						
						$icon = $this->General->getIcon($this->data['Social'][$socialCount]['social_name']);
						if($icon == 'media/linkedin.png')
							$linkedinURL = $this->data['Social'][$socialCount]['social_name'];
                                          if($icon == 'media/twitter.png')
							$twitterURL = $this->data['Social'][$socialCount]['social_name'];
                                          if($icon == 'media/Google+.png')
							$googleURL = $this->data['Social'][$socialCount]['social_name'];
                                          if($icon == 'media/facebook.png')
							$facebookURL = $this->data['Social'][$socialCount]['social_name'];
						if($icon == 'media/youtube.png')
							$youtubeURL = $this->data['Social'][$socialCount]['social_name'];
						$socialCount++;
						}
				     ?>

		<ul>
		    <li id="bg1" class="profile-nav-current"><a href="#profile"  onclick="tog1();">Profile</a></li>
		    <li id="bg2" class="profile-hover-current"><a href="#case_studies" onclick="tog2();">Case Studies</a></li>
		    <li id="bg4" class="profile-hover-current"><a href="#activity" onclick="tog4();">Activity</a></li>
                  <li id="bg5" class="profile-hover-current"><a href="#testimonials" onclick="tog5();">Testimonials</a></li>
		    
		</ul>
		</div>


		<div class="profile-socialmedia">
		<ul>
		    <?php if($linkedinURL !=''){
                   if(($pos =strpos($linkedinURL,'http'))!==false)
						{
							$link=$linkedinURL;
						}
						else
						{
							$link="https://".$linkedinURL;
						}?>
		    <li><a href="<?php e($link);?>" target="_blank"><img src="<?php e(SITE_URL)?>/imgs/linkedin.png" style="padding-bottom:6px;"></a></li>
                  <?php }?>
                  <?php if($twitterURL !=''){?>
		    <li>&nbsp;&nbsp;<a href="<?php e($twitterURL);?>" target="_blank"><img src="<?php e(SITE_URL)?>/imgs/twitter.png"></a></li>
                   <?php }?>
                  <?php if($googleURL !=''){?>
		    <li><a href="<?php e($googleURL);?>" target="_blank"><img src="<?php e(SITE_URL)?>/imgs/google.png"></a></li>
                  <?php }?>
                  <?php if($facebookURL !=''){?>
		    <li><a href="<?php e($facebookURL);?>" target="_blank"><img src="<?php e(SITE_URL)?>/imgs/facebook.png"></a></li>
                  <?php }?>
                   <?php if($youtubeURL !=''){?>
		    <li><a href="<?php e($youtubeURL);?>" target="_blank"><img src="<?php e(SITE_URL)?>/imgs/youtube.png"></a></li>
                  <?php }?>


                  <?php if(($this->data['UserReference']['business_website']) !=''){
                     $link ='';
                     $url ='';
                       if(($pos =strpos($this->data['UserReference']['business_website'],'http'))!==false)
						{
							$link=$this->data['UserReference']['business_website'];
						}
						else
						{
							$link="https://".$this->data['UserReference']['business_website'];
						}
                                         ?>
                              
                                     

		    <li class="profile-socialmedia-url"><a href="<?php e($link);?>" target="_blank">Website</a></li>
                 
                   <?php }?>

		</ul>
		</div>
	    </div>
	</div>
    </div>
</div>

    <div id="inner-content-wrapper">
	<div id="inner-content-box" class="pagewidth">
	    <div class="Profile_page_container">
                  <div class="profile-sidebar" id="sidebar" style="display:block;">
		    <div class="profile-sidebar-shim"></div>
		    <div class="profile-sidebar-border"></div>
		    <div class="profile-sidebar-icons">
                      
                         <div class="socials"> 
                             <?php
                                    $url ='';
					$socialCount=0;
                                    if(($pos =strpos($this->data['UserReference']['business_website'],'http'))!==false)
						{
							$url=$this->data['UserReference']['business_website'];
						}
						else
						{
							$url="https://".$this->data['UserReference']['business_website'];
						}
                               
					
					while(count($this->data['Social'])>$socialCount)
					{
					
					if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'http'))!==false)
						{
							$link=$this->data['Social'][$socialCount]['social_name'];
						}
						else
						{
							$link="https://".$this->data['Social'][$socialCount]['social_name'];
						}
						
						$icon = $this->General->getIcon($this->data['Social'][$socialCount]['social_name']);
						if($icon != 'media/linkedin.png' && $icon != 'media/linkedin.png' && $icon != 'media/twitter.png' && $icon != 'media/Google+.png' && $icon != 'media/facebook.png' && $icon != 'media/youtube.png'  && $url != $link)
						{	
						e($html->link($html->image($icon,array('alt'=>$this->data['Social'][$socialCount]['social_name'] , 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						}
						$socialCount++;
						}
				?>
   
		           </div>
       
                    <a id='edit_btn' style="cursor:pointer" ><span>Edit social links</span></a>
		   
                 </div>
 

                          <?php
                    $socialCount=0;
                    while(count($this->data['Social'])>$socialCount)
                    {
                        if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'http'))!==false)
                        {
                            $link=$this->data['Social'][$socialCount]['social_name'];
                        }
                        else
                        {
                            $link="http://".$this->data['Social'][$socialCount]['social_name'];
                        }
                        
                        $icon = $this->General->getIcon($this->data['Social'][$socialCount]['social_name']);
                       $socialCount++;
                    }
                ?>
                 

                    

             
                   

               <div id="mentor-detail-right" style="display:block;">
                
                    <div id="edit_box" style="display:none;">
                    <div class="edit_colum">
                        
                        <?php
                            $val='';
                            if(isset($this->data['Social'][0]['social_name']))
                            {
                                $val=$this->data['Social'][0]['social_name'];
                            }
                        ?>
                        <?php e($form->input('',array('label'=>false,'div'=>false , "name"=>'link[]' , "id"=>'link[]' , "value"=>$val,"style"=>"height:20px;", "placeholder" =>"www.linkedin.com"))); ?>

                        <?php e($html->link($html->image('media/minus1.png',array('alt'=>'skype'  ,"class"=>"padding_right")),'javascript:act(0);',array('escape'=>false)));?>

                       
                        
                        <?php
                            $val='';
                            if(isset($this->data['Social'][1]['social_name']))
                            {
                                $val=$this->data['Social'][1]['social_name'];
                            }
                        ?>
                        <?php e($form->input('',array('label'=>false,'div'=>false , "name"=>'link[]' , "id"=>'link[]' , "value"=>$val,"style"=>"height:20px;","placeholder" =>"www.twitter.com"))); ?>
                            <?php e($html->link($html->image('media/minus1.png',array('alt'=>'skype'  )),'javascript:act(1);',array('escape'=>false)));?>
                         
                       
                    
                        
                        <?php
                            $val='';
                            if(isset($this->data['Social'][2]['social_name']))
                            {
                                $val=$this->data['Social'][2]['social_name'];
                            }
                        ?>
                            <?php e($form->input('',array('label'=>false,'div'=>false , "name"=>'link[]' , "id"=>'link[]' , "value"=>$val,"style"=>"height:20px;"))); ?>
                            <?php e($html->link($html->image('media/minus1.png',array('alt'=>'skype'  ,"class"=>"padding_right")),'javascript:act(2);',array('escape'=>false)));?>

                        
                            <?php
                            $val='';
                            if(isset($this->data['Social'][3]['social_name']))
                            {
                                $val=$this->data['Social'][3]['social_name'];
                            }
                        ?>
                            <?php e($form->input('',array('label'=>false,'div'=>false , "name"=>'link[]' , "id"=>'link[]' , "value"=>$val,"style"=>"height:20px;"))); ?>
                            
                            <?php e($html->link($html->image('media/minus1.png',array('alt'=>'skype'  )),'javascript:act(3);',array('escape'=>false)));?>

                      
                    
                    
                    
                            <?php
                            $val='';
                            if(isset($this->data['Social'][4]['social_name']))
                            {
                                $val=$this->data['Social'][4]['social_name'];
                            }
                        ?>
                        <?php e($form->input('',array('label'=>false,'div'=>false , "name"=>'link[]' , "id"=>'link[]' , "value"=>$val,"style"=>"height:20px;"))); ?>

                            <?php e($html->link($html->image('media/minus1.png',array('alt'=>'skype' ,"class"=>"padding_right" )),'javascript:act(4);',array('escape'=>false)));?>
                       
                       
                            <?php
                            $val='';
                            if(isset($this->data['Social'][5]['social_name']))
                            {
                                $val=$this->data['Social'][5]['social_name'];
                            }
                        ?>
                            <?php e($form->input('',array('label'=>false,'div'=>false , "name"=>'link[]' , "id"=>'link[]' , "value"=>$val,"style"=>"height:20px;"))); ?>
                            
                            <?php e($html->link($html->image('media/minus1.png',array('alt'=>'skype'  )),'javascript:act(5);',array('escape'=>false)));?>

                        
                        
                  
                    
                   
                            <?php
                            $val='';
                            if(isset($this->data['Social'][6]['social_name']))
                            {
                                $val=$this->data['Social'][6]['social_name'];
                            }
                        ?>
                        <?php e($form->input('',array('label'=>false,'div'=>false , "name"=>'link[]' , "id"=>'link[]' , "value"=>$val,"style"=>"height:20px;"))); ?>

                            <?php e($html->link($html->image('media/minus1.png',array('alt'=>'skype' ,"class"=>"padding_right" )),'javascript:act(6);',array('escape'=>false)));?>
                         
                       
                        
  
                      
                    </div>
                    </div>


               
                   
                    <div class="rates" >
                       
              
                    <h4><span style="font-family:'proximanova semibold',Ubuntu;">Business website</span></h4>
                     <p style="border-bottom: thin solid #dedede;">
                        <?php e($form->input('UserReference.business_website',array('div'=>false,'label'=>false, "class" => "smallest_paypal","style"=>"width:198px;height:20px;",'placeholder'=>'Enter URL here'))); ?>
                       
                     <div style="margin-top:5px;">
                     <h4><span style="font-family:'proximanova semibold',Ubuntu;">Intro video (embed link)</span></h4>
                     <p style="border-bottom: thin solid #dedede;">
                        <?php e($form->input('UserReference.intro_link',array('div'=>false,'label'=>false, "class" => "smallest_paypal","style"=>"width:198px;height:20px;",'placeholder'=>'Enter embed link here'))); ?>
                      </div> 

                    <h1><span style="font-family:'proximanova semibold',Ubuntu;font-size: large;">Fee</span></h1>
                    
                    
                    <div class="rates_container">
	                    <div class="floatL">$ <?php e($form->input('UserReference.fee_regular_session', array('div'=>false, 'label'=>false, "class" => "smallest", "style"=>"width:35px;height:28px;top:-25px;" , "onblur"=>"doSomething()" , "placeholder" => "")));?> / 
	                    <?php $options = array('1' => 'Hour', '2' => 'Month', '3' => 'Engagement', '4' => 'Day'); ?>
	                    </div>
                   
	                     <div class="timzoneselectbox_cont" >
	                    <?php echo $form->select('UserReference.regular_session_per',$options,null,array('div' =>false,'class'=>'select','style' => 'color:#292929;padding:5px 0;width:70px;','empty' => false,'selected'=>$this->data['UserReference']['regular_session_per']));
	                    ?>
	                    </div>
                    </div>
                    
                    <div style="clear:both;"></div>
                    
                    <div id="paypalacc" class="divclass" style="border-bottom: thin solid #dedede;"<?php if($this->data['UserReference']['fee_regular_session']>0 or $this->data['UserReference']['application_5_fee']==1 ) { ?> style="display:block;" <?php }else { ?> style="display:none;" <?php } ?> ><!--display:none; instead of  display:block; when no need  VL 25/12-->
                        <?php e($html->image('media/paypal.png',array('alt'=>'Insert Paypal Account'))); ?>
                        <?php e($form->input('UserReference.paypal_account', array('div'=>false, 'label'=>false, "class" => "smallest_paypal",   "style"=>"width:198px;height:35px;" , "placeholder" => "Enter your paypal account email ID")));?>
                        

                    </div>
                   
                    
                    <h1><span style="font-family:'proximanova semibold',Ubuntu;font-size: large;">Availability</span></h1>
                    <p style="border-bottom: thin solid #dedede;">
                        <?php e($form->textarea('Availablity.day_time', array('div'=>false, 'label'=>false, "class" => "smallest",  "id"=>"availablity", "style"=>"width:201px;height:50px;vertical-align:middle; padding-top:10px; padding-left:5px;" , "placeholder" => "e.g. Business hours (ET). By appointment")));?>
                    </p>
                    
                    <h1><span style="font-family:'proximanova semibold',Ubuntu;font-size: large;">Communication</span></h1>
                     <?php 
                          $modeTypeFaceToFace=$modeTypePhone=$modeTypeVideo=false ;
			              $modeValue1=$modeValue2=$modeValue3='';
			  if(isset($this->data['Communication'][0]['mode_type']))
			  {
			  		
					if($this->data['Communication'][0]['mode_type']=='face_to_face')
				{
						$modeTypeFaceToFace=true;
			  			$modeValue1=$this->data['Communication'][0]['mode'];
					}
					else if($this->data['Communication'][0]['mode_type']=='phone')
						{
						  $modeTypePhone=true;
			  			  $modeValue2=$this->data['Communication'][0]['mode'];
			  			}
			  			
			  			else
			  			{
			  			   $modeTypeVideo=true;
			  			   $modeValue3=$this->data['Communication'][0]['mode'];
					 }
					
				
				
			}
			  if(isset($this->data['Communication'][1]['mode_type']))
			  {
			  		
					if($this->data['Communication'][1]['mode_type']=='face_to_face')
				{
						$modeTypeFaceToFace=true;
			  			$modeValue1=$this->data['Communication'][1]['mode'];
					}
					else if($this->data['Communication'][1]['mode_type']=='phone')
						{
						   $modeTypePhone=true;
			  		       $modeValue2=$this->data['Communication'][1]['mode'];
			  			}
			  			
			  			else
			  			{
			  			   $modeTypeVideo=true;
			  			   $modeValue3=$this->data['Communication'][1]['mode'];
					 }
					
				
				
			}
			 if(isset($this->data['Communication'][2]['mode_type']))
			  {
			  		if($this->data['Communication'][2]['mode_type']=='face_to_face')
					{
						$modeTypeFaceToFace=true;
			  			$modeValue1=$this->data['Communication'][2]['mode'];
					}
						else if($this->data['Communication'][2]['mode_type']=='phone')
						{
						   $modeTypePhone=true;
			  		       $modeValue2=$this->data['Communication'][2]['mode'];
			  			}
			  			
			  			else
			  			{
			  			   $modeTypeVideo=true;
			  			   $modeValue3=$this->data['Communication'][2]['mode'];
					 }
					
					
			  }
			
			
				 ?>
			
                        
                        <div class="styled-form">
                          <?php e($form->input('facetofacemode_type', array('type'=>"checkbox" ,'div'=>false, 'label'=>false,  "id"=>"a3" ,"value" => "face_to_face" , "placeholder" => "" ,  'checked'=>$modeTypeFaceToFace  )));?>
                            
                            <label for="a3">In person</label>
                            
                            <p> 
                             <?php e($form->input('facetofacemode', array('div'=>false, 'label'=>false, "class" => "smallest", "value" => $modeValue1 ,  "style"=>"width:198px;height:35px;" , "placeholder" => "e.g. New York, NY")));?>
                            </p>
                             <?php e($form->input('videomode_type', array('type'=>"checkbox" ,'div'=>false, 'label'=>false,  "id"=>"a5" ,"value" => "video" , "placeholder" => "Skype, Google Hangout" ,  'checked'=>$modeTypeVideo  )));?>
                            
                            <label for="a5">Online</label>
                             
                           <p > <?php e($form->input('videomode', array('div'=>false, 'label'=>false, "class" => "smallest", "value" => $modeValue3 , "style"=>"width:198px;height:35px;" , "placeholder" => "e.g. Skype, Google Hangout")));?> </p>   
                            <?php e($form->input('phonemode_type', array('type'=>"checkbox" ,'div'=>false, 'label'=>false,  "id"=>"a4" ,"value" => "phone" , "placeholder" => "Business phone number" ,  'checked'=>$modeTypePhone  )));?>
                            
                            <label for="a4">Phone</label>
                            
                            <p > 
                             <?php e($form->input('phonemode', array('div'=>false, 'label'=>false, "class" => "smallest", "value" => $modeValue2 ,  "style"=>"width:198px;height:35px;" , "placeholder" => "Business phone number")));?>
                            </p>
                            
                        </div>
                        
                        
                
                
                
                </div>
            </div>  
            
		  
		    
  </div>
         
           <div  id="verticalline" style="display:none;border-left: 2px solid #dedede;height: 50%;top: 45px;position: absolute;margin-left:718px;">
	           	<div id="Apply" class="apply-button-search" role="Apply" style="float: left;margin-bottom: 5px;" >
                       <input id="apply" type="submit" value="Publish Profile" style="width:200px !important; margin-top: 10px;margin-left:10px;" class="submitProfile">
                    </div>
             </div>
               
		
	    <div class="profile-body" id="profilebody"style="width: 674px; margin-bottom: 30px; display: block;">

                 <?php if($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==Configure::read('App.Role.Mentor'))
				{echo $this->element('mentorsuser/mentor_dash_edit');}?>

		<img class="profile-section-icon" src="<?php e(SITE_URL)?>/imgs/mg-profile-expertise.png"><h2>Areas of expertise</h2>
		<p><?php e($form->input('UserReference.area_of_expertise', array('div'=>false, 'label'=>false, "class" => "forminput",  "style"=>"width:598px;position: relative;left: -2px;" ,"placeholder" => "Up to 3 keywords, separated by commas")));?></p>
	    	
               <img class="profile-section-icon" src="<?php e(SITE_URL)?>/imgs/mg-profile-profile.png"><h2>Desired client profile</h2>
		<p><?php e($form->input('UserReference.desire_mentee_profile', array('div'=>false, 'label'=>false, "class" => "forminput", "style"=>"width:598px;position: relative;left: -2px;" , "placeholder" => "e.g. Mid-sized Healthcare organizations")));?> </p>
                    
		<img class="profile-section-icon" src="<?php e(SITE_URL)?>/imgs/mg-profile-summary.png"><h2>Professional summary<span style="float:right; color: rgb(41, 41, 41); margin-right:12px; font-size: 12px; font-weight: normal;">(<span id="noOfChars" style="padding-left: 0px; color: rgb(41, 41, 41); font-size: 12px; font-weight: normal;"><?php e(3000 - strlen($this->data['UserReference']['background_summary']))?></span> characters remaining)</span></h2>
		<p><?php e($form->textarea('UserReference.background_summary', array('rows'=>6,'div'=>true, 'label'=>false, 'id'=>'text-area' , "class" => "forminput", "style"=>"width:598px;position: relative;left: -2px;font-family: Ubuntu, arial, vardana; font-size: 14px;" , "placeholder" => "Suggested format: Start with an overview. Then, explain your process and list past clients. In the end, mention related work, training & publications (3,000 characters limit)")));?></p>
              <br/>      
		<img class="profile-section-icon" src="<?php e(SITE_URL)?>/imgs/mg-profile-engagement.png"><h2>Engagement overview<span style="float:right; color: rgb(41, 41, 41); margin-right:12px; font-size: 12px; font-weight: normal;">(<span id="noOfChar" style="padding-left: 0px; color: rgb(41, 41, 41); font-size: 12px; font-weight: normal;"><?php e(1000 - strlen($this->data['UserReference']['feeNSession']))?></span> characters remaining)</span></h2>
		<p><?php  e($form->textarea('UserReference.feeNSession', array('rows'=>6,'div'=>true,'label'=>false,'id'=>'textareafee', "class" => "forminput", "style"=>"width:614px;position: relative;left: -2px;font-family: Ubuntu, arial, vardana; font-size: 14px;" , "placeholder" => "What's included in a typical engagement? Indicate duration, methodology and pricing")));?></p>
               <br/>  
		<img class="profile-section-icon" src="<?php e(SITE_URL)?>/imgs/mg-profile-clients.png"><h2>Clients</h2>
		<p><?php e($form->textarea('UserReference.past_clients', array('rows'=>6,'div'=>true, 'label'=>false, 'id'=>'past-clients' , "class" => "forminput", "style"=>"width:614px;position: relative;left: -2px;height:100px;font-family: Ubuntu, arial, vardana; font-size: 14px;", "placeholder"=>"List your past and present clients here. If a client name is confidential, indicate industry and size")));?></p>
               <br/> 
              <img class="profile-section-icon" src="<?php e(SITE_URL)?>/imgs/mg-profile-profile.png"><h2>Accepting new clients?</h2>
              <p class="styled-form" style="line-height:25px;">
              <?php e($form->radio('UserReference.accept_application', array('Y'=>'Yes','N'=>'No'),array('div'=>false,'label'=>true,'legend'=>false)));?></p>
                        
                <div id="Apply" role="Apply" style="float: right;margin-right: 23px;margin-bottom: 30px;" >
                        	<?php e($html->link('Cancel',SITE_URL.'users/my_account',array('class'=>'reset','style'=>"float: left; margin-top: 10px; margin-right: 30px;"))); ?>
                        	
                        	<input id="apply" type="submit" value="Save"  class="submitProfile">
                        </div>     
                         
                         
	    </div>

           <?php // for case study{?>
                <div id="divParent" style="width: 674px; display:none; padding-top: 15px;">	
                	<?php 
                	$Counter = 1;
                	foreach(array_reverse($this->data['Case_Study']) as $case_study)
					{		
					?>
					<p style="margin-top: 10px">
					<?php echo $case_study['title'];
					$Counter++;
					?>
					<span>
						<a href="<?php echo SITE_URL?>members/edit_casestudy/<?php echo $case_study['id']?>">Edit</a> &nbsp; 
						<a href="javascript:delCaseStudy(<?php echo $case_study['id']?>)">Delete</a>
					</span>
					</p>
					<br/>
					<?php }?>
                             <div class="apply-button-search" style="float: right; margin-bottom: 5px;margin-right:-10px;">
					<input class="profilebtn" type="button" style="width:211px !important;margin-top: 18px;" value="Add Case Study" onclick="window.location.href='<?php echo SITE_URL."members/add_casestudy/";?>';">
				 </div>
		  </div>


               <?php //case study end }?>

              <?php //for activity {?> 
				
                 	     <div id="feedDiv" style="width: 674px; display:none; padding-top: 15px;">	
					<div id="feedFetchDiv" style="display: inline-block;">
						<div class="expertise" style="margin-top: 15px;">
			                     <p><input id="feed_url" class="inpT" type="text" style="width:651px;" placeholder="Paste a URL..." name="data[url]"/></p>
			                     </div>
			                     <div class="editSUB" style="margin-top: 0px;">
					       <input class="feed_btn" type="button" style="width:151px !important;" value="Fetch Content" onclick="fetch_data();">
						</div>
					</div>

					<div id="feedSaveDiv" style="display:none;">
						<div class="expertise" style="margin-top: 15px;">
			                     <h1>Title <span style="float:right; font-weight: normal;"></h1>
			                      <p><input id="feed_title" class="inpT" type="text"style="width:651px;" placeholder="Title" name="data[title]" value=""/></p>
			                     </div>

			                     <div class="expertise">
			                      <p><textarea id="feed_summary" class="txtAR" rows="4" style="height:100px;width:659px;" type="text"  placeholder="Summary" name="data[summary]" value=""/></textarea></p>
			                     </div>

			                    <div class="editSUB" style="margin-top: 0px;">
                                         <a href="<?php echo SITE_URL?>users/edit_account/OpenActivityTab:1">Cancel</a>
                                         <input class="feed_save_btn" type="button" style="width:150px !important; margin-top: -11px;" value="Save" onclick="save_feed_data();">
	                                  </div>

					</div>

					<div id="feedDummyDiv"></div>
					
					<div id="allFeeds"style="margin-top: 40px;">
	                	           <?php 
	                	            $Counter = 1;
	                	            foreach(array_reverse($this->data['Feed']) as $feed)
						{		
						?>
                                         <?php if (strpos($feed['url'], "www")!=false){
                                     $pos = strpos($feed['url'], '.');
                                   
                                      $pos1 = strpos($feed['url'], '/');
                                      $pos2 = strpos($feed['url'],'/', $pos1 + strlen('/'));
                                      $pos3 = strpos($feed['url'],'/', $pos2 + strlen('/'));
                                      $final =$pos3 - $pos;
                                      $url = substr($feed['url'],$pos+1,$final-1);
                                       }
                                      else{
                                          $pos1 = strpos($feed['url'], '/');
                                         $pos2 = strpos($feed['url'],'/', $pos1 + strlen('/'));
                                        $pos3 = strpos($feed['url'],'/', $pos2 + strlen('/'));
                                        $final =$pos3 - $pos2;
                                      $url = substr($feed['url'],$pos2+1,$final-1);
                                      }
                                    ?>
                                          <span style ="color: #999; font-size:10px;font-weight:600;"><?php echo(strtoupper($url));?></span>
						<p id="feed<?php echo $feed['id']?>" style="margin-top:10px">
						<span style="font-weight:bold;font-size:15px;color:#2a2a2a;"><?php echo $feed['title'];
						$Counter++;
						?></a></span>
						<span>
							&nbsp;&nbsp;<a href="javascript:delFeed(<?php echo $feed['id']?>)">Delete</a>
						</span>
						<p id="summary<?php echo $feed['id']?>" >
						<?php echo $feed['summary'];
						?>
						</p>
						</p>
						<br/>
						<?php }?>

					</div>
				</div>
                 
                     <?php // activity end }?>
                  
                    <?php //for $testimonial {?> 

                    <div id="testimonialDiv" style="width: 674px;height: 100%;display:none; padding-top: 15px;">	
		     <div id="allTestimonials">
                                 
                	<?php 
                	foreach(array_reverse($this->data['Testimonial']) as $testimonial)
					{	   
                                    	
					?>
                                  
                                 <div class="expertise" style="padding-top:15px;margin-bottom:50px;width:674px;border-bottom:1px solid #bcbcbc;float:right;">  
                         
					
                              
					<p id="testimonial<?php echo $testimonial['id']?>" style="padding-top:20px;">
					<?php echo $testimonial['testimonial_detail'];?>
					</p>
                                     <p id="testimonial_client<?php echo $testimonial['id']?>" style="font-size:14px;font-weight:normal;" >
                                    
                                   <?php if ($testimonial['client_name'] == '' && $testimonial['client_organization'] != ''){?>
                                   
                                      <span style="float:right;"><?php echo $testimonial['client_organization']?></span>
                                 
                                    <?php } elseif ($testimonial['client_designation'] != '' && $testimonial['client_organization'] == ''){?>
                                  
                                 <span style="float:right;"><?php echo $testimonial['client_name']?>,&nbsp;<?php echo $testimonial['client_designation']?></span>
                                 
                                 <?php } else if( $testimonial['client_organization'] != '' && $testimonial['client_designation'] == ''){?>
                                  
                                 <span style="float:right;"><?php echo $testimonial['client_name']?></span><br><span style="float:right;"><?php echo $testimonial['client_organization']?></span>
                                  
                                 <?php }else if($testimonial['client_organization'] == '' && $testimonial['client_designation'] == ''){?>
                                    <span style="float:right;"><?php echo $testimonial['client_name']?></span>
                                  
                                <?php }else{?>
                                      <span style="float:right;"> <?php echo $testimonial['client_name']?>,&nbsp;<?php echo $testimonial['client_designation']?></span><br><span style="float:right;"><?php echo $testimonial['client_organization']?></span>
                              
                                    <?php }?>
                                 
                                     </p>
                               
                             </div>
					
					
					<?php }?>
		          </div>
	               </div>
                     <?php //testimonial end }?>

             
	    
	    
	</div>
    </div>
</div>

<?php e($form->end()); ?>
 

<?php
if(isset($OpenCaseStudiesTab)){?>
	<script type="text/javascript">
        document.getElementById('profile-sidebar').style.display="none";
        document.getElementById('verticalline').style.display="block"; 
		tog2();
	</script>
<?php
}?>
   <?php
if(isset($OpenActivityTab)){?>
	<script type="text/javascript">
	 document.getElementById('profile-sidebar').style.display="none";
        document.getElementById('verticalline').style.display="block"; 	
     tog4();
     
  
	</script>
<?php
}?>
<script type="text/javascript">
jQuery(document).ready(function(){
                  
                   var qText = jQuery("#textareafee").val();
    	        jQuery("#noOfChar").html(1000 - qText.length);
    	
		      function updateCount ()
		    {
		        var qText = jQuery("#textareafee").val();

		       if(qText.length < 1000) {
		           jQuery("#noOfChar").html(1000 - qText.length);
		       } else {
		           jQuery("#noOfChar").html(0);
		           jQuery("#textareafee").val(qText.substring(0,1000));
		       }
		    }

		    jQuery("#textareafee").keyup(function () {
		        updateCount();
		    });
		    jQuery("#textareafee").keypress(function () {
		        updateCount();
		    });
		    jQuery("#textareafee").keydown(function () {
		        updateCount();
		    });
		
                  var qTexts = jQuery("#text-area").val();
    	            jQuery("#noOfChars").html(3000 - qTexts.length);
    	
		    function updateCounts ()
		    {
		        var qTexts = jQuery("#text-area").val();

		       if(qTexts.length < 3000) {
		           jQuery("#noOfChars").html(3000 - qTexts.length);
		       } else {
		           jQuery("#noOfChars").html(0);
		           jQuery("#text-area").val(qTexts.substring(0,3000));
		       }
		    }

		    jQuery("#text-area").keyup(function () {
		        updateCounts();
		    });
		    jQuery("#text-area").keypress(function () {
		        updateCounts();
		    });
		    jQuery("#text-area").keydown(function () {
		        updateCounts();
		    });

                    var headTexts = jQuery("#link-headline").val();
    	            jQuery("#headlinenoOfChars").html(140 - headTexts.length);
    	
		 function updateCountss ()
		    {
		        var headTexts = jQuery("#link-headline").val();

		       if(headTexts.length < 140) {
		           jQuery("#headlinenoOfChars").html(140 - headTexts.length);
		       } else {
		           jQuery("#headlinenoOfChars").html(0);
		           jQuery("#link-headline").val(headTexts.substring(0,140));
		       }
		    }

		    jQuery("#link-headline").keyup(function () {
		        updateCountss();
		    });
		    jQuery("#link-headline").keypress(function () {
		        updateCountss();
		    });
		    jQuery("#link-headline").keydown(function () {
		        updateCountss();
		    });
});
</script>
    <script type="text/javascript">
    //validation
     
function delFeed(feed_id){

		jQuery(document).ready(function(){
			jQuery.ajax({
				url:SITE_URL+'members/del_feed',
				type:'post',
				dataType:'json',
				data:'feed_id='+feed_id,
				success:function(res){
					if(res.id !=''){

						var feed_title = document.getElementById('feed'+res.id).style.display="none";  
						var feed_summary = document.getElementById('summary'+res.id).style.display="none"; 

					}else{
						alert("Failure!!!");
					}
				}
			});
		});
    }
     
    function save_feed_data(){
		var feed_url = jQuery("#feed_url").val();
		var str = jQuery("#feed_title").val();
		
            
                if(str.indexOf("|") !=-1) {
                  var split = str.split("|"); 
                  var feed_title = split[0];
                 }
               else
                  {
                var feed_title = jQuery("#feed_title").val();
                 }
              var feed_summary = jQuery("#feed_summary").val();

		jQuery(document).ready(function(){
			jQuery.ajax({
				url:SITE_URL+'members/save_feed',
				type:'post',
				dataType:'json',
				data:'feed_url='+feed_url + '&feed_title=' + feed_title + '&feed_summary=' + feed_summary,
				success:function(res){
					if(res.id !=''){

						var questionDiv = jQuery('#allFeeds');

						var title = '<p id="feed'+res.id+'" style="margin-top: 10px"> <span style="font-weight:bold;font-size:15px;color:#2a2a2a;">';
						title += res.title;
						title +='</span><span>&nbsp;&nbsp;&nbsp;<a href="javascript:delFeed('+res.id+')">Delete</a>';
						title +='</span><p id="summary'+res.id+'">';
						title += res.summary;
						title += '</p></p>';

						jQuery(title).prependTo(questionDiv);
												
						document.getElementById('feedFetchDiv').style.display="inline-block";  
						document.getElementById('feedSaveDiv').style.display="none";  
						jQuery("#feed_url").val('');

					}else{
						alert("Failure!!!");
					}
				}
			});
		});
		
   	}
       	
   	function fetch_data(){
		var feed_url = jQuery("#feed_url").val();

		jQuery(document).ready(function(){
			jQuery.ajax({
				url:SITE_URL+'members/activity_feed',
				type:'post',
				dataType:'json',
				data:'feed_url='+feed_url,
				success:function(res){
					if(res.title !=''){
						document.getElementById('feedFetchDiv').style.display="none";  
						document.getElementById('feedSaveDiv').style.display="inline-block";  
                         
						jQuery('#feed_title').val(res.title);
                                          jQuery('#feed_summary').val(res.summary);
                                          
					}else{
						alert("Failure!!!");
					}
				}
			});
		});
		
   	}
    
    window.onload=function(){
	
	var a = document.createElement('a');
	a.href = document.URL;
	if(a.hash=='#profile' || a.hash=='')
	{
	}

       else if(a.hash=='#case_studies')
	{
		tog2(); 
	}
 
        else if(a.hash=='#activity')
	{
		tog4(); 
	}
    
	else
	{
		tog5(); 
	}
}
    
    jQuery(document).ready(function(){


	    
             //Initialising all tooltips
		var myImagetip = new Opentip("#profileImage", "", {style: "myErrorStyleCreation"});
		var myFNametip = new Opentip("#UserReferenceFirstName", "", {style: "myErrorStyleCreation"});
		var myLNametip = new Opentip("#UserReferenceLastName", "", {style: "myErrorStyleCreation"});
		var myZiptip = new Opentip("#UserReferenceZipcode", "", {style: "myErrorStyleCreation"});
		var myTexttip = new Opentip("#text-area", "", {style: "myErrorStyleCreation"});
		var myRegularttip= new Opentip("#UserReferenceFeeRegularSession", "", {style: "myErrorStyleBottomCreation"});
		var myPaypaltip= new Opentip("#UserReferencePaypalAccount", "", {style: "myErrorStyleBottomCreation"});
		var myAvailabilitytip = new Opentip("#availablity", "", {style: "myErrorStyleBottomCreation"});
		var myExpertisetip = new Opentip("#UserReferenceAreaOfExpertise", "", {style: "myErrorStyleCreation"});
		var myFModetip = new Opentip("#UserFacetofacemode", "", {style: "myErrorStyleBottomCreation"});
		var myPModetip = new Opentip("#UserPhonemode", "", {style: "myErrorStyleBottomCreation"});
		var myVModetip = new Opentip("#UserVideomode", "", {style: "myErrorStyleBottomCreation"});
              var myDesiredtip = new Opentip("#UserReferenceDesireMenteeProfile", "", {style: "myErrorStyleCreation"});
		var myFeeNSessiontip= new Opentip("#textareafee", "", {style: "myErrorStyleBottomCreation"});
              var myLinkheadline= new Opentip("#link-headline", "", {style: "myErrorStyleCreation"});
		//End
        
             jQuery(".submitProfile").click(function(){

             var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
             var flag2 = 0; 
           //var onlyPhonenumbers = /^[\\(]{0,1}([0-9]){3}[\\)]{0,1}[ ]?([^0-1]){1}([0-9]){2}[ ]?[-]?[ ]?([0-9]){4}[ ]*((x){0,1}([0-9]){1,5}){0,1}$/;

             var ext = ["doc", "docx", "ppt", "xlsx", "pdf", "jpg", "JPG" ,"jpeg", "png",  "gif", "pptx", "xls"];
        
            jQuery('input[name="data[UserReference][resource_location][]"]').each(function (){
            var fileExt=$(this).val(); 
        
            //alert('This file size is: ' + size + "MB");
            //alert(fileExt);
            //return false ;
            if(fileExt!="")
           {
            size=this.files[0].size/1024/1024;
            var psize=(Math.round( size * 10 ) / 10);
            if(psize>1)
            {
                flag2++;    
                alert('Your file size is: ' + psize + "MB" +' . Our limit is 1MB. ' );
                $(this).css('border-color','#F00');
                $(this).focus();
            }
            var resumeArr = fileExt.split(".");
            if(jQuery.inArray(resumeArr[1], ext)==-1)
            {
                //alert(resumeArr[1]);
                flag2++;    
                $(this).css('border-color','#F00');
                $(this).focus();
            }
        }
    });
    

    
            //set publish or draft
            var saveAs = jQuery(this).val();
            
            if(saveAs=='Save'){
            var goingID = '';
            var flag= 0;
            var zipValue = jQuery.trim(jQuery('#UserReferenceZipcode').val());
            //alert("ddd");
                    
                    //alert(flag);
                    jQuery("#UserAccessSpecifier").val('publish');          
            
                    //for first name
                    onlyNumbers = /^[0-9,]+(\.\d{0,2})?$/;
                   // onlyLetters = /^[a-zA-Z _!@#"$%\^\-:.<>?=&'()*\+,\/;\[\\\]\^_`{|}~]+$/.test(jQuery.trim(jQuery("#UserReferenceFirstName").val()))
                   onlyLetters = /^[a-zA-Z\-`']+$/.test(jQuery.trim(jQuery("#UserReferenceFirstName").val()))
                   
                    var imageName = jQuery.trim(jQuery('#image_name').val());
	                if(imageName == 'profile.png')
	            	{ 
	            		jQuery('#profileImage').css('border-color','#F00');
	            		jQuery('#profileImage').css('border-width','<?php e(ERROR_BORDER);?>px');
	            		jQuery('#profileImage').focus();
	            		if(goingID==''){ goingID = 'profileImage'; }
	            		myImagetip.setContent("Upload photo"); 
	            		myImagetip.show();
	            		flag++;
	            	}
	            	else
	            	{
	            		jQuery('#profileImage').css('border-color','rgb(221, 221, 221)');
	            		jQuery('#profileImage').css('border-width','<?php e(NORMAL_BORDER);?>px');
	            		myImagetip.hide();
	            	}
	            	
                    if(jQuery.trim(jQuery("#UserReferenceFirstName").val()) == '') {
                        jQuery('#UserReferenceFirstName').css('border-color','#F00');
                        jQuery('#UserReferenceFirstName').css('border-width','<?php e(ERROR_BORDER);?>px');
                        jQuery('#UserReferenceFirstName').focus();
                        if(goingID==''){ goingID = 'UserReferenceFirstName';}
                        myFNametip.setContent("Do not leave blank");
                        myFNametip.show();
                        flag++;
                    }else if(!onlyLetters){
                        jQuery('#UserReferenceFirstName').css('border-color','#F00');
                        jQuery('#UserReferenceFirstName').css('border-width','<?php e(ERROR_BORDER);?>px');
                        jQuery('#UserReferenceFirstName').focus();
                        goingID = 'UserReferenceFirstName';
                        myFNametip.setContent("Only alphabets are allowed");
                        myFNametip.show();
                        flag++;
                    }else {
                        jQuery('#UserReferenceFirstName').css('border-color','');  
                        jQuery('#UserReferenceFirstName').css('border-width','<?php e(NORMAL_BORDER);?>px'); 
                        myFNametip.hide();
                    }
                    //for last name
                    //var lastNameonlyLetters = /^[a-zA-Z _!@#"$%\^\-:.<>?=&'()*\+,\/;\[\\\]\^_`{|}~]+$/.test(jQuery.trim(jQuery("#UserReferenceLastName").val()))
                    var lastNameonlyLetters = /^[a-zA-Z\-`']+$/.test(jQuery.trim(jQuery("#UserReferenceLastName").val()))
                    if(jQuery.trim(jQuery('#UserReferenceLastName').val()) == '') {
                        jQuery('#UserReferenceLastName').css('border-color','#F00');
                        jQuery('#UserReferenceLastName').css('border-width','<?php e(ERROR_BORDER);?>px');
                        jQuery('#UserReferenceLastName').focus();
                        if(goingID==''){ goingID = 'UserReferenceLastName'; } 
                        myLNametip.setContent("Do not leave blank");
                        myLNametip.show(); 
                        flag++;
                    }else if(!lastNameonlyLetters){
                        jQuery('#UserReferenceLastName').css('border-color','#F00');
                        jQuery('#UserReferenceLastName').css('border-width','<?php e(ERROR_BORDER);?>px');
                        jQuery('#UserReferenceLastName').focus();
                        if(goingID==''){ goingID = 'UserReferenceLastName';}
                        myLNametip.setContent("Only alphabets are allowed");
                        myLNametip.show(); 
                        flag++;
                    } else {
                        jQuery('#UserReferenceLastName').css('border-color','');  
                        jQuery('#UserReferenceLastName').css('border-width','<?php e(NORMAL_BORDER);?>px');  
                        myLNametip.hide(); 
                    }
                    
                    //for UserReferenceZipcode
                    if(jQuery.trim(jQuery('#UserReferenceZipcode').val()) == '') {
                        jQuery('#UserReferenceZipcode').css('border-color','#F00');
                        jQuery('#UserReferenceZipcode').css('border-width','<?php e(ERROR_BORDER);?>px');
                        jQuery('#UserReferenceZipcode').focus();
                        goingID = 'UserReferenceZipcode';   
                        myZiptip.setContent("Do not leave blank");
                        myZiptip.show(); 
                        flag++;
                    }else if(isNaN(jQuery.trim(jQuery('#UserReferenceZipcode').val())) || zipValue.length<4){
                        jQuery('#UserReferenceZipcode').css('border-color','#F00');
                        jQuery('#UserReferenceZipcode').css('border-width','<?php e(ERROR_BORDER);?>px');
                        jQuery('#UserReferenceZipcode').focus();    
                        if(goingID==''){ goingID = 'UserReferenceZipcode';}  
                        myZiptip.setContent("ZIP should be 5 digits");
                        myZiptip.show();   
                        flag++;             
                    }else {
                        jQuery('#UserReferenceZipcode').css('border-color',''); 
                        jQuery('#UserReferenceZipcode').css('border-width','<?php e(NORMAL_BORDER);?>px');
                        myZiptip.hide(); 
                    }

                     //for Professional headline
                          // var headlineonlyLetters = /^[a-zA-Z\s]+$/.test(jQuery.trim(jQuery("#link-headline").val()))
                             if(jQuery.trim(jQuery("#link-headline").val()) == '') {
					jQuery('#link-headline').css('border-color','#F00');
					jQuery('#link-headline').css('border-width','<?php e(ERROR_BORDER);?>px');
					//jQuery('#link-headline').focus();
					if(goingID==''){ goingID = 'link-headline';}  
					flag++;
                                   myLinkheadline.setContent("Do not leave blank");
                               myLinkheadline.show();
					
				
				
					
				}else {
					jQuery('#link-headline').css('border-color','');	
					jQuery('#link-headline').css('border-width','<?php e(NORMAL_BORDER);?>px');
					myLinkheadline.hide();
				}
                     

 
                    //for BackgroundSummary
                    if(jQuery.trim(jQuery("#text-area").val()) == '') {
                        jQuery('#text-area').css('border-color','#F00');
                        jQuery('#text-area').css('border-width','<?php e(ERROR_BORDER);?>px');
                        jQuery('#text-area').focus();
                        if(goingID==''){ goingID = 'text-area';  }  
                        myTexttip.setContent("Do not leave blank");
                        myTexttip.show();  
                        flag++;
                    } else {
                            var qTexts = jQuery("#text-area").val();

		                  if(qTexts.length < 500)
                              {
                               jQuery('#text-area').css('border-color','#F00');
                               jQuery('#text-area').css('border-width','<?php e(ERROR_BORDER);?>px');
                                jQuery('#text-area').focus();   
                                myTexttip.setContent("1.Use 4-6 short paragraphs</br>2. Open with a 2-3 lines of overview/ summary</br>3. Focus on one area of specialization</br>4. Talk about past consulting work and methodology</br>5. Mention related experience, speaking engagements, publications</br>6. End with education, credentials, affiliations");
                                myTexttip.show();  
                                flag++;  
                             }
                             else
                             {
                                 jQuery('#text-area').css('border-color','');  
                                 jQuery('#text-area').css('border-width','<?php e(NORMAL_BORDER);?>px'); 
                                 myTexttip.hide();  
                             }
                           }

                  //check fees
                    //check UserReferenceFeeRegularSession
                    
                    if(jQuery.trim(jQuery('#UserReferenceFeeRegularSession').val()) !='' && !onlyNumbers.test(jQuery.trim(jQuery("#UserReferenceFeeRegularSession").val()))){
                        jQuery('#UserReferenceFeeRegularSession').css('border-color','#F00');
                        jQuery('#UserReferenceFeeRegularSession').css('border-width','<?php e(ERROR_BORDER);?>px');
                        jQuery('#UserReferenceFeeRegularSession').focus();  
                        if(goingID==''){ goingID = 'UserReferenceFeeRegularSession';}  
                        myRegularttip.setContent("Numeric values only");
                        myRegularttip.show();
                        flag++;             
                    }else {
                              if(jQuery('#UserReferenceFeeRegularSession').val()>99999.99)
                               {
                                    jQuery('#UserReferenceFeeRegularSession').css('border-color','#F00');
                                    jQuery('#UserReferenceFeeRegularSession').css('border-width','<?php e(ERROR_BORDER);?>px');
                                    jQuery('#UserReferenceFeeRegularSession').focus();  
                                    if(goingID==''){ goingID = 'UserReferenceFeeRegularSession'; }  
                                    myRegularttip.setContent("Fee should be less than $100,000");
                                    myRegularttip.show();
                                    flag++;
                                }
                                else{ 
                                    jQuery('#UserReferenceFeeRegularSession').css('border-color','');
                                    jQuery('#UserReferenceFeeRegularSession').css('border-width','<?php e(NORMAL_BORDER);?>px');
                                    myRegularttip.hide(); 
                                }    
                    }   


                    if(/*document.getElementById('UserReferenceApplication5Fee1').checked == true || (!isNaN(jQuery.trim(jQuery('#UserReferenceFeeFirstHour').val())) && jQuery.trim(jQuery('#UserReferenceFeeFirstHour').val()) != "0") || */(!isNaN(jQuery.trim(jQuery('#UserReferenceFeeRegularSession').val())) && jQuery.trim(jQuery('#UserReferenceFeeRegularSession').val()) != 0)){
                        if(jQuery.trim(jQuery('#UserReferencePaypalAccount').val()) == ''  || !jQuery('#UserReferencePaypalAccount').val().match(mailformat)){
                            jQuery('#UserReferencePaypalAccount').css('border-color','#F00');
                            jQuery('#UserReferencePaypalAccount').css('border-width','<?php e(ERROR_BORDER);?>px');
                            jQuery('#UserReferencePaypalAccount').focus();
                            if(goingID==''){ goingID = 'UserReferencePaypalAccount';    }   
                            myPaypaltip.setContent("Please enter PayPal receiver email account. Same as your PayPal username");
                            myPaypaltip.show();
                            flag++;
                        }else{
                            jQuery('#UserReferencePaypalAccount').css('border-color','');
                            jQuery('#UserReferencePaypalAccount').css('border-width','<?php e(NORMAL_BORDER);?>px');
                            myPaypaltip.hide();
                        }
                    }
                    //for Availablity.day_time
                    if(jQuery.trim(jQuery("#availablity").val()) == '') {
                        jQuery('#availablity').css('border-color','#F00');
                        jQuery('#availablity').css('border-width','<?php e(ERROR_BORDER);?>px');
                        jQuery('#availablity').focus(); 
                        if(goingID==''){ goingID = 'availablity';   } 
                        myAvailabilitytip.setContent("e.g.Business hours (ET). By appointment.");
                        myAvailabilitytip.show();
                        flag++;
                    } else {
                        jQuery('#availablity').css('border-color','');  
                        jQuery('#availablity').css('border-width','<?php e(NORMAL_BORDER);?>px');
                        myAvailabilitytip.hide();
                    }
                    
                    //for UserReference.area_of_expertise
                    var expertiseStr = ValidateAreaExpertise(jQuery.trim(jQuery('#UserReferenceAreaOfExpertise').val()));
                    //alert(expertiseStr);
                    if(!expertiseStr) { 
                        if(goingID==''){ goingID = 'UserReferenceAreaOfExpertise';} 
                        myExpertisetip.setContent("Up to 3 areas. Separated by commas");
                        myExpertisetip.show();
                        flag++;
                        jQuery('#UserReferenceAreaOfExpertise').css('border-color','#F00');
    					jQuery('#UserReferenceAreaOfExpertise').css('border-width','<?php e(ERROR_BORDER);?>px');
                    }else {
                        jQuery('#UserReferenceAreaOfExpertise').css('border-color',''); 
                        jQuery('#UserReferenceAreaOfExpertise').css('border-width','<?php e(NORMAL_BORDER);?>px');
                        jQuery('#area_of_expertise').html('');
                        myExpertisetip.hide();
                    }
                    

                    if(jQuery.trim(jQuery("#UserReferenceDesireMenteeProfile").val()) == '') {
                        if(goingID==''){ goingID = 'UserReferenceDesireMenteeProfile';} 
                        myDesiredtip.setContent("Do not leave blank");
                        myDesiredtip.show();
                        jQuery('#UserReferenceDesireMenteeProfile').css('border-color','#F00');
    					jQuery('#UserReferenceDesireMenteeProfile').css('border-width','<?php e(ERROR_BORDER);?>px');
                        flag++;
                    }else {
                        jQuery('#UserReferenceDesireMenteeProfile').css('border-color',''); 
                        jQuery('#UserReferenceDesireMenteeProfile').css('border-width','<?php e(NORMAL_BORDER);?>px');
                        jQuery('#UserReferenceDesireMenteeProfile').html('');
                        myDesiredtip.hide();
                    }
                    
                    //for Mode
                    /*
                    if(!jQuery("#a3").is(':checked') && !jQuery("#a5").is(':checked') ) {
                        alert("Please Select Mode !");
                        
                        flag++;
                    } else {
                        //jQuery('#a3').css('border-color',''); 
                    }
                    */
                                
                    //alert(flag);
                    
                    
                    

                    jQuery('#UserFacetofacemode').css('border-color','');
                    jQuery('#UserPhonemode').css('border-color','');
                    jQuery('#UserVideomode').css('border-color','');
                    myFModetip.hide();
                    myPModetip.hide();
                    myVModetip.hide();
                    jQuery('#UserFacetofacemode').css('border-width','<?php e(NORMAL_BORDER);?>px');
		      jQuery('#UserPhonemode').css('border-width','<?php e(NORMAL_BORDER);?>px');
		      jQuery('#UserVideomode').css('border-width','<?php e(NORMAL_BORDER);?>px');
                    if (jQuery('#a3').is(':checked') == false && jQuery('#a4').is(':checked') == false && jQuery('#a5').is(':checked') == false){
	             jQuery('#UserFacetofacemode').css('border-color','#F00');
		      jQuery('#UserPhonemode').css('border-color','#F00');
                    jQuery('#UserFacetofacemode').css('border-color','#F00');
                    jQuery('#UserVideomode').css('border-color','#F00'); 
                    jQuery('#UserFacetofacemode').css('border-color','#F00');
                    jQuery('#UserFacetofacemode').css('border-width','<?php e(ERROR_BORDER);?>px');
		      jQuery('#UserPhonemode').css('border-width','<?php e(ERROR_BORDER);?>px');
                    jQuery('#UserVideomode').css('border-width','<?php e(ERROR_BORDER);?>px');
                    jQuery('#UserFacetofacemode').focus();
		      if(goingID==''){ goingID = 'Facetofacemode';   } 	
		      myFModetip.setContent("Enter atleast one communication mode");
		      myFModetip.show();
		      flag++;		       
			} else {
                          if (jQuery('#a3').is(':checked') == true && jQuery.trim(jQuery('#UserFacetofacemode').val()) == ""){
                        jQuery('#UserFacetofacemode').css('border-color','#F00');
                        jQuery('#UserFacetofacemode').css('border-width','<?php e(ERROR_BORDER);?>px');
                        jQuery('#UserFacetofacemode').focus();  
                        if(goingID==''){ goingID = 'UserFacetofacemode';} 
                        myFModetip.setContent("Enter communication mode, without specific address or number");
                        myFModetip.show();
                        flag++;
                    }
                     if (jQuery('#a4').is(':checked') == true && jQuery.trim(jQuery('#UserPhonemode').val()) == ""){
                        jQuery('#UserPhonemode').css('border-color','#F00');
                        jQuery('#UserPhonemode').css('border-width','<?php e(ERROR_BORDER);?>px');
                        jQuery('#UserPhonemode').focus();  
                        if(goingID==''){ goingID = 'UserPhonemode';} 
                        myPModetip.setContent("Enter your phone number");
                        myPModetip.show();
                        flag++;
                    }
                    /*if( jQuery('#a4').is(':checked') == true && jQuery.trim(jQuery('#UserPhonemode').val()) !='' && !onlyPhonenumbers.test(jQuery.trim(jQuery("#UserPhonemode").val()))){
			   jQuery('#UserPhonemode').css('border-color','#F00');
			   jQuery('#UserPhonemode').css('border-width','<?php e(ERROR_BORDER);?>px');
			   jQuery('#UserPhonemode').focus();	
			   if(goingID==''){ goingID = 'UserPhonemode';   } 	
			   myPModetip.setContent("Numeric values only");
                        myPModetip.show();		
		          flag++;				
		      }*/
                    if (jQuery('#a5').is(':checked') == true && jQuery.trim(jQuery('#UserVideomode').val()) == ""){
                        jQuery('#UserVideomode').css('border-color','#F00');
                        jQuery('#UserVideomode').css('border-width','<?php e(ERROR_BORDER);?>px');
                        jQuery('#UserVideomode').focus();
                        if(goingID==''){ goingID = 'UserVideomode';} 
                        myVModetip.setContent("Enter communication mode, without specific address or number");
                        myVModetip.show();
                        flag++;
                    }   
                   }     
                    if(jQuery.trim(jQuery('#textareafee').val()) == ''){
                    jQuery('#textareafee').css('border-color','#F00');
                    jQuery('#textareafee').css('border-width','<?php e(ERROR_BORDER);?>px');
                    jQuery('#textareafee').focus();
                    if(goingID==''){ goingID = 'textareafee';    }   
                    myFeeNSessiontip.setContent("What's included and what's excluded? ");
                    myFeeNSessiontip.show();
                    flag++;
                }else{
                    jQuery('#textareafee').css('border-color','');
                    jQuery('#textareafee').css('border-width','<?php e(NORMAL_BORDER);?>px');
                    myFeeNSessiontip.hide();
                }

                    
                    jQuery("input[name='data[UserReference][resource_title][]']").each(function() {
                        //curr_index = jQuery("input[name='data[UserReference][resource_title][]']").index(this);
                        jQuery(this).css('border-color','');
                    }); 
                    
                    jQuery("input[name='data[UserReference][resource_location][]']").each(function() {
                        //curr_index = jQuery("input[name='data[UserReference][resource_location][]']").index(this);
                        jQuery(this).css('border-color','');
                    });
                    
                    resource_id_array = new Array();
                    index_count = 0;
                    jQuery("input[name='data[UserReference][resource_id][]']").each(function() {
                        curr_index = jQuery("input[name='data[UserReference][resource_id][]']").index(this);
                        if(jQuery("input[name='data[UserReference][resource_id][]']").eq(curr_index).val() != "NEW"){
                            resource_id_array[index_count] = curr_index;
                            index_count++;
                        }   
                    });
                    
                    jQuery("input[name='data[UserReference][resource_title][]']").each(function() {
                        curr_index = jQuery("input[name='data[UserReference][resource_title][]']").index(this);
							
                        if(jQuery.inArray(curr_index, resource_id_array) == -1){
                            
                            if(jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).val() != ""){
								if(resource_id_array.length > 0){
									curr_index = curr_index - resource_id_array.length;
								}
                                if(jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index).val() == ""){
                                    jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index).css('border-color','#F00');
                                    jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index).css('border-width','<?php e(ERROR_BORDER);?>px');
                                    jQuery(this).focus();
                                    new Opentip(jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index), "Upload a file", { style: "myErrorStyle" } );
                                    flag++;
                                }
                            }
                            
                        }else{
                            if(jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).val() == ""){
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-color','#F00');
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-width','<?php e(ERROR_BORDER);?>px');
                                    jQuery(this).focus();
                                    new Opentip(jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index), "Please provide a title", { style: "myErrorStyle" } );
                                    flag++;
                                }
                        }
                    }); 
                    
                    //*****************************************************
                    
                    jQuery("input[name='data[UserReference][resource_location][]']").each(function() {
                        curr_index = jQuery("input[name='data[UserReference][resource_location][]']").index(this);
						
						if(resource_id_array.length > 0){
							curr_index = curr_index + resource_id_array.length;
						}
						//alert(curr_index +'\n'+ resource_id_array.join(', '));
                        if(jQuery.inArray(curr_index, resource_id_array) == -1){
                            if(resource_id_array.length > 0){
								curr_index = curr_index - resource_id_array.length;
							}
                            if(jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index).val() != ""){
								if(resource_id_array.length > 0){
									curr_index = curr_index + resource_id_array.length;
								}
								
                                if(jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).val() == ""){
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-color','#F00');
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-width','<?php e(ERROR_BORDER);?>px');
                                    jQuery(this).focus();
                                    new Opentip(jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index), "Please provide a title", { style: "myErrorStyle" } );
                                    flag++;
                                }
                            }
                            
                        }else{
							//alert(curr_index + "  ELSE");
                            if(jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).val() == ""){
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-color','#F00');
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-width','<?php e(ERROR_BORDER);?>px');
                                    jQuery(this).focus();
                                    new Opentip(jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index), "Please provide a title", { style: "myErrorStyle" } );
                                    flag++;
                                }
                        }
                    }); 
                    
					jQuery("input[name='data[UserReference][resource_location][]']").each(function() {
						checkValidFile(this);
					});
                    //*****************************************************
                    
                    //flag++;
                    //alert(flag);
                    if(flag == 0 && flag2 == 0 ){
                        jQuery("#EditAccountform").submit();
                    }else{
                        if(goingID != ""){
                            jQuery('#'+goingID).focus();
                        }
                        return false;
                    }
            
            
            
            }else{
                //jQuery("#UserAccessSpecifier").val('draft');
                jQuery("#EditAccountform").submit();
            
            
            }
            
            
        });


    });
        
    function ValidateFileSize(iSize,Filename){
        var size=Math.round((iSize / 1024) * 100) / 100;
       
        if (size <2)
        {
            var SuccessMessage= Filename;
            jQuery("#resume_old").val(SuccessMessage);
            jQuery("#resume_old").html(SuccessMessage);
            jQuery("#resume_old").css('color','')
        }else{
            var ErrorMessage= iSize  + "kb is exceeded limit of file size";
            jQuery("#resume_old").val(ErrorMessage);
            jQuery("#resume_old").html(ErrorMessage);
            jQuery("#resume_old").css('color','#F00')
            jQuery("#resource_title").css('border','1px solid #F00');
            flag++;
        }

    }
    //Areas of Expertise id = UserReferenceAreaOfExpertise
    function ValidateAreaExpertise(str){
        
        //blank check
        if(str ==''){
            jQuery('#UserReferenceAreaOfExpertise').css('border-color','#F00'); 
            jQuery('#UserReferenceAreaOfExpertise').css('border-width','<?php e(ERROR_BORDER);?>px');
            jQuery('#UserReferenceAreaOfExpertise').focus();
            jQuery('#area_of_expertise').html('');
            jQuery('#area_of_expertise').css('color','#F00');   
            //alert(str);   
            return false;
        }else{
            if(str.indexOf(",") !=-1){ // checking comma
                var expertArr = str.split(",");
                var length = expertArr.length;
                if(length >3){
                
                    jQuery('#UserReferenceAreaOfExpertise').css('border-color','#F00'); 
                    jQuery('#UserReferenceAreaOfExpertise').css('border-width','<?php e(ERROR_BORDER);?>px');
                    jQuery('#UserReferenceAreaOfExpertise').focus();
                    jQuery('#area_of_expertise').html('Please enter 3 areas of expertise separated by commas.')
                    jQuery('#area_of_expertise').css('color','#F00');
                    //alert("b");                   
                    return false;
                }else{
                    for(i = 0;i<length;i++){
                        if(expertArr[i] ==''){
                            jQuery('#UserReferenceAreaOfExpertise').css('border-color','#F00'); 
                            jQuery('#UserReferenceAreaOfExpertise').css('border-width','<?php e(ERROR_BORDER);?>px');
                            jQuery('#UserReferenceAreaOfExpertise').focus();
                            jQuery('#area_of_expertise').html('Please enter 3 areas of expertise separated by commas.')
                            jQuery('#area_of_expertise').css('color','#F00');
                            //alert("c");   
                            return false;
                        }                   
                    }
                    
                }
            }
            
            
        }
        return true;

    }

</script>

<script>
var i=0;
var res = 1;
var iQuestion=0;
$(document).ready(function(){


    i =  <?php e($totalSocial) ; ?>;
  
    
    
    
    $('#reset').click(function() {
    while(i > 2) {
        $('.field:last').remove();
        i--;
    }
    });
    
    
/*VL 25/12 start*/
    $('#edit_btn').click(function() 
    {
        
        var id_div=document.getElementById('edit_box');
        var vis = id_div.style;
        
        if (vis.display=='none')
        {
            
            document.getElementById('edit_box').style.display='block';
            
        }
        else
        {
            document.getElementById('edit_box').style.display='none';
        }
        
    });
    
    $('#edit_1').click(function() 
    {
        document.getElementById('link_1').value='';
    });
    $('#edit_2').click(function() 
    {
        document.getElementById('link_2').value='';
    });
    $('#edit_3').click(function() 
    {
        document.getElementById('link_3').value='';
    });
    $('#edit_4').click(function() 
    {
        document.getElementById('link_4').value='';
    });
    $('#edit_5').click(function() 
    {
        document.getElementById('link_5').value='';
    });
    $('#edit_6').click(function() 
    {
        document.getElementById('link_6').value='';
    });
    $('#edit_7').click(function() 
    {
    	document.getElementById('link_7').value='';
    });
 
    /*VL 25/12 end*/    

});

function act(id)
    {
        
        var mutli_education = document.EditAccountform.elements["link[]"];
        //inputsData=document.getElementById('link');
        mutli_education[id].value="";
        //alert("ff");
        //alert(mutli_education[0].files[0].size);
        
        
    } 
    

</script>
<script type="text/javascript">
    jQuery(document).ready(function(){  

    // select element styling
            jQuery('select.select').each(function(){
                var title = jQuery(this).attr('title');
                if( jQuery('option:selected', this).val() != ''  ) title = jQuery('option:selected',this).text();
                jQuery(this)
                    .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
                    .after('<span class="select">' + title + '</span>')
                    .change(function(){
                        val = jQuery('option:selected',this).text();
                        jQuery(this).next().text(val);
                        })
            });
    });
</script>
