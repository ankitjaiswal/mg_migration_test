	
	<div class="fieldset" style="display: block;">
    <div class="adminrightinner" style="padding:0px;">

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td width="5%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Id</td>
                        <td  width="25%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Project Title</td>
                        <td  width="18%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">URL for consultant</td>
                        <td  width="19%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Public URL</td>
                        <td  width="18%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Process</td>
                         <td width="7%" align="center" valign="middle" class="Bdrrightbot Padtopbot6">State</td>
                        <td width="8%" align="center" valign="middle" class="Bdrrightbot Padtopbot6">Action</td>
                    </tr>
                    <?php 
                    if(!empty($projects) && count($projects)>0)
                    { 
                        foreach($projects as $msg)
                        {    
                     $project_url = $msg['Project']['project_url'];
 
                        ?>
                        <tr>
                            <td width="5%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo $msg['Project']['id']; ?></td>
                            <td  width="25%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php e($html->link($msg['Project']['title'],SITE_URL.'project/index/'.$project_url,array('escape'=>true, ))); ?></td>
                            <td  width="18%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php 
                              $link = SITE_URL."project/consultant_view/help@mentorsguild.com/".$project_url;
                              e($html->link("URL for consultant",$link,array('escape'=>false,'target'=>_blank)));?></td>
                           <td  width="19%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php 
                              $link = SITE_URL."project/project_details/".$project_url;
                              e($html->link("Public URL",$link,array('escape'=>false,'target'=>_blank)));?></td> 
                            <td width="18%" align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php e($html->link('Invite Consultants',SITE_URL.'project/invite/'.$msg['Project']['id'],array('escape'=>true, 'target'=>'_blank'))); ?></td>     
                            <?php if($msg['Project']['status'] == '4'){?>
                           <td width="7%" align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $html->image("active.png", array("title" => "Active User", "alt" => "Active", "border" => 0)); ?></td>
                             <?php }else{?>
                            <td width="7%" align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $html->image("deactive.png", array("title" => "Deactive User", "alt" => "Deactive", "border" => 0));?></td>
                           <?php }?>                          
                            <td width="8%" align="center" valign="middle" class="Bdrbot ActionIcon"><?php e($admin->getActionImage(array('delete' => array('controller' => 'project', 'action' => 'deleteproject', 'token' => 'delete')), $msg['Project']['id'])); ?></td>
                            
                        </tr>	
                   <?php
                        }
                    }else{ ?>
                   <tr>
                        <td colspan="3" width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Record not found</td>           
                    </tr>
                   <?php } ?>   
                </table>
            </div>
            <?php
     //   }
        ?>

    </div>
</div>
<div class="clr"></div>
<?php //echo $this->element('admin/admin_paging', array("paging_model_name" => "NewsLetter", "total_title" => "NewsLetter")); ?>	 