<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div class="mentor_details"><!-- VL 22/12-->
			<div id="mentor-detail-left1">
				<div id="area-box">
					<div class="expertise" style="padding-top:15px;">
						<?php e($form->hidden('Project.id'));?>
						
                                           <h1>You have shown interest in this project.</h1>
                                            <br/><br/>
						<div class="expertise">
                        	<h2>Project title</h2>
                        	<p><?php e($this->data['Project']['title']);?></p>
                   		</div>
                   		
				<div class="expertise">
                        	<h2>Business need</h2>
                        	<p><?php e($this->data['Project']['business_need']);?></p>
                   		</div>

				<div class="expertise">
                        	<h2>Solution type</h2>
                        	<p><?php e($this->data['Project']['solution_type']);?></p>
                   		</div>

                   		<div class="expertise">
                        	<h2>Project details</h2>
                        	<p>
                        		<?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['details']); 
								echo $this->General->make_links(nl2br($details));?>
							</p>
                   		</div>
                   		

                   		
                   		<?php if(isset($this->data['Project']['success']) && $this->data['Project']['success'] != '' ) {?>
						<div class="expertise">
                        	<h2>Project goal</h2>
                        	<p>
                        		<?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['success']); 
								echo $this->General->make_links(nl2br($details));?>
							</p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['budget']) && $this->data['Project']['budget'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Approximate Budget $ </h2>
                        	<p><?php e($this->data['Project']['budget']);?></p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['location']) && $this->data['Project']['location'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Location preference</h2>
                        	<p><?php e($this->data['Project']['location']);?></p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['duration']) && $this->data['Project']['duration'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Estimated duration</h2>
                        	<p><?php e($this->data['Project']['duration']);?></p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['start_date']) && $this->data['Project']['start_date'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Target start date</h2>
                        	<p><?php e($this->data['Project']['start_date']);?></p>
                   		</div>
                   		<?php }?>
                   		


                   		<?php if(isset($this->data['Project']['filepath']) && $this->data['Project']['filepath'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Attachment</h2>
                        	<div class="upload">
                            	<?php 
                            	$img = SITE_URL."img/".MENTEES_PROJECT_PATH.DS.$this->data['Project']['id'].DS.$this->data['Project']['filepath']; ?>
                            	
                            	<p><span style="font-size:14px;"> <?php e($html->link($this->data['Project']['filepath'],$img,array('escape'=>true , 'target'=>'_blank'))); ?> </span></p>
					   		</div>
                   		</div>
                   		<?php }?>
                   		

					</div>

				</div>
    		</div>
    		<?php if($this->Session->read('Auth.User.role_id')==Configure::read('App.Role.Mentor') && $this->Session->read('Auth.User.id') != $this->data['Project']['user_id'] || $this->Session->read('Auth.User.role_id')==Configure::read('App.Role.Admin') ) {?>

               <?php  $for_creator = ClassRegistry::init('User'); 	
                      $creator = $for_creator->find('first', array('conditions' => array('User.id' => $this->data['Project']['user_id'])));       	  
                      if($creator['User']['role_id'] == Configure::read('App.Role.Mentor')){?>

		<div id="mentor-detail-right" style="margin-bottom: 20px; display: block;">
    			<div class="expertise" style="margin-top: 15px;">
	    			  <?php
						$displink1=SITE_URL.strtolower($createdBy['User']['url_key']);
					  ?>
                     <h2>Created By: <?php  e($html->link($createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'],$displink1,array('escape'=>false, 'target'=>'_blank')));?></h2>
                     <div class="profileimg" style="margin-top: 15px;">
					<?php 
					if(isset($createdBy['UserImage'][0]['image_name']) && $createdBy['UserImage'][0]['image_name'] !=''){
							$image_path = MENTORS_IMAGE_PATH.DS.$createdBy['User']['id'].DS.$createdBy['UserImage'][0]['image_name'];
							e($html->link($html->image($image_path, array('class'=>'desaturate','alt' => $createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'])),$displink1,array('escape'=>false, 'target'=>'_blank','style'=>'border: 1px solid #DCDCDC; display:block; width:150px;')));
					}else{
							e($html->link($html->image('media/profile.png', array('class'=>'desaturate','alt' => $createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'])),$displink1,array('escape'=>false, 'target'=>'_blank','style'=>'border: 1px solid #DCDCDC; display:block; width:150px;')));
					}?>
				</div>
                </div>
			</div>
 
<?php } else{?>

		<div id="mentor-detail-right" style="margin-bottom: 20px; display: block;">
    			<div class="expertise" style="margin-top: 15px;">
	    			  <?php
						$displink1=SITE_URL."clients/my_account/".strtolower($createdBy['User']['url_key']);
					  ?>
                     <h2>Created By: <?php  e($html->link($createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'],$displink1,array('escape'=>false,'target'=>'_blank')));?></h2>
                     <div class="profileimg" style="margin-top: 15px;">
					<?php 
					if(isset($createdBy['UserImage'][0]['image_name']) && $createdBy['UserImage'][0]['image_name'] !=''){
							$image_path = MENTEES_IMAGE_PATH.DS.$createdBy['User']['id'].DS.$createdBy['UserImage'][0]['image_name'];
							e($html->link($html->image($image_path, array('class'=>'desaturate','alt' => $createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank','style'=>'border: 1px solid #DCDCDC; display:block; width:150px;')));
					}else{
							e($html->link($html->image('media/profile.png', array('class'=>'desaturate','alt' => $createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank','style'=>'border: 1px solid #DCDCDC; display:block; width:150px;')));
					}?>
				</div>
                </div>
			</div>
			<?php } }?>
    	</div>	

    </div>
</div>

<script type="text/javascript">
function close_window() {

	var ww = window.open(window.location, '_self'); 
	ww.close();
 }
</script>
    <script type="text/javascript">

     
function ignoreMember(confirm_id){
   if(confirm('Do you want to Ignore this member? This action can not be undone.')==true && confirm_id!='')
    {
		var ignore_id = 'Ignore'+confirm_id
		jQuery(document).ready(function(){
				jQuery.ajax({
					url:SITE_URL+'project/del_member',
					type:'post',
					dataType:'json',
					data:'confirm_id='+confirm_id,
					success:function(){
						document.getElementById(ignore_id).style.display = 'none';
					}
					});
                });
              }	
}
function consultation_request(confirm_id){

   if(confirm('Do you want to request for initial consultaion? ')==true && confirm_id!='')
    {		var ignore_id = 'Ignore'+confirm_id
		jQuery(document).ready(function(){
				jQuery.ajax({
					url:SITE_URL+'project/consultation_request_ajax',
					type:'post',
					dataType:'json',
					data:'confirm_id='+confirm_id,
					success:function(){
						document.getElementById(ignore_id).style.display = 'none';
                                       
					}
					});
                });
              }
}
</script>