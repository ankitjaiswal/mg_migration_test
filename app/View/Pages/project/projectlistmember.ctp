<style>
#pagingarea span{ font-size:15px; padding:5px; }
</style>
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth" style="margin-bottom:50px;">
     <div id="registerationFm" style="min-height: 450px;">
        <h1>My Projects</h1>
        
        <div class="feedbackcontent" style="padding-top: 12px;">
           <table width="100%" style="border:1px solid #CCCCCC;" cellspacing="2" cellpadding="10">
               <tr bgcolor="#2A2A2A" class="row_head">
                   <td>Created by</td>
                   <td>Date</td>
                   <td>Project title</td>
                   <td>Action</td>
               </tr>
           <?php if(!empty($data) && count($data)>0)
                {
                    $i=1; 
                  foreach($data as $project){
                      if($i%2==0)
                        $class = 'first_row';
                      else
                        $class = 'second_row';        

  
                     $for_creator = ClassRegistry::init('User'); 	
                     $creator = $for_creator->find('first', array('conditions' => array('User.id' => $project['Project']['user_id']))); 
                     if($creator['User']['role_id'] == Configure::read('App.Role.Mentor'))
                     $displink1=SITE_URL.strtolower($creator['User']['url_key']);
                     else
                     $displink1=SITE_URL."clients/my_account/".strtolower($creator['User']['url_key']);
           ?>
               <tr class="<?php echo $class; ?>">
                   <td><?php  e($html->link(ucfirst($creator['UserReference']['first_name'])." ".ucfirst($creator['UserReference']['last_name']),$displink1,array('escape'=>false,'target'=>'_blank')));?></td>
                   <td><?php echo date("m-d-Y", strtotime($project['Project']['created']));?></td>
                   <td><?php echo $project['Project']['title']; ?></td>
                   <td><?php e($html->link('View',array('controller'=>'project','action'=>'index/'.$project['Project']['project_url']),array('class'=>'delete','title'=>'View project'))); ?></td>
               </tr>
           <?php    
                    $i++;
                    }?>

      <?php 
                 } else{?>
              <tr class="first_row" style="text-align: center;">
                   <td colspan="7">No record found</td>
               </tr>
               <?php } ?>
           </table>
        </div>
         </div>
      
    </div>
</div>
<style>
    .row_head{color: #EEEEEE; font-size: 14px; height: 30px; background: #2A2A2A; text-align:left;}  
    .first_row{color: #000000; font-size: 14px; height: 30px; background:#CCCCCC; text-align:left;}
    .second_row{color: #000000; font-size: 14px; height: 30px; text-align:left;    
</style>