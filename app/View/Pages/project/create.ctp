<?php e($html->css(array('jquery/jquery-ui','opentip'))); ?>
<?php e($javascript->link(array('caljs/jquery-1.9.1','caljs/jquery-ui','opentip-jquery')));?> 
<link rel="stylesheet" href="<?php e(SITE_URL)?>css/project.css">
    
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div class="mentor_details"><!-- VL 22/12-->
			<div id="mentor-detail-left1">
				<form id="Appyform" enctype="multipart/form-data" method="post" action="<?php echo SITE_URL ?>project/create" accept-charset="utf-8">
				<div id="area-box">
					<div class="expertise" style="padding-top:30px;">
						<div style="display: block; height: 60px; margin-bottom: 20px; border-bottom: 1px solid rgb(188, 188, 188);">
							<div style="float: left;">
								<h1 style="font-size: 20px; border: 0px;">Post a project</h1>
								<sub class="MGSub">Free & confidential</sub> 
							</div>

						</div>
						<?php e($form->hidden('Project.id'));?>
						<div class="inputRELATIVE">
							<?php if(isset($this->data['Project']['title']) && $this->data['Project']['title'] != ''){?>
                        		<h3>Project title <span style="float:right; font-weight: normal;">(<span id="noOfCharTitle"><?php e(128 - strlen($this->data['Project']['title']))?></span> characters remaining)</span></h3>
                        	<?php }else{?>
                        		<h3>Title your project<span style="float:right; font-weight: normal;">(<span id="noOfCharTitle">128</span> characters remaining)</span></h3>
                        	<?php }?>
                        	<p style="padding-top: 5px;"><?php e($form->input('Project.title', array('div'=>false, 'label'=>false, "class" => "forminput",  "style"=>"width:651px;" ,"placeholder" => "A workshop on design thinking.", "id" => "project_title")));?></p>
                        	<div class="showHINT" style="height: 90px; margin-top: 25px;">
                        		<span></span><!--for left arrow show on hover-->
                        		<p style="margin-top: 30px;">
                        			Be specific
                        		</p>
                    		</div> 
                   		</div>
                   		
                   		<div class="inputRELATIVE" style="height: 80px;">
	                   		<div  style="width: 50%; float: left;">
	                        	<h3>My business need</h3>
	                        	<div class="caseRIGHT" style="float: none; margin-left: 0px;padding-top: 5px;">
									<select id="business_need" style="font-family: Ubuntu; font-size: 14px; color:#2a2a2a; margin-left: 0px; margin-top: 5px; margin-bottom: 50px; width: 327px; height: 47px;background-color:#fff;" name="data[Project][business_need]">
                                                                     <?php if($this->data['Project']['business_need'] == 'Please select...'){?>
											<option style="color:gray" value="Please select..." selected="selected">Please select...</option>
										<?php }else{
											if(isset($this->data['Project']['business_need']) == false){?>
											<option style="color:gray" value="Please select..." selected="selected">Please select...</option>
										<?php }else{?>
											<option style="color:gray" value="Please select...">Please select...</option>
										<?php }}?>
                                                                      <?php if($this->data['Project']['business_need'] == 'Strategic planning'){?>
											<option value="Strategic planning" selected="selected">Strategic planning</option>
										<?php }else{?>
											<option value="Strategic planning">Strategic planning</option>
										<?php }?>
										<?php if($this->data['Project']['business_need'] == 'Connect with customers'){?>
											<option value="Connect with customers" selected="selected">Connect with customers</option>
										<?php }else{?>
											<option value="Connect with customers">Connect with customers</option>
										<?php }?>
										<?php if($this->data['Project']['business_need'] == 'Develop employees & culture'){?>
											<option value="Develop employees & culture" selected="selected">Develop employees & culture</option>
										<?php }else{?>
											<option value="Develop employees & culture">Develop employees & culture</option>
										<?php }?>
										<?php if($this->data['Project']['business_need'] == 'Fix / improve a process'){?>
											<option value="Fix / improve a process" selected="selected">Fix / improve a process</option>
										<?php }else{?>
											<option value="Fix / improve a process">Fix / improve a process</option>
										<?php }?>
										<?php if($this->data['Project']['business_need'] == 'Access industry best practices'){?>
											<option value="Access industry best practices" selected="selected">Access industry best practices</option>
										<?php }else{?>
											<option value="Access industry best practices">Access industry best practices</option>
										<?php }?>
										<?php if($this->data['Project']['business_need'] == 'Drive reorganization'){?>
											<option value="Drive reorganization" selected="selected">Drive reorganization</option>
										<?php }else{?>
											<option value="Drive reorganization">Drive reorganization</option>
										<?php }?>
										<?php if($this->data['Project']['business_need'] == 'Project support / Other'){?>
											<option value="Project support / Other" selected="selected">Project support / Other</option>
										<?php }else{?>
											<option value="Project support / Other">Project support / Other</option>
                                                                     <?php }?>
                                                         </select>			
								</div>
	                   		</div>
	                   		<div  style="width: 50%; float: right;">
	                   			
		                        	<h3>&nbsp;&nbsp;&nbsp;Solution type</h3>
                                        <div class="caseRIGHT" style="float: none; margin-left: 0px;padding-top: 5px;">
									<select id="solution_type" style="font-family: Ubuntu; font-size: 14px; color:#2a2a2a; margin-left: 8px; margin-top: 5px; margin-bottom: 50px; width: 327px; height: 47px;background-color:#fff;" name="data[Project][solution_type]">
										<?php if($this->data['Project']['solution_type'] == 'Please select...'){?>
											<option style="color:gray" value="Please select..." selected="selected">Please select...</option>
										<?php }else{
											if(isset($this->data['Project']['solution_type']) == false){?>
											<option style="color:gray"value="Please select..." selected="selected">Please select...</option>
											<?php }else{?>
											<option style="color:gray" value="Please select...">Please select...</option>
										<?php }}?>										
                                                                     <?php if($this->data['Project']['solution_type'] == '1-Hour call with an expert'){?>
											<option value="1-Hour call with an expert" selected="selected">1-Hour call with an expert</option>
										<?php }else{?>
											<option value="1-Hour call with an expert">1-Hour call with an expert</option>
										<?php }?>
										<?php if($this->data['Project']['solution_type'] == 'Market research report'){?>
											<option value="Market research report" selected="selected">Market research report</option>
										<?php }else{?>
											<option value="Market research report">Market research report</option>
										<?php }?>
										<?php if($this->data['Project']['solution_type'] == 'Workshop/ Session'){?>
											<option value="Workshop/ Session" selected="selected">Workshop/ Session</option>

										<?php }else{?>
											<option value="Workshop/ Session">Workshop/ Session</option>
										<?php }?>
										<?php if($this->data['Project']['solution_type'] == 'Executive coaching'){?>
											<option value="Executive coaching" selected="selected">Executive coaching</option>
										<?php }else{?>
											<option value="Executive coaching">Executive coaching</option>
										<?php }?>
										<?php if($this->data['Project']['solution_type'] == 'Consulting project'){?>
											<option value="Consulting project" selected="selected">Consulting project</option>
										<?php }else{?>
											<option value="Consulting project">Consulting project</option>
										<?php }?>
										<?php if($this->data['Project']['solution_type'] == 'Interim management'){?>
											<option value="Interim management" selected="selected">Interim management</option>
										<?php }else{?>
											<option value="Interim management">Interim management</option>
										<?php }?>
									</select>
                                      </div>
	                        	</div>
	                   		
                   		</div>
                            
                   		<div class="inputRELATIVE">
                   			<?php if(isset($this->data['Project']['details']) && $this->data['Project']['details'] != ''){?>
                        		<h3>Describe your need <span style="float:right; font-weight: normal;">(<span id="noOfCharDetails"><?php e(5000 - strlen($this->data['Project']['details']))?></span> characters remaining)</span></h3>
                        	<?php } else {?>
                        		<h3>Describe your need <span style="float:right; font-weight: normal;">(<span id="noOfCharDetails">5000</span> characters remaining)</span></h3>
                        	<?php }?>
                        	<p style="padding-top: 5px;"><?php e($form->textarea('Project.details', array('rows'=>6,'div'=>true, 'label'=>false , "class" => "forminput", "style"=>"width:97%;font-family: Ubuntu, arial, vardana; font-size: 14px; margin-bottom: 20px; padding: 10px;" , "placeholder" => "Please provide all relevant details about your project.", "id" => "project_details")));?></p>
                        	<div class="showHINT" style="height: 175px; margin-top: 25px;">
                        		<span></span><!--for left arrow show on hover-->
                        		<p style="text-align: left; margin-top: 10px;">
									1. What is the key business challenge or opportunity?<br/><br/>
									2. What is the scope of this project?<br/><br/>
									3. How will you evaluate the candidates?
                        		</p>
                    		</div>
                   		</div>
                             <div style="margin-left:420px; margin-bottom:-25px; ">

                                <a href="javascript: void(0)" id="optionallink" class="showoptional">Enter optional fields</a>

                             </div>
                      <div id = "optional"  style="display: none;">	
					<br/><br/>	
			<div class="inputRELATIVE">
                        	<h3>Describe the ideal professional for your project</h3>
                        	<p style="padding-top: 5px;"><?php e($form->textarea('Project.success', array('rows'=>6,'div'=>true, 'label'=>false , "id" => "project_success" , "class" => "forminput", "style"=>"width:97%;font-family: Ubuntu, arial, vardana; font-size: 14px; margin-bottom: 20px; padding: 10px;" , "placeholder" => "Specific industry experience, key expertise, etc.")));?></p>
                        	<div class="showHINT" style="height: 115px; margin-top: 55px;">
                        		<span></span><!--for left arrow show on hover-->
                        		<p style="text-align: left; margin-top: 10px;">
									Link project goal to a business outcome. (Many projects may not have quantifiable ROI)<br/><br/>
                        		</p>
                    		</div>
                   		</div>
                   		
                   		<div class="inputRELATIVE" style="height: 80px;">
	                   		<div style="width: 50%; float: left;" class="inputShow">
		                        	<h3>Approximate Budget $</h3>
		                        	<div class="caseRIGHT" style="float: left; margin-left: 0px; width: 100px;padding-top: 5px;">
										<select style="font-family: Ubuntu; font-size: 14px; color:#2a2a2a; margin-left: 0px; margin-top: 5px; margin-bottom: 30px; width: 100px; height: 47px;background-color:#fff;" name="data[Project][budget_type]">
											<?php if($this->data['Project']['budget_type'] == 'Hourly'){?>
												<option value="Hourly" selected="selected">Hourly</option>
											<?php }else {?>
												<option value="Hourly">Hourly</option>
											<?php }?>
											<?php if($this->data['Project']['budget_type'] == 'Fixed'){?>
												<option value="Fixed" selected="selected">Fixed</option>
											<?php }else {?>
												<option value="Fixed">Fixed</option>
											<?php }?>
											<?php if($this->data['Project']['budget_type'] == 'Other'){?>
												<option value="Other" selected="selected">Other</option>
											<?php }else {?>
												<option value="Other">Other</option>
											<?php }?>
										</select>	
									</div>
									<div style="float: right;margin-top: 5px;">
										<?php e($form->input('Project.budget', array('div'=>false, 'label'=>false, "class" => "forminput",  "style"=>"width:200px; margin-top: 5px;" ,"placeholder" => "", "id" => "project_budget")));?>
									</div>
		
	                   		</div>
	                   		<div  style="width: 50%; float: right;">
	                   			<div style="float: right;">
		                        	<h3>Location preference</h3>
		                        	<p><?php e($form->input('Project.location', array('div'=>false, 'label'=>false, "class" => "forminput",  "style"=>"width:300px;margin-top: 5px;" ,"placeholder" => "", "id" => "project_location")));?></p>
	                        	</div>
	                    	</div>
                    	</div>
                   		
                   		<div class="inputRELATIVE"  style="height: 80px;">
	                   		<div  style="width: 50%; float: left;">
	                        	<h3 style="margin-top: 20px;">Estimated duration</h3>
	                        	<div class="caseRIGHT" style="float: none; margin-left: 0px;padding-top: 5px;">
									<select style="font-family: Ubuntu; font-size: 14px; color:#2a2a2a; margin-left: 0px; margin-top: 5px; margin-bottom: 30px; width: 335px; height: 47px;background-color:#fff;" name="data[Project][duration]">
										<?php if($this->data['Project']['duration'] == 'Less than 1 Month'){?>
											<option value="Less than 1 Month" selected="selected">Less than 1 Month</option>
										<?php }else{?>
											<option value="Less than 1 Month">Less than 1 Month</option>
										<?php }?>
										<?php if($this->data['Project']['duration'] == '1 to 3 Months'){?>
											<option value="1 to 3 Months" selected="selected">1 to 3 Months</option>
										<?php }else{?>
											<option value="1 to 3 Months">1 to 3 Months</option>
										<?php }?>
										<?php if($this->data['Project']['duration'] == '3 to 6 Months'){?>
											<option value="3 to 6 Months" selected="selected">3 to 6 Months</option>
										<?php }else{
												if(isset($this->data['Project']['duration']) == false){?>
													<option value="3 to 6 Months" selected="selected">3 to 6 Months</option>
												<?php }else{?>
													<option value="3 to 6 Months">3 to 6 Months</option>
										<?php }}?>
										<?php if($this->data['Project']['duration'] == 'Greater than 6 Months'){?>
											<option value="Greater than 6 Months" selected="selected">Greater than 6 Months</option>
										<?php }else{?>
											<option value="Greater than 6 Months">Greater than 6 Months</option>
										<?php }?>
										<?php if($this->data['Project']['duration'] == 'Flexible'){?>
											<option value="Flexible" selected="selected">Flexible</option>
										<?php }else{?>
											<option value="Flexible">Flexible</option>
										<?php }?>
									</select>			
								</div>
	                   		</div>
	                   		<div  style="width: 50%; float: right;">
	                   			<div style="float: right;">
		                        	<h3 style="margin-top: 20px;">Target start date</h3>
		                        	<p>
		                        		<?php e($form->input('Project.start_date', array('div'=>false, 'label'=>false, "class" => "forminput",  "style"=>"width:300px;margin-top: 5px;" ,"placeholder" => "MM/DD/YYYY", "id" => "project_start_date")));?>
		                        	</p>
	                        	</div>
	                   		</div>
                   		</div>
                   		
                   		<div class="inputRELATIVE" style="padding-top: 30px;">
                        	<h3>Attachment</h3>
                        	<div class="upload" style="margin-bottom: 20px; margin-top: 5px;">
                            	<?php e($form->file('Resume1.resume_location', array("size" => 30, 'style' => 'height:25px;padding-top: 4px;margin-top: 5px;')));?>
					   		</div>
					   		<div class="showHINT" style="height: 90px; margin-top: 45px; right: 183px; ">
                        		<span></span><!--for left arrow show on hover-->
                        		<p style="text-align: left; padding-top: 0px; margin-top: 10px; padding-bottom: 0px; padding: 10px 0px; line-height: 140%; font-size: 14px; color: white !important;">
									Upload your RFP or Subcontractor Agreement (PDF, 2MB limit)
                        		</p>
                    		</div>
                    		<?php if(!empty($this->data['Project']) && isset($this->data['Project']['filepath']) && $this->data['Project']['filepath'] != ''){?>
                    			<?php 
                            	$img = SITE_URL."img/".MENTEES_PROJECT_PATH.DS.$this->data['Project']['id'].DS.$this->data['Project']['filepath']; ?>
                            	
                            	<p><span style="font-size:14px; color: #000;"> Attached:  <?php e($html->link($this->data['Project']['filepath'],$img,array('escape'=>true , 'target'=>'_blank'))); ?> </span></p>
                    			<?php }?>
                   		</div>
					  	<span id="resume_old"  class="errormsg"></span> 

		</div>  	
                   		<div id="Apply" style="float: right; margin-bottom:30px; " role="Apply">

							<input id="apply" class="submitProfile1" type="submit" value="Preview">

						</div>

					</div>
			   <div style="margin-left:425px; margin-bottom:30px; ">
                               <a href="javascript: void(0)" id="hidelink" style="display: none;" class="hideoptional">Hide optional fields</a>   
                 		
                   		</div>
				</div>
				</form>
    		</div>
    		<div id="mentor-detail-right">
    			<div style="margin-top: 90px; border-top: 1px solid #F13031;">
    				<p style="font-weight: bold; font-size: 18px; border-bottom: 1px solid rgb(188, 188, 188); padding-top: 18px; padding-bottom: 5px;">How does it work?</p>
    				<p style="padding-top: 10px;"><span style="font-weight: bold;">1. Post your project. In minutes.</span></br></br>Post your project using our simple, 1-page form.</p>
    				
    				<p style="padding-top: 10px;"><span style="font-weight: bold;">2. Review applicants. For free.</span></br></br>You'll start receiving applications from relevant, pre-screened professionals. Conduct free consultations to select the candidate of your choice.</p>
    				<p style="padding-top: 10px;"><span style="font-weight: bold;">3. Get work done. With minimum overheads.</span></br></br>Our admin staff and support tools keep your overheads to a minimum, so you and your chosen professional can focus on the project.</p>
                            
    			       <p style="font-weight: bold; font-size: 18px; border-bottom: 1px solid rgb(188, 188, 188); padding-top: 18px; padding-bottom: 5px;">Not ready to post yet?</p>
    				<p style="padding-top: 10px;">Browse <a href="<?php e(SITE_URL)?>">our consultants</a> or get <a href="<?php e(SITE_URL.'qanda')?>">free advice</a>.</p></br></br>
    			</div>
    		</div>
    	</div>	
    </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){

	jQuery( "#project_start_date" ).datepicker({minDate:0});

	function updateCountTitle ()
    {
        var qText = jQuery("#project_title").val();

       if(qText.length < 128) {
           jQuery("#noOfCharTitle").html(128 - qText.length);
       } else {
           jQuery("#noOfCharTitle").html(0);
           jQuery("#project_title").val(qText.substring(0,128));
       }
    }

    jQuery("#project_title").keyup(function () {
    	updateCountTitle();
    });
    jQuery("#project_title").keypress(function () {
    	updateCountTitle();
    });
    jQuery("#project_title").keydown(function () {
    	updateCountTitle();
    });

	function updateCountDetails ()
    {
        var qText = jQuery("#project_details").val();

       if(qText.length < 5000) {
           jQuery("#noOfCharDetails").html(5000 - qText.length);
       } else {
           jQuery("#noOfCharDetails").html(0);
           jQuery("#project_details").val(qText.substring(0,5000));
       }
    }

    jQuery("#project_details").keyup(function () {
    	updateCountDetails();
    });
    jQuery("#project_details").keypress(function () {
    	updateCountDetails();
    });
    jQuery("#project_details").keydown(function () {
    	updateCountDetails();
    });

    
	var flag1 = 0;
	jQuery("#Resume1ResumeLocation").change(function () {

		var uploadedFile=jQuery("#Resume1ResumeLocation").val();
		if(jQuery.trim(uploadedFile!='')) {	
		var resumeArr = uploadedFile.split(".");			
		var ext = ["pdf" ]; // Creating Extension array 
			if(jQuery.inArray(resumeArr[1], ext)==-1){ // Checking value of extension
				jQuery("#Resume1ResumeLocation").css('border','1px solid #F00');
				jQuery("#resume_old").css('color','#F00');
				jQuery("#resume_old").html("Upload only "+ext);	
				flag1++;
			}else{
				jQuery('#Resume1ResumeLocation').css('border-color', '');
				jQuery("#resume_old").html('');
				var iSize = (jQuery("#Resume1ResumeLocation")[0].files[0].size / 1024);				
				var res = ValidateFileSize(iSize,uploadedFile);
				if(!res){
					flag1++;
				}else{
					flag1 = 0;
				}
			}
		}else{
			flag1 = 0;
		}
								
	});

	function ValidateFileSize(iSize,Filename){
		var size=Math.round((iSize / 1024) * 100) / 100;
		if (size <2)
		{
					
			jQuery("#resume_old").html('');
			jQuery('#Resume1ResumeLocation').css('border-color', '');
			return true;
		}else{
			var ErrorMessage = "2MB size limit";
			
			jQuery("#resume_old").html(ErrorMessage);
			jQuery("#resume_old").css('color','#F00')
			jQuery("#Resume1ResumeLocation").css('border','1px solid #F00');
			return false;
		}

	}
	
	
	jQuery(".submitProfile").click(function(){

	   var title = jQuery("#project_title").val();
	   var details = jQuery("#project_details").val();

	   var flag = 0;
   		if(jQuery.trim(title) == '') {
			jQuery('#project_title').css('border-color','#F00');
			jQuery('#project_title').focus();
			flag++;
		} else {
			jQuery('#project_title').css('border-color',''); 
		}

   		if(jQuery.trim(details) == '') {
			jQuery('#project_details').css('border-color','#F00');
			jQuery('#project_details').focus();
			flag++;
		} else {
			jQuery('#project_details').css('border-color','');
		}

		if(flag > 0) {
			return false;
		} else {
		       jQuery.ajax({
					url:SITE_URL+'project/add_project',
					type:'post',
					dataType:'json',
					data: 'title='+title + '&details=' + details + '&url=' + url,
					success:function(id){
						window.location.href = SITE_URL+'project/view/id';
					}
				});	
		       
		   		return false;
		}
    });

	jQuery(".submitProfile1").click(function(){

	   var title = jQuery("#project_title").val();
	   var details = jQuery("#project_details").val();
      var businessneed = jQuery("#business_need").val();
      var solutiontype = jQuery("#solution_type").val();

	   var flag = 0;
   		if(jQuery.trim(title) == '') {
			jQuery('#project_title').css('border-color','#F00');
			jQuery('#project_title').focus();
			flag++;
		} else {
			jQuery('#project_title').css('border-color',''); 
		}

   		if(jQuery.trim(details) == '') {
			jQuery('#project_details').css('border-color','#F00');
			jQuery('#project_details').focus();
			flag++;
		} else {
			jQuery('#project_details').css('border-color','');
		}

   		if(jQuery.trim(businessneed) == "Please select...") {
			jQuery('#business_need').css('border-color','#F00');
			jQuery('#business_need').focus();
			flag++;
		} else {
			jQuery('#business_need').css('border-color','');
		}
   		if(jQuery.trim(solutiontype) == "Please select...") {
			jQuery('#solution_type').css('border-color','#F00');
			jQuery('#solution_type').focus();
			flag++;
		} else {
			jQuery('#solution_type').css('border-color','');
		}
		if(flag > 0) {
			return false;
		} else {
		      flag = 0;
		}
    });

	jQuery(".showoptional").click(function() {
		document.getElementById('optional').style.display = 'block';
		document.getElementById('optionallink').style.display = 'none';
		document.getElementById('hidelink').style.display = 'block';
	});
	jQuery(".hideoptional").click(function() {
		document.getElementById('optional').style.display = 'none';
		document.getElementById('optionallink').style.display = 'block';
		document.getElementById('hidelink').style.display = 'none';
	});
	
});
</script>
<style>
textarea {resize:vertical}


</style>
<script type="text/javascript">
$(document).ready(function() {
   $('#business_need').css('color','gray');
   $('#business_need').change(function() {
      var current = $('#business_need').val();
      if (current != "Please select...") {
          $('#business_need').css('color','black');
      } else {
          $('#business_need').css('color','gray');
      }
   }); 

   $('#solution_type').css('color','gray');
   $('#solution_type').change(function() {
      var current = $('#solution_type').val();
      if (current != "Please select...") {
          $('#solution_type').css('color','black');
      } else {
          $('#solution_type').css('color','gray');
      }
   });

});
</script>