  <link rel="stylesheet" href="<?php e(SITE_URL)?>css/project.css">
    
   <div id="inner-content-wrapper">
     <div id="inner-content-box" class="pagewidth">
        <div class="mentor_details"><!-- VL 22/12-->
			<div id="mentor-detail-left1">
			
				<form id="Appyform" enctype="multipart/form-data" method="post" action="<?php echo SITE_URL ?>project/finalise" accept-charset="utf-8">
				 <div id="area-box">
				 
					<div class="expertise" style="padding-top:30px;min-height: 610px;">
					
						<div style="display: block; height: 60px; margin-bottom: 20px; border-bottom: 1px solid rgb(188, 188, 188);">
							<div style="float: left;">
								<h1 style="font-size: 20px; border: 0px;">Post a project</h1>
								<sub class="MGSub">Free & confidential</sub> 
							</div>

						</div>
						
						<?php e($form->hidden('Project.id'));?>
						
				        <div class="expertise">
                        	<h2>Project title</h2>
                        	<p><?php e($this->data['Project']['title']);?></p>
                   		</div>
                   		

				        <div class="expertise">
                        	<h2>Business need</h2>
                        	<p><?php e($this->data['Project']['business_need']);?></p>
                   		</div>

				        <div class="expertise">
                        	<h2>Solution type</h2>
                        	<p><?php e($this->data['Project']['solution_type']);?></p>
                   		</div>

                   		<div class="expertise">
                        	<h2>Describe your need</h2>
                        	<p>
                        		<?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['details']); 
								echo $this->General->make_links(nl2br($details));?>
							</p>
                   		</div>
                   		
                   		<?php if(isset($this->data['Project']['success']) && $this->data['Project']['success'] != '' ) {?>
						<div class="expertise">
                        	<h2>How will you define success of the project?</h2>
                        	<p>
                        		<?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['success']); 
								echo $this->General->make_links(nl2br($details));?>
							</p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['budget']) && $this->data['Project']['budget'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Approximate Budget $ </h2>
                        	<?php if($this->data['Project']['budget_type'] == 'Hourly'){?>
                        		<p><?php e($this->data['Project']['budget']." Per Hour");?></p>
                        	<?php }else{?>
                        		<p><?php e($this->data['Project']['budget']);?></p>
                        	<?php }?>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['location']) && $this->data['Project']['location'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Location preference</h2>
                        	<p><?php e($this->data['Project']['location']);?></p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['duration']) && $this->data['Project']['duration'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Estimated duration</h2>
                        	<p><?php e($this->data['Project']['duration']);?></p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['start_date']) && $this->data['Project']['start_date'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Target start date</h2>
                        	<p><?php e($this->data['Project']['start_date']);?></p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['filepath']) && $this->data['Project']['filepath'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Attachment</h2>
                        	<div class="upload">
                            	<?php 
                            	$img = SITE_URL."img/".MENTEES_PROJECT_PATH.DS.$this->data['Project']['id'].DS.$this->data['Project']['filepath']; ?>
                            	
                            	<p><span style="font-size:14px;"> <?php e($html->link($this->data['Project']['filepath'],$img,array('escape'=>true , 'target'=>'_blank'))); ?> </span></p>
					   		</div>
                   		</div>
                   		<?php }?>
                   		<?php if($this->Session->read('Auth.User.id') == '') {?>
						
						<div id="oldButtonDiv" style="float: right; margin-bottom: 30px">
				            <?php  e($html->link("Edit",SITE_URL.'project/create/'.$this->data['Project']['id'],array('escape'=>false)));?>
					    	<input style="float: none; margin-left: 15px; font-weight: normal;" value="Post a project" type="button" class="mgButton" id="previewButton">
			            </div>
					</div>
				</div>
				</form>

				  <div style="margin-left: 260px; margin-top: 20px; display: none;" id="regDivText">
					<span style="font-weight: bold; margin-right: 10px; margin-top: 18px;"><img src="<?php e(SITE_URL);?>img/media/arrow.png"></img> To continue, Register or Submit as a guest</span>
				  </div>
				   
				  <div id="linkedinDiv" style="display: none; margin-top: 20px; width: 850px;" class="pagewidth">
					<div style="float: left; width: 42%;" >
						<div> 
							<div class="submitProfile" style="cursor:pointer; margin-top: 95px;">
								
								<?php e($html->image('linked-in.png',array('alt'=>'Linked in','style'=>'cursor:pointer;'))); ?>
							</div>
						</div>
						<div style="display: block; font-size: 11px; margin-bottom:80px; margin-top: 10px; float: right;">Mentors Guild will <span style="font-weight: bold;">never </span>post to your LinkedIn account without your permission.</div>
					</div>
					
					<div class="vrtLINE"></div>
					<div style="float: right; width: 49%;" >
						<div style="margin-top: 20px;"> 
							      <?php
	      							echo $form->create(null,array('url'=>array('controller'=>'fronts','action'=>'new_project_request'),'id'=>'newmentorshipForm'));
	      							?>
	      							<table>
	      								<tbody>
	      									<tr>
	      										<td style="float: right; margin-top: 16px; margin-right: 10px;">Name</td>
	      										<td>
	      											<input name="data[Mentorship][firstname]" type="text" class="forminput" id="userfname" value="" maxlength="120" placeholder="First" style="width:153px;">
													<input name="data[Mentorship][lastname]" type="text" class="forminput" id="userlname" value="" maxlength="120" placeholder="Last" style="width:153px;">
	      										</td>
	      									</tr>
	      									<tr>
	      										<td style="float: right; margin-top: 10px; margin-left: 5px;">Work email</td>
	      										<td>
													<input name="data[Mentorship][email]" type="text" class="forminput" placeholder = "email@yourcompany.com" id="useremail" value="" maxlength="250" style="width:332px;">      										
												</td>
	      									</tr>
	      									<tr>
	      										<td style="float: right; margin-top: 16px; margin-right: 10px;">Phone</td>
	      										<td>
													<input name="data[Mentorship][phone]" type="text" class="forminput" placeholder = "(xxx) xxx-xxxx" id="userphone" value="" maxlength="250" style="width:332px;">      										
												</td>
	      									</tr>
	      								</tbody>
	      							</table>
								
									<input name="data[Project][phone_number]" type="hidden" value="" id="phonenumber">
									<?php e($form->hidden('Project.id'));?>
								
						</div>
						<div style="float: right;" >
							<span style="float:left;padding:11px 18px;"><?php e($html->link('Edit',SITE_URL.'project/create/'.$this->data['Project']['id'],array('class'=>'reset','style'=>""))); ?></span>	
			                    <input id="sendButton" type="submit" value="Post" class="mgButton" style="margin-bottom: 20px; float: right;margin-right: 5px;">
						</div>	
	
						</form>

                                        

<?php } else {?>
						<div style="float: right;" >
							<span style="float:left;padding:11px 18px;"><?php e($html->link('Edit',SITE_URL.'project/create/'.$this->data['Project']['id'],array('class'=>'reset','style'=>""))); ?></span>	
			                <input id="mgbutton" type="submit" value="Post a project" class="mgButton" style="margin-bottom: 20px; float: right;margin-right: 5px;">
						</div>	

<?php }?>
					</div>	
						</div>
    		</div>
    		<div id="mentor-detail-right">
    			<div style="margin-top: 90px; border-top: 1px solid #F13031;">
    				<p style="font-weight: bold; font-size: 18px; border-bottom: 1px solid rgb(188, 188, 188); padding-top: 18px; padding-bottom: 5px;">How does it work?</p>
    				<p style="padding-top: 10px;"><span style="font-weight: bold;">1. Post your project. In minutes.</span></br></br>Post your project using our simple, 1-page form.</p>
    				
    				<p style="padding-top: 10px;"><span style="font-weight: bold;">2. Review applicants. For free.</span></br></br>You'll start receiving applications from relevant, pre-screened professionals. Conduct free consultations to select the candidate of your choice.</p>
    				<p style="padding-top: 10px;"><span style="font-weight: bold;">3. Get work done. With minimum overheads.</span></br></br>Our admin staff and support tools keep your overheads to a minimum, so you and your chosen professional can focus on the project.</p>
                            
    			       <p style="font-weight: bold; font-size: 18px; border-bottom: 1px solid rgb(188, 188, 188); padding-top: 18px; padding-bottom: 5px;">Not ready to post yet?</p>
    				<p style="padding-top: 10px;">Browse <a href="<?php e(SITE_URL)?>">our consultants</a> or get <a href="<?php e(SITE_URL.'qanda')?>">free advice</a>.</p></br></br>
    			</div>
    		</div>

    	</div>	
    </div>
</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){

});

	jQuery(".submitProfile").click(function(){

       var projectId = jQuery('#ProjectId').val();
		window.location.href = SITE_URL+'linkedin/signuplinkclientpopup.php?projectId='+projectId;
    });

    	jQuery("#mgbutton").click(function(){

           jQuery("#Appyform").submit();
    });
    		jQuery("#previewButton").click(function() {
    			document.getElementById('linkedinDiv').style.display = 'inline-block';
    			document.getElementById('oldButtonDiv').style.display = 'none';
    			document.getElementById('regDivText').style.display = 'block';
    			jQuery('html, body').animate({
    		        scrollTop: jQuery("#linkedinDiv").offset().top
    		    }, 2000);
    		});
    		
    		jQuery("#sendButton").click(function() {
                            
                var flag = 0;
				
		      if(jQuery.trim(jQuery("#userfname").val()) == '') {
                    jQuery('#userfname').css('border-color','#F00');
                    jQuery('#userfname').focus();                
                    flag++;
                    }else {
                    jQuery('#userfname').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#userlname").val()) == '') {
                    jQuery('#userlname').css('border-color','#F00');
                    jQuery('#userlname').focus();                
                    flag++;
                    }else {
                    jQuery('#userlname').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#useremail").val()) == '') {
                    jQuery('#useremail').css('border-color','#F00');
                    jQuery('#useremail').focus();                
                    flag++;
                    }else {
                    jQuery('#useremail').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#userphone").val()) == '') {
                    jQuery('#userphone').css('border-color','#F00');
                    jQuery('#userphone').focus();                
                    flag++;
                    }else {
                    jQuery('#userphone').css('border-color','');                 
                    }
    			if(flag > 0) {
    				return false;
    			} else {

				var answer = jQuery.trim(jQuery("#phone_number").val());
				jQuery("#phonenumber").val(answer);

				jQuery("#newmentorshipForm").submit();
                     }
    		});
    		
    		
</script>
