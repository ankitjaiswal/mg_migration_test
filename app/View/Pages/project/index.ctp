<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth" style="margin-bottom:50px;">
        <div class="mentor_details"><!-- VL 22/12-->
			<div id="mentor-detail-left1">
				<div id="area-box">
					<div class="expertise" style="padding-top:15px;">
						<?php e($form->hidden('Project.id'));?>
						
				<div class="expertise">
                        	<h2>Project title</h2>
                        	<p><?php e($this->data['Project']['title']);?></p>
                   		</div>
                   		
				<div class="expertise">
                        	<h2>Business need</h2>
                        	<p><?php e($this->data['Project']['business_need']);?></p>
                   		</div>

				<div class="expertise">
                        	<h2>Solution type</h2>
                        	<p><?php e($this->data['Project']['solution_type']);?></p>
                   		</div>

                   		<div class="expertise">
                        	<h2>Project details</h2>
                        	<p>
                        		<?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['details']); 
								echo $this->General->make_links(nl2br($details));?>
							</p>
                   		</div>

                   		<?php if(isset($this->data['Project']['success']) && $this->data['Project']['success'] != '' ) {?>
						<div class="expertise">
                        	<h2>Project goal</h2>
                        	<p>
                        		<?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['success']); 
								echo $this->General->make_links(nl2br($details));?>
							</p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['budget']) && $this->data['Project']['budget'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Approximate Budget $ </h2>
                        	<p><?php e($this->data['Project']['budget']);?></p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['location']) && $this->data['Project']['location'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Location preference</h2>
                        	<p><?php e($this->data['Project']['location']);?></p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['duration']) && $this->data['Project']['duration'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Estimated duration</h2>
                        	<p><?php e($this->data['Project']['duration']);?></p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['start_date']) && $this->data['Project']['start_date'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Target start date</h2>
                        	<p><?php e($this->data['Project']['start_date']);?></p>
                   		</div>
                   		<?php }?>
                            <?php  if($this->Session->read('Auth.User.role_id')== Configure::read('App.Role.Mentee') || $this->Session->read('Auth.User.role_id')== '1' || ($this->Session->read('Auth.User.role_id')==Configure::read('App.Role.Mentor') && $this->Session->read('Auth.User.id') == $this->data['Project']['user_id'])) {                		
                   		if(isset($this->data['Project']['phone_number']) && $this->data['Project']['phone_number'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Phone number</h2>
                        	<p><?php e($this->data['Project']['phone_number']);?></p>
                   		</div>
                   		<?php } }?>

                   		<?php if(isset($this->data['Project']['filepath']) && $this->data['Project']['filepath'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Attachment</h2>
                        	<div class="upload">
                            	<?php 
                            	$img = SITE_URL."img/".MENTEES_PROJECT_PATH.DS.$this->data['Project']['id'].DS.$this->data['Project']['filepath']; ?>
                            	
                            	<p><span style="font-size:14px;"> <?php e($html->link($this->data['Project']['filepath'],$img,array('escape'=>true , 'target'=>'_blank'))); ?> </span></p>
					   		</div>
                   		</div>
                   		<?php }?>
                   		

                   		<?php  if($this->Session->read('Auth.User.role_id')==Configure::read('App.Role.Admin')) {?>
	                   		<?php if(!empty($assignedmentorsArray)){?>
	                   		<div class="expertise">
	                        	<h2>Assigned consultants</h2>
	                        	
			                   		<?php foreach ($assignedmentorsArray as $value) {

			                   				if(isset($value['UserImage'][0]['image_name']) && $value['UserImage'][0]['image_name'] !=''){
				                   			$image_path = MENTORS_IMAGE_PATH.DS.$value['User']['id'].DS.$value['UserImage'][0]['image_name'];
				                   		} else {
											$image_path = 'media/profile.png';
				                   		}?>
                                                        
			                   			<p>
				            				<img src="<?php e(SITE_URL.'img/'.$image_path)?>" width="30" height="30" alt="<?php e($value['UserReference']['first_name']." ".$value['UserReference']['last_name']);?>" title="<?php e($value['UserReference']['first_name']." ".$value['UserReference']['last_name']);?>"/>
				                   			<?php e($html->link(ucfirst($value['UserReference']['first_name']).' '.ucfirst($value['UserReference']['last_name']),'javascript:openProfilePopup('.$value['User']['id'].');',array('escape'=>false))); ?>

                                                      </p>
                                                 <?php }?>

			                   		
		                   		</div>
	                   		
	                   		<?php }?>


	                   		<?php if(!empty($mentorsArray)){?>
	                   		<div class="expertise">
	                        	<h2>Interested consultants</h2>
	                        	<div>
			                   		<?php foreach ($mentorsArray as $value) {
			                   			$consultant = ClassRegistry::init('Project_member_confirm');
                                                        $show_interest = $consultant->find('first', array('conditions' => array('AND' =>array('Project_member_confirm.project_id ' => $this->data['Project']['id'] ,'Project_member_confirm.member_id' => $value['User']['id']))));

                                                       if($show_interest['Project_member_confirm']['status'] == '0' || $show_interest['Project_member_confirm']['status'] == '1'){
				                   		if(isset($value['UserImage'][0]['image_name']) && $value['UserImage'][0]['image_name'] !=''){
				                   			$image_path = MENTORS_IMAGE_PATH.DS.$value['User']['id'].DS.$value['UserImage'][0]['image_name'];
				                   		} else {
											$image_path = 'media/profile.png';
				                   		}?>
                                                        <div id="Ignore<?php echo $show_interest['Project_member_confirm']['id']?>"> 
			                   			<p>
				            				<img src="<?php e(SITE_URL.'img/'.$image_path)?>" width="30" height="30" alt="<?php e($value['UserReference']['first_name']." ".$value['UserReference']['last_name']);?>" title="<?php e($value['UserReference']['first_name']." ".$value['UserReference']['last_name']);?>"/>
				                   			<?php e($html->link(ucfirst($value['UserReference']['first_name']).' '.ucfirst($value['UserReference']['last_name']),'javascript:openProfilePopup('.$value['User']['id'].');',array('escape'=>false))); ?>

                                                      </p>
                                                      </div>
			                   		<?php e($form->hidden('Project.User.'.$value['User']['id'],array('value'=>$value['User']['id'])));
			                   		} }?>
		                   	  </div>
	                   		</div>
	                   		<?php }?>

	                   		<?php if(!empty($mentorsArray)){?>
	                   		<div class="expertise">
	                        	<h2>Requested for consultation</h2>
	                        	<div>
			                   		<?php foreach ($mentorsArray as $value) {
			                   			$consultant = ClassRegistry::init('Project_member_confirm');
                                                        $show_interest = $consultant->find('first', array('conditions' => array('AND' =>array('Project_member_confirm.project_id ' => $this->data['Project']['id'] ,'Project_member_confirm.member_id' => $value['User']['id']))));

                                                       if($show_interest['Project_member_confirm']['status'] == '1'){
				                   		if(isset($value['UserImage'][0]['image_name']) && $value['UserImage'][0]['image_name'] !=''){
				                   			$image_path = MENTORS_IMAGE_PATH.DS.$value['User']['id'].DS.$value['UserImage'][0]['image_name'];
				                   		} else {
											$image_path = 'media/profile.png';
				                   		}?>
                                                        <div id="Ignore<?php echo $show_interest['Project_member_confirm']['id']?>"> 
			                   			<p>
				            				<img src="<?php e(SITE_URL.'img/'.$image_path)?>" width="30" height="30" alt="<?php e($value['UserReference']['first_name']." ".$value['UserReference']['last_name']);?>" title="<?php e($value['UserReference']['first_name']." ".$value['UserReference']['last_name']);?>"/>
				                   			<?php e($html->link(ucfirst($value['UserReference']['first_name']).' '.ucfirst($value['UserReference']['last_name']),'javascript:openProfilePopup('.$value['User']['id'].');',array('escape'=>false))); ?>

                                                      </p>
                                                      </div>
			                   		<?php e($form->hidden('Project.User.'.$value['User']['id'],array('value'=>$value['User']['id'])));
			                   		} }?>
		                   	  </div>
	                   		</div>
	                   		<?php }?>

	                   		<?php if(!empty($mentorsArray)){?>
	                   		<div class="expertise">
	                        	<h2>Ignored consultants</h2>
	                        	<div>
			                   		<?php foreach ($mentorsArray as $value) {
			                   			$consultant = ClassRegistry::init('Project_member_confirm');
                                                        $show_interest = $consultant->find('first', array('conditions' => array('AND' =>array('Project_member_confirm.project_id ' => $this->data['Project']['id'] ,'Project_member_confirm.member_id' => $value['User']['id']))));

                                                       if($show_interest['Project_member_confirm']['status'] == '-1' ){
				                   		if(isset($value['UserImage'][0]['image_name']) && $value['UserImage'][0]['image_name'] !=''){
				                   			$image_path = MENTORS_IMAGE_PATH.DS.$value['User']['id'].DS.$value['UserImage'][0]['image_name'];
				                   		} else {
											$image_path = 'media/profile.png';
				                   		}?>
                                                        <div id="Ignore<?php echo $show_interest['Project_member_confirm']['id']?>"> 
			                   			<p>
				            				<img src="<?php e(SITE_URL.'img/'.$image_path)?>" width="30" height="30" alt="<?php e($value['UserReference']['first_name']." ".$value['UserReference']['last_name']);?>" title="<?php e($value['UserReference']['first_name']." ".$value['UserReference']['last_name']);?>"/>
				                   			<?php e($html->link(ucfirst($value['UserReference']['first_name']).' '.ucfirst($value['UserReference']['last_name']),'javascript:openProfilePopup('.$value['User']['id'].');',array('escape'=>false))); ?>

                                                      </p>
                                                      </div>
			                   		<?php e($form->hidden('Project.User.'.$value['User']['id'],array('value'=>$value['User']['id'])));
			                   		} }?>
		                   	  </div>
	                   		</div>
	                   		<?php }?>


                   		<?php }?>

                   		<?php  if($this->Session->read('Auth.User.role_id')==Configure::read('App.Role.Mentee') || ($this->Session->read('Auth.User.role_id')==Configure::read('App.Role.Mentor') && $this->Session->read('Auth.User.id') == $this->data['Project']['user_id'])) {?>
	                   		<?php if(!empty($mentorsArray)){?>
	                   		<div class="expertise">
	                        	<h2>Interested consultants</h2>
	                        	<div>
			                   		<?php foreach ($mentorsArray as $value) {
			                   			$consultant = ClassRegistry::init('Project_member_confirm');
                                                        $show_interest = $consultant->find('first', array('conditions' => array('AND' =>array('Project_member_confirm.project_id ' => $this->data['Project']['id'] ,'Project_member_confirm.member_id' => $value['User']['id']))));

                                                       if($show_interest['Project_member_confirm']['status'] == '0' || $show_interest['Project_member_confirm']['status'] == '1'){
				                   		if(isset($value['UserImage'][0]['image_name']) && $value['UserImage'][0]['image_name'] !=''){
				                   			$image_path = MENTORS_IMAGE_PATH.DS.$value['User']['id'].DS.$value['UserImage'][0]['image_name'];
				                   		} else {
											$image_path = 'media/profile.png';
				                   		}?>
                                                        <div id="Ignore<?php echo $show_interest['Project_member_confirm']['id']?>"> 
			                   			<p>
				            				<img src="<?php e(SITE_URL.'img/'.$image_path)?>" width="30" height="30" alt="<?php e($value['UserReference']['first_name']." ".$value['UserReference']['last_name']);?>" title="<?php e($value['UserReference']['first_name']." ".$value['UserReference']['last_name']);?>"/>
				                   			<?php e($html->link(ucfirst($value['UserReference']['first_name']).' '.ucfirst($value['UserReference']['last_name']),'javascript:openProfilePopup('.$value['User']['id'].');',array('escape'=>false))); ?>
                                                        <?php if($show_interest['Project_member_confirm']['status'] == '0'){?>
					                   	 <div id="Apply" style="float: right; margin-top:-40px; " role="Apply">
									<a  style="margin-right:10px;" href="javascript:ignoreMember('<?php echo $show_interest['Project_member_confirm']['id']?>')">Ignore</a>
									<input id="applyNext" class="mgButton" style="width: 230px;font-weight:normal;" type="Button" value="Request a Free Consultation" onclick="javascript:consultation_request(<?php e($show_interest['Project_member_confirm']['id']); ?>); ">
                                                                             
								 </div>
                                                      <?php }?>
                                                      </p>
                                                      </div>
			                   		<?php e($form->hidden('Project.User.'.$value['User']['id'],array('value'=>$value['User']['id'])));
			                   		} }?>
		                   		</div>
	                   		</div>
	                   		<?php }?>
                   		<?php }?>





					</div>
					<?php  if($this->Session->read('Auth.User.role_id')==Configure::read('App.Role.Mentee')) {?>
						<div id="Apply" style="float: right; margin-bottom:30px; " role="Apply">
							<input id="applyNext" class="mgButton" type="Button" value="Back" onclick="window.location.href='<?php echo SITE_URL."project/projectlistclient"; ?>'; ">
						</div>

					<?php } else if($this->Session->read('Auth.User.role_id')==Configure::read('App.Role.Mentor') && $this->Session->read('Auth.User.id') == $this->data['Project']['user_id']) {?>
						<div id="Apply" style="float: right; margin-bottom:30px; " role="Apply">
							<input id="applyNext" class="mgButton" type="Button" value="Back" onclick="window.location.href='<?php echo SITE_URL."project/projectlistmember"; ?>'; ">
						</div>
					<?php } else if($this->Session->read('Auth.User.role_id')==Configure::read('App.Role.Mentor') && $this->Session->read('Auth.User.id') != $this->data['Project']['user_id']) {?>
			                       <?php 




                                                      if (empty($mentorsArray)){?>
							      <div id="Apply" style="float: right; margin-bottom:30px; " role="Apply">
								<?php e($html->link('Ignore',SITE_URL."users/my_account",array('escape'=>false, 'style'=>'margin-right:10px;'))); ?>
								<input id="applyNext" class="mgButton" style="width: 130px;" type="Button" value="Show interest" onclick="window.location.href='<?php echo SITE_URL."project/showInterest/".$this->Session->read('Auth.User.id')."/".$this->data['Project']['id']; ?>';">
							      </div>

                                                      <?php }else if($count == 0){?>
							      <div id="Apply" style="float: right; margin-bottom:30px; " role="Apply">
								<?php e($html->link('Ignore',SITE_URL."users/my_account",array('escape'=>false, 'style'=>'margin-right:10px;'))); ?>
								<input id="applyNext" class="mgButton" style="width: 130px;" type="Button" value="Show interest" onclick="window.location.href='<?php echo SITE_URL."project/showInterest/".$this->Session->read('Auth.User.id')."/".$this->data['Project']['id']; ?>';">
							      </div>
						            <?php }else {?>
							<div id="Apply" style="float: right; margin-bottom:30px; " role="Apply">
								<input id="closeButton" value="Done" class="submitProfile" type="submit" style="margin-bottom:10px; margin-left: 10px;" onclick="window.location.href='<?php echo SITE_URL."project/projectlistmember"; ?>'; ">
							</div>

						<?php }?>
					<?php } else if($this->Session->read('Auth.User.role_id')==Configure::read('App.Role.Admin')&& $this->data['Project']['status'] != '4' ) {?>
					<div id="Apply" style="float: right; margin-bottom:30px; " role="Apply">
							<?php e($html->link('Back',SITE_URL."admin/project/review",array('escape'=>false, 'style'=>'margin-right:10px;'))); ?>
							<?php e($html->link('Disapprove',SITE_URL."admin/project/disapproveproject/".$this->data['Project']['id'],array('escape'=>false, 'style'=>'margin-right:10px;'))); ?>
							<input id="applyNext" class="mgButton" style="width: 130px;" type="Button" value="Approve" onclick="window.location.href='<?php echo SITE_URL."admin/project/approveproject/".$this->data['Project']['id']; ?>'; ">
						</div>
					<?php } ?>
				</div>
    		</div>
    		

               <?php  $for_creator = ClassRegistry::init('User'); 	
                      $creator = $for_creator->find('first', array('conditions' => array('User.id' => $this->data['Project']['user_id'])));       	  
                      if($creator['User']['role_id'] == Configure::read('App.Role.Mentor')){?>

		<div id="mentor-detail-right" style="margin-bottom: 20px; display: block;">
    			<div class="expertise" style="margin-top: 15px;">
	    			  <?php
						$displink1=SITE_URL.strtolower($createdBy['User']['url_key']);
					  ?>
                     <h2>Created By: <?php  e($html->link($createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'],$displink1,array('escape'=>false, 'target'=>'_blank')));?></h2>
                     <div class="profileimg" style="margin-top: 15px;">
					<?php 
					if(isset($createdBy['UserImage'][0]['image_name']) && $createdBy['UserImage'][0]['image_name'] !=''){
							$image_path = MENTORS_IMAGE_PATH.DS.$createdBy['User']['id'].DS.$createdBy['UserImage'][0]['image_name'];
							e($html->link($html->image($image_path, array('class'=>'desaturate','alt' => $createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'])),$displink1,array('escape'=>false, 'target'=>'_blank','style'=>'border: 1px solid #DCDCDC; display:block; width:150px;')));
					}else{
							e($html->link($html->image('media/profile.png', array('class'=>'desaturate','alt' => $createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'])),$displink1,array('escape'=>false, 'target'=>'_blank','style'=>'border: 1px solid #DCDCDC; display:block; width:150px;')));
					}?>
				</div>
                </div>
                <?php  $mentorshipData = $this->General->getMentorshipSessionsWithClient($createdBy['User']['id']);
                	if (empty($mentorshipData) == false && $mentorshipData['Mentorship']['applicationType'] != 1) { ?>
                		<div class="apply-button-search" style="float: left; margin-bottom: 5px;">
		                      <input class="profilebtn" type="button" style="width:211px !important; margin-top: 10px;" value="Send Invoice" onclick="window.location.href='<?php echo SITE_URL."invoice/invoice_create/".$mentorshipData['Mentorship']['id']."/".$createdBy['User']['id']; ?>'; ">
		                </div> 
				<?php }
                ?>
			</div>
 
<?php } else{?>

		<div id="mentor-detail-right" style="margin-bottom: 20px; display: block;">
    			<div class="expertise" style="margin-top: 15px;">
	    			  <?php
						$displink1=SITE_URL."clients/my_account/".strtolower($createdBy['User']['url_key']);
					  ?>
                     <h2>Created By: <?php  e($html->link($createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'],$displink1,array('escape'=>false,'target'=>'_blank')));?></h2>
                     <div class="profileimg" style="margin-top: 15px;">
					<?php 
					if(isset($createdBy['UserImage'][0]['image_name']) && $createdBy['UserImage'][0]['image_name'] !=''){
							$image_path = MENTEES_IMAGE_PATH.DS.$createdBy['User']['id'].DS.$createdBy['UserImage'][0]['image_name'];
							e($html->link($html->image($image_path, array('class'=>'desaturate','alt' => $createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank','style'=>'border: 1px solid #DCDCDC; display:block; width:150px;')));
					}else{
							e($html->link($html->image('media/profile.png', array('class'=>'desaturate','alt' => $createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank','style'=>'border: 1px solid #DCDCDC; display:block; width:150px;')));
					}?>
				</div>
                </div>
			</div>
			<?php  }?>
    	</div>	

    </div>
</div>

<script type="text/javascript">
function close_window() {

	var ww = window.open(window.location, '_self'); 
	ww.close();
 }
</script>
    <script type="text/javascript">

     
function ignoreMember(confirm_id){
   if(confirm('Do you want to Ignore this member? This action can not be undone.')==true && confirm_id!='')
    {
		var ignore_id = 'Ignore'+confirm_id
		jQuery(document).ready(function(){
				jQuery.ajax({
					url:SITE_URL+'project/del_member',
					type:'post',
					dataType:'json',
					data:'confirm_id='+confirm_id,
					success:function(){
						document.getElementById(ignore_id).style.display = 'none';
					}
					});
                });
              }	
}
function consultation_request(confirm_id){

 		var ignore_id = 'Ignore'+confirm_id
		jQuery(document).ready(function(){
				jQuery.ajax({
					url:SITE_URL+'project/consultation_request_ajax',
					type:'post',
					dataType:'json',
					data:'confirm_id='+confirm_id,
					success:function(id){
                                           
						document.getElementById(ignore_id).style.display = 'none';
                                         window.location.href='<?php echo SITE_URL."project/consultation_request/".$this->Session->read('Auth.User.id')."/'+id+'/".$this->data['Project']['id']; ?>';
                                       
					}
					});
                });
             
}
</script>