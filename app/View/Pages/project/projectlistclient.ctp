<style>
#pagingarea span{ font-size:15px; padding:5px; }
</style>
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
     <div id="registerationFm" style="min-height: 450px;">
        <h1>My Projects</h1>
        
        <div class="feedbackcontent" style="padding-top: 12px;">
           <table width="100%" style="border:1px solid #CCCCCC;" cellspacing="2" cellpadding="10">
               <tr bgcolor="#2A2A2A" class="row_head">
                   
                   <td>Date</td>
                   <td>Project title</td>

                   <td>Status</td>
                   <td>Action</td>
               </tr>
           <?php if(!empty($data) && count($data)>0)
                {
                    $i=1; 
                  foreach($data as $project){
                      if($i%2==0)
                        $class = 'first_row';
                      else
                        $class = 'second_row'; 

        
           ?>
               <tr class="<?php echo $class; ?>">
                   
                   <td><?php echo date("m-d-Y", strtotime($project['Project']['created']));?></td>
                   <td><?php echo $project['Project']['title']; ?></td>

                   <?php if($project['Project']['status'] == 1) {   ?> 
                           <td>Project saved as draft</td>
                           <td><?php e($html->link('Complete now',array('controller'=>'project','action'=>'create/'.$project['Project']['id']),array('class'=>'delete','title'=>'Complete project'))); ?></td>
                    <?php } 
                         else if($project['Project']['status'] == 2) {?>
                           <td>Project created. To be posted</td>
                           <td><?php e($html->link('Post now',array('controller'=>'project','action'=>'view/'.$project['Project']['id']),array('class'=>'delete','title'=>'Edit and post project')));?></td>
                    <?php } else if($project['Project']['status'] == 3) { ?>
                           <td>Project posted. Pending approval</td>
                           <td><?php e($html->link('View',array('controller'=>'project','action'=>'index/'.$project['Project']['project_url']),array('class'=>'delete','title'=>'Edit and post project'))); ?></td>
                    <?php } else {?>
                    	   <td>Project approved</td>
                    	   <td><?php e($html->link('View',array('controller'=>'project','action'=>'index/'.$project['Project']['project_url']),array('class'=>'delete','title'=>'View project'))); ?></td>
                    <?php }?>
               </tr>
           <?php    
                    $i++;
                    }
           if($this->params['paging']['Project']['pageCount']>1)
           { ?>
             <tr id="pagingarea">
                <?php
                $paginator->options(array('update'=>'CustomerPaging','url'=>array('controller'=>'project', 'action'=>'projectlistclient'),'indicator' => 'LoadingDiv')); ?>
                <td colspan="6"><?php echo $paginator->prev('<< Previous', array('class' => 'previous-off'), null); ?><?php echo $paginator->numbers(array('separator'=>'','class'=>'pagingnum')); ?><?php echo $paginator->next('Next >>', array('class' => 'next next-off'), null); ?></td>
                
            </tr>
      <?php }
                 } else{?>
              <tr class="first_row" style="text-align: center;">
                   <td colspan="7">No record found</td>
               </tr>
               <?php } ?>
           </table>
        </div>
         </div>
      
    </div>
</div>
<style>
    .row_head{color: #EEEEEE; font-size: 14px; height: 30px; background: #2A2A2A; text-align:left;}  
    .first_row{color: #000000; font-size: 14px; height: 30px; background:#CCCCCC; text-align:left;}
    .second_row{color: #000000; font-size: 14px; height: 30px; text-align:left;    
</style>