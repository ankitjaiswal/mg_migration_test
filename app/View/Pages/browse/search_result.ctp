<script type="text/javascript">
mixpanel.track("Page View", {"URL": document.URL});
</script>
<?php header("Cache-control: public"); 

$cachefile = WWW_ROOT."cache/".$cache_keyword."-".$cache_adCity."-".$cache_adstate.".html";

if (file_exists($cachefile)) {
	include($cachefile); 
} else {
	ob_start();
	
?>
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
      <div id="financial-management">
       
           <p>
		<span><?php 
		$memCount = count($data);
		$dirCount = count($dUsersFinal);
              $totCount = $memCount + $dirCount;
              $i ='0'; 
              if((strpos($adCity, " ")!== false))
              {
               $city = explode(" ", $adCity); // splitting the city
               $i ='1';
              }
		if($keyword == ''){
              $keyword = "Experts near";
              if($i =='1')
              echo (ucfirst($keyword).' '.ucfirst($city[0]).' '.ucfirst($city[1]) .', '.$adstate);
              else
              echo (ucfirst($keyword).' '.ucfirst($adCity).', '.$adstate);
		}
              else if($keyword != '' && $keyword == 'executive coaching'){
              if($i =='1')
		echo (ucfirst($keyword).' in '.ucfirst($city[0]).' '.ucfirst($city[1]) .', '.$adstate);
              else	
              echo (ucfirst($keyword).' in '.ucfirst($adCity).', '.$adstate);
		}
              else{
               echo ucfirst($keyword);
              }
		?></span> 
		<span style="float: right; font-weight: normal;"> <?php e($totCount." experts");?></span>
		</p>
      </div>
       
      <div id="mentors-content">
		<?php
			e($this->element('front/search'));
		?>
      </div>
    </div>
  </div> 
<?php 
if(empty($data)){?>
 <script type="text/javascript">
	jQuery(window).bind('load resize',function(){
		if(jQuery("body").height() < jQuery(window).height()){
			jQuery("#footer-wrapper").css({'position':'fixed'})
		}
		
	});
 </script>
 <?php
}?>
<?php
	$fp = fopen($cachefile, 'w'); 
	
// save the contents of output buffer to the file
fwrite($fp, ob_get_contents()); 
// close the file
fclose($fp); 
// Send the output to the browser
ob_end_clean();

include($cachefile); 

}