<script type="text/javascript">
mixpanel.track("Page View", {"URL": document.URL});
</script>
<?php header("Cache-control: public"); 

$cachefile = WWW_ROOT."cache/cache-".$cache_keyword.".html";

if (file_exists($cachefile)) {
       
	include($cachefile); 
} else {
	ob_start();
	
?>
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
      <div id="financial-management">
        <p>
		<span><?php 
		$memCount = count($data);
		$dirCount = count($dUsersFinal);
		$totCount = $memCount + $dirCount;
		if($keyword == ''){
		echo "Browse all";
		}else{
		echo $keyword;	
		}
		?></span> 
		<span style="float: right; font-weight: normal;"> <?php e($totCount." experts");?></span>
		</p>
      </div>
      <div id="mentors-content">
		<?php
			e($this->element('front/search'));
		?>
      </div>
    </div>
  </div> 
<?php 
if(empty($data)){?>
 <script type="text/javascript">
	jQuery(window).bind('load resize',function(){
		if(jQuery("body").height() < jQuery(window).height()){
			jQuery("#footer-wrapper").css({'position':'fixed'})
		}
		
	});
 </script>
 <?php
}?>
<?php
	$fp = fopen($cachefile, 'w'); 
	
// save the contents of output buffer to the file
fwrite($fp, ob_get_contents()); 
// close the file
fclose($fp); 
// Send the output to the browser
ob_end_clean();

include($cachefile); 

}