<div class="fieldset">
    <h3 class="legend">
		Member Indexes
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <?php e($form->create('User', array('name' => 'Admin', 'url' => array('controller' => 'qna', 'action' => 'member_categories')))); ?>    
        <input type="hidden" name="pageAction" id="pageAction"/>	 
        <?php
        if (!empty($data)) {
            ?> 

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td width="20%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Member id</td>
                        <td width="11%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Member name</td>
                        <td width="23%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Index</td>
                    </tr>	
                    <?php
                    foreach ($data as $value) {
                        ?>
                        <tr>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php e(ucwords($value['User']['id'])); ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                               <?php e(ucwords($value['UserReference']['first_name']) . ' ' . $value['UserReference']['last_name']); ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                               <?php e(ucwords($value['Indexes']['index'])); ?>
                            </td>
                        </tr>	
                        <?php
                    }
                    ?>
                </table>
            </div>
            <div><input type="submit" value="Submit" class="submit_button" /></div>
            <?php
        }
        ?>
<?php if (!empty($data)) { ?>
            <?php
            e($form->end());
        } else {
            ?>
            <div style="color:blue, font-size:20; padding-bottom:30px;padding-top:30px;" align="center" ><strong>No Records Found.</strong></div>
        <?php } ?>

    </div>
</div>
<div class="clr"></div>
