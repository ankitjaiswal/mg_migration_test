
<div class="users form">
    <h2><?php __('Add Page'); ?></h2>
    <div class="actions">
        <ul>
            <li><?php echo $this->Html->link(__('All Pages', true), array('action'=>'index')); ?></li>
        </ul>
    </div>
    <?php echo $this->Form->create('Page');?>
    <fieldset>
        

            <div id="user-main">
            <?php
   
                echo $this->Form->input('title');
                echo $this->Form->textarea('content');
            ?>
            </div>
    </fieldset>

    <div  style="float:left">
    <?php 
	   echo $this->Form->submit(__('Save', true),array('div'=>false));
	   echo $this->Html->link(__('Cancel', true), array(
            'action' => 'index',
        ), array(
            'class' => 'cancel',
        ));
       echo $this->Form->end();
        
    ?>
    </div>
</div>