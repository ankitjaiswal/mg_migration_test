<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
     
        <div class="Profile_page_container">
        <div id="pro-creation">
                <h1>Invoice creation</h1>
            </div>
          <div class="figure-left align-left-botttom "> 
		  
		  <div id="profileImage" class="profileimg">
			<?php 
			$MentorData = $this->General->getUserInfo($data['Mentorship']['mentor_id']);
            if($MentorData['UserImage']['image_name'] !=''){
				$image_path = '/img/members/profile_images/'.$data['Mentorship']['mentor_id'].'/'.$MentorData['UserImage']['image_name'];
			}else{
				$image_path = '/img/media/profile.png';
			}?>        
            <?php e($html->image($image_path,array('alt'=>$MentorData['UserReference']['first_name'].' '.$MentorData['UserReference']['last_name'],'class'=>'desaturate')));
			 ?>	
			</div>
			
			
			
            <div class="figcaption">
 			<h1><span><?php e($MentorData['UserReference']['first_name'].' '.$MentorData['UserReference']['last_name']); ?></span></h1>
				<p><?php echo $MentorData['location']['city_name'];?>, <br /><?php echo $MentorData['location']['state'];?></p>	
            </div>
          </div>
          <?php e($form->create('Invoice',array('url'=>array('controller'=>'invoice','action'=>'mentee_invoice'),'id'=>'invoiceForm')));  ?>
		  <div class="mentor_details mr-bt20" style="margin-top:0px; float:none;">
			<div id="mentor-detail-left1">
				<div id="area-box">
					<div class="invoce-creation">
						<h1><?php echo ucfirst($data['Invoice']['title'])." : ". ucfirst(strtolower($MentorData['UserReference']['first_name'])).' '.ucfirst(strtolower($MentorData['UserReference']['last_name'])); ?> (Member) - <?php echo ucfirst(strtolower($data['Mentee']['first_name'])).' '.ucfirst(strtolower($data['Mentee']['last_name'])); ?> </h1>
					 <?php /* <div class="formrow">
                        <p><b>Session Tiltle</b></p>
                        <?php e($form->input('title', array('div'=>false,'id'=>"title",'value'=>$data['Invoice']['title'],'readonly'=>true, 'label'=>false, "class" => "forminput1" ,'style'=>'width:200px;', "placeholder" => "Enter session title")));?>
                      </div>*/?>
					  <div class="formrow">
                        <p><b>Date</b></p>
                        <?php 
                            $invDate = date('m/d/Y',$data['Invoice']['inv_date']);
                        e($form->input('inv_date', array('div'=>false,'id'=>"inv_date",'value'=>$invDate,'readonly'=>true, 'label'=>false, "class" => "forminput1" ,'readOnly'=>'true','style'=>'width:200px;')));?>
                      </div>
                      <div class="formrow" disabled="disabled">
                        <p><b>Time</b></p>
                        <?php $timeData = explode(":",$data['Invoice']['inv_time']); ?>
                        <?php e($form->input('time_hh', array('div'=>false,'value'=>$timeData[0],'readonly'=>true,'id'=>"time_hh",'maxlength'=>'2', 'label'=>false, "class" => "forminput1", "style"=>"width:50px; text-align:center; margin-right:10px;" )));?>
                        <?php e($form->input('time_mm', array('div'=>false,'value'=>$timeData[1],'readonly'=>true,'id'=>"time_mm",'maxlength'=>'2', 'label'=>false, "class" => "forminput1", "style"=>"width:50px; text-align:center; margin-right:10px;")));?>
                        <?php e($form->input('time_AM_PM', array('div'=>false,'value'=>$timeData[2],'readonly'=>true,'id'=>"time_AM_PM",'maxlength'=>'2', 'label'=>false, "class" => "forminput1", "style"=>"width:50px; text-align:center; margin-right:10px;")));?>
                        </div>
                      <div class="formrow">
                        <div id="mode">
        <p><b>Mode</b></p>
    </div>
  <?php   $face='';$online='';$phone='';
                 if(isset($data['Invoice']['inv_mode']) && $data['Invoice']['inv_mode']==1): 
                            $face='checked="checked"';  
                 elseif(isset($data['Invoice']['inv_mode']) && $data['Invoice']['inv_mode']==2):
                         $online='checked="checked"'; 
                     elseif(isset($data['Invoice']['inv_mode']) && $data['Invoice']['inv_mode']==3):
                         $phone='checked="checked"';  
                     else:
                         $face='checked="checked"';
                     endif; ?>
   <div id="face-box">
        <p class="styled-form">
        	<input type="radio" name="data[Invoice][inv_mode]" <?php echo $face; ?>  disabled='disabled' value="1" id="a1">
            <label for="a1" style="font-family:'Ubuntu';">In person</label>
        </p>
        <p class="styled-form">
        	<input type="radio" name="data[Invoice][inv_mode]" <?php echo $online; ?>  disabled='disabled' value="2" id="a2">
        	<label for="a2" style="font-family:'Ubuntu';">Online</label>
        </p>
        <p class="styled-form">
            <input type="radio" name="data[Invoice][inv_mode]" <?php echo $phone; ?>  disabled='disabled' value="3" id="a3">
        	<label for="a3" style="font-family:'Ubuntu';">Phone</label>
        </p>
    </div>
    <div id="face-text" style="padding-top:25px;">
    	<?php e($form->input('mode_text', array('div'=>false,'id'=>"mode_text",'value'=>$data['Invoice']['mode_text'],'readonly'=>true, 'label'=>false, "class" => "forminput1", "style"=>"margin-top:6px;")));?>
    </div>
                        
                      </div>
                      <div class="formrow">
                        <p><strong>Comments</strong></p>
                        <?php e($form->textarea('comment', array('rows'=>6,'cols'=>'30','value'=>$data['Invoice']['comment'], 'label'=>false, 'id'=>'text-area', "style"=>"width:525px;padding:10px;" , "placeholder" => "Enter your comment here")));?>
                      </div>
                       <div class="savedraffbtn">
                        <?php e($html->link('Request to reschedule','javascript:void(0);',array('class'=>'submitProfile'))); ?>
                      </div>
                      <div id="Apply" role="Apply">
							<input id="apply" value="Confirm" class="submitProfile" type="submit" style="margin-bottom:10px; margin-left: 10px;">
				      </div>
					</div>
				</div>
				
			</div>
			
			<div id="invoce-right">
			<div class="invoice-detail">
            <h1>Invoice: <?php echo $data['Invoice']['inv_no']; ?></h1>
            <?php 
            $price = $data['Invoice']['price'];
            $discount = $data['Invoice']['discount'];
            $subTotal = $data['Invoice']['price']-$data['Invoice']['discount'];
            $tax = $data['Invoice']['tax'];
            $total = $subTotal + $tax;
			?>
            <div class="invoice-price">
            <div class="price-row">
            <div class="left-price">Price</div>
            <div class="right-price">$<strong id="price"><?php echo number_format($price,2); ?></strong></div>
            </div>
            <div class="price-row">
            <div class="left-price">Discount </div>
            <div class="right-price">
            	$<?php e($form->input('discount', array('div'=>false,'value'=> $discount,'readonly'=>true, 'label'=>false,'id'=>"discount", "class" => "price-textbox", "style"=>"margin-bottom:5px;" , "placeholder" => "0.00")));?>
            </div>
            <div class="price-row">
            <div class="left-price"><strong>Subtotal</strong> </div>
            <div class="right-price">$<strong id="subtotal"><?php echo number_format($subTotal,2); ?></strong></div>
            </div>
            <div class="price-row">
            <div class="left-price">Tax</div>
            <div class="right-price">$<?php e($form->input('tax', array('div'=>false,'id'=>"tax",'value'=> $tax,'readonly'=>true,'label'=>false, "class" => "price-textbox", "style"=>"margin-bottom:5px;" , "placeholder" => "0.00")));?>
            </div>
            </div>
            <div class="price-total">
            <div class="left-price"><strong>Total</strong></div>
            <div class="right-price">$<strong id="total"><?php echo number_format($total,2); ?></strong></div>
            </div>
            </div>
            </div>
            <div class="invoice-cal">
            <?php 
			$rate = $data['Invoice']['rate'];	
			$MGFee = ($total)*($rate)/100;
			$myshare = $subTotal - $MGFee;
            echo $form->hidden('id',array('value'=>$data['Invoice']['id']));
            echo $form->hidden('mentorship_id',array('value'=>$data['Invoice']['mentorship_id']));
            echo $form->hidden('clickValue',array('value'=>'rechedule'));
            ?>
            <h1>Earnings Calculator</h1>
            <div class="invoice-calbox">
            <div class="price-row">
            <div class="left-price"><strong>My Share </strong></div>
            <div class="right-price">$<strong id="myShare"><?php echo number_format($myshare,2);?></strong></div>
            </div>
            <div class="price-row">
            <div class="left-price">Mentors guild fee </div>
            <div class="right-price">$<strong id="MGFee"><?php echo number_format($MGFee,2);?></strong></div>
            </div>
            </div>
            </div>
			</div>
		  </div>
        </div>
        <?php e($form->end());?>
      
    </div>
  </div>
<script type="text/javascript">
jQuery(function() {
    jQuery(".submitProfile").click(function(){
        var actionValue = jQuery(this).val();
        var invComment = jQuery('#text-area').val();
        var flag = 0;
           
        if(jQuery.trim(invComment)=='')
        {
           jQuery('#text-area').css('border-color','#F00');
           flag++;
        }
        else
        { 
            jQuery('#text-area').css('border-color',''); 
            if(actionValue =='Confirm')
            {
                jQuery("#InvoiceClickValue").val('confirm');
            }
        }
        if(flag>0)
        { return false; 
        }
        else
        {
             jQuery("#invoiceForm").submit(); 
        }
    });
});
   
</script>