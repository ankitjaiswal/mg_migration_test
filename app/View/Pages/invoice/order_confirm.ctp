<script type="text/javascript">
jQuery(document).ready(function(){
    window.onload=noBack1();
});
function noBack1() {
        window.history.forward(); }
</script>
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
     
        <div class="Profile_page_container">
            
        <div id="pro-creation">
                <h1>Order confirmation
                <?php if($this->Session->read('Auth.User.role_id')=='2'){?>
                    <a title="Back" href="<?php echo SITE_URL."users/my_account";?>" style="float:right; font-size:14px;">&lt;&lt;Back to profile</a>
           <?php } ?>
            <?php if($this->Session->read('Auth.User.role_id')=='3'){?>
                        <a title="Back" href="<?php echo SITE_URL."clients/my_account";?>" style="float:right; font-size:14px;">&lt;&lt;Back to profile</a>
             <?php } ?> </h1>
            </div>
            <?php
            echo $session->flash();
        ?>
          <div class="orderconformation">
     <?php e($form->create('Invoice',array('url'=>array('controller'=>'invoice','action'=>'order_confirm'),'id'=>'invoiceForm')));  ?>  
          <div class="mentor_details mr-bt20" style="margin-top:0px; float:none;">
            <div id="mentor-detail-left1">
                <div id="area-box">
                    <div class="invoce-creation">
                    <div class="orderdetail">
                    
                      	<?php 
                        $price = $data['Invoice']['price'];
                        $discount = $data['Invoice']['discount'];
                        $subTotal = $data['Invoice']['price']-$data['Invoice']['discount'];
                        $tax = $data['Invoice']['tax'];
                        $total = $subTotal + $tax; ?> 
                        
                    <?php if($data['Invoice']['inv_to'] == null){?>
                    <h1>Title </h1>
                    <div class="content">
                    <h2><?php echo ucfirst($data['Invoice']['title']); ?>: <strong>
                        <?php echo ucfirst(strtolower($data['Mentor']['first_name'])).' '.ucfirst(strtolower($data['Mentor']['last_name'])); ?> - <?php  echo ucfirst(strtolower($data['Mentee']['first_name'])).' '.ucfirst(strtolower($data['Mentee']['last_name'])); ?></strong></h2>
                    <div class="product">
                     <?php if($data['Invoice']['inv_mode']=='1'){
                         //&amp;iwloc=near for remove popup&amp;&zoom=3&amp;size=5x5&amp;maptype=roadmap
                              echo  "<iframe width='635' height='300' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0' src='https://maps.google.com/maps?&amp;q=".urlencode($data['Invoice']['mode_text'])."&amp;output=embed&amp;iwloc=near'></iframe>";
                            } ?>
                      
                          <?php  
                         $timeData2='';$timeData3='';$date2='';$date3='';
                         $timeData1 = $this->General->timeformatchange($data['Invoice']['inv_time'], $data['Invoice']['duration_hours'], $data['Invoice']['duration_minutes']);
                         $date1 = date('m/d/Y',$data['Invoice']['inv_date']);
                         if($data['Invoice']['inv_time1']!=''){
                             $timeData2 = $this->General->timeformatchange($data['Invoice']['inv_time1'], $data['Invoice']['duration_hours'], $data['Invoice']['duration_minutes']);
                             $date2 = date('m/d/Y',$data['Invoice']['inv_date1']);
                         }
                         if($data['Invoice']['inv_time2']!=''){
                            $timeData3 = $this->General->timeformatchange($data['Invoice']['inv_time2'], $data['Invoice']['duration_hours'], $data['Invoice']['duration_minutes']);
                             $date3 = date('m/d/Y',$data['Invoice']['inv_date2']);
                         }
                          ?>   
                      <p style="float:left!important;margin-top:15px;"><?php /*Date: <?php echo date('m/d/Y',$data['Invoice']['inv_date']); ?><br /><br />*/ ?>
                        <?php if($data['Invoice']['inv_mode']=='1')
                                 $mode = 'In person';
                              if($data['Invoice']['inv_mode']=='2')
                                 $mode = 'Online';
                              if($data['Invoice']['inv_mode']=='3')
                                 $mode = 'Phone';
                         ?>
                         
                        <?php echo $mode; ?>: <?php echo $data['Invoice']['mode_text']; ?><br /><br />
                        <?php if($data['User']['timezone']!=0){
                            echo "Time zone: ".$this->General->getTimeZoneById($data['Invoice']['inv_timezone'])."<br/><br/>";
                        } ?>
                    <?php //  <a href="#">Add to calendar</a><br/> ?>
              <?php echo $this->Html->link('Add to calendar',array('controller'=>'invoice','action'=>'ical',$data['Invoice']['id']),array('escape'=>false));
            ?>
                        </p>  
                      <div id="face-box" style="float: none!important; margin-top:125px!important;">
                          <p class="styled-form" style="width:250px;">Choose suitable time </p>
                            <p class="styled-form" style="width:270px; padding-top:0px!important;">
                                <input type="radio" name="data[Invoice][inv_time_choose]" checked="checked" value="1" id="a1">
                                <label for="a1" style="font-family:'Ubuntu';"><?php echo $date1.", ".$timeData1; ?></label>
                            </p>
                            <?php if($timeData2!=''){ ?>
                            <p class="styled-form" style="width:270px; padding-top:0px!important;">
                                <input type="radio" name="data[Invoice][inv_time_choose]" value="2" id="a2">
                                <label for="a2" style="font-family:'Ubuntu';"><?php echo $date2.", ".$timeData2; ?></label>
                            </p>
                            <?php } ?>
                             <?php if($timeData3!=''){ ?>
                            <p class="styled-form" style="width:270px; padding-top:0px!important;">
                                <input type="radio" name="data[Invoice][inv_time_choose]" value="3" id="a3">
                                <label for="a3" style="font-family:'Ubuntu';"><?php echo $date3.", ".$timeData3; ?></label>
                            </p>
                            <?php } ?>
                        </div> 
                   
                      <div class="clear"></div>
                    </div>
                    </div>
                      <div class="formrow">
                        <p><b>Comments</b></p>
                        <?php e($form->textarea('comment', array('rows'=>6,'cols'=>'30',"maxlength"=>"2000",'readonly'=>true,'value'=>$data['Invoice']['comment'], 'label'=>false, 'id'=>'text-area', "style"=>"width:651px; padding:10px;" , "placeholder" => "Enter your comment here")));?>
                       </div>
                       <?php }else {?>
                 		<div class="orderdetail">
	                 		<table style="width: 100%;">
                 				<tbody>
                 					<tr> 
                 						<td style="width: 30%;"><strong>Title: </strong></td><td><?php echo $data['Invoice']['title']?></td>
                 					</tr>
                 					<tr>
                 						<td><strong>To: </strong></td><td><?php echo $data['Invoice']['inv_to']?></td>
                 					</tr>
                 					<tr>
                 						<td><strong>From: </strong></td><td><?php echo $data['Invoice']['inv_from']?></td>
                 					</tr>
                 					<tr>
                 						<td><strong>Description: </strong></td><td><?php echo $data['Invoice']['inv_description']?></td>
                 					</tr>
                 				</tbody>
                 			</table>
                 		</div>
                 	<?php }?>
                    
                      <?php if($data['Invoice']['inv_date'] != null){?> 
                      <div id="Apply" role="Apply" style="float: right;">
                      	<?php if($data['Invoice']['pay_in_advance'] == 1){
                      		if($total > 0){ 
	                            e($form->submit('Submit', array('type'=>'image','src' =>SITE_URL.'/img/express-checkout-hero.png','class'=>'mentorshipBtn'))); 
	
	                      } else { ?>
	                           <input id="apply_button" value="Confirm" class="submitProfile" type="submit" style="margin:6px 12px 10px 10px;">
	                       <?php } 
	                       } else {?>
	                       		<input id="apply_button" value="Confirm" class="submitProfile" type="submit" style="margin:6px 12px 10px 10px;">
	                       	<?php }?>
	                      </div>
	                      <div class="savedraffbtn" style="float: right; padding-top:6px;padding-right:12px;">
                       	         <?php e($html->link('Request to reschedule','javascript:void(0);',array('onclick'=>'changeButton();'))); ?>
                       	  </div>
	                       	
	                       <?php } else {?> 
							<div id="Apply" role="Apply" style="float: right;">
	                       	<?php e($form->submit('Submit', array('type'=>'image','src' =>SITE_URL.'/img/express-checkout-hero.png','class'=>'mentorshipBtn')));?>
	                       	</div>
	                     <?php  }?>
                      
                      <div class="note">
                      <div class="formrow">
                        <p><b>Note:</b></p>
                        <p> You should pay fees (if any) only if all the terms are acceptable.</p>
                      </div>
                      </div>
                    </div>
                </div>
                
            </div>
            <?php 
            
            echo $form->hidden('id',array('value'=>$data['Invoice']['id']));
            echo $form->hidden('mentorship_id',array('value'=>$data['Invoice']['mentorship_id']));
            echo $form->hidden('clickValue',array('value'=>'rechedule'));
            ?>
          </div>
                 	<?php if($data['Invoice']['pay_in_advance'] == 1  || $data['Invoice']['inv_to'] != null){?>
                      <div id="invoce-right">
                        <div class="invoice-detail">
                        <h1 style="color: #000000; ">Invoice: <?php echo $data['Invoice']['inv_no'];?></h1>
                        <div class="invoice-price">
                        <div class="price-row">
                        <div class="left-price">Price</div>
                        <div class="right-price">$<?php echo number_format($price,2); ?></div>
                        </div>
                        <div class="price-row">
                        <div class="left-price">Discount </div>
                        <div class="right-price">$<?php echo number_format($discount,2); ?></div>
                        </div>
                        <div class="price-row">
                        <div class="left-price"><strong>Subtotal</strong> </div>
                        <div class="right-price"><strong>$<?php echo number_format($subTotal,2); ?></strong></div>
                        </div>
                        <div class="price-row">
                        <div class="left-price">Tax</div>
                        <div class="right-price">$<?php echo number_format($tax,2); ?></div>
                        </div>
                        <div class="price-total">
                        <div class="left-price"><strong>Total</strong></div>
                        <div class="right-price">$<strong id="totalprice"><?php echo number_format($total,2); ?></strong></div>
                        </div>
                        </div>
                        </div>
                    </div>          
                    <?php }?>  
          <?php e($form->end());?>
          </div>
        </div>
      
    </div>
  </div>
  <script type="text/javascript">
   function changeButton()
    { 
        jQuery('#text-area').attr('readonly',false);
        jQuery('#text-area').focus();

        jQuery('#a1').prop('checked', false);
        jQuery('#a1').attr('disabled', true);
        jQuery('#a2').prop('checked', false);
        jQuery('#a2').attr('disabled', true);
        jQuery('#a3').prop('checked', false);
        jQuery('#a3').attr('disabled', true);
        
        jQuery('#Apply').html('');
        jQuery('#Apply').html('<input id="apply_button" value="Send" class="submitProfile" type="submit" style="margin:25px 0 10px 10px;">');
        jQuery('.savedraffbtn').hide();
    }
jQuery(function() {
    
    jQuery("#apply_button").live("click",function(){
        var actionValue = jQuery(this).val();
        var invComment = jQuery('#text-area').val();
        var flag = 0;
        var price = jQuery('#totalprice').html();
        if(price=='0.00' && actionValue=='Send')
        {
            /*if(jQuery.trim(invComment)=='')
            {
               jQuery('#text-area').css('border-color','#F00');
               flag++;
            }
            else
            { 
                jQuery('#text-area').css('border-color','');
             }*/
        }
            if(actionValue =='Send')
            {
                jQuery("#InvoiceClickValue").val('rechedule');
            }
            if(actionValue=='Confirm')
            {
                jQuery("#InvoiceClickValue").val('complete'); 
            }
        //}
        if(flag>0)
        { return false; 
        }
        else
        {
             jQuery("#invoiceForm").submit(); 
        }
    });
    
    jQuery(".mentorshipBtn").click(function(){
        var actionValue = 'paypal';
        var invComment = jQuery('#text-area').val();
        var flag = 0;
       /* if(jQuery.trim(invComment)=='')
        {
           jQuery('#text-area').css('border-color','#F00');
           flag++;
        }
        else
        { 
            jQuery('#text-area').css('border-color','');*/ 
            if(actionValue =='paypal')
            {
                jQuery("#InvoiceClickValue").val('paypal');
            }
           
        //}
        if(flag>0)
        { return false; 
        }
        else
        {
             jQuery("#invoiceForm").submit(); 
        }
    });
});
   
</script>
<style>
    #flashMessage{
        margin-top:5px !important;
    }
</style>