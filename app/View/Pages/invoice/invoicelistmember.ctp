<style>
#pagingarea span{ font-size:15px; padding:5px; }
</style>
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
     <div id="registerationFm" style="min-height: 450px;">
        <h1>My invoices <?php //echo ucfirst($mentorData['UserReference']['first_name'])." ".strtolower($mentorData['UserReference']['last_name']);?></h1>
        
        <div class="feedbackcontent" style="padding-top: 12px;">
           <table width="100%" style="border:1px solid #CCCCCC;" cellspacing="2" cellpadding="10">
               <tr bgcolor="#2A2A2A" class="row_head">
                   <td>Invoice no.</td>
                   <td>Date</td>
                   <td>Client name</td>
                   <td>Session title</td>
                   <td>Price</td>
                   <td>Status</td>
                   <td>Action</td>
               </tr>
           <?php if(!empty($data) && count($data)>0)
                {
                    $i=1; 
                  foreach($data as $invoice){
                      if($i%2==0)
                        $class = 'first_row';
                      else
                        $class = 'second_row';          
           ?>
               <tr class="<?php echo $class; ?>">
                   <td><?php e($html->link($invoice['Invoice']['inv_no'],array('controller'=>'invoice','action'=>'invoice_view/'.$invoice['Invoice']['id']),array('class'=>'delete','title'=>'View invoice'))); ?></td>
                   <td><?php echo $this->Time->format('d-m-Y',$invoice['Invoice']['created']); ?></td>
                   <td><?php echo ucfirst(strtolower($invoice['Mentee']['first_name']))." ".ucfirst(strtolower($invoice['Mentee']['last_name'])); ?></td>
                   <td><?php echo $invoice['Invoice']['title']; ?></td>
        <?php      if($invoice['Invoice']['total']==0)
                   {
                       $total = 'FREE';
                   }
                   else
                    $total =  '$'.number_format($invoice['Invoice']['total'],2); ?>
                   <td><?php echo $total; ?></td>
                   <?php if($invoice['Invoice']['inv_create_status']=='1')      
                                $status = 'CLOSED';
                         else
                                $status = 'OPEN';
                    ?>
                   <td><?php echo $status; ?></td>
               <td align="center"><?php e($html->link('Print',array('controller'=>'invoice','action'=>'view_pdf/'.$invoice['Invoice']['id']),array('class'=>'delete','title'=>'Print invoice'))); ?></td>
               </tr>
           <?php    
                    $i++;
                    }
           if($this->params['paging']['Invoice']['pageCount']>1)
           { ?>
             <tr id="pagingarea">
                <?php
                $paginator->options(array('update'=>'CustomerPaging','url'=>array('controller'=>'invoice', 'action'=>'invoicelistmember'),'indicator' => 'LoadingDiv')); ?>
                <td colspan="6"><?php echo $paginator->prev('<< Previous', array('class' => 'previous-off'), null); ?><?php echo $paginator->numbers(array('separator'=>'','class'=>'pagingnum')); ?><?php echo $paginator->next('Next >>', array('class' => 'next next-off'), null); ?></td>
                
            </tr>
      <?php }
                  } else{?>
              <tr class="first_row" style="text-align: center;">
                   <td colspan="7">No record found</td>
               </tr>
                 <?php } ?>
           </table>
<div id ="app-button">		
                <br/><br/>
                <input type="submit" value="Done" onclick="window.location.href='<?php echo SITE_URL."users/my_account"; ?>'; ">
            
        </div>       
 </div>


         </div>
      
    </div>
</div>
<style>
    .row_head{color: #EEEEEE; font-size: 14px; height: 30px; background: #2A2A2A; text-align:left;}  
    .first_row{color: #000000; font-size: 14px; height: 30px; background:#CCCCCC; text-align:left;}
    .second_row{color: #000000; font-size: 14px; height: 30px; text-align:left;    
</style>