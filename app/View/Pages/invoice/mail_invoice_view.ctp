<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
     
        <div class="Profile_page_container">
        <div id="pro-creation">
            <?php if($data['Invoice']['inv_create_status']==0): $status = 'Invoice status (OPEN)'; else: $status = 'Confirmed order'; endif; ?>
                <h1><?php echo $status; ?>
           <?php if($this->Session->read('Auth.User.role_id')=='2'){?>
                    <a title="Back" href="<?php echo SITE_URL."users/my_account";?>" style="float:right; font-size:14px;">&lt;&lt;Back to dashboard</a>
           <?php } ?>
            <?php if($this->Session->read('Auth.User.role_id')=='3'){?>
                        <a title="Back" href="<?php echo SITE_URL."clients/my_account";?>" style="float:right; font-size:14px;">&lt;&lt;Back to dashboard</a>
             <?php } ?> </h1>
            </div>
          <div class="orderconformation">	
		  <div class="mentor_details mr-bt20" style="margin-top:0px; float:none;">
			<div id="mentor-detail-left1">
				<div id="area-box">
					<div class="invoce-creation">
					<div class="orderdetail">
                    <h1>Title </h1>
                    <div class="content">
                    <h2><?php echo ucfirst($data['Invoice']['title']); ?> : <strong><?php echo ucfirst($data['Mentor']['first_name']).' '.ucfirst($data['Mentor']['last_name']); ?> (Mentor) - <?php  echo ucfirst($data['Mentee']['first_name']).' '.ucfirst($data['Mentee']['last_name']); ?> (Mentee) </strong></h2>
                    <div class="product">
                     <?php if($data['Invoice']['inv_mode']=='1'){
                         //&amp;iwloc=near for remove popup&amp;&zoom=3&amp;size=5x5&amp;maptype=roadmap
                              echo  "<iframe width='635' height='300' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0' src='https://maps.google.com/maps?&amp;q=".urlencode($data['Invoice']['mode_text'])."&amp;output=embed&amp;iwloc=near'></iframe>";
                            } ?>
                      
                      <p style="float:left;margin-top:15px;">Date: <?php echo date('m/d/Y',$data['Invoice']['inv_date']); ?><br />
                        <?php $timeData = explode(":",$data['Invoice']['inv_time']);
                              $second = $timeData[0]+1;  
                         ?>
                        Time: <?php echo str_pad($timeData[0],2,0,STR_PAD_LEFT).":".str_pad($timeData[1],2,0,STR_PAD_LEFT)."-".str_pad($second,2,0,STR_PAD_LEFT).":".str_pad($timeData[1],2,0,STR_PAD_LEFT)." ".$timeData[2];?> <br />
                        <?php if($data['Invoice']['inv_mode']=='1')
                                 $mode = 'In person';
                              if($data['Invoice']['inv_mode']=='2')
                                 $mode = 'Online';
                              if($data['Invoice']['inv_mode']=='3')
                                 $mode = 'Phone';
                                  
                         ?>
                         <?php 
                        $price = $data['Invoice']['price'];
                        $discount = $data['Invoice']['discount'];
                        $subTotal = $data['Invoice']['price']-$data['Invoice']['discount'];
                        $tax = $data['Invoice']['tax'];
                        $total = $subTotal + $tax; ?> 
                         
                        <?php echo $mode; ?> : <?php echo $data['Invoice']['mode_text']; ?><br />
                   
                        </p>
                      </div>
                      <div class="clear"></div>
                    </div>
                    </div>
                      <div class="formrow">
                        <p><b>Comments</b></p>
                        <?php e($form->textarea('comment', array('rows'=>6,'cols'=>'30','readonly'=>true,"maxlength"=>"2000",'value'=>$data['Invoice']['comment'], 'label'=>false, 'id'=>'text-area', "style"=>"width:651px; padding:10px;" , "placeholder" => "Enter your comment here")));?>
                       </div>
                      
                      <div class="note">
                      <div class="formrow">
                        <p><b>Note:</b></p>
                        <p> You should pay fees only if all terms are acceptable.</p>
                      </div>
                      </div>
					</div>
                 
				</div>
				
			</div>
			<div id="invoce-right">
			<div class="invoice-detail">
            <h1 style="color: #000000;">Invoice: <?php echo $data['Invoice']['inv_no'];?></h1>
            <div class="invoice-price">
            <div class="price-row">
            <div class="left-price">Price</div>
            <div class="right-price">$<?php echo number_format($price,2); ?></div>
            </div>
            <div class="price-row">
            <div class="left-price">Discount </div>
            <div class="right-price">$<?php echo number_format($discount,2); ?></div>
            </div>
            <div class="price-row">
            <div class="left-price"><strong>Subtotal</strong> </div>
            <div class="right-price"><strong>$<?php echo number_format($subTotal,2); ?></strong></div>
            </div>
            <div class="price-row">
            <div class="left-price">Tax</div>
            <div class="right-price">$<?php echo number_format($tax,2); ?></div>
            </div>
            <div class="price-total">
            <div class="left-price"><strong>Total</strong></div>
            <div class="right-price"><strong>$<?php echo number_format($total,2); ?></strong></div>
            </div>
            </div>
            </div>
			</div>
		  </div>
          </div>
        </div>
      
    </div>
  </div>