<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
     
        <div class="Profile_page_container">
        <div id="pro-creation">
            <?php if($data['Invoice']['inv_create_status']==0): $status = 'Invoice status (OPEN)'; else: $status = 'Confirmed order'; endif; ?>
                <h1><?php echo $status; ?>
           <?php if($this->Session->read('Auth.User.role_id')=='2'){?>
                    <a title="Back" href="<?php echo SITE_URL."users/my_account";?>" style="float:right; font-size:14px;">&lt;&lt;Back to profile</a>
           <?php } ?>
            <?php if($this->Session->read('Auth.User.role_id')=='3'){?>
                        <a title="Back" href="<?php echo SITE_URL."clients/my_account";?>" style="float:right; font-size:14px;">&lt;&lt;Back to profile</a>
             <?php } ?> </h1>
            </div>
          <div class="orderconformation">	
		  <div class="mentor_details mr-bt20" style="margin-top:0px; float:none;">
			<div id="mentor-detail-left1">
			
				<div id="area-box">
				<div class="invoce-creation">
					<div class="orderdetail">
					<?php if($data['Invoice']['inv_to'] == null){?>
                    <h1>Appointment details</h1>
                    <div class="content">
                   <?php if($this->Session->read('Auth.User.id')!='' && $this->Session->read('Auth.User.role_id')=='2'){?>

                    <h2><?php echo ucfirst($data['Invoice']['title']); ?> : <strong><?php echo ucfirst(strtolower($data['Mentor']['first_name'])).' '.ucfirst(strtolower($data['Mentor']['last_name'])); ?> - <?php  echo ucfirst(strtolower($data['Mentee']['first_name'])).' '.ucfirst(strtolower($data['Mentee']['last_name'])); ?> </strong></h2>              
        
                  <?php }else{?>
                    
                    <h2><?php echo ucfirst($data['Invoice']['title']); ?> : <strong><?php echo ucfirst(strtolower($data['Mentor']['first_name'])).' '.ucfirst(strtolower($data['Mentor']['last_name'])); ?> - <?php  echo ucfirst(strtolower($data['Mentee']['first_name'])).' '.ucfirst(strtolower($data['Mentee']['last_name'])); ?> </strong></h2> 
                  <?php }?> 
                  <div class="product">
                     <?php if($data['Invoice']['inv_mode']=='1'){
                         //&amp;iwloc=near for remove popup&amp;&zoom=3&amp;size=5x5&amp;maptype=roadmap
                              echo  "<iframe width='635' height='300' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0' src='https://maps.google.com/maps?&amp;q=".urlencode($data['Invoice']['mode_text'])."&amp;output=embed&amp;iwloc=near'></iframe>";
                            } ?>
                      
                      <table cellspacing="10px;" style="margin-top: 15px;">
                      	<tr>
                      		<td style="float:left;">
                      			<span style="font-weight: bold;">Date</span>
                      		</td>
                      		<td>
                      			<?php echo date('d/m/Y',$data['Invoice']['inv_date']); ?>
                      			(<?php echo date('l',$data['Invoice']['inv_date']); ?>)
                      		</td>
                      	</tr>
                      	<tr>
                      		<td style="float:left;">
                      			<span style="font-weight: bold;">Time</span> 
                      		</td>
                      		<td>
                      			<?php $timeData1 = $this->General->timeformatchange($data['Invoice']['inv_time'], $data['Invoice']['duration_hours'], $data['Invoice']['duration_minutes']); ?>
                        		<?php echo $timeData1;
			                        if($data['Invoice']['inv_timezone']!=0){
									echo "	(".$this->General->getTimeZoneById($data['Invoice']['inv_timezone']).")";
									}
                        		?> 
                      		</td>
                      	</tr>
                      	<tr>
                      		<td style="float:left;">
                      			<?php if($data['Invoice']['inv_mode']=='1')
                                 $mode ='In person';
	                              if($data['Invoice']['inv_mode']=='2')
	                                 $mode = 'Online';
	                              if($data['Invoice']['inv_mode']=='3')
	                                 $mode = 'Phone';
		                         ?>
                      			<span style="font-weight: bold;"><?php echo $mode; ?>	</span>
                      		</td>
                      		<td >
	                          <?php echo $data['Invoice']['mode_text']; ?>
                      		</td>
                      	</tr>
                      	<tr>
                      		<td style="float:left;">
                      		<a href="<?php echo SITE_URL."invoice/ical/".$data['Invoice']['id']; ?>" style="color:#f23034;">Add to calendar</a>
                      		</td>
                      	</tr>
                      </table>
                      </div>
                      <div class="clear"></div>
                    </div>
                      <div class="formrow">
                        <p><b>Comments</b></p>
                        <?php e($form->textarea('comment', array('rows'=>6,'cols'=>'30','readonly'=>true,"maxlength"=>"2000",'value'=>$data['Invoice']['comment'], 'label'=>false, 'id'=>'text-area', "style"=>"width:651px; padding:10px;" , "placeholder" => "")));?>
                       </div>
                      
                 	<?php } else {?>
                 		<div class="orderdetail">
                 			<table style="width: 100%;">
                 				<tbody>
                 					<tr> 
                 						<td style="width: 30%;"><strong>Title: </strong></td><td><?php echo $data['Invoice']['title']?></td>
                 					</tr>
                 					<tr>
                 						<td><strong>To: </strong></td><td><?php echo $data['Invoice']['inv_to']?></td>
                 					</tr>
                 					<tr>
                 						<td><strong>From: </strong></td><td><?php echo $data['Invoice']['inv_from']?></td>
                 					</tr>
                 					<tr>
                 						<td><strong>Description: </strong></td><td><?php echo $data['Invoice']['inv_description']?></td>
                 					</tr>
                 				</tbody>
                 			</table>
                 		</div>
                 	<?php }?>
                 	
                 	  <div class="note">
                      <div class="formrow">
                        <p><b>Note:</b></p>
                        <p> You should pay fees only if all terms are acceptable.</p>
                      </div>
                      </div>
                      
                 	</div>
                 	</div>
				</div>
				
				
			</div>
			<?php if($data['Invoice']['pay_in_advance'] == 1 || $data['Invoice']['inv_to'] != null){?>
				                    <?php 
	                        $price = $data['Invoice']['price'];
	                        $discount = $data['Invoice']['discount'];
	                        $subTotal = $data['Invoice']['price']-$data['Invoice']['discount'];
	                        $tax = $data['Invoice']['tax'];
	                        $total = $subTotal + $tax; ?> 
	                        
			<div id="invoce-right">
			<div class="invoice-detail">
            <h1 style="color: #000000; font-weight: bold; padding-top: 0;">Invoice: <?php echo $data['Invoice']['inv_no'];?></h1>
            <div class="invoice-price">
            <div class="price-row">
            <div class="left-price">Price</div>
            <div class="right-price">$<?php echo number_format($price,2); ?></div>
            </div>
            <div class="price-row">
            <div class="left-price">Discount </div>
            <div class="right-price">$<?php echo number_format($discount,2); ?></div>
            </div>
            <div class="price-row">
            <div class="left-price"><strong>Subtotal</strong> </div>
            <div class="right-price"><strong>$<?php echo number_format($subTotal,2); ?></strong></div>
            </div>
            <div class="price-row">
            <div class="left-price">Tax</div>
            <div class="right-price">$<?php echo number_format($tax,2); ?></div>
            </div>
            <div class="price-total">
            <div class="left-price"><strong>Total</strong></div>
            <div class="right-price"><strong>$<?php echo number_format($total,2); ?></strong></div>
            </div>
            </div>
            </div>
			</div>
			<?php }?>
		  </div>
          </div>
        </div>
      
    </div>
  </div>