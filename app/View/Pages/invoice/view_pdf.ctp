<?php 
ob_clean();
App::import('Vendor','tcpdf');
//echo $html->css(array('style'));
$tcpdf = new TCPDF();
$textfont = 'helvetica';

$tcpdf->SetAuthor("Julia Holland");
$tcpdf->SetAutoPageBreak(true);
 
$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(false);
 
//$tcpdf->SetTextColor(56, 150, 100);
//$tcpdf->SetFont($textfont,'',9);
$invNo = $data['Invoice']['inv_no'];
$mentorName = ucfirst(strtolower($data['Mentor']['first_name'])).' '.ucfirst(strtolower($data['Mentor']['last_name'])); 
$menteeName = ucfirst(strtolower($data['Mentee']['first_name'])).' '.ucfirst(strtolower($data['Mentee']['last_name'])); 
$invCreatedDate =  date('d M, Y');
$invDate =  date('d M, Y');
$invTitle = $data['Invoice']['title'];
$plantype = $data['User']['plan_type'];
if($data['Invoice']['inv_to'] == null) {
$invTime = $this->General->timeformatchange($data['Invoice']['inv_time'], $data['Invoice']['duration_hours'], $data['Invoice']['duration_minutes']);
$timezoneValue = '';
if($data['Invoice']['inv_timezone']!=0)
{
    $timezone = $this->General->getTimeZoneById($data['Invoice']['inv_timezone']);
    $timezoneValue = $timezone; 
}
if($data['Invoice']['inv_mode']=='1')
    $mode = 'Face to face';
if($data['Invoice']['inv_mode']=='2')
    $mode = 'Online';
if($data['Invoice']['inv_mode']=='3')
    $mode = 'Phone';
$modeText = $data['Invoice']['mode_text'];

$comment = $data['Invoice']['comment'];
}

$price = number_format($data['Invoice']['price'],2);
$discount = number_format($data['Invoice']['discount'],2);
$subtotal = number_format(($price-$discount),2);
$tax = number_format($data['Invoice']['tax'],2);
$total = number_format(($subtotal + $tax),2);

$inv_to = $data['Invoice']['inv_to'];
$inv_from = $data['Invoice']['inv_from'];
$inv_description = nl2br(strip_tags($data['Invoice']['inv_description']));

//$imgPath = SITE_URL.$html->image('media/logo.png');
$imgPath = SITE_URL.'/img/media/logo.png';

$tcpdf->AddPage();

if($data['Invoice']['inv_to'] == null) {
// create some HTML content
$htmlcontent = <<<EOF
<div style="width: 800px; margin: 0 auto;" >
        <h1><img src="$imgPath" width="200" /></h1>
                                    <div style="clear:both"></div>
       <table cellpadding="5" cellspacing="0" style="border-collapse: collapse;margin-top: 1px; margin-bottom: 30px; float: right; ">
                <tr><td colspan="2" style=" height: 15px; width: 100%; margin: 20px 0; background: #222; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; ">INVOICE</td>
                </tr>
                <tr>
                    <td colspan="2" style="border:1px solid #000;"><strong>Invoice:</strong> $invNo</td>
                </tr>
                <tr>
                    <td colspan="2" style="border:1px solid #000;"><strong>Invoice Date:</strong> $invCreatedDate</td>
                </tr>

            </table>
   
        <table cellpadding="5" cellspacing="0" style="border-collapse:collapse; width: 100%; margin: 30px 0 0 0; ">
        
          <tr>
              <th colspan="4" style="border:1px solid #000;"><strong>Description:</strong></th>
              <th style="border:1px solid #000;">Price</th>
          </tr>
          
          <tr>
             
              <td colspan="4" style="border:1px solid #000;"><strong>$invTitle: </strong>$mentorName (Consultant) & $menteeName ($plantype Client)<br/><br/>
				  <table>
				  	<tr>
				  		<td style="float:left; width: 20%;"><strong>Date  </strong></td> <td style="float:left;">$invDate</td>
				  	</tr>
				  	<tr>
				  		<td style="float:left; width: 20%;"><strong>Time  </strong></td> <td style="float:left;">$invTime ($timezoneValue)</td>
				  	</tr>
				  	<tr>
				  		<td style="float:left; width: 20%;"><strong>$mode  </strong></td> <td style="float:left;">$modeText</td>
				  	</tr>
				  </table>
             </td>
             <td style="border:1px solid #000;">$$price</td>
             
          </tr>
          
          <tr>
              <td colspan="4" style="border:1px solid #000;text-align: right;">Discount</td>
              <td style="border:1px solid #000;text-align: left;">$$discount</td>
          </tr>
          <tr>
              <td colspan="4" style="border:1px solid #000;text-align: right;">SubTotal</td>
              <td style="border:1px solid #000;text-align: left;">$$subtotal</td>
          </tr>
          <tr>
              <td colspan="4" style="border:1px solid #000;text-align: right;">Tax</td>
              <td style="border:1px solid #000;text-align: left;">$$tax</td>
          </tr>
           <tr>
              <td colspan="4" style="border:1px solid #000;text-align: right;"><strong>Total</strong></td>
              <td style="border:1px solid #000;text-align: left;"><strong>$$total</strong></td>
          </tr>
        </table>    
    </div>
EOF;
} else {
	// create some HTML content
	$htmlcontent = <<<EOF
<div style="width: 800px; margin: 0 auto;" >
        <h1><img src="$imgPath" width="200" /></h1>
	
       <table cellpadding="5" cellspacing="0" style="border-collapse: collapse;margin-top: 1px; margin-bottom: 30px; float: right; ">
                <tr><td colspan="2" style=" height: 15px; width: 100%; margin: 20px 0; background: #222; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; ">INVOICE</td>
                </tr>
                <tr>
                    <td colspan="2" style="border:1px solid #000;">Invoice : $invNo</td>
                </tr>
                <tr>
                    <td colspan="2" style="border:1px solid #000;">Invoice Date : $invCreatedDate</td>
                </tr>
	
            </table>
  
        <table cellpadding="5" cellspacing="0" style="border-collapse:collapse; width: 100%; margin: 30px 0 0 0; ">
	
          <tr>
              <th colspan="4" style="border:1px solid #000;">Description</th>
              <th style="border:1px solid #000;">Price</th>
          </tr>
	
          <tr>
       
              <td colspan="4" style="border:1px solid #000;">Mentors Guild $invTitle <br/>
				  <table>
				  	<tr>
				  		<td style="float:left; width: 20%;"><strong>To:  </strong></td> <td style="float:left;">$inv_to</td>
				  	</tr>
				  	<tr>
				  		<td style="float:left; width: 20%;"><strong>From:  </strong></td> <td style="float:left;">$inv_from</td>
				  	</tr>
				  	<tr>
				  		<td style="float:left; width: 20%;"><strong>Description:  </strong></td> <td style="float:left;">$inv_description</td>
				  	</tr>
				  </table>
             </td>
             <td style="border:1px solid #000;">$$price</td>
       
          </tr>
	
          <tr>
              <td colspan="4" style="border:1px solid #000;text-align: right;"><strong>Discount</strong></td>
              <td style="border:1px solid #000;text-align: left;">$$discount</td>
          </tr>
          <tr>
              <td colspan="4" style="border:1px solid #000;text-align: right;"><strong>SubTotal</strong></td>
              <td style="border:1px solid #000;text-align: left;">$$subtotal</td>
          </tr>
          <tr>
              <td colspan="4" style="border:1px solid #000;text-align: right;"><strong>Tax</strong></td>
              <td style="border:1px solid #000;text-align: left;">$$tax</td>
          </tr>
           <tr>
              <td colspan="4" style="border:1px solid #000;text-align: right;"><strong>Total</strong></td>
              <td style="border:1px solid #000;text-align: left;"><strong>$$total</strong></td>
          </tr>
        </table>
    </div>
EOF;
}
// output the HTML content
$tcpdf->writeHTML($htmlcontent, true, 0, true, 0);
$filename = "MentorsGuildInvoice_".date("d-m-Y");
$tcpdf->Output($filename.".pdf", 'D'); ?>