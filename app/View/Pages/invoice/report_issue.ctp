<?php e($html->css(array('regist_popup'))); ?>
<div id="registerationFm">
	<h1>Report an issue</h1>
	<?php 
		e($form->create('Invoice',array('url'=>array('controller'=>'invoice','action'=>'report_issue',$mentorship_id,$from),'id'=>'feedbackForm'))); 
		
	?>	
	<div class="feedbackcontent">
		<div class="form_row">
			<textarea  id="report_comment" style="height:161px;"  maxlength="100" div="1" rows="9" name="data[Invoice][comment]"></textarea>           
		</div>
	</div>
	<div class="submit_bar">
		<div class="submit_bar_right" style="float:right;">
			<?php e($form->submit('Submit',array('class'=>'btn','onclick' => 'return validateComment();'))); ?> 
		</div>
	</div>
	<?php 
		e($form->end()); 
	?>	
</div>
</div>
</div>