<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div id="inner-content-wrapper" style="height: 400px">
    <div id="inner-content-box" class="pagewidth">
        <div id="user">
            <h1 style="color:#000;font-size:15px;">
                Your order has been confirmed.
            </h1>
        </div>
        <div style="padding: 20px 0px;color:grey;font-size:15px;">
            <h1 style="font-size:15px;color:#000;">
                Thanks for your interest!
            </h1>
        </div>
        <div id ="app-button">	
        	 <?php if($this->Session->read('Auth.User.role_id')== Configure::read('App.Role.Mentee'))
            			e($form->create('User', array('url' => '/clients/my_account','method'=>'link')));
					else 
						e($form->create('User', array('url' => '/users/my_account','method'=>'link')));
					?>
                <input type="submit" value="Done" id="submit"/>
            <?php e($form->end()); ?>
        </div>
    </div>
</div>
<style type="text/css">
.flash_good {
    font-family: Ubuntu;
    font-size: 15px;
}
</style>