<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="google-site-verification" content="UroIkuqOwc9A-KngERWfg2Mam9Fw_0UmVECjjujMYMY" />
        <script type="text/javascript">var NREUMQ=NREUMQ||[];NREUMQ.push(["mark","firstbyte",new Date().getTime()]);</script>
<title>
            Mentors Guild        </title>
        <link rel="icon" href="/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

        				<meta name="description" content="Mentors Guild is the trusted advisor to smart business executives. Get free advice, consult leading experts, and make better decisions." />
								<meta name="keywords" content="executive coaching, executive coach, business coaching, business advice, business consulting, business expertise, CEO coach, executive development, leadership coaching" />
			  
      
    </head>
<body>
<?php //echo $plan_type; ?>
<?php// echo $role_id;?>
        <?php //$value = $this->Session->read('ab');?>
        <?php //echo $value; ?> 
	<div id="inner-content-wrapper">
	  <div id="inner-content-box" class="pagewidth"> 
	    <div id="user-account">
	      <?php //e($form->create('User', array('url' => array('controller' => 'clients', 'action' => 'account_setting'),'id'=>'accountSettingForm')));?>     
	      <div class="account-form">

					<div class="onbanner">
	          <h1>                 
	            <div id="spec1" style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;">From <span style="text-decoration: underline;">Executive Coaching</span> to <span style="text-decoration: underline;">New Product Launch</span></div>
				
				<div id="spec2" style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;display:none;">From <span style="text-decoration: underline;">Business Strategy</span> to <span style="text-decoration: underline;">Succession Planning</span></div>
				
				<div id="spec3" style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;display:none;">From <span style="text-decoration: underline;">Workplace Conflict</span> to <span style="text-decoration: underline;">Customer Service</span></div>
				
				<div id="spec4" style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;display:none;">From <span style="text-decoration: underline;">Mergers &amp; Acquisitions</span> to <span style="text-decoration: underline;">Change Management</span></div>
				
				<div id="spec5" style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;display:none;">From <span style="text-decoration: underline;">Employee Engagement</span> to <span style="text-decoration: underline;">Lean Six Sigma</span></div>  
	          </h1>
	          
				<div style="position:relative;top:80px;"><p><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal;font-size: 18px;">Your organization has evolving needs. Mentors Guild has them covered.</span></div>

</div>

			
			
			<script>
		
				var $body = jQuery('body'),
				    cycle;
				(cycle = function() {
					jQuery('#spec1').delay(5000).fadeOut('slow');
				 
					
                                   jQuery('#spec2').delay(6500).fadeIn('slow');
				    jQuery('#spec2').delay(5000).fadeOut('fast');
					
					
					jQuery('#spec3').delay(13000).fadeIn('slow');
				    jQuery('#spec3').delay(5000).fadeOut('fast');
					
					jQuery('#spec4').delay(19500).fadeIn('slow');
				    jQuery('#spec4').delay(5000).fadeOut('fast');
					

					jQuery('#spec5').delay(26000).fadeIn('slow');
				    jQuery('#spec5').delay(5000).fadeOut('fast');
				
				jQuery('#spec1').delay(27500).fadeIn('slow',cycle);	
									
				})();
				
				</script>
		
		
	        <table class="planstable" width="100%" cellpadding="0" cellpadding="0">
	          <tr>
	            <th width="420">&nbsp;</th>
	            <th width="180">&nbsp;</th>
	            <th width="180">&nbsp;</th>
	            <th width="180">&nbsp;</th>
	          </tr>
	         
 <tr class="borderbtm">
	            <th align="left" valign="bottom" class="numbeR">
		
<div style="position: absolute;"><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Have questions? Call&nbsp;</span><strong><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: bold;font-size: 18px;"><span>1</span>-<span>866</span>-<span>511</span>-<span>1898</span><span style="color: black; font-weight: bold;">.</span></span></strong></div>
		</th><br/>
<?php 
if($this->Session->read('Auth.User.id')==''){ ?>
	            <th></br><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Basic</span><br/><span>Free</span><br/>
		<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
                  
    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:register_popup();"></div>
					<div style="float: none;
								text-align: center;
								font-style: normal;
								text-decoration: none;
								font-size: 80%;
								vertical-align: bottom;
								display: block;
								position: static;
								margin-bottom: 0;
										"><span>&nbsp;</span></div></th>
	            <th><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Pro<br/></span><span>$95 per month</span>
			<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
	                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:submitPlan(2);"></div>
	
	            <th><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Enterprise</span><br/><span>$195 per month</span>		<br/>
				<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
		                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:submitPlan(3);">


</div>
						
	          
 <?php } elseif($this->Session->read('Auth.User.id')!='' && $this->Session->read('Auth.User.role_id')=='2'){?>
	            <th></br><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Basic</span><br/><span>Free</span><br/>
		<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
                  
    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="window.location.href='<?php e(SITE_URL."thanks1");?>'">

</div>
<div style="float: none;
								text-align: center;
								font-style: normal;
								text-decoration: none;
								font-size: 80%;
								vertical-align: bottom;
								display: block;
								position: static;
								margin-bottom: 0;
										"><span>&nbsp;</span></div></th>
	            <th><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Pro<br/></span><span>$195 per month</span>
			<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
	                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="window.location.href='<?php e(SITE_URL."thanks");?>'"></div>
	
	            <th><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Enterprise</span><br/><span>$395 per month</span>		<br/>
				<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
		                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="window.location.href='<?php e(SITE_URL."thanks");?>'">


</div>
	
                 
<?php } elseif($this->Session->read('Auth.User.id')!='' && $this->Session->read('Auth.User.role_id')=='3' && $plan_type =='Basic'){ ?>

<th></br><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Basic</span><br/><span>Free</span><br/>
		<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
                   
 <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Your plan" onclick="javascript:register_popup();"></div>
					<div style="float: none;
								text-align: center;
								font-style: normal;
								text-decoration: none;
								font-size: 80%;
								vertical-align: bottom;
								display: block;
								position: static;
								margin-bottom: 0;
										"><span>&nbsp;</span></div></th>
	            <th><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Pro<br/></span><span>$195 per month</span>
			<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
	                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:submitPlan(2);"></div>
	
	            <th><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Enterprise</span><br/><span>$395 per month</span>		<br/>
				<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
		                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:submitPlan(3);">
	 
</div>
							
	          

	        

<?php } elseif($this->Session->read('Auth.User.id')!='' && $this->Session->read('Auth.User.role_id')=='3' && $plan_type =='Pro'){ ?>

	            <th></br><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Basic</span><br/><span>Free</span><br/>
		<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
                  
 <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:register_popup();"></div>
					<div style="float: none;
								text-align: center;
								font-style: normal;
								text-decoration: none;
								font-size: 80%;
								vertical-align: bottom;
								display: block;
								position: static;
								margin-bottom: 0;
										"><span>&nbsp;</span></div></th>
	            <th><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Pro<br/></span><span>$195 per month</span>
			<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
	                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Your plan " onclick="javascript:submitPlan(2);"></div>
	
	            <th><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Enterprise</span><br/><span>$395 per month</span>		<br/>
				<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
		                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:submitPlan(3);">
	 
</div>
							
	      

         <?php } else{?>  
      
     <th><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Basic</span><br/><span>Free</span><br/>
		<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
                   
<input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:register_popup()div>
					<div style="float: none;
								text-align: center;
								font-style: normal;
								text-decoration: none;
								font-size: 80%;
								vertical-align: bottom;
								display: block;
								position: static;
								margin-bottom: 0;
										"><span>&nbsp;</span></div></th>
	            <th><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Pro<br/></span><span>$195 per month</span>
			<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
	                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:submitPlan(2);"></div>
	
	            <th><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Enterprise</span><br/><span>$395 per month</span>		<br/>
				<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
		                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Your plan" onclick="javascript:submitPlan(3);">

</div>
							
	          

	         
<?php } ?>
</tr> 
<tr>
	            <td>
	              <?php echo Configure::read('P1');?>              
	            </td>
	            <td>
	              View past answers
	            </td>
	            <td>
	              View past answers<br/>
	              or ask questions
	            </td>
	            <td>
	              View past answers<br/>
				  or ask questions
	            </td>
	          </tr>
	          <tr>
				<tr>
	            <td style="position:relative;">
              <?php echo Configure::read('P2');?>
                    	<br/>              
</td>
<td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
				<td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
	          </tr>
	            <td>
	              <?php echo Configure::read('P3');?>            
                 </td>
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
	          </tr>
	          <tr>
			 <tr>
	            <td>
	              <?php echo Configure::read('P4');?>      
                   </td>
	            <td></td>
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
	          </tr>
	           <tr>
	            <td>
	             <?php echo Configure::read('P5');?>
 </td>
	            <td></td>
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
	          </tr>
                 <tr>
	            <td>
	              <?php echo Configure::read('P6');?>

                     </td>
	            <td></td>
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
	          </tr>
                 <tr>
	            <td>
	              
	              <?php echo Configure::read('P7');?>
                      </td>
	            <td></td>
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
	          </tr>
                   <tr>
	            <td>
	             <?php echo Configure::read('P8');?>
                   </td>
	            <td></td>
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png"></span></td>
	          </tr>
                  
              <tr>
	            <td>
	              <?php echo Configure::read('P9');?>
				</td>
	            
	            <td><span style="font-weight : bold;font-size: 14px;">1</span></td>
	            <td><span style="font-weight : bold;font-size: 14px;">3</span></td>
                   <td><span style="font-weight : bold;font-size: 14px;">7</span></td>
   	          
               </tr>


	          <tr>
	            <td>
	              <?php echo Configure::read('P11');?>
				</td>
	            <td></td>
	            <td><span style="font-weight : bold;font-size: 14px;">Quarterly</span></td>
	            <td><span style="font-weight : bold;font-size: 14px;">Monthly</span></td>
	          </tr>
	          <tr>
</tr>

             <tr class="lastbottom">
	            <td><?php echo Configure::read('P12');?>
 </td>
	            <td>
                   
		
<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
<?php 
if($this->Session->read('Auth.User.id')==''){ ?>
                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:register_popup();"></div></td>
	            <td>	<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
			                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:submitPlan(2);"></div></td>
	            <td>				<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
						                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:submitPlan(3);"></div></td>
 <?php } elseif($this->Session->read('Auth.User.id')!='' && $this->Session->read('Auth.User.role_id')=='2'){?>
<input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="window.location.href='<?php e(SITE_URL."thanks");?>'"></div></td>
	            <td>	<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
			                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="window.location.href='<?php e(SITE_URL."thanks");?>'"></div></td>
	            <td>				<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
						                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="window.location.href='<?php e(SITE_URL."thanks");?>'"></div></td>
 
<?php } elseif($this->Session->read('Auth.User.id')!='' && $this->Session->read('Auth.User.role_id')=='3' && $plan_type =='Basic'){ ?>	          
<input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Your plan" onclick="javascript:register_popup();"></div></td>
	            <td>	<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
			                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:submitPlan(2);"></div></td>
	            <td>				<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
						                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:submitPlan(3);"></div></td>

<?php } elseif($this->Session->read('Auth.User.id')!='' && $this->Session->read('Auth.User.role_id')=='3' && $plan_type =='Pro'){ ?>

<input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:register_popup();"></div></td>
	            <td>	<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
			                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Your plan" onclick="javascript:submitPlan(2);"></div></td>
	            <td>				<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
						                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:submitPlan(3);"></div></td>

 <?php } else{?> 
<input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:register_popup();"></div></td>
	            <td>	<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
			                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Start now" onclick="javascript:submitPlan(2);"></div></td>
	            <td>				<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
						                    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Your plan" onclick="javascript:submitPlan(3);"></div></td>

 


<?php }?>

</tr>
	        

</table>
	        	          
	      </div>
		 		<?php //e($form->end()); ?>
	    </div>
	  </div>
	</div>
 </br><br/><br/>
	<script src="http://code.jquery.com/jquery-1.7.2.js"></script>
	<script>
	$(document).ready(function(){
	  $(function(){
	  var from=['Business Strategy','Workplace Conflict','Mergers &amp; Acquisition','Employee Engagement','Executive Coaching'], i=1; // i for counting
	      setInterval(function(){
	          $('#from').fadeOut(function(){ //fadeout text
	          $(this).html(from[i=(i+1)%from.length]).fadeIn(); //update, count and fadeIn
	          });
	      }, 3500 ); //2s

	  var to=['Succession Planning','Customer Service','Change Management','Lean Six Sigma','New Product Launch'], j=1; // j for counting
	      setInterval(function(){
	          $('#to').fadeOut(function(){ //fadeout text
	          $(this).html(to[j=(j+1)%to.length]).fadeIn(); //update, count and fadeIn
	          });
	      }, 3500 ); //2s

	  });
	});

	function submitPlan(plan_id) {
		window.location.href = SITE_URL+'linkedin/signuplinkclientpopup.php?plan_id='+plan_id;
	}
	</script>

</body>
</html>
