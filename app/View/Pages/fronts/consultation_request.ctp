<!--------------content-wrapper-started------------->
<div id="inner-content-wrapper">
<div id="inner-content-box" class="pagewidth">
  <div class="inner_wrapper">
    <div class="underline">
      <h1 style="font-size: 20px;">Request a free consultation</h1>
    </div>
    <div class="">
        <?php
            echo $session->flash();
        ?>
        <?php 
					  	$urlk1 = ($this->data['User']['url_key']);
						$displink=SITE_URL.strtolower($urlk1); 
              ?>
      <div class="figure-left align-left-botttom ">
        <?php
        $flag = 0; $defaultQuestion=0;
        if (isset($this->data['UserImage'][0]['image_name']) && $this->data['UserImage'][0]['image_name'] != '') {
            $file = file_exists(WWW_ROOT . 'img' . '/' . MENTORS_IMAGE_PATH . '/' . $this->data['User']['id'] . '/' . $this->data['UserImage'][0]['image_name']);
            if ($file == 0) {
                $flag++;
            }
        } else {
            $flag++;
        }
        if ($flag == 0) {
            e($html->link($html->image(MENTORS_IMAGE_PATH . '/' . $this->data['User']['id'] . '/' . $this->data['UserImage'][0]['image_name'],array('alt' => $this->data['UserReference']['first_name'],'class'=>'thumb')),$displink,array('escape'=>false,'style'=>'border: 0px;')));
      } else {
            e($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name'],'class'=>'thumb')));
        }
      ?>      
        <div class="figcaption" style="margin-top: 115px; position: static;">        
         <h2><span><?php e($html->link(ucwords($this->data['UserReference']['first_name']).' '.($this->data['UserReference']['last_name']),$displink,array('escape'=>false)));?></span></h2>
            <div class="profile-city" style="color: #292929;"> <span style="font-family:Ubuntu,arial,vardana;font-size: 12px;"><?php echo $cityData['City']['city_name'].', '.$cityData['City']['state'];?></span>
                    </div>
            <div class="profile-url" style="color: #292929;">
             <?php
				$disp="mentorsguild.com/".strtolower($this->data['User']['url_key']);
				$completelink=SITE_URL.strtolower($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($this->data['User']['url_key']);
				?>
				<?php e($disp); ?></div>
        </div>
      </div>
      <?php
      echo $form->create(null,array('url'=>array('controller'=>'fronts','action'=>'consultation_request',$this->data['User']['url_key']),'id'=>'mentorshipForm'));
      ?>
      <div>
      <?php 
      $i = 0;
      if(!empty($this->data['Question']) && false){?>
            <?php           
            foreach($this->data['Question'] as $key=>$question){
                if($question['question_name'] !=''){                    
                    ?>              
                    <p>
                      <label><?php e($question['question_name']); ?> </label>
                      <br />
                      <?php e($form->hidden('Answer.'.$i.'.question_name',array('value'=>$question['question_name']))); ?>
                      <?php e($form->hidden('Answer.'.$i.'.question_id',array('value'=>$question['id']))); ?>
                      <?php e($form->textarea('Answer.'.$i.'.answer',array('style'=>'width:98%;height:30px;padding-top:10px;'))); ?>              
                    </p>
                    <?php
                    $i++;
                }
                                
            }?>
      
        <?php
     }
     
     if($i == 0) {
           $defaultQuestion = 1;
     ?>
        
        <p>
          <label>What do you need solved?</label>
          <br />
          <?php e($form->textarea('Mentorship.mentee_need',array('div'=>false,'label'=>false,'class'=>"textarea big",'style'=>'width:98%;height:60px;padding-top:10px;'))); ?>
          
        </p>
        <?php
     }
     e($form->hidden('count',array('value'=>$i,'id'=>'count')));
        if($this->data['UserReference']['application_5_fee'] ==1){?>
            <p>
            Pay $<?php echo Configure::read('ApplicationFee');?> application fee
        <?php e($form->hidden('paypal_fee',array('value'=>Configure::read('ApplicationFee')))); ?>
            </p>        
        <?php
        }?>     
        <?php
       /* if($this->data['UserReference']['application_5_fee'] ==1){
            e($form->submit('Submit', array('type'=>'image','src' =>SITE_URL.'/img/express-checkout-hero.png','class'=>'mentorshipBtn')));  
            
        }else{
            e($form->submit('Submit',array('class'=>'mentorshipBtn', 'style'=>'float:right; margin-right: 10px;')));
        }*/?>
        
        <?php if($this->Session->read('Auth.User.id') == '') {?>
            <div id="oldButtonDiv" style="float: right; margin-bottom:80px; margin-top: 20px;" >
					<span style="float:left;padding:11px 18px;"><?php e($html->link('Cancel',SITE_URL.'fronts/index',array('class'=>'reset','style'=>""))); ?></span>	
                   <input id="openButton" type="button" value="Send request" class="mgButton" style="margin-right: 11px;">
			</div>	
			<?php e($form->end()); ?>
			<div style="margin-left: 260px; margin-top: 20px; display: none;" id="regDivText">
				<span style="font-weight: bold; margin-right: 10px; margin-top: 18px;"><img src="<?php e(SITE_URL);?>img/media/arrow.png"></img> To continue, Register or Submit as a guest</span>
			</div>
			<div id="linkedinDiv" style="display: none; margin-top: 20px; width: 850px;" class="pagewidth">
				<div style="float: left; width: 42%;" >
					<div> 
						<div class="submitProfile" style="cursor:pointer; margin-top: 95px;">
							
							<?php e($html->image('linked-in.png',array('alt'=>'Linked in','style'=>'cursor:pointer;'))); ?>
						</div>
					</div>
					<div style="display: block; font-size: 11px; margin-bottom:80px; margin-top: 10px; float: right;">Mentors Guild will <span style="font-weight: bold;">never </span>post to your LinkedIn account without your permission.</div>
				</div>
				<div class="vrtLINE"></div>
				<div style="float: right; width: 49%;" >
					<div style="margin-top: 20px;"> 
						      <?php
      							echo $form->create(null,array('url'=>array('controller'=>'fronts','action'=>'new_consultation_request',$this->data['User']['url_key']),'id'=>'newmentorshipForm'));
      							?>
      							<table>
      								<tbody>
      									<tr>
      										<td style="float: right; margin-top: 16px; margin-right: 10px;">Name</td>
      										<td>
      											<input name="data[Mentorship][firstname]" type="text" class="forminput" id="userfname" value="" maxlength="120" placeholder="First" style="width:153px;">
												<input name="data[Mentorship][lastname]" type="text" class="forminput" id="userlname" value="" maxlength="120" placeholder="Last" style="width:153px;">
      										</td>
      									</tr>
      									<tr>
      										<td style="float: right; margin-top: 10px; margin-left: 5px;">Work email</td>
      										<td>
												<input name="data[Mentorship][email]" type="text" class="forminput" placeholder = "email@yourcompany.com" id="useremail" value="" maxlength="250" style="width:332px;">      										
											</td>
      									</tr>
      									<tr>
      										<td style="float: right; margin-top: 16px; margin-right: 10px;">Phone</td>
      										<td>
												<input name="data[Mentorship][phone]" type="text" class="forminput" placeholder = "(xxx) xxx-xxxx" id="userphone" value="" maxlength="250" style="width:332px;">      										
											</td>
      									</tr>
      								</tbody>
      							</table>
							
							<input name="data[Mentorship][mentee_need]" type="hidden" value="" id="myanswer">
							
					</div>
					<div style="float: right;" >
						<span style="float:left;padding:11px 18px;"><?php e($html->link('Cancel',SITE_URL.'fronts/index',array('class'=>'reset','style'=>""))); ?></span>	
		                   <input id="sendButton" type="submit" value="Send request" class="mgButton" style="margin-bottom: 20px; margin-right: 5px;float: right;">
					</div>	
					</form>
				</div>
			</div>
			<div>
				<p style="margin-top: 60px;"><strong>Note:</strong></p>
		        <ul>
		          <li><?php e(ucfirst(strtolower($this->data['UserReference']['first_name'])));?> will get back to you within 2 business days.</li>
		          <li>Need assistance? Call us toll free at <b>1-866-511-1898</b>.</li>
		        </ul>
	        </div>
	        
		<?php } else {?>
			   <?php  e($form->submit('Send request',array('class'=>'mentorshipBtn', 'style'=>'float:right;margin-top: 20px; margin-right: 11px;')));?>
                    <span style="float:right;padding:11px 18px;margin-top: 20px;"><?php e($html->link('Cancel',SITE_URL.'fronts/index',array('class'=>'reset','style'=>""))); ?></span>
			<div>
				<p style="margin-top: 60px;"><strong>Note:</strong></p>
		        <ul>
		          <li><?php e(ucfirst(strtolower($this->data['UserReference']['first_name'])));?> will get back to you within 2 business days.</li>
		          <li>Need assistance? Call us toll free at <b>1-866-511-1898</b>.</li>
		        </ul>
	        </div>
                   <?php }?>
                  </div>
      
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
    //validation    
    jQuery(document).ready(function(){
        
        jQuery(".mentorshipBtn").click(function(){
            var flag = 0;
            var count = jQuery("#count").val();
            <?php if($defaultQuestion==0):?>
            for(i=0;i<count;i++){
            
                if(jQuery.trim(jQuery("#Answer"+i+"Answer").val()) == '') {
                    jQuery("#Answer"+i+"Answer").css('border-color','#F00');
                    jQuery("#Answer"+i+"Answer").focus();               
                    flag++;
                }else {
                    jQuery("#Answer"+i+"Answer").css('border-color','');                    
                }           
                
            }
            <?php else: ?>
                if(jQuery.trim(jQuery("#MentorshipMenteeNeed").val()) == '') {
                    jQuery('#MentorshipMenteeNeed').css('border-color','#F00');
                    jQuery('#MentorshipMenteeNeed').focus();                
                    flag++;
                }else {
                    jQuery('#MentorshipMenteeNeed').css('border-color','');                 
                }
           <?php endif; ?>
            if(flag==0){
                //jQuery("#mentorshipForm").submit();
                return true;
            }else{              
                return false;
            }
        });

    	jQuery(".submitProfile").click(function(){

            var flag = 0;
            var count = jQuery("#count").val();
            var mentor_id = <?php e($this->data['User']['id']);?>;
            <?php if($defaultQuestion==0):?>
            for(i=0;i<count;i++){
                if(jQuery.trim(jQuery("#Answer"+i+"Answer").val()) == '') {
                    jQuery("#Answer"+i+"Answer").css('border-color','#F00');
                    jQuery("#Answer"+i+"Answer").focus();               
                    flag++;
                }else {
                    jQuery("#Answer"+i+"Answer").css('border-color','');                    
                }           
            }
            <?php else: ?>
            var mentorshipneed = jQuery("#MentorshipMenteeNeed").val();
                if(jQuery.trim(jQuery("#MentorshipMenteeNeed").val()) == '') {
                    jQuery('#MentorshipMenteeNeed').css('border-color','#F00');
                    jQuery('#MentorshipMenteeNeed').focus();                
                    flag++;
                }else {
                    jQuery('#MentorshipMenteeNeed').css('border-color','');                 
                }
           <?php endif; ?>

    			if(flag > 0) {
    				return false;
    			} else {
    			       jQuery.ajax({
    						url:SITE_URL+'fronts/consultation_request_ajax',
    						type:'post',
    						dataType:'json',
    						data: '&mentorshipneed=' + mentorshipneed + '&mentor_id=' + mentor_id,
    						success:function(id){
    							window.location.href = SITE_URL+'linkedin/signuplinkclientpopup.php?mentorshipId='+id;
    						}
    					});	
    			       
    			   		return false;
    			}
    	    });

    		jQuery("#openButton").click(function() {
    			document.getElementById('linkedinDiv').style.display = 'inline-block';
    			document.getElementById('oldButtonDiv').style.display = 'none';
    			document.getElementById('regDivText').style.display = 'block';
    			jQuery('html, body').animate({
    		        scrollTop: jQuery("#regDivText").offset().top
    		    }, 1000);
    		});


			jQuery("#sendButton").click(function() {
                            var flag = 0;
				
				var answer = jQuery.trim(jQuery("#MentorshipMenteeNeed").val());
		      if(jQuery.trim(jQuery("#MentorshipMenteeNeed").val()) == '') {
                    jQuery('#MentorshipMenteeNeed').css('border-color','#F00');
                    jQuery('#MentorshipMenteeNeed').focus();                
                    flag++;
                    }else {
                    jQuery('#MentorshipMenteeNeed').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#userfname").val()) == '') {
                    jQuery('#userfname').css('border-color','#F00');
                    jQuery('#userfname').focus();                
                    flag++;
                    }else {
                    jQuery('#userfname').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#userlname").val()) == '') {
                    jQuery('#userlname').css('border-color','#F00');
                    jQuery('#userlname').focus();                
                    flag++;
                    }else {
                    jQuery('#userlname').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#useremail").val()) == '') {
                    jQuery('#useremail').css('border-color','#F00');
                    jQuery('#useremail').focus();                
                    flag++;
                    }else {
                    jQuery('#useremail').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#userphone").val()) == '') {
                    jQuery('#userphone').css('border-color','#F00');
                    jQuery('#userphone').focus();                
                    flag++;
                    }else {
                    jQuery('#userphone').css('border-color','');                 
                    }
    			if(flag > 0) {
    				return false;
    			} else {
				jQuery("#myanswer").val(answer);

				jQuery("#newmentorshipForm").submit();
                     }
    		});
    		
    });
</script>
