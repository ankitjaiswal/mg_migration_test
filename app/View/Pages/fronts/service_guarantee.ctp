<?php header("Cache-control: public"); 

$cachefile = WWW_ROOT."cache/serviceguarantee.html";

if (file_exists($cachefile)) {
       
	include($cachefile); 
} else {
	ob_start();
	
?>
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
       <div id="content-submit">           
			<?php echo $this->data['StaticPage']['description'];?>
			<div class="text-content_row">
				<p style="text-align: left;">In addition to above, several of our members do not invoice you, unless you are fully satisfied with their service. Look for <img src="<?php e(SITE_URL);?>img/media/gurantee_shield28.png" alt="" width="22" height="22" style="margin-left: -2px; margin-right: -2px;"/> sign on their profile.</p>
			</div>
			<div style="float: right;">
		    	<input style="float: none; margin-top: 30px; margin-bottom: 30px; font-weight: normal;" value="Back to home" type="button" class="mgButton" onclick="window.location.href='<?php echo SITE_URL."fronts/index"; ?>'; ">
            </div>
	  </div>
     </div>
</div>
<?php
	$fp = fopen($cachefile, 'w'); 
	
// save the contents of output buffer to the file
fwrite($fp, ob_get_contents()); 
// close the file
fclose($fp); 
// Send the output to the browser
ob_end_clean();

include($cachefile); 

}