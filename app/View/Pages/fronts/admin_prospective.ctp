<?php e($javascript->link(array('fancybox/jquery.fancybox-1.3.4.pack', 'fancybox/jquery.mousewheel-3.0.4.pack'), false)); ?>
<?php e($html->css(array('jquery.fancybox-1.3.4')), false); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.addAttribute').fancybox();
    });		
</script>


<div class="fieldset">
    <h3 class="legend">
		Members
        <div class="total" style="float:right"> Total Members : <?php e(count($data)); ?>
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td  width="12%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Name</td>
                        <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Email</td>
                        <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Company</td>
                        <td  width="5%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Phone</td>
		          		<td  width="15%" align="left" valign="middle" class="Bdrrightbot Padtopbot6" style="padding-left:9px;">Answer</td>
			   			<td  width="15%" align="left" valign="middle" class="Bdrrightbot Padtopbot6" style="padding-left:9px;">Type</td>
                        <td  width="15%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Action</td>
                    </tr>
                    <?php 
                    if(!empty($data) && count($data)>0)
                    { 
                        foreach(array_reverse($data) as $key => $value)
                        {    
                        
                        ?>
                        <tr>
                            <td  width="12%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php e(ucwords($value['Prospect']['firstname']. ' '.$value['Prospect']['lastname'])); ?></td>
                            <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php e($value['Prospect']['email']); ?></td>  
                            <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php e($value['Prospect']['company']); ?></td>   
                            <td  width="5%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php e($value['Prospect']['phone']); ?></td>
                            <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php e($value['Prospect']['answer']); ?></td>
                            <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php e($value['Prospect']['prospect_type']); ?></td>
                            <td  width="15%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">
                           		<?php 
                           		if($value['Prospect']['isApproved'] == 0) {
                           		e($admin->getActionImage(array(
                           			'approve' => array('controller' => 'fronts', 'action' => 'prospective_approve'), 
                           			'delete' => array('controller' => 'fronts', 'action' => 'prospective_delete')), 
                           			$value['Prospect']['id']));
                           			}
                           			?>
                           </td> 
                        </tr>	
                   <?php
                        }
                    }else{ ?>
                   <tr>
                        <td colspan="3" width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Record not found</td>           
                    </tr>
                   <?php } ?>   
                </table>
            </div>
     <?php if (!empty($data)) { ?>
            <?php
            e($form->end());
        } else {
            ?>
            <div style="color:blue, font-size:20; padding-bottom:30px;padding-top:30px;" align="center" ><strong>No Records Found.</strong></div>
        <?php } ?>

    </div>
</div>
<div class="clr"></div>
	 