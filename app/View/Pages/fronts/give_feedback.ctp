<div id="inner-content-wrapper">
<div id="inner-content-box" class="pagewidth">
  <div class="inner_wrapper">
    <h1>Feedback</h1>
	<?php 
		e($form->create('Client',array('url'=>array('controller'=>'fronts','action'=>'give_feedback'),'id'=>'feedbackForm'))); 
		echo $form->hidden('Directory_user.id',array('value'=>$dUSer['Directory_user']['id'], 'id'=>'hidId'));
	?>	
	<div class="feedbackcontent">
		<div class="form_row">
			<p>Would you recommend this expert to others?</p>
			<div class="styled-form feedback-form">
				 <?php 
				 $y='';$n='';$m='';
				 if(isset($data['Feedback']['recommended']) && $data['Feedback']['recommended']==0): 
				 			$n='checked="checked"';  
				 elseif(isset($data['Feedback']['recommended']) && $data['Feedback']['recommended']==2):
					     $y='checked="checked"'; 
					 elseif(isset($data['Feedback']['recommended']) && $data['Feedback']['recommended']==1):
					     $m='checked="checked"';  
					 else:
						 $y='checked="checked"';
					 endif; ?>
				<input type="radio" <?php echo $y;?> value="2" id="feedback_y" name="data[Feedback][recommended]"><label for="feedback_y" style="font-family:'Ubuntu';">Yes</label>
				<input type="radio" <?php echo $n;?> value="0" id="feedback_n" name="data[Feedback][recommended]"><label for="feedback_n" style="font-family:'Ubuntu';">No</label>  
				<input type="radio" <?php echo $m;?> value="1" id="feedback_mb" name="data[Feedback][recommended]"><label for="feedback_mb" style="font-family:'Ubuntu';">Maybe</label>
				<div class="clear"> </div>
			</div>
		</div>
		<div class="form_row">
			<p>How has the expert helped you?</p>
			<textarea  style="height:161px; width: 835px;" id="feedbackText"  maxlength="1200" div="1" rows="9" name="data[Feedback][description]"><?php if(isset($data['Feedback']['description'])): echo $data['Feedback']['description']; endif;?></textarea>           
		</div>
	</div>
	      <?php if($this->Session->read('Auth.User.id') != '') {?>
				<div id="Apply" style="float: right; margin-bottom:40px;" >
					<input id="apply" class="submitProfile1 LTSpreview" type="submit" value="Submit">
				</div>
				<?php } else {?>
				<div style="float: right; margin-bottom:40px; float: right;" >
					<input id="openButton" type="button" value="Submit" class="mgButton">
				</div>	
			<?php }?>
	<?php 
		e($form->end()); 
	?>	
</div>
		<div id="linkedinDiv" style="display: none; margin-right: 50px;">
			<div style="float: right;" class="pagewidth"> 
				<div class="submitProfile" style="cursor:pointer;">
					<?php e($html->image('linked-in.png',array('alt'=>'Linked in','style'=>'cursor:pointer; float: right;'))); ?>
					<span style="font-weight: bold; margin-right: 10px; float: right; margin-top: 18px;">Only registered users can submit feedback  </span>
					<img style="float: right; margin-top: 15px;" src="<?php e(SITE_URL);?>img/media/arrow.png"></img>
				</div>  
			</div>
			<div style="float: right; display: block; margin-right: 70px; font-size: 11px; margin-bottom:80px; margin-top: 10px;">Mentors Guild will <b style="font-weight: bolder; font-family: Ubuntu,arial,vardana;">never</b> post to your LinkedIn account without your permission.</div>
		</div>
		
</div>
</div>
    
<script type="text/javascript">
jQuery(document).ready(function(){

    function updateCount ()
    {
        var qText = jQuery("#questionTextBox").val();

       if(qText.length < 4000) {
           jQuery("#noOfChar").html(4000 - qText.length);
       } else {
           jQuery("#noOfChar").html(0);
           jQuery("#questionTextBox").val(qText.substring(0,4000));
       }
    }

    jQuery("#questionTextBox").keyup(function () {
        updateCount();
    });
    jQuery("#questionTextBox").keypress(function () {
        updateCount();
    });
    jQuery("#questionTextBox").keydown(function () {
        updateCount();
    });


    jQuery(".submitProfile1").click(function(){

    	var flag = 0;
    	if(jQuery.trim(jQuery("#feedbackText").val()) == '') {
			jQuery('#feedbackText').css('border-color','#F00');
			jQuery('#feedbackText').focus();
			flag++;
		} else {
			jQuery('#feedbackText').css('border-color',''); 
		}

		if(flag > 0) {
			return false;
		} else {
			jQuery('#feedbackForm').submit();
		}
    });
        
    jQuery(".submitProfile").click(function(){
    	var flag = 0;
    	if(jQuery.trim(jQuery("#feedbackText").val()) == '') {
			jQuery('#feedbackText').css('border-color','#F00');
			jQuery('#feedbackText').focus();
			flag++;
		} else {
			jQuery('#feedbackText').css('border-color',''); 
		}

		if(flag > 0) {
			return false;
		} else {

			   var hidId = jQuery('#hidId').val();
			   var message = jQuery.trim(jQuery("#feedbackText").val());
			   var feedback = -1;
			   if(jQuery('#feedback_y').is(':checked'))
				   feedback = 1;
			   else if(jQuery('#feedback_n').is(':checked'))
				   feedback = 2;
			   else if(jQuery('#feedback_mb').is(':checked'))
				   feedback = 3;
		       jQuery.ajax({
					url:SITE_URL+'fronts/give_feedback',
					type:'post',
					dataType:'json',
					data: 'message='+ message + '&hidId=' + hidId  + '&feedback=' + feedback,
					success:function(id){
						window.location.href = SITE_URL+'linkedin/signuplinkclientpopup.php?feedbackId='+id;
					}
				});	
			return false;
			
		}
		
    });

	jQuery("#openButton").click(function() {
		document.getElementById('linkedinDiv').style.display = 'block';
		jQuery('html, body').animate({
	        scrollTop: jQuery("#linkedinDiv").offset().top
	    }, 2000);
	});
	
});
</script>
