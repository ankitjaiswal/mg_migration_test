<div id="inner-content-wrapper">
<div id="inner-content-box" class="pagewidth">
  <div class="inner_wrapper">
    <h2>Claim your free Mentors Guild profile</h2>
	<?php 
		e($form->create('Client',array('url'=>array('controller'=>'fronts','action'=>'claim_profile'),'id'=>'feedbackForm'))); 
		echo $form->hidden('Directory_user.id',array('value'=>$dUSer['Directory_user']['id'], 'id'=>'hidId'));
	?>	
	<div class="feedbackcontent">
		<div class="form_row" style="margin-left: 10px;">
			<ul>
				<li>Enhance your online presence</li>
				<li>Add profile details, case studies</li>
				<li>Answer questions from potential clients</li>
				<li>Leverage our sales engine and admin support</li>
			</ul>
		</div>
		<div  class="styled-form" style="margin-top: 17px;">
		<input type="checkbox" class="group1" name="catCheck[]" id="an"  placeholder=""  ><label for="an"><span style="font-family: Ubuntu, arial, vardana;font-size: 14px;"> </input>
                I confirm that I am <span style="font-weight:bold;"><?php e($dUSer['Directory_user']['first_name']." ".$dUSer['Directory_user']['last_name'])?></span>, and I accept Mentors Guild's <a href="<?php e(SITE_URL.'fronts/terms_of_service');?>" target="_blank;"> Terms of Service</a> and <a href="<?php e(SITE_URL.'fronts/privacy_policy');?>" target="_blank;"> Privacy policy</a></label>
		</div>
	</div>
	      <?php if($this->Session->read('Auth.User.id') != '') {?>
				<div id="Apply" style="float: right; margin-bottom:80px;">
					<input id="apply" class="submitProfile1 LTSpreview" type="submit" value="Submit" >
				</div>
				<?php } else {?>
				<div style="float: right; margin-top:17px;" class="pagewidth"> 
					<div class="submitProfile" style="cursor:pointer;">
						<?php e($html->image('linked-in.png',array('alt'=>'Linked in','style'=>'cursor:pointer; float: right;'))); ?>
						<span style="font-weight: bold; margin-right: 10px; float: right; margin-top: 18px;">To claim your profile </span>
						<img style="float: right; margin-top: 15px;" src="<?php e(SITE_URL);?>img/media/arrow.png"></img>
					</div>  
				</div>
				<div style="float: right; display: block; font-size: 11px; margin-bottom:80px; margin-top: 10px;">Mentors Guild will use LinkedIn to validate your identity. Mentors Guild will <b style="font-weight: bolder; font-family: Ubuntu,arial,vardana;">never</b> post to your LinkedIn account without your permission.</div>
			<?php }?>
	<?php 
		e($form->end()); 
	?>	
</div>
</div>
</div>
    
<script type="text/javascript">
jQuery(document).ready(function(){

    function OpenCheckboxAlert(){
 			var path=SITE_URL+'/members/checkbox_alert';
			TINY.box.show({url:path,width:400,height:140})
               }

    function updateCount ()
    {
        var qText = jQuery("#questionTextBox").val();

       if(qText.length < 4000) {
           jQuery("#noOfChar").html(4000 - qText.length);
       } else {
           jQuery("#noOfChar").html(0);
           jQuery("#questionTextBox").val(qText.substring(0,4000));
       }
    }

    jQuery("#questionTextBox").keyup(function () {
        updateCount();
    });
    jQuery("#questionTextBox").keypress(function () {
        updateCount();
    });
    jQuery("#questionTextBox").keydown(function () {
        updateCount();
    });

        if(!jQuery("#an").is(':checked')){
                       jQuery('#an').html('You must agree to the terms first.');                        
                       jQuery('#an').css('border-color','#F00');
			   jQuery('#an').focus();	
				
                      }else {
                    jQuery('#an').css('border-color','');
                        
                   }
    jQuery(".submitProfile").click(function(){
    	 var matches = [];
         jQuery(".group1:checked").each(function() {
              matches.push(this.value);
          }); 

         if(matches == 'on'){
    		var id = jQuery('#hidId').val();
			window.location.href = SITE_URL+'linkedin/signuplinkmemberpopup.php?d_user_id='+id;
         }
       else{
        javascript:OpenCheckboxAlert();
         }
    });
});
</script>
