<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div id="inner-content-wrapper" style="height: 400px">
    <div id="inner-content-box" class="pagewidth">
        <div id="user">
            <h1 style="color:#000;font-size:15px;">
				PayPal payment was not confirmed.<br /><br />
				Please try again or contact <a href="mailto:help@mentorsguild.com">help@mentorsguild.com</a><br /><br />
				Thanks!
            </h1>
        </div>
        <div id ="app-button">		
            <?php e($form->create('User', array('url' => '/','method'=>'link')));?>     
                <input type="submit" value="Done" id="submit"/>
            <?php e($form->end()); ?>
        </div>
    </div>
</div>
<style type="text/css">
.flash_good {
    font-family: Ubuntu;
    font-size: 15px;
}
</style>