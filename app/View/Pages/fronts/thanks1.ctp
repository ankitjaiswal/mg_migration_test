
<div id="inner-content-wrapper" style="height: 400px">
    <div id="inner-content-box" class="pagewidth">
        <div id="user">
            <h1 style="color:#000;font-size:15px;">
                Currently, consultants cannot register as clients. Instead, use our <a href='http://www.linkedin.com/groups?gid=4410518' target='_blank'>Linked In</a> group to collaborate.
            </h1>
        </div>
        <div style="padding: 20px 0px;color:grey;font-size:15px;">
            <h1 style="font-size:15px;color:#000;">
                <br/><br/>Thanks for your interest!
            </h1>
        </div>
        <div id ="app-button">		
                <br/><br/>
                <input type="submit" value="Done" onclick="window.location.href='<?php echo SITE_URL."users/my_account"; ?>'; ">
            
        </div>
    </div>
</div>