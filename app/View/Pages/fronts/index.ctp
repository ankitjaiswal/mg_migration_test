<script type="text/javascript">
function tipSShow(){ document.getElementById('showSearch').style.display="block"; }
function tipSHide(){ document.getElementById('showSearch').style.display="none"; }

function tipAShow(){ document.getElementById('showApply').style.display="block"; }
function tipAHide(){ document.getElementById('showApply').style.display="none"; }

function tipEShow(){ document.getElementById('showEngage').style.display="block"; }
function tipEHide(){ document.getElementById('showEngage').style.display="none"; }
</script>

<script type="text/javascript" src="<?php e(SITE_URL)?>js/jquery.tinycarousel.min.js"></script>
	<script type="text/javascript">         
    jQuery(document).ready(function(){               
                
    	jQuery('#slider-code').tinycarousel({ pager: true, interval: true, intervaltime: 10000, duration: 500    });
        
    });
</script> 


        <!-- for brand-icon patty -->

        <p style="text-align:center;clear:both; padding-top:20px; font-size:16px; font-family: 'Ubuntu';">Our Consultants Have Worked With</p>
        <div class="brand-icon">       	
        	<div class="apple_gray"></div>
        	<div class="apple"></div>

        	<div class="accenture_gray"></div>
        	<div class="accenture"></div>

        	<div class="ted_gray"></div>
        	<div class="ted"></div>

        	<div class="ge_gray"></div>
        	<div class="ge"></div>

        	<div class="citi_gray"></div>
        	<div class="citi"></div>

        	<div class="deloitte_gray"></div>
        	<div class="deloitte"></div>

        	<div class="gg_gray"></div>
        	<div class="gg"></div>

        	<div class="hilton_gray"></div>
        	<div class="hilton"></div>

        	<div class="oracle_gray"></div>
        	<div class="oracle"></div>

        	<div class="png_gray"></div>
        	<div class="png"></div>

        	<div class="hp_gray"></div>
        	<div class="hp"></div>
        </div>
  
<div>

    <div id="content-box" style="height:40px;">
    	<div class="bgbanner" style="background:#f9f8f8;">
			<div class="widthsize">
				<table>
					<tr>

							<p style="text-align:center;clear:both; font-size:16px; font-family: 'Ubuntu';">
								...And Scores of Small and Mid Sized Organizations.&nbsp;<a href="<?php (SITE_URL); ?>/ivan.rosenberg#case_studies">A Sample Case Study</a>.
                                                             
							</p>

					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

    <div id="content-box" >
    	<div class="bgbanner">
			<div class="widthsize">
				<table>
					<tr>
						<td style="width: 90%;">
							<p style="padding-top: 20px; font-family: 'Ubuntu', arial; font-size: 16px;">
								<?php echo Configure::read('projectHome');?>
							</p>
						</td>
						<td>
							<a href="<?php e(SITE_URL);?>fronts/service_guarantee" class="gaurantee_img"></a>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
  
	<div style="background-color: #292929;width: 100%;">
<div id="slider-code" class="pagewidth">
    <div class="viewport">
        <ul class="overview">
        	<?php foreach($featuredMentorArray as $featuredMentor){?>
            <li>
            	<div id="content-bottom" class="pagewidth">
		            <h2 style="text-align:center;clear:both; padding-top:20px; color: #dedede;font-size: 16px; font-family: 'Ubuntu';  padding-bottom: 5px;">Testimonial</h2>
		            <?php
						$flag = 0;
						if (isset($featuredMentor['UserImage'][0]['image_name']) && $featuredMentor['UserImage'][0]['image_name'] != '') {
							$file = file_exists(WWW_ROOT . 'img' . '/' . MENTORS_IMAGE_PATH . '/' . $featuredMentor['User']['id'] . '/' . $featuredMentor['UserImage'][0]['image_name']);
							if ($file == 0) {
								$flag++;
							}
						} else {
							$flag++;
						}
						?>						
						<div id="content-left">
						<?php
						$displink1=SITE_URL.strtolower($featuredMentor['User']['url_key']);
						?>
							<?php
							if ($flag == 0) {
									
									
								e($html->link($html->image(MENTORS_IMAGE_PATH . '/' . $featuredMentor['User']['id'] . '/' . $featuredMentor['UserImage'][0]['image_name'], array('class'=>'desaturate','width'=>'200px','height'=>'200px','alt' => $featuredMentor['UserReference']['first_name']." ".$featuredMentor['UserReference']['last_name'])),$displink1,array('escape'=>false)));
							} else {
								e($html->link($html->image('media/profile.png',array('width'=>'200px','height'=>'200px','alt'=>$featuredMentor['UserReference']['first_name']." ".$featuredMentor['UserReference']['last_name'])),$displink1,array('escape'=>false,'style'=>'border: 1px solid #DCDCDC;display:block;')));
		
							}
							?>
						</div>
						<div id="content-right" itemscope itemtype="http://schema.org/Person">
							<h2 style="font-family: Ubuntu; font-size: 20px; font-weight: bold;border-bottom: 1px solid #BEBEBE; padding-top:4px; padding-bottom: 5px;"><span itemprop="name"><?php e($html->link(ucfirst(strtolower($featuredMentor['UserReference']['first_name'])) . ' ' . ucfirst($featuredMentor['UserReference']['last_name']),$displink1,array('escape'=>false,'style'=>'color:#fff;'))); ?> </span>

							<span style="color:#dedede;font-family: Ubuntu; font-size: 15px; font-weight: normal;">&#8212; <?php e($featuredMentor['UserReference']['area_of_expertise']) ?></span>
							</h2>
							<br>
							<div id="text-content" >

								<p>
                                                <?php $count = 1; 
                                                   foreach ($featuredMentor['Testimonial'] as $testValue) {
                                                       $count++;}
                                                       $testimonialCount = $count - 1;?>

                                              <span style="color: #dedede;font-size: 15px;font-style: italic;font-family: Ubuntu;"><?php $i=0;
                	                          foreach(($featuredMentor['Testimonial']) as $testimonial) {	
			                             $i++;	
			                               if($i == $testimonialCount){
                                                    $length = strlen($testimonial['testimonial_detail']);
                                              if($length >= 300){
                                               $string = substr($testimonial['testimonial_detail'], 0, strpos(wordwrap($testimonial['testimonial_detail'], 300), "\n"));
                                                  echo ($string);} 
                                                  else{
                                              echo ($testimonial['testimonial_detail']);} 
                                              }}?>..</span>
                                              <?php
							$disp=SITE_URL.$featuredMentor['User']['url_key'];
							$displink=SITE_URL.strtolower($featuredMentor['User']['url_key'])."#testimonials";
							?>
							<?php e($html->link('read more',$displink,array('escape'=>false,'style'=>'font-family: Ubuntu;'))); ?>                                                           
                                               <br><br>
                                             <span style="float:right;color: #dedede;font-family: Ubuntu; font-size: 14px; "> <?php echo $testimonial['client_name']?>,&nbsp;<?php echo $testimonial['client_designation']?></span><br><span style="float:right;color: #dedede;font-family: Ubuntu; font-size: 14px; "><?php echo $testimonial['client_organization']?></span>		
								</p>
							</div>

						</div>
        			</div>	
           		 </li>
            <?php }?>
        </ul>
    </div>

</div>
	<div>
</div>
</div>
        <div  id="register" style="margin-top: 40px;margin-bottom: 40px;">
	<div id="inner-content-box" class="pagewidth">
		<div id="inner-content-wrapper">
		  <div id="inner-content-box"> 
				<div class="logotextdot" style="padding-top:15px;">
					<p>
						<b>Mentors Guild</b><span style="color:#F32C33; font-size:30px;">.</span>
						<span style="font-family:Ubuntu;font-size:16px;color:#2a2a2a;font-weight:normal;"><?php echo Configure::read('Regpopup1');?></span>
					</p>
                                    </div>
					<div class="consultantbox">
						<span style="font-family:Ubuntu,arial,vardana;font-size:16px;color: #2a2a2a;font-weight:bold;margin-left:85px;">Consultants</span>
						<p style="font-color:#2a2a2a;">
							<?php echo Configure::read('Regpopup2');?>
						</p>
						<p>
                                        <input class="btn"  onclick="window.location.href='<?php echo SITE_URL;?>members/eligibility'" type="submit" style="margin-left:74px;" value="Apply Now">
					     </p>
                                           </div>
					<div class="vrtLINE" style="margin-left:150px;"></div>
					<div class="clientbox">
						<span style="font-family:Ubuntu,arial, vardana;font-size:16px;color: #2a2a2a;font-weight:bold;margin-left:107px;">Clients</span>
						<p style="font-color:#2a2a2a;">
							<?php echo Configure::read('Regpopup3');?></p>
                                          <p>
						<input class="btn" onclick="window.location.href='<?php echo SITE_URL;?>pricing'" type="submit" style="margin-left:86px;" value="Get Started">
					      </p>
                                         </div>
					<div class="clear"></div>
				</div>
			
			
		</div>
	</div>
</div>
<script type="text/javascript">
mixpanel.track("Page View", {"URL": document.URL});
</script>
<style>
.bgbanner{width:100%;height:112px;background-color:rgb(230,231,232);font-family:Ubuntu, sans-serif;}
</style>