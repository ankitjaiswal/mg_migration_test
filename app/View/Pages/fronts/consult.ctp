<div id="inner-content-wrapper">
<div id="inner-content-box" class="pagewidth">
  <div class="inner_wrapper">
    <div class="underline">
      <h1 style="font-size: 20px;">Request a free initial consultation</h1>
    </div>
    <div class="">
      <?php
      echo $form->create(null,array('url'=>array('controller'=>'fronts','action'=>'consult'),'id'=>'mentorshipForm'));
      ?>
      <input type="hidden" name="data[Directory_user][id]" value="<?php e($dUSer['Directory_user']['id']);?>" id="hidId"/>
      <div>
        <p>
          <label>What do you need solved? <span style="float:right;">(<span id="noOfChar">4000</span>  characters remaining)</span></label>
          <br />
          <?php e($form->textarea('Mentorship.mentee_need',array('rows'=>10,'div'=>false,'label'=>false,'class'=>"textarea big",'style'=>'width:98%;padding-top:10px;', 'id'=>'questionTextBox'))); ?>
        </p>
      </div>
      <?php if($this->Session->read('Auth.User.id') == '') {?>


           <div id="oldButtonDiv" style="float: right; margin-bottom:80px; margin-top: 20px;" >
			<span style="float:left;padding:11px 18px;"><?php e($html->link('Cancel',SITE_URL.'fronts/index',array('class'=>'reset','style'=>""))); ?></span>	
                   <input id="openButton" type="button" value="Send request" class="mgButton" style="margin-right: 11px;">
			</div>	
			<?php e($form->end()); ?>
			<div style="margin-left: 260px; margin-top: 20px; display: none;" id="regDivText">
				<span style="font-weight: bold; margin-right: 10px; margin-top: 18px;"><img src="<?php e(SITE_URL);?>img/media/arrow.png"></img> To continue, Register or Submit as a guest</span>
			</div>
			<div id="linkedinDiv" style="display: none; margin-top: 20px; width: 850px;" class="pagewidth">
				<div style="float: left; width: 42%;" >
					<div> 
						<div class="submitProfile" style="cursor:pointer; margin-top: 95px;">
							
							<?php e($html->image('linked-in.png',array('alt'=>'Linked in','style'=>'cursor:pointer;'))); ?>
						</div>
					</div>
					<div style="display: block; font-size: 11px; margin-bottom:80px; margin-top: 10px; float: right;">Mentors Guild will <span style="font-weight: bold;">never </span>post to your LinkedIn account without your permission.</div>
				</div>
				<div class="vrtLINE"></div>
				<div style="float: right; width: 49%;" >
					<div style="margin-top: 20px;"> 
						      <?php
      							echo $form->create(null,array('url'=>array('controller'=>'fronts','action'=>'visitor_consultation_request',$dUSer['Directory_user']['id']),'id'=>'newmentorshipForm'));
      							?>
      							<table>
      								<tbody>
      									<tr>
      										<td style="float: right; margin-top: 16px; margin-right: 10px;">Name</td>
      										<td>
      											<input name="data[Mentorship][firstname]" type="text" class="forminput" id="userfname" value="" maxlength="120" placeholder="First" style="width:153px;">
												<input name="data[Mentorship][lastname]" type="text" class="forminput" id="userlname" value="" maxlength="120" placeholder="Last" style="width:153px;">
      										</td>
      									</tr>
      									<tr>
      										<td style="float: right; margin-top: 10px; margin-left: 5px;">Work email</td>
      										<td>
												<input name="data[Mentorship][email]" type="text" class="forminput" id="useremail" value="" maxlength="250" style="width:332px;">      										
											</td>
      									</tr>
      									<tr>
      										<td style="float: right; margin-top: 16px; margin-right: 10px;">Phone</td>
      										<td>
												<input name="data[Mentorship][phone]" type="text" class="forminput" placeholder = "(xxx) xxx-xxxx" id="userphone" value="" maxlength="250" style="width:332px;">      										
											</td>
      									</tr>
      								</tbody>
      							</table>
							
							<input name="data[Mentorship][mentee_need]" type="hidden" value="" id="myanswer">
							
					</div>
					<div style="float: right;" >
						<span style="float:left;padding:11px 18px;"><?php e($html->link('Cancel',SITE_URL.'fronts/index',array('class'=>'reset','style'=>""))); ?></span>	
		                   <input id="sendButton" type="submit" value="Send request" class="mgButton" style="margin-bottom: 20px; margin-right: 5px;float: right;">
					</div>	
					</form>
				</div>
			</div>


				<?php } else {?>
			   <?php  e($form->submit('Send request',array('class'=>'mentorshipBtn', 'style'=>'float:right; margin-right: 11px;')));?>
                    <span style="float:right;padding:11px 18px;"><?php e($html->link('Cancel',SITE_URL.'fronts/index',array('class'=>'reset','style'=>""))); ?></span>
			<?php }?>
      <?php e($form->end()); ?>
    </div>
  </div>

		
</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){

    function updateCount ()
    {
        var qText = jQuery("#questionTextBox").val();

       if(qText.length < 4000) {
           jQuery("#noOfChar").html(4000 - qText.length);
       } else {
           jQuery("#noOfChar").html(0);
           jQuery("#questionTextBox").val(qText.substring(0,4000));
       }
    }

    jQuery("#questionTextBox").keyup(function () {
        updateCount();
    });
    jQuery("#questionTextBox").keypress(function () {
        updateCount();
    });
    jQuery("#questionTextBox").keydown(function () {
        updateCount();
    });

    		jQuery("#openButton").click(function() {
    			document.getElementById('linkedinDiv').style.display = 'inline-block';
    			document.getElementById('oldButtonDiv').style.display = 'none';
    			document.getElementById('regDivText').style.display = 'block';
    			jQuery('html, body').animate({
    		        scrollTop: jQuery("#regDivText").offset().top
    		    }, 1000);
    		});

			jQuery("#sendButton").click(function() {
                            var flag = 0;
				
		      var answer = jQuery.trim(jQuery("#questionTextBox").val());
		      if(jQuery.trim(jQuery("#questionTextBox").val()) == '') {
                    jQuery('#questionTextBox').css('border-color','#F00');
                    jQuery('#questionTextBox').focus();                
                    flag++;
                    }else {
                    jQuery('#questionTextBox').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#userfname").val()) == '') {
                    jQuery('#userfname').css('border-color','#F00');
                    jQuery('#userfname').focus();                
                    flag++;
                    }else {
                    jQuery('#userfname').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#userlname").val()) == '') {
                    jQuery('#userlname').css('border-color','#F00');
                    jQuery('#userlname').focus();                
                    flag++;
                    }else {
                    jQuery('#userlname').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#useremail").val()) == '') {
                    jQuery('#useremail').css('border-color','#F00');
                    jQuery('#useremail').focus();                
                    flag++;
                    }else {
                    jQuery('#useremail').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#userphone").val()) == '') {
                    jQuery('#userphone').css('border-color','#F00');
                    jQuery('#userphone').focus();                
                    flag++;
                    }else {
                    jQuery('#userphone').css('border-color','');                 
                    }
    			if(flag > 0) {
    				return false;
    			} else {
				jQuery("#myanswer").val(answer);

				jQuery("#newmentorshipForm").submit();
                     }
    		});

    jQuery(".mentorshipBtn").click(function(){

    	var flag = 0;
    	if(jQuery.trim(jQuery("#questionTextBox").val()) == '') {
			jQuery('#questionTextBox').css('border-color','#F00');
			jQuery('#questionTextBox').focus();
			flag++;
		} else {
			jQuery('#questionTextBox').css('border-color',''); 
		}

		if(flag > 0) {
			return false;
		} else {
			jQuery('#mentorshipForm').submit();
		}
    });
    
    jQuery(".submitProfile").click(function(){
    	var flag = 0;
    	if(jQuery.trim(jQuery("#questionTextBox").val()) == '') {
			jQuery('#questionTextBox').css('border-color','#F00');
			jQuery('#questionTextBox').focus();
			flag++;
		} else {
			jQuery('#questionTextBox').css('border-color',''); 
		}

		if(flag > 0) {
			return false;
		} else {

			   var hidId = jQuery('#hidId').val();
			   var message = jQuery.trim(jQuery("#questionTextBox").val());
		       jQuery.ajax({
					url:SITE_URL+'fronts/consult',
					type:'post',
					dataType:'json',
					data: 'message='+ message + '&hidId=' + hidId,
					success:function(id){
						window.location.href = SITE_URL+'linkedin/signuplinkclientpopup.php?directoryConsultationId='+id;
					}
				});	
			return false;
			
		}
		
    });


});
</script>
