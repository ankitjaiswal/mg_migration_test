<?php e($javascript->link(array('fancybox/jquery.fancybox-1.3.4.pack', 'fancybox/jquery.mousewheel-3.0.4.pack'), false)); ?>
<?php e($html->css(array('jquery.fancybox-1.3.4')), false); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.addAttribute').fancybox();
    });		
</script>


<div class="fieldset">
    <h3 class="legend">
		Members
        <div class="total" style="float:right"> Total Members : <?php e(count($data)); ?>
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td  width="25%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Takeaway Input Url</td>
                         <td  width="5%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"></td>
                        <td  width="20%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Takeaway Url</td>
                        
                        <td  width="20%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Consultant Name</td>
                        <td  width="20%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Prospect email</td>
                        <td  width="10%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Action</td>
                    </tr>
                    <?php 
                    if(!empty($data) && count($data)>0)
                    { 
                        foreach(array_reverse($data) as $key => $value)
                        {    
                        if($value['Takeaway_prospect']['prospect_email'] != '') {
                        ?>
                        <tr>
                            <td  width="25%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php e($value['Takeaway_prospect']['takeaway_input']); ?></td> 
                            <td  width="5%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"></td>
                            <td  width="20%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php e($value['Takeaway_prospect']['takeaway_link']); ?></td>  
                             
                            <td  width="20%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php e($value['Takeaway_prospect']['consultant_name']); ?></td> 
                            <td  width="20%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php e($value['Takeaway_prospect']['prospect_email']); ?></td> 
                             <td  width="10%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">
                           		<?php 
                           		if($value['Takeaway_prospect']['isApproved'] == 0) {
                           		e($admin->getActionImage(array(
                           			'approve' => array('controller' => 'fronts', 'action' => 'takeaway_prospective_approve'), 
                           			'delete' => array('controller' => 'fronts', 'action' => 'takeaway_prospective_delete')), 
                           			$value['Takeaway_prospect']['id']));
                           			}
                           			?>
                           </td> 
                        </tr>	
                   <?php
                       } }
                    }else{ ?>
                   <tr>
                        <td colspan="3" width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Record not found</td>           
                    </tr>
                   <?php } ?>   
                </table>
            </div>
     <?php if (!empty($data)) { ?>
            <?php
            e($form->end());
        } else {
            ?>
            <div style="color:blue, font-size:20; padding-bottom:30px;padding-top:30px;" align="center" ><strong>No Records Found.</strong></div>
        <?php } ?>

    </div>
</div>
<div class="clr"></div>
	 