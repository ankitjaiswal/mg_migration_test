<?php
/**
 * General Helper
 *
 *
 * @category Helper
 */
class GeneralHelper extends AppHelper {

    /**
     * Other helpers used by this helper
     *
     * @var array
     * @access public
     */
	   var $helpers = array('Html', 'Session');
	   
	  

				


	function generateTreeList($data = null, $level=0){
		$output 		= '';		
		$delimiter 		= "";
		$delimiters 	= "\n" . str_repeat($delimiter, $level * 2);
		
		foreach($data as $value){	
			$class = ''; 		
			if($value['Category']['parent_id'] == 0) {
			  $class = 'BgTab';
			  if($value['Category']['show_navigation'] == 0) {
				 $class .= ' viewAllCategory';	
			  } 
			
			}	
			$output.= '<li  name="toggle'.$value['Category']['id'].'" class="'.$class.'">'.$this->Html->link($delimiters.$value['Category']['name'],array('controller'=>'fronts','action'=>'product_info',$value['Category']['id']));	
			if(isset($value['children'][0])){
				$output .= '<ul>'.$this->generateTreeList($value['children'], $level+1).'</ul>';	  		
			}
			$output.= '</li>';	
		}	
		return $output;
	  }
 
	/*********************************
	* this function is used in admin for active and deactive link.
	*/
	function changeStatus($options = null){
		App::import('AppHelper', 'View/Helper');
		$html = new HtmlHelper();
		if($options['status'] == 1){
			$status_icon = SITE_URL.'img/deactive.png';
			$title = 'Active';
		}elseif($options['status'] == 0){
			$status_icon = SITE_URL.'img/active.png';
			$title = 'Deactive';
		}		
		$actions = "&nbsp;&nbsp;<span id='statuscoloumn_".$options['id']."'>".$html->link($html->image($status_icon,array('width'=>'15','height'=>'14')),'javascript:void(0);', array('title' => $title,'alt'=> $title,"onclick"=>"changeStatus('".$options['id']."','".$options['status']."','".$options['controller']."');",'escape'=>false), false, false)."</span>";
		return $actions;
	}
	/* options for total rooms */
	function totalRooms(){
		$totalRooms = array(''=>'Please Select Total Rooms');
		for($i=1;$i<=10;$i++){
			$totalRooms[$i] = $i;
		}
		return $totalRooms;
	}
	/* options for total bathrooms */
	function totalBathrooms(){
		$totalBathrooms = array(''=>'Please Select Total Bath-Rooms');
		for($i=1;$i<=10;$i++){
			$totalBathrooms[$i] = $i;
		}
		return $totalBathrooms;
	}	
	/* ==========get profile image if found otherwise first image will be return============ */
	function getProfileImage($userImage = null){
		$profileImage = array();
		if($userImage){
			foreach($userImage as $key=>$imageArr){
				$profileImage['image_name'] = $imageArr['image_name'];
				$profileImage['hash'] = $imageArr['hash'];
				if($imageArr['profile_default'] == 1){
					$profileImage['image_name'] = $imageArr['image_name'];
					break;
				}
			}
		}
		return $profileImage;
	}
	function getcity($zipcode){
		
		$obj = new City();
		$data=$obj->find('first',array('conditions'=>array('City.zip_code'=>$zipcode)));
		echo $data['City']['city_name'].',&nbsp;'.$data['City']['state'];
		
	}
	function checkSubscribe()
	{
		$userEmail = $this->Session->read('Auth.User.username');
		$news=ClassRegistry::init('NewsLetter') ;
		$data = $news->find('first',array('conditions'=>array('NewsLetter.email'=>$userEmail)));
		return $data;
	}
	
	/*function MenteeLog()
	{
		$userId = $this->Session->read('Auth.User.id');
		ClassRegistry::init('Mentorship')->bindModel(
				array(
				'belongsTo' => array(
					'Mentee' => array(
						'className' => 'User',
						'foreignKey' => 'mentee_id',
				 	),
					'Mentor' => array(
						'className' => 'User',
						'foreignKey' => 'mentor_id',
				 	),
					'UserReference' => array(
						'className' => 'UserReference',
						'foreignKey' => false,
						'conditions' =>array('Mentorship.mentor_id=UserReference.user_id'),
				 	),
				 	'Invoice' => array(
                        'className' => 'Invoice',
                        'foreignKey' => false,
                        'conditions' =>array('Mentorship.id=Invoice.mentorship_id'),
                    )
					)));
			
			$ApplyData = ClassRegistry::init('Mentorship')->find('all',array(
							'conditions'=>array('Mentorship.mentee_id'=>$userId,'Mentorship.is_deleted!=1','applicationType!=6'),
							'fields'=>array('UserReference.first_name','UserReference.last_name','Mentor.url_key','Mentee.url_key','Mentorship.*','Invoice.id','Invoice.inv_create_status'),
							'order'=>'Mentorship.id desc',
							'group'=>'Mentorship.mentor_id',
							));
			return $ApplyData;
	}*/
	
	/*function MentorLog1()
	{
		$userId = $this->Session->read('Auth.User.id');
		ClassRegistry::init('Mentorship')->bindModel(
				array(
				'belongsTo' => array(
					'Mentee' => array(
						'className' => 'User',
						'foreignKey' => 'mentee_id',
				 	),
					'Mentor' => array(
						'className' => 'User',
						'foreignKey' => 'mentor_id',
				 	),
					'UserReference' => array(
						'className' => 'UserReference',
						'foreignKey' => false,
						'conditions' =>array('Mentorship.mentee_id=UserReference.user_id'),
				 	),
				 	'Invoice' => array(
						'className' => 'Invoice',
						'foreignKey' => false,
						'conditions' =>array('Mentorship.id=Invoice.mentorship_id'),
				 	)
					)));
			
			$ApplyData = ClassRegistry::init('Mentorship')->find('all',array(
							'conditions'=>array('Mentorship.mentor_id'=>$userId,'Mentorship.is_deleted!=1','applicationType!=6'),
							'fields'=>array('UserReference.first_name','UserReference.last_name','Mentor.url_key','Mentee.url_key','Mentorship.*','Invoice.id','Invoice.inv_create_status'),
							'order'=>'Mentorship.modified desc',
							'group'=>'Mentorship.mentee_id',
							));
			return $ApplyData;
	}*/
	
	function checkMentorApply($id=null)
	{
		$userId = $this->Session->read('Auth.User.id');
		ClassRegistry::init('Mentorship')->recursive=0;
		$ApplyData = ClassRegistry::init('Mentorship')->find('first',array('conditions'=>array('Mentorship.mentee_id'=>$userId,'Mentorship.mentor_id'=>$id,'Mentorship.is_deleted!=1')));
        //echo "<pre>"; print_r($ApplyData);
		return $ApplyData;
		
	}
	
	function checkMentorRequest($id=null)
	{
		$userId = $this->Session->read('Auth.User.id');
		$ApplyData = ClassRegistry::init('TempRequest')->find('first',array('conditions'=>array('TempRequest.mentee_id'=>$userId,'TempRequest.mentor_id'=>$id)));
       // echo "<pre>"; print_r($ApplyData);
		return $ApplyData;
		
	}
	
	function getUserInfo($id=null)
	{
		if($id=='')
			$userId = $this->Session->read('Auth.User.id');
		else
			$userId= $id;	
		ClassRegistry::init('UserReference')->bindModel(
				array(
				'belongsTo' => array(
					'UserImage' => array(
						'className' => 'UserImage',
						'foreignKey' => false,
						'conditions' =>array('UserReference.user_id=UserImage.user_id'),
				 	))));
		$fields = array('UserReference.first_name','UserReference.last_name','UserReference.zipcode','UserImage.image_name','UserReference.fee_regular_session','UserReference.fee_first_hour');
		$userData = ClassRegistry::init('UserReference')->find('first',array('conditions'=>array('UserReference.user_id'=>$userId),'fields'=>$fields));
		$dataZip=mysql_fetch_array(mysql_query("SELECT * FROM cities WHERE zip_code IN (".$str = ltrim($userData['UserReference']['zipcode'], '0').")"));
		$userData['location']['city_name'] = $dataZip['city_name'];
		$userData['location']['state'] = $dataZip['state'];
		return $userData;
	}
	
	/* funciton for generate unique invoice no data */
	function generateInvoiceData($id=null)
	{
		ClassRegistry::init('Mentorship')->bindModel(
				array(
				'belongsTo' => array(
					'Mentor' => array(
								'className' => 'UserReference',
								'foreignKey' => false,
								'conditions' =>array('Mentor.user_id=Mentorship.mentor_id'),
								'fields'=>array('Mentor.first_name','Mentor.last_name')
								),
					'Mentee' => array(
								'className' => 'UserReference',
								'foreignKey' => false,
								'conditions' =>array('Mentee.user_id=Mentorship.mentee_id'),
								'fields'=>array('Mentee.first_name','Mentee.last_name')
								),
					)));
		$mentorshipData = ClassRegistry::init('Mentorship')->findById($id);
	//	$invNo = 'Invoice-'.$mentorshipData['Mentorship']['mentor_id'].'-'.$mentorshipData['Mentorship']['mentee_id'].'-'.$id;
		
		return $mentorshipData;
	}
	
	function checkInitialRegular($id=null)
	{
		$mentorId = $this->Session->read('Auth.User.id');
		$mentershipData = ClassRegistry::init('Mentorship')->find('first',array('conditions'=>array('Mentorship.mentor_id'=>$mentorId,'Mentorship.mentee_id'=>$id,'Mentorship.applicationType'=>'5','Mentorship.is_deleted=0')));
		return $mentershipData;
	}

    function MentorLog()
    {
        $userId = $this->Session->read('Auth.User.id');
      /*  $mentorData = ClassRegistry::init('Mentorship')->query('
                (SELECT A.*,invoices.id as inv_id,invoices.inv_create_status as inv_status,invoices.title as inv_title,invoices.inv_date as inv_date,invoices.inv_time as inv_time  
                FROM `mentorships` AS A left join invoices on (A.id = invoices.mentorship_id)
                WHERE NOT
                EXISTS (SELECT * FROM `mentorships` AS B WHERE A.id = B.parent_id ) AND A.parent_id =0 AND A.applicationType!=6 AND A.is_deleted!=1 AND A.mentor_id = '.$userId.'
                )
                UNION ( SELECT  B.*,invoices.id as inv_id,invoices.inv_create_status as inv_status,invoices.title as inv_title,invoices.inv_date as inv_date,invoices.inv_time as inv_time  
                FROM `mentorships` AS B left join invoices on (B.id = invoices.mentorship_id)
                WHERE
                EXISTS ( SELECT * FROM `mentorships` AS A WHERE A.id = B.parent_id ) AND B.id = (SELECT max( C.id ) FROM `mentorships` AS C, `mentorships` AS D WHERE D.id = C.parent_id ) AND  B.applicationType!=6 AND B.is_deleted!=1 AND B.mentor_id = '.$userId.') order by modified desc ');*/
      
      $mentorData =  ClassRegistry::init('Mentorship')->query('(
                                SELECT A.*,invoices.id as inv_id,invoices.inv_create_status as inv_status,invoices.title as inv_title,invoices.inv_date as inv_date,invoices.inv_time as inv_time, invoices.inv_timezone as inv_timezone
                                FROM mentorships as A left join invoices on (A.id = invoices.mentorship_id)
                                WHERE NOT EXISTS(SELECT * FROM mentorships as B WHERE B.parent_id = A.id) AND A.parent_id =0 AND A.applicationType!=6 AND A.is_deleted!=1 AND (A.mentor_id = '.$userId.' OR A.mentee_id = '.$userId.')
                                )
                                UNION
                                (
                                SELECT B.*,invoices.id as inv_id,invoices.inv_create_status as inv_status,invoices.title as inv_title,invoices.inv_date as inv_date,invoices.inv_time as inv_time, invoices.inv_timezone as inv_timezone 
                                FROM mentorships AS B left join invoices on (B.id = invoices.mentorship_id)
                                
                                WHERE B.id IN(
                                SELECT max(id)
                                FROM mentorships as A
                                WHERE EXISTS(SELECT * FROM mentorships as B WHERE A.parent_id = B.id) GROUP BY A.parent_id) AND B.applicationType!=6 AND B.is_deleted!=1 AND (B.mentor_id = '.$userId.' OR B.mentee_id = '.$userId.')
                                )  order by modified desc ');
                              
      
       return $mentorData; 
   }

    function MenteeLog()
        {
            $userId = $this->Session->read('Auth.User.id');
            /*$menteeData = ClassRegistry::init('Mentorship')->query('
                    (SELECT A.*,invoices.id as inv_id,invoices.inv_create_status as inv_status,invoices.title as inv_title,invoices.inv_date as inv_date,invoices.inv_time as inv_time 
                    FROM `mentorships` AS A left join invoices on (A.id = invoices.mentorship_id)
                    WHERE NOT
                    EXISTS (SELECT * FROM `mentorships` AS B WHERE A.id = B.parent_id ) AND A.parent_id =0 AND A.applicationType!=6 AND A.is_deleted!=1 AND A.mentee_id = '.$userId.'
                    )
                    UNION ( SELECT  B.*,invoices.id as inv_id,invoices.inv_create_status as inv_status,invoices.title as inv_title,invoices.inv_date as inv_date,invoices.inv_time as inv_time 
                    FROM `mentorships` AS B left join invoices on (B.id = invoices.mentorship_id)
                    WHERE
                    EXISTS ( SELECT * FROM `mentorships` AS A WHERE A.id = B.parent_id ) AND B.id = (SELECT max( C.id ) FROM `mentorships` AS C, `mentorships` AS D WHERE D.id = C.parent_id ) AND  B.applicationType!=6 AND B.is_deleted!=1 AND B.mentee_id = '.$userId.') order by modified desc ');*/
           
           $menteeData =  ClassRegistry::init('Mentorship')->query('(
                                SELECT A.*,invoices.id as inv_id,invoices.inv_create_status as inv_status,invoices.title as inv_title,invoices.inv_date as inv_date,invoices.inv_time as inv_time, invoices.inv_timezone as inv_timezone
                                FROM mentorships as A left join invoices on (A.id = invoices.mentorship_id)
                                WHERE NOT EXISTS(SELECT * FROM mentorships as B WHERE B.parent_id = A.id) AND A.parent_id =0 AND A.applicationType!=6 AND A.is_deleted!=1 AND A.mentee_id = '.$userId.'
                                )
                                UNION
                                (
                                SELECT B.*,invoices.id as inv_id,invoices.inv_create_status as inv_status,invoices.title as inv_title,invoices.inv_date as inv_date,invoices.inv_time as inv_time, invoices.inv_timezone as inv_timezone 
                                FROM mentorships AS B left join invoices on (B.id = invoices.mentorship_id)
                                
                                WHERE B.id IN(
                                SELECT max(id)
                                FROM mentorships as A
                                WHERE EXISTS(SELECT * FROM mentorships as B WHERE A.parent_id = B.id) GROUP BY A.parent_id) AND B.applicationType!=6 AND B.is_deleted!=1 AND B.mentee_id = '.$userId.'
                                ) order by modified desc  ');
           
           
           return $menteeData; 
       }
       
    function GetUnreadLogs($mentorshipId=null){
    	
    	$mentorship = mysql_fetch_array(mysql_query("SELECT *  FROM mentorships where id ='".$mentorshipId."'"));
    	
    	$mentee_id = $mentorship['mentee_id'];
    	$mentor_id = $mentorship['mentor_id'];
    	
    	$roleId = $this->Session->read('Auth.User.role_id');
    	
    	$unreadMessagesCount = mysql_fetch_array(mysql_query("SELECT count(*) FROM messages where mentor_id = '".$mentor_id. "' and
    																							  mentee_id = '".$mentee_id. "' and
    																						      sent_by != '".$roleId. "' and
    																							  isRead = 0"));
    	
    	return $unreadMessagesCount;
    	
    }

    function getUserReferenceData($id=null)
    {
        $ApplyData = ClassRegistry::init('UserReference')->find('first',array('conditions'=>array('UserReference.user_id'=>$id),'fields'=>array('UserReference.first_name','UserReference.last_name')));
        $urlData = ClassRegistry::init('User')->find('first',array('conditions'=>array('User.id'=>$id),'fields'=>array('User.url_key', 'User.role_id')));
        $ApplyData['UserReference']['url_key'] = $urlData['User']['url_key'];
        $ApplyData['User']['role_id'] = $urlData['User']['role_id'];
        return $ApplyData;
    }
    
    /* function for checking mentee user first time or second time*/
    function getMenteeStatusInitalRegular($id=null)
    {
        $userId = $this->Session->read('Auth.User.id');
        $mentorshipCount = ClassRegistry::init('Mentorship')->find('count',array('conditions'=>array('Mentorship.mentor_id'=>$id,'Mentorship.mentee_id'=>$userId,'is_deleted!=1','applicationType'=>'5')));
        return $mentorshipCount;
    }

    /* function for checking feedbacks */
    function checkFeedback($id=null,$type=null)
    {
        $userId = $this->Session->read('Auth.User.id');
        if($userId!=$id)
        {
            $type = '1';            
        }
        else {
	           $type = array(0,1);
        }
       ClassRegistry::init('Feedback');
       $urlData = ClassRegistry::init('Feedback')->find('count',array('conditions'=>array('Feedback.member_id'=>$id,'Feedback.type'=>$type)));
       
       /*if($urlData == 0) {
       	
       	$feedbackLink = ClassRegistry::init('UserReference')->find('first',array('conditions'=>array('UserReference.user_id'=>$id),'fields'=>array('UserReference.feedback_link')));
       	
       	if($feedbackLink['UserReference']['feedback_link'] != '') {
       		$urlData = 1;
       	}
       }*/
       return $urlData; 
    }
    
    function getMentorStatic($mentor_id=null)
    {
      //  $menteeCount = ClassRegistry::init('Mentorship')->query('count',array('conditions'=>array('Mentorhip.mentor_id'=>$mentor_id,'Mentorship.is_deleted!=1'),'fields'=>array('count(distinct(Mentorship.mentee_id))')));
        $menteeCount = ClassRegistry::init('Mentorship')->query('SELECT count(distinct(`Mentorship`.`mentee_id`)) as countMentee 
                    FROM `mentorships` AS `Mentorship` 
                    WHERE `Mentorship`.`mentor_id` ='.$mentor_id.' AND `Mentorship`.`applicationType`=5 AND Mentorship.mentee_need="Schedule next new session"');
        //$menteeCount = $mentee[0][0]['countMentee'];
        $SessionCount = ClassRegistry::init('Mentorship')->find('count',array('conditions'=>array('Mentorship.mentor_id'=>$mentor_id,'Mentorship.parent_id!=0','Mentorship.applicationType'=>'5')));
        
        $data['menteeCount'] = $menteeCount[0][0]['countMentee'];
        
        if($SessionCount==0 || empty($SessionCount))
            $data['sessionCount'] = 0;
        else
            $data['sessionCount'] = $SessionCount;
        return $data;    
    }
    
    function getCountry($zipCode=null)
    {
        if(trim($zipCode)!='')
        {
            $countryname = ClassRegistry::init('City')->find('first',array('conditions'=>array('City.zip_code'=>$zipCode),'fields'=>array('City.city_name','City.state')));
            if(empty($countryname))
                return $zipCode;
            else
                return $countryname['City']['city_name'].", ".$countryname['City']['state'];
       }
        else
             return 'None';
    }
    
    function FindUserImage($id=null)
    {
        $userImageData= ClassRegistry::init('UserImage')->find('first',array('conditions'=>array('UserImage.user_id'=>$id),'fields'=>array('UserImage.image_name')));
        if(!empty($userImageData))
           return $userImageData['UserImage']['image_name'];
        else
            return '';
    }
    
    function FindSocialLink($id=null)
    {
        if(!isset($id)){ $id =0; }
        $userLinks= ClassRegistry::init('Social')->find('all',array('conditions'=>array('Social.user_id'=>$id),'fields'=>array('Social.social_name')));
        if(!empty($userLinks) && count($userLinks)>0)
           return $userLinks;
        else
           return '';   
    }

    function getMentorStaticAdmin($mentor_id=null,$start=null,$end=null)
    {
        $totAmount = 0;
        $totCommission = 0;
        $st_date = explode("/",$start);
        $ed_date = explode("/",$end);
        $searchStartDate = $st_date[2].'-'.$st_date[0].'-'.$st_date[1];
        $searchEndDate = $ed_date[2].'-'.$ed_date[0].'-'.$ed_date[1];
        
        $searchCondition[] = array('Mentorship.modified >= '=>$searchStartDate.' 00:00:00'); 
        $searchCondition[] = array('Mentorship.modified <= '=>$searchEndDate.' 23:59:59');
        $searchCondition['Mentorship.mentor_id'] =  $mentor_id;
        
        $mentee = ClassRegistry::init('Mentorship')->query('SELECT count(distinct(`Mentorship`.`mentee_id`)) as countMentee 
                    FROM `mentorships` AS `Mentorship` 
                    WHERE `Mentorship`.`mentor_id` ='.$mentor_id.' AND modified>=\''.$searchStartDate.'00:00:00'.'\'  AND modified<=\''.$searchEndDate.'23:59:59'.'\' AND `Mentorship`.`applicationType`=5 AND Mentorship.mentee_need="Schedule next new session"');
        $menteeCount = $mentee[0][0]['countMentee'];
        $SessionCount = ClassRegistry::init('Mentorship')->find('count',array('conditions'=>array($searchCondition,'Mentorship.parent_id!=0','Mentorship.applicationType'=>'5')));
        $ids = ClassRegistry::init('Mentorship')->find('all',array('conditions'=>$searchCondition,'fields'=>array('Mentorship.id'),'recursive'=>'0'));
        $data['menteeCount'] = $menteeCount;
        if($SessionCount==0 || empty($SessionCount))
            $data['sessionCount'] = 0;
        else
            $data['sessionCount'] = $SessionCount;
        if(!empty($ids))
         { 
            foreach($ids as $mentId)
                    {
                        $mentIds[] = $mentId['Mentorship']['id']; 
                    }
                    $condition['Invoice.mentorship_id'] =  $mentIds;
                    
                    $totalEarning = ClassRegistry::init('Invoice')->find('first',array('conditions'=>array('Invoice.mentorship_id'=>$mentIds),'fields'=>array('sum(((Invoice.total) - ((Invoice.total * Invoice.rate)/100))) AS Amount,sum((Invoice.total * Invoice.rate)/100) as commission ')));
                    if($totalEarning[0]['Amount']!='')
                        $totAmount= $totalEarning[0]['Amount'];
                    if($totalEarning[0]['commission']!='')
                        $totCommission= round($totalEarning[0]['commission'],2);
          }
         $data['amount'] = $totAmount;
         $data['commission'] = $totCommission;
         
        return $data;    
    }

    function timeformatchange1($data, $duration_hours, $duration_minutes)
    {
        $timedata = explode(':',$data);
        if($timedata[0]=='12' || $timedata[0]=='11')
        {
            if($timedata[2]=='AM')
            {
                $finaltimeFirst = str_pad($timedata[0],2,0,STR_PAD_LEFT).":".str_pad($timedata[1],2,0,STR_PAD_LEFT)." ".$timedata[2];
                if($timedata[0]=='11')
                {
                    $finaltimeSecond = "12:".str_pad($timedata[1],2,0,STR_PAD_LEFT);
                    $sendTime = $finaltimeFirst." - ".$finaltimeSecond." PM";
                }
                else 
                {
            	    $finaltimeSecond = "01:".str_pad($timedata[1],2,0,STR_PAD_LEFT);
                    $sendTime = $finaltimeFirst." - ".$finaltimeSecond." AM";
                  }
            }
            else 
            {
            	$finaltimeFirst = str_pad($timedata[0],2,0,STR_PAD_LEFT).":".str_pad($timedata[1],2,0,STR_PAD_LEFT)." ".$timedata[2];
                if($timedata[0]=='11')
                {
                    $finaltimeSecond = "12:".str_pad($timedata[1],2,0,STR_PAD_LEFT);
                    $sendTime = $finaltimeFirst." - ".$finaltimeSecond." AM";
                }
                else 
                {
                    $finaltimeSecond = "01:".str_pad($timedata[1],2,0,STR_PAD_LEFT);
                    $sendTime = $finaltimeFirst." - ".$finaltimeSecond." PM";
                  }
            }    
        }
        else 
        {
	            $finaltimeFirst = str_pad($timedata[0],2,0,STR_PAD_LEFT).":".str_pad($timedata[1],2,0,STR_PAD_LEFT)." ".$timedata[2];
                $finaltimeSecond = str_pad(($timedata[0]+1),2,0,STR_PAD_LEFT).":".str_pad($timedata[1],2,0,STR_PAD_LEFT)." ".$timedata[2];
                $sendTime = $finaltimeFirst." - ".$finaltimeSecond;
        }
        return $sendTime;
    }
    
    function timeformatchange($data, $duration_hours, $duration_minutes)
    {
    	$timedata = explode(':',$data);
    	
    	$hours = $timedata[0] + $duration_hours;
    	$minutes = $timedata[1] + $duration_minutes;
    	if($minutes > 59){
    		$minutes = $minutes%60;
    		$hours++;
    	}
    	if($hours > 12) {
    		$hours = $hours%12;
    	}
    	
    	$finaltimeFirst = str_pad($timedata[0],2,0,STR_PAD_LEFT).":".str_pad($timedata[1],2,0,STR_PAD_LEFT)." ".$timedata[2];
    	$finaltimeSecond = str_pad($hours,2,0,STR_PAD_LEFT).":".str_pad($minutes,2,0,STR_PAD_LEFT);
    	
    	if(($hours < $timedata[0]) || ($hours == 12 && $timedata[0] < 12)) {  //Change from AM to PM and vice versa
    		
    		if($timedata[2]=='AM') { //Second time will be PM
    			
    			$sendTime = $finaltimeFirst." - ".$finaltimeSecond." PM";
    		} else {//Second time will be AM
    			
    			$sendTime = $finaltimeFirst." - ".$finaltimeSecond." AM";
    		}
    	} else {
    		$sendTime = $finaltimeFirst." - ".$finaltimeSecond.$timedata[2];
    	}
    	return $sendTime;
    }
    
    function fetchProgramId($mentorId=null,$mentee_id=null)
    {
        $id = ClassRegistry::init('Mentorship')->find('first',array('conditions'=>array('Mentorship.mentor_id'=>$mentorId,'Mentorship.mentee_id'=>$mentee_id),'fields'=>array('Mentorship.id'),'order'=>array('Mentorship.id desc'),'recursive'=>'0'));
        $prgId = str_pad($id['Mentorship']['id'],4,0,STR_PAD_LEFT);
        return 'P'.$prgId;
    }
    
    function invoiceTotal($start=null,$end=null,$mentor=null,$mentee=null)
    {
        if($start!=0 && $end!=0)
        {
            $mentorData = ClassRegistry::init('Invoice')->query('SELECT sum(invoices.total) as inv_total
                                FROM mentorships as A left join invoices on (A.id = invoices.mentorship_id) where A.mentor_id="'.$mentor.'" AND A.mentee_id="'.$mentee.'" AND A.is_deleted!=1 AND (A.modified>='.$start.' AND A.modified<='.$end.') group by A.mentor_id');

        }
        else 
        {
              $mentorData = ClassRegistry::init('Invoice')->query('SELECT sum(invoices.total) as inv_total
                                FROM mentorships as A left join invoices on (A.id = invoices.mentorship_id) where A.mentor_id="'.$mentor.'" AND A.mentee_id="'.$mentee.'" AND A.is_deleted!=1 group by A.mentor_id');
            
        }   
        if(!empty($mentorData))
            return $mentorData[0][0]['inv_total'];
        else
            return 0;
    }

	function getlabel($value){
		if($value == '1'){
			return "/ Hour";
		}else if($value == '2'){
			return "/ Month";
		}else if($value == '3'){
			return "/ Engagement";
		}else if($value == '4'){
			return "/ Day";
		}
	}
    
    function gettimezone($userid)
    {
       $timezone = ClassRegistry::init('User')->findById($userid);
       return $timezone['User']['timezone'];  
    }

    function getTimeZoneById($id)
    {
       $timezone = ClassRegistry::init('Timezone')->findById($id);
       if(empty($timezone))
           return 0;
       else
            return $timezone['Timezone']['name'];   
    }
    
    function addhttp($url) 
    {
        if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
            $url = "http://" . $url;
        }
        return $url;
    }
    
    function getMentorshipSessionsWithClient($clientId = null){
    	
    	$mentor_id = $this->Session->read('Auth.User.id');
    	
    	$mentorship = ClassRegistry::init('Mentorship')->find('all',array('conditions'=>array('Mentorship.mentee_id'=>$clientId,'Mentorship.mentor_id'=>$mentor_id), 'order'=>'Mentorship.id asc'));
    	
    	return $mentorship[0];
    	
    }
    
    function generateSessionName($mentor_id=null,$mentee_id=null,$mentorshipId=null)
    {
    	$mentshipCount = ClassRegistry::init('Mentorship')->find('count',array('conditions'=>array('Mentorship.mentor_id'=>$mentor_id,'Mentorship.mentee_id'=>$mentee_id,'Mentorship.is_deleted!=1')));
    	
    	{
    		$sessname = "Session ".($mentshipCount);
    	}
    	return $sessname ;
    }
    
    function getUnpaidInvoices(){
    	
    	$clientId = $this->Session->read('Auth.User.id');
    	
    	$unpaidInvoiceCount = ClassRegistry::init('Invoice')->find('count',array('conditions'=>array('Invoice.inv_client_id'=>$clientId,'Invoice.inv_create_status'=>'0')));
    	
    	return $unpaidInvoiceCount;
    }
    
    function getIcon($url = null){
    	
    	if(($pos =strpos($url,'www.linkedin.com/influencer'))!==false)
    	{
    		return 'media/LinkedIn_Today_logo.png';
    	}
    	else if(($pos =strpos($url,'linkedin'))!==false)
    	{
    		return 'media/linkedin.png';
    	}
    	else if(($pos =strpos($url,'twitter'))!==false)
    	{
    		return 'media/twitter.png';
    	}
    	else if(($pos =strpos($url,'facebook'))!==false)
    	{
    		return 'media/facebook.png';
    	}
    	else if(($pos =strpos($url,'google'))!==false)
    	{
    		return 'media/Google+.png';
    	}
    	else if(($pos =strpos($url,'blogspot'))!==false)
    	{
    		return 'media/blogger.png';
    	}
    	else if(($pos =strpos($url,'wordpress'))!==false)
    	{
    		return 'media/wordpress.png';
    	}
    	else if(($pos =strpos($url,'youtube'))!==false)
    	{
    		return 'media/youtube.png';
    	}
    	else if(($pos =strpos($url,'yahoo'))!==false)
    	{
    		return 'media/yahoo.png';
    	}
    	else if(($pos =strpos($url,'delicious'))!==false)
    	{
    		return 'media/delicious.png';
    	}
    	else if(($pos =strpos($url,'digg'))!==false)
    	{
    		return 'media/digg.png';
    	}
    	else if(($pos =strpos($url,'orkut'))!==false)
    	{
    		return 'media/orkut.png';
    	}
    	else if(($pos =strpos($url,'www.ted.com'))!==false)
    	{
    		return 'media/TEDa.jpg';
    	}
    	else if(($pos =strpos($url,'forbes'))!==false)
    	{
    		return 'media/Forbes_Icon.png';
    	}
    	else if(($pos =strpos($url,'hbr'))!==false)
    	{
    		return 'media/HBR.png';
    	}
    	else if(($pos =strpos($url,'about.me'))!==false)
    	{
    		return 'media/aboutdotme.png';
    	}
    	else if(($pos =strpos($url,'psychologytoday.com'))!==false)
    	{
    		return 'media/psychologytoday.jpg';
    	}
    	else if(($pos =strpos($url,'vimeo'))!==false)
    	{
    		return 'media/youtube.png';
    	}
    	else if(($pos =strpos($url,'fastcompany.com'))!==false)
    	{
    		return 'media/fast_company.gif';
    	}
    	else if(($pos =strpos($url,'www.inc.com'))!==false)
    	{
    		return 'media/inc.jpg';
    	}
    	else if(($pos =strpos($url,'soundcloud'))!==false)
    	{
    		return 'media/SoundCloud.png';
    	}
    	else if(($pos =strpos($url,'amazon.com'))!==false)
    	{
    		return 'media/amazon.png';
    	}
       else if(($pos =strpos($url,'amazon.in'))!==false)
    	{
    		return 'media/amazon.png';
    	}
        else if(($pos =strpos($url,'amzn.com'))!==false)
    	{
    		return 'media/amazon.png';
    	}
    	else if(($pos =strpos($url,'entrepreneur.com'))!==false)
    	{
    		return 'media/twitter_entrepreneur.png';
    	}
    	else if(($pos =strpos($url,'softwaremag.com'))!==false)
    	{
    		return 'media/sw.jpg';
    	}
    	else if(($pos =strpos($url,'time.com'))!==false)
    	{
    		return 'media/twitter_time.png';
    	}
    	else if(($pos =strpos($url,'cbsnews.com'))!==false)
    	{
    		return 'media/CBS.jpg';
    	}
    	else if(($pos =strpos($url,'corporatecomplianceinsights.com'))!==false)
    	{
    		return 'media/cci.jpg';
    	}
    	else if(($pos =strpos($url,'slideshare.net'))!==false)
    	{
    		return 'media/slideshare_logo.jpg';
    	}
    	else if(($pos =strpos($url,'ceo.com'))!==false)
    	{
    		return 'media/CEOdotCOM_logo.gif';
    	}
    	else if(($pos =strpos($url,'huffingtonpost'))!==false)
    	{
    		return 'media/icon_huffington.jpg';
    	}
    	else if(($pos =strpos($url,'medium.com'))!==false)
    	{
    		return 'media/icon_medium.jpg';
    	}
    	else if(($pos =strpos($url,'ere.net'))!==false)
    	{
    		return 'media/icon_ere.png';
    	}
    	else if(($pos =strpos($url,'aom.org'))!==false)
    	{
    		return 'media/icon_aom.jpg';
    	} 
    	else if(($pos =strpos($url,'businessbecause.com'))!==false)
    	{
    		return 'media/icon_BusinessBecause.png';
    	}
    	else if(($pos =strpos($url,'examiner.com'))!==false)
    	{
    		return 'media/icon_examiner.jpg';
    	}
    	else if(($pos =strpos($url,'50topcoaches.com'))!==false)
    	{
    		return 'media/icon_50.png';
    	}
    	else if(($pos =strpos($url,'speakers.com'))!==false)
    	{
    		return 'media/icon_speakers.png';
    	}
    	else if(($pos =strpos($url,'leadingauthorities.com'))!==false)
    	{
    		return 'media/LeadingAuthorities_icon.jpg';
    	}
    	else if(($pos =strpos($url,'udemy.com'))!==false)
    	{
    		return 'media/icon_udemy.png';
    	}
    	else if(($pos =strpos($url,'nxtbook.com'))!==false)
    	{
    		return 'media/icon_managementtoday.jpg';
    	}
    	else if(($pos =strpos($url,'astd.org'))!==false)
    	{
    		return 'media/logo-small.jpg';
    	}
    	else if(($pos =strpos($url,'orgsurvival.com'))!==false)
    	{
    		return 'media/OSP_icon.jpg';
    	}
    	else if(($pos =strpos($url,'ftpress'))!==false)
    	{
    		return 'media/FTPress_Twitter_logo.jpg';
    	}
    	else if(($pos =strpos($url,'businessweek.com'))!==false)
    	{
    		return 'media/Businessweek_icon.jpg';
    	}
    	else if(($pos =strpos($url,'executivetravelmagazine.com'))!==false)
    	{
    		return 'media/ET_Facebook_Profile_image.png';
    	}
    	else if(($pos =strpos($url,'cfo.com'))!==false)
    	{
    		return 'media/icon_cfo.png';
    	}
    	else if(($pos =strpos($url,'scribd.com'))!==false)
    	{
    		return 'media/icon_scribd.jpg';
    	}
       else if(($pos =strpos($url,'strategy-business.com'))!==false)
    	{
    		return 'media/strategy-business.png';
    	}
       else if(($pos =strpos($url,'clomedia.com'))!==false)
    	{
    		return 'media/clomedia.jpg';
    	}
       else if(($pos =strpos($url,'paper.li'))!==false)
    	{
    		return 'media/paper_icon.jpg';
    	}
    	else
    	{
    		return 'media/share-icon-1.png';
    	}
    }
    
    function get_tiny_url($url)  {
    	$ch = curl_init();
    	$timeout = 5;
    	curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url);
    	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
    	curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
    	$data = curl_exec($ch);
    	curl_close($ch);
    	return $data;
    }
    
    function make_links($text)
    {
    	return  preg_replace(
    			array(
    					'/(?(?=<a[^>]*>.+<\/a>)
             (?:<a[^>]*>.+<\/a>)
             |
             ([^="\']?)((?:https?|ftp|bf2|):\/\/[^<> \n\r]+)
         )/iex',
    					'/<a([^>]*)target="?[^"\']+"?/i',
    					'/<a([^>]+)>/i',
    					'/(^|\s)(www.[^<> \n\r]+)/iex',
    					'/(([_A-Za-z0-9-]+)(\\.[_A-Za-z0-9-]+)*@([A-Za-z0-9-]+)
       (\\.[A-Za-z0-9-]+)*)/iex'
    			),
    			array(
    					"stripslashes((strlen('\\2')>0?'\\1<a href=\"\\2\">\\2</a>\\3':'\\0'))",
    					'<a\\1',
    					'<a\\1 target="_blank">',
    					"stripslashes((strlen('\\2')>0?'\\1<a href=\"http://\\2\">\\2</a>\\3':'\\0'))",
    					"stripslashes((strlen('\\2')>0?'<a href=\"mailto:\\0\">\\0</a>':'\\0'))"
    			),
    			trim($text)
    	);
    }
    

}
?>