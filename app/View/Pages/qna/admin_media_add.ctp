<?php e($javascript->link(array('autocomplete/jquery.coolautosuggest')));?>
<?php e($html->css(array('autocomplete/jquery.coolautosuggest')));?>
<div class="adminrightinner">
	<?php e($form->create(Null, array('url' => array('controller' => 'qna', 'action' => 'media_add'),'type'=>'file')));?>     
	<div class="tablewapper2 AdminForm">
		<h3 class="legend1">Add Media Query</h3>
		<table border="0" class="Admin2Table" width="100%">
                      <?php $currentTime = date('Y-m-d 23:59:59');?>
			<tr>
				<td valign="middle" class="Padleft26">Deadline</td>
				<td><?php e($form->input('MediaRecords.deadline', array('div'=>false, 'label'=>false, "class" => "Testbox5",'value'=>$currentTime)));?></td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Reporter name</td>
				<td><?php e($form->input('MediaRecords.reporter_name', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Reporter email</td>
				<td><?php e($form->input('MediaRecords.reporter_email', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Media outlet</td>
				<td><?php e($form->input('MediaRecords.media_outlet', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>
				</td>
			</tr>
                     <tr>
				<td valign="middle" class="Padleft26">Subject</td>
				<td><?php e($form->input('MediaRecords.subject', array('div'=>false, 'label'=>false, "class" => "Testbox", "style" =>"width: 800px;height: 22px;")));?>
				</td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Reporter query</td>
				<td><?php e($form->input('MediaRecords.reporter_query', array('div'=>false, 'label'=>false, "class" => "Testbox", "style" =>"width: 800px;height: 140px;")));?>
				</td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Requirement</td>
				<td><?php e($form->input('MediaRecords.requirement', array('div'=>false, 'label'=>false, "class" => "Testbox", "style" =>"width: 800px;height: 140px;")));?>
				</td>
			</tr>

                    <?php $options[-1] = '';
                       foreach ($cat as $value) {
                      
                      if($value['Qna_category']['id'] != 1 && $value['Qna_category']['id'] != 2)
                      $options[$value['Qna_category']['id']] = $value['Qna_category']['category'];
                      
                    }?>
			<tr>
				<td valign="middle" class="Padleft26">Category</td>
				<td><?php e($form->input('MediaRecords.category', array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"category1_id", "style"=>"width:160px; color: #000",'options'=>$options))); ?>
				&nbsp;&nbsp;Compulsory</td>
                            
			</tr>
			
			<tr>
				<td valign="middle" class="Padleft26">Category</td>
				<td><?php e($form->input('MediaRecords.category1', array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"category1_id", "style"=>"width:160px; color: #000",'options'=>$options))); ?>
				</td>
                            
			</tr>

                            <tr>
                                  <td valign="middle" class="Padleft26">Ask an expert</td>
	                        	<td>     <?php $option = array('Yes'=>'Yes','No'=>'No'); ?>
		                          <?php e($form->input('MediaRecords.checkbox', array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','options'=>$option, "style"=>"width:50px; color: #000")));?>
                                </td>
                            </tr>

		</table>
	</div>
	<div class="buttonwapper">
		<div><input type="submit" value="Submit" class="submit_button" /></div>
		<div class="cancel_button"><?php echo $html->link("Cancel", "/admin/qna/media_record/", array("title"=>"", "escape"=>false)); ?>
		</div>
	</div>
	<?php e($form->end()); ?>	
</div>
