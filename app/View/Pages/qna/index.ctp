<?php
$pageLimit = 3;
$data_flag = false;
$num = 0;
?>
	
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth" style="margin-top:60px; margin-bottom:30px;">
        <div id="" style="background:url('<?php e(SITE_URL)?>img/media/home-banner4A.jpg');border:0px;">
        <form id="mainSearchForm" method="post" action="<?php echo SITE_URL ?>qna/search"" accept-charset="utf-8">
            <div id="spotlight1" class="pagewidth1">
                <div id="find-mentor1">
                <h1>Get practical & actionable business advice.</h1>
                <div id="text-box">
                    <div id="enter-keywords">
                        <div id="enter-text1">
                            <input name="data[keyword]" type="text" id="CityKeyword" class="forminput-new" placeholder="Enter keywords e.g. career" value="" style="width:100%;" />
                        </div>
                    </div>
                    <div id="search">
                        <input class="searchSubmit" type="submit" value="Search" />
                    </div>       
                </div>
                </div>  
            </div>
        </form>
        </div>
    </div>
    <!-- middle part -->
    <div id="inner-content-box" class="pagewidth relatiVE">
      <div id="user-account">
          	<div class="account-form">
          		<div class="column first" style="width: 44%;">           
        			<div class="subheading">
          				<h2>Browse by category<sub style="font-size: 11px; font-weight: normal; font-family: Ubuntu">&nbsp;</sub></h2>
        			</div>
        			<div class="clear"></div>
       		 		<div>
            			<table class="RAtable" width="100%">
            				<tr>
            					<td><strong>Functions</strong></td>
            					<td><strong>Industries</strong></td>
            				</tr>
            			<?php 
            			$count = 0;
            			foreach ($fcategories as $fValue) {?>
            				<tr>
            					<td>
                                                       <?php { 
                                                             $str = $fValue['Qna_category']['category'];
                                                            if(false !== stripos($fValue['Qna_category']['category'],"&"))
                                                            $str = str_replace("&","_and",$fValue['Qna_category']['category']);
                                                            if(false !== stripos($fValue['Qna_category']['category'],"/"))
                                                            $str = str_replace("/","_slash",$fValue['Qna_category']['category']);
                                                            $str = str_replace(" ","-",$str);
                                                            e($html->link($fValue['Qna_category']['category'],SITE_URL.'qna/category/'.$str,array('escape'=>false)));}?>
		            					       
                                          </td>
            					<td>
            						<?php 
            							if(isset($dcategories[$count])){
                                                                       $link = $dcategories[$count]['Qna_category']['category'];
                                                                       if(false !== stripos($dcategories[$count]['Qna_category']['category'],"/")){
                                                                        $link  = str_replace("/","_slash",$dcategories[$count]['Qna_category']['category']);}
                                                                        $link = str_replace(" ","-",$link);
		            						e($html->link($dcategories[$count]['Qna_category']['category'],SITE_URL.'qna/category/'.$link,array('escape'=>false)));
		            						$count++;
            							}
            						?>
            					</td>
            				</tr>
            			<?php }?>
            			</table>
        			</div>
      			</div>

      			<?php  if($this->Session->read('Auth.User.role_id')!=Configure::read('App.Role.Mentor')) {?>
      			<div class="column last">
        			<div class="clear"></div>
        				<div class="subheading supreFREE">
          					<h2>Ask an expert <sub style="font-size: 11px; font-weight: normal; font-family: Ubuntu">Free & anonymous advice for business leaders</sub></h2>
        				</div>
        			<div class="clear"></div>

        			<form id="Addform" method="post" action="<?php echo SITE_URL ?>qna/question_preview">
        			<div class="styled-form">     
        				<span>Your question</span> <span style="float:right;">(<span id="noOfChar">128</span> characters remaining)</span>
        				<br/>
        				<div class="QHINTarea">
        					<textarea id="questionTextBox"  style="margin-top: 5px; height: 75px;" placeholder="Enter question e.g. How do I drive employee engagement?" name="data[Qna_question][question_text]"></textarea>
        				</div>    
        				<div>        				
	        				<br/>
	              			<input name="applyPreviewButton" type="button" class="mgButton" value="Ask now" id="applyPreviewButton" style="float: right"/> 
              			</div>
        			</div>
        			<div style="display: none" id="questionContextDiv">
	        			<div class="expertise">
							<span>Provide details</span> <span style="float:right;"></span>
							<div class="inputRELATIVE">
		                        <p>
		                        	<textarea id="questionContextBox" class="forminput" name="data[Qna_question][question_context]" style="width:438px; padding:10px; height:150px;" placeholder="To get a useful response provide all relevant background, but avoid any personally identifiable information." ></textarea> 
		                        </p>
	                    	</div> 
	                    </div>
	                    <div id="Apply" style="float: right;" role="Apply">
							<a class="reset" style="float: left; margin-top: 3px;" href="" id="cancelLink">Cancel</a>
							<input id="applyPreview" class="submitProfile" type="submit" value="Preview">
						</div>
					</div>
        			</form>   
      			</div>
      			<?php } else {?>
      			<div class="column last">
        			<div class="clear"></div>
        				<div class="subheading supreFREE">
          					<h2>Share your insight <sub style="font-size: 11px; font-weight: normal; font-family: Ubuntu">Advice for business leaders</sub></h2>
        				</div>
        			<div class="clear"></div>

        			<form id="AddformIn" method="post" action="<?php echo SITE_URL ?>insight/edit">
        			<div class="styled-form">     
        				<span>Title</span> <span style="float:right;">(<span id="noOfCharIn">128</span> characters remaining)</span>
        				<br/>
        				<div class="QHINTarea">
        					<textarea id="titleTextBox"  style="margin-top: 5px; height: 75px;" placeholder="Be creative and clear" name="data[Insight][title]"></textarea>
        				</div>    
        				<div>        				
	        				<br/>
	              			<input name="applyPreviewButton" type="button" class="mgButton" value="Post" id="applyPreviewButtonIn" style="float: right"/> 
              			</div>
        			</div>
        			</form>   
      			</div>
      			<?php }?>
        	</div>
            <div class="clear"></div>
            <div class="HRLine"></div>
	 	</div>

        <div class="tpLINE"></div>
        <div class="orMIDDLE">Or</div>
        <div class="btLINE"></div>
    </div>

    <div id="inner-content-box" class="pagewidth headH2"> 
    <?php if(isset($questionArray) && $questionArray != ''){
			$data_flag = true;
			?>
        <div>
            <h2>Recent answers</h2>
        </div>
        <table id="results" style="width:100% !important;">
        <?php $count = 1; 
        $qCount = count($questionArray);
        foreach ($questionArray as $key => $qaValue) {
        
		if($key%$pageLimit == 0){
			$num = floor($key/$pageLimit);
		}
		?>
		<tr class="set_<?php e($num); ?>">
			<td>
        <div class="TXT-LHight">
            <p>
                <a href="<?php e(SITE_URL.'qna/question/'.$qaValue['Qna_question']['url_key'])?>" class="questionA"><?php e($qaValue['Qna_question']['question_text'])?> </a>
                <?php e($html->link("Read more",SITE_URL.'qna/question/'.$qaValue['Qna_question']['url_key'],array('escape'=>false,'class'=>'readMore')));?>
            </p>
            <div class="QnAbutton">
            	<?php foreach ($qaValue['Qna_question_category'] as $catValue) {
                                                            $str = ($categories[$catValue['category_id']]);
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"&"))
                                                            $str = str_replace("&","_and",($categories[$catValue['category_id']]));
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"/"))
                                                            $str = str_replace("/","_slash",($categories[$catValue['category_id']]));
                                                            $str = str_replace(" ","-",$str);
            		 e($html->link($categories[$catValue['category_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false)));
                 }?>
                <br/><br/>
            </div>
            <div class="TxTimg">
            
                 <?php foreach ($qaValue['User'] as $uValue) {
            		if(isset($uValue['user_image'])){
            			$image_path = MENTORS_IMAGE_PATH.DS.$uValue['member_id'].DS.$uValue['user_image'];?>
            			<a href="<?php e(SITE_URL.strtolower($uValue['url_key']));?>">
            				<img src="<?php e(SITE_URL.'img/'.$image_path)?>" width="30" height="30" alt="<?php e($uValue['first_name']." ".$uValue['last_name']);?>" title="<?php e($uValue['first_name']." ".$uValue['last_name']);?>"/>
            			</a>
            		<?php } else {
            			$image_path = 'profile.png';?>
            			<a href="<?php e(SITE_URL.strtolower($uValue['url_key']));?>">
            				<img src="<?php e(SITE_URL.'img/media/'.$image_path)?>" width="30" height="30" alt="<?php e($uValue['first_name']." ".$uValue['last_name']);?>" title="<?php e($uValue['first_name']." ".$uValue['last_name']);?>"/>
            			</a>
            		<?php }
                 }?>
                
            </div>
            <?php if($key == ($qCount - 1)) {?>
            	<div class="HRLine" style="border-top: 0px;"></div>
            <?php } else {?>
            <?php if($count < $pageLimit) { ?>
            	<div class="HRLine"></div>
            <?php $count++; }else {?>
            	<div class="HRLine" id="HRLine_<?php e($num); ?>"></div>
            <?php $count = 1;}
            }?>
        </div>
        </td>
					</tr>
		<?php }?>
		</table>
		  <?php /*   if($num >0){
			 <div class="more-button-search" style="width:61%; float:right; padding-bottom:30px; text-align:left; margin-top: 50px;" id="pageNavPosition">
				<input type="button" value="MORE ANSWERS">
			</div>
	 } */?>
	 <?php }	?>
	 
	<?php $qCount = count($insights); if($qCount > 0){ ?>
	<div style="margin-top: 50px;">
        <h2>Recent insights</h2>
    </div>
    <div id="mentors-content">
    <?php $countA = 1; foreach ($insights as $key => $qaValue) {?>
        <div class="TXT-LHight">
            <p>
                <a href="<?php e(SITE_URL.'insight/insight/'.$qaValue['Insight']['url_key'])?>" class="questionA"><?php e($qaValue['Insight']['title'])?> </a>
               <?php  e($html->link("Read more",SITE_URL.'insight/insight/'.$qaValue['Insight']['url_key'],array('escape'=>false,'class'=>'readMore')));?>
            </p>
            <div class="QnAbutton">
            	<?php foreach ($qaValue['Insight_category'] as $catValue) {

                                                            $str = ($categories[$catValue['category_id']]);
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"&"))
                                                            $str = str_replace("&","_and",($categories[$catValue['category_id']]));
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"/"))
                                                            $str = str_replace("/","_slash",($categories[$catValue['category_id']]));
                                                            $str = str_replace(" ","-",$str);
            		 e($html->link($categories[$catValue['category_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false)));
            		
                 }?>
                <br/><br/>
            </div>
            <div class="TxTimg">
            
                 <?php 
            		if(isset($qaValue['User']['user_image'])){
            			$image_path = MENTORS_IMAGE_PATH.DS.$qaValue['Insight']['user_id'].DS.$qaValue['User']['user_image'];?>
            			<a href="<?php e(SITE_URL.strtolower($qaValue['User']['url_key']));?>">
            				<img src="<?php e(SITE_URL.'img/'.$image_path)?>" width="30" height="30" alt="<?php e($qaValue['User']['first_name']." ".$qaValue['User']['last_name']);?>" title="<?php e($qaValue['User']['first_name']." ".$qaValue['User']['last_name']);?>"/>
            			</a>
            		<?php } else {
            			$image_path = 'profile.png';?>
            			<a href="<?php e(SITE_URL.strtolower($qaValue['User']['url_key']));?>">
            				<img src="<?php e(SITE_URL.'img/media/'.$image_path)?>" width="30" height="30" alt="<?php e($qaValue['User']['first_name']." ".$qaValue['User']['last_name']);?>" title="<?php e($qaValue['User']['first_name']." ".$qaValue['User']['last_name']);?>"/>
            			</a>
            		<?php }
                 ?>
                
            </div>
            <?php if($key == ($qCount - 1)) { ?>
            <div class="HRLine" style="border-top: 0px;"></div>
            <?php }else {?>
            <div class="HRLine"></div>
            <?php }?>
        </div>
		<?php $countA++;}?>
	</div>
	<?php }?>
    </div>
</div>

<?php
if($data_flag){?>	
	<?php //e($form->hidden('num',array('value'=>$num,'id'=>'num'))); ?>
	<?php //e($form->hidden('countShow',array('id'=>'countShow'))); ?>
	<?php
}else{?>
	<?php //e($form->hidden('num',array('value'=>'NOT','id'=>'num'))); ?>
	<?php
}?>

<script type="text/javascript">
jQuery(document).ready(function(){

	/*var num = jQuery("#num").val();	
	
	if(num !='NOT'){			
		jQuery(".set_0").show();
		jQuery("#countShow").val(0);	
	}

	jQuery("#pageNavPosition").click(function(){
		var num = parseInt(jQuery("#num").val(),10);	
		var countShow = parseInt(jQuery("#countShow").val(),10) + 1;
		//createCookie('cookie_countShow',countShow,0);
		
		if(num > countShow){
			jQuery(".set_"+countShow).show();
			jQuery("#HRLine_"+(countShow-1)).show();
			jQuery("#countShow").val(countShow);	
		}else if(num == countShow){
			jQuery(".set_"+countShow).show();
			jQuery("#countShow").val(countShow);
			jQuery("#HRLine_"+(countShow-1)).show();
			jQuery("#pageNavPosition").hide();	
			jQuery("#openRegisterNav").hide();
			jQuery("#HRLine_"+(countShow)).show();
		}
	});*/

    function updateCount ()
    {
        var qText = jQuery("#questionTextBox").val();

       if(qText.length < 128) {
           jQuery("#noOfChar").html(128 - qText.length);
       } else {
           jQuery("#noOfChar").html(0);
           jQuery("#questionTextBox").val(qText.substring(0,128));
       }
    }

    jQuery("#questionTextBox").keyup(function () {
        updateCount();
    });
    jQuery("#questionTextBox").keypress(function () {
        updateCount();
    });
    jQuery("#questionTextBox").keydown(function () {
        updateCount();
    });

    jQuery("#applyPreview").click(function(){

    	var flag = 0;
    	if(jQuery.trim(jQuery("#questionTextBox").val()) == '') {
			jQuery('#questionTextBox').css('border-color','#F00');
			jQuery('#questionTextBox').focus();
			flag++;
		} else {
			jQuery('#questionTextBox').css('border-color',''); 
		}

    	var context = jQuery.trim(jQuery("#questionContextBox").val());
    	var words = countWords('questionContextBox');
    	if(context == '' /*|| words < 50*/) {
			jQuery('#questionContextBox').css('border-color','#F00');
			jQuery('#questionContextBox').focus();
			flag++;
		} else {
			jQuery('#questionContextBox').css('border-color','');
		}

		if(flag > 0) {
			return false;
		} else {
			jQuery("#Addform").submit();
			return true;
		}
		
    });

    jQuery("#questionTextBox").click(function(){

        jQuery("#questionContextDiv").show();
        jQuery("#applyPreviewButton").hide();
        
    });
    
    jQuery("#applyPreviewButton").click(function(){
        
    	jQuery("#questionContextDiv").show();
        jQuery("#applyPreviewButton").hide();
    });

    jQuery("#cancelLink").click(function(){
        
    	jQuery("#questionContextDiv").hide();
        jQuery("#applyPreviewButton").show();
        jQuery('#questionContextBox').css('border-color','');
        jQuery('#questionTextBox').css('border-color','');
        return false;
    });

    function updateCountIn()
    {
        var qText = jQuery("#titleTextBox").val();

       if(qText.length < 128) {
           jQuery("#noOfCharIn").html(128 - qText.length);
       } else {
           jQuery("#noOfCharIn").html(0);
           jQuery("#titleTextBox").val(qText.substring(0,128));
       }
    }

    jQuery("#titleTextBox").keyup(function () {
        updateCountIn();
    });
    jQuery("#titleTextBox").keypress(function () {
        updateCountIn();
    });
    jQuery("#titleTextBox").keydown(function () {
        updateCountIn();
    });

    jQuery("#applyPreviewButtonIn").click(function(){

    	var flag = 0;
    	if(jQuery.trim(jQuery("#titleTextBox").val()) == '') {
			jQuery('#titleTextBox').css('border-color','#F00');
			jQuery('#titleTextBox').focus();
			flag++;
		} else {
			jQuery('#titleTextBox').css('border-color',''); 
		}

		if(flag > 0) {
			return false;
		} else {
			jQuery("#AddformIn").submit();
			return true;
		}
		
    });

});
</script>
<script type="text/javascript">
mixpanel.track("Page View", {"URL": document.URL});
</script>