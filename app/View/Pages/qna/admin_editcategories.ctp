	<div class="adminrightinner">
		<?php e($form->create('Qna_category', array('url' => array('controller' => 'qna', 'action' => 'editcategories'))));?>
		<?php e($form->input('id'));?>
		<div class="tablewapper2 AdminForm">
			<table border="0" class="Admin2Table" width="100%">			   
				<tr>
					<td valign="middle" class="Padleft26"><?php echo $res['Qna_category']['parent_id']; ?><span class="input_required">*</span></td>
					<td><?php e($form->input($modelName.'.category', array('div'=>false, 'label'=>false, "class" => "TextBox5")));?></td>
				</tr>
			</table>
		</div>
		<div class="buttonwapper">
			<div><input type="submit" value="Submit" class="submit_button" /></div>
			<div class="cancel_button"><?php echo $html->link("Cancel", "/admin/qna/categories", array("title"=>"", "escape"=>false)); ?></div>
		</div>
		<?php  e($form->end());	?>	
	</div>

