<form action="<?php echo SITE_URL ?>qna/add_question" method="post" id="Addform">
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div id="content-submit">
        	<div class="mentor_details" style="width: 960px;"><!-- VL 22/12-->
        		<input type="hidden" name="data[Qna_question][question_text]" value="<?php e($Question_text);?>"/>
                <input type="hidden" name="data[Qna_question][question_context]" value="<?php e($Question_context);?>"/>
                <input type="hidden" name="data[Qna_question][id]" id="qId" value="<?php e($Question_id);?>"/>
				<div id="mentor-detail-left1" style="min-width: 600px;">
					<div id="area-box">
						<div class="expertise" style="padding-top:15px;">
							<h1>Review your question<sub style="font-size: 11px; font-weight: normal; font-family: Ubuntu; margin-left: 5px;">Free & anonymous advice for business leaders</sub></h1>
							<br/>
							<h5><?php e($Question_text) ?></h5>
							<p style="text-align: left;">
								<?php echo nl2br($Question_context);?>
								<br/><br/>
								<a href="<?php e(SITE_URL.'qna/ask_question/')?>" style="float: right;">Edit</a>
							</p>
						</div>
					</div>
    			</div>

    			<div id="mentor-detail-right" style="margin-bottom: 20px;"><!-- VL 22/12 start-->
					<div class="rates labelSET">
						<h1>Choose Category</h1>
						<br/>
						<h5>Functions</h5>
						<?php $countC = 0; foreach($fcategories as $fvalue) {?>
						<label><input class="group1" type="checkbox" name="catCheck[]" value="<?php e($fvalue['Qna_category']['id']);?>"></input><?php e($fvalue['Qna_category']['category']);?></label><br/>
						<?php }?>
						<br/><br/>
						<h5>Domains</h5>
						<?php foreach($dcategories as $dvalue) {?>
						<label><input class="group1" type="checkbox" name="catCheck[]" value="<?php e($dvalue['Qna_category']['id']);?>"></input><?php e($dvalue['Qna_category']['category']);?></label><br/>
						<?php $countC++;}?>
					</div>
				</div>

				<div class="clear"></div>
				<div class="HRLine"></div>
				<?php if($this->Session->read('Auth.User.id') != '') {?>
				<div id="Apply" style="float: right; margin-bottom:80px; margin-right: 50px;" >
					<input id="apply" class="submitProfile1 LTSpreview" type="submit" value="Submit">
				</div>
				<?php } else {?>
				<div id="oldButtonDiv" style="float: right; margin-bottom:80px; margin-right: 100px;" >
					<input id="openButton" type="button" value="Submit" class="mgButton">
				</div>	
				<?php }?>
				<div class="clear"></div>
			</div>
		</div>
		<?php e($form->end()); ?>
			<div style="margin-left: 260px; margin-top: 20px; display: none;" id="regDivText">
				<span style="font-weight: bold; margin-right: 10px; margin-top: 18px;"><img src="<?php e(SITE_URL);?>img/media/arrow.png"></img> To continue, Register or Submit as a guest</span>
			</div>
			<div id="linkedinDiv" style="display: none; margin-top: 20px; width: 850px;" class="pagewidth">
				<div style="float: left; width: 42%;" >
					<div> 
						<div class="submitProfile" style="cursor:pointer; margin-top: 95px;">
							
							<?php e($html->image('linked-in.png',array('alt'=>'Linked in','style'=>'cursor:pointer;'))); ?>
						</div>
					</div>
					<div style="display: block; font-size: 11px; margin-bottom:80px; margin-top: 10px; float: right;">Mentors Guild will <span style="font-weight: bold;">never </span>post to your LinkedIn account without your permission.</div>
				</div>
				<div class="vrtLINE"></div>
				<div style="float: right; width: 49%;" >
					<div style="margin-top: 20px;"> 
						      <?php
      							echo $form->create(null,array('url'=>array('controller'=>'fronts','action'=>'new_add_question'),'id'=>'newmentorshipForm'));
      							?>
      							<table>
      								<tbody>
      									<tr>
      										<td style="float: right; margin-top: 16px; margin-right: 10px;">Name</td>
      										<td>
      											<input name="data[Mentorship][firstname]" type="text" class="forminput" id="userfname" value="" maxlength="120" placeholder="First" style="width:153px;">
												<input name="data[Mentorship][lastname]" type="text" class="forminput" id="userlname" value="" maxlength="120" placeholder="Last" style="width:153px;">
      										</td>
      									</tr>
      									<tr>
      										<td style="float: right; margin-top: 10px; margin-left: 5px;">Work email</td>
      										<td>
												<input name="data[Mentorship][email]" type="text" class="forminput" placeholder = "email@yourcompany.com" id="useremail" value="" maxlength="250" style="width:332px;">      										
											</td>
      									</tr>
      									<tr>
      										<td style="float: right; margin-top: 16px; margin-right: 10px;">Phone</td>
      										<td>
												<input name="data[Mentorship][phone]" type="text" class="forminput" placeholder = "(xxx) xxx-xxxx" id="userphone" value="" maxlength="250" style="width:332px;">      										
											</td>
      									</tr>
      								</tbody>
      							</table>
      							
					</div>
					<div style="float: right;" >
		                   <input id="sendButton" type="submit" value="Submit" class="mgButton" style="margin-bottom: 20px;margin-right: 5px; float: right;">
					</div>	
					</form>
				</div>
			</div>
	</div>
</div>
</form>

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery(".submitProfile").click(function(){
       var matches = [];
       jQuery(".group1:checked").each(function() {
            matches.push(this.value);
        }); 

       var qId = jQuery('#qId').val();

       jQuery.ajax({
			url:SITE_URL+'/qna/add_question',
			type:'post',
			dataType:'json',
			data: 'checkBoxes='+matches + '&qId=' + qId,
			success:function(){
				window.location.href = SITE_URL+'linkedin/signuplinkclientpopup.php?questionId='+qId;
			}
		});	
	return false;
    });

	jQuery("#openButton").click(function() {
		document.getElementById('linkedinDiv').style.display = 'inline-block';
		document.getElementById('oldButtonDiv').style.display = 'none';
		document.getElementById('regDivText').style.display = 'block';
		jQuery('html, body').animate({
	        scrollTop: jQuery("#linkedinDiv").offset().top
	    }, 2000);
	});
	
	jQuery("#sendButton").click(function() {
    
       var matches = [];
       jQuery(".group1:checked").each(function() {
            matches.push(this.value);
        }); 

       var qId = jQuery('#qId').val();

       jQuery.ajax({
			url:SITE_URL+'/qna/add_question',
			type:'post',
			dataType:'json',
			data: 'checkBoxes='+matches + '&qId=' + qId,
			success:function(){
				
				var flag = 0;
				
		      	if(jQuery.trim(jQuery("#userfname").val()) == '') {
                    jQuery('#userfname').css('border-color','#F00');
                    jQuery('#userfname').focus();                
                    flag++;
                 }else {
                    jQuery('#userfname').css('border-color','');                 
                 }
		         if(jQuery.trim(jQuery("#userlname").val()) == '') {
                    jQuery('#userlname').css('border-color','#F00');
                    jQuery('#userlname').focus();                
                    flag++;
                 }else {
                    jQuery('#userlname').css('border-color','');                 
                 }
		      	 if(jQuery.trim(jQuery("#useremail").val()) == '') {
                    jQuery('#useremail').css('border-color','#F00');
                    jQuery('#useremail').focus();                
                    flag++;
                 }else {
                    jQuery('#useremail').css('border-color','');                 
                 }
		         if(jQuery.trim(jQuery("#userphone").val()) == '') {
                    jQuery('#userphone').css('border-color','#F00');
                    jQuery('#userphone').focus();                
                    flag++;
                 }else {
                    jQuery('#userphone').css('border-color','');                 
                 }
    			 if(flag > 0) {
    				return false;
    			 } else {
					jQuery("#newmentorshipForm").submit();
                 }
			}
			});	
			return false;
    		});
    		
});
</script>