<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div class="mentor_details"><!-- VL 22/12-->
			<div id="mentor-detail-left1">
				<form action="<?php echo SITE_URL ?>qna/question_preview" method="post" id="Addform">
				<div id="area-box">
					<div class="expertise headH1" style="padding-top:15px;">
						<h1>Have a question? Ask our experts</h1>
						<br/>						
					</div>
					<?php if(isset($Question_id)){?>
	                	<input type="hidden" name="data[Qna_question][id]" value="<?php e($Question_id);?>"/>
	                <?php }?>
					<div class="expertise">
						<span>Your question</span> <span style="float:right;">(<span id="noOfChar"><?php e(128 - strlen($Question_text))?></span>  characters remaining)</span>
                        <div class="inputRELATIVE">
                        	<p>
                        		<input id="questionTextBox" name="data[Qna_question][question_text]" type="text" style="width:651px;" placeholder="Enter question e.g. How do I drive employee engagement?"  value="<?php e(htmlspecialchars($Question_text))?>" />
                        	</p>
                        	<div class="showHINT" style="height: 90px;">
                        		<span></span><!--for left arrow show on hover-->
                        		<p style="margin-top: 30px;">
                        			Be to the point
                        		</p>
                    		</div>                        	
                        </div>
                    </div>
                    <div class="expertise">
						<span>Provide details</span> <span style="float:right;"></span>
						<div class="inputRELATIVE">
	                        <p>
	                        	<textarea id="questionContextBox" class="forminput" name="data[Qna_question][question_context]" style="width:651px; padding:10px; height:150px;" placeholder="Enter details and background." ><?php e(htmlspecialchars($Question_context))?></textarea> 
	                        </p>
                        	<div class="showHINT" style="height: 285px;">
                        		<span></span><!--for left arrow show on hover-->
                        		<p style="text-align: left;">
                        			1. Add all relevant information needed for a good answer <br/><br/>
									2. Read out loud to see if it makes sense <br/><br/>
									3. Remove any personally identifiable information (names, phone numbers, etc)<br/><br/>
									4. We will send you an email confirmation
                        		</p>
                    		</div>
                    	</div> 
                    </div>

                    <div id="Apply" style="float: right;" role="Apply">
						<a class="reset" style="float: left; margin-top: 10px; margin-right: 30px;" href="<?php e(SITE_URL)?>qanda">Cancel</a>
						<input id="applyPreview" class="submitProfile" type="submit" value="Preview">
					</div>
					<br/><br/>
				</div>
				</form>
    		</div>
    	</div>	
    </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){

    function updateCount ()
    {
        var qText = jQuery("#questionTextBox").val();

       if(qText.length < 128) {
           jQuery("#noOfChar").html(128 - qText.length);
       } else {
           jQuery("#noOfChar").html(0);
           jQuery("#questionTextBox").val(qText.substring(0,128));
       }
    }

    jQuery("#questionTextBox").keyup(function () {
        updateCount();
    });
    jQuery("#questionTextBox").keypress(function () {
        updateCount();
    });
    jQuery("#questionTextBox").keydown(function () {
        updateCount();
    });

   
    jQuery("#applyPreview").click(function(){

    	var flag = 0;
    	if(jQuery.trim(jQuery("#questionTextBox").val()) == '') {
			jQuery('#questionTextBox').css('border-color','#F00');
			jQuery('#questionTextBox').focus();
			flag++;
		} else {
			jQuery('#questionTextBox').css('border-color',''); 
		}

    	var context = jQuery.trim(jQuery("#questionContextBox").val());
    	var words = countWords('questionContextBox');
    	if(context == '' /*|| words < 50*/) {
			jQuery('#questionContextBox').css('border-color','#F00');
			jQuery('#questionContextBox').focus();
			flag++;
		} else {
			jQuery('#questionContextBox').css('border-color','');
		}

		if(flag > 0) {
			return false;
		} else {
			jQuery("#Addform").submit();
			return true;
		}
		
    });
});
</script>

	
