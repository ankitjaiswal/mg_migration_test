<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth" style=" margin-bottom:30px; min-height: 500px;">
    <div id="financial-management">
        <p>
		<span><?php e($queryString);?></span> 
		</p>
    </div>
    <div id="mentors-content">
    <?php $countA = 1; foreach ($questionArray as $qaValue) {?>
    	<?php if($qaValue['Qna_question']['haveAnswer'] == 1 ){?>
        <div class="TXT-LHight" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
           <h1><span style="display:none;">Mentors Guild | <?php e($queryString);?>'s Question & Answer</span></h1> 
           <p>
                <a href="<?php e(SITE_URL.'qna/question/'.$qaValue['Qna_question']['url_key'])?>" class="questionA" itemprop="url"><?php e($qaValue['Qna_question']['question_text'])?> </a>
               <?php  e($html->link("Read more",SITE_URL.'qna/question/'.$qaValue['Qna_question']['url_key'],array('escape'=>false,'class'=>'readMore')));?>
            </p>
            <div class="QnAbutton">
            	<?php foreach ($qaValue['Qna_question_category'] as $catValue) {
            		 e($html->link($categories[$catValue['category_id']],SITE_URL.'qna/category_questions/'.$catValue['category_id'],array('escape'=>false)));
                 }?>
                <br/><br/>
            </div>
            <div class="TxTimg">
            
                 <?php foreach ($qaValue['User'] as $uValue) {
            		if(isset($uValue['user_image'])){
            			$image_path = MENTORS_IMAGE_PATH.DS.$uValue['member_id'].DS.$uValue['user_image'];?>
            			<a href="<?php e(SITE_URL.strtolower($uValue['url_key']));?>"rel="author">
            				<img src="<?php e(SITE_URL.'img/'.$image_path)?>" width="30" height="30" alt="<?php e($uValue['first_name']." ".$uValue['last_name']);?>" title="<?php e($uValue['first_name']." ".$uValue['last_name']);?>"/>
            			</a>
            		<?php } else {
            			$image_path = 'profile.png';?>
            			<a href="<?php e(SITE_URL.strtolower($uValue['url_key']));?>">
            				<img src="<?php e(SITE_URL.'img/media/'.$image_path)?>" width="30" height="30" alt="<?php e($uValue['first_name']." ".$uValue['last_name']);?>" title="<?php e($uValue['first_name']." ".$uValue['last_name']);?>"/>
            			</a>
            		<?php }
                 }?>
                
            </div>
            <div class="HRLine"></div>
        </div>
        <?php }?>
		<?php $countA++; }?>
		<?php $countU = 1; foreach ($questionArray as $qaValue) {?>
    	<?php if($qaValue['Qna_question']['haveAnswer'] == 0 && ($this->Session->read('Auth.User.id') != '' &&  $this->Session->read('Auth.User.role_id') == Configure::read('App.Role.Mentor'))){?>
        <div class="TXT-LHight">
            <p>
                <a href="<?php e(SITE_URL.'qna/question/'.$qaValue['Qna_question']['url_key'])?>" class="questionA"><?php e($qaValue['Qna_question']['question_text'])?> </a>
               <?php  e($html->link("Read more",SITE_URL.'qna/question/'.$qaValue['Qna_question']['url_key'],array('escape'=>false,'class'=>'readMore')));?>
            </p>
            <div class="QnAbutton">
            	<?php foreach ($qaValue['Qna_question_category'] as $catValue) {
            		 e($html->link($categories[$catValue['category_id']],SITE_URL.'qna/category_questions/'.$catValue['category_id'],array('escape'=>false)));
                 }?>
                <br/><br/>
            </div>
            <div class="TxTimg">
            
            	<?php foreach ($qaValue['User'] as $uValue) {
            		if(isset($uValue['user_image'])){
            			$image_path = MENTORS_IMAGE_PATH.DS.$uValue['member_id'].DS.$uValue['user_image'];?>
            			<a href="<?php e(SITE_URL.strtolower($uValue['url_key']));?>">
            				<img src="<?php e(SITE_URL.'img/'.$image_path)?>" width="30" height="30" alt="<?php e($uValue['first_name']." ".$uValue['last_name']);?>" title="<?php e($uValue['first_name']." ".$uValue['last_name']);?>"/>
            			</a>
            		<?php } else {
            			$image_path = 'profile.png';?>
            			<a href="<?php e(SITE_URL.strtolower($uValue['url_key']));?>">
            				<img src="<?php e(SITE_URL.'img/media/'.$image_path)?>" width="30" height="30" alt="<?php e($uValue['first_name']." ".$uValue['last_name']);?>" title="<?php e($uValue['first_name']." ".$uValue['last_name']);?>"/>
            			</a>
            		<?php }
                 }?>
                
            </div>
            <div class="HRLine"></div>
        </div>
        <?php $countU++;}?>
		<?php }?>
		<?php if($countA == 1 && $countU == 1 ) {?>
			<div class="TXT-LHight">
	            <p>No questions in this category</p>
	        <div class="HRLine"></div>
	        </div>
        <?php } ?>       
		</div>
		
		<div id="financial-management" style="margin-top: 40px;">
	        <p>
			<span>Insights for <?php e($queryString);?></span> 
			</p>
	    </div>
    <div id="mentors-content">
    <?php $countA = 1; $qCount = count($insights); foreach ($insights as $key => $qaValue) {?>
        <div class="TXT-LHight">
            <p>
                <a href="<?php e(SITE_URL.'insight/insight/'.$qaValue['Insight']['url_key'])?>" class="questionA"><?php e($qaValue['Insight']['title'])?> </a>
               <?php  e($html->link("Read more",SITE_URL.'insight/insight/'.$qaValue['Insight']['url_key'],array('escape'=>false,'class'=>'readMore')));?>
            </p>
            <div class="QnAbutton">
            	<?php foreach ($qaValue['Insight_category'] as $catValue) {
            		 e($html->link($categories[$catValue['category_id']],SITE_URL.'qna/category_questions/'.$catValue['category_id'],array('escape'=>false)));
                 }?>
                <br/><br/>
            </div>
            <div class="TxTimg">
            
                 <?php 
            		if(isset($qaValue['User']['user_image'])){
            			$image_path = MENTORS_IMAGE_PATH.DS.$qaValue['Insight']['user_id'].DS.$qaValue['User']['user_image'];?>
            			<a href="<?php e(SITE_URL.strtolower($qaValue['User']['url_key']));?>">
            				<img src="<?php e(SITE_URL.'img/'.$image_path)?>" width="30" height="30" alt="<?php e($qaValue['User']['first_name']." ".$qaValue['User']['last_name']);?>" title="<?php e($qaValue['User']['first_name']." ".$qaValue['User']['last_name']);?>"/>
            			</a>
            		<?php } else {
            			$image_path = 'profile.png';?>
            			<a href="<?php e(SITE_URL.strtolower($qaValue['User']['url_key']));?>">
            				<img src="<?php e(SITE_URL.'img/media/'.$image_path)?>" width="30" height="30" alt="<?php e($qaValue['User']['first_name']." ".$qaValue['User']['last_name']);?>" title="<?php e($qaValue['User']['first_name']." ".$qaValue['User']['last_name']);?>"/>
            			</a>
            		<?php }
                 ?>
                
            </div>
            <?php if($key == ($qCount - 1)) { ?>
            <div class="HRLine" style="border-top: 0px;"></div>
            <?php }else {?>
            <div class="HRLine"></div>
            <?php }?>
        </div>
		<?php $countA++;}?>
		<?php if($countA == 1) {?>
			<div class="TXT-LHight">
	            <p>No matching insights</p>
	        </div>
        <?php } ?> 
		<a class="reset" href="<?php e(SITE_URL.'qanda');?>" style="float: left; margin-bottom: 80px; margin-top: 30px;">Back to Q&A</a>
	</div>
	
	</div>
</div>
	