<?php
$pageLimit = 5;
$data_flag = false;
$num = 0;
	?>
<?php $completelink = SITE_URL."qna/question/".($question['Qna_question']['url_key']);?>
<?php $tinyUrl = $this->General->get_tiny_url(urlencode($completelink));?>
<?php $atMentorsGuild = "@MentorsGuild: "?>
<?php $atsybmol = "@"?>
<?php $MentorsGuild = "Mentors Guild: "?>
<?php $compTitle = $question['Qna_question']['question_text'];?>
<?php $linkTitle = $question['Qna_question']['question_text'];?>
	
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div id="content-submit" itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
       		<h1><span itemprop="title"><?php e($question['Qna_question']['question_text']); ?></span></h1>
				<div id="text-content"><!--VL 1/8/13-->
					<p>
						<?php echo nl2br($question['Qna_question']['question_context']); ?>
					</p>
					<div class="QnAbutton">
						<?php 
	  foreach ($question['Qna_question_category'] as $catValue) {

                                                            $str = ($categories[$catValue['category_id']]);
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"&"))
                                                            $str = str_replace("&","_and",($categories[$catValue['category_id']]));
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"/"))
                                                            $str = str_replace("/","_slash",($categories[$catValue['category_id']]));

            		 e($html->link($categories[$catValue['category_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false)));
			    		}?>
						<br/><br/>
					</div>
					
					<?php  if($this->Session->read('Auth.User.role_id') == Configure::read('App.Role.Mentor')) {?>
					<div style="width: 360px; float: right; padding-top: 15px;">
						<input class="mgButton" type="button" value="Add your answer" style="margin-top: -10px; width: 120px; font-weight: normal; float: right;" id="add_ans"/>
						<a href="" style="margin-top: 3px;margin-left: 50px;" id="href_addans">Is this your area of expertise?</a>
					</div>
					<?php } else{?>
                                    <div style="width: 460px; float: right; padding-top: 15px;">
						<input class="mgButton" type="button"   value="Add a question or a comment" style="margin-top: -10px; width: 220px; font-weight: normal; float: right;" id="add_a_q"/>
						<a href="" style="margin-top: 15px;" id="href_add">Not quite what you were looking for?</a>
					</div>
					<?php }?>
					
				<br/><br/>
				</div>
				
			<?php if(isset($answers) && $answers != ''){
				$data_flag = true;
			?>
			<h2><span style="font-size:20px;color: #363636;font-family: Ubuntu;font-weight: normal;">Answered by</span></h2>
			<br/><br/>
			<?php }?>
			<div id="mentors-content" style="padding-bottom: 0px;">
                <?php if(isset($focusid) && $focusid != ''){?>
                        <input type="hidden" name="Language" value="<?php echo($focusid)?>" id="focusJS">
               <?php }?> 



				<table id="results" style="width:100% !important;">
					<?php if(isset($answers) && $answers != ''){
						$noOfAnswers = count($answers);
						e($form->hidden($noOfAnswers,array('value'=>$noOfAnswers,'id'=>'noOfAnswers')));
						foreach($answers as $key => $answer){
							
							if($key%$pageLimit == 0){
								$num = floor($key/$pageLimit);
								}
					?>
					<tr class="set_<?php e($num); ?>">
						<td id="a<?php echo($answer['Qna_answer']['id']);?>" >
							<div class="box1">
                                                       </br></br>
								<?php if($answer['Qna_answer']['status'] == 1) {?>
						 			<div class="content-left-img QnAconsult" style="width: 130px;"> 
						 				<?php if(isset($answer['Qna_answer']['user_image']) && $answer['Qna_answer']['user_image'] !=''){
					            			$image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$answer['Qna_answer']['member_id'].DS.$answer['Qna_answer']['user_image'];
					            		} else {
					            			$image_path = SITE_URL.'img/media/profile.png';
					            		}?>
						  				<a href="<?php e(SITE_URL.strtolower($answer['Qna_answer']['url_key']));?>" style="border: 0px;">
						  					<img class="desaturate" src="<?php e($image_path);?>" alt="<?php e($answer['Qna_answer']['first_name']." ".$answer['Qna_answer']['last_name']);?>" style="width:100px; height:100px;" />
						  				</a><br/>
						  					<input id="button<?php e($answer['Qna_answer']['id']) ?>" class="mgButton" type="button" value="Upvote | <?php e($answer['Qna_answer']['upvotes']);?>" style="margin-top: 5px; width: 100px; font-weight: normal" onclick="upVote(<?php e($answer['Qna_answer']['id']) ?>);">
						  			</div>
					  			<?php }?>
					  			<?php if($answer['Qna_answer']['status'] == 1) {?>
                     				<div class="content-right-text" style="width: 824px;">
                     					<h2>
                     					<span>
											<a href="<?php e(SITE_URL.strtolower($answer['Qna_answer']['url_key']));?>" rel="author"><?php e($answer['Qna_answer']['first_name']." ".$answer['Qna_answer']['last_name']);?></a>
										</span> 

										<br/>
										<?php 
										$disp=SITE_URL.strtolower($answer['Qna_answer']['url_key'])."#answers";
										if($answer['Qna_answer']['ans_count'] >1)
											e($html->link($answer['Qna_answer']['ans_count']." answers",$disp,array('escape'=>false, 'style'=>'font-size: 14px;font-family:Ubuntu;font-weight: normal;')));
										else
											e($html->link($answer['Qna_answer']['ans_count']." answer",$disp,array('escape'=>false, 'style'=>'font-size: 14px;font-family:Ubuntu;font-weight: normal;')));?>
                     					
                     					<div class="share_icons" onmouseout="doSomethingMouseOut(<?php e($key)?>);"  onmouseover="doSomethingMouseOver(<?php e($key)?>)" style="width: 220px; float: right; margin-top: -10px;">
                     			<?php }else {?>
                     				<div class="content-right-text" style="width: 824px; margin-left: 130px;">
                     					<h2 style="height: 30px;">
                     					<span>
											<a href="#"><?php e($answer['Qna_answer']['first_name']." ".$answer['Qna_answer']['last_name']);?></a>
										</span>
										
										<div class="share_icons" onmouseout="doSomethingMouseOut(<?php e($key)?>);"  onmouseover="doSomethingMouseOver(<?php e($key)?>)" style="width: 220px; float: right; margin-top: 5px;">
                     			<?php }?>
									
					
						<div style="float:right;" >
                                                 <input class="mgButton" type="button" style="width: 77px;height:25px; font-weight: normal; float: right;" value="Share" onclick='javascript:doSomething('.$key.');',onmouseover = 'doSomethingMouseOver('.$key.');'>
							<?php /*e($html->link($html->image('images/share_icon.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'77')),'javascript:doSomething('.$key.')',array('escape'=>false,'onmouseover' => 'doSomethingMouseOver('.$key.')', 'style'=>'padding-right: 0px;')));*/?>
						</div>
						<?php if($key == $noOfAnswers - 1 && ($answer['Qna_answer']['member_id'] == $this->Session->read('Auth.User.id'))) {?>
						<div id="shareicon_<?php e($key);?>" style="float:right; visibility:visible; width: 130px;">
						<?php }else {?>
						<div id="shareicon_<?php e($key);?>" style="float:right; visibility:hidden; width: 130px;">
						<?php }?>
							&nbsp; 
							<a href="javascript:window.open('https://plus.google.com/share?url=<?php e($completelink);?>&title=<?php e($atMentorsGuild. $answer['Qna_answer']['first_name']." ".$answer['Qna_answer']['last_name'] .$compTitle);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
							   <?php e($html->image('images/g_plus_icon.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'27'))); ?></a>
							

							<a title="Share this post/page"href="javascript:window.open('http://www.facebook.com/sharer.php?s=100&p[url]=<?php echo $completelink; ?>&p[title]=<?php e($atMentorsGuild. $answer['Qna_answer']['first_name']." ".$answer['Qna_answer']['last_name'] ." ".$compTitle);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
							      <?php e($html->image('images/fb_icon.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'27'))); ?></a>
							
                                                 <?php 
								 $linkdinUrl="http://www.linkedin.com/shareArticle?mini=true&url=".$completelink."&title=".$MentorsGuild.$answer['Qna_answer']['first_name']." ". $answer['Qna_answer']['last_name']." " .$linkTitle;
                                                        if($answer['Qna_answer']['social'] ==''){
                                                          $tweet="http://twitter.com/intent/tweet?text=".$answer['Qna_answer']['first_name']." ". $answer['Qna_answer']['last_name'] ." &#58; ".$compTitle." " .$tinyUrl."/a".$answer['Qna_answer']['id'];
								} else{
                                                          $tweet="http://twitter.com/intent/tweet?text=".$atsybmol.$answer['Qna_answer']['social']." ".$compTitle." " .$tinyUrl."/a".$answer['Qna_answer']['id'];
                                                        }
							?>	
                                               <a href="javascript:window.open('<?php echo ($linkdinUrl);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')"><?php e($html->image('images/logo_linkedin.png',array('alt'=>'LinkedIn' , 'height'=>'25', 'width'=>'27', 'id'=>'linkedin_Share'))); ?></a>
                                               <a href="javascript:window.open('<?php echo ($tweet);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')"><?php e($html->image('images/twit_icon.png',array('alt'=>'twitter' , 'height'=>'25', 'width'=>'27', 'id'=>'twitter_Share'))); ?></a>

							 
						 </div>
						<!-- Share on social media end -->
					</div>
					
									</h2>
									<div class="text-content-box">
								  		<p> 
								  			<?php if($this->Session->read('Auth.User.role_id') != Configure::read('App.Role.Mentor')){
                                                                                   $user = ClassRegistry::init('User');
                                                                                   $mentorlink = $user->find('first', array('conditions' => array('User.id' => $answer['Qna_answer']['member_id'])));
                                                                                   $disp = SITE_URL.'fronts/consultation_request/'.$mentorlink['User']['url_key'];
                                                                                   $fname =($answer['Qna_answer']['first_name']);
                                                                                   $answer = str_replace('  ', '&nbsp;&nbsp;', $answer['Qna_answer']['answer_text']);
                                                                                   $answer = trim($answer);
											echo $this->General->make_links(nl2br($answer)); 
                                                                              e($html->link("<br/><br/>Is this answer helpful? Schedule a 30-min free consultation with $fname.",$disp,array('escape'=>false, 'style'=>'font-size: 14px;font-family:Ubuntu;font-weight: normal;')));
                                                                             } else{
                                                                             $disp = $disp=SITE_URL.strtolower($answer['Qna_answer']['url_key'])."#answers";
                                                                                $fname =($answer['Qna_answer']['first_name']);
                                                                               $answer = str_replace('  ', '&nbsp;&nbsp;', $answer['Qna_answer']['answer_text']); 
                                                                               $answer = trim($answer);
											echo $this->General->make_links(nl2br($answer)); 
                                                                              e($html->link("<br/><br/>Is this answer helpful? Schedule a 30-min free consultation with $fname.",$disp,array('escape'=>false, 'style'=>'font-size: 14px;font-family:Ubuntu;font-weight: normal;')));
                                                                             }?>

								  		</p>
									</div>
								</div>
							</div>		
						</td>
					</tr>
					<?php }
					}?>
					<?php  if($this->Session->read('Auth.User.role_id')==Configure::read('App.Role.Mentor')) {?>
					<tr id="memberAnswer" style="display:none;">
						<td>
							<div class="box1">
					  			<div class="content-left-img QnAconsult" style="width: 130px;"> 
					  				<?php if(isset($userData['UserImage'][0]['image_name']) && $userData['UserImage'][0]['image_name'] !=''){
				            			$image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$userData['User']['id'].DS.$userData['UserImage'][0]['image_name'];
				            		} else {
				            			$image_path = SITE_URL.'img/media/profile.png';
				            		}?>
					  				<a href="<?php e(SITE_URL.strtolower($userData['User']['url_key']));?>">
					  					<img class="desaturate" src="<?php e($image_path);?>" alt="<?php e($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?>" style="width:100px; height:100px;" />
					  				</a><br/>
					  			</div>
                     			<div class="content-right-text" style="width: 824px;">
									<h2>
										<span>
											<a href="<?php e(SITE_URL.strtolower($userData['User']['url_key']));?>"><?php e($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?></a>
										</span>

										<br/>
										<?php 
										$disp=SITE_URL.strtolower($userData['User']['url_key'])."#answers";
										if($userData['ans_count'] > 1)
											e($html->link($userData['ans_count']." answers",$disp,array('escape'=>false, 'style'=>'font-size: 14px;')));
										else
											e($html->link($userData['ans_count']." answer",$disp,array('escape'=>false, 'style'=>'font-size: 14px;'))); ?>
									</h2>
									<div class="text-content-box">
								  		<p id="answerPara" style="min-height: 100px;"> 
								  		</p>
									</div>
									<div style="float: right;">
										<a href="#answerTextBox" id="editAnswer"> Edit </a>
					    				<input class="mgButton" style="float: none; margin-left: 15px; font-weight: normal;" value="Submit" type="button" id="submitButton">
			            			</div>
								</div>
							</div>		
						</td>
					</tr>
					<?php }?>
				</table>
			</div>	
			
			<?php 	
			/* if($num >0){ 
			 <div class="more-button-search" style="width:61%; float:right; padding-bottom:30px; text-align:left" id="pageNavPosition">
				<input type="button" value="MORE ANSWERS">
			</div>
			 } */?>
			
			<?php  if($this->Session->read('Auth.User.role_id') == Configure::read('App.Role.Mentor') && $userData['User']['access_specifier'] == 'publish') {?>
			  <div id="addanswer"></div>
                        <div id="mentors-content" style="margin-top: 50px;">
                          <table id="results" style="width:100% !important;">
					<tr class="set_X" id="answerTextBox">
						<td>
							<form action="<?php echo SITE_URL ?>qna/add_answer" method="post" id="AddAnswerform">
							<div class="box1">
					 			<div class="content-left-img" style="width: 130px;"> 
					  				<?php if(isset($userData['UserImage'][0]['image_name']) && $userData['UserImage'][0]['image_name'] !=''){
				            			$image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$userData['User']['id'].DS.$userData['UserImage'][0]['image_name'];
				            		} else {
				            			$image_path = SITE_URL.'img/media/profile.png';
				            		}?>
					  				<a href="<?php e(SITE_URL.strtolower($userData['User']['url_key']));?>">
					  					<img class="desaturate" src="<?php e($image_path);?>" alt="<?php e($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?>" style="width:100px; height:100px;" />
					  				</a><br/>
					  			</div>
                     			<div class="content-right-text" style="width: 824px;">
									<h2>
										<span>
											<a href="<?php e(SITE_URL.strtolower($userData['User']['url_key']));?>"><?php e($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?></a>
										</span> 

										<br/>
										<?php 
										$disp=SITE_URL.strtolower($userData['User']['url_key'])."#answers";
										if($userData['ans_count'] > 1)
											e($html->link($userData['ans_count']." answers",$disp,array('escape'=>false, 'style'=>'font-size: 14px;')));
										else
											e($html->link($userData['ans_count']." answer",$disp,array('escape'=>false, 'style'=>'font-size: 14px;'))); ?>
											
										<span style="float:right; font-size: 14px; font-weight: normal;">(<span style="font-size: 14px; font-weight: normal;" id="noOfChar">2000</span> characters remaining)</span>
									</h2>
									<div class="text-content-box">
								  		<p> 
								  			<input type="hidden" name="data[Qna_answer][id]" value="<?php e($question['Qna_question']['id']);?>"/>
								  			<textarea name="data[Qna_answer][answer_text]" style="width:98%; height:200px; padding:6px;" placeholder="Enter your advice here. Please avoid direct solicitation." id="memberAnswerTextArea"></textarea>
								  		</p>
									</div>
									<div style="float: right;">
				            			<?php  e($html->link("Cancel",SITE_URL.'users/my_account#answers',array('escape'=>false)));?>
					    				<input style="float: none; margin-left: 15px; font-weight: normal;" value="Preview" type="button" class="mgButton" id="previewButton">
			            			</div>
								</div>
							</div>		
							</form>
						</td>
					</tr>
				</table>
			</div>
			<?php }?>
	    </div>
    </div>
			    <div id="inner-content-box" class="pagewidth headH2" style="margin-top: 40px;"> 
    				<?php if(isset($questionArray) && $questionArray != ''){
						$data_flag = true;
					?>
			        <div>
			            <h2 style="font-size: 20px;">Similar questions</h2>
			        </div>
			        <table id="results" style="width:100% !important;">
			        <?php $count = 1; 
			        $qCount = count($questionArray);
			        foreach ($questionArray as $key => $qaValue) {
			        
					if($key%$pageLimit == 0){
						$num = floor($key/$pageLimit);
					}
					?>
					<tr class="set_<?php e($num); ?>">
						<td>
			        <div class="TXT-LHight">
			            <p>
			                <a href="<?php e(SITE_URL.'qna/question/'.$qaValue['Qna_question']['url_key'])?>" class="questionA"><?php e($qaValue['Qna_question']['question_text'])?> </a>
			                <?php e($html->link("Read more",SITE_URL.'qna/question/'.$qaValue['Qna_question']['url_key'],array('escape'=>false,'class'=>'readMore','style'=>'font-size: 14px; border-bottom: 0px;')));?>
			            </p>
			            <div class="QnAbutton">
	     <?php foreach ($qaValue['Qna_question_category'] as $catValue) {

                                                            $str = ($categories[$catValue['category_id']]);
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"&"))
                                                            $str = str_replace("&","_and",($categories[$catValue['category_id']]));
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"/"))
                                                            $str = str_replace("/","_slash",($categories[$catValue['category_id']]));

            		 e($html->link($categories[$catValue['category_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false)));
			                 }?>
			                <br/><br/>
			            </div>
			            <div class="TxTimg">
			            
			                 <?php foreach ($qaValue['User'] as $uValue) {
			            		if(isset($uValue['user_image'])){
			            			$image_path = MENTORS_IMAGE_PATH.DS.$uValue['member_id'].DS.$uValue['user_image'];?>
			            			<a href="<?php e(SITE_URL.strtolower($uValue['url_key']));?>">
			            				<img src="<?php e(SITE_URL.'img/'.$image_path)?>" width="30" height="30" alt="<?php e($uValue['first_name']." ".$uValue['last_name']);?>" title="<?php e($uValue['first_name']." ".$uValue['last_name']);?>"/>
			            			</a>
			            		<?php } else {
			            			$image_path = 'profile.png';?>
			            			<a href="<?php e(SITE_URL.strtolower($uValue['url_key']));?>">
			            				<img src="<?php e(SITE_URL.'img/media/'.$image_path)?>" width="30" height="30" alt="<?php e($uValue['first_name']." ".$uValue['last_name']);?>" title="<?php e($uValue['first_name']." ".$uValue['last_name']);?>"/>
			            			</a>
			            		<?php }
			                 }?>
			                
			            </div>
			            <?php if($key == ($qCount - 1)) {?>
			            	<div class="HRLine" style="border-top: 0px;"></div>
			            <?php } else {?>
			            <?php if($count < $pageLimit) { ?>
			            	<div class="HRLine"></div>
			            <?php $count++; }else {?>
			            	<div class="HRLine" id="HRLine_<?php e($num); ?>"></div>
			            <?php $count = 1;}
			            }?>
			        </div>
			        </td>
								</tr>
					<?php }?>
					</table>
			    <?php }	?>
			    <?php  if($this->Session->read('Auth.User.role_id') != Configure::read('App.Role.Mentor')) {?>
			    	<div id="ask_an_expert"></div>
                            <div class="column last" style="margin-top: 70px; width: 460px;" >
        			<div class="clear"></div>
        				<div class="subheading supreFREE" >
          					<h2 style="font-size: 20px;">Ask an expert <sub style="font-size: 11px; font-weight: normal; font-family: Ubuntu">Free & anonymous advice for business leaders</sub></h2>
        				</div>
        			<div class="clear"></div>

        			<form id="Addform" method="post" action="<?php echo SITE_URL ?>qna/question_preview">
        			<div class="styled-form">     
        				<span>Your question</span> <span style="float:right;">(<span id="noOfChar1">128</span> characters remaining)</span>
        				<br/>
        				<div class="QHINTarea">
        					<textarea id="questionTextBox"  style="margin-top: 5px; height: 75px;" placeholder="Enter question e.g. How do I drive employee engagement?" name="data[Qna_question][question_text]"></textarea>
        				</div>    
        				<div>        				
	        				<br/>
	              			<input name="applyPreviewButton" type="button" class="mgButton" value="Ask now" id="applyPreviewButton" style="float: right; margin-bottom: 30px;"/> 
              			</div>
        			</div>
        			<div style="display: none" id="questionContextDiv">
	        			<div class="expertise">
							<span>Provide details</span> <span style="float:right;"></span>
							<div class="inputRELATIVE">
		                        <p>
		                        	<textarea id="questionContextBox" class="forminput" name="data[Qna_question][question_context]" style="width: 96%; padding:10px; height:150px;" placeholder="To get a useful response provide all relevant background, but avoid any personally identifiable information." ></textarea> 
		                        </p>
	                    	</div> 
	                    </div>
	                    <div id="Apply" style="float: right; margin-bottom: 30px;" >
							<a class="reset" style="float: left; margin-top: 10px; margin-right: 10px;" href="" id="cancelLink">Cancel</a>
							<input id="applyPreview" class="submitProfile" type="submit" value="Preview">
						</div>
					</div>
        			</form>   
      			</div>
      			<?php }?>
			     </div>
</div>
<?php
if($data_flag){?>	
	<?php e($form->hidden('num',array('value'=>$num,'id'=>'num'))); ?>
	<?php e($form->hidden('countShow',array('id'=>'countShow'))); ?>
	<?php
}else{?>
	<?php e($form->hidden('num',array('value'=>'NOT','id'=>'num'))); ?>
	<?php
}?>

<script type="text/javascript">

var clickTrack = new Array(100);
var noOfAnswers = jQuery("#noOfAnswers").val();	
for (var i = 0; i < noOfAnswers; i++) clickTrack[i] = 0; 
var clickTrack1 = 0;

jQuery(document).ready(function(){

       var focus = jQuery('#focusJS').val();
       if(focus !=''){
       document.getElementById(focus).focus();     
       document.getElementById(focus).scrollIntoView();
       }	
	function updateCount ()
    {
        var qText = jQuery("#memberAnswerTextArea").val();
        jQuery("#noOfChar").html(2000 - qText.length);

       if(qText.length < 2000) {
           jQuery('#noOfChar').css('color','#000');
       } else {
           jQuery('#noOfChar').css('color','#F00');
       }
    }

    jQuery("#memberAnswerTextArea").keyup(function () {
        updateCount();
    });
    jQuery("#memberAnswerTextArea").keypress(function () {
        updateCount();
    });
    jQuery("#memberAnswerTextArea").keydown(function () {
        updateCount();
    });

    
	var num = jQuery("#num").val();	
	
	if(num !='NOT'){			
		jQuery(".set_0").show();
		jQuery("#countShow").val(0);	
	}
	
});

function doSomethingMouseOver(key){
	if(clickTrack[key] == 0){
		var id_div=document.getElementById("shareicon_"+key);
		var vis = id_div.style;
		vis.visibility='visible';
		vis.position='relative';
	}
}

function doSomethingMouseOut(key){
	if(clickTrack[key] == 0){
		var id_div=document.getElementById("shareicon_"+key);
		var vis = id_div.style;
		vis.visibility='hidden';
		vis.position='absolute';
	}
}

function doSomething(key)
{
	var id_div=document.getElementById("shareicon_"+key);
	var vis = id_div.style;
	if ( (vis.visibility=='visible' || vis.visibility=='hidden') && clickTrack[key] == 0)
	{
		clickTrack[key] = 1;
		vis.visibility='visible';
		vis.position='relative';
	}
	else if (vis.visibility=='visible' && clickTrack[key] == 1)
	{
		clickTrack[key] = 0;
		vis.visibility='hidden';
		vis.position='absolute';
		
	}
}

function doSomething1MouseOut(){
	if(clickTrack1 == 0){
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
		vis.visibility='hidden';
		vis.position='absolute';
	}
}

function doSomething1MouseOver(){
	if(clickTrack1 == 0){
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
		vis.visibility='visible';
		vis.position='relative';
	}
}

function doSomething1()
{
		//alert(clickTrack);	
	var id_div=document.getElementById("shareicon");
	var vis = id_div.style;
	if ( (vis.visibility=='visible' || vis.visibility=='hidden') && clickTrack1 == 0)
	{
		clickTrack1 = 1;
		vis.visibility='visible';
		vis.position='relative';
	}
	else if (vis.visibility=='visible' && clickTrack1 == 1)
	{
		clickTrack1 = 0;
		vis.visibility='hidden';
		vis.position='absolute';
		
	}
}

/*
jQuery("#pageNavPosition").click(function(){
	var num = parseInt(jQuery("#num").val(),10);	
	var countShow = parseInt(jQuery("#countShow").val(),10) + 1;
	//createCookie('cookie_countShow',countShow,0);
	
	if(num > countShow){			
		jQuery(".set_"+countShow).show();
		jQuery("#countShow").val(countShow);	
	}else if(num == countShow){
		jQuery(".set_"+countShow).show();
		jQuery("#countShow").val(countShow);
		jQuery("#pageNavPosition").hide();	
		jQuery("#openRegisterNav").hide();
	}
});
*/

jQuery("#previewButton").click(function(){

	var answerEntered = jQuery("#memberAnswerTextArea").val();
	answerEntered = answerEntered.replace(/  /gi, '&nbsp;&nbsp;');

	chars = jQuery("#memberAnswerTextArea").val().length;
	
	if(answerEntered == '' || chars > 2000){
		jQuery('#memberAnswerTextArea').css('border-color','#F00');
		jQuery('#memberAnswerTextArea').css('border-width','<?php e(ERROR_BORDER);?>px');
        jQuery('#memberAnswerTextArea').focus();
        return false;
	} else {
		jQuery('#memberAnswerTextArea').css('border-color',''); 
        jQuery('#memberAnswerTextArea').css('border-width','<?php e(NORMAL_BORDER);?>px');  
	}

	var num = parseInt(jQuery("#num").val(),10);

	if(num !='NOT'){	
		for ( var countShow = 0 ; countShow < num ; countShow++){
			jQuery(".set_"+countShow).show();
		}
	}
	
	jQuery("#answerPara").html(nl2br(answerEntered));
	jQuery("#answerTextBox").hide();
	jQuery("#pageNavPosition").hide();
	jQuery("#memberAnswer").show();

});

jQuery("#editAnswer").click(function(){

	jQuery("#answerTextBox").show();
	jQuery("#memberAnswer").hide();
});

jQuery("#submitButton").click(function(){

	jQuery("#AddAnswerform").submit();
	jQuery('#submitButton').attr("disabled", true);
});

function nl2br (str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br/>' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

</script>


<script type="text/javascript">
jQuery(document).ready(function(){

    function updateCount ()
    {
        var qText = jQuery("#questionTextBox").val();

       if(qText.length < 128) {
           jQuery("#noOfChar1").html(128 - qText.length);
       } else {
           jQuery("#noOfChar1").html(0);
           jQuery("#questionTextBox").val(qText.substring(0,128));
       }
    }

    jQuery("#questionTextBox").keyup(function () {
        updateCount();
    });
    jQuery("#questionTextBox").keypress(function () {
        updateCount();
    });
    jQuery("#questionTextBox").keydown(function () {
        updateCount();
    });

    jQuery("#applyPreview").click(function(){

    	var flag = 0;
    	if(jQuery.trim(jQuery("#questionTextBox").val()) == '') {
			jQuery('#questionTextBox').css('border-color','#F00');
			jQuery('#questionTextBox').focus();
			flag++;
		} else {
			jQuery('#questionTextBox').css('border-color',''); 
		}

    	var context = jQuery.trim(jQuery("#questionContextBox").val());
    	var words = countWords('questionContextBox');
    	if(context == '' /*|| words < 50*/) {
			jQuery('#questionContextBox').css('border-color','#F00');
			jQuery('#questionContextBox').focus();
			flag++;
		} else {
			jQuery('#questionContextBox').css('border-color','');
		}

		if(flag > 0) {
			return false;
		} else {
			jQuery("#Addform").submit();
			return true;
		}
		
    });

    jQuery("#questionTextBox").click(function(){

        jQuery("#questionContextDiv").show();
        jQuery("#applyPreviewButton").hide();
        
    });
    
    jQuery("#applyPreviewButton").click(function(){
        
    	jQuery("#questionContextDiv").show();
        jQuery("#applyPreviewButton").hide();
    });

    jQuery("#cancelLink").click(function(){
        
    	jQuery("#questionContextDiv").hide();
        jQuery("#applyPreviewButton").show();
        jQuery('#questionContextBox').css('border-color','');
        jQuery('#questionTextBox').css('border-color','');
        return false;
    });

    jQuery("#add_a_q").click(function() {
		jQuery('html, body').animate({
	        scrollTop: jQuery("#ask_an_expert").offset().top
	    }, 1000);
	});

    jQuery("#href_add").click(function() {
		jQuery('html, body').animate({
	        scrollTop: jQuery("#ask_an_expert").offset().top
	    }, 1000);
	    return false;
	});

     jQuery("#add_ans").click(function() {
		jQuery('html, body').animate({
	        scrollTop: jQuery("#addanswer").offset().top
	    }, 1000);
	});

    jQuery("#href_addans").click(function() {
		jQuery('html, body').animate({
	        scrollTop: jQuery("#addanswer").offset().top
	    }, 1000);
	    return false;
	});

});

function upVote(id){
	
	jQuery.ajax({
				url:SITE_URL+'qna/upvote/'+id,
				type:'post',
				dataType:'json',
				success:function(res){
					if(res.value == -1){
						loginpopup();
					} else
						jQuery('#button'+id).val('Upvote | ' + res.value);
				}
			});	
}
</script>
<script type="text/javascript">
mixpanel.track("Page View", {"URL": document.URL});
</script>
  

    
