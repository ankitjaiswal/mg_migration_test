	<div class="adminrightinner">
		<?php echo($this->Form->create('QnaQuestion', array('url' => array('controller' => 'qna', 'action' => 'admin_editquestion'), 'id'=>'qForm') ));?>
		<?php echo($this->Form->input('id'));?>
		<div class="tablewapper2 AdminForm">
			<table border="0" class="Admin2Table" width="100%">			   
				<tr>
					<td valign="middle" class="Padleft26"><?php echo "Id :".$res['QnaQuestion']['id']; ?></td>
				</tr>
				<tr>
					<td><?php echo($this->Form->input($modelName.'.question_text', array('div'=>false, 'label'=>false, "class" => "TextBox5" , "style" => "width: 800px;")));?></td>
				</tr>
				<tr>
					<td><?php echo($this->Form->textarea($modelName.'.question_context', array('rows'=>6,'div'=>true, 'label'=>false, 'id'=>'text-area' , "class" => "forminput", "style"=>"width:800px; height:100px;font-family: Ubuntu, arial, vardana; font-size: 14px;")));?></td>
				</tr>
				<?php
$options[-1] = '';
                    foreach ($categories as $value) {
	                    	$options[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
                    }?>
                    <tr>
						<table>
							<tr>
								<td style="width: 200px;">
									<?php 
										if(isset($res['QnaQuestionCategory'][0]))
											echo($this->Form->input("category1_id", array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"category1_id", "style"=>"width:160px; color: #000",'options'=>$options, 'value'=>$res['QnaQuestionCategory'][0]['topic_id'])));
										else
											echo($this->Form->input("category1_id", array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"category1_id", "style"=>"width:160px; color: #000",'options'=>$options))); ?>
								</td>
								<td style="width: 200px;">
									<?php 
										if(isset($res['QnaQuestionCategory'][1]))
											echo($this->Form->input("category2_id", array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"category2_id", "style"=>"width:160px; color: #000",'options'=>$options, 'value'=>$res['QnaQuestionCategory'][1]['topic_id'])));
										else
											echo($this->Form->input("category2_id", array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"category2_id", "style"=>"width:160px; color: #000",'options'=>$options))); ?>
								</td>
								<td style="width: 200px;">
									<?php 
										if(isset($res['QnaQuestionCategory'][2]))
											echo($this->Form->input("category3_id", array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"category3_id", "style"=>"width:160px; color: #000",'options'=>$options, 'value'=>$res['QnaQuestionCategory'][2]['topic_id'])));
										else
											echo($this->Form->input("category3_id", array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"category3_id", "style"=>"width:160px; color: #000",'options'=>$options))); ?>
								</td>
							</tr>
						</table>
					</tr>
					<tr>
						<td valign="middle" class="Padleft26">
							<?php echo "Categories added by user:"?>
							<?php  
	                            $cCount = count($res['QnaQuestionCategory']);
	                            foreach($res['QnaQuestionCategory'] as $key => $cValue) {
	                            	echo $options[$cValue['topic_id']];
	                            	
	                            	if($key != $cCount-1)
	                            		echo ",";
	                		}?>
						
						</td>
					</tr>
			</table>
		</div>
		<div class="buttonwapper">
			<div><input type="submit" value="Submit" class="submit_button" id="submit_button"/></div>
			<div class="cancel_button"><?php echo $this->Html->link("Cancel", "/admin/qna/question_review", array("title"=>"", "escape"=>false)); ?></div>
		</div>
		<?php  echo($this->Form->end());	?>	
		
		<div style="display: block; position: absolute; margin-top: 60px; background-color: #fff; width: 950px; height: 200px;">
			<?php echo "Members in these categories:"?>
			<?php 
			$cCount = count($membersAttached);
			$count = 0;
			foreach ($membersAttached as $key =>$val) {
			
				echo $val['UserReference']['first_name'].' '.$val['UserReference']['last_name'];
				
				if($count != $cCount-1)
					echo ",";
				$count++;
			}?>
		</div>
		
	</div>
	
<script type="text/javascript">
<!--//--><![CDATA[//><!--
    jQuery(document).ready(function()
    {
    	jQuery('#submit_button').click(function()
        {
    		jQuery(this).attr('disabled',true);
    		jQuery('#qForm').submit();
        });
    });
//--><!]]>
</script>

