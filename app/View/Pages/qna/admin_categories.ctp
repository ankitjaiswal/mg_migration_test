	<div class="adminrightinner" style="display: block;">
		<?php e($form->create('Qna_category', array('url' => array('controller' => 'qna', 'action' => 'addcategories'))));?>
		<div class="tablewapper2 AdminForm">
			<table border="0" class="Admin2Table" width="100%">			   
				<tr>
					<td>Parent Id</td>
					 <td><?php e($form->input('parent', array('div'=>false, 'label'=>false, "class" => "TextBox5")));?></td>
				</tr>
				<tr>
					<td>Category</td>
					<td><?php e($form->input('category', array('div'=>false, 'label'=>false, "class" => "TextBox5")));?></td>
				</tr>
				<tr>
					<td></td>
					<td>
					<div class="buttonwapper">
						<div><input type="submit" value="Submit" class="submit_button" /></div>
						<div class="cancel_button"><?php echo $html->link("Cancel", "/admin/qna/categories", array("title"=>"", "escape"=>false)); ?></div>
					</div>
					</td>
				</tr>
			</table>
			
		</div>
		<?php  e($form->end());	?>	
	</div>

	
	<div class="fieldset" style="display: block;">
    <div class="adminrightinner" style="padding:0px;">

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td width="5%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Id</td>
                        <td  width="5%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Parent Id</td>
                        <td  width="50%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Category</td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Assigned to</td>
                        <td align="center" valign="middle" class="Bdrrightbot Padtopbot6">Action</td>
                    </tr>
                    <?php 
                    if(!empty($data) && count($data)>0)
                    { 
                        foreach($data as $msg)
                        {    
                        
                        ?>
                        <tr>
                            <td width="5%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo $msg['Qna_category']['id']; ?></td>
                            <td  width="5%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $msg['Qna_category']['parent']; ?></td>
                            <td  width="50%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $msg['Qna_category']['category']; ?>  </td>
                            <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $msg['Qna_category']['assigned_to']; ?>  </td>
                            <td align="center" valign="middle" class="Bdrbot ActionIcon"><?php e($admin->getActionImage(array('edit'=>array('controller'=>'qna', 'action'=>'editcategories'),'delete' => array('controller' => 'qna', 'action' => 'deletecategories', 'token' => 'delete')), $msg['Qna_category']['id'])); ?></td>  
                        </tr>	
                   <?php
                        }
                    }else{ ?>
                   <tr>
                        <td colspan="3" width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Record not found</td>           
                    </tr>
                   <?php } ?>   
                </table>
            </div>
            <?php
     //   }
        ?>

    </div>
</div>
<div class="clr"></div>
<?php //echo $this->element('admin/admin_paging', array("paging_model_name" => "NewsLetter", "total_title" => "NewsLetter")); ?>	 