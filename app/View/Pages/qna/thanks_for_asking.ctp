<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div id="content-submit" class="thanksCENTER">
        	<h5>Thanks for asking!</h5>
        	<p>Your question has been submitted. You will receive an email as soon as an expert answers.</p>
        	<br/>
        	<div class="button-set">
        		<?php e($html->link("Back to Q&A",SITE_URL.'qanda',array('escape'=>false, 'style'=>"margin-right:20px; margin-top: 10px;" )));?>
				<input type="submit" style="margin-right:40px;" class="LTSpreview" value="Back to home" onclick="window.location.href='<?php echo SITE_URL?>'; " name="">
			</div>
	    </div>
    </div>
</div>
	
