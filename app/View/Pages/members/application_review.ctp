<!--------------content-wrapper-started------------->
<div id="inner-content-wrapper">
<div id="inner-content-box" class="pagewidth">
  <div class="inner_wrapper" style="min-height: 450px;">
	<div class="underline">

          <?php if($data['Mentee']['role_id'] == '3'){?>
	  <h1 style="font-size: 20px;">Consultation request from <a href="<?php echo SITE_URL."clients/my_account/".strtolower($data['Mentee']['url_key']);?>" target="_blank"><?php echo ucfirst(strtolower($data['UserReference']['first_name']))." ".ucfirst(strtolower($data['UserReference']['last_name']));?></a></h1>
	  <?php } else{?>
	  <h1 style="font-size: 20px;">Consultation request from <a href="<?php echo SITE_URL.strtolower($data['Mentee']['url_key']);?>" target="_blank"><?php echo ucfirst(strtolower($data['UserReference']['first_name']))." ".ucfirst(strtolower($data['UserReference']['last_name']));?></a></h1>
       <?php }?>  
       </div>
	<div class="">
	  <?php
	  	echo $form->create('Mentorship',array('url'=>array('controller'=>'members','action'=>'application_review'),'id'=>'applicationRequestForm'));
	 	echo $form->hidden('Mentorship.id',array('value'=>$data['Mentorship']['id']));
		echo $form->hidden('clickValue',array('value'=>'accept'));
	  ?>
	  <div>
	  <?php
	  if(!empty($data['Answer']) && count($data['Answer'])>0){?>
			<?php			
			foreach($data['Answer'] as $aswer)
			{
				?>				
				<p style="padding: 5px; color: #000; background: #B8B6B6;"><strong><?php e($aswer['question']); ?></strong></p>
				<p style="padding: 5px;"><label>&nbsp;Ans.&nbsp;&nbsp;<?php e($aswer['answer']); ?></label></p>
				<?php		
			}?>
		<?php
	 }else{?>
	 <p style="padding: 5px; color: #000; background: #B8B6B6;"><label>What do you need solved?</label></p>
		<p style="margin-top: 15px;">
		  <?php e($this->General->make_links($data['Mentorship']['mentee_need']));?>
		</p>
	<?php } ?>
		<div class="submit_bar" style="position:relative;height:65px;margin-top:40px;">
		
		<div class="submit_bar_right" style="float:right;margin-left: 20px;">
			<?php e($form->submit('Accept & schedule meeting',array('class'=>'btn','style'=>'width:200px;','onclick'=>'return changeValue("accept");'))); ?> 
		</div>
		<div class="submit_bar_right" style="float:right;padding-top:10px;">
			<?php //e($form->submit('Decline',array('class'=>'btn','style'=>'width:200px;','style'=>'width:200px;','onclick'=>'return changeValue("decline");'))); ?> 
			<a href="javascript:void(0);" onclick="return changeValue('decline');">Decline</a>
		</div>
		</div>
	  </div>
	  <?php e($form->end()); ?>
	</div>
  </div>
</div>
</div>
<script type="text/javascript">
	//validation
	function changeValue(type)
	{
		var flag=0;
		if(type=='accept'){
			jQuery('#MentorshipClickValue').attr('value','accept');
			flag++;
		}
		if(type=='decline'){
			if(confirm('Do you really want to Decline?')==true)
			{
				jQuery('#MentorshipClickValue').attr('value','decline');
				flag++;
			}
		}
		if(flag!=0){
		jQuery("#applicationRequestForm").submit();
		}
		else
			return false;
			
	}
</script>
