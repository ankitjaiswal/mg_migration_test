
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth" style="margin-bottom:50px;">
 	    <div id="user-account">
	      <div class="account-form">	    
		  	<div class="onbanner">
		          <h1>                 
		            <div style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;"><span style="text-decoration: underline;">Business Documents by Flevy</span></div></h1>
                        
				  <div style="position:relative;top:80px;"><p><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal;font-size: 18px;">Mentors Guild helps experienced consultants attract more clients.</span></div>	
			</div>

                     <br/><br/><br/><br/>
                    <div class ="expertise">
                    <h2 style="color:#333333;font-size: 18px;font-family:Ubuntu;font-weight: bold;">Business Documents by <img src ="<?php e(SITE_URL)?>/imgs/logo3.png" style="padding-bottom:-15px;"></h2>
                     </div>
                   <br/><br/>
                    <h2 style="color:#333333;font-size: 16px;font-family:Ubuntu;font-weight: bold;">Free documents</h2><br/>
    <ol class="premium-list">
        
        <li class=""><div class = "solTitle"><a href="<?php e(SITE_URL)?>/imgs/Strategy_Development_Execution_Primer.ppt" download id ="Strategy frameworks">Strategy frameworks</a></div></li>
        <li class=""><div class = "solTitle"><a href="<?php e(SITE_URL)?>/imgs/Powerpoint_toolkit.ppt"  download id="Powerpoint toolkit">Powerpoint toolkit</a></div></li>
        <li class=""><div class = "solTitle"><a href="<?php e(SITE_URL)?>/imgs/Operational_Excellence.pptx" download id="Operational Excellence">Operational Excellence</a></div></li>
    </ol>

<br/><br/>
             <div class ="expertise">        
             <h2 style="color:#333333;font-size: 18px;font-family:Ubuntu;font-weight: bold;">All documents on <a href="http://flevy.com/" target="_blank">Flevy.com</a> <span style="font-weight:normal;font-size: 14px;">(10% off for Regular Members and 15% off for Premium Members)</span></h2>
              <?php if($this->Session->read('Auth.User.role_id') == Configure::read('App.Role.Mentor') && $this->Session->read('Auth.User.mentor_type') != 'Premium Member')
              { ?>
            <p style="font-size:14px;">Your promo code: <a href="http://flevy.com/" target="_blank">REGMG10</a></p>
              <?php } elseif($this->Session->read('Auth.User.role_id') == Configure::read('App.Role.Mentor') && $this->Session->read('Auth.User.mentor_type') == 'Premium Member')
             {?>
            <p style="font-size:14px;">Your promo code: <a href="http://flevy.com/" target="_blank">PREMG15</a></p>
            <?php }else{?>
             
             <p style="font-size:14px;">Promo code available only for members</p>
            <?php }?>
            </div>




</div>
</div>
</div>
</div>


<?php e($html->css(array('plans'))); ?>

<style>
.premium-list {
  display: block;
  position: relative;
  left: 14px;
}
ol {
  margin: 0;
  padding-left: 4px;
}
ol li {
  padding: 0 0 10px 0;
  margin: 0;
  color: #292929;
}
</style>

<script type="text/javascript">
jQuery(document).ready(function(){


jQuery(".solTitle a").click(function(e){
e.preventDefault();
 var title = jQuery(this).attr('id');
 var url =jQuery(this).attr("href");
  
  var member = '<?php echo ($this->Session->read('Auth.User.id')) ;?>';
  
		       jQuery.ajax({
					url:SITE_URL+'members/flevy_email',
					type:'post',
					dataType:'json',
					data: 'title='+ title + '&member=' + member + '&url=' + url,
					success:function(url){
                                           window.location.href= url;
						//alert('Thanks for downloading - ' +title);
                                        
					}
				});	

 
});
});
</script>













