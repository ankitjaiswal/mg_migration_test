<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div id="content-submit" class="thanksCENTER">
        	<?php e($this->Form->create('user',array('url'=>array('controller'=>'members','action'=>'unsubscribe_qna'))));?>
        	<?php e($form->hidden('User.id',array('value'=>$id))); ?>
        	<div style="width: 960px; display: inline-block;">
	        	<p style="margin-top: 50px;">Answering user questions, and sharing your answers on social media, can be an effective tool to get new clients.</p>
	        	<p style="float: left; margin-left: 130px;">It is simple, quick and free.</p>
        	</div>
        	<div style="width: 960px; display: inline-block; margin-top: 50px;">
        		<p>Do you really want to unsubscribe?</p>
        	</div>
        	<p>
        		<label><input type="radio" name="Role" value="Yes" style="margin-right: 5px;" id="yesRadio"></input>Yes</label>
        		<label style="margin-left: 20px;"><input type="radio" name="Role" value="No" style="margin-right: 5px;" id="noRadio" checked></input>No</label>
        	</p>
        	<br/>
        	<div id ="app-button">
				<?php e($this->Form->submit('Done',array('id'=>'submit','style'=>'margin-right: 130px;')));?>
			</div>
			<?php e($this->Form->end()); ?> 
	    </div>
    </div>
</div>
