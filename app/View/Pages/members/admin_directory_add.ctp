<?php e($javascript->link(array('autocomplete/jquery.coolautosuggest')));?>
<?php e($html->css(array('autocomplete/jquery.coolautosuggest')));?>
<div class="adminrightinner">
	<?php e($form->create(Null, array('url' => array('controller' => 'members', 'action' => 'directory_add'),'type'=>'file')));?>     
	<div class="tablewapper2 AdminForm">
		<h3 class="legend1">Add Directory User</h3>
		<table border="0" class="Admin2Table" width="100%">
			<tr>
				<td valign="middle" class="Padleft26">Email as username <span class="input_required">*</span></td>
				<td><?php e($form->input('Directory_user.username', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">First Name <span class="input_required">*</span></td>
				<td><?php e($form->input('Directory_user.first_name', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>
				</td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Last  Name <span class="input_required">*</span></td>
				<td><?php e($form->input('Directory_user.last_name', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>
				</td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Linkedin URL <span class="input_required">*</span></td>
				<td><?php e($form->input('Directory_user.linkedin_url', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>
				</td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Website <span class="input_required">*</span></td>
				<td><?php e($form->input('Directory_user.website', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>
				</td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Area of expertise <span class="input_required">*</span></td>
				<td><?php e($form->input('Directory_user.area_of_expertise', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>
				</td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Quality<span class="input_required">*</span></td>
				<td><?php e($form->input('Directory_user.quality', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>
				</td>
			</tr>
		</table>
	</div>
	<div class="buttonwapper">
		<div><input type="submit" value="Submit" class="submit_button" /></div>
		<div class="cancel_button"><?php echo $html->link("Cancel", "/admin/members/directory_index/", array("title"=>"", "escape"=>false)); ?>
		</div>
	</div>
	<?php e($form->end()); ?>	
</div>
