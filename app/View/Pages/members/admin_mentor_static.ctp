<?php e($html->css(array('jquery/jquery-ui'))); ?>
<?php e($javascript->link(array('caljs/jquery-1.9.1','caljs/jquery-ui')));?> 
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery( "#startdate" ).datepicker();
        jQuery( "#enddate" ).datepicker();
    });		
</script>

<div class="adminrightinner">
    <div class="tablewapper2 AdminForm">
        <h3 class="legend1">Search</h3>
        <?php e($form->create('Mentor', array('url' => array('admin' => true, 'controller' => 'members', 'action' => 'mentor_static')))); ?>
        <table border="0" width="50%" class="Admin2Table">
            <tbody>
                <tr>
                    <td width="18%" valign="middle" class="Padleft26">Start date :</td>
                    <td><?php e($form->input('start', array('id'=>'startdate','label' => false, 'div' => false, 'class' => 'InputBox'))); ?>  </td>
                    <td width="18%" valign="middle" class="Padleft26">End date :</td>
                    <td><?php e($form->input('end', array('id'=>'enddate','label' => false, 'div' => false, 'class' => 'InputBox'))); ?>  </td>
                </tr>
                <tr>
                    <td valign="middle" class="Padleft26">&nbsp;</td>
                    <td align="left"> 

                        <?php e($form->submit('Search', array('div' => false, 'class' => 'submit_button'))); ?>                 
                    </td>
                </tr>              
            </tbody>
        </table> 
        <?php e($form->end()); ?>    
        <div style="clear: both;"></div>
    </div>
</div>

<div class="fieldset">
    <h3 class="legend">
		Member statistics
        <div class="total" style="float:right">
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <?php e($form->create('Mentor', array('name' => 'Admin', 'url' => array('controller' => 'mentor_static', 'action' => 'process')))); ?>    
        <input type="hidden" name="pageAction" id="pageAction"/>	 
        <?php $userName = $this->General->getUserReferenceData($data['userId']);
        $menterName = ucfirst($userName['UserReference']['first_name'])." ".strtolower($userName['UserReference']['last_name'])."(".$data['userId'].")";
            ?> 

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">User ID</td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">No. Of clients</td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">No. Of Sessions</td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Total Earnings</td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Mentorsguild Commission</td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Action</td>
                    </tr>
                    <tr>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo $menterName; ?></td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo $data['menteeCount']; ?></td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $data['sessionCount']; ?></td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">$<?php echo number_format($data['amount'],2); ?></td>
                         <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">$<?php echo number_format($data['commission'],2); ?></td>
                        <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo $html->link("view program", array('controller' => 'members', 'action' => 'mentor_programlist/'.$data['userId']), array("title" => "", "escape" => false)); ?></td>
                    </tr>	
                   
                </table>
            </div>
            <?php
     //   }
        ?>

    </div>
</div>
<div class="clr"></div>
<?php //echo $this->element('admin/admin_paging', array("paging_model_name" => "NewsLetter", "total_title" => "NewsLetter")); ?>	 