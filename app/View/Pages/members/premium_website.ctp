
<div class="premium-content one">
    <div class="pagewidth">
        <img src ="<?php e(SITE_URL)?>/imgs/stars.png">
        <h1>Premium Websites</h1>

		<h2>High-performance websites for business consultants.</h2>
    </div>
	      <div class="apply-button-search" style="margin-top:15px; ">	
              <input class="profilebtn" type="button" style="width:180px !important;  margin-top: 1px;" value="A Sample Website" onclick="window.open('http://judithmbardwick.com/')">
		</div>

</div>

<div class="premium-content two dark">
    <div class="pagewidth">
    <div class="float-l"><img src="<?php e(SITE_URL)?>/imgs/lighthouse.png"></div>
    <h3>Are you discoverable?</h3>
    <p>Make it easy for prospective clients to find you.</p>

    <ul class="premium-list">
        <li class="">Personalized website for your unique brand and offerings.</li>
        <li class="">Search Engine Optimization (SEO) to drive prospects.</li>
    </ul>
</div>
</div>

<div class="premium-content three">
    <div class="pagewidth">
		<div class="float-r"><img src="<?php e(SITE_URL)?>/imgs/gear.png"></div>
        <h3>Do visitors spend time on your website?</h3>
        <p>Simple, effective design for desktop and mobile viewing.</p>
        <ul class="premium-list">
            <li class="">Channel traffic from social media and online activities.</li>
            <li class="">Purposeful content layout to engage prospects.</li>
        </ul>
    </div>
</div>

<div class="premium-content four dark">
    <div class="pagewidth">
        <div class="float-l"><img src="<?php e(SITE_URL)?>/imgs/handshake.png"></div>
		<h3>Does your website drive conversions?</h3>
        <p>Fill your funnel and close more clients.</p>
        <ul class="premium-list">
            <li class="">Strong calls-to-action that lead to client engagements.</li>
            <li class="">Monthly on-call reviews and analytics-based improvements.</li>
        </ul>
    </div>
</div>

<div class="premium-content five">
    <div class="pagewidth">
	<h3 class="header">Feel the difference</h3>
        <p>By combining branding, design and analytics, we help you build a 24/7 conversion engine.</p>




           
	    <div class="mg_quote2_container" onclick="window.open('<?php e(SITE_URL);?>judy.bardwick','mywindow');" style="cursor: hand;">
		    <div class="mg_photo">
		        <img src="<?php e(SITE_URL)?>/imgs/Judy_Bardwick.png">
		    </div>
		    <div class="mg_quote_box">

			    <div class="premium-testimonial-content">
				<div class="premium-testimonial3-quote" >I am delighted and amazed by the work Mentors Guild has done for me. It is personal, accurate, honest and original. The website they created is client-focused, eye-catching, and simultaneously captures the essence of what I deliver to Clients. And, the speed of the team's responsiveness and their attention to every detail <span style="font-style:italic;">is the best I have ever experienced</span>.</div>
				<div class="premium-testimonial2-name" style="margin-top:-2px;">Judy Bardwick</div>
				<div class="premium-testimonial2-org">Leading Organizational Psychologist</div>
		    </div></div>
		</div>
  

               

<br/><br/>         
             <?php if($this->Session->read('Auth.User.role_id') == Configure::read('App.Role.Mentor') && $this->Session->read('Auth.User.premium_website') == 0)
              { ?> 
		<div class="mg-button"><a href="<?php e(SITE_URL)?>members/striperecurr/">Get your premium website</a></div>
             
              <?php } elseif($this->Session->read('Auth.User.role_id') == Configure::read('App.Role.Mentor') && $this->Session->read('Auth.User.premium_website') == 1)
             {?>
             <div class="mg-button"><a href="<?php e(SITE_URL)?>members/cancel_plan/">Cancel Your Premium Website</a></div>
             <?php }else{?>
             <div class="mgbutton">CALL 1-866-511-1898 FOR INFORMATION</div>
           <?php }?>        
 
           <div class="signup" style="margin-top: 50px"><p>Our price is <span class="emphasize">$145 per month</span> (no contract). Includes all upgrades for new web standards and devices.</p></div>
    </div>
</div>


<div class="six">
    <div class="pagewidth">

	</div>
	<div class="six">
        <div class="mg-link">Have questions or suggestions about Premium Website? Call us at 1-866-511-1898.</div>
        </div>
    </div>
</div>
<style>
.one {
	background: url(../imgs/city2.jpg) no-repeat fixed -270px 0px;
	background-color: #212121;
	text-align: center;
	color: #fff;
	padding-bottom: 270px;
	padding-top: 100px;
}
.mg-button {
	color: #fff;
	background-color: #f32C33;
	border-style: none;
	padding: 10px 10px 10px 10px;
	margin: auto;
	text-align: center;
	width: 100%;
	display:block;
	position:relative;
	right:36px;
	margin-bottom:10px;
}

.mg-button a {
	text-decoration: none;
	color: #fff;
	font-size: 26px;
	border-style: none;
	font-family: Ubuntu;font-weight: regular;
	padding: 10px 10px 10px 10px;
	margin: auto;
	text-align: center;
	text-transform: uppercase;
}
.apply-button-search {
    float: none;
    
    vertical-align: middle;
}
</style>