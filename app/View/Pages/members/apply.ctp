<?php 
$first = '';$last = '';$username = '';$summary = '';$socialId = '';$social='';$profile_url='';
if(isset($_SESSION['linkedinArr']))
{
	$first = ucfirst($this->Session->read('linkedinArr.first_name'));
	$last = ucfirst($this->Session->read('linkedinArr.last_name'));
	$username = $this->Session->read('linkedinArr.username');
	$summary = $this->Session->read('linkedinArr.summary');
	$social =  $this->Session->read('linkedinArr.socialId');
       $profile_url =  $this->Session->read('linkedinArr.profile_url');
	unset($_SESSION['linkedinArr']);
}

?>
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div id="user">
            <div id="pro-creation">
                <h1>Apply to be a member</h1>
            </div>
            <div id ="my-inform" style="padding-left:25px;">
                           
				<?php e($html->image('linked-in.png',array('alt'=>'Linkedin',"onclick"=>"linkedin('mentor');",'style'=>'cursor:pointer;margin-top:8px;'))); ?> 
            </div>  
            <?php e($form->create('User', array('url' => array('controller' => 'members', 'action' => 'apply'),'type'=>'file','id'=>'Appyform')));?>     
                <div id="name-box">                   
                    <div id="country">
                        <div class="floatL">
                            <?php e($form->input('UserReference.first_name', array('maxlengh'=>15,'id'=>'regFirstName','div'=>false, 'label'=>false, "class" => "forminput1", "placeholder" => "First Name",'value'=>$first)));?>
							<?php echo $form->hidden('socialId',array('value'=>$social));?>
							<span id="regNameErr" class="errormsg"></span>
						</div>
                        <div class="floatL mr-lt20">
                            <?php e($form->input("UserReference.last_name", array('maxlengh'=>15,'id'=>'regLastName',"type" => "text", "div" => false, "label" => false,"class" => "forminput1", "placeholder" => "Last Name",'value'=>$last))); ?>
							<span id="reglastNameErr" class="errormsg"></span>                       
						</div>
						<div class="clear"></div>
                    </div>
                    <div id="country">						
                        <?php e($form->input('User.username', array('div'=>false, 'label'=>false,'id'=>'regUserName', 'class' => 'forminput1', 'placeholder'=>'Email','value'=>$username)));?>
						<span id="regEmailErr" class="errormsg"></span>
				   </div>
                    <!--  <div id="upload-resume">
                        <p>Upload a document<span style="font-family:'Proximanova','Ubuntu'"> (Optional)</span></p>
                        <div class="upload">
                            <?php /*e($form->file('Resume1.resume_location', array("size" => 30, 'style' => 'height:25px;'))); */?>
							
					   </div>
					  <span id="resume_old"  class="errormsg"></span>  -->                      
                        <p>Background summary</p>		
                        <?php  e($form->input('UserReference.background_summary', array('div'=>false, 'label'=>false,"id"=>"text-area", "style" => "margin-bottom:30px;" ,"placeholder" => "Give a snapshot of  what you have been up to, highlighting all accomplishments. 400 words limit.",'value'=>$summary)));?>
                        <?php  e($form->input('Social.0.social_name', array('div'=>false, 'label'=>false,"id"=>"social1", "style" => "margin-bottom:30px; width:675px;" ,"placeholder" => "Linked In profile URL",'value'=>$profile_url)));?>
                        <?php  e($form->input('Social.1.social_name', array('div'=>false, 'label'=>false,"id"=>"social2", "style" => "margin-bottom:30px; width:675px;" ,"placeholder" => "Business website URL")));?>
  
                    </div>
                    <div id="country">
                        <?php  e($form->input('Communication.mode', array('div'=>false,'id'=>'regPhone', 'label'=>false, "class" => "forminput1", "style"=>"margin-bottom:2px", "size" => 30, "placeholder" => "Phone number")));?>		
						<span id="regphoneErr" class="errormsg"></span>
					</div>
			
                                 
                <div class="styled-form" style="padding-top:17px;">
                          
<input type="checkbox" name="checkbox" id="an"  placeholder=""  >                            
                            <label for="an"><span style="font-family: Ubuntu, arial, vardana;
font-size: 14px;">I agree to the <a href="http://mentorsguild.com/testdemo/fronts/terms_of_service" target="_blank">Terms of service</a> and the
                     <a href="http://mentorsguild.com/testdemo/fronts/privacy_policy"target="_blank">Privacy policy</a>.</span>
</label></div>

            

                    <div id ="app-button" style="padding-bottom: 45px;width: 700px;">						
                        <input type="button" value="Apply now" id="submitAppy" onclick='if(!this.form.checkbox.checked){ javascript:OpenCheckboxAlert();return false}' />
                    </div>		
                </div>
            <?php e($form->end()); ?>
        </div>
    </div>
</div>
<script type="text/javascript">
	//validation
	
	jQuery(document).ready(function(){
		//for Resume1.resume_location
		var flag1 = 0;
		jQuery("#Resume1ResumeLocation").change(function () {

			var uploadedFile=jQuery("#Resume1ResumeLocation").val();
			if(jQuery.trim(uploadedFile!='')) {	
			var resumeArr = uploadedFile.split(".");			
			var ext = ["doc", "docx", "ppt", "xlsx", "pdf" ]; // Creating Extension array 
				if(jQuery.inArray(resumeArr[1], ext)==-1){ // Checking value of extension
					jQuery('#ResumeResumeLocation').css('border-color','1px solid #F00');	
					jQuery("#resume_old").html("Upload only "+ext);	
					jQuery("#resume_old").css('color','#F00');
					flag1++;
				}else{
					jQuery('#ResumeResumeLocation').css('border-color');
					var iSize = (jQuery("#Resume1ResumeLocation")[0].files[0].size / 1024);				
					var res = ValidateFileSize(iSize,uploadedFile);
					if(!res){
						flag1++;
					}else{
						flag1 = 0;
					}
				}
			}else{
				flag1 = 0;
			}
									
		});
		jQuery("#submitAppy").click(function(){
			////other validation
			//for first name
			var flag = 0;
			var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;			
			var onlyLetters = /^[a-zA-Z _!@#"$%\^\-:.<>?=&'()*\+,\/;\[\\\]\^_`{|}~]+$/.test(jQuery.trim(jQuery("#regFirstName").val()));
			var lastNameonlyLetters = /^[a-zA-Z _!@#"$%\^\-:.<>?=&'()*\+,\/;\[\\\]\^_`{|}~]+$/.test(jQuery.trim(jQuery("#regLastName").val()));
			var onlyNumbers = /^[0-9-]*$/;
			if(jQuery.trim(jQuery("#regFirstName").val()) == '') {
				jQuery('#regFirstName').css('border-color','#F00');
				jQuery('#regNameErr').html('');	
				jQuery('#regFirstName').focus();				
				flag++;
			}else if(!onlyLetters){
				jQuery('#regFirstName').css('border-color','#F00');
				jQuery('#regNameErr').html('Only alphabet & special characters are required.');
				jQuery('#regFirstName').focus();
				flag++;
			}else {
				jQuery('#regFirstName').css('border-color','');
				jQuery('#regNameErr').html('');	
			}
                     // for check box
                   if(!jQuery("#an").is(':checked')){
                       jQuery('#an').html('You must agree to the terms first.');                        
                       jQuery('#an').css('border-color','#F00');
			   jQuery('#an').focus();	
				flag++;
                      }else {
                    jQuery('#an').css('border-color','');    
                   }
                   
                   /* if (jQuery('#an').is(':checked') != true){
                        jQuery('#an').css('border-color','#F00');
                        jQuery('#an').css('border-width','<?php e(ERROR_BORDER);?>px');
                        jQuery('#an').focus();  
                        
                      
                            flag++;
                    }*/
			//for last name
			if(jQuery.trim(jQuery('#regLastName').val()) == '') {
				jQuery('#regLastName').css('border-color','#F00');
				jQuery('#reglastNameErr').html('');	
				jQuery('#regLastName').focus();	
				flag++;
			}else if(!lastNameonlyLetters){
				jQuery('#regLastName').css('border-color','#F00');
				jQuery('#reglastNameErr').html('Only alphabet & special characters are requried.');
				jQuery('#regLastName').focus();
				flag++;
			} else {
				jQuery('#regLastName').css('border-color','');
				jQuery('#reglastNameErr').html('');	
			}
			
			/*if(jQuery.trim(jQuery('#regPhone').val()) !='' && !onlyNumbers.test(jQuery("#regPhone").val())){
                    jQuery('#regPhone').css('border-color','#F00');
                    jQuery('#regPhone').focus();               
                    flag++;             
                }else {
                    jQuery('#regPhone').css('border-color','');    
                }*/
			//for BackgroundSummary
			if(jQuery.trim(jQuery("#text-area").val()) == '') {
				jQuery('#text-area').css('border-color','#F00');
				jQuery('#text-area').focus();	
				flag++;
			} else {
				        words = countWords('text-area');
                         if(words>400)
                         {
                           jQuery('#text-area').css('border-color','#F00');
                            jQuery('#text-area').focus();   
                            flag++;  
                         }
                         else
                         {
                             jQuery('#text-area').css('border-color','');     
                         }	
			         }			
			/*var phoneVal = jQuery("#regPhone").val();
			var phoneRes = validatePhone(phoneVal);
			if(!phoneRes){
				flag++;
			}*/
			
			// for email
			if(jQuery.trim(jQuery('#regUserName').val()) == '') {
				jQuery('#regEmailErr').hide();	
				jQuery('#regUserName').css('border-color','#F00');			
				flag++;
			} else if(!jQuery('#regUserName').val().match(mailformat)) {
				jQuery('#regUserName').css('border-color','#F00');
				jQuery('#regEmailErr').html('Invalid Email Id');
				jQuery('#regEmailErr').css('color','#F00');
				jQuery('#regEmailErr').show();
				flag++;
			}  else {
				var email = jQuery('#regUserName').val();
				var URL = SITE_URL+'/users/checkEmail';
				jQuery.ajax({
					url:URL,
					type:'post',
					dataType:'json',
					data:'email='+email,
					success:function(res){
						if(parseInt(res.value)>0){
							
							jQuery('#regUserName').css('border-color','#F00');
							jQuery('#regEmailErr').html('Email already exists.');
							jQuery('#regEmailErr').css('color','#F00');
							jQuery('#regEmailErr').show();
							flag++;
						}else{
							jQuery('#regEmailErr').html('');
							jQuery('#regUserName').css('border-color','');
							jQuery('#regEmailErr').html('');
							jQuery('#regEmailErr').hide();
						}
						if(flag > 0 && flag1>0) {							
							return false;
						} else {
								if(flag ==0){
									jQuery("#Appyform").submit();
								}
								return true;
						}					
						
					}
				});				
			}		
		});		
		

		
		
		
	});
	function ValidateFileSize(iSize,Filename){
		var size=Math.round((iSize / 1024) * 100) / 100;
		if (size <2)
		{
					
			jQuery("#resume_old").html('');
			jQuery("#resume_old").css('color','')
			return true;
		}else{
			var ErrorMessage= iSize  + "kb is exceeded limit of file size";
			
			jQuery("#resume_old").html(ErrorMessage);
			jQuery("#resume_old").css('color','#F00')
			jQuery("#Resume1ResumeLocation").css('border','1px solid #F00');
			return false;
		}

	}
	function validatePhone(tel) {
		
		var re = /^\d{10}$/;
		
		if (!re.test(tel) && tel !=''){
			
			var ErrorMessage= "The phone number contains illegal characters.";
			
			jQuery("#regphoneErr").html(ErrorMessage);
			jQuery("#regphoneErr").css('color','#F00')
			jQuery("#regPhone").css('border','1px solid #F00');
			return false;		
			
		} else{
			
			var ErrorMessage= "";			
			jQuery("#regphoneErr").html(ErrorMessage);			
			jQuery("#regPhone").css('border','');
			return false;		
			
		}
		return true;
	}
	
             function OpenCheckboxAlert(){
 			var path=SITE_URL+'/members/checkbox_alert';
			TINY.box.show({url:path,width:400,height:140})
               }
	
</script>