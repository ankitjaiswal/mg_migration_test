<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div id="content-submit" class="thanksCENTER">
        	<?php e($this->Form->create('Directory_user',array('url'=>array('controller'=>'members','action'=>'unsubscribe'))));?>
        	<?php e($form->hidden('Directory_user.id',array('value'=>$id))); ?>
        	<h5>Do you really want to unsubscribe? Our "initial consultation request" is a free service!</h5>
        	<p>
        		<label><input type="radio" name="Role" value="Yes" style="margin-right: 5px;" id="yesRadio"></input>Yes</label>
        		<label style="margin-left: 20px;"><input type="radio" name="Role" value="No" style="margin-right: 5px;" id="noRadio" checked></input>No</label>
        	</p>
        	<br/>
        	<div id ="app-button">
				<?php e($this->Form->submit('Done',array('id'=>'submit','style'=>'margin-right: 200px;')));?>
			</div>
			<?php e($this->Form->end()); ?> 
	    </div>
    </div>
</div>
