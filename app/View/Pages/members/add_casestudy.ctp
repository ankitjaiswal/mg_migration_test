<?php e($html->css(array('opentip'))); ?>
<?php e($javascript->link(array('opentip-jquery')));?>

<div class="widthsize">
	<div class="margPAD">
		<form action="<?php echo SITE_URL ?>members/add_casestudy" method="post" id="Addform">
		<h2>Case Studies</h2>
		<div class="hr"></div>
		<div class="clear"></div>
		<div class="caseLEFT">
		
			<div class="expertise" style="margin-top: 15px;">
                <h1>Title <span style="float:right; font-weight: normal;">(<span id="noOfChar">128</span> characters remaining)</span> </h1>
                <p><input id="title" class="inpT" type="text" placeholder="Title" name="data[Case_Study][title]"/></p>
            </div>

            <div class="expertise">
                <h1>Client</h1>
                <p><input id="client_details" class="inpT" type="text" placeholder="Client name, or industry & size" name="data[Case_Study][client_details]" /></p>
            </div>
            
            <div class="expertise">
                <h1>Web reference</h1>
                <p><input id="url" class="inpT" type="text" placeholder="e.g. client website or link to more details" name="data[Case_Study][url]"/></p>
            </div>

            <div class="expertise">
                <h1>Situation <span style="float:right; font-weight: normal;">(<span id="noOfCharSituations">1250</span> characters remaining)</span> </h1>
                <p><textarea id="situation" style="width: 607px;" class="txtAR" placeholder="Tell a story your ideal prospects can relate to, in their language" name="data[Case_Study][situation]" ></textarea></p>
            </div>
            
            <div class="expertise">
                <h1>Actions <span style="float:right; font-weight: normal;">(<span id="noOfCharActions">1250</span> characters remaining)</span> </h1>
                <p><textarea id="actions" style="width: 607px;" class="txtAR" placeholder="Where did you come in and what did you do?" name="data[Case_Study][actions]" ></textarea></p>
            </div>
            
            <div class="expertise">
                <h1>Results <span style="float:right; font-weight: normal;">(<span id="noOfCharResults">1250</span> characters remaining)</span> </h1>
                <p><textarea id="results" style="width: 607px;" class="txtAR" placeholder="What was the outcome of your intervention?" name="data[Case_Study][result]"></textarea></p>
            </div>
			
			<div class="editSUB">
				<a href="<?php echo SITE_URL?>users/edit_account/OpenCaseStudiesTab:1">Cancel</a> 
				<input type="submit" value="Save & Preview" class="submitForm" style="width: 150px;">
			</div>					
		</div>
		<div class="caseRIGHT">
			<select onchange="sel(this.value)" style="font-size: 15px; font-family: Ubuntu; color:#2a2a2a; font-weight: bold;">
				<option value="testimonial" selected="selected">Testimonial</option>
				<option value="keyinsight">Key Insight</option>
			</select>			
			<textarea id="tex1" class="testimOPTION" placeholder="Quote from client" name="data[Case_Study][testimonial]"></textarea>
			<input id="tex2" class="testimOPTION" placeholder="e.g. Alan Wilson, CEO"  name="data[Case_Study][testimonial_by]" />
			<textarea id="tex3" class="testimOPTION" placeholder="e.g. Strategic plans cannot be executed unless mid-level managers 'own' them" name="data[Case_Study][insights]"></textarea>
		</div>
		
		<div class="clear"></div>
		</form>
	</div>	
</div>

<script type="text/javascript">
function sel(value)
{
	if(value=="testimonial")
	{
		document.getElementById('tex1').style.display="block";
		document.getElementById('tex2').style.display="block";
		document.getElementById('tex3').style.display="none";
	}
	else if(value=="keyinsight"){
		document.getElementById('tex1').style.display="none";
		document.getElementById('tex2').style.display="none";
		document.getElementById('tex3').style.display="block";
		}
}
jQuery(document).ready(function(){

	new Opentip("#title", "Keep it short & interesting (128 characters)<br/><br/>e.g. Driving Transformation in a Family Owned Business", { style: "myErrorStyle" } );
	new Opentip("#client_details", "e.g. $250 Million retail franchise", { style: "myErrorStyle" } );
	new Opentip("#url", "e.g. video or SlideShare", { style: "myErrorStyle" } );
	new Opentip("#situation", "Capture reader's interest (1250 characters)<br/><br/> - What does the client do? <br/> - What was the context/ environment?<br/> - What were the client needs?<br/> - What was the complexity?", { style: "myErrorStyle" } );
	new Opentip("#actions", "Get specific (1250 characters)<br/><br/> - Where did you begin?<br/> - What were the different steps/ milestones?<br/> - What actions did you take, for how long?<br/> - Show how external intervention helps<br/> - The 'solution' should not be obvious", { style: "myErrorStyle" } );
	new Opentip("#results", "Talk ROI (1250 characters)<br/><br/> - Use numbers or tangible benefits, if possible<br/> - Both short term and long term benefits<br/> - Finish your story", { style: "myErrorStyle" } );

	function updateCount ()
    {
        var qText = jQuery("#title").val();

       if(qText.length < 128) {
           jQuery("#noOfChar").html(128 - qText.length);
       } else {
           jQuery("#noOfChar").html(0);
           jQuery("#title").val(qText.substring(0,128));
       }
    }

    jQuery("#title").keyup(function () {
        updateCount();
    });
    jQuery("#title").keypress(function () {
        updateCount();
    });
    jQuery("#title").keydown(function () {
        updateCount();
    });


	function updateCountSituation()
    {
       var qText = jQuery("#situation").val();
       jQuery("#noOfCharSituations").html(1250 - qText.length);
        
       if(qText.length <= 1250) {
            jQuery('#noOfCharSituations').css('color','#000');
       } else {
            jQuery('#noOfCharSituations').css('color','#F00');
       }
    }

    jQuery("#situation").keyup(function () {
    	updateCountSituation();
    });
    jQuery("#situation").keypress(function () {
    	updateCountSituation();
    });
    jQuery("#situation").keydown(function () {
    	updateCountSituation();
    });


	function updateCountAction()
    {
       var qText = jQuery("#actions").val();
       jQuery("#noOfCharActions").html(1250 - qText.length);
        
       if(qText.length <= 1250) {
            jQuery('#noOfCharActions').css('color','#000');
       } else {
            jQuery('#noOfCharActions').css('color','#F00');
       }
    }

    jQuery("#actions").keyup(function () {
    	updateCountAction();
    });
    jQuery("#actions").keypress(function () {
    	updateCountAction();
    });
    jQuery("#actions").keydown(function () {
    	updateCountAction();
    });


	function updateCountResult()
    {
       var qText = jQuery("#results").val();
       jQuery("#noOfCharResults").html(1250 - qText.length);
        
       if(qText.length <= 1250) {
           jQuery('#noOfCharResults').css('color','#000');
       } else {
           jQuery('#noOfCharResults').css('color','#F00');
       }
    }

    jQuery("#results").keyup(function () {
    	updateCountResult();
    });
    jQuery("#results").keypress(function () {
    	updateCountResult();
    });
    jQuery("#results").keydown(function () {
    	updateCountResult();
    });
    
});

jQuery(".submitForm").click(function(){

	var goingID = '';
    var flag= 0;
    var url_reg  = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
    
	if(jQuery.trim(jQuery("#title").val()) == '') {
        jQuery('#title').css('border-color','#F00');
        jQuery('#title').css('border-width','<?php e(ERROR_BORDER);?>px');
        jQuery('#title').focus();
        goingID = 'title';
        //new Opentip("#title", "Do not leave blank", { style: "myErrorStyle" } );
        flag++;
    }else {
        if(jQuery("#title").val().length>128)
        {
           jQuery('#title').css('border-color','#F00');
           jQuery('#title').css('border-width','<?php e(ERROR_BORDER);?>px');
           if(goingID==''){ goingID = 'title';   }  
           flag++;  
        }
        else
        {
            jQuery('#title').css('border-color','');    
            jQuery('#title').css('border-width','<?php e(NORMAL_BORDER);?>px');
        }	
   }
	

	if(jQuery.trim(jQuery("#client_details").val()) == '') {
        jQuery('#client_details').css('border-color','#F00');
        jQuery('#client_details').css('border-width','<?php e(ERROR_BORDER);?>px');
        jQuery('#client_details').focus();
        goingID = 'client_details';
        //new Opentip("#client_details", "Do not leave blank", { style: "myErrorStyle" } );
        flag++;
    }/* else {
        words = countWords('client_details');
        if(words>15)
        {
           jQuery('#client_details').css('border-color','#F00');
           if(goingID==''){ goingID = 'client_details';   }  
           flag++;  
        }*/
        else
        {
            jQuery('#client_details').css('border-color','');     
            jQuery('#client_details').css('border-width','<?php e(NORMAL_BORDER);?>px');
        }	
   //}

	/*if(jQuery.trim(jQuery("#url").val()) == '') {
        jQuery('#url').css('border-color','#F00');
        jQuery('#url').focus();
        goingID = 'url';
        //new Opentip("#url", "Do not leave blank", { style: "myErrorStyle" } );
        flag++;
    }else */if(jQuery.trim(jQuery("#url").val()) != '' && !url_reg.test(jQuery('#url').val())) {
        jQuery('#url').css('border-color','#F00');
        jQuery('#url').css('border-width','<?php e(ERROR_BORDER);?>px');
        goingID = 'url';
        flag++;
    }else {
        jQuery('#url').css('border-color',''); 
        jQuery('#url').css('border-width','<?php e(NORMAL_BORDER);?>px');
    }

	if(jQuery.trim(jQuery("#situation").val()) == '') {
        jQuery('#situation').css('border-color','#F00');
        jQuery('#situation').css('border-width','<?php e(ERROR_BORDER);?>px');
        jQuery('#situation').focus();
        goingID = 'situation';
        //new Opentip("#situation", "Do not leave blank", { style: "myErrorStyle" } );
        flag++;
    }else {
    	chars = jQuery("#situation").val().length;
        if(chars>1250)
        {
           jQuery('#situation').css('border-color','#F00');
           jQuery('#situation').css('border-width','<?php e(ERROR_BORDER);?>px');
           if(goingID==''){ goingID = 'situation';   }  
           flag++;  
        }
        else
        {
            jQuery('#situation').css('border-color','');  
            jQuery('#situation').css('border-width','<?php e(NORMAL_BORDER);?>px'); 
        }	
   }

	if(jQuery.trim(jQuery("#actions").val()) == '') {
        jQuery('#actions').css('border-color','#F00');
        jQuery('#actions').css('border-width','<?php e(ERROR_BORDER);?>px');
        jQuery('#actions').focus();
        goingID = 'actions';
        //new Opentip("#actions", "Do not leave blank", { style: "myErrorStyle" } );
        flag++;
    }else {
    	chars = jQuery("#actions").val().length;
        if(chars>1250)
        {
           jQuery('#actions').css('border-color','#F00');
           jQuery('#actions').css('border-width','<?php e(ERROR_BORDER);?>px');
           if(goingID==''){ goingID = 'actions';   }  
           flag++;  
        }
        else
        {
            jQuery('#actions').css('border-color',''); 
            jQuery('#actions').css('border-width','<?php e(NORMAL_BORDER);?>px');
        }	
   }

	if(jQuery.trim(jQuery("#results").val()) == '') {
        jQuery('#results').css('border-color','#F00');
        jQuery('#results').css('border-width','<?php e(ERROR_BORDER);?>px');
        jQuery('#results').focus();
        goingID = 'results';
        //new Opentip("#results", "Do not leave blank", { style: "myErrorStyle" } );
        flag++;
    }else {
    	chars = jQuery("#results").val().length;
        if(chars>1250)
        {
           jQuery('#results').css('border-color','#F00');
           jQuery('#results').css('border-width','<?php e(ERROR_BORDER);?>px');
           if(goingID==''){ goingID = 'results';   }  
           flag++;  
        }
        else
        {
            jQuery('#results').css('border-color','');  
            jQuery('#results').css('border-width','<?php e(NORMAL_BORDER);?>px');  
        }	
   }

	 if(flag == 0){
		 jQuery("#Addform").submit();
     }else{
         if(goingID != ""){
             jQuery('#'+goingID).focus();
         }
         return false;
     }

	 
		 
});
</script>