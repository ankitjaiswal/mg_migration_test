<div class="fieldset">
	<h3 class="legend">
		Search Results
		<div class="total" style="float:right"> Total Members : <?php e($this->params["paging"]['User']["count"]);?>
		</div>
	</h3>
 <div class="adminrightinner" style="padding:0px;">
	   <input type="hidden" name="pageAction" id="pageAction"/>	 
       <?php
	   if(!empty($data)){
	   
	   $exPaginator->options = array('url' => $this->passedArgs);
       $paginator->options(array('url' => $this->passedArgs));?> 
	   
	<div class="tablewapper2">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
		<tr class="head">	
		<td width="30%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php e($exPaginator->sort('Name', 'UserReference.first_name'))?></td>
		<td width="30%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php e($exPaginator->sort('Email', 'User.username'))?></td>
		<td width="30%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php e($exPaginator->sort('Created Date', 'User.created'))?></td>
		<td width="10%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Status</td>
	 </tr>	
	 <?php
	   foreach($data as $value){		 
	 ?>
	 <tr>		
		<td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
		<?php e(ucwords($value['UserReference']['first_name']).' '.$value['UserReference']['last_name']);?></td>
		<td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
			<?php e($value['User']['username']);?>
		</td>
		<td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
			<?php e($value['User']['created']);?>
		</td>
		<td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
		<?php
			if($value['User']['id'] == $featuredDataResult[0]['FeaturedMentor']['user_id'] || $value['User']['id'] == $featuredDataResult[1]['FeaturedMentor']['user_id'] || $value['User']['id'] == $featuredDataResult[2]['FeaturedMentor']['user_id']){
				echo $html->image("active.png", array("title" => "Featured Mentor", "alt" => "Featured Mentor", "border" => 0));
			}else{
				echo $html->link($html->image("deactive.png", array("title" => "No featured Member", "alt" => "No featured Member", "border" => 0)),array('controller'=>'members','action'=>'featured',$value['User']['id']),array('escape'=>false));
				
			}
		?>
		</td>
	 </tr>	
	 <?php
	  }
	 ?>
	 </table>
	 </div>
	 <?php
	 }
	 ?>
		
</div>
</div>
<div class="clr"></div> 