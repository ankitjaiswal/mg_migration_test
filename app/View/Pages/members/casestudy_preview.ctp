<div class="widthsize">
	<div class="margPAD">
		<h2>Case Study</h2>
		<div class="hr"></div>
		<div class="clear"></div>
		<div class="caseLEFT">
					<?php if(isset($case_study_session['Case_Study']['url']) && $case_study_session['Case_Study']['url'] != '') {?>
					<h3>URL</h3>
					<div class="hr"></div>
					<p>
					<?php 
					$link = $case_study_session['Case_Study']['url'];
					
					$icon = $this->General->getIcon($link);
					e($html->link($html->image($icon,array('alt'=>$case_study_session['Case_Study']['url'] , 'height'=>'32', 'width'=>'32')),$link,array('escape'=>false , 'target'=>'_blank')));
						
					?>
			</p>
			<?php }?>
			<h3>Title</h3>
			<div class="hr"></div>
			<p>
				<?php 
				echo $case_study_session['Case_Study']['title']; ?>
			</p>

			<h3>Client</h3>
			<div class="hr"></div>
			<p>
				<?php echo $case_study_session['Case_Study']['client_details']; ?>
			</p>

			<h3>Situation</h3>
			<div class="hr"></div>
			<p>
				<?php $situation = str_replace('  ', '&nbsp;&nbsp;', $case_study_session['Case_Study']['situation']); 
					echo nl2br($situation);?>
			</p>

			<h3>Actions</h3>
			<div class="hr"></div>
			<p>
				<?php $actions = str_replace('  ', '&nbsp;&nbsp;', $case_study_session['Case_Study']['actions']); 
					echo nl2br($actions);?>
			</p>

			<h3>Results</h3>
			<div class="hr"></div>
			<p>
				<?php $result = str_replace('  ', '&nbsp;&nbsp;', $case_study_session['Case_Study']['result']); 
					echo nl2br($result);?>
			</p>
			<br/>
			<div class="editSUB">
				<a href="<?php echo SITE_URL?>members/edit_casestudy/<?php echo $case_study_session['Case_Study']['id']; ?>">Edit</a> 
				<input type="button" value="Publish" class="submitForm" onclick="window.location.href='<?php echo SITE_URL."members/edit_casestudy/".$case_study_session['Case_Study']['id']."/1"; ?>'; ">
			</div>
		</div>
		<?php if(trim($case_study_session['Case_Study']['testimonial']) != '') {?>
			<div class="caseRIGHT">
				<h3 style="font-size: 15px; font-weight: bold; font-family: Ubuntu;">Testimonial</h3>
				<div class="hr"></div>
				<p>
					<i><?php echo "\"".nl2br(strip_tags($case_study_session['Case_Study']['testimonial']))."\""; ?></i><br/><br/>
					By: <?php echo $case_study_session['Case_Study']['testimonial_by']; ?>
				</p>
			</div>
		<?php } else if(trim($case_study_session['Case_Study']['insights']) != '') {?>
			<div class="caseRIGHT">
				<h3 style="font-size: 15px; font-weight: bold; font-family: Ubuntu;">Key Insight</h3>
				<div class="hr"></div>
				<p>
					<i><?php echo "\"".nl2br(strip_tags($case_study_session['Case_Study']['insights']))."\""; ?></i>
				</p>
			</div>
		<?php }?>
		<div class="clear"></div>
	</div>	

</div>

