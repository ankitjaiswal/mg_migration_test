<?php e($javascript->link(array('fancybox/jquery.fancybox-1.3.4.pack', 'fancybox/jquery.mousewheel-3.0.4.pack'), false)); ?>
<?php e($html->css(array('jquery.fancybox-1.3.4')), false); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.addAttribute').fancybox();
    });		
</script>

<div class="adminrightinner">
    <div class="tablewapper2 AdminForm">
        <h3 class="legend1">Search</h3>
        <?php e($form->create('Directory_user', array('url' => array('admin' => true, 'controller' => 'members', 'action' => 'directory_index')))); ?>
        <table border="0" width="100%" class="Admin2Table">
            <tbody>
                <tr>
                    <td width="18%" valign="middle" class="Padleft26">Member's Name :</td>
                    <td><?php e($form->input('first_name', array('label' => false, 'div' => false, 'class' => 'InputBox'))); ?>  </td>
                    <td width="18%" valign="middle" class="Padright26">Member's Expertise :</td>
                    <td><?php e($form->input('area_of_expertise', array('label' => false, 'div' => false, 'class' => 'InputBox'))); ?>  </td>
       
         </tr>
                <tr>
                    <td valign="middle" class="Padleft26">&nbsp;</td>
                    <td align="left"> 

                        <?php e($form->submit('Search', array('div' => false, 'class' => 'submit_button'))); ?>                 
                    </td>
                    <td valign="middle" class="Padright26">&nbsp;</td>
                    <td align="right"> 

                        <?php e($form->submit('Search', array('div' => false, 'class' => 'submit_button'))); ?>                 
                    </td>

                </tr>              
            </tbody>
        </table> 
        <?php e($form->end()); ?>    
        <div style="clear: both;"></div>
    </div>
</div>

<div class="fieldset">
    <h3 class="legend">
		Directory Users
        <div class="total" style="float:right"> Total Members : <?php e($this->params["paging"]['Directory_user']["count"]); ?>
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <?php e($form->create('Directory_user', array('name' => 'Admin', 'url' => array('controller' => 'members', 'action' => 'process')))); ?>    
        <input type="hidden" name="pageAction" id="pageAction"/>	 


<?php
        if (!empty($data)) {
            $exPaginator->options = array('url' => $this->passedArgs);
            $paginator->options(array('url' => $this->passedArgs));
            ?> 

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td width="5%" align="center" valign="middle" class="Bdrrightbot Padtopbot6">
                            <input name="chkbox_n" id="chkbox_id" type="checkbox" value="" class="Chkbox" onclick="javascript:check_uncheck('Admin')" />
                        </td>	    

                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php e($exPaginator->sort('Name', 'Directory_user.first_name')) ?></td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php e($exPaginator->sort('Email', 'Directory_user.username')) ?></td>
                        <td width="30%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Autocomplete expertise</td>
						<td width="10%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Quality</td>
						<td width="10%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Send Email</td>
                        <td  width="15%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Action</td>
                    </tr>	
                    <?php
                    foreach ($data as $value) {
                        ?>
                        <tr>
                            <td align="center" valign="middle" class="Bdrrightbot Padtopbot6">
        <?php e($form->checkbox('Directory_user.' . $value['Directory_user']['id'], array("class" => "Chkbox", 'value' => $value['Directory_user']['id']))) ?>
                            </td>		

                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php e(ucwords($value['Directory_user']['first_name']) . ' ' . $value['Directory_user']['last_name']); ?></td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
        <?php e($value['Directory_user']['username']); ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php e($value['Directory_user']['area_of_expertise']); ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php e($value['Directory_user']['quality']); ?>
                            </td>
							<td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
								<?php e($value['Directory_user']['sendEmail']); ?>
							</td> 
                            <td align="center" valign="middle" class="Bdrbot ActionIcon">
        					<?php e($admin->getActionImage(array('edit' => array('controller' => 'members', 'action' => 'directory_edit'), 'delete' => array('controller' => 'members', 'action' => 'directory_delete', 'token' => $this->params['_Token']['key'])), $value['Directory_user']['id'])); ?>
                            </td>
                        </tr>	
                        <?php
                    }
                    ?>
                </table>
            </div>
            <?php
        }
        ?>
<?php if (!empty($data)) {
            e($form->end());
        } else {
            ?>
            <div style="color:blue, font-size:20; padding-bottom:30px;padding-top:30px;" align="center" ><strong>No Records Found.</strong></div>
        <?php } ?>
    </div>
</div>
<div class="clr"></div>
<?php echo $this->element('admin/admin_paging', array("paging_model_name" => "Directory_user", "total_title" => "User")); ?>	 