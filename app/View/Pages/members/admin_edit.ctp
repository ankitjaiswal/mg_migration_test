<?php e($javascript->link(array('autocomplete/jquery.coolautosuggest')));?>
<?php e($html->css(array('autocomplete/jquery.coolautosuggest')));?>
	<div class="adminrightinner">
		<?php 
		e($form->create('User', array('url' => array('controller' => 'members', 'action' => 'edit'),'type'=>'file')));
		e($form->hidden('User.id'));
		e($form->hidden('UserReference.id'));
		e($form->hidden('Availablity.id'));
		e($form->hidden('UserImage.0.id'));
		e($form->hidden('UserImage.0.image_name'));
		
		if(count($this->data['Communication']) >0){
			foreach($this->data['Communication'] as $key=>$value){
				if(isset($value['id']) && $value['id'] !=''){
					e($form->hidden('Communication.'.$key.'.id'));
				}
			}	
		}
		if(count($this->data['Question']) >0){
			foreach($this->data['Question'] as $key=>$value){
				if(isset($value['id']) && $value['id'] !=''){
					e($form->hidden('Question.'.$key.'.id'));
				}
			}	
		}	
		if(count($this->data['Source']) >0){
			foreach($this->data['Source'] as $key=>$value){
				if(isset($value['id']) && $value['id'] !=''){
					e($form->hidden('Source.'.$key.'.id'));
				}
			}	
		}		
		if(count($this->data['Social']) >0){
			foreach($this->data['Social'] as $key=>$value){
				if(isset($value['id']) && $value['id'] !=''){
					e($form->hidden('Social.'.$key.'.id'));
				}
			}	
		}
		
		?>    
		<div class="tablewapper2 AdminForm">
			<h3 class="legend1">Edit Member  </h3>
			<?php e($this->element('mentorsuser/form'));?>
		</div>
		<div class="buttonwapper">
			<div>
				<input type="submit" value="Submit" class="submit_button" />
			</div>
			<div class="cancel_button">
				<?php echo $html->link("Cancel", "/admin/members/search/", array("title"=>"", "escape"=>false)); ?>
			</div>
		</div>
		<?php e($form->end()); ?>
		
	</div>

