<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="fieldset">
    <h3 class="legend">
		List
        <div class="total" style="float:right">Total Pending Members : <?php e($this->params["paging"]['User']["count"]); ?>
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <input type="hidden" name="pageAction" id="pageAction"/>	 
        <?php
        if (!empty($data)) {

            $exPaginator->options = array('url' => $this->passedArgs);
            $paginator->options(array('url' => $this->passedArgs));
            ?> 

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">	
                        <td width="30%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php e($exPaginator->sort('Name', 'UserReference.first_name')) ?></td>
                        <td width="30%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php e($exPaginator->sort('Email', 'User.username')) ?></td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php e($exPaginator->sort('Created Date', 'User.created')) ?></td>
						<td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php e($exPaginator->sort('Status', 'User.is_approved')) ?></td>                       
					   <td width="10%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Action</td>
                    </tr>	
                    <?php
                    foreach ($data as $value) {
                        ?>
                        <tr>		
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php echo $html->link(ucwords($value['UserReference']['first_name']) . ' ' . $value['UserReference']['last_name'], array('controller'=>'members','action'=>'detail',$value['User']['id']),array('escape'=>false, "style" => "text-decoration:underline;")); ?></td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php e($value['User']['username']); ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php e($value['User']['created']); ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php 
								if($value['User']['is_approved'] ==1){
									e('Approved');
								}elseif($value['User']['is_approved'] ==2){
									
									e('Declined');
								}elseif($value['User']['is_approved'] ==0){
									e('Pending');
								}else{
									e('Undefined');
								}								
								?>
                            </td>							
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php
                                    echo $html->link($html->image("MailIcon.png", array("title" => "Accept Member Request", "alt" => "mail", "border" => 0)),array('controller'=>'members','action'=>'activateMentor',$value['User']['id']),array('escape'=>false));
                                    if($value['User']['is_approved'] !=2){
										echo $html->link($html->image("erase01.png", array("title" => "Decline Member Request", "alt" => "mail", "border" => 0)),array('controller'=>'members','action'=>'declineMentor',$value['User']['id']),array('escape'=>false));
                					}
									e($this->Html->link($this->Html->image('/img/admin/trash_icon.png', array('class'=>'viewstatusimg1','title'=>'Delete user','alt'=>'delete','width'=>'18','height'=>'18','style'=>'margin:0 0 2px 4px;')), array('controller'=>'members','action'=>'delete',$value['User']['id']),array('class'=>'deleteItem', 'escape'=>false)));	
				                ?>
                            </td>
                        </tr>	
                        <?php
                    }
                    ?>
                </table>
            </div>
            <?php
        }else{?>
			No Records Found
			<?php
		}?>

    </div>
</div>
<div class="clr"></div> 