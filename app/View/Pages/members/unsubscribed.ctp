<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div id="content-submit" class="thanksCENTER">
        	<p style="margin-top: 100px;">You have been unsubscribed.<p>
        	<p>To re-subscribe go to your Account settings, and select <span style="font-weight: bold;">User questions.</span><p>
        	<br/>
        	<div id ="app-button">
				<input type="submit" value="Done" id="submit" onclick="window.location.href='<?php e(SITE_URL);?>'" style="margin-right: 250px;"/>
			</div>
	    </div>
    </div>
</div>
