
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:title" content=" Takeaway | Business Consulting | Mentors Guild">
        <meta name="description" property="og:description" content="Embed takeaway into the articles you share.  Promote yourself as you share Forbes, TechCrunch, Mashable, etc.">
        <meta name="keywords" content="Takeaway">
        <meta property="og:image" content="http://www.mentorsguild.com/img/media/Final-Logo.png">
        <meta property="og:site_name" content="Mentors Guild">
        <meta property="og:type" content="website">
        <meta name="language" content="english">
         
        <link rel="stylesheet" href="<?php e(SITE_URL)?>css/normalize.min.css">
        <link rel="stylesheet" href="<?php e(SITE_URL)?>css/mainX.css">

        <script src="<?php e(SITE_URL)?>js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

        <title>Takeaway | Business Consulting | Mentors Guild </title>

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
     <?php 
             $link = $this->Session->read('link');
              $this->Session->delete('link');
               
             $comment = $this->Session->read('comment');
              $this->Session->delete('comment');

             $title = $this->Session->read('title');
              $this->Session->delete('title');

             $url = $this->Session->read('url');
             $this->Session->delete('url');

             $checkbox = $this->Session->read('checkbox');
             $this->Session->delete('checkbox');

             $destinationurl = $this->Session->read('destinationurl');
             $this->Session->delete('destinationurl');

					if(($pos =strpos($destinationurl,'http'))!==false)
						{
							$websitelink=$destinationurl;
						}
						else
						{
							$websitelink="https://".$destinationurl;
						}
         ?>
          
        <iframe src="<?php echo $link;?>"></iframe>



<?php if($destinationurl != "NULL"){?>
        <div class="footer-container">

            <footer class="wrapper" style="font-family:Ubuntu;font-weight:normal;  display: none;"  id ="text">
             <h3>
                    <a href="<?php echo (SITE_URL);?>" ><img src ="<?php e(SITE_URL)?>imgs/mg_monogram.png" style="width:50px; height:50px;margin-top:-4px;margin-right:10px; float: left;"></a>
                              <?php if($checkbox == "Yes"){?>
                    <div style="text-align:center;font-weight:normal;margin-top:-12px;">
                   <input type="hidden" name="Language" value="<?php echo($url)?>" id="url">
                   <input type="hidden" name="Language" value="<?php echo($websitelink)?>" id="link">  
                 <style="font-family:Ubuntu;font-size:16px;"><?php echo $comment;?></span>&nbsp;&nbsp;<input id="prospect_client" class="inpT" type="text" style="width:170px !important;height:30px;color:#000;" placeholder="email@company.com" name="data[Case_Study][client_details]" onfocus="this.placeholder=''" onblur="this.placeholder='email@company.com'" /> &nbsp;&nbsp;<input class="create_btn" id="prospect" type="button" style="width:150px !important;height:40px;font-size:16px;background:none repeat scroll 0 0 #F32C33;color:#FFFFFF;cursor:pointer;height:40px;text-align:center;" value="Learn more" onclick='save_db();'>
<?php }else{?>
                    <div  style="text-align:center;font-weight:normal;margin-top:-12px;">
<style="font-family:Ubuntu;font-size:16px;"><?php echo $comment;?></span>  &nbsp;&nbsp;<input class="create_btn" type="button" style="width:150px !important;height:40px;font-size:16px;background:none repeat scroll 0 0 #F32C33;color:#FFFFFF;cursor:pointer;height:40px;text-align:center;" value="Learn more" onclick='window.open("<?php echo ($websitelink);?>");'>

<?php }?>
</div>
                </h3>
            </footer>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php e(SITE_URL)?>js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="<?php e(SITE_URL)?>js/main.js"></script>
<div id="popup">
   <div id="boxes">
  <div id="dialog" class="window">
    <b>Share this</b>
          <div class="QHINTarea">
         <p><textarea id="link"  type="text" style="margin-top: 5px;width:96%; height: 75px;font-size:17px;background-color : #CCCCCC;" placeholder="http://bit.ly"  value=""/><?php echo ($title);?> <?php echo ($url);?></textarea></p>

        </div>

			<div class="share_icons" onmouseout="doSomethingMouseOut();"  onmouseover="doSomethingMouseOver()" id = "share_icon" style="width: 220px; float: right;">
			<div class="editSUB" style="float:right; padding-right: 11px;" >
                      <input class="create_btn" type="button" style="width:77px !important;height:25px;" value="Share" onclick='javascript:doSomething();',onmouseover = 'doSomethingMouseOver();'>

			
			</div>

						
<div id="shareicon" style="float:right;visibility:hidden;">						

                               
                                <input type="hidden" name="Language" value="<?php echo($completelink)?>" id="completelinkJS">
                                <input type="hidden" name="Language" value="<?php echo($compTitle)?>" id="compTitleJS">

                                <a href="https://plus.google.com/share?url=<?php echo ($url);?>&title=@MentorsGuild: <?php echo ($title);?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php e(SITE_URL)?>img/images/g_plus_icon.png" alt="Google" height="25" width="27" id="google_share" ></a>
                                <a href="http://www.facebook.com/sharer.php?s=100&p[url]=<?php echo ($url);?>&p[title]=<?php echo ($title);?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php e(SITE_URL)?>img/images/fb_icon.png" alt="Facebook" height="25" width="27" id="facebook_share"></a>
                                <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo ($url);?>&title=<?php echo ($title);?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php e(SITE_URL)?>img/images/logo_linkedin.png" alt="LinkedIn" height="25" width="27" id="linkedin_share"></a>
                                <a href="http://twitter.com/intent/tweet?text=<?php echo ($title);?> <?php echo ($url);?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php e(SITE_URL)?>img/images/twit_icon.png" alt="twitter" height="25" width="27" id="twitter_share" style="margin-right: 7px;"></a>




			</div>			
			</div>


  </div>

  <div id="mask"></div>
 </div>
</div>

<?php } else{?>

        <div class="footer-container">
            <footer class="wrapper" style="font-family:Ubuntu;font-weight:normal; display: none;"  id ="text">
             <h3>
              <?php 
					  	$urlk1 = ($this->data['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1); 
                                           if($this->data['UserReference']['business_website'] !=''){
                                                $link = $this->data['UserReference']['business_website'];  
                                                $consultation_link = SITE_URL."fronts/consultation_request/".$urlk1; 
                                           } else{
                                              $link = SITE_URL.strtolower($urlk1);
                                              $consultation_link = SITE_URL."fronts/consultation_request/".$urlk1;
                                            }?>

                                             
					 <?php 
					 if(!empty($this->data['UserImage'])){
						if(file_exists(WWW_ROOT . 'img' . '/' .MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'])){ 
							e($html->link($html->image(MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'], array('class'=>'desaturate','alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'],'style'=>'width:50px; height:50px;margin-top:-15px;margin-left:-8px;margin-right:10px; background-color:#555555; float: left; display: block;')),$displink1,array('escape'=>false,'target'=>'_blank')));       
                					
						}else{				
							e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'],'style'=>'width:50px; height:50px;margin-top:-15px;margin-left:-8px;margin-right:10px; background-color:#555555; float: left; display: block;')),$displink1,array('escape'=>false)));  
						}
						
					}else{
						e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'],'style'=>'width:50px; height:50px;margin-top:-15px;margin-left:-8px;margin-right:10px; background-color:#555555; float: left; display: block;')),$displink1,array('escape'=>false)));  

					}
					?><a href ="<?php e($link);?>"style="text-decoration:none;color:#d03135;font-weight: normal;margin-top:65px;" target="_blank" ><?php e(ucfirst($this->data['UserReference']['first_name']));?> <?php e($this->data['UserReference']['last_name']); ?></a>
                 <?php if($checkbox == "Yes"){?>
                 <div style="text-align:center;font-weight:normal;margin-top:-32px;">
                   <input type="hidden" name="Language" value="<?php echo($this->data['User']['id'])?>" id="member_detail">
                   <input type="hidden" name="Language" value="<?php echo($url)?>" id="url">
                   <input type="hidden" name="Language" value="<?php echo($consultation_link)?>" id="link">
                 <style="font-family:Ubuntu;font-weight: normal;"><?php echo $comment;?></span>&nbsp;&nbsp; <input id="prospect_client" class="inpT" type="text" style="width:170px !important;height:30px;color:#000; " placeholder="email@company.com" name="data[Case_Study][client_details]" onfocus="this.placeholder=''" onblur="this.placeholder='email@company.com'" />&nbsp;&nbsp; <input class="create_btn" id="prospect" type="button" style="width:150px !important;height:40px;font-size:16px;background:none repeat scroll 0 0 #F32C33;color:#FFFFFF;cursor:pointer;height:40px;text-align:center;" value="Learn more" onclick='save_db();'> </div>
                <?php }else{?>
                 <div style="text-align:center;font-weight:normal;margin-top:-32px;">

                 <style="font-family:Ubuntu;font-weight: normal;"><?php echo $comment;?></span>&nbsp;&nbsp; <input class="create_btn" id="without_prospect" type="button" style="width:150px !important;height:40px;font-size:16px;background:none repeat scroll 0 0 #F32C33;color:#FFFFFF;cursor:pointer;height:40px;text-align:center;" value="Learn more" onclick='window.open("<?php echo ($consultation_link);?>");'> </div>

             <?php }?>
             </h3>
            </footer>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="<?php e(SITE_URL)?>js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

        <script src="<?php e(SITE_URL)?>js/main.js"></script>
<div id="popup">
   <div id="boxes">
  <div id="dialog" class="window">
    <b>Share this</b>
          <div class="QHINTarea">
         <p><textarea id="link"  type="text" style="margin-top: 5px;width:96%; height: 75px;font-size:17px;background-color : #CCCCCC;" placeholder="http://bit.ly"  value=""/><?php echo ($title);?> <?php echo ($url);?></textarea></p>

        </div>

			<div class="share_icons" onmouseout="doSomethingMouseOut();"  onmouseover="doSomethingMouseOver()" id = "share_icon" style="width: 220px; float: right;">
			<div class="editSUB" style="float:right; padding-right: 11px;" >
                      <input class="create_btn" type="button" style="width:77px !important;height:25px;" value="Share" onclick='javascript:doSomething();',onmouseover = 'doSomethingMouseOver();'>

			
			</div>

						
<div id="shareicon" style="float:right;visibility:hidden;">						

                               
                                <input type="hidden" name="Language" value="<?php echo($completelink)?>" id="completelinkJS">
                                <input type="hidden" name="Language" value="<?php echo($compTitle)?>" id="compTitleJS">

                                <a href="https://plus.google.com/share?url=<?php echo ($url);?>&title=@MentorsGuild: <?php echo ($title);?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php e(SITE_URL)?>img/images/g_plus_icon.png" alt="Google" height="25" width="27" id="google_share" ></a>
                                <a href="http://www.facebook.com/sharer.php?s=100&p[url]=<?php echo ($url);?>&p[title]=<?php echo ($title);?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php e(SITE_URL)?>img/images/fb_icon.png" alt="Facebook" height="25" width="27" id="facebook_share"></a>
                                <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo ($url);?>&title=<?php echo ($title);?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php e(SITE_URL)?>img/images/logo_linkedin.png" alt="LinkedIn" height="25" width="27" id="linkedin_share"></a>
                                <a href="http://twitter.com/intent/tweet?text=<?php echo ($title);?> <?php echo ($url);?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php e(SITE_URL)?>img/images/twit_icon.png" alt="twitter" height="25" width="27" id="twitter_share" style="margin-right: 7px;"></a>




			</div>			
			</div>


  </div>

  <div id="mask"></div>
 </div>
</div>
<?php }?>

    </body>

<style>

input{margin:0;padding:0; border:0;}
.inpT{border:1px solid #e6e7e8;text-align:center;}
input,textarea{font-family:Ubuntu,arial,vardana;font-size:14px;}
.editSUB input{background:none repeat scroll 0 0 #F32C33;color:#FFFFFF;cursor:pointer;height:40px;text-align:center;}
.SUB input:hover{background:#9E0606;}

#mask {
  position: absolute;
  left: 0;
  top: 0;
  z-index: 9000;
  background-color: #000;
  display: none;
  opacity: 0.7;
}

#boxes .window {
  position: absolute;
  left: 0;
  top: 0;
  width: 440px;
  height: 200px;
  display: none;
  z-index:9999;
  padding: 20px;
  border-radius: 5px;
  text-align: center;
}

#boxes #dialog {
  width: 550px;
  height: 200px;
  padding: 10px;
  background-color: #eee;
  border-bottom:1px solid #333;
  font-family: 'Segoe UI Light', sans-serif;
  font-size: 15pt;
}

</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script> 

<script type="text/javascript">
$(document).ready(function() {	
$('#text').delay(7000).fadeIn(400);
var id = '#dialog';
	
//Get the screen height and width
var maskHeight = $(document).height();
var maskWidth = $(window).width();
	
//Set heigth and width to mask to fill up the whole screen
$('#mask').css({'width':maskWidth,'height':maskHeight});

//transition effect
setTimeout(function(){$('#mask').fadeIn(1000);}, 45000);	
setTimeout(function(){$('#mask').fadeTo("fast",0.7);}, 45000);	
	
//Get the window height and width
var winH = $(window).height();
var winW = $(window).width();
              
//Set the popup window to center
$(id).css('top',  winH/2-$(id).height()/2);
$(id).css('left', winW/2-$(id).width()/2);
	
//transition effect
setTimeout(function(){$(id).fadeIn(2000);}, 45000);	
	
//if close button is clicked
$('.window .close').click(function (e) {
//Cancel the link behavior
e.preventDefault();

$('#mask').hide();
$('.window').hide();
});

//if mask is clicked
$('#mask').click(function () {
$(this).hide();
$('.window').hide();
});
	
});
</script>
<script>
var clickTrack = 0;
function doSomethingMouseOver(){
	if(clickTrack == 0){
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
		vis.visibility='visible';
		vis.position='relative';
	}
}

function doSomethingMouseOut(){
	if(clickTrack == 0){
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
		vis.visibility='hidden';
		vis.position='absolute';
	}
}

function doSomething()
{
		
		//alert(clickTrack);	
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
	if ( (vis.visibility=='visible' || vis.visibility=='hidden') && clickTrack == 0)
	{
		clickTrack = 1;
		vis.visibility='visible';
		vis.position='relative';
	}
	else if (vis.visibility=='visible' && clickTrack == 1)
	{
		clickTrack = 0;
		vis.visibility='hidden';
		vis.position='absolute';
		
	}
}
</script>
<script type="text/javascript">
   	function save_db(){
		var url = jQuery("#url").val();
              var member_detail = jQuery("#member_detail").val();
              var link = jQuery("#link").val();
              var client = jQuery("#prospect_client").val();
              
		jQuery(document).ready(function(){
			jQuery.ajax({
				url:'http://www.mentorsguild.com/test/members/prospect_client',
				type:'post',
				dataType:'json',
				data:'url='+ url + '&link=' + link + '&client=' + client +'&member_detail=' +member_detail,
				success:function(res){
					if(res.title !=''){
						 

                                          window.open(link, '_blank');
                                          
                                          
					}else{
						alert("Failure!!!");
					}
				}
			});
		});
		

		
   	}



 </script>