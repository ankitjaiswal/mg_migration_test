 <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
     
        <div class="Profile_page_container">
        <div id="pro-creation">
                <h1>Invoice creation</h1>
            </div>
          <div class="figure-left align-left-botttom "> 
		  
		  <div id="profileImage" class="profileimg">
			<img src="<?php echo SITE_URL?>img/media/profile.png" alt="bill-kenny">            
            	
			</div>
			
			
			
            <div class="figcaption">
 			<h1><span>EG sebastian</span></h1>
				<p>San Francisco, <br />SA</p>	
            </div>
          </div>
		  <div class="mentor_details mr-bt20" style="margin-top:0px; float:none;">
			<div id="mentor-detail-left1">
				
				
					
					
					<div id="edit_box" style="display:none;">
					<div class="edit_row">
						<div class="edit_col1">
												<input name="link[]" id="link[]" style="height:20px;" maxlength="64" type="text">
						<a href="javascript:act(0);"><img src="Edit%20Profile%20_%20MentorsGuild_files/2-11-09delete.jpg" alt="skype" class="padding_right"></a>
						</div>
						<div class="edit_col2">
												<input name="link[]" id="link[]" style="height:20px;" maxlength="64" type="text">							<a href="javascript:act(1);"><img src="Edit%20Profile%20_%20MentorsGuild_files/2-11-09delete.jpg" alt="skype"></a>
						</div>
					</div>
					
					<div class="edit_row">
						<div class="edit_col1">
													<input name="link[]" id="link[]" style="height:20px;" maxlength="64" type="text">							<a href="javascript:act(2);"><img src="Edit%20Profile%20_%20MentorsGuild_files/2-11-09delete.jpg" alt="skype" class="padding_right"></a>
						</div>
						<div class="edit_col2">
														<input name="link[]" id="link[]" style="height:20px;" maxlength="64" type="text">							
							<a href="javascript:act(3);"><img src="Edit%20Profile%20_%20MentorsGuild_files/2-11-09delete.jpg" alt="skype"></a>
						</div>
					</div>
					
					
					<div class="edit_row">
						<div class="edit_col1">
													<input name="link[]" id="link[]" style="height:20px;" maxlength="64" type="text">
							<a href="javascript:act(4);"><img src="Edit%20Profile%20_%20MentorsGuild_files/2-11-09delete.jpg" alt="skype" class="padding_right"></a>						</div>
						
					</div>
					</div>
					
			
				
				<div class="white_box mr-bt20" style="width:90%; display:none;">
				  <ul class="h_list">
					<li class="one"><a href="#"><img src="Edit%20Profile%20_%20MentorsGuild_files/minus.png" alt="skype"></a></li>
					<li class="two">Maichel Dang </li>
					<li class="three"><a href="#">Log</a></li>
					<li class="four">Scheduled </li>
					<li class="floatR five"><a href="#">Mark session as complete </a></li>
				  </ul>
				  <ul class="h_list">
					   <li class="one"><a href="#"><img src="Edit%20Profile%20_%20MentorsGuild_files/minus.png" alt="skype"></a></li>
					 <li class="two">Stphene Dang</li>
					 <li class="three"><a href="#">Log</a></li>
					<li class="four">Applied</li>
					<li class="floatR five"><a href="#">Accept </a> <a href="#"> Reject </a> </li>
				  </ul>
				  <ul class="h_list">
					  <li class="one"><a href="#"><img src="Edit%20Profile%20_%20MentorsGuild_files/minus.png" alt="skype"></a></li>
					 <li class="two">Mark Dang </li>
					 <li class="three"><a href="#">Log</a></li>
					 <li class="four">Submitted feedback </li>
					<li class="floatR five"><a href="#">Review</a></li>
				  </ul>
				  <ul class="h_list">
					   <li class="one"><a href="#"><img src="Edit%20Profile%20_%20MentorsGuild_files/minus.png" alt="skype"></a></li>
					 <li class="two">Calude Dang </li>
					 <li class="three"><a href="#">Log</a></li>
					 <li class="four">Registered</li>
					<li class="floatR five"><a href="#">Scheduled orientation</a></li>
				  </ul>
				</div>
				<div id="area-box">
					<div class="invoce-creation">
			
						<h1>Session 3: Bill Kenny (Mentor) - Robert Wang (Mentee)    </h1>
					  <div class="formrow">
                        <p><b>Date</b></p>
                        <input type="text" id="dob" placeholder="MM/DD/YYYY"  maxlength="150" size="30" name="data[UserReference][area_of_expertise]">
                      </div>
                      <div class="formrow">
                        <p><b>Time</b></p>
                        <input type="text" id="UserReferenceAreaOfExpertise" placeholder="HH" style="width:50px; text-align:center; margin-right:10px;"  maxlength="150" size="30" name="data[UserReference][area_of_expertise]"><input type="text" id="UserReferenceAreaOfExpertise" placeholder="MM" style="width:50px; text-align:center; margin-right:10px;"  maxlength="150" size="30" name="data[UserReference][area_of_expertise]"><select style="width:50px;"><option>AM</option><option>PM</option></select>
                        </div>
                      <div class="formrow">
                        <div id="mode">
        <p><b>Mode</b></p>
    </div>
    <div id="face-box">
        <p class="styled-form">
            <input name="data[Communication][0][mode_type]" id="a1" value="face_to_face" type="checkbox">
            <label for="a1">Face to Face</label>
        </p>
        <p class="styled-form">
            <input name="data[Communication][1][mode_type]" id="a2" value="online" type="checkbox">
            <label for="a2">Online</label>
        </p>
    </div>
    <div id="face-text" style="padding-top:25px;">
                <input name="data[Communication][1][mode]" maxlength="35" class="forminput1" placeholder="Exact location on map OR Skype ID, etc." style="margin-top:6px;" id="Communication1Mode" type="text"><!--)));?-->
    </div>
                        
                      </div>
                      <div class="formrow">
                        <p><b>Comments</b></p>
                        <textarea name="data[UserReference][background_summary]" maxlength="2000" style="width:525px; padding:10px;" placeholder="Enter your comment here" cols="30" rows="6">Background Summary</textarea>
                      </div>
                      <div id="Apply" role="Apply">
							<input id="apply" value="Submit" class="submitProfile" type="submit" style="margin-bottom:10px;">
				  </div>
					</div>
                    
					
					
					
					
				</div>
				
			</div>
			
			<div id="invoce-right">
			<div class="invoice-detail">
            <h1>Invoice: #1235-HGFY</h1>
            <div class="invoice-price">
            <div class="price-row">
            <div class="left-price">Price</div>
            <div class="right-price">$100.00</div>
            </div>
            <div class="price-row">
            <div class="left-price">Discount </div>
            <div class="right-price"><input type="text" id="UserReferenceAreaOfExpertise" placeholder="$10.00" class="price-textbox"  maxlength="150" size="30" name="data[UserReference][area_of_expertise]"></div>
            </div>
            <div class="price-row">
            <div class="left-price"><strong>Subtotal</strong> </div>
            <div class="right-price"><strong>$90.00</strong></div>
            </div>
            <div class="price-row">
            <div class="left-price">Tax</div>
            <div class="right-price"><input type="text" id="UserReferenceAreaOfExpertise" placeholder="$4.50" class="price-textbox"  maxlength="150" size="30" name="data[UserReference][area_of_expertise]"></div>
            </div>
            <div class="price-total">
            <div class="left-price"><strong>Total</strong></div>
            <div class="right-price"><strong>$94.50</strong></div>
            </div>
            </div>
            </div>
            <div class="invoice-cal">
            <h1>Earnings Calculator</h1>
            <div class="invoice-calbox">
            <div class="price-row">
            <div class="left-price"><strong>My Share </strong></div>
            <div class="right-price"><strong>$80.00</strong></div>
            </div>
            <div class="price-row">
            <div class="left-price">Mentors guild fee </div>
            <div class="right-price">$20.00</div>
            </div>
            </div>
            </div>
			</div>
		  </div>
          
        
      
		  
		  
        </div>
      
    </div>
  </div>
<script type="text/javascript">
 jQuery(function() {
        jQuery( "#dob" ).datepicker();
    });
</script>