<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="fieldset">
    <h3 class="legend">
		Apply to be a Member Detail
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <input type="hidden" name="pageAction" id="pageAction"/>	 
        
            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr>
                        <td>
                           <b>Name</b>
                        </td>
                        <td>
                            <?php echo ucwords($mentor_data['UserReference']['first_name']) . ' ' . $mentor_data['UserReference']['last_name']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <b> Email</b>
                        </td>
                        <td>
                            <?php echo $mentor_data['User']['username']; ?>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <b>Background summary</b>
                        </td>
                        <td>
                           <?php echo $mentor_data['UserReference']['background_summary']; ?> 
                       
						</td>
                    </tr>
                    <tr>
                        <td>
                            <b>Linked In profile URL</b>
                        </td>
                        <td>
                           <?php echo $mentor_data['Social']['0']['social_name']; ?> 
                       
						</td>
                    </tr>
                    <tr>
                        <td>
                            <b>Business website URL</b>
                        </td>
                        <td>
                           <?php echo $mentor_data['Social']['1']['social_name']; ?> 
                       
						</td>
                    </tr>
                    <tr>
                        <td>
                           <b>Phone</b>
                        </td>
                        <td>
                           <?php echo $mentor_data['Communication']['0']['mode']; ?> 
                        </td>
                    </tr>
                </table>
            </div>
    </div>
</div>
<div class="buttonwapper">
	<div class="cancel_button">
		<?php echo $html->link("Back", "/admin/members/pending/", array("title"=>"", "escape"=>false)); ?>
	</div>
</div>
<script>
jQuery(document).ready(function(){
	jQuery(".saveDate").click(function(){
		var hrefvalue = jQuery(this).attr('title')		
		window.location.href = hrefvalue;
	})

});

</script>