<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php e($html->css(array('opentip'))); ?>
<?php e($javascript->link(array('opentip-jquery')));?>

<script type="text/javascript">
    jQuery(document).ready(function(){  

    // select element styling
            jQuery('select.select').each(function(){
                var title = jQuery(this).attr('title');
                if( jQuery('option:selected', this).val() != ''  ) title = jQuery('option:selected',this).text();
                jQuery(this)
                    .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
                    .after('<span class="select">' + title + '</span>')
                    .change(function(){
                        val = jQuery('option:selected',this).text();
                        jQuery(this).next().text(val);
                        })
            });
    
    });
</script>

<script>
	function submitForm(){

			var summary = jQuery('#text-area').val();
			var UserReferenceFirstName = jQuery('#UserReferenceFirstName').val();
			var UserReferenceLastName = jQuery('#UserReferenceLastName').val();
			var UserReferenceZipcode = jQuery('#UserReferenceZipcode').val();
                     var UserReferenceLinkedinHeadline = jQuery('#link-headline').val();
                     var UserReferencePastClient = jQuery('#past-client').val();
			var UserReferenceAreaOfExpertise = jQuery('#UserReferenceAreaOfExpertise').val();
			var UserReferenceDesireMenteeProfile = jQuery('#UserReferenceDesireMenteeProfile').val();
                     var UserReferenceWebsite = jQuery('#website').val();
			var UserReferenceFeeRegularSession = jQuery('#UserReferenceFeeRegularSession').val();
			var UserReferencePaypalAccount = jQuery('#UserReferencePaypalAccount').val();
                     var UserReferenceEngagementOverview = jQuery('#textareafee').val();


			jQuery.ajax({
				url:SITE_URL+'/members/mentor_draft_autosave',
				type:'post',
				dataType:'json',
				data: 'summary='+summary + 
				'&UserReferenceFirstName=' + UserReferenceFirstName + 
				'&UserReferenceLastName=' + UserReferenceLastName + 
				'&UserReferenceZipcode=' + UserReferenceZipcode + 
                            '&UserReferenceLinkedinHeadline=' + UserReferenceLinkedinHeadline+
                            '&UserReferencePastClient=' + UserReferencePastClient+
				'&UserReferenceAreaOfExpertise=' + UserReferenceAreaOfExpertise +
				'&UserReferenceDesireMenteeProfile=' + UserReferenceDesireMenteeProfile +
                            '&UserReferenceWebsite=' + UserReferenceWebsite+
				'&UserReferenceFeeRegularSession=' + UserReferenceFeeRegularSession +
				'&UserReferencePaypalAccount=' + UserReferencePaypalAccount+
                            '&UserReferenceEngagementOverview=' + UserReferenceEngagementOverview,
				success:function(){
				}
			});	
		return true;
	}
</script>
	
<script type="text/javascript">

jQuery(document).ready(function(){
    window.onload=initDraftSave();
});

	function initDraftSave() {
	setTimeout("initDraftSave();", 120000);

	submitForm();
}

</script>

<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div id="user">
            <div id="pro-creation">
                <h1>Profile Creation</h1>
            </div>
            <?php e($form->create('User', array('url' => array('controller' => 'members', 'action' => 'member_full_profile'),'type'=>'file','id'=>'profileForm')));?>     
                <?php e($form->hidden('User.id')); ?>
				<?php e($form->hidden('User.username')); ?>
				<?php e($form->hidden('UserReference.id')); ?>
				<div id="name-box">
                    <br />     
                    <div id="user-image">
						<div id="profileImage" style="width:200px;height:200px;">
						<?php
						if(isset($this->data['User']['id'])):
							$user_name = ucwords($this->data['UserReference']['first_name']).' '.ucwords($this->data['UserReference']['last_name']);
						else:
							$user_name = '';
						endif;
						if(isset($this->data['UserImage'][0]['image_name']) && $this->data['UserImage'][0]['image_name'] !=''){
							if($this->data['User']['role_id']==Configure::read('App.Role.Mentor')){
								$image_path = MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'];
								e($html->image($image_path,array('alt'=>$user_name)));	
							}else{
								$image_path = MENTEES_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'];
								e($html->image($image_path,array('alt'=>$user_name)));	
							}	
							e($form->hidden($this->data['UserImage'][0]['image_name'],array('value'=>$this->data['UserImage'][0]['image_name'],'id'=>'image_name')));
						}else{
							e($html->image('media/profile.png',array('alt'=>$user_name,'style' => 'padding-bottom:51px;')));
							e($form->hidden('profile.png',array('value'=>'profile.png','id'=>'image_name')));
						}?>						
						</div>
					<div><?php e($this->element('mentorsuser/add_image')); ?></div>
					</div>					
                    <div id="input-names"> 						
						<?php e($form->input('UserReference.first_name', array('div'=>false,'maxlength'=>15, 'label'=>false, "class" => "forminput1", "placeholder" => "First Name")));?>
						<?php e($form->input("UserReference.last_name", array("type" => "text",'maxlength'=>15, "div" => false, "label" => false,"class" => "forminput", "placeholder" => "Last Name", "style" => "margin-left:0;"))); ?>
						<?php  e($form->input('UserReference.zipcode', array('div'=>false,'maxlength'=>5, 'label'=>false, "class" => "forminput", "placeholder" => "ZIP","style" => "margin-left:0;")));?>
                                        	<div style="margin-right:10px;">
                                          <h5 style="font-weight:normal;margin-top:10px;margin-bottom:-10px;">Sample Profiles</h5>
                                          <a href="http://www.mentorsguild.com/ivan.rosenberg" target="_blank">mentorsguild.com/ivan.rosenberg</a><br/>
                                          <a href="http://www.mentorsguild.com/mark.palmer" target="_blank">mentorsguild.com/mark.palmer</a></div>	
				   </div>
                                          
                                          <?php //e($html->link($html->image('media/mark_screen.png',array('alt'=>'Sample profile','title'=>'Sample profile','style'=>'width:100px; height:100px;')),SITE_URL.'mark.palmer',array('escape'=>false, 'target'=>'_blank'))); ?>
                    <div id="country">                        
                        <?php  e($form->hidden('UserReference.country', array('value'=>'US')));?>		
                        <?php  //e($form->input('UserReference.zipcode', array('div'=>false,'maxlength'=>5, 'label'=>false, "class" => "forminput", "placeholder" => "ZIP","style" => "margin-left:0;")));?>		
                    </div>
                     
                    <div id="request" style="padding-top:30px;">
                      <p><b>Headline</b><span style="float:right; color: rgb(41, 41, 41); margin-right: 60px; font-size: 12px; font-weight: normal;">(<span id="headlinenoOfChars" style="padding-left: 0px; color: rgb(41, 41, 41); font-size: 12px; font-weight: normal;"><?php e(140 - strlen($this->data['UserReference']['linkedin_headline']))?></span> characters remaining)</span></p>
                      <?php e($form->input('UserReference.linkedin_headline', array('div'=>false,'label'=>false,"id"=>"link-headline","class" => "forminput","placeholder" => "Your professional headline", "style"=>"width:675px; height:25px;font-family: Ubuntu, arial, vardana; font-size: 14px; padding:10px 10px 10px;")));?>
                    </div>

                      
                    <div id="upload-resume" style="padding-top:20px;">
                        <p><b>Professional summary</b><span style="float:right; color: rgb(41, 41, 41); margin-right: 60px; font-size: 12px; font-weight: normal;">(<span id="noOfChars" style="padding-left: 0px; color: rgb(41, 41, 41); font-size: 12px; font-weight: normal;"><?php e(3000 - strlen($this->data['UserReference']['background_summary']))?></span> characters remaining)</span></p>		
                         <?php  e($form->input('UserReference.background_summary', array('div'=>false,'label'=>false,"id"=>"text-area", "placeholder" => "Start with an overview. Then, explain your process and list past clients. In the end, mention related work, training & publications (3,000 characters limit)",'style'=>"font-size: 14px;font-family:'Helvetica'")));?>
					</div>
					<div id="request" style="padding-top:20px;">
                        <p><b>Clients</b></p>
                        <p><?php e($form->textarea('UserReference.past_clients', array('div'=>false,'label'=>false, 'id'=>'past-client' , "class" => "forminput", "style"=>"width:675px; height:100px;font-family: Ubuntu, arial, vardana; font-size: 14px; padding:10px 10px 20px;", "placeholder" => "List your past and present clients here. If a client name is confidential, indicate industry and size")));?></p>
                    </div>
					<div id="request" style="padding-top:20px;">
						<p><b>Areas of expertise</b></p>
						<?php e($form->input('UserReference.area_of_expertise',array('size'=>30,'maxlength'=>150,'div'=>false,'label'=>false,'class'=>'forminput1','placeholder'=>'Up to 3 keywords separated by commas'))); ?>
						
					</div>	

                     </div>
					<div id="request" style="padding-top:20px;">
						<p><b>Business website</b></p>
						<?php e($form->input('UserReference.business_website',array('size'=>30,'maxlength'=>150,'div'=>false,'label'=>false,'id'=>'website' ,'class'=>'forminput1','placeholder'=>'Enter URL here'))); ?>
						
					</div>					
                    <?php e($this->element('mentorsuser/social_link_front')); ?>	
                    <?php //e($this->element('mentorsuser/resource_front_new')); ?>
					<div id="request" style="padding-top:20px;">
						<p><b>Desired client profile</b></p><!--VL 2/1/2013 -->
						<?php e($form->input('UserReference.desire_mentee_profile',array('size'=>30,'maxlength'=>200,'div'=>false,'label'=>false,'class'=>'forminput1','placeholder'=>'e.g. Mid-sized Healthcare organizations'))); ?>
					</div>			
                    <?php e($this->element('mentorsuser/communication_mode_front')); ?>
                    <?php e($this->element('mentorsuser/avaliability_front')); ?>
                    <div id="accepting" style="padding-top:20px;">
						<p class="styled-form"><b>Accepting new clients?</b> &nbsp;&nbsp;
						<?php 						
						e($form->radio('UserReference.accept_application', array('Y'=>'Yes','N'=>'No'),array('div'=>false,'label'=>true,'legend'=>false)));
						?>
						</p>
                    </div>
					<div class="clear"></div>
                    <?php // e($this->element('mentorsuser/mentorship_front')); ?>
                    <div id="fees" style="padding-top:20px;"> 
                        <p><b>Engagement Fee $</b></p>
                    </div>
                    <div class="orientation" style="width:96%;">
                        <?php  e($form->input('UserReference.fee_regular_session', array('div'=>false, 'label'=>false, "class" => "forminput1", "placeholder"=>"0",'style'=>'width:360px;')));?><span style="font-weight:bold;">&nbsp;&nbsp; /</span>
	                <span style="float: right; font-size: 15px; margin-right: 357px; margin-top: 54px;">
							<?php $options = array('1' => 'Hour', '2' => 'Month', '3' => 'Engagement', '4' => 'Day'); ?>
	                        <div class="timzoneselectbox_cont">
							<?php echo $form->select('UserReference.regular_session_per',$options,null,array('div' =>false,'class'=>'select','style' => 'color:#292929;width:85px;','empty' => false)); ?>
						</span> 	</div>
                    </div>
					<div id="request" style="padding-top:16px;">
						<p><b>Configure your&nbsp;</b><!--VL 2/1/2013 -->
						<?php e($html->image('media/paypal.png',array('alt'=>'PayPal username (Primary email)'))); ?>
						
						</p>
						<?php e($form->input('UserReference.paypal_account',array('size'=>30,'maxlength'=>200,'div'=>false,'label'=>false,'placeholder'=>'PayPal account email','style' => 'width:323px;'))); ?>
					</div>		
					<div id="request" style="padding-top:20px;">
						<p><b>Engagement overview</b> <span style="float:right; color: rgb(41, 41, 41); margin-right: 60px; font-size: 12px;margin-right: 276px; font-weight: normal;">(<span id="noOfChar" style="padding-left: 0px; color: rgb(41, 41, 41); font-size: 12px; font-weight: normal;"><?php e(1000 - strlen($this->data['UserReference']['feeNSession']))?></span> characters remaining)</span></p>
						<?php  e($form->input('UserReference.feeNSession', array('div'=>false,'label'=>false,"id"=>"textareafee", "placeholder" => "What's included in a typical engagement? Indicate duration, methodology and pricing",'style'=>"font-size: 14px;font-family:'Helvetica'; padding: 10px; width: 670px;")));?>
					</div>			
					<?php e($form->hidden('User.access_specifier')); ?>
                    <div id ="publish-button"style="margin-right:215px;" >
                    <div class="savedraffbtn" >
 						<?php e($html->link('Save as draft','javascript:void(0);',array('class'=>'submitProfile'))); ?></div>
              	
						<input type="button" value="Publish" class="submitProfile">

                    </div>
					<?php
					//set flag before submit form
					e($form->hidden('resourceFlag',array('id'=>'resourceFlag','value'=>0,'label'=>false,'div'=>false)));
					?>
                </div>
            <?php e($form->end()); ?>
        </div>
    </div>
</div>
<?php
if(!$this->Session->read('Mentor.PasswordSet') && $this->Session->read('Auth.User.id') ==''){?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			passwordSettingPopup();
		});
	</script>
<?php
}?>
<script type="text/javascript">
	//validation
	
	jQuery(document).ready(function(){

		 var qText = jQuery("#textareafee").val();
    	 jQuery("#noOfChar").html(1000 - qText.length);
    	
		 function updateCount ()
		    {
		        var qText = jQuery("#textareafee").val();

		       if(qText.length < 1000) {
		           jQuery("#noOfChar").html(1000 - qText.length);
		       } else {
		           jQuery("#noOfChar").html(0);
		           jQuery("#textareafee").val(qText.substring(0,1000));
		       }
		    }

		    jQuery("#textareafee").keyup(function () {
		        updateCount();
		    });
		    jQuery("#textareafee").keypress(function () {
		        updateCount();
		    });
		    jQuery("#textareafee").keydown(function () {
		        updateCount();
		    });
		    
                  var qTexts = jQuery("#text-area").val();
    	            jQuery("#noOfChars").html(3000 - qTexts.length);
    	
		 function updateCounts ()
		    {
		        var qTexts = jQuery("#text-area").val();

		       if(qTexts.length < 3000) {
		           jQuery("#noOfChars").html(3000 - qTexts.length);
		       } else {
		           jQuery("#noOfChars").html(0);
		           jQuery("#text-area").val(qTexts.substring(0,3000));
		       }
		    }

		    jQuery("#text-area").keyup(function () {
		        updateCounts();
		    });
		    jQuery("#text-area").keypress(function () {
		        updateCounts();
		    });
		    jQuery("#text-area").keydown(function () {
		        updateCounts();
		    });

                 var headTexts = jQuery("#link-headline").val();
    	            jQuery("#headlinenoOfChars").html(140 - headTexts.length);
    	
		 function updateCountss ()
		    {
		        var headTexts = jQuery("#link-headline").val();

		       if(headTexts.length < 140) {
		           jQuery("#headlinenoOfChars").html(140 - headTexts.length);
		       } else {
		           jQuery("#headlinenoOfChars").html(0);
		           jQuery("#link-headline").val(headTexts.substring(0,140));
		       }
		    }

		    jQuery("#link-headline").keyup(function () {
		        updateCountss();
		    });
		    jQuery("#link-headline").keypress(function () {
		        updateCountss();
		    });
		    jQuery("#link-headline").keydown(function () {
		        updateCountss();
		    });

		//for validation on resource file uploading
		jQuery(".resourceFile").change(function () {

			var uploadedFile=jQuery(this).val();
			if(jQuery.trim(uploadedFile) !='') {	
				var resumeArr = uploadedFile.split(".");			
				var ext = ["doc", "docx", "ppt", "xlsx", "pdf" ]; // Creating Extension array 
				if(jQuery.inArray(resumeArr[1], ext)==-1){ // Checking value of extension
					alert("Allowed File type only "+ext);					
					jQuery("#resourceFlag").val(1);
					jQuery(this).val('');
				}else{
					var iSize = (jQuery(this)[0].files[0].size / 1024);	

					ValidateFileSize(iSize,uploadedFile,jQuery(this));
				}
			}else{
				jQuery("#resourceFlag").val(0);
			}
									
		});

        //Initialising all tooltips
		var myImagetip = new Opentip("#user-image", "", {style: "myErrorStyleCreation"});
		var myFNametip = new Opentip("#UserReferenceFirstName", "", {style: "myErrorStyleCreation"});
		var myLNametip = new Opentip("#UserReferenceLastName", "", {style: "myErrorStyleCreation"});
		var myZiptip = new Opentip("#UserReferenceZipcode", "", {style: "myErrorStyleCreation"});
		var myTexttip = new Opentip("#text-area", "", {style: "myErrorStyleCreation"});
		var myRegularttip= new Opentip("#UserReferenceFeeRegularSession", "", {style: "myErrorStyleCreation"});
		var myPaypaltip= new Opentip("#UserReferencePaypalAccount", "", {style: "myErrorStyleCreation"});
		var myAvailabilitytip = new Opentip("#AvailablityDayTime", "", {style: "myErrorStyleCreation"});
		var myExpertisetip = new Opentip("#UserReferenceAreaOfExpertise", "", {style: "myErrorStyleCreation"});
		var myCom0tip = new Opentip("#Communication0Mode", "", {style: "myErrorStyleCreation"});
		var myCom1tip = new Opentip("#Communication1Mode", "", {style: "myErrorStyleCreation"});
              var myCom2tip = new Opentip("#Communication2Mode", "", {style: "myErrorStyleCreation"});
		var myDesiredtip = new Opentip("#UserReferenceDesireMenteeProfile", "", {style: "myErrorStyleCreation"});
		var myFeeNSessiontip= new Opentip("#textareafee", "", {style: "myErrorStyleCreation"});
              var myLinkheadline= new Opentip("#link-headline", "", {style: "myErrorStyleCreation"});
              //var mypaypaltip= new Opentip("#paypaltext", "", {style: "myErrorStyleBottomCreation"}); 
           //End
		
		jQuery(".submitProfile").click(function(){
		
			//set publish or draft
			var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			var saveAs = jQuery(this).val();		
			var flag = parseInt(jQuery("#resourceFlag").val(),10);
			var zipValue = jQuery.trim(jQuery('#UserReferenceZipcode').val());
			var onlyNumbers = /^[0-9,]+(\.\d{0,2})?$/;
			//var onlyPhonenumbers = /^[\\(]{0,1}([0-9]){3}[\\)]{0,1}[ ]?([^0-1]){1}([0-9]){2}[ ]?[-]?[ ]?([0-9]){4}[ ]*((x){0,1}([0-9]){1,5}){0,1}$/;
                     var goingID = '';
			if(saveAs=='Publish'){
				jQuery("#UserAccessSpecifier").val('publish');			
			
				//for first name
				// onlyLetters = /^[a-zA-Z _!@#"$%\^\-:.<>?=&'()*\+,\/;\[\\\]\^_`{|}~]+$/.test(jQuery.trim(jQuery("#UserReferenceFirstName").val()))
                onlyLetters = /^[a-zA-Z\-`']+$/.test(jQuery.trim(jQuery("#UserReferenceFirstName").val()))
				
				var imageName = jQuery.trim(jQuery('#image_name').val());
                if(imageName == 'profile.png')
            	{ 
            		jQuery('#user-image').css('border-color','#F00');
            		jQuery('#user-image').css('border-width','<?php e(ERROR_BORDER);?>px');
            		//jQuery('#user-image').focus();
            		if(goingID==''){ goingID = 'user-image'; }
            		myImagetip.setContent("Upload photo"); 
            		myImagetip.show();
            		flag++;
            	}
            	else
            	{
            		jQuery('#user-image').css('border-color','rgb(221, 221, 221)');
            		jQuery('#user-image').css('border-width','<?php e(NORMAL_BORDER);?>px');
            		myImagetip.hide();
            	}
            	
				if(jQuery.trim(jQuery("#UserReferenceFirstName").val()) == '') {
					jQuery('#UserReferenceFirstName').css('border-color','#F00');
					jQuery('#UserReferenceFirstName').css('border-width','<?php e(ERROR_BORDER);?>px');
					//jQuery('#UserReferenceFirstName').focus();
					if(goingID==''){ goingID = 'UserReferenceFirstName';}  
					myFNametip.setContent("Do not leave blank");
                    myFNametip.show();
					flag++;
				
				}else if(!onlyLetters){
					jQuery('#UserReferenceFirstName').css('border-color','#F00');
					jQuery('#UserReferenceFirstName').css('border-width','<?php e(ERROR_BORDER);?>px');
					//jQuery('#UserReferenceFirstName').focus();
					goingID = 'UserReferenceFirstName';
					myFNametip.setContent("Only alphabets are allowed");
                    myFNametip.show();
					flag++;
				}else {
					jQuery('#UserReferenceFirstName').css('border-color','');	
					jQuery('#UserReferenceFirstName').css('border-width','<?php e(NORMAL_BORDER);?>px');
					myFNametip.hide();
				}
				//for last name
				//var lastNameonlyLetters = /^[a-zA-Z _!@#"$%\^\-:.<>?=&'()*\+,\/;\[\\\]\^_`{|}~]+$/.test(jQuery.trim(jQuery("#UserReferenceLastName").val()))
                var lastNameonlyLetters = /^[a-zA-Z\-`']+$/.test(jQuery.trim(jQuery("#UserReferenceLastName").val()))
				if(jQuery.trim(jQuery('#UserReferenceLastName').val()) == '') {
					jQuery('#UserReferenceLastName').css('border-color','#F00');
					jQuery('#UserReferenceLastName').css('border-width','<?php e(ERROR_BORDER);?>px');
					//jQuery('#UserReferenceLastName').focus();	
					if(goingID==''){ goingID = 'UserReferenceLastName';   }  
					myLNametip.setContent("Do not leave blank");
                    myLNametip.show();  
					flag++;
				}else if(!lastNameonlyLetters){
					jQuery('#UserReferenceLastName').css('border-color','#F00');
					jQuery('#UserReferenceLastName').css('border-width','<?php e(ERROR_BORDER);?>px');
					//jQuery('#UserReferenceLastName').focus();
					if(goingID==''){ goingID = 'UserReferenceLastName';   } 
					myLNametip.setContent("Only alphabets are allowed");
                    myLNametip.show();
					flag++;
				} else {
					jQuery('#UserReferenceLastName').css('border-color','');	
					jQuery('#UserReferenceLastName').css('border-width','<?php e(NORMAL_BORDER);?>px');
					myLNametip.hide(); 
				}

                             //for Professional headline
                          // var headlineonlyLetters = /^[a-zA-Z\s]+$/.test(jQuery.trim(jQuery("#link-headline").val()))
                             if(jQuery.trim(jQuery("#link-headline").val()) == '') {
					jQuery('#link-headline').css('border-color','#F00');
					jQuery('#link-headline').css('border-width','<?php e(ERROR_BORDER);?>px');
					//jQuery('#link-headline').focus();
					if(goingID==''){ goingID = 'link-headline';}  
					flag++;
                                   myLinkheadline.setContent("Do not leave blank");
                    myLinkheadline.show();
					
				
				
					
				}else {
					jQuery('#link-headline').css('border-color','');	
					jQuery('#link-headline').css('border-width','<?php e(NORMAL_BORDER);?>px');
					myLinkheadline.hide();
				}
                     
				
				//for BackgroundSummary
				if(jQuery.trim(jQuery("#text-area").val()) == '') {
					jQuery('#text-area').css('border-color','#F00');
					jQuery('#text-area').css('border-width','<?php e(ERROR_BORDER);?>px');
					//jQuery('#text-area').focus();
					if(goingID==''){ goingID = 'text-area';   } 	
					flag++;
					myTexttip.setContent("Do not leave blank");
                    myTexttip.show(); 
				} else {
					     var qText = jQuery("#text-area").val();

		                if(qText.length < 500)
                             {
                                jQuery('#text-area').css('border-color','#F00');
                                jQuery('#text-area').css('border-width','<?php e(ERROR_BORDER);?>px');
                                if(goingID==''){ goingID = 'text-area';   }  
                                flag++;  
                                myTexttip.setContent("1.Use 4-6 short paragraphs</br>2. Open with a 2-3 lines of overview/ summary</br>3. Focus on one area of specialization</br>4. Talk about past consulting work and methodology</br>5. Mention related experience, speaking engagements, publications</br>6. End with education, credentials, affiliations</br>(Minimum 500 characters)");
                                myTexttip.show();
                             }
                             else
                             {
                                 jQuery('#text-area').css('border-color',''); 
                                 jQuery('#text-area').css('border-width','<?php e(NORMAL_BORDER);?>px');
                                 myTexttip.hide();     
                             }	
				        }

				//for UserReferenceZipcode
				if(jQuery.trim(jQuery('#UserReferenceZipcode').val()) == '') {
					jQuery('#UserReferenceZipcode').css('border-color','#F00');
					jQuery('#UserReferenceZipcode').css('border-width','<?php e(ERROR_BORDER);?>px');
					//jQuery('#UserReferenceZipcode').focus();
					if(goingID==''){ goingID = 'UserReferenceZipcode';   } 				
					flag++;
					myZiptip.setContent("Do not leave blank");
                    myZiptip.show(); 
				}else if(isNaN(jQuery.trim(jQuery('#UserReferenceZipcode').val())) || zipValue.length<4){
					jQuery('#UserReferenceZipcode').css('border-color','#F00');
					jQuery('#UserReferenceZipcode').css('border-width','<?php e(ERROR_BORDER);?>px');
					//jQuery('#UserReferenceZipcode').focus();	
					if(goingID==''){ goingID = 'UserReferenceZipcode';   } 	
					myZiptip.setContent("ZIP should be 5 digits");
                    myZiptip.show(); 	
					flag++;				
				}else {
					jQuery('#UserReferenceZipcode').css('border-color','');	
					jQuery('#UserReferenceZipcode').css('border-width','<?php e(NORMAL_BORDER);?>px');
					myZiptip.hide(); 
				}			
				
				//for UserReference.area_of_expertise
				var expertiseStr = ValidateAreaExpertise(jQuery.trim(jQuery('#UserReferenceAreaOfExpertise').val()));
				if(!expertiseStr) {	
				    if(goingID==''){ goingID = 'UserReferenceAreaOfExpertise';   } 					
					flag++;
					jQuery('#UserReferenceAreaOfExpertise').css('border-color','#F00');
					jQuery('#UserReferenceAreaOfExpertise').css('border-width','<?php e(ERROR_BORDER);?>px');
					myExpertisetip.setContent("Up to 3 areas. Separated by commas");
                    myExpertisetip.show();
				}else {
					jQuery('#UserReferenceAreaOfExpertise').css('border-color','');	
					jQuery('#UserReferenceAreaOfExpertise').css('border-width','<?php e(NORMAL_BORDER);?>px');
					jQuery('#area_of_expertise').html('');
					myExpertisetip.hide();
				}

				 if(jQuery.trim(jQuery("#UserReferenceDesireMenteeProfile").val()) == '') {
                     if(goingID==''){ goingID = 'UserReferenceDesireMenteeProfile';} 
                     myDesiredtip.setContent("Do not leave blank");
                     myDesiredtip.show();
                     jQuery('#UserReferenceDesireMenteeProfile').css('border-color','#F00');
 					 jQuery('#UserReferenceDesireMenteeProfile').css('border-width','<?php e(ERROR_BORDER);?>px');
                     flag++;
                 }else {
                     jQuery('#UserReferenceDesireMenteeProfile').css('border-color',''); 
                     jQuery('#UserReferenceDesireMenteeProfile').css('border-width','<?php e(NORMAL_BORDER);?>px');
                     jQuery('#UserReferenceDesireMenteeProfile').html('');
                     myDesiredtip.hide();
                 }
						
				
				
				//For resorces File Title & File combination
				jQuery("input[name='data[UserReference][resource_title][]']").each(function() {
                        //curr_index = jQuery("input[name='data[UserReference][resource_title][]']").index(this);
                        jQuery(this).css('border-color','');
                        jQuery(this).css('border-width','<?php e(NORMAL_BORDER);?>px');
                    }); 
                    
                    jQuery("input[name='data[UserReference][resource_location][]']").each(function() {
                        //curr_index = jQuery("input[name='data[UserReference][resource_location][]']").index(this);
                        jQuery(this).css('border-color','');
                        jQuery(this).css('border-width','<?php e(NORMAL_BORDER);?>px');
                    });
                    
                    resource_id_array = new Array();
                    index_count = 0;
                    jQuery("input[name='data[UserReference][resource_id][]']").each(function() {
                        curr_index = jQuery("input[name='data[UserReference][resource_id][]']").index(this);
                        if(jQuery("input[name='data[UserReference][resource_id][]']").eq(curr_index).val() != "NEW"){
                            resource_id_array[index_count] = curr_index;
                            index_count++;
                        }   
                    });
                    
                    jQuery("input[name='data[UserReference][resource_title][]']").each(function() {
                        curr_index = jQuery("input[name='data[UserReference][resource_title][]']").index(this);
							
                        if(jQuery.inArray(curr_index, resource_id_array) == -1){
                            
                            if(jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).val() != ""){
								if(resource_id_array.length > 0){
									curr_index = curr_index - resource_id_array.length;
								}
                                if(jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index).val() == ""){
                                    jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index).css('border-color','#F00');
                                    jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index).css('border-width','<?php e(ERROR_BORDER);?>px');
                                    jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index).opentip("Upload a file", {
                  						 style: "myErrorStyle" 
                  						});
                                    //jQuery(this).focus();
                                    flag++;
                                }
                            }
                            
                        }else{
                            if(jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).val() == ""){
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-color','#F00');
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-width','<?php e(ERROR_BORDER);?>px');
                                    jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index).opentip("Please provide a title", {
                 						 style: "myErrorStyle" 
                 						});
                                    //jQuery(this).focus();
                                    flag++;
                                }
                        }
                    }); 
                    
                    //*****************************************************
                    
                    jQuery("input[name='data[UserReference][resource_location][]']").each(function() {
                        curr_index = jQuery("input[name='data[UserReference][resource_location][]']").index(this);
						
						if(resource_id_array.length > 0){
							curr_index = curr_index + resource_id_array.length;
						}
						//alert(curr_index +'\n'+ resource_id_array.join(', '));
                        if(jQuery.inArray(curr_index, resource_id_array) == -1){
                            if(resource_id_array.length > 0){
								curr_index = curr_index - resource_id_array.length;
							}
                            if(jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index).val() != ""){
								if(resource_id_array.length > 0){
									curr_index = curr_index + resource_id_array.length;
								}
								
                                if(jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).val() == ""){
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-color','#F00');
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-width','<?php e(ERROR_BORDER);?>px');
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).opentip("Please provide a title", {
                 						 style: "myErrorStyle" 
                 						});
                                    //jQuery(this).focus();
                                    flag++;
                                }
                            }
                            
                        }else{
							//alert(curr_index + "  ELSE");
                            if(jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).val() == ""){
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-color','#F00');
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-width','<?php e(ERROR_BORDER);?>px');
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).opentip("Please provide a title", {
                						 style: "myErrorStyle" 
                						});
                                    //jQuery(this).focus();
                                    flag++;
                                }
                        }
                    }); 
                    
					jQuery("input[name='data[UserReference][resource_location][]']").each(function() {
						checkValidFile(this);
					});
                    //*****************************************************
				
					//for Communication Mode Face to Face &  Phone & Skype
					jQuery('#Communication0Mode').css('border-color','');
					jQuery('#Communication1Mode').css('border-color','');
					jQuery('#Communication2Mode').css('border-color','');
					jQuery('#Communication0Mode').css('border-width','<?php e(NORMAL_BORDER);?>px');
					jQuery('#Communication1Mode').css('border-width','<?php e(NORMAL_BORDER);?>px');
					jQuery('#Communication2Mode').css('border-width','<?php e(NORMAL_BORDER);?>px');
					
					if (jQuery('#a1').is(':checked') == false && jQuery('#a2').is(':checked') == false && jQuery('#a3').is(':checked') == false){
						jQuery('#Communication0Mode').css('border-color','#F00');
						jQuery('#Communication1Mode').css('border-color','#F00');
                                          jQuery('#Communication0Mode').css('border-color','#F00');
                                          jQuery('#Communication2Mode').css('border-color','#F00');
                                          jQuery('#Communication0Mode').css('border-color','#F00');
                                          jQuery('#Communication0Mode').css('border-width','<?php e(ERROR_BORDER);?>px');
                                          jQuery('#Communication1Mode').css('border-width','<?php e(ERROR_BORDER);?>px');
					       jQuery('#Communication2Mode').css('border-width','<?php e(ERROR_BORDER);?>px');                        
                                        //jQuery('#Communication0Mode').focus();
						if(goingID==''){ goingID = 'Communication0Mode';   } 	
						myCom0tip.setContent("Enter atleast one communication mode");
						myCom0tip.show();
						flag++;
					} else {
						if (jQuery('#a1').is(':checked') == true && jQuery.trim(jQuery('#Communication2Mode').val()) == ""){
							jQuery('#Communication2Mode').css('border-color','#F00');
							jQuery('#Communication2Mode').css('border-width','<?php e(ERROR_BORDER);?>px');
							//jQuery('#Communication2Mode').focus();	
							if(goingID==''){ goingID = 'Communication2Mode';   }
							myCom2tip.setContent("Enter your phone number");
							myCom2tip.show(); 
							flag++;
						} else {
							myCom2tip.hide(); 
						}
                                           if (jQuery('#a2').is(':checked') == true && jQuery.trim(jQuery('#Communication0Mode').val()) == ""){
							jQuery('#Communication0Mode').css('border-color','#F00');
							jQuery('#Communication0Mode').css('border-width','<?php e(ERROR_BORDER);?>px');
							//jQuery('#Communication0Mode').focus();	
							if(goingID==''){ goingID = 'Communication0Mode';   }
							myCom0tip.setContent("Enter communication mode, without specific address or number");
							myCom0tip.show(); 
							flag++;
						}  
                                              else {
							myCom0tip.hide(); 
						}
                                    
 
						if (jQuery('#a3').is(':checked') == true && jQuery.trim(jQuery('#Communication1Mode').val()) == ""){
							jQuery('#Communication1Mode').css('border-color','#F00');
							jQuery('#Communication1Mode').css('border-width','<?php e(ERROR_BORDER);?>px');
							//jQuery('#Communication1Mode').focus();	
							if(goingID==''){ goingID = 'Communication1Mode';   }
							myCom1tip.setContent("Enter communication mode, without specific address or number");
							myCom1tip.show(); 
							flag++;
						} else {
							myCom1tip.hide(); 
						}
					}
                                
					//for Availablity.day_time
					
					if(jQuery.trim(jQuery('#AvailablityDayTime').val()) == '') {
						jQuery('#AvailablityDayTime').css('border-color','#F00');
						jQuery('#AvailablityDayTime').css('border-width','<?php e(ERROR_BORDER);?>px');
						//jQuery('#AvailablityDayTime').focus();	
						if(goingID==''){ goingID = 'AvailablityDayTime';   } 
						myAvailabilitytip.setContent("e.g. Business hours (ET). By appointment");
                                          myAvailabilitytip.show();	
						flag++;
					} else {
						jQuery('#AvailablityDayTime').css('border-color','');
						jQuery('#AvailablityDayTime').css('border-width','<?php e(NORMAL_BORDER);?>px');
						myAvailabilitytip.hide();	
					}
					
				//check fees
				//check UserReferenceFeeRegularSession
				
				if(jQuery.trim(jQuery('#UserReferenceFeeRegularSession').val()) !='' && !onlyNumbers.test(jQuery.trim(jQuery("#UserReferenceFeeRegularSession").val()))){
					jQuery('#UserReferenceFeeRegularSession').css('border-color','#F00');
					jQuery('#UserReferenceFeeRegularSession').css('border-width','<?php e(ERROR_BORDER);?>px');
					//jQuery('#UserReferenceFeeRegularSession').focus();	
					if(goingID==''){ goingID = 'UserReferenceFeeRegularSession';   } 	
					myRegularttip.setContent("Numeric values only");
                    myRegularttip.show();		
					flag++;				
				}else {
				           if(jQuery('#UserReferenceFeeRegularSession').val()>99999.99)
                           {
                                jQuery('#UserReferenceFeeRegularSession').css('border-color','#F00');
                                jQuery('#UserReferenceFeeRegularSession').css('border-width','<?php e(ERROR_BORDER);?>px');
                                //jQuery('#UserReferenceFeeRegularSession').focus();   
                                if(goingID==''){ goingID = 'UserReferenceFeeRegularSession';   }  
                                myRegularttip.setContent("Fee should be less than $100,000");
                                myRegularttip.show();       
                                flag++;
                            }else if(jQuery('#UserReferenceFeeRegularSession').val() == 0)
                           {
                                jQuery('#UserReferenceFeeRegularSession').css('border-color','#F00');
                                jQuery('#UserReferenceFeeRegularSession').css('border-width','<?php e(ERROR_BORDER);?>px');
                                //jQuery('#UserReferenceFeeRegularSession').focus();   
                                if(goingID==''){ goingID = 'UserReferenceFeeRegularSession';   }  
                                myRegularttip.setContent("Engagement fee is kept private. We use this information while making recommendations to potential clients. Engagements cannot be FREE.");
                                myRegularttip.show(); 
                                flag++;
                            }
                            else{ 
                                jQuery('#UserReferenceFeeRegularSession').css('border-color',''); 
                                jQuery('#UserReferenceFeeRegularSession').css('border-width','<?php e(NORMAL_BORDER);?>px');
                                myRegularttip.hide();
                            }    

				}		
				/* validation for paypal */
				var RSFee = jQuery.trim(jQuery('#UserReferenceFeeRegularSession').val());
				//var AFee = jQuery.trim(jQuery('#UserReferenceApplication5Fee1').checked);
				//var AFee = jQuery('#UserReferenceApplication5Fee1:checked').val(); 
				
				if(RSFee != 0 && RSFee !=''){
					if(jQuery.trim(jQuery('#UserReferencePaypalAccount').val()) == ''  || !jQuery('#UserReferencePaypalAccount').val().match(mailformat)){
						jQuery('#UserReferencePaypalAccount').css('border-color','#F00');
						jQuery('#UserReferencePaypalAccount').css('border-width','<?php e(ERROR_BORDER);?>px');
						//jQuery('#UserReferencePaypalAccount').focus();	
						if(goingID==''){ goingID = 'UserReferencePaypalAccount';   } 
						myPaypaltip.setContent("Please enter PayPal receiver email account. Same as your PayPal username");
                        myPaypaltip.show();			
						flag++;				
					}else{
						jQuery('#UserReferencePaypalAccount').css('border-color','');
						jQuery('#UserReferencePaypalAccount').css('border-width','<?php e(NORMAL_BORDER);?>px');
						myPaypaltip.hide();
					}
				}/*else if(AFee ==1){
					if(jQuery.trim(jQuery('#UserReferencePaypalAccount').val()) == ''  || !jQuery('#UserReferencePaypalAccount').val().match(mailformat)){
						jQuery('#UserReferencePaypalAccount').css('border-color','#F00');
						jQuery('#UserReferencePaypalAccount').focus();	
						if(goingID==''){ goingID = 'UserReferencePaypalAccount';   } 	
						jQuery("#UserReferencePaypalAccount").opentip("Please enter valid PayPal account email", {
    						 style: "myErrorStyle" 
    						});			
						flag++;
					}else{
						jQuery('#UserReferencePaypalAccount').css('border-color','');
					}		
				}*/else{
					jQuery('#UserReferencePaypalAccount').css('border-color','');
					jQuery('#UserReferencePaypalAccount').css('border-width','<?php e(NORMAL_BORDER);?>px');
					myPaypaltip.hide();
				}
                             
                      /*                if(jQuery.trim(jQuery('#textareafee').val()) == ''){
                    jQuery('#textareafee').css('border-color','#F00');
                    jQuery('#textareafee').css('border-width','<?php e(ERROR_BORDER);?>px');
                    jQuery('#textareafee').focus();
                    if(goingID==''){ goingID = 'textareafee';    }   
                    mypaypaltip.setContent("What's included and what's excluded? ");
                    mypaypaltip.show();
                    flag++;
                }else{
                    jQuery('#textareafee').css('border-color','');
                    jQuery('#textareafee').css('border-width','<?php e(NORMAL_BORDER);?>px');
                    myFeeNSessiontip.hide();
                }*/
                             if(jQuery.trim(jQuery('#textareafee').val()) == ''){
                    jQuery('#textareafee').css('border-color','#F00');
                    jQuery('#textareafee').css('border-width','<?php e(ERROR_BORDER);?>px');
                    jQuery('#textareafee').focus();
                    if(goingID==''){ goingID = 'textareafee';    }   
                    myFeeNSessiontip.setContent("What's included and what's excluded? ");
                    myFeeNSessiontip.show();
                    flag++;
                }else{
                    jQuery('#textareafee').css('border-color','');
                    jQuery('#textareafee').css('border-width','<?php e(NORMAL_BORDER);?>px');
                    myFeeNSessiontip.hide();
                }				
				}else{
					
					jQuery("#profileForm input[type='text'], textarea").each(function() {
                        jQuery(this).css('border-color','');
                        jQuery(this).css('border-width','<?php e(NORMAL_BORDER);?>px');
                    });
					jQuery("input[name='data[UserReference][resource_title][]']").each(function() {
                        //curr_index = jQuery("input[name='data[UserReference][resource_title][]']").index(this);
                        jQuery(this).css('border-color','');
                        jQuery(this).css('border-width','<?php e(NORMAL_BORDER);?>px');
                    }); 
                    
                    jQuery("input[name='data[UserReference][resource_location][]']").each(function() {
                        //curr_index = jQuery("input[name='data[UserReference][resource_location][]']").index(this);
                        jQuery(this).css('border-color','');
                        jQuery(this).css('border-width','<?php e(NORMAL_BORDER);?>px');
                    });
                    
                    resource_id_array = new Array();
                    index_count = 0;
                    jQuery("input[name='data[UserReference][resource_id][]']").each(function() {
                        curr_index = jQuery("input[name='data[UserReference][resource_id][]']").index(this);
                        if(jQuery("input[name='data[UserReference][resource_id][]']").eq(curr_index).val() != "NEW"){
                            resource_id_array[index_count] = curr_index;
                            index_count++;
                        }   
                    });
                    
                    jQuery("input[name='data[UserReference][resource_title][]']").each(function() {
                        curr_index = jQuery("input[name='data[UserReference][resource_title][]']").index(this);
							
                        if(jQuery.inArray(curr_index, resource_id_array) == -1){
                            
                            if(jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).val() != ""){
								if(resource_id_array.length > 0){
									curr_index = curr_index - resource_id_array.length;
								}
                                if(jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index).val() == ""){
                                    jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index).css('border-color','#F00');
                                    jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index).css('border-width','<?php e(ERROR_BORDER);?>px');
                                    jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index).opentip("Upload a file", {
               						 style: "myErrorStyle" 
               						});	
                                    //jQuery(this).focus();
                                    flag++;
                                }
                            }
                            
                        }else{
                            if(jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).val() == ""){
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-color','#F00');
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-width','<?php e(ERROR_BORDER);?>px');
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).opentip("Please provide a title", {
                  						 style: "myErrorStyle" 
                  						});
                                    //jQuery(this).focus();
                                    flag++;
                                }
                        }
                    }); 
                    
                    //*****************************************************
                    
                    jQuery("input[name='data[UserReference][resource_location][]']").each(function() {
                        curr_index = jQuery("input[name='data[UserReference][resource_location][]']").index(this);
						if(resource_id_array.length > 0){
							curr_index = curr_index + resource_id_array.length;
						}
						//alert(curr_index +'\n'+ resource_id_array.join(', '));
                        if(jQuery.inArray(curr_index, resource_id_array) == -1){
                            if(resource_id_array.length > 0){
								curr_index = curr_index - resource_id_array.length;
							}
                            if(jQuery("input[name='data[UserReference][resource_location][]']").eq(curr_index).val() != ""){
								if(resource_id_array.length > 0){
									curr_index = curr_index + resource_id_array.length;
								}
								
                                if(jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).val() == ""){
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-color','#F00');
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-width','<?php e(ERROR_BORDER);?>px');
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).opentip("Please provide a title", {
                 						 style: "myErrorStyle" 
                 						});
                                    //jQuery(this).focus();
                                    flag++;
                                }
                            }
                            
                        }else{
							//alert(curr_index + "  ELSE");
                            if(jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).val() == ""){
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-color','#F00');
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).css('border-width','<?php e(ERROR_BORDER);?>px');
                                    jQuery("input[name='data[UserReference][resource_title][]']").eq(curr_index).opentip("Please provide a title", {
                 						 style: "myErrorStyle" 
                 						});
                                    //jQuery(this).focus();
                                    flag++;
                                }
                        }
                    }); 
                    
					jQuery("input[name='data[UserReference][resource_location][]']").each(function() {
						checkValidFile(this);
					});
                    //*****************************************************
                    
					
					

					
					jQuery("#UserAccessSpecifier").val('draft');
				}
				if(flag ==0){
					jQuery("#profileForm").submit();
				}
				else{
				    if(goingID != ""){

					    if(goingID == 'user-image') {
					    	document.getElementById ("user-image").scrollIntoView(true);
					    }else{
							jQuery('#'+goingID).focus();
					    }
					}
				    return false;
				}
		});


	});
		
	function ValidateFileSize(iSize,Filename,obj){
		var size=Math.round((iSize / 1024) * 100) / 100;
		

		if (size > 2){
			alert("Exceeded maximum size limit 2MB");			
			jQuery("#resourceFlag").val(1);	
			obj.val('');	

		}else{
			jQuery("#resourceFlag").val(0);
		}

	}
	//Areas of Expertise id = UserReferenceAreaOfExpertise
	function ValidateAreaExpertise(str){
		
		//blank check
		if(str ==''){
			jQuery('#UserReferenceAreaOfExpertise').css('border-color','#F00');	
			jQuery('#UserReferenceAreaOfExpertise').css('border-width','<?php e(ERROR_BORDER);?>px');
			//jQuery('#UserReferenceAreaOfExpertise').focus();
			jQuery('#area_of_expertise').html('');
			jQuery('#area_of_expertise').css('color','#F00');		
			jQuery('#area_of_expertise').css('border-width','<?php e(ERROR_BORDER);?>px');
			return false;
		}else{
			if(str.indexOf(",") !=-1){ // checking comma
				var expertArr = str.split(",");
				var length = expertArr.length;
				if(length >3){
				
					jQuery('#UserReferenceAreaOfExpertise').css('border-color','#F00');	
					jQuery('#UserReferenceAreaOfExpertise').css('border-width','<?php e(ERROR_BORDER);?>px');
					//jQuery('#UserReferenceAreaOfExpertise').focus();
					jQuery('#area_of_expertise').html('Please enter 3 areas of expertise separated by commas.')
					jQuery('#area_of_expertise').css('color','#F00');		
					jQuery('#area_of_expertise').css('border-width','<?php e(ERROR_BORDER);?>px');		
					return false;
				}else{
					for(i = 0;i<length;i++){
						if(expertArr[i] ==''){
							jQuery('#UserReferenceAreaOfExpertise').css('border-color','#F00');	
							jQuery('#UserReferenceAreaOfExpertise').css('border-width','<?php e(ERROR_BORDER);?>px');
							//jQuery('#UserReferenceAreaOfExpertise').focus();
							jQuery('#area_of_expertise').html('Please enter 3 areas of expertise separated by commas.')
							jQuery('#area_of_expertise').css('color','#F00');
							jQuery('#area_of_expertise').css('border-width','<?php e(ERROR_BORDER);?>px');
							return false;
						}					
					}
					
				}
			}
			
			
		}
		return true;

	}
</script>
