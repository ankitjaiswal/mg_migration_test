<head>
           <?php {
             $name = explode(".", $this->data['User']['url_key']);
            }?>
           <meta property="og:title" content="<?php echo e(ucfirst($name[0]." ".$name[1]));?>'s Badge - Mentors Guild" />
           <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, mentors guild, management consultant" />
	    <meta name="description"  property="og:description" content="Mentors Guild is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />

	     <title><?php echo e(ucfirst($name[0]." ".$name[1]));?> Badge - Mentors Guild</title>
</head>
<div id="inner-content-wrapper" style="height:110px;">
    <div id="inner-content-box" >
     <?php if($this->Session->read('badgetype') == 'answers1'){
             $type = $this->Session->read('badgetype');
            }
          elseif($this->Session->read('badgetype') == 'answers2'){
           $type = $this->Session->read('badgetype');
          }
           elseif($this->Session->read('badgetype') == 'roundtable1'){
           $type = $this->Session->read('badgetype');
          }
          else{
           $type = $this->Session->read('badgetype');
          }
         ?>
      <?php if($type == 'answers1'){?>
   <div class="mg_answers1container">
    <div class="mg_photo">
              <?php 
					  	$urlk1 = ($this->data['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1)."#answers"; ?>
					 <?php 
					 if(!empty($this->data['UserImage'])){
						if(file_exists(WWW_ROOT . 'img' . '/' .MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'])){ 
							e($html->link($html->image(MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'], array('class'=>'desaturate','alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank')));                       					
						}else{				
							e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  
						}
						
					}else{
						e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  

					}
					?>
    </div>
    <div class="mg_content">
        <div class="mg_name">
          <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>#answers"style="text-decoration:none;color:#fff;"target="_blank"><?php e(ucfirst($this->data['UserReference']['first_name']));?> <?php e($this->data['UserReference']['last_name']); ?></a>
        </div>
        <div class="mg_answers1subhead">
            <?php
				$disp="mentorsguild.com/".strtolower($this->data['User']['url_key']);
				$completelink=SITE_URL.strtolower($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($this->data['User']['url_key']);
                            ?>
             <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>#answers"style="text-decoration:none;color:#fff;"target="_blank">Read my business advice</a>
	</div>
        <div class="mg_logo">
            <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>#answers"style="text-decoration:none;"target="_blank"><img src="<?php e(SITE_URL)?>/imgs/answers_r.png"></a>
        </div>
         <div class="mg_answerbutton">
           <a style="text-decoration:none;color:white;background-color:#f32c33;font-family:'proximanova', Ubuntu;font-weight:normal;font-size:14px;text-align:center;border-style:none;" target="_blank" type="button" href="<?php e(SITE_URL."qanda");?>">Ask a question</a>
        </div>
    </div>
</div>

<?php } elseif($type == 'answers2'){?>
   <div class="mg_answers2container">
    <div class="mg_photo">
            <?php 
					  	$urlk1 = ($this->data['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1); ?>
					 <?php 
					 if(!empty($this->data['UserImage'])){
						if(file_exists(WWW_ROOT . 'img' . '/' .MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'])){ 
							e($html->link($html->image(MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'], array('class'=>'desaturate','alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank')));                       					
						}else{				
							e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  
						}
						
					}else{
						e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  

					}
					?>
    </div>
    <div class="mg_content">
        <div class="mg_name">
           <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>#answers"style="text-decoration:none;color:#333;"target="_blank"><?php e(ucfirst($this->data['UserReference']['first_name']));?> <?php e($this->data['UserReference']['last_name']); ?></a>
        </div>
        <div class="mg_answers2subhead">
            <?php
				$disp="mentorsguild.com/".strtolower($this->data['User']['url_key']);
				$completelink=SITE_URL.strtolower($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($this->data['User']['url_key']);
                            ?>
                  <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>#answers"style="text-decoration:none;color:#333;"target="_blank">Read my business advice</a>
	</div>
        <div class="mg_logo">
            <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>#answers"style="text-decoration:none;"target="_blank"><img src="<?php e(SITE_URL)?>/imgs/answers.png"></a>
        </div>
         <div class="mg_answerbutton">
           <a style="text-decoration:none;color:white;background-color:#f32c33;font-family:'proximanova', Ubuntu;font-weight:normal;font-size:14px;text-align:center;border-style:none;" target="_blank" type="button" href="<?php e(SITE_URL."qanda");?>">Ask a question</a>
          </div>
    </div>
</div>
<?php }elseif($type == 'roundtable1'){?>
   <div class="mg_answers1container">
    <div class="mg_photo">
             <?php 
					  	$urlk1 = ($this->data['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1); ?>
					 <?php 
					 if(!empty($this->data['UserImage'])){
						if(file_exists(WWW_ROOT . 'img' . '/' .MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'])){ 
							e($html->link($html->image(MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'], array('class'=>'desaturate','alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank')));                       					
						}else{				
							e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  
						}
						
					}else{
						e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  

					}
					?>
           
    </div>
    <div class="mg_content">
        <div class="mg_name">
            <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>"style="text-decoration:none;color:#fff;"target="_blank"><?php e(ucfirst($this->data['UserReference']['first_name']));?> <?php e($this->data['UserReference']['last_name']); ?></a>
          </div>
        <div class="mg_answers1subhead">
            <?php
				$disp="mentorsguild.com/".strtolower($this->data['User']['url_key']);
				$completelink=SITE_URL.strtolower($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($this->data['User']['url_key']);?>
             <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>"style="text-decoration:none;color:#fff;"target="_blank">Let's discuss your key challenge</a>
	</div>
        <div class="mg_roundtablelogo">
           <a href ="<?php e(SITE_URL."roundtable");?>"style="text-decoration:none;"target="_blank"> <img src="<?php e(SITE_URL)?>/imgs/executive_roundtable_r.png"></a>
        </div>
         <div class="mg_roundtablebutton">
           <a style="text-decoration:none;color:white;background-color:#f32c33;font-family:'proximanova', Ubuntu;font-weight:normal;font-size:14px;text-align:center;border-style:none;" target="_blank" type="button" href="<?php e(SITE_URL."roundtable");?>">Learn more</a>
   </div>
    </div>
</div>
<?php }else if($type == 'roundtable2'){?>
 <div class="mg_answers2container">
    <div class="mg_photo">
            <?php 
					  	$urlk1 = ($this->data['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1); ?>
					 <?php 
					 if(!empty($this->data['UserImage'])){
						if(file_exists(WWW_ROOT . 'img' . '/' .MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'])){ 
							e($html->link($html->image(MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'], array('class'=>'desaturate','alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank')));                       					
						}else{				
							e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  
						}
						
					}else{
						e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  

					}
					?>
    </div>
    <div class="mg_content">
        <div class="mg_name">
         <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>"style="text-decoration:none;color:#333;"target="_blank"><?php e(ucfirst($this->data['UserReference']['first_name']));?> <?php e($this->data['UserReference']['last_name']); ?></a>
         </div>
        <div class="mg_answers2subhead">
            <?php
				$disp="mentorsguild.com/".strtolower($this->data['User']['url_key']);
				$completelink=SITE_URL.strtolower($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($this->data['User']['url_key']);
                            ?>
         <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>"style="text-decoration:none;color:#333;"target="_blank">Let's discuss your key challenge</a>
	</div>
        <div class="mg_roundtablelogo">
            <a href ="<?php e(SITE_URL."roundtable");?>"style="text-decoration:none;"target="_blank"><img src="<?php e(SITE_URL)?>/imgs/executive_roundtable.png"></a>
        </div>
         <div class="mg_roundtablebutton">
           <a style="text-decoration:none;color:white;background-color:#f32c33;font-family:'proximanova', Ubuntu;font-weight:normal;font-size:14px;text-align:center;border-style:none;" target="_blank" type="button" href="<?php e(SITE_URL."roundtable");?>">Learn more</a>
              </div>
    </div>
</div>

<?php }else if($type == 'answers_v_1'){?>
   <div class="mg_answers_1_vcontainer">
    <div class="mg_1_vphoto">
              <?php 
					  	$urlk1 = ($this->data['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1)."#answers"; ?>
					 <?php 
					 if(!empty($this->data['UserImage'])){
						if(file_exists(WWW_ROOT . 'img' . '/' .MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'])){ 
							e($html->link($html->image(MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'], array('class'=>'desaturate','alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank')));                       					
						}else{				
							e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  
						}
						
					}else{
						e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  

					}
					?>
    </div>
    <div class="mg_1_vcontent">
        <div class="mg_1_vname">
          <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>#answers"style="text-decoration:none;color:#fff;"target="_blank"><?php e(ucfirst($this->data['UserReference']['first_name']));?> <?php e($this->data['UserReference']['last_name']); ?></a>
        </div>
        <div class="mg_1_vsubhead">
            <?php
				$disp="mentorsguild.com/".strtolower($this->data['User']['url_key']);
				$completelink=SITE_URL.strtolower($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($this->data['User']['url_key']);
                            ?>
             <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>#answers"style="text-decoration:none;color:#fff;"target="_blank">Read my business advice</a>
	</div>
        <div class="mg_answers_vlogo">
            <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>#answers"style="text-decoration:none;"target="_blank"><img src="<?php e(SITE_URL)?>/imgs/answers_r.png"></a>
        </div>
         <div class="mg_1_vbutton">
           <a style="text-decoration:none;color:white;background-color:#f32c33;font-family:'proximanova', Ubuntu;font-weight:normal;font-size:14px;text-align:center;border-style:none;" target="_blank" type="button" href="<?php e(SITE_URL."qanda");?>">Ask a question</a>
        </div>
    </div>
</div>

<?php } elseif($type == 'answers_v_2'){?>
   <div class="mg_answers_2_vcontainer">
    <div class="mg_1_vphoto">
            <?php 
					  	$urlk1 = ($this->data['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1); ?>
					 <?php 
					 if(!empty($this->data['UserImage'])){
						if(file_exists(WWW_ROOT . 'img' . '/' .MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'])){ 
							e($html->link($html->image(MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'], array('class'=>'desaturate','alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank')));                       					
						}else{				
							e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  
						}
						
					}else{
						e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  

					}
					?>
    </div>
    <div class="mg_1_vcontent">
        <div class="mg_1_vname">
           <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>#answers"style="text-decoration:none;color:#333;"target="_blank"><?php e(ucfirst($this->data['UserReference']['first_name']));?> <?php e($this->data['UserReference']['last_name']); ?></a>
        </div>
        <div class="mg_1_vsubhead">
            <?php
				$disp="mentorsguild.com/".strtolower($this->data['User']['url_key']);
				$completelink=SITE_URL.strtolower($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($this->data['User']['url_key']);
                            ?>
                  <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>#answers"style="text-decoration:none;color:#333;"target="_blank">Read my business advice</a>
	</div>
        <div class="mg_answers_vlogo">
            <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>#answers"style="text-decoration:none;"target="_blank"><img src="<?php e(SITE_URL)?>/imgs/answers.png"></a>
        </div>
         <div class="mg_1_vbutton">
           <a style="text-decoration:none;color:white;background-color:#f32c33;font-family:'proximanova', Ubuntu;font-weight:normal;font-size:14px;text-align:center;border-style:none;" target="_blank" type="button" href="<?php e(SITE_URL."qanda");?>">Ask a question</a>
          </div>
    </div>
</div>

<?php }elseif($type == 'roundtable_v_1'){?>
   <div class="mg_answers_1_vcontainer">
    <div class="mg_1_vphoto">
             <?php 
					  	$urlk1 = ($this->data['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1); ?>
					 <?php 
					 if(!empty($this->data['UserImage'])){
						if(file_exists(WWW_ROOT . 'img' . '/' .MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'])){ 
							e($html->link($html->image(MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'], array('class'=>'desaturate','alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank')));                       					
						}else{				
							e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  
						}
						
					}else{
						e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  

					}
					?>
           
    </div>
    <div class="mg_1_vcontent">
        <div class="mg_1_vname">
            <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>"style="text-decoration:none;color:#fff;"target="_blank"><?php e(ucfirst($this->data['UserReference']['first_name']));?> <?php e($this->data['UserReference']['last_name']); ?></a>
          </div>
        <div class="mg_1_vsubhead">
            <?php
				$disp="mentorsguild.com/".strtolower($this->data['User']['url_key']);
				$completelink=SITE_URL.strtolower($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($this->data['User']['url_key']);?>
             <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>"style="text-decoration:none;color:#fff;"target="_blank">Let's discuss your key challenge</a>
	</div>
        <div class="mg_roundtable_vlogo">
           <a href ="<?php e(SITE_URL."roundtable");?>"style="text-decoration:none;"target="_blank"> <img src="<?php e(SITE_URL)?>/imgs/executive_roundtable_r.png"></a>
        </div>
         <div class="mg_1_vbutton">
           <a style="text-decoration:none;color:white;background-color:#f32c33;font-family:'proximanova', Ubuntu;font-weight:normal;font-size:14px;text-align:center;border-style:none;" target="_blank" type="button" href="<?php e(SITE_URL."roundtable");?>">Learn more</a>
   </div>
    </div>
</div>

<?php }else if($type == 'roundtable_v_2'){?>
 <div class="mg_answers_2_vcontainer">
    <div class="mg_1_vphoto">
            <?php 
					  	$urlk1 = ($this->data['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1); ?>
					 <?php 
					 if(!empty($this->data['UserImage'])){
						if(file_exists(WWW_ROOT . 'img' . '/' .MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'])){ 
							e($html->link($html->image(MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'], array('class'=>'desaturate','alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank')));                       					
						}else{				
							e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  
						}
						
					}else{
						e($html->link($html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  

					}
					?>
    </div>
    <div class="mg_1_vcontent">
        <div class="mg_1_vname">
         <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>"style="text-decoration:none;color:#333;"target="_blank"><?php e(ucfirst($this->data['UserReference']['first_name']));?> <?php e($this->data['UserReference']['last_name']); ?></a>
         </div>
        <div class="mg_1_vsubhead">
            <?php
				$disp="mentorsguild.com/".strtolower($this->data['User']['url_key']);
				$completelink=SITE_URL.strtolower($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($this->data['User']['url_key']);
                            ?>
         <a href ="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>"style="text-decoration:none;color:#333;"target="_blank">Let's discuss your key challenge</a>
	</div>
        <div class="mg_roundtable_vlogo">
            <a href ="<?php e(SITE_URL."roundtable");?>"style="text-decoration:none;"target="_blank"><img src="<?php e(SITE_URL)?>/imgs/executive_roundtable.png"></a>
        </div>
         <div class="mg_1_vbutton">
           <a style="text-decoration:none;color:white;background-color:#f32c33;font-family:'proximanova', Ubuntu;font-weight:normal;font-size:14px;text-align:center;border-style:none;" target="_blank" type="button" href="<?php e(SITE_URL."roundtable");?>">Learn more</a>
              </div>
    </div>
</div>

<?php }else if($type == 'logo1'){?>

<div class="mg_logo1container">
    <div class="mg_content">
        <div class="mg_logo1">
            <a href="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>" target="_blank""><img src="<?php e(SITE_URL)?>/imgs/mg_logo.png" title="company logo 1" alt="company logo 1"></a>
        </div>

    </div>
</div>


<?php }else if($type == 'logo2'){?>

<div class="mg_logo2container">
    <div class="mg_content">
        <div class="mg_logo1">
            <a href="<?php e(SITE_URL);?><?php e(strtolower($this->data['User']['url_key']));?>" target="_blank""><img src="<?php e(SITE_URL)?>/imgs/mg_logo_r.png" title="company logo 2" alt="company logo 2"></a>
        </div>

    </div>
</div><?php }else{?>
<?php }?>
</div>
</div>
<style>
@import url(http://fonts.googleapis.com/css?family=Ubuntu:300);
.mg_answers1container
{
    width:320px;
    height:90px;
    padding:10px;
    background-color:#333; /* #eee (light) */
    color:#fff; /* #333 (light) */
    font-family:Ubuntu, Arial, vardana;
}

.mg_answers1subhead
{
    font-family:'proximanova light', Ubuntu;
    font-size:13px;
    margin-bottom:26px;
}
.mg_answers2container
{
    width:320px;
    height:90px;
    padding:10px;
    background-color:#eee; /* #eee (light) */
    color:#333; /* #333 (light) */
    font-family:Ubuntu, Arial, vardana;
}
.mg_content
{
    display:block;
    padding-top:0;
    position:relative;
    bottom:0;
}
.mg_photo img
{
    display:block;
    width:90px;
    height:90px;
    float:left;
    margin-right:12px;
}
.mg_name
{
    font-family:'proximanova semibold', Ubuntu;
    font-weight:bold;
    font-size:15px;
}
.mg_answers2subhead
{
    font-family:'proximanova light', Ubuntu;
    font-size:13px;
    margin-bottom:26px;
}
.mg_logo img
{
    width:71px;
    height:20px;
    margin-top:8px;
    float:left;
    margin-right:10px;
}
.mg_answerbutton
{
    color:white;
    background-color:#f32c33;
    border-style:none;
    font-family:'proximanova', Ubuntu;
    font-weight:normal;
    font-size:14px;
    padding:8px 4px 4px 4px;
    text-align:center;
    width:126px;
    height:20px;
    float:right;
}
  .mg_roundtablelogo img
    {
        width:100px;
        height:30px;
        float:left;
        margin-right:10px;
    }
.mg_roundtablebutton
    {
        color:white;
        background-color:#f32c33;
        border-style:none;
        font-family:'proximanova', Ubuntu;
        font-weight:normal;
        font-size:14px;
        padding:8px 4px 4px 4px;
        text-align:center;
        width:100px;
        height:20px;
        float:right;
    }
  .mg_logo1container
    {
        width:200px;
        height:50px;
        padding:10px;
        background-color:#eee; /* #eee (light) */
        color:#333; /* #333 (light) */
        font-family:Ubuntu, Arial, vardana;
    }
 .mg_logo1 img
    {
        width:158px;
        height:24px;
        float:left;
        margin-right:10px;
    }
.mg_logo2container
    {
        width:200px;
        height:50px;
        padding:10px;
        background-color:#333; /* #eee (light) */
        color:#fff; /* #333 (light) */
        font-family:Ubuntu, Arial, vardana;
    }
.mg_answers_1_vcontainer
    {
        width:120px;
        height:auto;
        padding:10px;
        background-color:#333; /* #eee (light) */
        color:#fff; /* #333 (light) */
        font-family:Ubuntu, Arial, vardana;
    }    
      .mg_1_vcontent
    {
        display:block;
        padding-top:6px;
        position:relative;
        bottom:0;
        clear:both;
    }
    .mg_1_vphoto img
    {
        display:block;
        width:120px;
        height:120px;
        margin-right:12px;
    }
    .mg_1_vname
    {
        font-family:'proximanova semibold', Ubuntu;
        font-weight:bold;
        font-size:16px;
    }
    .mg_1_vsubhead
    {
        font-family:'proximanova light', Ubuntu;
        font-size:13px;
        margin-bottom:12px;
        margin-top:6px;
    }
    .mg_roundtable_vlogo img
    {
        width:100px;
        height:30px;
    }
    .mg_answers_vlogo img
    {
      //margin-left:15px;
      width:71px;
      height:20px;
      
    }
    .mg_1_vbutton
    {
        color:white;
        background-color:#f32c33;
        border-style:none;
        font-family:'proximanova', Ubuntu;
        font-weight:normal;
        font-size:14px;
        padding:8px 4px 4px 4px;
        text-align:center;
        width:112px;
        height:20px;
        clear:both;
        margin-top:12px;
    }
.mg_answers_2_vcontainer
    {
        width:120px;
        height:auto;
        padding:10px;
        background-color:#eee; /* #eee (light) */
        color:#333; /* #333 (light) */
        font-family:Ubuntu, Arial, vardana;
    }    
   
</style>