
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth" style="margin-bottom:50px;">
 	    <div id="user-account">
	      <div class="account-form">	    
		  	<div class="onbanner">
		          <h1>                 
		            <div style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;"><span style="text-decoration: underline;">Member Services</span></div></h1>
                        
				  <div style="position:relative;top:80px;"><p><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal;font-size: 18px;">Mentors Guild helps experienced consultants attract more clients.</span></div>	
			</div>

                     <br><br><br><br>
                    <div class ="expertise">
                    <h2 style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">1. <a href="<?php echo SITE_URL?>members/pr" style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">PR Opportunities</a></h2>
                    <p style="font-size:16px;">Send press releases and get media mentions. <a href="<?php echo SITE_URL?>members/pr" >Learn More</a>.</p>
                   </div>
                   <div class ="expertise">
                    <h2 style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">2. <a href="<?php echo SITE_URL?>members/premium_membership" style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;" >Premium Membership</a></h2>
                    <p style="font-size:16px;">Increase your visibility on Mentors Guild, Linked In and beyond. <a href="<?php echo SITE_URL?>members/premium_membership" >Learn More</a>.</p>
                   </div>
                   <div class ="expertise">
                    <h2 style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">3. <a href="<?php echo SITE_URL?>members/premium_website"style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;" >Premium Website</a></h2>
                    <p style="font-size:16px;">Get more visitors on your website, increase their engagement level and convert them to prospective clients. <a href="<?php echo SITE_URL?>members/premium_website" >Learn More</a>.</p>
                   </div>

                   <div class ="expertise">
                    <h2 style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">4. <a href="<?php echo SITE_URL?>project/create" style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">Post a Project</a></h2>
                    <p style="font-size:16px;">Subcontract projects to fellow members. <a href="<?php echo SITE_URL?>project/create" >Learn More</a>.</p>
                   </div>
                   <?php if($this->Session->read('Auth.User.id') !='' && $this->Session->read('Auth.User.role_id') == '2') {?>
                   <div class ="expertise">
                    <h2 style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">5. Special Features</h2>
                    <ul>
                    <li style="font-size:16px;margin-top:12px;">&#8211; Badges, email signature, etc. <a href="<?php echo SITE_URL?>members/my_badges">Learn More</a>.</li>
                    <li style="font-size:16px;">&#8211; Business Documents by Flevy. <a href="<?php echo SITE_URL?>members/flevy">Learn More</a>.</li>
                    <li style="font-size:16px;margin-bottom:20px;">&#8211; My Takeaway: Links that drive traffic to your website every time you share. <a href="<?php echo SITE_URL?>members/takeaway_input">Learn More</a>.</li>
                    </ul>
                    
                   </div>
                   <?php }else{?>
                   <div class ="expertise">
                    <h2 style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">5. Special Features</h2>
                    <ul>
                    <li style="font-size:16px;margin-top:12px;">&#8211; Badges, email signature, etc. <a href="javascript:void(0)" onclick='javascript:loginpopup();'>Learn More</a>.</li>
                    <li style="font-size:16px;">&#8211; Business Documents by Flevy. <a href="javascript:void(0)" onclick='javascript:loginpopup();'>Learn More</a>.</li>
                    <li style="font-size:16px;margin-bottom:20px;">&#8211; My Takeaway: Links that drive traffic to your website every time you share. <a href="javascript:void(0)" onclick='javascript:loginpopup();'>Learn More</a>.</li>
                    </ul>
                   </div>
                     <?php }?>
                   <div class ="expertise">
                    <h2 style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">6. <a href="<?php echo SITE_URL?>members/invitation_eligibility" style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;" >Invite a member</a></h2>
                    <p style="font-size:16px;">Invite other qualified consultants to join Mentors Guild. <a href="<?php echo SITE_URL?>members/invitation_eligibility" >Learn More</a>.</p>
                   </div>
                  <br/>
                  <div class="mg_quote4_container" onclick="window.open('<?php e(SITE_URL);?>chip.evans','mywindow');" style="cursor: hand;">
		    <div class="mg_photo">
		        <img src="<?php e(SITE_URL)?>/imgs/ChipEvans.jpg">
		    </div>

		    <div class="mg_quote_box">

			    <div class="premium-testimonial-content">
				<div class="premium-testimonial2-quote">Mentors Guild provided my consulting company, The Evans Group LLC, with an opportunity to work with and consult for Mr. Dave Evans (no relation to our firm), who contracted with me for counsel on startup companies, mergers & acquisitions, and venture & angel capital infusion.</br></br>Mentors Guild was extraordinary in how they represented the client looking for consulting guidance and in organizing and following up to ensure the consulting times and description of work were clear.</br>
</br>I'd highly recommend working with Mentors Guild and am very pleased with our relationship.</div>
				<div class="premium-testimonial2-name">Chip Evans, Ph.D</div>
				<div class="premium-testimonial2-org">President & Founder at The Evans Group LLC</div>
		    </div></div>
		</div>



</div>
</div>
</div>
</div>


<?php e($html->css(array('plans'))); ?>
























