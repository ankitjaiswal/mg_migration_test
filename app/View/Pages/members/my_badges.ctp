
<script type="text/javascript" src="<?php e(SITE_URL)?>js/mg_iframe.js"></script>
<style>
.signbox{
    background-color: #fff;
    width: 658px;
    height: 220px;
    padding: 25px;
    border: 1px solid #DDDDDD;
   margin: 13px;
}
a:hover {
    cursor: pointer;
    cursor: hand;
}
</style>

<div id="inner-content-wrapper">
                <div class="pagewidth" id="inner-content-box" style="margin-bottom:40px;margin-top:40px;">
                    <div class="Profile_page_container">
                        <div class="badge-body">
                       <div class="row">
                          <div class ="signbox" contenteditable="true">
  
  <table width="100%" border="0" cellpadding="0">
    <tbody>
      <tr>
        <td align="left" valign="top" width="10">

          <p style="margin-right:10px;font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:14px">


              <?php $link = (SITE_URL."img/members/profile_images".'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name']);
					  	
						$displink1=$this->data['UserReference']['business_website'];
                  ?>
            <a style="text-decoration:none" href="<?php e($displink1);?>" target="_blank" >
                <img src="<?php e($link);?>" alt="Mentors Guild" border="0" height="80" width="80">
            </a>

          </p>
        </td>
        <td align="left">
          <p style="font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:14px;color:rgb(33,33,33)"><span style="font-weight:bold;color:rgb(33,33,33);display:inline"><?php e(ucfirst($this->data['UserReference']['first_name']));?> <?php e($this->data['UserReference']['last_name']); ?></span>
         
      <span style="display:inline"></br></span>
      <a href="mailto:<?php e($this->data['User']['username']);?>" style="color:rgb(208,49,53);text-decoration:none;display:inline" target="_blank"><?php e($this->data['User']['username']);?></a><span style="color:rgb(33,33,33)"></span></p>
    <p style="font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:14px">
        <span style="font-weight:bold;color:rgb(33,33,33);display:inline"><?php e(ucfirst($this->data['UserReference']['linkedin_headline']));?></span>
			<?php 
			  $modeTypeFaceToFace=$modeTypePhone=$modeTypeVideo='FALSE' ;
			  $modeValue1=$modeValue2=$modeValue3='';
			  if(isset($this->data['Communication'][0]['mode_type']) && $this->data['Communication'][0]['mode_type'] != '')
			  {
			  		
					if($this->data['Communication'][0]['mode_type']=='face_to_face')
				{
						$modeTypeFaceToFace='TRUE';
			  			$modeValue1=$this->data['Communication'][0]['mode'];
					}
					else if($this->data['Communication'][0]['mode_type']=='phone')
						{
						  $modeTypePhone='TRUE';
			  			  $modeValue2=$this->data['Communication'][0]['mode'];
			  			}
			  			
			  			else
			  			{
			  			   $modeTypeVideo='TRUE';
			  			   $modeValue3=$this->data['Communication'][0]['mode'];
					 }
					
	
				
			}
			  if(isset($this->data['Communication'][1]['mode_type']) && $this->data['Communication'][1]['mode_type'] != '')
			  {
			  		
					if($this->data['Communication'][1]['mode_type']=='face_to_face')
				{
						$modeTypeFaceToFace='TRUE';
			  			$modeValue1=$this->data['Communication'][1]['mode'];
					}
					else if($this->data['Communication'][1]['mode_type']=='phone')
						{
						   $modeTypePhone='TRUE';
			  		       $modeValue2=$this->data['Communication'][1]['mode'];
			  			}
			  			
			  			else
			  			{
			  			   $modeTypeVideo='TRUE';
			  			   $modeValue3=$this->data['Communication'][1]['mode'];
					 }
					
		
				
			}
			 if(isset($this->data['Communication'][2]['mode_type']) && $this->data['Communication'][2]['mode_type'] != '')
			  {
			  		if($this->data['Communication'][2]['mode_type']=='face_to_face')
					{
						$modeTypeFaceToFace='TRUE';
			  			$modeValue1=$this->data['Communication'][2]['mode'];
					}
						else if($this->data['Communication'][2]['mode_type']=='phone')
						{
						   $modeTypePhone='TRUE';
			  		       $modeValue2=$this->data['Communication'][2]['mode'];
			  			}
			  			
			  			else
			  			{
			  			   $modeTypeVideo='TRUE';
			  			   $modeValue3=$this->data['Communication'][2]['mode'];
					 }
					
					
			  }
			
			
				 ?>
       <?php if($modeTypePhone=='TRUE' && trim($modeValue2)!='') {?>
        <span style="display:inline"><br></span>
         <span style="color:rgb(33,33,33);display:inline"><?php echo $modeValue2; ?></span> 
         <?php }?>
        <?php if($city != '') {?>
                        

        
        <span style="display:block"></span> <span style="color:rgb(33,33,33);display:inline"><?php echo e($city) ;?>, <?php echo  e($state) ;?></span>
        <?php }?>
                  <?php if(($this->data['UserReference']['business_website']) !=''){
                     $link ='';
                     $url ='';
                       if(($pos =strpos($this->data['UserReference']['business_website'],'http'))!==false)
						{
							$link=$this->data['UserReference']['business_website'];
						}
						else
						{
							$link="https://".$this->data['UserReference']['business_website'];
						}
                                         ?>
        <span style="display:inline"><br></span>
        <a href="<?php e($link);?>" style="color:rgb(208,49,53);text-decoration:none;display:inline" target="_blank"><?php e($link);?></a>
          <?php }?>
    </p>


<?php
					$socialCount=0;
					$linkedinURL = '';
                                   $twitterURL ='';
                                   $googleURL ='';
                                   $facebookURL ='';
                                   $youtubeURL ='';
					while(count($this->data['Social'])>$socialCount)
					{
					
					if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'http'))!==false)
						{
							$link=$this->data['Social'][$socialCount]['social_name'];
						}
						else
						{
							$link="https://".$this->data['Social'][$socialCount]['social_name'];
						}
						
						$icon = $this->General->getIcon($this->data['Social'][$socialCount]['social_name']);
						if($icon == 'media/linkedin.png')
							$linkedinURL = $link;
                                          if($icon == 'media/twitter.png')
							$twitterURL = $link;
                                          if($icon == 'media/Google+.png')
							$googleURL = $link;
                                          if($icon == 'media/facebook.png')
							$facebookURL = $link;
						if($icon == 'media/youtube.png')
							$youtubeURL = $link;
						$socialCount++;
						}?>
		
<p style="font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:14px">
<?php if($linkedinURL !=''){
                   if(($pos =strpos($linkedinURL,'http'))!==false)
						{
							$link=$linkedinURL;
						}
						else
						{
							$link="https://".$linkedinURL;
						}?>		     

      

<a style="display:inline;text-decoration:none" href="<?php e($link);?>" target="_blank">
  <img width="24" style="margin-bottom:5px;border:none;display:inline" height="24" src="<?php e(SITE_URL)?>sign_imgs/linkedin.png" alt="Linkedin">
</a>
<?php }?>
<?php 					  	$urlk1 = ($this->data['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1);
                                        ?>
<a style="text-decoration:none" href="<?php e($displink1);?>">
  <img width="24" style="margin-bottom:5px;border:none;" height="24" src="<?php e(SITE_URL)?>sign_imgs/mg_monogram.png" alt="Mentors Guild Profile">
</a>

<?php if($twitterURL !=''){?>
<a style="text-decoration:none;display:inline" href="<?php e($twitterURL);?>" target="_blank">
  <img width="24" style="margin-bottom:5px;border:none;display:inline" height="24" src="<?php e(SITE_URL)?>sign_imgs/twitter.png" alt="Twitter">
</a>
<?php }?>

<?php if($facebookURL !=''){?>
        <a style="text-decoration:none;display:inline" href="<?php e($facebookURL);?>" target="_blank">
  <img width="24" style="margin-bottom:5px;border:none;display:inline" height="24" src="<?php e(SITE_URL)?>sign_imgs/facebook.png" alt="Facebook">
</a>
<?php }?>
<?php if($googleURL !=''){?>
<a style="text-decoration:none;display:inline" href="<?php e($googleURL);?>" target="_blank">
  <img width="24" style="margin-bottom:5px;border:none;display:inline" height="24" src="<?php e(SITE_URL)?>sign_imgs/googleplus.png" alt="Google Plus">
</a>
<?php }?>

<?php if($youtubeURL !=''){?>
<a style="text-decoration:none;display:inline" href="<?php e($youtubeURL);?>" target="_blank">
  <img width="24" style="margin-bottom:5px;border:none;display:inline" height="24" src="<?php e(SITE_URL)?>sign_imgs/youtube.png" alt="Youtube">
</a>
<?php }?>





</p>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          
        </td>
      </tr>
      <tr>
        <td colspan="2">
          

        </td>
      </tr>
     <tr>
        <td colspan="2"><p style="font-family:Helvetica,Arial,sans-serif;color:rgb(146,146,146);font-size:12px;line-height:12px">NOTICE: The contents of this email and any attachments are confidential. They are intended for the named recipient(s) only. If you have received this email by mistake, please notify the sender immediately.</p></td>
      </tr>

    </tbody>
  </table>

                                    
                                    
                                
                            </div>
                         </div>
                            <div class="row">
                                <div class="container_box">
                                    <h3><strong>Mentors Guild logo</strong> (light)</h3>

                                    <iframe frameborder="0" height="48"marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=logo1" width="180"></iframe>
                                    <div class="overlay" onclick="TINY.box.show({html:document.getElementById('mg-div').innerHTML,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">
                                    <p>Click for code snippet.</p>
                                    </div>
                                </div>


                                <div class="container_box">
                                    <h3><strong>Mentors Guild logo</strong> (dark)</h3>

                                    <iframe frameborder="0" height="48"marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=logo2"width="180"></iframe>
                                    <div class="overlay" onclick="TINY.box.show({html:document.getElementById('mg_r-div').innerHTML,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">
                                    <p>Click for code snippet.</p>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="container_box">
                                    <h3><strong>Answers</strong> (light)</h3>
                                    
                                    <iframe src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=answers2" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe>
                                    <div class="overlay-lg" onclick="TINY.box.show({html:document.getElementById('ans-div').innerHTML,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">
                                    <p>Click for code snippet.</p>
                                    </div>
                                </div>


                                <div class="container_box">
                                    <h3><strong>Answers</strong> (dark)</h3>
                                    
                                    <iframe src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=answers1" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe>
                                    <div class="overlay-lg" onclick="TINY.box.show({html:document.getElementById('ans_r-div').innerHTML,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">
                                    <p>Click for code snippet.</p>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="container_box">
                                    <h3><strong>Executive Roundtable</strong> (light)</h3>
                                    
                                    <iframe src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=roundtable2" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe>
                                    <div class="overlay-lg" onclick="TINY.box.show({html:document.getElementById('rt-div').innerHTML,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">
                                    <p>Click for code snippet.</p>
                                    </div>
                                </div>
               

                                <div class="container_box">
                                    <h3><strong>Executive Roundtable</strong> (dark)</h3>
                                   
                                    <iframe src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=roundtable1" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe>
                                    <div class="overlay-lg" onclick="TINY.box.show({html:document.getElementById('rt_r-div').innerHTML,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">
                                    <p>Click for code snippet.</p>
                                    </div>
                                </div>
                          </div>

                            <div class="row">
                              <div class="container_box">
                              <h3><strong>Answers</strong> (light, vertical)</h3>

                              <iframe frameborder="0" height="120" marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=answers_v_2" width="100%" id="mentorsguild-vr-2" onLoad="autoResize('mentorsguild-vr-2');"></iframe>
                              <div class="overlay-lg-v" onclick="TINY.box.show({html:document.getElementById('vans-div').innerHTML,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">
                             <p>Click for code snippet.</p>
                             </div>
                             </div>
                         
                              <div class="container_box">
                              <h3><strong>Answers</strong> (dark, vertical)</h3>

                              <iframe frameborder="0" height="120" marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=answers_v_1" width="100%" id="mentorsguild-vr-1" onLoad="autoResize('mentorsguild-vr-1');"></iframe>
                              <div class="overlay-lg-v" onclick="TINY.box.show({html:document.getElementById('vans_r-div').innerHTML,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">
                             <p>Click for code snippet.</p>
                             </div>
                             </div>
                          </div>
                          
                            <div class="row">
                              <div class="container_box">
                              <h3><strong>Executive Roundtable</strong> (light, vertical)</h3>

                              <iframe frameborder="0" height="120" marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=roundtable_v_2" width="100%" id="mentorsguild-vr-3" onLoad="autoResize('mentorsguild-vr-3');"></iframe>
                              <div class="overlay-lg-v" onclick="TINY.box.show({html:document.getElementById('vrt-div').innerHTML,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">
                             <p>Click for code snippet.</p>
                             </div>
                             </div>
                         
                              <div class="container_box">
                              <h3><strong>Executive Roundtable</strong> (dark, vertical)</h3>

                              <iframe frameborder="0" height="120" marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=roundtable_v_1" width="100%" id="mentorsguild-vr-4" onLoad="autoResize('mentorsguild-vr-4');"></iframe>
                              <div class="overlay-lg-v" onclick="TINY.box.show({html:document.getElementById('vrt_r-div').innerHTML,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">
                             <p>Click for code snippet.</p>
                             </div>
                             </div>
                          </div>      



                            <div class="hidden" id="mg-div">
                                <div class="container_box">
                                    <h3><strong>Mentors Guild logo</strong> (light)</h3>

                                    <iframe frameborder="0" height="48" marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=logo1" width="180"></iframe>
                                    <textarea cols="40" id="txtarea" rows="5"><iframe frameborder="0" height="48"marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=logo1" width="180"></iframe></textarea>
                                </div>
                            </div>



                        <div class="hidden" id="mg_r-div">
                            <div class="container_box">
                                <h3><strong>Mentors Guild logo</strong> (dark)</h3>

                                <iframe frameborder="0" height="48" marginheight="5" marginwidth="5" scrolling="no"src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=logo2" width="180"></iframe>
                                <textarea cols="40" id="txtarea" rows="5"><iframe frameborder="0" height="48" marginheight="5" marginwidth="5" scrolling="no"src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=logo2" width="180"></iframe></textarea>
                            </div>
                        </div>



                        <div class="hidden" id="ans-div">
                            <div class="container_box">
                                <h3><strong>Answers</strong> (light)</h3>

                                <iframe src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=answers2" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe>
                                <textarea cols="40" id="txtarea" rows="5"><iframe src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=answers2" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe></textarea>
                            </div>
                        </div>


                        <div class="hidden" id="ans_r-div">
                            <div class="container_box">
                                <h3><strong>Answers</strong> (dark)</h3>
                               
                                 <iframe src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=answers1" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe>
                                 <textarea cols="40" id="txtarea" rows="5"><iframe src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=answers1" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe></textarea>
                            </div>
                        </div>


                        <div class="hidden" id="rt-div">
                            <div class="container_box">
                                <h3><strong>Executive Roundtable</strong> (light)</h3>
                                
                                 <iframe src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=roundtable2" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe>
                                 <textarea cols="40" id="txtarea" rows="5"><iframe src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=roundtable2" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe></textarea>
                            </div>
                        </div>


                        <div class="hidden" id="rt_r-div">
                            <div class="container_box">
                                <h3><strong>Executive Roundtable</strong> (dark)</h3>
                                
                                <iframe src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=roundtable1" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe>
                                <textarea cols="40" id="txtarea" rows="5"><iframe src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=roundtable1" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe></textarea>
                            </div>
                        </div>

                            <div class="hidden" id="vans-div">
                            <div class="container_box">
                                <h3><strong>Answers</strong> (light, vertical)</h3>
                                
                                <iframe frameborder="0" height="120" marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=answers_v_2" width="100%" id="mentorsguild-vr-2" onLoad="autoResize('mentorsguild-vr-2');"></iframe>
                                <textarea cols="40" id="txtarea" rows="5"><script src="<?php e(SITE_URL)?>js/mg_iframe.js"></script><iframe frameborder="0" height="120" marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=answers_v_2" width="100%" id="mentorsguild-vr-2" onLoad="autoResize('mentorsguild-vr-2');"></iframe></textarea>
                            </div>
                        </div>


                        <div class="hidden" id="vans_r-div">
                            <div class="container_box">
                                <h3><strong>Answers</strong> (dark, vertical)</h3>
                              
                                 <iframe frameborder="0" height="120" marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=answers_v_1" width="100%" id="mentorsguild-vr-1" onLoad="autoResize('mentorsguild-vr-1');"></iframe>
                                 <textarea cols="40" id="txtarea" rows="5"><script src="<?php e(SITE_URL)?>js/mg_iframe.js"></script><iframe frameborder="0" height="120" marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=answers_v_1" width="100%" id="mentorsguild-vr-1" onLoad="autoResize('mentorsguild-vr-1');"></iframe></textarea>
                            </div>
                        </div>


                        <div class="hidden" id="vrt-div">
                            <div class="container_box">
                                <h3><strong>Executive Roundtable</strong> (light, vertical)</h3>
                               
                                 <iframe frameborder="0" height="120" marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=roundtable_v_2" width="100%" id="mentorsguild-vr-3" onLoad="autoResize('mentorsguild-vr-3');"></iframe>    
                                 <textarea cols="40" id="txtarea" rows="5"><script src="<?php e(SITE_URL)?>js/mg_iframe.js"></script><iframe frameborder="0" height="120" marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=roundtable_v_2" width="100%" id="mentorsguild-vr-3" onLoad="autoResize('mentorsguild-vr-3');"></iframe></textarea>
                            </div>
                        </div>


                        <div class="hidden" id="vrt_r-div">
                            <div class="container_box">
                                <h3><strong>Executive Roundtable</strong> (dark, vertical)</h3>
                                 
                                <iframe frameborder="0" height="120" marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=roundtable_v_1" width="100%" id="mentorsguild-vr-4" onLoad="autoResize('mentorsguild-vr-4');"></iframe>
                                <textarea cols="40" id="txtarea" rows="5"><script src="<?php e(SITE_URL)?>js/mg_iframe.js"></script><iframe frameborder="0" height="120" marginheight="5" marginwidth="5" scrolling="no" src="<?php e(SITE_URL)?>members/badges/?id=<?php e($this->data['User']['url_key']);?>&type=roundtable_v_1" width="100%" id="mentorsguild-vr-4" onLoad="autoResize('mentorsguild-vr-4');"></iframe></textarea>
                            </div>
                        </div>


                </div>


                <div class="badge-sidebar">
                    <div class="badge-sidebar-shim">
                    </div>


                    <div class="badge-sidebar-border">
                    </div>

                    
                    <h2 style="font-size: large;">How do I add signature to my email?</h2>
                   <br/>

                    <p>1. You can make minor changes on the left  (don't use Safari browser)</p>
                    <p>2. Once final, just Select and Copy</p>
                    <p>3. Paste in the Signature setting of your email service</p>

                    
                    <br/><br/>
                    <h2 style="font-size: large;">How do I add the badge to my website or blog?</h2>
                    <br/>

                    <p>Click on the image you want to embed, then copy and paste the code into the HTML of your site.</p>


                    
                    <br/><br/>
                    <h2 style="font-size: large;">Need help?</h2>

                     <br/>
                    


                    <p>Call 1-866-511-1898 or email us at <a href=
                    "mailto:help@mentorsguild.com">help@mentorsguild.com</a>.</p>
                     
            </div>
            </div>
        </div>
    </div>

