<?php e($html->css(array('regist_popup')));
if(isset($this->data['User']['id'])):
	$id = $this->data['User']['id'];
else:
	$id = 0;
endif;
?>

<div id="registerationFm" style="height: 512px; width: 512px; background-image: none;">
	<h1>Register as a client</h1>
	<div class="infobar"><!-- VL 27/12--><!-- VL 2/1/2013-->
		<p style="float:left; margin-top:12px; font-weight:normal;">&nbsp;&nbsp;&nbsp;</p><p style="float:left; padding-left:10px"><?php e($html->image('linked-in.png',array('alt'=>'Linkedin',"onclick"=>'linkedin("pricingPopUp","'.$id.'");','style'=>'cursor:pointer;'))); ?></p>
	</div><!-- VL 27/12-->
	<div class="infobar noborder"><!-- VL 2/1/2013-->
		<span>Or register using email id</span>
	</div>
	
<div class="clear">	
	<?php e($form->create('User',array('url'=>array('controller'=>'pricing','action'=>'password_setting_popup'),'id'=>'passwordSettingPopupForm'))); ?>	
	<div>
		<div class="floatL">
			<?php 
			if(isset($this->data['User']['id']) && isset($this->data['UserReference']['id']) && $id!=0){
				e($form->hidden('User.id',array('id'=>'popupId','div'=>false,'label'=>false)));
				e($form->hidden('UserReference.id',array('div'=>false,'label'=>false)));
			
			}
			e($form->input('UserReference.first_name',array('id'=>'fname','class'=>'forminput1','size'=>25,'label'=>false,'div'=>false,'placeholder'=>'First Name')));
			?>		
			<span class="errormsg" id="firstNameErr"></span>
		</div>
		<div class="floatL mr-lt20">
			<?php 
			e($form->input('UserReference.last_name',array('id'=>'lname','class'=>'forminput1','size'=>25,'label'=>false,'div'=>false,'placeholder'=>'Last Name')));
			?>
		</div>
		<div class="clear"></div>
	</div>
	<div>
		<?php 
		e($form->input('User.username',array('id'=>'email','class'=>'forminput Big','label'=>false,'div'=>false,'placeholder'=>'Email')));
		?>
		<div id="UserEmailErr" class="errormsg"></div>
	</div>
	<div>
	<?php 
	e($form->password('User.password2',array('class'=>'forminput Big','label'=>false,'div'=>false,'placeholder'=>'Password','style' => 'font-family:verdana;')));
	?>	
	<div id="PassLengtherr" class="errormsg"></div>			
	</div>
	<div>
	<?php 
	e($form->input('UserReference.zipcode',array('id'=>'zipcode','maxlength'=>'5','class'=>'forminput Big','label'=>false,'div'=>false,'placeholder'=>'ZIP')));
	?>
	<div id="zipError" class="errormsg"></div>	
	</div>	
	<div class="floatR" style="padding:8px 12px 0 0 !important;">
	<?php e($form->submit('Register',array('class'=>'btn','onclick'=>'return passwordSettingPopupValidate();'))); ?>
	</div>
	<?php e($form->end()); ?>	
	
</div>
</div>
