
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="google-site-verification" content="UroIkuqOwc9A-KngERWfg2Mam9Fw_0UmVECjjujMYMY" />
        <script type="text/javascript">var NREUMQ=NREUMQ||[];NREUMQ.push(["mark","firstbyte",new Date().getTime()]);</script>
		
        <link rel="icon" href="/favicon.ico?v=2" type="image/x-icon" />
        <link rel="shortcut icon" href="/favicon.ico?v=2" type="image/x-icon" />

        </head>

<body>


        <?php e($html->css(array('plans'))); ?>
	<div id="inner-content-wrapper">
	  <div id="inner-content-box" class="pagewidth"> 
	    <div id="user-account">
	      <div class="account-form">
		  	<div class="onbanner">
		          <h1>                 
		            <div id="spec1" style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;">From <span style="text-decoration: underline;">Executive Coaching</span> to <span style="text-decoration: underline;">New Product Launch</span></div>
					<div id="spec2" style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;display:none;">From <span style="text-decoration: underline;">Business Strategy</span> to <span style="text-decoration: underline;">Succession Planning</span></div>
					<div id="spec3" style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;display:none;">From <span style="text-decoration: underline;">Workplace Conflict</span> to <span style="text-decoration: underline;">Customer Service</span></div>
					<div id="spec4" style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;display:none;">From <span style="text-decoration: underline;">Mergers &amp; Acquisitions</span> to <span style="text-decoration: underline;">Change Management</span></div>
					<div id="spec5" style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;display:none;">From <span style="text-decoration: underline;">Employee Engagement</span> to <span style="text-decoration: underline;">Lean Six Sigma</span></div>  
		          </h1>
				  <div style="position:relative;top:80px;"><p><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal;font-size: 18px;">Your organization has evolving needs. Mentors Guild has them covered.</span></div>
			</div>
                    <div style="margin-bottom:15px;">
                     <p>
                    <span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Mentors Guild offers its client organizations easy access to relevant expertise.<br/><br/>Unlike traditional consulting firms, we're a national community of experienced Subject Matter Experts (SMEs) and Coaches who work with business leaders - to solve problems, transfer knowledge and drive growth.<br/><br/>For clients, Basic membership is free and self-service. It is sufficient for a short project.<br/><br/>Business membership is an ongoing partnership. A part-time Project Manager learns about your context, translates business needs to projects and drives projects to closure.
                    </span></p></div> 
                   

             
		
		<script>
		var $body = jQuery('body'),
		    cycle;
			(cycle = function() {
			jQuery('#spec1').delay(2000).fadeOut('slow');
			
            jQuery('#spec2').delay(2750).fadeIn('slow');
			jQuery('#spec2').delay(2000).fadeOut('fast');
					
			jQuery('#spec3').delay(5500).fadeIn('slow');
		    jQuery('#spec3').delay(2000).fadeOut('fast');
					
			jQuery('#spec4').delay(8250).fadeIn('slow');
		    jQuery('#spec4').delay(2000).fadeOut('fast');

			jQuery('#spec5').delay(11000).fadeIn('slow');
		    jQuery('#spec5').delay(2000).fadeOut('fast');
				
			jQuery('#spec1').delay(11750).fadeIn('slow',cycle);	
									
			})();
		</script>
	        

                 <table class="planstable" width="100%" cellpadding="0" cellpadding="0">
	        	<tr>
		            <th width="490">&nbsp;</th>
		            <th width="235">&nbsp;</th>

		            <th width="235">&nbsp;</th>
	          	</tr>
 				<tr class="borderbtm">
	            	<th align="left" valign="bottom" class="numbeR">
						<div style="position: absolute;">
							<span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Have questions? Call</span>
							<strong>
								<span style="font-family: 'proximanova semibold',Ubuntu;color: #333333;font-size: 18px;">
									<span>1</span>-<span>866</span>-<span>511</span>-<span>1898</span><span style="color: black; font-weight: normal;">.</span>
								</span>
							</strong>
						</div>
					</th><br/>
<?php if($this->Session->read('Auth.User.id')==''){ ?>
	            	<th>
	            		<br/><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Basic</span><br/><span>Free</span><br/>
							<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
    							<input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;font-weight:normal;" value="Start now" onclick="javascript:submitPlan(1);">
    						</div>
							<div style="float: none;text-align: center;font-style: normal;text-decoration: none;font-size: 80%;vertical-align: bottom;display: block;position: static;margin-bottom: 0;">
								<span>&nbsp;</span>
							</div>
					</th>

	            	<th>
	            		<span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Business</span><br/>
	            		<span>$495 per month</span><br/>
						<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
		                    <input class="profilebtn" type="button" style="width:180px !important; margin-top: 1px;font-weight:normal;" value="Start your free month" onclick="javascript:submitPlan(2);">
						</div>
					</th>
 <?php } elseif($this->Session->read('Auth.User.id')!=''){?>
	            	<th>	
	            		</br><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Basic</span>
	            		<br/><span>Free</span><br/>
						<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
						<?php if($this->Session->read('Auth.User.plan_type') == "Basic" && $this->Session->read('Auth.User.role_id')!= Configure::read('App.Role.Mentor')) {?>
						    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Your Plan">
						<?php } else {?>
							<input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;font-weight:normal;" value="Start now" onclick="window.location.href='<?php echo SITE_URL."clients/cancel_plan" ?>';">
						<?php }?>
						</div>

					</th>

	            		<th></br></br><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal; color: #333333;font-size: 18px;">Business</span><br/>
	            		<span>$495 per month</span><br/>
						<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
						<?php if($this->Session->read('Auth.User.plan_type') == "Business" && $this->Session->read('Auth.User.role_id')!= Configure::read('App.Role.Mentor')) {?>
						    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Your Plan">
						<?php } else {
                                               $id =2; ?>
							<input class="profilebtn" type="button" style="width:180px !important; margin-top: 1px;font-weight:normal;" value="Start your free month" onclick="window.location.href='<?php echo SITE_URL."clients/striperecurr/".$id ?>'; ">
						<?php }?>
						</div>
						<div style="float: none;text-align: center;font-style: normal;text-decoration: none;font-size: 80%;vertical-align: bottom;display: block;position: static;margin-bottom: 0;">
							<span>&nbsp;</span>
						</div>
					</th>
<?php } ?>
</tr> 

<tr>
	            <td style="position:relative;">
              <?php echo Configure::read('client_satisfaction_guarantee');?>
                    	<br/>              
				</td>
				<td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png" alt="check mark"></span></td>
	            
				<td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png" alt="check mark"></span></td>
</tr>
<tr>
	            <td>
	              <?php echo Configure::read('project_management');?>            
                 </td>
	            <td></td>
	            
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png" alt="check mark"></span></td>
</tr>

<tr>
	            <td>
	              <?php echo Configure::read('hire_a_specialist');?>            
                 </td>
                       <td><span style="font-weight:bold;font-size:14px;">Search by expertise<br/>OR<br/><a href="http://www.mentorsguild.com/project/create" target="_blank" >Post a project</a></span></td>
	            
	            <td><span style="font-weight:bold;font-size:14px;">Your Project Manager<br/>will assist</span></td>
</tr>

<tr>
	            <td>
	              <?php echo Configure::read('one_contract');?>      
                   </td>
	            
	            <td></td>
	            <td><span class="checkmark"><img src="<?php e(SITE_URL)?>/img/media/check-mark.png" alt="check mark"></span></td>
</tr>

<tr>
	            <td>
	              <?php echo Configure::read('complimentary_consulting_hours');?>

                     </td>
	           
	            <td></td>
	            <td><span style="font-weight:bold;font-size:14px;">Includes first 3 hours<br/>of project work<br/>AND<br/><a href="http://www.mentorsguild.com/roundtable" target="_blank" >Quarterly Roundtables</a></span></td>
	          </tr>



<tr class="lastbottom">
                 <td></td>
	            <td>
					<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
<?php if($this->Session->read('Auth.User.id')==''){ ?>
                    	<input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;font-weight:normal;" value="Start now" onclick="javascript:submitPlan(1);">
                    </div>
                 </td>

	            <td>
	            	<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
                       <input class="profilebtn" type="button" style="width:180px !important; margin-top: 1px;font-weight:normal;" value="Start your free month" onclick="javascript:submitPlan(2);">
                   </div>
               </td>
 <?php } else{?>
 						<?php if($this->Session->read('Auth.User.plan_type') == "Basic" && $this->Session->read('Auth.User.role_id')!= Configure::read('App.Role.Mentor')) {?>
						    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Your Plan">
						<?php } else {?>
							<input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;font-weight:normal;" value="Start now" onclick="window.location.href='<?php echo SITE_URL."clients/cancel_plan" ?>';">
						<?php }?>
					</div>
				</td>

	            <td>
	            	<div class="apply-button-search" style="float: none; margin: auto; margin-bottom: 5px;">
	            		<?php if($this->Session->read('Auth.User.plan_type') == "Business") {?>
						    <input class="profilebtn" type="button" style="width:100px !important; margin-top: 1px;" value="Your Plan">
						<?php } else {
                                                    $id =2;?>
							<input class="profilebtn" type="button" style="width:180px !important; margin-top: 1px;font-weight:normal;" value="Start your free month" onclick="window.location.href='<?php echo SITE_URL."clients/striperecurr/".$id; ?>'; ">
						<?php }?>
					</div>
				</td>
<?php }?>
</tr>
</table>
	      </div>
	    </div>
<br/>
	  </div>
	</div>
 </br><br/><br/>
	<script src="http://code.jquery.com/jquery-1.7.2.js"></script>
	<script>
	$(document).ready(function(){
	  $(function(){
	  var from=['Business Strategy','Workplace Conflict','Mergers &amp; Acquisition','Employee Engagement','Executive Coaching'], i=1; // i for counting
	      setInterval(function(){
	          $('#from').fadeOut(function(){ //fadeout text
	          $(this).html(from[i=(i+1)%from.length]).fadeIn(); //update, count and fadeIn
	          });
	      }, 3500 ); //2s

	  var to=['Succession Planning','Customer Service','Change Management','Lean Six Sigma','New Product Launch'], j=1; // j for counting
	      setInterval(function(){
	          $('#to').fadeOut(function(){ //fadeout text
	          $(this).html(to[j=(j+1)%to.length]).fadeIn(); //update, count and fadeIn
	          });
	      }, 3500 ); //2s

	  });
	});

	function submitPlan(plan_id) {
		pricingAuthPopSettingPopup(plan_id);
	  
	}

	function changePlan(plan_id) {
		changePlanAfterCheckingUser(plan_id);
	}
</script>
	
<?php
if(isset($plan_id_for_login) && $this->Session->read('Auth.User.id') ==''){?>

<?php
	e($form->hidden('plan_id_for_login',array('value'=>$plan_id_for_login,'id'=>'plan_id_for_login')));	
?>	
	<script type="text/javascript">

		jQuery(document).ready(function(){
			var plan_id = document.getElementById('plan_id_for_login').value;
			pricingloginpopup(plan_id);
		});
	</script>
<?php
}?>
