

<style>
<!--[if lt IE 9]>
#nav-search { border:0px solid #eaeaea !important; }
#nav-search #searchform input{
   padding-top:0px; 
}
<![endif]-->
</style>

<?php //e($javascript->link(array('front'))); ?>
<?php 
$searchClass='style="width:auto;"';
if($this->Session->read('Auth.User.id') !='' && $this->Session->read('Auth.User.id') !=1){
	$searchClass = ""; ?>
	<script type="text/javascript">
	loggedUserId = <?php echo $this->Session->read('Auth.User.id'); ?>
	</script>
<?php } ?>

<?php e($this->element('front/howitworks'));?>

<div id="header-box" class="pagewidth">
	<div id="header-content">  
		<div id="logo-box" style="margin-top: -8px; margin-left: -55px;" itemscope itemtype="http://schema.org/Organization">
			<?php e($html->link($html->image('media/Final-Logo.png',array('alt'=>'Mentors Guild','width'=>'355px','height'=>'51px','itemprop'=>'logo')),'/',array('escape'=>false))); ?>
		</div>	
               				
		<div id="inner-nav-box" <?php echo $searchClass; ?>>
		
        <?php				
				/*if($this->params['action'] !='index'){
					
					$keyword = '';
					if($this->Session->check(session_id()))
					{
						$sessionData = $this->Session->read(session_id());
						$sess_id = session_id();
						if(isset($_SESSION[$sess_id]['keyword'])):
							$keyword = $sessionData['keyword'];
						endif;
					}
					if($keyword==''): $keyword = "Search for an expert..."; endif;
					
					?>
                
					<div id="nav-search" style="margin-top:-4px; border:1px solid #eaeaea;">
						<form action="<?php echo SITE_URL;?>fronts/search_result" id="searchform" method="post" role="search">
							<input type="text" onfocus="if (this.value == 'Search for an expert...'){this.value = '';}" onblur="if (this.value == '')	{this.value = 'Search for an expert...';}"  name="data[City][keyword]" value="<?php echo $keyword;?>">
							<?php
							if(!isset($loggedInUsercityData['City']['city_name'])): $loggedInUsercityData['City']['city_name']='New York'; endif;
							if(!isset($loggedInUsercityData['City']['state'])): $loggedInUsercityData['City']['state']='NY'; endif;
							if(!isset($city_name)): $city_name=''; endif;
							
								e($form->input('City.city_name',array('id'=>'username','div'=>false,'label'=>false, 'type'=>'hidden',
								    'placeholder'=>$loggedInUsercityData['City']['city_name'].','.$loggedInUsercityData['City']['state'],'value'=>$city_name)));
							?>
							<input type="submit" value="" id="searchsubmit">
						</form>
					</div>			
					<?php
				}*/?>		
			<?php
			if($this->Session->read('Auth.User.id') !='' && $this->Session->read('Auth.User.id') !=1){?>
				<div id="nav-box" style="padding-top:12px;">
					<ul style="margin-right: 100px;">
						<?php	if($this->params['controller'] == 'fronts' && $this->params['action'] =='index'){ ?>
						<li>
							<a href="javascript:void(0);" id="howItWorksLink">How it works</a> <span style="padding-left: 15px;">|</span>
						</li>
						<?php	} else {?>
						<li>
							<a href="<?php e(SITE_URL)?>">Search</a> <span style="padding-left: 15px;">|</span> 
						</li>
						<?php }?>
						<li id="showsubnavi" onclick="shownavi('hidesubnavi');">
								<a href="#">Practice areas </a><span class="arrowBLK">&#9660;</span>
								<span style="padding-left: 15px;">|</span> 
							<div id="hidesubnavi" onmouseover="shownavi('hidesubnavi');"  onmouseout="mouseout('hidesubnavi');">
								<table >
		            				<tr>
		            					<td width="200" style="border-right: 1px solid #DDDDDD;"><strong>Functions</strong></td>
		            					<td width="160"><strong>Industries</strong></td>
		            				</tr>
		            			<?php 
		            			$Functions = $this->Session->read('Functions');
		            			$Domains = $this->Session->read('Domains');
		            			$count = 0;
		            			$fCount = count($Functions);
		            			$half = $fCount/2;
		            			$fC = 0;
		            			$Functions[0]['Qna_category']['category'] = "Career";
		            			$Functions[0]['Qna_category']['id'] = 3;
		            			$Functions[1]['Qna_category']['category'] = "Change Management";
		            			$Functions[1]['Qna_category']['id'] = 4;
		            			$Functions[2]['Qna_category']['category'] = "Communication";
		            			$Functions[2]['Qna_category']['id'] = 5;
		            			$Functions[3]['Qna_category']['category'] = "Culture";
		            			$Functions[3]['Qna_category']['id'] = 7;
		            			$Functions[4]['Qna_category']['category'] = "Customers";
		            			$Functions[4]['Qna_category']['id'] = 8;
		            			$Functions[5]['Qna_category']['category'] = "Financial Management";
		            			$Functions[5]['Qna_category']['id'] = 9;
		            			$Functions[6]['Qna_category']['category'] = "Growth & Profitability";
		            			$Functions[6]['Qna_category']['id'] = 10;
		            			$Functions[7]['Qna_category']['category'] = "Innovation";
		            			$Functions[7]['Qna_category']['id'] = 11;
                                $Functions[8]['Qna_category']['category'] = "Interim Management";
		            			$Functions[8]['Qna_category']['id'] = 26;
		            			$Functions[9]['Qna_category']['category'] = "Leadership";
		            			$Functions[9]['Qna_category']['id'] = 12;
		            			$Functions[10]['Qna_category']['category'] = "Mergers & Acquisitions";
		            			$Functions[10]['Qna_category']['id'] = 24;
		            			$Functions[11]['Qna_category']['category'] = "Operations";
		            			$Functions[11]['Qna_category']['id'] = 15;
		            			$Functions[12]['Qna_category']['category'] = "Performance Management";
		            			$Functions[12]['Qna_category']['id'] = 25;
		            			$Functions[13]['Qna_category']['category'] = "Project Management";
		            			$Functions[13]['Qna_category']['id'] = 27;

		            			$Functions[15]['Qna_category']['category'] = "Strategy";
		            			$Functions[15]['Qna_category']['id'] = 17;
		            			$Functions[16]['Qna_category']['category'] = "Technology";
		            			$Functions[16]['Qna_category']['id'] = 18;
		            			
		            			$Domains[0]['Qna_category']['category'] = "Government";
		            			$Domains[0]['Qna_category']['id'] = 20;
		            			$Domains[1]['Qna_category']['category'] = "Healthcare";
		            			$Domains[1]['Qna_category']['id'] = 19;
		            			$Domains[2]['Qna_category']['category'] = "Non-profit";
		            			$Domains[2]['Qna_category']['id'] = 21;
		            			$Domains[3]['Qna_category']['category'] = "Professional Services";
		            			$Domains[3]['Qna_category']['id'] = 22;
		            			$Domains[4]['Qna_category']['category'] = "Retail";
		            			$Domains[4]['Qna_category']['id'] = 23;
		            			$Domains[5]['Qna_category']['category'] = "Small Business/Startup";
		            			$Domains[5]['Qna_category']['id'] = 16;
		            			
		            			
		            			foreach ($Functions as $key=>$fValue) {?>
		            				<tr>
		            					<td width="200" style="border-right: 1px solid #DDDDDD;">
		            				       <?php {
                                                            $str = $fValue['Qna_category']['category'];
                                                            if(false !== stripos($fValue['Qna_category']['category'],"&"))
                                                            $str = str_replace("&","_and",$fValue['Qna_category']['category']);

                                                            $str = str_replace(" ","-",$str);
                                                            e($html->link($fValue['Qna_category']['category'],SITE_URL.'browse/category/'.$str,array('escape'=>false)));}?>
                                                     </td>
		            					<td width="160" style="padding-left: 15px;">
		            						<?php 
		            							if(isset($Domains[$count])){
                                                                       $link = $Domains[$count]['Qna_category']['category'];
                                                                       if(false !== stripos($Domains[$count]['Qna_category']['category'],"/")){
                                                                        $link  = str_replace("/","_slash",$Domains[$count]['Qna_category']['category']);
                                                                        }
                                                                             $link = str_replace(" ","-",$link);
				            						e($html->link($Domains[$count]['Qna_category']['category'],SITE_URL.'browse/category/'.$link ,array('escape'=>false)));
				            						$count++;
		            							}
		            						?>
		            					</td>
		            				</tr>
		            			<?php }?>
		            			</table>
							</div>
						</li>
                                            <?php if($this->Session->read('Auth.User.role_id')==Configure::read('App.Role.Mentor')){ ?>
						<li>
							<a href="<?php e(SITE_URL)?>members/member_services" style="font-weight: normal;">Member services</a> <span style="padding-left: 15px;">|</span> 
						</li>
						<?php	} else {?>	
                                        	  <li>
							<a href="<?php e(SITE_URL)?>pricing" style="font-weight: normal;">Client pricing</a> <span style="padding-left: 15px;">|</span> 
						</li>
					      <?php }?>
                                            <?php if($this->Session->read('Auth.User.role_id')==Configure::read('App.Role.Mentor')){ ?> 
                                            <li>
							<a href="<?php e(SITE_URL)?>qanda" style="font-weight: normal;">Share your insight</a> <span style="padding-left: 15px;">|</span> 
						</li>
						<?php	} else {?>	
                                        	  <li>
							<a href="<?php e(SITE_URL)?>qanda" style="font-weight: normal;">Ask an expert</a> <span style="padding-left: 15px;">|</span> 
						</li>
					    <?php }?>
					</ul>
				</div>	
				<div id="inner-nav-list" >
					<?php			
					$userPhoto = $this->General->FindUserImage($this->Session->read('Auth.User.id'));
                    if($userPhoto !=''){
						if($this->Session->read('Auth.User.role_id')==Configure::read('App.Role.Mentor')){
							$image_path = '/img/members/profile_images/'.$this->Session->read('Auth.User.id').'/small/'.$userPhoto;
						}else{
							$image_path = '/img/clients/profile_images/'.$this->Session->read('Auth.User.id').'/small/'.$userPhoto;
						}						
					}else{
						$image_path = 'media/small_Profile.png';
					}?>	
					
					<?php 
                    if($this->Session->read('Auth.User.role_id')=='4')
                    { ?>            
                    <a href="<?php echo SITE_URL?>users/my_account" class="user">
                    <?php e($html->image('media/small_Profile.png',array('alt'=>'black','class'=>'desaturate'))); ?>
                    </a>
                    <?php } ?>
                    
					
					<?php 
                    if($this->Session->read('Auth.User.role_id')=='2')
                    { ?>            
                    <a href="<?php echo SITE_URL?>users/my_account" class="user">
                    <?php e($html->image($image_path,array('alt'=>'black','class'=>'desaturate'))); ?>
                    </a>
                    <?php } ?>
                    
                    
					<?php 
					if($this->Session->read('Auth.User.role_id')=='3')
                    {
                        if($this->Session->read('Auth.User.access_specifier') =='draft'){ 
                               if($this->Session->read('Auth.User.email_send') =='0'): ?>
                                           <a href="<?php echo SITE_URL?>users/registration_step2" class="user">
                                   <?php elseif($this->Session->read('Auth.User.email_send')=='1' && $this->Session->read('Auth.User.is_approved') =='0'): ?>
                                           <a href="<?php echo SITE_URL?>users/thanksuser" class="user">
                                <?php     endif;
					     } else { ?>
					        <a href="<?php echo SITE_URL?>clients/my_account" class="user">
					   <?php } ?>
					<?php e($html->image($image_path,array('alt'=>'black','class'=>'desaturate'))); ?>
					</a>
					<?php } ?>
					<ul>
						<?php
							if($this->Session->read('Auth.User.role_id')=='2')
							{
									if($this->Session->read('Auth.User.access_specifier') =='draft')
									{ ?>								
										<li class="user-profile-list"> <?php	e($html->link('My profile',array('controller'=>'members','action'=>'member_full_profile'),array('alt'=>'full')));  ?> </li>	
							<?php	}
									else{	
									?>
									<li class="user-profile-list"> <?php e($html->link('Edit profile',array('controller'=>'users','action'=>'edit_account'),array('alt'=>'edit_account')));  ?> </li>
									<li class="user-profile-list"> <?php e($html->link('View profile',array('controller'=>'users','action'=>'my_account'),array('alt'=>'my_account')));?> </li> 					
									<?php }								
									if($this->Session->read('Auth.User.access_specifier') !='draft')
									{	?>
										<li class="user-profile-list"> <?php	e($html->link('My account',array('controller'=>'members','action'=>'account_setting'),array('alt'=>'Account setting')));?> </li> 	
								<?php }
							}
							
							if($this->Session->read('Auth.User.role_id')=='3')
							{
								if($this->Session->read('Auth.User.access_specifier') =='draft')
								{ ?>
											<?php /*	<li class="user-profile-list"> e($html->link('My profile',array('controller'=>'users','action'=>'registration_step2'),array('alt'=>'full')));  ?> </li> */ ?>	
									<?php 	} 
								else{	?>
										<li class="user-profile-list"> <?php	e($html->link('Edit profile',array('controller'=>'clients','action'=>'edit_account'),array('alt'=>'edit_account')));  ?> </li>
										<li class="user-profile-list"> <?php	e($html->link('View profile',array('controller'=>'clients','action'=>'my_account'),array('alt'=>'my_account')));?> </li> 			
										<li class="user-profile-list"> <?php	e($html->link('My account',array('controller'=>'clients','action'=>'account_setting'),array('alt'=>'Account setting')));?> </li>
										<?php if($this->Session->read('Auth.User.isPlanAdmin')=='1'){?>
											<li class="user-profile-list"> <?php	e($html->link('Plan Dashboard',array('controller'=>'pricing','action'=>'dashboard'),array('alt'=>'Plan Dashboard')));?> </li>
										<?php }?> 			
								<?php }
							} ?>
							
						<?php 
                   		if($this->Session->read('Auth.User.role_id')=='2')
                   		{ ?>  
                                        
                                          <li class="user-profile-list">
						<a href="<?php e(SITE_URL)?>insights" title="Post an event">Weekly Insights</a>
                			       </li>

                                          <?php } ?>
						<?php 
                   		if($this->Session->read('Auth.User.role_id')=='3')
                   		{ ?>  
					 <li class="user-profile-list">
						<?php e($html->link('Invite a member','javascript:client_invitation_popup();',array('alt'=>'Client Invitation','style' =>'font-weight: normal;'))); ?>
						</li>
                                          <li class="user-profile-list">
						<a href="<?php e(SITE_URL)?>insights" title="Post an event">Weekly Insights</a>
                                          </li> 
                                           
                                          
                                          <li class="user-profile-list">
						<a href="<?php e(SITE_URL)?>project/create" title="Post a project" style="font-weight:bold;">Post a project</a>
                                          </li>
                                          <li class="user-profile-list">
						<a href="<?php e(SITE_URL)?>roundtable" title="Executive roundtable">Executive roundtable</a>
                                          </li>
						<?php } ?>
						<li class="user-profile-list">
						<?php e($html->link('Sign out',array('controller'=>'users','action'=>'logout'),array('alt'=>'Sign Out'))); ?>
						</li>
					</ul>
				</div>
				<?php
			}elseif($this->Session->read('Auth.User.role_id') ==1){?>
				<div id="nav-box" style="padding:15px;">
					<ul>			
						<li>
							<?php e($html->link('Admin Sign Out',array('controller'=>'users','action'=>'logout'),array('alt'=>'Sign Out'))); ?>
						</li>
					</ul>
				</div>						
				<?php			
			}else{?>
				<div id="nav-box" style="padding-top:12px;">
					<ul style="padding-right:64px;">
						<?php	if($this->params['controller'] == 'fronts' && $this->params['action'] =='index'){ ?>
						<li>
							<a href="javascript:void(0);" id="howItWorksLink">How it works</a> <span style="padding-left: 15px;">|</span>
						</li>
						<?php	} else {?>
						<li>
							<a href="<?php e(SITE_URL)?>">Search</a> <span style="padding-left: 15px;">|</span> 
						</li>
						<?php }?>
						<li id="showsubnavi" onclick="shownavi('hidesubnavi');">
								<a href="#">Practice areas </a><span class="arrowBLK">&#9660;</span> 
								<span style="padding-left: 15px;">|</span> 
							<div id="hidesubnavi" onmouseover="shownavi('hidesubnavi');"  onmouseout="mouseout('hidesubnavi');">
								<table >
		            				<tr>
		            					<td width="200" style="border-right: 1px solid #DDDDDD;"><strong>Functions</strong></td>
		            					<td width="160"><strong>Industries</strong></td>
		            				</tr>
		            			<?php 
		            			$Functions = $this->Session->read('Functions');
		            			$Domains = $this->Session->read('Domains');
		            			$count = 0;
		            			$fCount = count($Functions);
		            			$half = $fCount/2;
		            			$fC = 0;
		            			foreach ($Functions as $key=>$fValue) {?>
		            				<tr>
		            					<td width="200" style="border-right: 1px solid #DDDDDD;">
		            				       <?php { 
                                                             $str = $fValue['Qna_category']['category'];
                                                            if(false !== stripos($fValue['Qna_category']['category'],"&"))
                                                            $str = str_replace("&","_and",$fValue['Qna_category']['category']);

                                                            $str = str_replace(" ","-",$str);
                                                            e($html->link($fValue['Qna_category']['category'],SITE_URL.'browse/category/'.$str,array('escape'=>false)));}?>
                                                       </td>
		            					<td width="160" style="padding-left: 15px;">
		            						<?php 
		            							if(isset($Domains[$count])){
                                                                       $link = $Domains[$count]['Qna_category']['category'];
                                                                       if(false !== stripos($Domains[$count]['Qna_category']['category'],"/")){
                                                                        $link  = str_replace("/","_slash",$Domains[$count]['Qna_category']['category']);
                                                                        }
                                                                        $link = str_replace(" ","-",$link);
				            						e($html->link($Domains[$count]['Qna_category']['category'],SITE_URL.'browse/category/'.$link ,array('escape'=>false)));
				            						$count++;
		            							}
		            						?>
		            					</td>
		            				</tr>
		            			<?php }?>
		            			</table>
							</div>
						</li>
                                            <li>
							<a href="<?php e(SITE_URL)?>pricing" style="font-weight: normal;">Client pricing</a> <span style="padding-left: 15px;">|</span> 
						</li>
                                           <li>
							<a href="<?php e(SITE_URL)?>qanda" style="font-weight: normal;">Ask an expert</a> <span style="padding-left: 15px;">|</span> 
						</li>
                                           
                                     </ul>
                                
                                  
				</div>	
                             	
				<?php						
			}?>
                        <?php  if($this->Session->read('Auth.User.role_id') ==''){?>
                              <div id="dropdown-nav-list">
                              <a href="#" style="text-decoration: none;margin-top:25px;"><span style ="font-weight: normal;font-size: 14px;">More</span> <span class="arrowBLK">&#9660;</span></a>
                             <ul style="padding:10px;"> 
                                          <li class="more-link-list">
						<a href="<?php e(SITE_URL)?>insights" title="Post an event">Weekly Insights</a>
                                          </li>  
                                         
                                          
                                          <li class="more-link-list">
						<a href="<?php e(SITE_URL)?>project/create" title="Post a project" style="font-weight:bold;">Post a project</a>
                                          </li>
                                          <li class="more-link-list">
						<a href="<?php e(SITE_URL)?>roundtable" title="Executive roundtable">Executive roundtable</a>
                                          </li>
                                           
                                           <li class="more-link-list">
                                          <a href="<?php e(SITE_URL)?>#register" style="display: inline;">Register</a>

                                          </li>
                                          <li class="more-link-list" >
                                          <?php e($html->link('Login','javascript:loginpopup();',array('class'=>'login','style'=>'display: inline;'))); ?>
						</li>
						
						
                                 </ul>
                             </div>
                          <?php }?>
		</div>		
	</div>
</div>


<script type="text/javascript">

var shown = 0;
function shownavi(show)
{
	var s=document.getElementById(show);
	s.style.display="block";
}
function mouseout(mout)
{
	var ms=document.getElementById(mout);
	ms.style.display="none";
}

jQuery("#howItWorksLink").click(function(){
	
	if(shown == 0) {

     jQuery("#howItWorks").slideDown("slow");

		shown = 1;
	}
	else {
		jQuery("#howItWorks").slideUp("slow");
		shown = 0;
	}
});

function hideme(){
	jQuery("#howItWorks").slideUp("slow");
	shown = 0;
}

jQuery(window).scroll(function() {
    var height = jQuery(window).scrollTop();
if(shown == 1 && height  >= 617) {

     jQuery("#howItWorks").hide();
     jQuery(window).scrollTop(0, "slow");
       shown = 0;
      
    }

});	
</script>