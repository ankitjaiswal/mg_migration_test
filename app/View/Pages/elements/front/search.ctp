<?php
$pageLimit = 3;
$data_flag = false;
//prd($data);
if(!empty($data)){
	$data_flag = true;
	?>
	<table id="results" style="width:100% !important;">
	<?php	
	foreach($data as $key => $value){

			if($key%$pageLimit == 0){
				$num = floor($key/$pageLimit);
			}
			?>
			<tr class="set_<?php e($num); ?>">
				<td>
					<div class="box1">
					  <div class="content-left-img" style="width: 180px;"> 
					  <?php 
					  	$urlk1 = ($value['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1); ?>
					 <?php 
					 if(!empty($value['UserImage'])){
						if(file_exists(WWW_ROOT . 'img' . '/' .MENTORS_IMAGE_PATH.'/'.$value['User']['id'].'/'.$value['UserImage'][0]['image_name'])){ 
							e($html->link($html->image(MENTORS_IMAGE_PATH.'/'.$value['User']['id'].'/'.$value['UserImage'][0]['image_name'], array('class'=>'desaturate','alt'=>$value['UserReference']['first_name']." ".$value['UserReference']['last_name'],'style'=>'width:150px; height:150px;')),$displink1,array('escape'=>false)));                       					
						}else{				
							e($html->link($html->image('media/profile.png',array('alt'=>$value['UserReference']['first_name']." ".$value['UserReference']['last_name'],'style'=>'width:150px; height:150px;')),$displink1,array('escape'=>false)));  
						}
						
					}else{
						e($html->link($html->image('media/profile.png',array('alt'=>$value['UserReference']['first_name']." ".$value['UserReference']['last_name'],'style'=>'width:150px; height:150px;')),$displink1,array('escape'=>false)));  

					}
					?>
					</div>
                     <?php
						    $urlk = ($value['User']['url_key']);
							$displink=SITE_URL.strtolower($urlk);
				          ?>
					  <div class="content-right-text">
						<h1><span><?php e($html->link(ucwords($value['UserReference']['first_name']).' '.$value['UserReference']['last_name'],$displink,array('escape'=>false))); ?></span> - <?php e($value['UserReference']['City']['city_name'].', '.$value['UserReference']['City']['state']);?>
						<?php if($value['linkedinURL'] != '') {?>
						<?php }?>

                                          <span style="float:right;font-size:14px;padding-top:4px;font-weight:normal;">Call 1-866-511-1898 <span style="font-size:14px;font-weight:bold;">x<?php echo ($value['User']['extension']);?></span></span>
						</h1>
						<div class="text-content-box">
						  <p> 
						  <?php
							$strdata=strlen ($value['UserReference']['background_summary']);
							if($strdata < 700){
							echo $this->Text->truncate( $value['UserReference']['background_summary'], 400, array('ending' => '...', 'exact' => false));
							}else{
								echo $this->Text->truncate( $value['UserReference']['background_summary'], 400, array('ending' => '...', 'exact' => false));
							}
						  ?>
						  
						 
				          <?php e($html->link('read more',$displink,array('escape'=>false,'onclick' => 'setCookieReadMore();'))); ?>

						  </p>
							
						</div>
						<div class="expertise-header">
						  <h1>Areas of expertise</h1>
						  <p><?php e($value['UserReference']['area_of_expertise']);?></p>

						</div>
						<div class="apply-button-search">
								<input type="button" style="font-weight: normal;" value="View Profile" onclick="window.location.href='<?php echo SITE_URL.strtolower($urlk1);?>';">
						</div>	
						<?php 
							$data='';
							/*if($this->Session->read('Auth.User.role_id')=='3' ||$this->Session->read('Auth.User.role_id')==''):*/
							if(false):
								$data = $this->General->checkMentorApply($value['User']['id']);
							
							if($value['UserReference']['accept_application'] == 'Y')
							{
								if(empty($data)):
						?>						
								<div class="apply-button-search">
									<input type="button" value="Apply" onclick="javascript:apply('<?php echo SITE_URL."fronts/consultation_request/"; ?>','<?php e($value['User']['url_key']); ?>');">
								</div>	
						<?php	else: ?>
								<div class="apply-button-search">
									<span>Already applied</span>
								</div>		
						<?php	endif;
							}else{
									$tempData = $this->General->checkMentorRequest($value['User']['id']);
									if(empty($tempData)):
						?>	
									<div class="apply-button-search" id="reqbutton">
										<input type="button" value="Request" onclick="javascript:apply('<?php echo SITE_URL."fronts/request_for_mentership/"; ?>','<?php e($value['User']['id']); ?>','request');" title="Be notified when Mentor becomes available."/>
									</div>	
						<?php		else: ?>
									<div class="apply-button-search">
										<span>Already requested</span>
									</div>
						<?php 		endif;
								}
						endif;
						?>
					  </div>
					</div>		
				</td>
			</tr>			
		<?php
		
	}?>
	</table>
	<?php e($this->element('browse/directory'));?>
    <div style="width:81%; float:right; padding-bottom:30px; text-align:center">
	<?php
		if($num >0){
			$style ="background: none repeat scroll 0 0 #F32C33; color: #FFFFFF; cursor: pointer; font-weight: bold; height: 14px; text-align: center; margin:auto; padding: 10px; width:50%;";
			?>
				<div id="openRegisterNav" class="more-button-search">
		        <input type="button" value="MORE MEMBERS"></div>
			<?php
		}
	?>
	</div>
	<?php

}else{?>
	 <div class="box1">
	 <div class="content-right-text">
		<div class="expertise" style="margin-top:20px;width:950px;">
		<h1><span style="color: #292929;text-decoration:none">Sorry, no consultant profiles were found. Try one of these options.</span></h1>
               </div>
            <div style="color: #292929; margin-top:50px;font-size:14px;height:150px;width:950px;">

              <div style="float: left;margin-left: 0;width:300px;">
               <h2 style="color: #292929;font-size:16px;">Search Suggestions</h2>
                <ul class="premium-list"style="margin-top:20px;">
                  <li>Check your spelling</li>
                  <li>Try more general words</li>
                  <li>Try different words that mean the same thing</li>
                  <li><a href="<?php echo SITE_URL?>qanda" class="internal">Ask a question</a> and get answers from Experts</li>
                 </ul>
               </div>
              <div style="float: right;width:500px;margin-left:120px;">
               <h2 style="color: #292929;font-size:16px;">Top Searches</h2>

                <div style="float: left;width:140px;margin-left:0px;">
                 <ul style="list-style-type: none;margin-top:20px;">
                  <li>1. <a href="<?php echo SITE_URL?>john.baldoni">John Baldoni</a></li>
                  <li>2. <a href="<?php echo SITE_URL?>mark.palmer">Mark Palmer</a></li>
                  <li>3. <a href="<?php echo SITE_URL?>bill.fotsch">Bill Fotsch</a></li>
                  <li>4. <a href="<?php echo SITE_URL?>judy.bardwick">Judy Bardwick</a></li>
                  <li>5. <a href="<?php echo SITE_URL?>ivan.rosenberg">Ivan Rosenberg</a></li>
                 </ul>
                </div>
                <div style="float: right;width:350px;">
                <div style="float: left; width:170px;">
                    <ul style="list-style-type: none;margin-top:20px;">
                     <li>6. <a href="<?php echo SITE_URL?>browse/category/Leadership">Leadership</a></li>
                     <li>7. <a href="<?php echo SITE_URL?>browse/category/Customers">Customers</a></li>
                     <li>8. <a href="<?php echo SITE_URL?>browse/category/Interim Management">Interim Management</a></li>
                     <li>9. <a href="<?php echo SITE_URL?>browse/category/Strategy">Strategy</a></li>
                     <li>10. <a href="<?php echo SITE_URL?>browse/category/Healthcare">Healthcare</a></li>
                  </ul>
                </div>

                <div style="float: right;width:170px; margin-right:0px;">
                    <ul style="list-style-type: none;margin-top:20px;">
                     <li>11. <a href="<?php echo SITE_URL?>browse/search_result/executive-coaching/NY/new-york">Executive Coaching</a></li>
                     <li>12. <a href="<?php echo SITE_URL?>browse/category/Retail">Retail</a></li>
                     <li>13. <a href="<?php echo SITE_URL?>browse/category/Innovation">Innovation</a></li>
                     <li>14. <a href="<?php echo SITE_URL?>browse/search_result/browse-all/CA/los-angeles">Los Angeles</a></li>
                     <li>15. <a href="<?php echo SITE_URL?>browse/search_result/browse-all/NY/new-york">New York</a></li>
                 </ul>
                </div>
               </div>
              </div>

             </div>   
                  		
	 </div>
	 </div>
	 <?php e($this->element('browse/directory'));?>
	<?php 
} ?>
<?php
if($data_flag){?>	
	<?php e($form->hidden('num',array('value'=>$num,'id'=>'num'))); ?>
	<?php e($form->hidden('countShow',array('id'=>'countShow'))); ?>
	<?php
}else{?>
	<?php e($form->hidden('num',array('value'=>'NOT','id'=>'num'))); ?>
	<?php
}?>
<?php e($javascript->link(array('jquery/jquery','ui/jquery-ui-1.8.2.custom.min','Placeholders')));?>
<?php e($javascript->link(array('front'))); ?>
<script type="text/javascript">
var openRegister = 0;
	function apply(url,mentor_id,ClickType){
		var role_id = "<?php echo $this->Session->read('Auth.User.role_id'); ?>";
		var email_send = "<?php echo $this->Session->read('Auth.User.email_send'); ?>";
		var is_approved = "<?php echo $this->Session->read('Auth.User.is_approved'); ?>";
		if(role_id==''){
			//applyloginpopup(mentor_id);
			applyregisterpopup();
			
		}else if(role_id==2){
			alert("Sorry, mentors are currently not allowed to apply for mentorship from other mentors.");
		}else{
		   if(is_approved=='0')
			{
				location.href = SITE_URL+"users/logout";
			}
			else
			{
				if(ClickType == "request"){
					requestMentorship(mentor_id);
				}
				else{
					location.href = url+mentor_id;
				}
			}
		}
	
	}
	// more mentor show functionality
	//history.navigationMode = 'compatible';
	jQuery(document).ready(function(){
		
		var num_tot = parseInt(jQuery("#num").val(),10);
		
		for(var iii = 0; iii <= num_tot; iii++){
			if(iii != 0)
				jQuery(".set_"+iii).hide();
		}

		var num = jQuery("#num").val();	
		if(num == 0) {
			jQuery("#user-account").show();
		}

		if(num == 'NOT') {
			jQuery("#user-account").show();
		}
		
		if(num !='NOT'){			
			jQuery(".set_0").show();
			jQuery("#countShow").val(0);	
		}
		var old_countShow = readCookie('cookie_countShow')
		if (old_countShow) {
			for(showCnt=0;showCnt<old_countShow;showCnt++){
				jQuery("#countShow").val(showCnt);
				paging_hack();		
			}
			
		}	
		eraseCookie('cookie_countShow');
	});

		jQuery("#openRegisterNav").click(function(){
		
		if(loggedUserId == ''){
			if(openRegister == 0) {
				loginpopup();
				return;
			}
		} 
		
		var num = parseInt(jQuery("#num").val(),10);	
		var countShow = parseInt(jQuery("#countShow").val(),10) + 1;
		
		if(num > countShow){			
			jQuery(".set_"+countShow).show();
			jQuery("#countShow").val(countShow);	
		}else if(num == countShow){
			jQuery(".set_"+countShow).show();
			jQuery("#countShow").val(countShow);
			jQuery("#openRegisterNav").hide();
			jQuery("#user-account").show();
		}
	});
	
	function paging_hack(){
		var num = parseInt(jQuery("#num").val(),10);	
		var countShow = parseInt(jQuery("#countShow").val(),10) + 1;
		createCookie('cookie_countShow',countShow,0);
		
		if(num > countShow){			
			jQuery(".set_"+countShow).show();
			jQuery("#countShow").val(countShow);	
		}else if(num == countShow){
			jQuery(".set_"+countShow).show();
			jQuery("#countShow").val(countShow);
			jQuery("#openRegisterNav").hide();
		}
	}
	
	function setCookieReadMore(){
		var countShow = parseInt(jQuery("#countShow").val(),10) + 1;
		createCookie('cookie_countShow',countShow,0);
	}

	function showSideDiv(id){
		jQuery("#dUser_"+id).show();
	}

	function hideSideDiv(id){
		jQuery("#dUser_"+id).hide();
	}
	
</script>


<script type="text/javascript">
function showonlyone(thechosenone) {
 jQuery('.openCONTENT').each(function(index) {
      if (jQuery(this).attr("id") == thechosenone) {
    	  jQuery(this).toggle(300); }
      else {
    	  jQuery(this).hide(600); }
 });
}

jQuery(document).mouseup(function (e) {

	var container = jQuery(".openCONTENT");

	if (!container.is(e.target) && container.has(e.target).length === 0){
		  container.hide();
	}
});	
</script>
