<?php e($javascript->link(array('front'))); ?>                                                              
<?php
if(isset($back_creation)){?>
	<script type="text/javascript">
		jQuery("document").ready(function(){
			loginpopup();		
		});
		
	</script>
	<?php
}?>

<?php
if(isset($openloginpopup)){?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			loginpopup();
		});
	</script>
<?php
}?>

<?php
if(isset($OpenPlanPopup)){?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			var path=SITE_URL+'/pricing/password_setting_popup';
			OPENTINY.box.show({url:path,width:500,height:512,close:true})
		});
	</script>
<?php
}?>
<?php
if(isset($OpenProjectPopup)){?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			var path=SITE_URL+'/project/password_setting_popup';
			TINY.box.show({url:path,width:500,height:510,close:true})
		});
	</script>
<?php
}?>
<?php
if(isset($OpenSubscribePopup)){?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			var path=SITE_URL+'/members/subscribe_to_premium';
			TINY.box.show({url:path,width:500,height:280})
		});
	</script>
<?php
}?>

<script type="text/javascript">

jQuery(document).ready(function(){
    window.onload=initOpen();
});

	function initOpen() {
		setTimeout(	function OpenPopUp(){
			jQuery.ajax({
				url:SITE_URL+'users/isOpenPopUp',
				type:'post',
				dataType:'json',
				success:function(res){
					if(res.value == 1) {
						register_popup();
					}
				}
			});	
		}, 80000);
	}

</script>

<?php
if(isset($question_url) && $this->Session->read('Auth.User.id') ==''){?>

<?php
	e($form->hidden('question_url',array('value'=>$question_url,'id'=>'question_url')));	
?>	
	<script type="text/javascript">

		jQuery(document).ready(function(){
			var q_url = document.getElementById('question_url').value;
			loginpopup_question(q_url);
		});
	</script>
<?php
}?>

<?php
if(!$this->Session->read('Mentor.PasswordSet') && $this->Session->read('Auth.User.id') =='' && isset($OpenRegisterPopup)){?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			passwordSettingPopup();
		});
	</script>
<?php
}?>
<script type="text/javascript">
function twitterCallback2(twitters) {
  var statusHTML = [];
  for (var i=0; i<twitters.length; i++){
    var username = twitters[i].user.screen_name;
	
	if (twitters[i]['retweeted_status'] != null) {
    	twitters[i].text = "RT @"+twitters[i]['retweeted_status']['user']['screen_name']+" : " + twitters[i]['retweeted_status']['text'];
    }
    var status = twitters[i].text.replace(/((https?|s?ftp|ssh)\:\/\/[^"\s\<\>]*[^.,;'">\:\s\<\>\)\]\!])/g,
    function(url){
		return '<a href="'+url+'" target="_blank"><span>'+url+'</span></a>';
    }).replace(/\B@([_a-z0-9]+)/ig, function(reply) {
	    return  reply.charAt(0)+'<a href="http://twitter.com/'+reply.substring(1)+'" style="color:#EEEEEE;" target="_blank">'+reply.substring(1)+'</a>';
    });
    statusHTML.push('<p>'+status+'<br>');
  }
  statusHTML.push('</p>');
  document.getElementById('twitter_update_list').innerHTML = statusHTML.join('');
}
</script>

<div id="footer-wrapper">
	<div id="footer-box"  class="pagewidth">
		<div id="footer-top">
			<div id="footer-logo">
				<?php e($html->link($html->image('media/footer-logo.png',array('alt'=>'mentor-gulid','width'=>'150px','height'=>'22px')),array('controller'=>'fronts','action'=>'index'),array('escape'=>false))); ?>
			</div>
			<div id="footer-about">
				<div id="left-box" style="width:40% !important;">
					<h2 style="color:#F32C33; font-weight:bold; font-size:15px; font-family:ubuntu;">About</h2>
					<p><?php echo Configure::read('AboutFooterText');?></p>
					<p><span style="color:#EEEEEE !important;"><?php echo Configure::read('AboutFooterPhone');?></span><br/>
					<a href='mailto&#58;h&#101;lp&#64;&#109;&#101;&#110;%74o%7&#50;%73g%75ild&#37;2Ecom'><span>hel&#112;&#64;mentor&#115;gui&#108;&#100;&#46;c&#111;m</span></a></p><!--VL 21-12 -->
				</div>
				<div id="right-box" >
					<h2 style="color:#F32C33; font-weight:bold; font-size:15px; font-family:ubuntu;"><?php e($html->link('Tweets','http://www.twitter.com/mentorsguild',array('target'=>'_blank'))); ?></h2>
					 <div id="twitter_update_list" >
					 </div>					
				</div>
			</div>
			<div id="footer-change">
				<h2 style="color:#F32C33; font-weight:bold; font-size:15px; font-family:ubuntu;">Social</h2>
		        <?php $linkdinUrlFooter="http://www.linkedin.com/shareArticle?mini=true&url=http://mentorsguild.com&title=Mentors Guild&summary=http://mentorsguild.com Mentors Guild is a community of experts, eager to share their expertise with others&source=http://mentorsguild.com"; ?>
				<p class="social-links">
                                   <span>
                                   
					<a  href="https://www.linkedin.com/company/2966858" title="Follow Mentors Guild" target="_blank" class="iconlinkedFooter" style="margin-bottom: 15px;padding-top:3px;"></a>
                                    <a  href="http://www.twitter.com/mentorsguild" title="Follow @MentorsGuild" target="_blank" class="icontwittFooter" rel="nofollow" style="margin-bottom: 15px;"></a>
	                             <a  href="https://plus.google.com/u/0/+MentorsguildLive" title="Follow Mentors Guild" rel="publisher" target="_blank" class="icongoogleFooter" style="margin-bottom: 15px;"></a>
	                              </span>


				</p>	
			</div>
		</div>
		<div id="footer-bottom">
			<div id="privacy">
				<p><?php e($html->link('Privacy',array('controller'=>'fronts','action'=>'privacy_policy'),array('style'=>"color:#777"))); ?><span>|</span><?php e($html->link('Terms of service',array('controller'=>'fronts','action'=>'terms_of_service'),array('style'=>"color:#777"))); ?></p>
			</div>
			<div id="all-rights">
				<p>&copy; <?php echo date('Y'); ?> Mentors Guild Inc. All Rights Reserved</p>
			</div>
		</div>
	</div>
	<?php if(isset($_SESSION['User_type'])){
			if($this->Session->read('Auth.User.id') =='') {
				$this->Session->delete('User_type');
			} else {
		?>
			
		<a href="<?php echo SITE_URL?>members/switch_user/1">Switch to admin</a>
	<?php }
	}?>
</div>
<?php
if(isset($_SESSION['reqReschdule']) && $_SESSION['reqReschdule']=='user')
{
    unset($_SESSION['reqReschdule']); ?>
    <script type="text/javascript">loginpopup();</script>
<?php }
if(isset($_SESSION['menteeActEmail']) && $_SESSION['menteeActEmail']!=''):
?>
<script type="text/javascript">loginpopup();</script>			
<?php endif; 
if(isset($backreg)){?>
	<script type="text/javascript">	
		function reloadHome(){
				window.location.href = SiteUrl;
		}
		TINY.box.show({url:SiteUrl+'/users/register',width:500,fixed:false,closejs:function(){reloadHome()}});			
	</script>  	
	<?php
}
if(isset($account)){?>
	<script type="text/javascript">	
		function reloadHome(){
				window.location.href = SiteUrl;
		}
		TINY.box.show({url:SiteUrl+'/users/login',width:500,fixed:false,closejs:function(){reloadHome()}});			
	</script>  	
<?php
}

require_once('../webroot/TwitterAPIExchange.php');

/** Set access tokens here - see: https://dev.twitter.com/apps/ **/
$settings = array(
		'oauth_access_token' => "307588817-leh1xtUeQhdhHYT6D00hHXAG8z9DYy9hxKRKjfIC",
		'oauth_access_token_secret' => "GyrsONya3VQxpMMhF8QZPzRmSLTlRmbrphkwGetI",
		'consumer_key' => "R4lSLx1Ez2FLfrypycP9cw",
		'consumer_secret' => "xUKyUhH1lkIcIRbuWu8wLK4l5CtGzmZa4CqajMnQFpw"
);

$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
$getfield = '?screen_name=mentorsguild&include_rts=true&count=2';
$requestMethod = 'GET';
$twitter = new TwitterAPIExchange($settings);
$twitters = $twitter->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest();

echo '<script type="text/javascript"> twitterCallback2('.$twitters.'); </script>';
  		
?>
<script type="text/javascript">
jQuery(document).ready(function(){

    flashClass = '';
    flashClass = jQuery('#flashMessage').attr('class');
    if(flashClass!='notclass')
    {
       jQuery('#flashMessage').fadeOut(7000);
    }
	Placeholders.init({
		live: true, //Apply to future and modified elements too
		//hideOnFocus: true //Hide the placeholder when the element receives focus
	});

    jQuery(function() {
    	jQuery("a").live('click',function(e){
	        var url = jQuery(this).attr("href");
	        var target = jQuery(this).attr("target");
	        // Console logs shows the domain name of the link being clicked and the current window
	        // console.log('e.currentTarget.host: ' + e.currentTarget.host);
	        // console.log('window.location.host: ' + window.location.host);
	        // If the domains names are different, it assumes it is an external link
	        // Be careful with this if you use subdomains
	        
	        if (e.currentTarget.host == '')
		    	return true;
		    
	        if (e.currentTarget.host != window.location.host) {
	             //console.log('external link click');
	            // Outbound link! Fires the Google tracker code.
	            _gat._getTrackerByName()._trackEvent("Outbound Links", e.currentTarget.host.replace(':80',''), url, 0);
	            // Checks to see if the ctrl or command key is held down
	            // which could indicate the link is being opened in a new tab
	            if (e.metaKey || e.ctrlKey) {
	                //console.log('ctrl or meta key pressed');
	                var newtab = true;
	            }
	            // If it is not a new tab, we need to delay the loading
	            // of the new link for a just a second in order to give the
	            // Google track event time to fully fire
	            if (!newtab) {
	                //console.log('default prevented');
	                e.preventDefault();
	                //console.log('loading link after brief timeout');
	                if(target == '_blank')
	                	setTimeout(function(){window.open(url, '_blank')}, 100);
	                else
	                	setTimeout('document.location = "' + url + '"', 100);
	            }
	        }
	        else {
	            //console.log('internal link click');
	        }
	    });
	});
});
</script>
<script type="text/javascript">

function relative_time(time_value) {
  var values = time_value.split(" ");
  time_value = values[1] + " " + values[2] + " " + values[5] + " " + values[3];
  var parsed_date = new Date();
  parsed_date.setTime(Date.parse(time_value)); 
  var months = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
     'Sep', 'Oct', 'Nov', 'Dec');
  var m = parsed_date.getMonth();
  var postedAt = '';
  postedAt = months[m];
  postedAt += " "+ parsed_date.getDate();
  postedAt += ","
  postedAt += " "+ parsed_date.getFullYear();
  return postedAt;
}                                                 
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  var pluginUrl = 
	  '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
	 _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
  _gaq.push(['_setAccount', 'UA-38592072-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>






<!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('8431-155-10-7537');/*]]>*/</script><noscript><a href="https://www.olark.com/site/8431-155-10-7537/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code -->
