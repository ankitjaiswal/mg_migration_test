<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
$count = 0;
$resourceData = array();
//pr($this->data['Source']);
if (isset($this->data['Source']) && count($this->data['Source']) >0) {
    foreach ($this->data['Source'] as $key => $value) {
		
        if ($value['resource_title'] != '' || $value['resource_location'] != '') {
            $count++;
			if (strpos($value['resource_location'], '://')) {
				$resourceData[] = array('resource_location'=>$value['resource_location'],'title' => $value['resource_title'], 'location' => $value['resource_location'],'id' => $value['id']);
			}else{
				$resourceData[] = array('resource_location'=>$value['resource_location'],'title' => $value['resource_title'], 'location' => $value['resource_location'],'id'=>$value['id']);
			}
        }
    }
}
if (!$count) {
    $count = 1;
    $resourceData[] = array('title' => '', 'resource_location' => '');
}

?>
<div id="resources" style="padding-top:20px;">
    <p><b>Resources</b></p>
    <?php   		
    for ($key = 0; $key < $count; $key++) {
		if(isset($this->data['Source'][$key]['id'])){
			e($form->hidden('Source.' . $key . '.id'));
			e($form->hidden('Source.' . $key . '.resource_location',array('value' => $resourceData[$key]['resource_location'])));
		}	
        ?>
        <p id="my_uploded_resource_<?php @e($resourceData[$key]['id']); ?>">
            <label><input type="text" id="Source<?php e($key); ?>ResourceTitle" maxlength="20" size="30" name="data[Source][<?php e($key); ?>][resource_title]" value="<?php e($resourceData[$key]['title']); ?>" placeholder="Title" style="width:182px;"/></label>
			<label>Upload file
			<?php e($form->file('Source.' . $key . '.resource_location_file', array('class'=>'resourceFile','div' => false, 'label' => false))); ?>
			</label>			
			<?php
            if ($key == 0) {?>
                <span id="addResource" class="addmore" style="cursor:pointer;">[ + ]</span>
				<?php 
			} else { ?>
                <span onclick="unlink_resource_draft('<?php e($resourceData[$key]['id']); ?>')" class="remResource addmore" style="cursor:pointer;">&nbsp;[ - ]</span>						
				<?php 
			} ?>
        </p>
    <?php } ?>		
</div>
<script>
    jQuery(document).ready(function(){

        jQuery(function() {
            var scntDiv = jQuery('#resources');
            var i = jQuery('#resources p').size() + 1;
			
            jQuery('#addResource').live('click', function() {
			
				if(i>=5){
					alert("You can enter up to 3 resources");
					return false;
				}
                var title = '<label>';
                title +='<input type="text" id="Source'+i+'ResourceTitle" maxlength="20" size="30" name="data[Source]['+i+'][resource_title]" value="" placeholder="Title" style="width:182px;"/>';
                title +='</label>';						
                var remove = '<span  class="remResource addmore" style="cursor:pointer;">&nbsp;[ - ]</span>';

                var  resourcefile= '<label> Upload file';
                resourcefile +='&nbsp;<input type="file" id="Source'+i+'resource_location_file" name="data[Source]['+i+'][resource_location_file]" />';
                resourcefile +='</label>';
					
                jQuery('<p>'+title+resourcefile+remove+'</p>').appendTo(scntDiv);
                i++;
                return false;
            });			
            jQuery('.remResource').live('click', function() { 
                if( i > 2 ) {
                    jQuery(this).parents('p').remove();
                    i--;
                }
                return false;
            });
        });

    });
</script>
