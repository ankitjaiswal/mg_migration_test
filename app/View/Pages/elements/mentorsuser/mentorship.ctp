<?php
$count = 0;
$questionData = array();
if(isset($this->data['Question'])){	
	foreach($this->data['Question'] as $key=>$value){
		if($value !=''){
			$count++;
			$questionData[] = $value;			
		}		
	}	
}
if(!$count){
	$count = 1;
	$questionData[] = array('question_name'=>'');
}
?>
<tr class="AddProFrm">
	<td colspan="2"><h3>Add Questions for mentorship</h3></td>
</tr> 

<tr>	
	<td colspan="2">
	<div id="p_question">
		<?php		
		for($key=0;$key<$count;$key++){?>
			<p><label>
			<?php
			e($form->input('Question.'.$key.'.question_name',array('value'=>$questionData[$key]['question_name'],'label'=>false,'div'=>false,'size'=>97,'placeholder'=>'Eg: What is holding you back from achieving your goal(s)?')));
			if($key ==($count-1)){							
				?>
				</label><span id="addQuestion" class="addmore">[ + ]</span>
				<?php
			}else{?>
				</label><span class="remQuestion addmore">&nbsp;[ - ]</span>						
				<?php
			}?>
			</p>
			<?php 		
			
		}?>		
		
	</div>		
	</td>					
</tr>

<script>
jQuery(document).ready(function(){

	jQuery(function() {
			var questionDiv = jQuery('#p_question');
			var i = jQuery('#p_question p').size() + 1;
			
			jQuery('#addQuestion').live('click', function() {
					var title = '<label>';
						title +='<input type="text" id="Question'+i+'QuestionName" size="97" name="data[Question]['+i+'][question_name]" value="" placeholder="Eg: What is holding you back from achieving your goal(s)?" />';
						title +='</label>';
					var remove = '<span class="remQuestion addmore">&nbsp;[ - ]</span>';
					jQuery('<p>'+title+remove+'</p>').appendTo(questionDiv);
					i++;
					return false;
			});
			
			jQuery('.remQuestion').live('click', function() { 
					if( i > 2 ) {
							jQuery(this).parents('p').remove();
							i--;
					}
					return false;
			});
	});

});
</script>