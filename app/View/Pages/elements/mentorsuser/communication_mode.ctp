<?php 
$facecm = '';
$phonecm = '';
$videocm = '';
if(isset($this->data['Communication'][0]['mode_type']) && $this->data['Communication'][0]['mode_type']){
	$facecm = 'checked';
}
if(isset($this->data['Communication'][1]['mode_type']) && $this->data['Communication'][1]['mode_type']){
	$phonecm = 'checked';
}
if(isset($this->data['Communication'][2]['mode_type']) && $this->data['Communication'][2]['mode_type']){
	$videocm = 'checked';
}
?>
<tr class="AddProFrm">
	<td colspan="2"><h3>Add Communication</h3></td>
</tr>
<tr>
	<td colspan="2">
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%">
				<input type="checkbox" name="data[Communication][0][mode_type]" id="Communication0ModeType" value="face_to_face" <?php e($facecm); ?> />
				In person
				</td>
				<td><?php e($form->input('Communication.0.mode',array('div'=>false,'label'=>false,'style'=>'width:430px;','placeholder'=>'e.g. New York, NY')));?></td>					
			</tr>
				
			<tr>
				<td>
				<input type="checkbox" name="data[Communication][2][mode_type]" id="Communication2ModeType" value="video" <?php e($videocm); ?> />
				Video
				</td>
				<td><?php e($form->input('Communication.2.mode',array('div'=>false, 'label'=>false,'style'=>'width:430px;','placeholder'=>'e.g. Skype, G+ Hangouts, etc.')));?></)));?></td>					
			</tr>		
		       <tr>
				<td>
				<input type="checkbox" name="data[Communication][1][mode_type]" id="Communication1ModeType" value="phone" <?php e($phonecm); ?> />
				Phone
				</td>
				<td><?php e($form->input('Communication.1.mode',array('div'=>false, 'label'=>false,'style'=>'width:430px;','placeholder'=>'Enter your phone number')));?></)));?></td>					
			</tr>
                </table>
	</td>
</tr>