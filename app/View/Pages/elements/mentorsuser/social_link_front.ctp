<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
$count = 0;
$socialData = array();
if (isset($this->data['Social'])) {
    foreach ($this->data['Social'] as $key => $value) {
        if ($value != '') {
            $socialData[] = $value;
            $count++;
        }
    }
    $this->data['Social'] = $socialData;
}
if (!$count) {
    $count = 1;
    $socialData[] = array('social_name' => '');
}?>
<div id="links" style="padding-top:20px;">
    <p><b>Profile links</b></p>
    <?php
    for ($key = 0; $key < $count; $key++) {
		if(isset($this->data['Social'][$key]['id'])){
			e($form->hidden('Social.' . $key . '.id'));
		}?>
		<p>
		<label>
			<?php
			e($form->input('Social.' . $key . '.social_name', array('value' => $socialData[$key]['social_name'], 'label' => false, 'div' => false, 'size' => 97,'placeholder'=>'e.g. twitter.com/name')));
			if ($key == 0) {?>			
				</label><span id="addSocial" class="addmore" style="cursor:pointer;">[ + ]</span>
				<?php
			} else { ?>
				</label><span class="remSocial addmore" style="cursor:pointer;">&nbsp;[ - ]</span>						
				<?php
			} ?>
		</p>
		<?php 
	} ?>
</div>
<script>
    jQuery(document).ready(function(){

        jQuery(function() {
            var socialDiv = jQuery('#links');
            var i = jQuery('#links p').size() + 1;
			
            jQuery('#addSocial').live('click', function() {
            	if(i == 10){
					alert("You can enter up to 7 Profile links.");
					return false;
			
				}				
				
				var title = '<label>';
                title +='<input type="text" id="Social'+i+'SocialName" size="97" name="data[Social]['+i+'][social_name]" value=""/>';
                title +='</label>';
                var remove = '<span class="remSocial addmore" style="cursor:pointer;">&nbsp;[ - ]</span>';
                jQuery('<p>'+title+remove+'</p>').appendTo(socialDiv);
                i++;
                return false;
            });
			
            jQuery('.remSocial').live('click', function() { 
                if( i > 2 ) {
                    jQuery(this).parents('p').remove();
                    i--;
                }
                return false;
            });
        });

    });
</script>