<tr class="AddProFrm">
	<td colspan="2"><h3>Add Case Studies</h3></td>
</tr>		
<tr>
	<td colspan="2">
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="middle" class="Padleft26">Add Title</td>
				<td>
					<?php  e($form->input('Case_Study.0.title', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>		
				</td>
			</tr>	
			<tr>
				<td valign="middle" class="Padleft26">Add Client Details</td>
				<td>
					<?php  e($form->input('Case_Study.0.client_details', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>		
				</td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Add URL</td>
				<td>
					<?php  e($form->input('Case_Study.0.url', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>		
				</td>
			</tr>		
			<tr>
				<td valign="middle" class="Padleft26">Add Situation</td>
				<td>
					<?php  e($form->input('Case_Study.0.situation', array('div'=>false, 'label'=>false, "class" => "Testbox")));?>		
				</td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Add Action</td>
				<td>
					<?php  e($form->input('Case_Study.0.actions', array('div'=>false, 'label'=>false, "class" => "Testbox")));?>		
				</td>
			</tr>	
			<tr>
				<td valign="middle" class="Padleft26">Add Result</td>
				<td>
					<?php  e($form->input('Case_Study.0.result', array('div'=>false, 'label'=>false, "class" => "Testbox")));?>		
				</td>
			</tr>
		</table>
	</td>
	
</tr>
	