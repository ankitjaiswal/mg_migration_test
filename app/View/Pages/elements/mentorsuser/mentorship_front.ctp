<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
$count = 0;
$questionData = array();
if (isset($this->data['Question'])) {
    foreach ($this->data['Question'] as $key => $value) {
        if ($value != '') {
            $count++;
            $questionData[] = $value;
        }
    }
}
if (!$count) {
    $count = 1;
    $questionData[] = array('question_name' => '');
}
?>
<div id="requestMentor" style="padding-top:20px;"><!--VL 31/12-->
    <p><span><b>Questions for applicants</b> </span><span class="grey">(optional)</span></p>
		<?php
		for ($key = 0; $key < $count; $key++) {
			if(isset($this->data['Question'][$key]['id'])){
				e($form->hidden('Question.' . $key . '.id'));
			}?>
        <p><label>
                <?php
		  e($form->input('Question.' . $key . '.question_name', array('maxlength'=>150,'value' => $questionData[$key]['question_name'], 'label' => false, 'div' => false, 'size' => 97, 'placeholder' => 'e.g. What is holding you back from achieving your goal(s)?','style'=>'width:675px;')));//VL 31/12
                if ($key == ($count - 1)) {
                    ?>
                </label><span id="addQuestion" class="addmore" style="cursor:pointer;color:red;">[ + ]</span>
                <?php } else { ?>
                </label><span class="remQuestion addmore" style="cursor:pointer;color:red;">&nbsp;[ - ]</span>						
                <?php } ?>
        </p>
        <?php
		} ?>
</div>
<script>
    jQuery(document).ready(function(){

        jQuery(function() {
            var questionDiv = jQuery('#requestMentor');
            var i = jQuery('#request p').size() + 1;
			
            jQuery('#addQuestion').live('click', function() {
				if(jQuery("input[id$='QuestionName']").length < 3){
					var title = '<label>';
					title +='<input type="text" id="Question'+i+'QuestionName" size="97" name="data[Question]['+i+'][question_name]" value="" maxlength="150",placeholder="Eg: Please explain your need for mentorshop" />';//VL 2/1/2013
					title +='</label>';
					var remove = '<span class="remQuestion addmore" style="cursor:pointer;color:red;">&nbsp;[ - ]</span>';
					jQuery('<p>'+title+remove+'</p>').appendTo(questionDiv);
					i++;
					return false;
				}else{
					alert("You can enter up to 3 questions for applicants.");
					return false;
				}				
            });
			
            jQuery('.remQuestion').live('click', function() { 
                if( i > 2 ) {
                    jQuery(this).parents('p').remove();
                    i--;
                }
                return false;
            });
        });

    });
</script>