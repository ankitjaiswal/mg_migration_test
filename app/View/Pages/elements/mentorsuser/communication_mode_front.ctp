<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php 
$facecm = '';
$phonecm = '';
$videocm = '';
if(isset($this->data['Communication'][0]['mode_type']) && $this->data['Communication'][0]['mode_type'] ){
	$facecm = 'checked';
}
if(isset($this->data['Communication'][2]['mode_type']) && $this->data['Communication'][2]['mode_type'] ){
	$phonecm = 'checked';
}
if(isset($this->data['Communication'][1]['mode_type']) && $this->data['Communication'][1]['mode_type']){
	$videocm = 'checked';
}
if(isset($this->data['Communication'][0]['id'])){
	e($form->hidden('Communication.0.id'));
}
if(isset($this->data['Communication'][1]['id'])){
	e($form->hidden('Communication.1.id'));
}
if(isset($this->data['Communication'][2]['id'])){
	e($form->hidden('Communication.2.id'));
}
?>
<div id="face-mode" style="padding-top:20px;">
    <div id="mode">
        <p><b>Mode</b></p>
    </div>
  
    <div id="face-box">
        <p class="styled-form">
            <input type="checkbox" name="data[Communication][0][mode_type]" id="a2" value="face_to_face" <?php e($facecm); ?> />
            <label for="a2"><img src="<?php e(SITE_URL)?>/imgs/locator-pin.png" style="margin-right:2px;width: 16px;height: 16px;">In person</label>
        </p>
       <p class="styled-form">
            <input type="checkbox" name="data[Communication][1][mode_type]" id="a3" value="video" <?php e($videocm); ?> />
            <label for="a3"><img src="<?php e(SITE_URL)?>/imgs/computer.png" style="margin-right:4px;width: 16px;height: 16px;">Online</label>
        </p>  
      <p class="styled-form">
            <input type="checkbox" name="data[Communication][2][mode_type]" id="a1" value="phone" <?php e($phonecm); ?> />
            <label for="a1"><img src="<?php e(SITE_URL)?>/imgs/phone.png" style="margin-right:2px;width: 16px;height: 16px;">Phone</label>
        </p>
       
    </div>
    <div id="face-text">
        <?php e($form->input('Communication.0.mode',array('maxlength'=>35,'div'=>false,'label'=>false,'class'=>'forminput1','placeholder'=>'e.g. New York, NY')));?>
        <?php e($form->input('Communication.1.mode',array('maxlength'=>35,'div'=>false, 'label'=>false,'class'=>'forminput1','placeholder'=>'e.g. Skype, Google Hangout')));?></)));?>
         <?php e($form->input('Communication.2.mode',array('maxlength'=>35,'div'=>false, 'label'=>false,'class'=>'forminput1','placeholder'=>'Business phone number')));?></)));?>
            </div>
</div>