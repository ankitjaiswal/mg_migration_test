<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php 
$facecm = '';
$onlinecm = '';
if(isset($this->data['Communication'][0]['mode_type']) && $this->data['Communication'][0]['mode_type']){
	$facecm = 'checked';
}
if(isset($this->data['Communication'][1]['mode_type']) && $this->data['Communication'][1]['mode_type']){
	$onlinecm = 'checked';
}
if(isset($this->data['Communication'][0]['id'])){
	e($form->hidden('Communication.0.id'));
}
if(isset($this->data['Communication'][1]['id'])){
	e($form->hidden('Communication.1.id'));
}
?>
<div id="face-mode">
    <div id="mode">
        <p><b>Mode</b></p>
    </div>
    <div id="face-box">
        <p class="styled-form">
            <input type="checkbox" name="data[Communication][0][mode_type]" id="a1" value="face_to_face" <?php e($facecm); ?> />
            <label for="a1">Face to Face</label>
        </p>
        <p class="styled-form">
            <input type="checkbox" name="data[Communication][1][mode_type]" id="a2" value="online" <?php e($onlinecm); ?> />
            <label for="a2">Online</label>
        </p>
    </div>
    <div id="face-text">
        <?php e($form->input('Communication.0.mode',array('maxlength'=>35,'div'=>false,'label'=>false,'class'=>'forminput1','placeholder'=>'Eg.Honololu downtown')));?>
        <?php e($form->input('Communication.1.mode',array('maxlength'=>35,'div'=>false, 'label'=>false,'class'=>'forminput1','placeholder'=>'Eg.Skype')));?></)));?>
    </div>
</div>