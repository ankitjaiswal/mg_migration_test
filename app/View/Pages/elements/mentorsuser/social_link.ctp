<?php
$count = 0;
$socialData = array();
if(isset($this->data['Social'])){	
	foreach($this->data['Social'] as $key=>$value){
		if($value !=''){
			$socialData[] = $value;
			$count++;			
		}		
	}	
	$this->data['Social'] = $socialData;	
}
if(!$count){
	$count = 1;
	$socialData[] = array('social_name'=>'');
}
?>
<tr class="AddProFrm">
	<td colspan="2"><h3>Add Social Links</h3></td>
</tr>

<tr>	
	<td colspan="2">
	<div id="p_social">

			<?php
			//echo count($this->data['Social']);
			
			for($key=0;$key<$count;$key++){?>
				<p><label>
				<?php
				e($form->input('Social.'.$key.'.social_name',array('value'=>$socialData[$key]['social_name'],'label'=>false,'div'=>false,'size'=>97,'placeholder'=>'Twitter')));
				if($key ==0){						
					?>
					</label><span id="addSocial" class="addmore">[ + ]</span>
					<?php
				}else{?>
					</label><span class="remSocial addmore">&nbsp;[ - ]</span>						
					<?php
				}?>
				</p>
				<?php 		
				
			}?>
	</div>		
	</td>					
</tr>

<script>
jQuery(document).ready(function(){

	jQuery(function() {
			var socialDiv = jQuery('#p_social');
			var i = jQuery('#p_social p').size() + 1;
			
			jQuery('#addSocial').live('click', function() {
			    if(i == 8){
                    alert("You can enter up to 7 Profile links.");
                    return false;
            
                }
					var title = '<label>';
						title +='<input type="text" id="Social'+i+'SocialName" size="97" name="data[Social]['+i+'][social_name]" value="" />';
						title +='</label>';
					var remove = '<span class="remSocial addmore">&nbsp;[ - ]</span>';
					jQuery('<p>'+title+remove+'</p>').appendTo(socialDiv);
					i++;
					return false;
			});
			
			jQuery('.remSocial').live('click', function() { 
					if( i > 2 ) {
							jQuery(this).parents('p').remove();
							i--;
					}
					return false;
			});
	});

});
</script>