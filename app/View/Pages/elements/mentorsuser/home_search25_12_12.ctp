<div id="spotlight-wrapper">
<?php  e($form->create('City', array('url' => array('controller' => 'fronts', 'action' => 'search_result'),'id'=>'mainSearchForm')));?>
	<div id="spotlight" class="pagewidth">
		<div id="find-mentor">
			<h1>Find your mentor</h1>
			<h2>Unique access to top experts, to help you soar</h2>
			<div id="text-box">
				<div id="enter-keywords">
					<div id="enter-text">
					<?php 	e($form->input('City.keyword',array('div'=>false,'label'=>false,'class'=>'forminput-new','placeholder'=>'Mentor name or expertise... or just leave blank')))	?>
					</div>
					<div id="new-york">
						<?php 	e($form->input('City.city_name',array('id'=>'username','div'=>false,'label'=>false,'class'=>'newyork')))	?>

						<?php e($html->image('loading.gif',array('id'=>'loading','style'=>'margin-top:-30px;float:right;display:none;')));?>
																								
					</div>
				</div>
			   <div id="search">
				<?php e($form->submit('Search',array('class'=>'searchSubmit','div'=>false,'label'=>false))); ?>
			   </div>		

			</div>
		</div>		
	</div>
	<?php e($form->end()); ?>
</div>
<!-- http://www.codeproject.com/Tips/191892/How-to-find-current-location-based-on-the-IP-addre -->
<script language="JavaScript" src="http://j.maxmind.com/app/geoip.js"></script>
<script language="JavaScript">
	var conntry_code 		= geoip_country_code();
	var geoip_city 	 		= geoip_city();	
	var geoip_region_name 	= geoip_region_name();	
	jQuery(document).ready(function(){
		jQuery.ajax({
			url:SITE_URL+'/fronts/getStateCode',
			type:'post',
			dataType:'json',
			data:'state_name='+geoip_region_name,
			success:function(res){
				if(res.value !=''){
					state_name = res.value;	
					if(conntry_code !='IN'){
						jQuery(".newyork").val(geoip_city+', '+state_name);
					}else{
						jQuery(".newyork").val('New York, NY');
					}					
				}else{
					if(conntry_code !='IN'){
						jQuery(".newyork").val(geoip_city+', '+geoip_region_name);
					}else{
						jQuery(".newyork").val('New York, NY');
					}
				}
			}
		});
		
	});
</script>

