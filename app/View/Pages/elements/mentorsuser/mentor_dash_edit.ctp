<?php
$ApplyRequestData = $this->General->MentorLog();
if(count($ApplyRequestData)>0 && !empty($ApplyRequestData)){ ?> 
<div style="padding-top:15px;padding-bottom:15px;">	
<h1><span style="font-family:'proximanova semibold',Ubuntu;font-size: large;margin-left:37px;">Status</span></h1>
<hr style="width:thin solid #dedede;padding-left:10px;"></hr>

<style>
   .col3{width:38%!important;}
   .col4{width:33%!important;}
</style>
<div id="user-status" style="width:598px;margin-left:37px;margin-bottom:10px;">				  
                      <?php
							$col3='';
							foreach($ApplyRequestData as $apply)
							{
								if($apply[0]['applicationType'] == '1' || $apply[0]['inv_date'] != null){
							    $userInfo = $this->General->getUserReferenceData($apply[0]['mentee_id']);
                                if($apply[0]['inv_title']!=''){
                                    $title = ucfirst($apply[0]['inv_title']);
	                                if(strlen($title) > 25) {
	                                	$title = substr($title, 0, 25);
	                                	$title = $title."...";
	                                }
                                }
                                else
                                    $title = '';
                                
                                if($apply[0]['inv_date']!='')
                                    $invDate = date('d-M-y',$apply[0]['inv_date']);
                                else
                                    $invDate = '';
                                if($apply[0]['inv_time']!=''){
                                    $TimeData = explode(":",$apply['0']['inv_time']);
                                    $invTime = $TimeData[0].":".$TimeData[1]." ".$TimeData[2];
                                }
                                else
                                    $invTime = '';
                                
								if($apply[0]['inv_timezone'] == 1){
			                    	$invTimeZone = 'Pacific';
			                    }else if($apply[0]['inv_timezone'] == 2){
			                    	$invTimeZone = 'Alaska Time';
			                    }else if($apply[0]['inv_timezone'] == 3){
			                    	$invTimeZone = 'Hawaii';
			                    }else if($apply[0]['inv_timezone'] == 4){
			                    	$invTimeZone = 'Mountain';
			                    }else if($apply[0]['inv_timezone'] == 5){
			                    	$invTimeZone = 'Central';
			                    }else if($apply[0]['inv_timezone'] == 6){
			                    	$invTimeZone = 'Eastern';
			                    }
                                
                                
								if($apply[0]['applicationType']=='1')
									$col3='Consultation request received on '.date('dS M Y',strtotime($apply[0]['created']));
                                
								if($apply[0]['applicationType']=='3')
									$col3=$title.' proposed';
                                
								if($apply[0]['applicationType']=='4')
									$col3='Request to reschedule';
                                
								if($apply[0]['applicationType']=='5')
									$col3=$html->link($title,array('controller'=>'invoice','action'=>'invoice_view/'.$apply[0]['inv_id']),array('class'=>'delete')).' completed';
                                
								if($apply[0]['applicationType']=='6')
									$col3='Application reject';
                                
                                if($apply[0]['applicationType']=='7')
                                    $col3=$html->link($title,array('controller'=>'invoice','action'=>'invoice_view/'.$apply[0]['inv_id']),array('class'=>'delete')).' confirmed<br/>'.$invDate.", ".$invTime." (" .$invTimeZone.")";
					  ?>
                     			<div class="row" id="edit_row_delete<?php echo $apply[0]['id'];?>">
                                                          <?php if($userInfo['User']['role_id'] == '3'){?>

								  <div class="col1">
								      <div class="minus_user">
								          <a id='remove' style="cursor:pointer" onclick=removedSession('<?php echo $apply[0]['id']; ?>') ><?php e($html->image('media/minus1.png' , array('style'=>'cursor:pointer', "height"=>"19", "width"=>"24"))); ?></a>
								      </div>
								  <a href="<?php echo SITE_URL."clients/my_account/".strtolower($userInfo['UserReference']['url_key']);?>"><?php echo ucfirst(strtolower($userInfo['UserReference']['first_name'])).' '.ucfirst(strtolower($userInfo['UserReference']['last_name']));?></a></div>
								  <?php }else{?>
                                                         <div class="col1">
								      <div class="minus_user">
								          <a id='remove' style="cursor:pointer" onclick=removedSession('<?php echo $apply[0]['id']; ?>') ><?php e($html->image('media/minus1.png' , array('style'=>'cursor:pointer', "height"=>"19", "width"=>"24"))); ?></a>
								      </div>
								  <a href="<?php echo SITE_URL.strtolower($userInfo['UserReference']['url_key']);?>"><?php echo ucfirst(strtolower($userInfo['UserReference']['first_name'])).' '.ucfirst(strtolower($userInfo['UserReference']['last_name']));?></a></div>
                                                         <?php }?>
                                                         <div class="col2">
								  
								  <?php if($apply[0]['applicationType']=='1'):   ?>
								      &nbsp;
								  <?php else: 
								  
								  $unreadMessagesCount = $this->General->GetUnreadLogs($apply[0]['id']);

								  if($unreadMessagesCount[0] > 0){
									?>
								  	<a href="<?php echo SITE_URL."clients/private_log/".$apply[0]['id'];?>" style="font-weight:bold;">Log</a>
								  	<?php 
								  }
								  else {
								  	?>
								  	<a href="<?php echo SITE_URL."clients/private_log/".$apply[0]['id'];?>">Log</a>
								  	<?php 
								  }
								  ?>
								 <?php endif; ?></div>
								  <div class="col3"><?php echo $col3;?></div>
								  
								<?php if($apply[0]['applicationType']=='1'):   ?>
								    
										<div class="col4" style="text-align: right;"><a href="<?php echo SITE_URL."members/application_review/".$apply[0]['id'];?>">Review</a></div>
										
								<?php /*	<div class="col4"><a href="javascript:mentorshipStatusChange('accept','<?php echo $apply[0]['Mentorship']['id'];?>');">Accept</a> &nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:mentorshipStatusChange('reject','<?php echo $apply['Mentorship']['id'];?>');">Reject</a></div>*/ ?>
								
								<?php elseif($apply[0]['applicationType']=='3' && ($apply[0]['inv_status']=='0' || $apply[0]['inv_status']=='')): 
									
									$hideShow = 'display:block;';
									if($this->Session->check('hide_reminder_link') && $this->Session->read('hide_reminder_link') == $apply[0]['id']){
										$hideShow = 'display:none;';
										$this->Session->delete('hide_reminder_link');
									}
								?>
								    
											<div class="col4" style="text-align: right;<?php echo $hideShow;?>"><a class="send_reminder_hide" href="<?php echo SITE_URL."invoice/send_reminder/".$apply[0]['id']."/member/orientation";?>">Send reminder</a></div>
								
								<?php 	elseif($apply[0]['applicationType']=='4'): ?>
								
											<div class="col4" style="text-align: right;"><a href="<?php echo SITE_URL."invoice/edit_invoice/".$apply[0]['inv_id'];?>">Review</a></div>
								
								<?php 		elseif($apply[0]['applicationType']=='5'): ?>
								
												<div class="col4" style="text-align: right;"><a href="<?php echo SITE_URL."invoice/schedule_next/".$apply[0]['id'];?>">Schedule next session</a></div>
								
								<?php 			elseif($apply[0]['applicationType']=='7'): ?>
								
												    <div class="col4" style="text-align: right;"><a class="registeration" alt="Registration" href="<?php echo SITE_URL."invoice/complete_session/".$apply[0]['id'];?>">Mark as complete</a>
													<?php
														$hideShow = 'display:inline-block;';
														if($this->Session->check('hide_reportissue_link') && $this->Session->read('hide_reportissue_link') == $apply[0]['id']){
															$hideShow = 'display:none;';
															$this->Session->delete('hide_reportissue_link');
														}
													?>
													<span style="<?php echo $hideShow;?>" id="pipe_<?php echo $apply[0]['id']; ?>">&nbsp;|&nbsp;</span>
													<a style="<?php echo $hideShow;?>" class="report_issue_hide_<?php echo $apply[0]['id']; ?>" alt="Registration" href="javascript:reportissue_popup('<?php echo $apply[0]['id']; ?>','mentor');">Report an issue</a></div>							
								
								<?php 	        endif; ?>
								
								<?php echo $html->image('loading.gif',array('style'=>'display:none;','id'=>'loadImage'.$apply[0]['id'])); ?>
								
								</div>
					 <?php }} ?>
					  <div class="clear"></div>
					  </div>
<?php } ?>

<script type="text/javascript">
function applicationReview_popup(mentorship_id){
	TINY.box.show({url:SITE_URL+'/members/application_review/'+mentorship_id,width:500,height:500});
}	

</script>