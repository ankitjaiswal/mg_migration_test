<div id="navigation">
    <div class="glossymenu">
        <?php /*         * ************************** Admin Management Management Start**************************** */ ?>			
        <?php e($html->link('Admin Management', array(), array('class' => 'menuitem submenuheader'))); ?>
        <div class="submenu">
            <ul>
                <li><?php e($html->link(__('List Admin User', true), array('controller' => 'users', 'action' => 'index'))); ?></li>                             
            </ul>
        </div>	
        <?php /*         * ************************** Admin Management Management End**************************** */ ?>			
        <?php e($html->link('Directory Management', array(), array('class' => 'menuitem submenuheader'))); ?>
        <div class="submenu">
            <ul>
                <li><?php e($html->link(__('List Directory Users', true), array("controller" => "members", "action" => "directory_index"))); ?></li>
                <li>
                    <?php e($html->link(__('Clean Directory Data', true), array("controller" => "members", "action" => "directory_clean"))); ?>
                </li> 
                <li>
                    <?php e($html->link(__('Add Directory User', true), array("controller" => "members", "action" => "directory_add"))); ?>
                </li>                
            </ul>
        </div>
        <?php /*         * ************************** Mentors Management Start**************************** */ ?>			
        <?php e($html->link('Members Management', array(), array('class' => 'menuitem submenuheader'))); ?>
        <div class="submenu">
            <ul>
                <li><?php e($html->link(__('List Members', true), array("controller" => "members", "action" => "index"))); ?></li>
                <li>
                    <?php e($html->link(__('Add Member', true), array("controller" => "members", "action" => "add"))); ?>
                </li> 
                <li>
                    <?php e($html->link(__('Featured Member', true), array("controller" => "members", "action" => "featured"))); ?>
                </li>
                <li>
                    <?php e($html->link(__('Pending Member', true), array("controller" => "members", "action" => "pending"))); ?>
                </li>  
                <li>
                    <?php e($html->link(__('Search Member', true), array("controller" => "members", "action" => "search"))); ?>
                </li>              
            </ul>
        </div>
        <?php /*         * ************************** Mentees Management Start**************************** */ ?>			
        <?php e($html->link('Clients Management', array(), array('class' => 'menuitem submenuheader'))); ?>
        <div class="submenu">
            <ul>
                <li><?php e($html->link(__('List Clients', true), array("controller" => "clients", "action" => "index"))); ?></li>
                <li>
                    <?php e($html->link(__('Add Client', true), array("controller" => "clients", "action" => "add"))); ?>
                </li> 
                <li>
                    <?php e($html->link(__('Prospective Client', true), array("controller" => "fronts", "action" => "prospective"))); ?>
                </li>                        
            </ul>
        </div>	
        <?php /*         * ************************** Answers Management Start**************************** */ ?>			
        <?php e($html->link('Answers Management', array(), array('class' => 'menuitem submenuheader'))); ?>
        <div class="submenu">
            <ul>
                <li>
                    <?php e($html->link(__('QnA Categories', true), array('controller' => 'qna', 'action' => 'categories'))); ?>
                </li> 
                <li>
                    <?php e($html->link(__('Member Categories', true), array("controller" => "qna", "action" => "member_categories"))); ?>
                </li>   
                <li>
                    <?php e($html->link(__('Question Review', true), array("controller" => "qna", "action" => "question_review"))); ?>
                </li> 
                <li>
                    <?php e($html->link(__('Media Record', true), array("controller" => "qna", "action" => "media_record"))); ?>
                </li>
                <li>
                    <?php e($html->link(__('Add Media Query', true), array("controller" => "qna", "action" => "media_add"))); ?>
                </li>                     
            </ul>
        </div>		
        <?php e($html->link('Project Management', array(), array('class' => 'menuitem submenuheader'))); ?>
        <div class="submenu">
            <ul>
                <li>
                    <?php e($html->link(__('Project review', true), array('controller' => 'project', 'action' => 'review'))); ?>
                </li> 
            </ul>
        </div>		
        <?php e($html->link('Global Settings Management', array(), array('class' => 'menuitem submenuheader'))); ?>
        <div class="submenu">
            <ul>
                <li><?php e($html->link(__('Global Settings Management List', true), array('plugin' => null, 'controller' => 'settings', 'action' => 'index'))); ?></li>
                <?php /* <li><?php e($html->link(__('Global Email Template Settings', true), array('plugin' => null, 'controller' => 'email_templates', 'action' => 'index'))); ?></li> */?>
                <li><?php e($html->link(__('Static Pages Manger', true), array('controller' => 'static_pages', 'action' => 'index'))); ?></li>
                <li>
                    <?php # e($html->link(__('Add Voucher', true), array("controller"=>"vouchers", "action"=>"add")));?>
                </li>     
                <li>
                    <?php e($html->link(__('Autocomplete expertise', true), array('controller' => 'reports', 'action' => 'autocomplete'))); ?>
                </li> 
                <li>
                    <?php e($html->link(__('Search Indexes', true), array('controller' => 'browse', 'action' => 'indexes'))); ?>
                </li> 
            </ul>
        </div>
        <?php e($html->link('Newsletters Management', array('controller' => 'pages', 'action' => 'index'), array('class' => 'menuitem submenuheader'))); ?>
        <div class="submenu">
            <ul>
                <li>
                    <?php e($html->link(__('List Subscriber Member', true), array('controller' => 'news_letters', 'action' => 'index'))); ?>
                </li><?php /*?>
                <li>
                    <?php e($html->link(__('Add NewsLetter', true), array('controller' => 'news_letters', 'action' => 'add'))); ?>
                </li>                        <?php */?>
            </ul>
        </div>	
         <?php e($html->link('User Reports Management', array('controller' => 'pages', 'action' => 'index'), array('class' => 'menuitem submenuheader'))); ?>
         <div class="submenu">
            <ul>
                <li>
                    <?php e($html->link(__('Users list', true), array('controller' => 'reports', 'action' => 'userlist'))); ?>
                </li>
                <li>
                    <?php e($html->link(__('Members list', true), array('controller' => 'reports', 'action' => 'mentorlist'))); ?>
                </li>
                <li>
                    <?php e($html->link(__('Program list', true), array('controller' => 'reports', 'action' => 'programlist'))); ?>
                </li>
                 <li>
                    <?php e($html->link(__('Program status', true), array('controller' => 'reports', 'action' => 'weeklyprogramlist'))); ?>
                </li>
                <li>
                    <?php e($html->link(__('Invoice list', true), array('controller' => 'reports', 'action' => 'invoicelist'))); ?>
                </li>
                <li>
                    <?php e($html->link(__('Message log users', true), array('controller' => 'reports', 'action' => 'userlog'))); ?>
                </li>
                <li>
                    <?php e($html->link(__('Member statistics', true), array('controller' => 'reports', 'action' => 'mentorstatics'))); ?>
                </li>
                <li>
                    <?php e($html->link(__('Search details', true), array('controller' => 'reports', 'action' => 'search'))); ?>
                </li>
                <li>
                    <?php e($html->link(__('Referral details', true), array('controller' => 'reports', 'action' => 'referral'))); ?>
                </li>
            </ul>
        </div>  		
        <?php /*         * ************************** Static Page Management Start**************************** */ ?>
        <?php //e($html->link('Static Page Management', array('controller'=>'pages', 'action'=>'index'), array('class'=>'menuitem submenuheader')));?>
        <!-- <div class="submenu">
<ul>
<li>
        <?php e($html->link(__('List Pages', true), array('controller' => 'static_pages', 'action' => 'index'))); ?>
						</li>
<li>
        <?php e($html->link(__('Add Page', true), array('controller' => 'static_pages', 'action' => 'add'))); ?>
						</li>                        
</ul>
				</div> -->	
        <?php /*         * ************************** Static Page Management End**************************** */ ?>


    </div>
</div>