 <?php 
 e($javascript->link(array('fckeditor'), false));
 //e($form->hidden('token_key', array('value' => $this->params['_Token']['key'])));
 ?>
<table border="0" class="Admin2Table" width="100%">		
	<tr>
		<td valign="middle" class="Padleft26"  width="20%">To <span class="input_required">*</span></td>
		<td><?php e($form->input('NewsLetterHistory.to', 
		array('div'=>false, 'label'=>false, "class" => "TextBox5",'readonly'=>true)));?>
		</td>
	</tr>			  
	<tr>
		<td valign="middle" class="Padleft26">Subject <span class="input_required">*</span></td>
		<td><?php e($form->input('NewsLetterHistory.subject', 
		array('div'=>false, 'label'=>false, "class" => "TextBox5")));?>
		</td>
	</tr>

	<tr>
		<td valign="middle" class="Padleft26">Message <span class="input_required">*</span></td>
		<td>
		<?php e($form->input('NewsLetterHistory.description', array('type'=>'textarea','div'=>false, 'label'=>false, "class" => "textarea")));?>
		<?php echo $fck->load('NewsLetterHistory/description',"Default");?>
		</td>
	</tr>    			  
</table>