 <?php  
 e($form->hidden('token_key', array('value' => $this->params['_Token']['key'])));
 ?>
<table border="0" class="Admin2Table" width="100%">		
	<tr>
		<td valign="middle" class="Padleft26"  width="20%">Email <span class="input_required">*</span></td>
		<td><?php e($form->input('NewsLetter.email', 
		array('div'=>false, 'label'=>false, "class" => "TextBox5")));?>
		</td>
	</tr>  
	<tr>
		<td valign="middle" class="Padleft26">Status</td>
		<td>
		<?php e($form->input('NewsLetter.status', array('options'=>Configure::read('Status'),'div'=>false, 'label'=>false, "class" => "TextBox5")));?>
		</td>
	</tr> 		  
</table>