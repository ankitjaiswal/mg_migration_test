<?php
  e($javascript->link(array('prototype','jquery/jquery.min', 'product'), false));  				
?>
<?php
   e($form->create('Product', array('url'=>'/search','name'=>'search', 'id'=>'search', 'onsubmit'=>'return searchProduct()')));
?>
<div class="SearchBlock">
<div class="InputBlock">
<?php
    e($form->input('Product.search', array('value'=>'Search here', 'div'=>false, 'label'=>false, 'onfocus'=>"this.value==this.defaultValue?this.value='':null" , 'onblur'=>"this.value==''?this.value=this.defaultValue:null;")));
?>
</div>
<div class="FloatLeft">
   <?php e($form->submit('search_icone.jpg'));?>
</div>
<div class="Clear"></div>
</div>
<?php
  e($form->end());
?>