<?php

/**
 * Users Controller
 *
 * PHP version 5
 * Project start date 29 Mar 2012
 * Developed by :Sandeep singh 
 * Company info:Octalsolution 
 * @Users Controller
 */
App::import('Sanitize');

class UsersController extends AppController {

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    var $name = 'Users';
    /**
     * Models used by the Controller
     *
     * @var array
     * @access public
     */
    var $uses = array('User','UserImage', 'UserReference','Source','Availablity','Communication','Question','Social');
    var $helpers = array('General', 'Form', 'Ajax', 'javascript', 'Paginator');
    var $components = array("Session", "Email", "Auth", "RequestHandler" ,"Upload");

    function beforeFilter() {
		//echo Security::hash('admin1467', null, true);die;
        parent::beforeFilter();
        $this->Auth->allow('register','login','registration_step2','checkEmail','thanks','checkEmailPassword','profile_photo_edit');
		
    }

	function logout_message(){
		$this->layout = 'ajax';
	}
    function admin_login() {
        $this->set('title_for_layout', 'Admin User Login');
        $this->layout = 'admin_login';
        if ($this->Auth->user()) {
            if ($this->Auth->user('id') == '1') {
                $this->Session->write('Admin.email', $this->data['User']['email']);
                $this->Session->write('Admin.password', $_POST['data']['User']['password']);
                $this->redirect($this->Auth->redirect());
            }
        }
    }

    function admin_dashboard() {
        $this->set('title_for_layout', 'Dashboard');
    }

    function admin_logout() {
        $this->Session->setFlash(__('Log out successful.', true));
        $this->redirect($this->Auth->logout());
    }

    function admin_index() {
        if (!isset($this->params['named']['page'])) {
            $this->Session->delete('UserSearch');
        }
        $this->User->bindModel(array('hasOne' => array('UserReference')), false);
        $filters = array('User.role_id=1');
        if (!empty($this->data)) {
            $this->Session->delete('UserSearch');
            if (!empty($this->data['UserReference']['first_name'])) {
                $keyword = Sanitize::escape($this->data['UserReference']['first_name']);
                $this->Session->write('UserSearch', $keyword);
            }
        }

        if ($this->Session->check('UserSearch')) {
            $filters[] = array('UserReference.first_name LIKE' => "%" . $this->Session->read('UserSearch') . "%");
        }
        /* Paginate method is used to find all listing of countries */
        $this->paginate['User'] = array(
            'limit' => Configure::read('App.AdminPageLimit'),
            'order' => array('User.username' => 'ASC'),
            'conditions' => $filters
        );

        $data = $this->paginate('User');
        //prd($data);
        $this->set('data', $data);
        $this->set('title_for_layout', __('User', true));
    }

    function admin_add() {
        $this->set('title_for_layout', 'Add Admin Users');
        if (!empty($this->data)) {
            $this->User->set($this->data);
            $this->User->setValidation('admin');
            $this->UserReference->set($this->data);
            $this->UserReference->setValidation('admin');
            $this->data['User']['password'] = $this->data['User']['password2'];//Security::hash($this->data['User']['password2'], null, true);
            if ($this->User->saveAll($this->data, array('validate' => 'first'))) {
                $this->Session->setFlash('The Admin has been saved', 'admin_flash_good');
                $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash('Please correct the errors listed below.', 'admin_flash_bad');
            }
        } else {
            unset($this->data['User']['username']);
        }
    }

    function admin_edit($id = null) {

        $this->set('title_for_layout', "Edit Admin Users");
        if (!$id && empty($this->data)) {
            $this->Session->setFlash('Invalid user');
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            $this->User->data['User']['role_id'] = Configure::read('App.Role.Admin');
            $this->User->set($this->data);
            $this->User->setValidation('admin');

            $this->UserReference->set($this->data);
            $this->UserReference->setValidation('admin');

            if ($this->User->validates() && $this->UserReference->validates()) {
                if ($this->User->saveAll($this->data)) {

                    $this->Session->setFlash('Updated successfully', 'admin_flash_good');
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash('Please correct the errors listed below.', 'admin_flash_bad');
                }
            } else {
                $this->Session->setFlash('The Admin could not be updated. Please, try again.', 'admin_flash_bad');
            }
        } else {
            $this->data = $this->User->read(null, $id);
        }
    }

    function admin_delete($id = null) {
        if (!$id) {
            $this->Session->setFlash('Invalid id for Admin');
            $this->redirect(array('action' => 'index'));
        }
        $admin = $this->User->read(null, $id);
        if (empty($admin)) {
            $this->Session->setFlash('Invalid User', 'admin_flash_bad');
            $this->redirect(array('action' => 'index'));
        }
        if ($this->User->delete($id)) {
            $this->Session->setFlash('User has been deleted successfully', 'admin_flash_good');
            $this->redirect(array('action' => 'index'));
        }
        $this->Session->setFlash('User has not deleted', 'admin_flash_bad');
        $this->redirect(array('action' => 'index'));
    }

    function message_delete($flag = null, $id=null) {
        $this->loadModel('MessageCenter');
        if (!$id) {
            $this->Session->setFlash('Invalid id for users');
            $this->redirect(array('action' => 'message_center'));
        }
        $front = $this->MessageCenter->read(null, $id);
        if (empty($front)) {
            $this->Session->setFlash('Invalid Message', 'flash_bad');
            $this->redirect(array('action' => 'message_center'));
        }
        if ($flag == "inbox") {
            $data = array(
                'MessageCenter' => array(
                    'id' => $id,
                    'receiver_delete' => 1
                )
            );
            $this->MessageCenter->save($data, false, array('receiver_delete'));
        }
        if ($flag == "outbox") {
            $data = array(
                'MessageCenter' => array(
                    'id' => $id,
                    'sender_delete' => 1
                )
            );
            $this->MessageCenter->save($data, false, array('sender_delete'));
        }
        $this->Session->setFlash('User has been deleted successfully', 'flash_bad');
        $this->redirect(array('action' => 'message_center'));
    }

    function admin_process() {

        if (!empty($this->data)) {
            App::import('Sanitize');
            $action = Sanitize::escape($this->data['User']['pageAction']);
            foreach ($this->data['User'] AS $value) {
                if ($value != 0) {
                    $ids[] = $value;
                }
            }

            if (count($this->data) == 0 || $this->data['User'] == null) {
                $this->Session->setFlash('No items selected.', 'admin_flash_bad');
                $this->redirect(array('controller' => 'Users', 'action' => 'index'));
            }
            if ($action == "delete") {
                $this->User->deleteAll(array('User.id' => $ids));
                $this->Session->setFlash('Users have been deleted successfully', 'admin_flash_good');
                $this->redirect(array('controller' => 'Users', 'action' => 'index'));
            }
            if ($action == "activate") {
                $this->User->updateAll(array('User.status' => Configure::read('App.Status.active')), array('User.id' => $ids));
                $this->Session->setFlash('Users have been activated successfully', 'admin_flash_good');
                $this->redirect(array('controller' => 'Users', 'action' => 'index'));
            }
            if ($action == "deactivate") {
                $this->User->updateAll(array('User.status' => Configure::read('App.Status.inactive')), array('User.id' => $ids));
                $this->Session->setFlash('Users have been deactivated successfully', 'admin_flash_good');
                $this->redirect(array('controller' => 'Users', 'action' => 'index'));
            }
        } else {
            $this->redirect(array('controller' => 'Users', 'action' => 'index'));
        }
    }

    function admin_changepassword($id = null) {
        $this->pageTitle = __('Change Password', true);
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid User', true));
            $this->redirect(array('action' => 'index'));
        }
        if (!empty($this->data)) {
            $this->User->setValidation('change_password');
            $this->User->set($this->data);
            if ($this->User->validates()) {
                $this->data['User']['password'] = $this->data['User']['password2'];//Security::hash($this->data['User']['password2'], null, true);
                $this->User->updateAll(array('password' => "'" . $this->data['User']['password'] . "'"), array('User.id' => $id));
                $this->Session->setFlash('Password has been changed successfully', 'admin_flash_good');
                $this->redirect(array('action' => 'index'));
            }
        } else {
            $this->data = $this->User->read(null, $id);
        }
    }

    function admin_viewuser($id=null) {
        $this->set('title_for_layout', 'Admin Personal Information');
        if ($id) {
            $user_information = $this->User->find('all', array('conditions' => array('User.id' => $this->Auth->user('id'))));
            $this->set('data', $user_information);
        }
    }
    function register(){
		//step1
    	$this->layout = 'ajax';    	
    }
    
    //step2
    function registration_step2(){
    	
 		if(isset($this->data) && count($this->data)>0){
 			if(isset($this->data['UserReference']['country'])){ 
 				
	 			$this->data['User']['password'] = $this->data['User']['password2'];//Security::hash($this->data['User']['password2'], null, true);
				$this->data['User']['role_id'] = Configure::read('App.Role.Mentee');
				$this->data['User']['status'] = Configure::read('App.Status.active');
				if($this->User->save($this->data)){
					
					
					$id = $this->User->getInsertID();
					$this->data['UserReference']['user_id'] = $id ;
					$this->UserReference->save($this->data['UserReference']);
					$this->_insertFile(array('id'=>$id ,'resume'=>$this->data['Resume1']));	
					///send email
					$this->Email->to = $this->data['User']['username'];
					$this->Email->from = Configure::read('Site.email');
					$this->Email->subject = 'Welcome to Mentorsguild';
					$this->Email->sendAs = 'html';
					$this->Email->template = 'mentee_register';					
					$this->set('data',$this->data);
					$this->Email->send();
					////////////////
					$this->redirect(array('action'=>'thanks',$id));
				}else{
					$this->Session->setFlash("Please remove below errors");
				}				
 			} 			
			
 		}else{
 			$this->redirect(array('controller'=>'fronts','action'=>'index'));
 			
 		}
    }
	function _insertFile($imageDataArr){
		////upload resume...
		
		if(isset($imageDataArr['resume']) && $imageDataArr['resume']['resume_location']['name'] !=''){
			$this->loadModel('Resume');
			$destination = IMAGES.MENTEES_RESUME_PATH.DS.$imageDataArr['id'];
			if(!is_dir($destination)){
				mkdir($destination, 0777);
			}
			$destination .=DS.$imageDataArr['resume']['resume_location']['name'];
			move_uploaded_file($imageDataArr['resume']['resume_location']['tmp_name'], $destination);
			
			$data = array('resume_location'=>$destination,'user_id'=>$imageDataArr['id']);
			$this->Resume->save($data);					
		}				
		//////////////////////////////////////////////			
		
	}
    
    function thanks($lastInsertId = null){
    	if(!$lastInsertId){
    		$this->redirect(array('controller'=>'fronts','action'=>'index'));
    	}
    	
    } 
    function checkEmail(){
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $email = $_REQUEST['email'];
           
            $count = $this->User->find('count', array('conditions' => array('User.status' =>array(0,1),'User.username' =>trim($email))));
            
          
            $this->layout = '';
            $this->render(false);
            echo json_encode(array('value' => $count));
            exit();
        }   	
    	
    }
    function checkEmailPassword(){
    	
        if ($this->RequestHandler->isAjax()) {
            Configure::write('debug', 0);
            $email = $_REQUEST['email'];
            $pass = $_REQUEST['pass'];//Security::hash($_REQUEST['pass'], null, true);
            $count = $this->User->find('count', array('conditions' => array('User.status' =>1,'User.username' =>trim($email),'User.password' =>trim($pass))));
            $this->layout = '';
            $this->render(false);
            echo json_encode(array('value' => $count));
            exit();
        }    	
    }  
    function login(){
		/*
		*if mentor id we get from apply for mentorship page
		*/
		if(isset($this->params['named']['mentor_id'])){
			$this->data['User']['mentor_id'] = $this->params['named']['mentor_id'];
		}
    	
    	$this->layout = 'ajax';
        $this->set('title_for_layout', 'User Login'); 
		
        if ($this->Auth->user()) {	

			$UserData = $this->User->read('password',$this->Auth->user('id'));
			//set image logined user
			if(isset($UserData['UserImage'][0]['image_name'])){
				$this->Session->write('User.image',$UserData['UserImage'][0]['image_name']);
			}
			
			if($this->Auth->user('id') ==1){			
				$this->redirect(array('controller'=>'fronts','action'=>'index'));
			}
			////////////Apply for mentorship section for login controller/////////////////////
			if(isset($this->data['User']['mentor_id']) && $this->data['User']['mentor_id'] !=''){
				if($this->Auth->user('role_id') == 2){
					$this->Session->setFlash('Sorry! You have logined as mentor.');
					$this->redirect(array('controller'=>'static_pages','action'=>'thanks'));
				}else{
					$this->redirect(array('controller'=>'fronts','action'=>'consultation_request',$this->data['User']['mentor_id']));
				}
			}		
			/////////////////////////////////

			
			if($this->Auth->user('access_specifier') =='draft' && $this->Auth->user('role_id') == Configure::read('App.Role.Mentor')){
				$this->redirect(array('controller'=>'members','action'=>'member_full_profile'));
				
			}elseif($this->Auth->user('access_specifier') =='publish' && $this->Auth->user('role_id') == Configure::read('App.Role.Mentor') && $UserData['User']['password'] ==''){
				$this->redirect(array('controller'=>'members','action'=>'account_setting'));				
			}else{
				$this->redirect(array('controller'=>'fronts','action'=>'index'));
			}			
           
        }   	
    }
    function logout(){
   		$this->redirect($this->Auth->logout());
    	
    }    
    function my_account($id= null){
		$this->set("title_for_layout","My Profile");		
		
		if($id=="")
		{
			$id=$this->Auth->user('id');
		}
		$this->User->Behaviors->attach('Containable');
		$this->data = $this->User->find('first',
			array(
				'contain'=>array(
					'UserReference'=>array(
						'fields'=>array('first_name','last_name','background_summary','area_of_expertise','desire_mentee_profile','fee_first_hour','fee_regular_session','created'),
					),
					'UserImage'	,
					'Source',
					'Communication',
					'Availablity',
					'Social'
				),
				'conditions'=>array('User.id'=>$id)		
			)
	
		);
		
		//$user_information = $this->City->find('all', array('conditions' => array('City.zip_code' => $this->Auth->user('id'))));
        //$this->set('data', $user_information);
		$dataZip=mysql_fetch_array(mysql_query("SELECT * FROM cities WHERE zip_code IN ( SELECT zipcode FROM user_references WHERE user_id='".$id."')"));
		$this->set("city",$dataZip['city_name']);
		$this->set("state",$dataZip['state']);
		$this->set("resourcepath","img/".MENTORS_RESOURCE_PATH.DS.$this->Auth->user('id').DS);
		//prd($this->data);
    	
    }    
	
	 function edit_account($id= null){
		$this->set("title_for_layout","Edit Profile");	
		$this->User->Behaviors->attach('Containable');
		if (empty($this->data)) 
		{	
		
			$this->User->Behaviors->attach('Containable');
			$this->data = $this->User->find('first',
				array(
					'contain'=>array(
						'UserReference'=>array(
							'fields'=>array('first_name','last_name','background_summary','area_of_expertise','desire_mentee_profile','fee_first_hour','fee_regular_session','created','zipcode','application_5_fee','paypal_account'),
						),
						'UserImage'	,
						'Source',
						'Communication',
						'Availablity',
						'Social',
						'Question',
					),
					'conditions'=>array('User.id'=>$this->Auth->user('id'))		
				)
		
			);
			//prd($this->data);
			//$user_information = $this->City->find('all', array('conditions' => array('City.zip_code' => $this->Auth->user('id'))));
			//$this->set('data', $user_information);
			$totalSocial=count($this->data['Source']);
			$totalQuestion=count($this->data['Question']);
			$dataZip=mysql_fetch_array(mysql_query("SELECT * FROM cities WHERE zip_code IN ( SELECT zipcode FROM user_references WHERE user_id='".$this->Auth->user('id')."')"));
			$this->set("totalSocial",$totalSocial);
			$this->set("totalQuestion",$totalQuestion);
			$this->set("city",$dataZip['city_name']);
			$this->set("state",$dataZip['state']);
			$this->set("resourcepath",IMAGES.MENTORS_RESOURCE_PATH.DS.$this->Auth->user('id').DS);
		}
		else
		{
			 //App::import('Sanitize');
			// data updae UserReference Table
			
			$this->UserReference->updateAll(array('UserReference.first_name' => "'" . $this->data['UserReference']['first_name'] . "'",'UserReference.last_name' => "'" . $this->data['UserReference']['last_name'] . "'", 'UserReference.zipcode' => "'" . $this->data['UserReference']['zipcode'] . "'",'UserReference.area_of_expertise' => "'" . $this->data['UserReference']['area_of_expertise'] . "'", 'UserReference.background_summary' => "'" . $this->data['UserReference']['background_summary']  . "'" , 'UserReference.desire_mentee_profile' => "'" . $this->data['UserReference']['desire_mentee_profile']  . "'" , 'UserReference.fee_first_hour' => "'" . $this->data['UserReference']['fee_first_hour']  . "'" , 'UserReference.fee_regular_session' => "'" . $this->data['UserReference']['fee_regular_session']  . "'", 'UserReference.application_5_fee' => "'" . $this->data['UserReference']['application_5_fee']  . "'", 'UserReference.paypal_account' => "'" . $this->data['UserReference']['paypal_account']  . "'"), array('UserReference.user_id' => $this->Auth->user('id')));
			
			
			// first data delete and insert data 
			//$this->Source->deleteAll(array('User.id' => $this->Auth->user('id')));
			/*$condition=array('Source.user_id' => $id);
			$this->Source->deleteAll($condition);*/
			$resourceCount=0;
			/*prd($_REQUEST['resource_title']);
			die();*/
			$resourceOldArray=array();
			$resourceOldArrayCounter=1;
			$resourceOldSql=mysql_query("SELECT * FROM sources WHERE user_id='".$this->Auth->user('id')."'");
			while($resourceOldData=mysql_fetch_array($resourceOldSql))
			{
				$resourceOldArray[$resourceOldArrayCounter]=$resourceOldData['resource_title'];
				$resourceOldArrayCounter++;
			}
			
			while(count($_REQUEST['resource_title'])>$resourceCount)
			{
					$key=0;
					$key=array_search($_REQUEST['resource_title'][$resourceCount], $resourceOldArray);
					if($key>=1)
					{
						$resourceOldArray[$key]="";
					}
				/*prd($_FILES['resource_location']);
				die();*/
				if(trim($_REQUEST['resource_title'][$resourceCount])!='' and  trim($_FILES['resource_location']['name'][$resourceCount])!='')
				{
					
					$this->_insertFileResource(array('id'=>$this->Auth->user('id') ,'resource_title'=>$_REQUEST['resource_title'][$resourceCount],'resource_location'=>$_FILES['resource_location']),$resourceCount);	
					/*$array['Source']['user_id']=$id;
					$array['Source']['resource_title']=$_REQUEST['resource_title'][$resourceCount];
					$array['Source']['resource_location']=$_REQUEST['resource_location'][$resourceCount];
					$this->Source->create();
					$this->Source->save($array,false);*/
				}
				$resourceCount++;
			}
			/*prd($resourceOldArray);
			die();*/
			$resourceDeleteCount=1;
			while(count($resourceOldArray)>=$resourceDeleteCount)
			{
				if(trim($resourceOldArray[$resourceDeleteCount])!="")
				{
					$condition=array('Source.user_id' => $this->Auth->user('id'), 'Source.resource_title' => trim($resourceOldArray[$resourceDeleteCount]));
					$this->Source->deleteAll($condition);
				}
				$resourceDeleteCount++;
			}
			$this->Availablity->updateAll(array('Availablity.day_time' => "'" . $this->data['Availablity']['day_time'] . "'"), array('Availablity.user_id' => $this->Auth->user('id')));
			$questionCount=0;
			//prd($this->data);
			//die();
			$this->Question->deleteAll(array('Question.user_id' => $id));
			while(count($_REQUEST['question'])>$questionCount)
			{
				//prd($_FILES['resource_location']);
				if(trim($_REQUEST['question'][$questionCount])!='')
				{
					$questionArray['Question']['user_id']=$id;
					$questionArray['Question']['question_name']=$_REQUEST['question'][$questionCount];
					$this->Question->create();
					$this->Question->save($questionArray,false);
				}
				$questionCount++;
			}
			$this->Communication->deleteAll(array('Communication.user_id' => $id));
			//prd($this->data['User']);
			
			if($this->data['User']['facetofacemode_type']!="0")
			{
				$array=array();
				$array['Communication']['user_id']=$id;
				$array['Communication']['mode']=$this->data['User']['facetofacemode'];
				$array['Communication']['mode_type']="face_to_face";
				$this->Communication->create();
				$this->Communication->save($array,false);
			}
			if($this->data['User']['onlinemode_type']!="0")
			{
				$array=array();
				$array['Communication']['user_id']=$id;
				$array['Communication']['mode']=$this->data['User']['onlinemode'];
				$array['Communication']['mode_type']="online";
				$this->Communication->create();
				$this->Communication->save($array,false);
			}
			//// Social link edit START /////////////////
			$this->Social->deleteAll(array('Social.user_id' => $this->Auth->user('id')));
			$linkCount=0;
			
			while(count($_REQUEST['link'])>$linkCount)
			{
				//prd($_FILES['resource_location']);
				if(trim($_REQUEST['link'][$linkCount])!='')
				{
					//prd($_REQUEST['link'][$linkCount]);
					$linkArray=array();
					$linkArray['Social']['user_id']=$this->Auth->user('id');
					$linkArray['Social']['social_name']=$_REQUEST['link'][$linkCount];
					$this->Social->create();
					$this->Social->save($linkArray,false);
				}
				$linkCount++;
			}
			//// Social link edit END /////////////////
			
			//$this->set($this->data,$this->data);
			//$this->Session->setFlash("Your Resource file size not more than 2 MB");
			$this->redirect(array('action' => 'my_account'));
			
			
		}
		//prd($this->data);
    	
    }
	
	function _insertFileResource($imageDataArr,$pointer){
		////upload resume...
		//prd("dd");
		if($imageDataArr['resource_location']['name'][$pointer] !=''){
		
			$ext=$this->_findexts($imageDataArr['resource_location']['name'][$pointer]);
			$size=$imageDataArr['resource_location']['size'][$pointer];
			if(($ext=="jpg" || $ext=="jpeg" || $ext=="png" ||  $ext=="gif" || $ext=="ppt" || $ext=="doc" || $ext=="pdf" || $ext=="docx" || $ext=="pptx" || $ext=="xlsx" || $ext=="xls") &&  $size<=2097152)
			{
				$this->loadModel('Source');
				$condition=array('Source.user_id' => $imageDataArr['id'], 'Source.resource_title' => trim($imageDataArr['resource_title']));
				$this->Source->deleteAll($condition);
				$destination = IMAGES.MENTORS_RESOURCE_PATH.DS.$imageDataArr['id'];
				if(!is_dir($destination)){
					mkdir($destination, 0777);
				}
				else
				{
					chmod($destination, 0777);
				}
				$destination .=DS.$imageDataArr['resource_location']['name'][$pointer];
				move_uploaded_file($imageDataArr['resource_location']['tmp_name'][$pointer], $destination);
				
				$data = array('resource_title'=>$imageDataArr['resource_title'],'resource_location'=>$imageDataArr['resource_location']['name'][$pointer],'user_id'=>$imageDataArr['id']);
				$this->Source->create();
				$this->Source->save($data);	
			}
			else
			{
				$this->Session->setFlash("Your Resource file size not more than 2 MB");
			}				
		}
	}				        
	function autocompleteZipcode(){
		
		
		$data = array('AD'=>'aaaa','a'=>'ssss');
		echo json_encode($data);
		
	}	
    function admin_runQuery() {
        $this->autoRender = false;
		/*ALTER TABLE `user_references` ADD `area_of_expertise` VARCHAR( 500 ) NOT NULL AFTER `phone` ,
ADD `desire_mentee_profile` VARCHAR( 1000 ) NOT NULL AFTER `area_of_expertise` */
        $this->User->query("ALTER TABLE `user_references` DROP `city_id`");
        echo "runed successfully.";
        die;
    }
	
	function _findexts ($filename)
	{
		$filename = strtolower($filename) ;
		$exts = split("[/\\.]", $filename) ;
		$n = count($exts)-1;
		$exts = $exts[$n];
		return $exts;
	} 
	
	function profile_photo_edit()
	{
		$this->layout = 'ajax';
        $this->set('title_for_layout', 'Photo Edit'); 
		if (empty($this->data)) 
		{
			$this->set("id",$this->Auth->user('id'));
		}
		else
		{
			/*prd(phpinfo());
			die();*/
			$this->_insertProfileImage($_FILES['photoedit'] , $this->Auth->user('id'));	
			$this->redirect(array('action' => 'edit_account'));
		}       
        
	}
	function social_link_edit()
	{
		//$this->autoRender=false;
		$this->layout = 'ajax';
        $this->set('title_for_layout', 'Social link Edit'); 
		if (empty($this->data)) 
		{
			$this->loadModel('UserImage');
			$this->data = $this->User->find('first',
				array(
					'contain'=>array(
						'Social',
					),
					'conditions'=>array('User.id'=>$this->Auth->user('id'))		
				)
		
			);
			$this->set("id",$this->Auth->user('id'));
		}
		else
		{
			
			$this->Social->deleteAll(array('Social.user_id' => $this->Auth->user('id')));
			$linkCount=0;
			while(count($_REQUEST['link'])>$linkCount)
			{
				//prd($_FILES['resource_location']);
				if(trim($_REQUEST['link'][$linkCount])!='')
				{
					//prd($_REQUEST['link'][$linkCount]);
					$linkArray=array();
					$linkArray['Social']['user_id']=$this->Auth->user('id');
					$linkArray['Social']['social_name']=$_REQUEST['link'][$linkCount];
					$this->Social->create();
					$this->Social->save($linkArray,false);
				}
				$linkCount++;
			}
			//$this->_insertProfileImage($_FILES['photoedit'] , $this->Auth->user('id'));	
			$this->redirect(array('action' => 'edit_account'));
		}       
	}
	
	  function _insertProfileImage($imageDataArr,$id) {
        //////uppload profile....
        
		$ext=$this->_findexts($imageDataArr['name']);
		if($ext=="jpg" || $ext=="jpeg" || $ext=="png" ||  $ext=="gif" )
		{
			
			if (isset($imageDataArr['name']) && $imageDataArr['name'] != '') {
			
			$this->loadModel('UserImage');
				
				$destination = IMAGES . MENTORS_IMAGE_PATH . DS . $id;
				if (!is_dir($destination)) {
					mkdir($destination, 0777);
				}
				$destination .=DS;
				if (!is_dir($destination.'small')) {
					mkdir($destination.'small', 0777);
				}
				$imageArr = $imageDataArr;
				//prd($destination);
			/*	$ad_image = md5(rand() * time()) . ".$ext";
				$originalImage=$destination.$ad_image;
				@copy($imageArr['tmp_name'],$originalImage);
				$actualImageName=md5(rand() * time()) . "11.$ext";
				$this->_smart_resize_image($imageArr['tmp_name'], $width = 150, $height = 150, $proportional = true, $output = $destination.$actualImageName, $delete_original = false, $use_linux_commands = false, $apply_watermark = false, $watermark_image = false );
				$this->_smart_resize_image($imageArr['tmp_name'], $width = 30, $height = 30, $proportional = true, $output = $destination.'small/'.$actualImageName, $delete_original = true, $use_linux_commands = false, $apply_watermark = false, $watermark_image = false );*/
				
				$result = $this->Upload->upload($imageArr, $destination, null, array('type' => 'resizecrop', 'size' => array('150', '150')));
				$result = $this->Upload->upload($imageArr, $destination.'small/', $this->Upload->result, array('type' => 'resizecrop', 'size' => array('30', '30')));
				//prd($this->Upload->result);
				$this->UserImage->updateAll(array('UserImage.image_name' => "'" . $this->Upload->result . "'"), array('UserImage.user_id' => $id));
						
			}
		}
	}
	
	

    /*     * *************front page processing******** */
}

?>