<?php
// during registeration from admin as mentors by created by admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($data['UserReference']['first_name'])){
			echo ucfirst(strtolower($data['UserReference']['first_name'])).' '.ucfirst(strtolower($data['UserReference']['last_name']));
		}else{
			echo 'xxxx';
		}		
		?></td>
	</tr>
	<tr>
		<td>
		<p>
			You are now a member of Mentors Guild, and we are proud to have you in our midst.
		</p>
		<p>
			The Guild is a fellowship of experts and experienced professionals, such as yourself, who want to help others succeed. We want to make it easy for you to find high<br>
			quality learners who respect your time and expertise.
		</p>
		<p>
		Our rules of engagement are fairly straightforward, but if you haven't already done so, please take a few minutes to read our <?php e($html->link('Terms of Service',SITE_URL."fronts/terms_of_service",array('style'=>"color:#f23034;",'target'=>'blank'))); ?> and <?php e($html->link('Privacy Policy.',SITE_URL."fronts/privacy_policy",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>
		<p>
		We are commited to support you in this journey. Please contact us with your ideas, questions or concerns.
		</p>
		<p>
		Thank you,<br>
		<b>Iqbal Ashraf</b><br>
		Executive Director<br>
		<?php e($html->link($html->image(SITE_URL.'img/email-logo.png',array('alt'=>'logo','style'=>'padding-top:5px;')),SITE_URL,array('escape'=>false))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>		
		</td>
	</tr>	
</table>