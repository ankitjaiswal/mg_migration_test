<?php if($from=='mentor'){ ?>
<table>
	<tr>
		<td>Dear Administrator,</td>
	</tr>
	<tr>
		<td>
		<p>
			 <?php e(ucwords($data['Mentor']['first_name'].' '.$data['Mentor']['last_name'])); ?>(Mentor) has reported <?php e(ucwords($data['Mentee']['first_name'].' '.$data['Mentee']['last_name'])); ?>(Mentee).
		</p>
		With following comment:-
		<p>
			<?php
				echo $comment;
			?>
		</p>
		<p>
		<?php e($html->link($html->image(SITE_URL.'img/email-logo.png',array('style'=>'padding-top:5px;','alt'=>'logo')),SITE_URL,array('escape'=>false))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>		
		</td>
	</tr>	
</table>
<?php } ?>
<?php if($from=='mentee'){ ?>
<table>
    <tr>
        <td>Dear Administrator,</td>
    </tr>
    <tr>
        <td>
        <p>
         <?php e(ucwords($data['Mentee']['first_name'].' '.$data['Mentee']['last_name'])); ?>(Mentee) has reported <?php e(ucwords($data['Mentor']['first_name'].' '.$data['Mentor']['last_name'])); ?> (Mentor).
        </p>
		
		With following comment:-
		<p>	
			<?php
				echo $comment;
			?>
		</p>
		<p>
        <?php e($html->link($html->image(SITE_URL.'img/email-logo.png',array('style'=>'padding-top:5px;','alt'=>'logo')),SITE_URL,array('escape'=>false))); ?>
        </p>
        <p>
        Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
        Phone : 808.729.5850<br>
        eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
        </p>        
        </td>
    </tr>   
</table>
<?php }
?>