<?php
// during registeration from admin as mentors by created by admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($data['User']['first_name'])){
			echo $data['User']['first_name'];
		}else{
			echo 'xxxx';
		}		
		?></td>
	</tr>
	<tr>
		<td>
		<p>
			You are now a member of Mentors Guild, and we are proud to have you in our midst.
		</p>
		<p>
			The Guild is a fellowship of experts and experienced professionals, who want to help others succeed. We want to make it easy for you to find high quality mentors who are there to help you achieve expertise.
		</p>
		<p>
		Our rules of engagement are fairly straightforward, but if you haven't already done so, please take a few minutes to read our <?php e($html->link('Terms of Service',SITE_URL."fronts/terms_of_service",array('style'=>"color:#f23034;",'target'=>'blank'))); ?> and <?php e($html->link('Privacy Policy.',SITE_URL."fronts/privacy_policy",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>
		<p>
		We are commited to support you in this journey. Please contact us with your ideas, questions or concerns.
		</p>
		
		<p>
		Your Mentorsguild Credentials Are: <br/>
		<br/>Username: <?php echo $data['User']['username'];?><br />
		     Password: <?php echo $data['User']['password2'];?><br />
		</p>
		<p>
		Thank you,<br>
		<b>Iqbal Ashraf</b><br>
		Executive Director<br>
		<?php e($html->image(SITE_URL.'img/email-logo.png',array('style'=>'padding-top:5px;','alt'=>'logo'))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>		
		</td>
	</tr>	
</table>