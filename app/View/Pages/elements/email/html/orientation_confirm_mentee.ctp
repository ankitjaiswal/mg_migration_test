<?php
// during mentors_declined from admin  by  admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($data['Mentee']['first_name'])){
			echo "<b>".ucwords($data['Mentee']['first_name']).",</b>";
		}else{
			echo 'xxxx';
		}
        $timeData = $this->General->timeformatchange($data['Invoice']['inv_time'], $data['Invoice']['duration_hours'], $data['Invoice']['duration_minutes']);
        
		?></td>
	</tr>
	 <?php if($data['Invoice']['inv_mode']=='1')
                 $mode = 'Face to face';
              if($data['Invoice']['inv_mode']=='2')
                 $mode = 'Online';
              if($data['Invoice']['inv_mode']=='3')
                 $mode = 'Phone';
                      
             ?>
	<tr>
		<td>
		<p>
		Your next appointment is confirmed for <br/>
        Date        : <?php echo date('m/d/Y',$data['Invoice']['inv_date']); ?> (<?php echo date('l',$data['Invoice']['inv_date']); ?>)<br/> 
        Time        : <?php echo $timeData; ?><br/>
        <?php echo $mode; ?> : <?php echo $data['Invoice']['mode_text']; ?><br/>
        </p>
        
        <p>
        <a href="<?php echo SITE_URL."invoice/ical/".$data['Invoice']['id']; ?>" style="color:#f23034;">Add to calendar</a>&nbsp;&nbsp;&nbsp;&nbsp;
        <?php if($data['Invoice']['inv_mode']=='1'){ ?>
            <a href="<?php echo SITE_URL."invoice/invoice_view/".$data['Invoice']['id']; ?>" style="color:#f23034;">Map</a><br/>
       <?php } ?><br/>
       </p>
       <p>
        Please contact us if you have questions.<br/>
		</p>
		
		<p>
		
		<?php e($html->image(SITE_URL.'img/email-logo.png',array('alt'=>'logo','style'=>'padding-top:5px;'))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>
		</td>
	</tr>	
</table>