<?php
// during mentors_declined from admin  by  admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($data['UserReference']['first_name'])){
			echo "<b>".ucwords($data['UserReference']['first_name'].' '.$data['UserReference']['last_name']).",</b>";
		}else{
			echo 'xxxx';
		}
		
		?></td>
	</tr>
	<tr>
		<td>
		<p>Sorry, <?php echo ucwords($data['Mentor']['first_name'].' '.$data['Mentor']['last_name']); ?> has declined your application.<br/>
		There are several possible reasons for this. Common ones are either that your profile is not complete; or maybe the mentor has chosen an applicant who they can help better.<br/>
        Here are a few things you can do to improve your chances<br/>
        1. <a href="<?php echo SITE_URL."clients/edit_account"; ?>">Update</a> your profile<br/>
        2. Review Mentor's 'Desired mentee profile'<br/>
        3. Apply to another mentor in your area of interest<br/>
		</p>
		<p>As always, we are here to help.<br><br>
		<?php e($html->image(SITE_URL.'img/email-logo.png',array('alt'=>'logo','style'=>'padding-top:5px;'))); ?>
		</p>
		<p>
        Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
        Phone : 808.729.5850<br>
        eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
        </p>
		</td>
	</tr>	
</table>
