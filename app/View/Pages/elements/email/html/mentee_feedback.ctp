<?php
// during mentors_declined from admin  by  admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($data['UserReference']['first_name'])){
			echo "<b>".ucwords($data['UserReference']['first_name']).",</b>";
		}else{
			echo 'xxxx';
		}
		
		?></td>
	</tr>
	<tr>
		<td>
		<p>
		You have <a href="<?php echo SITE_URL."users/my_account"; ?>">feedback</a> from <?php echo "<b>".ucwords($data['Mentee']['first_name'].' '.$data['Mentee']['last_name']).",</b>";?>.  You may make it 'Public' to be viewable by everyone, or may keep it 'Private' to yourself.<br/>
        Making feedback 'public' will help attract more applications for mentorship.<br/>
		Contact us if you have questions.
		</p>
		
		<p>
		We thank you for your interest.<br><br>
		<?php e($html->image(SITE_URL.'img/email-logo.png',array('alt'=>'logo','style'=>'padding-top:5px;'))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>
		</td>
	</tr>	
</table>