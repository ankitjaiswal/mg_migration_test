<?php
// during mentors_declined from admin  by  admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($data['UserReference']['first_name'])){
			echo "<b>".ucwords($data['UserReference']['first_name'].' '.$data['UserReference']['last_name']).",</b>";
		}else{
			echo 'xxxx';
		}
		
		?></td>
	</tr>
	<tr>
		<td><p>We thank you for your interest.</p>
		<p>We get a lot of applications, and we only accept new members in small batches to ensure we can serve them well.</p>
		<p>Although at this time, we are unable to include you as a member of Mentors Guild, we will retain your information in our database and review it again in coming weeks.</p>
		<p>We will be in touch.</p>
		<p><?php e($html->image(SITE_URL.'img/email-logo.png',array('alt'=>'logo','style'=>'padding-top:5px;'))); ?>
		</p>
		</td>
	</tr>	
</table>