<?php
// during registeration from admin as mentors by created by admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($mentorData['UserReference']['first_name'])){
			echo ucwords($mentorData['UserReference']['first_name'].' '.$mentorData['UserReference']['last_name']);
		}else{
			echo 'xxxx';
		}		
		?></td>
	</tr>
	<tr>
		<td>
		<p>
			You have received an <a href="<?php echo SITE_URL."users/my_account"; ?>" style="color:#f23034;">application</a> from  <?php echo ucwords($menteeData['UserReference']['first_name'].' '.$menteeData['UserReference']['last_name']); ?>. Please review <a href="<?php echo SITE_URL."mentors/application_review/".$mentorship_id; ?>" style="color:#f23034;">here</a>.
		</p>
		<p>
			If you are not accepting new applications, right now, be sure to change your <a href="<?php echo SITE_URL."users/edit_account"; ?>" style="color:#f23034;">profile settings.</a></p>
		<p>
		Contact us if you have questions.
		</p>
				
		<p>
		Thank you,<br>
		<b>Iqbal Ashraf</b><br>
		
		<?php e($html->link($html->image(SITE_URL.'img/email-logo.png',array('alt'=>'logo','style'=>'padding-top:5px;')),SITE_URL,array('escape'=>false))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>		
		</td>
	</tr>	
</table>