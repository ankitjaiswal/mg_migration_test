<?php
// during registeration from admin as mentors by created by admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($menteeData['UserReference']['first_name'])){
			echo ucwords($menteeData['UserReference']['first_name'].' '.$menteeData['UserReference']['last_name']);
		}else{
			echo 'xxxx';
		}		
		?></td>
	</tr>
	<tr>
		<td>
		<p>
			You have applied to <?php e(ucwords($mentorData['UserReference']['first_name'].' '.$mentorData['UserReference']['last_name'])); ?>  for mentorship.
		</p>
		<p>
			Please allow <?php e(ucwords($mentorData['UserReference']['first_name'])); ?> 2 business days to review your application.
		</p>
		<p>
		Contact us if there are any delays or if you have questions.		
		</p>
				
		<p>
		Thank you,<br>
		<b>Iqbal Ashraf</b><br>
		
		<?php e($html->link($html->image(SITE_URL.'img/email-logo.png',array('style'=>'padding-top:5px;','alt'=>'logo')),SITE_URL,array('escape'=>false))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>		
		</td>
	</tr>	
</table>