<?php
// during registeration from admin as mentors by created by admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($data['MenteeFName'])){
			echo ucwords($data['MenteeFName']).",";
		}else{
			echo 'xxxx';
		}		
		?></td>
	</tr>
	<tr>
		<td>
		<p>
			 Per your request, we will let you know whenever <?php e(ucwords($data['MentorFName'])." ".ucwords($data['MentorLName'])); ?> restarts<br/>
	         accepting mentorship applications.<br/>
	         Contact us if you have questions.  
	    </p>
				
		<p>
		<?php e($html->link($html->image(SITE_URL.'img/email-logo.png',array('style'=>'padding-top:5px;','alt'=>'logo')),SITE_URL,array('escape'=>false))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>		
		</td>
	</tr>	
</table>