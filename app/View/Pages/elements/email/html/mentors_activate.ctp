<?php
// during activateMentor from admin  by  admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($data['UserReference']['first_name'])){
			echo "<b>".ucwords($data['UserReference']['first_name'].' '.$data['UserReference']['last_name']).",</b>";
		}else{
			echo 'xxxx';
		}
		
		?></td>
	</tr>
	<tr>
		<td>
		<p>
		Congratulations! Your application has been accepted. There are a couple more steps needed for you to become a member.
		
		</p>
		<ul>
			<li>-Take a few minutes to read our <?php e($html->link('Terms of Service',SITE_URL."fronts/terms_of_service",array('style'=>"color:#f23034;",'target'=>'blank'))); ?> and <?php e($html->link('Privacy Policy',SITE_URL."fronts/privacy_policy",array('style'=>"color:#f23034;",'target'=>'blank'))); ?></li>
			<li>-Create your <a href="<?php echo SITE_URL."mentors/activate/{$data['User']['username']}/{$data['User']['activation_key']}"; ?>">profile</a>. This might take several minutes, but a sharp profile makes a world of difference to the quality of mentorship applications you might get)<br/>
			</li>
		</ul>
		<p>Please contact us with your ideas, questions or concerns</p>
		<p>
		 Thank you,<br></p>
		<p><b>Iqbal Ashraf</b><br>
		Executive Director<br>
		<?php e($html->image(SITE_URL.'img/email-logo.png',array('style'=>'padding-top:5px;','alt'=>'logo'))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<a href="mailto:help@mentorsguild.com" style="color:#f23034;"><span style="color:#f23034;">help@mentorsguild.com </span></a>
		</p>
		</td>
	</tr>	
</table>