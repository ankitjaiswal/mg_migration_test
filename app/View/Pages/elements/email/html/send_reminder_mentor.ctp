<?php if($email_type=='application'){ ?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($data['Mentor']['first_name'])){
			echo ucwords($data['Mentor']['first_name'].' '.$data['Mentor']['last_name']);
		}else{
			echo 'xxxx';
		}		
		?></td>
	</tr>
	<tr>
		<td>
		<p>
			 <?php e(ucwords($data['Mentee']['first_name'].' '.$data['Mentee']['last_name'])); ?> are applied for mentorship orientation please accept his reqeuest. 
		<p>
		<?php e($html->link($html->image(SITE_URL.'img/email-logo.png',array('style'=>'padding-top:5px;','alt'=>'logo')),SITE_URL,array('escape'=>false))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>		
		</td>
	</tr>	
</table>
<?php } ?>

<?php if($email_type=='reschedule request'){?>
   <table>
    <tr>
        <td>Dear
        <?php
        if(isset($data['Mentor']['first_name'])){
            echo ucwords($data['Mentor']['first_name'].' '.$data['Mentor']['last_name']);
        }else{
            echo 'xxxx';
        }       
        ?></td>
    </tr>
    <tr>
        <td>
          <p><?php echo ucwords($data['Mentee']['first_name'].' '.$data['Mentee']['last_name']); ?> has sent you a request to reschedule the appointment.<br/>
Contact us if you have questions.</p>
        <?php e($html->link($html->image(SITE_URL.'img/email-logo.png',array('style'=>'padding-top:5px;','alt'=>'logo')),SITE_URL,array('escape'=>false))); ?>
        </p>
        <p>
        Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
        Phone : 808.729.5850<br>
        eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
        </p>        
        </td>
    </tr>   
</table> 
<?php } ?>