<?php
// during activateMentor from admin  by  admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($data['UserReference']['first_name'])){
			echo "<b>".ucwords($data['UserReference']['first_name'].' '.$data['UserReference']['last_name']).",</b>";
		}else{
			echo 'xxxx';
		}
		
		?></td>
	</tr>
	<tr>
		<td>
		<p>
            Congratulations! You are now a registered user of Mentors Guild.
        </p>
        <p>We want to make it easy for you to find high quality mentors, who are vested in your success. Our rules of engagement are fairly straightforward, but if you haven't already done so, please take a few minutes to read our <?php e($html->link('Terms of Service',SITE_URL."fronts/terms_of_service",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>  and <?php e($html->link('Privacy Policy.',SITE_URL."fronts/privacy_policy",array('style'=>"color:#f23034;",'target'=>'blank'))); ?></p>
        
        <p>
		To activate your profile, copy & paste this link in your browser <br/>
        <span style="color: #F32C33;"><?php echo SITE_URL."clients/activate/{$data['User']['username']}/{$data['User']['activation_key']}"; ?></span><br/>
        <p>
        Your access credentials are: <br/>
        <br/>eMail: <?php echo $data['User']['username'];?><br />
        Password: <?php echo $data['User']['password2'];?><br />
        </p>
		<p>
		Thank you,<br>
		<b>Iqbal Ashraf</b><br>
		Executive Director<br>
		<?php e($html->image(SITE_URL.'img/email-logo.png',array('style'=>'padding-top:5px;','alt'=>'logo'))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>	
		</td>
	</tr>	
</table>