<?php
// during mentors_declined from admin  by  admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($data['Mentor']['first_name'])){
			echo "<b>".ucwords($data['Mentor']['first_name']).",</b>";
		}else{
			echo 'xxxx';
		}
		?></td>
	</tr>
	<tr>
		<td>
		<p>
		Invoice number <?php echo $data['Invoice']['inv_no']; ?> has been paid.<br/>
        </p>
        <p>
        <a href="<?php echo SITE_URL."invoice/invoice_view/".$data['Invoice']['id']; ?>" style="color:#f23034;">Review payment</a>
        </p>
       	<p>
        Please contact us if you have questions.<br/>
		</p>
		
		<p>
		<?php e($html->image(SITE_URL.'img/email-logo.png',array('alt'=>'logo','style'=>'padding-top:5px;'))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>
		</td>
	</tr>	
</table>