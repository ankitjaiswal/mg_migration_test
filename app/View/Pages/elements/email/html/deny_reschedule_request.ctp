<?php
// during mentors_declined from admin  by  admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($data['Mentee']['first_name'])){
			echo "<b>".ucwords($data['Mentee']['first_name'].' '.$data['Mentee']['last_name']).",</b>";
		}else{
			echo 'xxxx';
		}
		
		?></td>
	</tr>
	<tr>
		<td>
		<p>
		<?php echo ucwords($data['Mentor']['first_name'].' '.$data['Mentor']['last_name']); ?> cannot accommodate your request to reschedule.<br/>
        Please either confirm your session or use the log for further discussion.<br/>
        Contact us if you have questions.<br/>
		</p>
		
		<p>
		
		<?php e($html->image(SITE_URL.'img/email-logo.png',array('alt'=>'logo','style'=>'padding-top:5px;'))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>
		</td>
	</tr>	
</table>