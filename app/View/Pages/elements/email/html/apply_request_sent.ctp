<?php
// during registeration from admin as mentors by created by admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($menteeData['UserReference']['first_name'])){
			echo ucwords($menteeData['UserReference']['first_name']);
		}else{
			echo 'xxxx';
		}		
		?></td>
	</tr>
	<?php $displink=SITE_URL."users/my_account/".strtolower($this->Session->read('Auth.User.url_key')); ?>
	<tr>
		<td>
		<p>
			 We wanted to let you know that <?php e(ucwords($menteeData['Mentor']['first_name'])." ".ucwords($menteeData['Mentor']['last_name'])); ?> is now accepting mentorship applications. You may now <a href="<?php echo $displink; ?>">apply</a>.<br/>
			   Contact us if you have questions. </p>
				
		<p>
		<?php e($html->link($html->image(SITE_URL.'img/email-logo.png',array('style'=>'padding-top:5px;','alt'=>'logo')),SITE_URL,array('escape'=>false))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>		
		</td>
	</tr>	
</table>