<?php
// during mentors_declined from admin  by  admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($data['Mentor']['first_name'])){
			echo "<b>".ucwords($data['Mentor']['first_name'].' '.$data['Mentor']['last_name']).",</b>";
		}else{
			echo 'xxxx';
		}
		
		?></td>
	</tr>
	<tr>
		<td>
		<p>
		<?php echo ucwords($data['Mentee']['first_name'].' '.$data['Mentee']['last_name']); ?> has sent you a <a href="<?php echo SITE_URL."invoice/edit_invoice/".$data['Invoice']['id']; ?>">request to reschedule</a> the appointment.<br/>
        Contact us if you have questions.
		</p>
		
		<p>
		
		<?php e($html->image(SITE_URL.'img/email-logo.png',array('alt'=>'logo','style'=>'padding-top:5px;'))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>
		</td>
	</tr>	
</table>