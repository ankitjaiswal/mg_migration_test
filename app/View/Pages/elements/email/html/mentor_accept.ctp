<?php
// during registeration from admin as mentors by created by admin
?>
<table>
	<tr>
		<td>Dear
		<?php
		if(isset($data['Mentee']['first_name'])){
			echo ucwords($data['Mentee']['first_name']);
		}else{
			echo 'xxxx';
		}		
		?></td>
	</tr>
	<tr>
		<td>
		<p>
			 Congratulations! Your application to <?php echo ucwords($data['Mentor']['first_name'].' '.$data['Mentor']['last_name']); ?>  has been accepted. </p><br/>
		-<a href="<?php echo SITE_URL."invoice/order_confirm/".$data['Invoice']['id']; ?>">Confirm</a> your Initial consultation. Please also print the page and take it with you for the appointment.<br/>
        -Though you may <a href="<?php echo SITE_URL."invoice/order_confirm/".$data['Invoice']['id']; ?>">request to reschedule</a> , it is up to the Mentor to accept your request<br/>
        -A <a href="<?php echo SITE_URL."clients/private_log/".$data['Mentorship']['id']; ?>">log</a> has been created for you and <?php echo ucwords($data['Mentor']['first_name'].' '.$data['Mentor']['last_name']); ?> to track progress<br/><br/>
         Best of luck! Contact us if you have questions.
		<p>
		<?php e($html->link($html->image(SITE_URL.'img/email-logo.png',array('style'=>'padding-top:5px;','alt'=>'logo')),SITE_URL,array('escape'=>false))); ?>
		</p>
		<p>
		Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
		Phone : 808.729.5850<br>
		eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
		</p>		
		</td>
	</tr>	
</table>