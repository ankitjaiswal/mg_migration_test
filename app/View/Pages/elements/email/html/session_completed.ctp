<table>
    <tr>
        <td>Dear
        <?php
        if(isset($data['Mentee']['first_name'])){
            echo ucfirst(strtolower($data['Mentee']['first_name']));
        }else{
            echo 'xxxx';
        }       
        ?>,</td>
    </tr>
    <tr>
        <td>
<?php   if($data['Invoice']['title']=='Initial consultation'){ ?>
        <p>
        <?php echo ucfirst(strtolower($data['Mentor']['first_name']))." ".ucfirst(strtolower($data['Mentor']['last_name'])); ?> has marked your Initial consultation as complete. We hope it went well.</p>
        <p>Contact us if you have questions.</p>
<?php   }else{ ?>        
        <p>
        <?php echo ucfirst(strtolower($data['Mentor']['first_name']))." ".ucfirst(strtolower($data['Mentor']['last_name'])); ?> has marked your session as complete. We hope it went well.</p>
        <p>Please take a moment to provide us your <a href="<?php echo SITE_URL."clients/my_account"; ?>">feedback</a>. We will share this feedback with <?php echo ucfirst(strtolower($data['Mentor']['first_name'])); ?>.</p>
         <p>If you are in anyway dissatisfied with our service, please <a href="<?php echo SITE_URL."clients/my_account"; ?>">report</a> your issue and we will try our best to help you.</p>
<?php   } ?>        
        <p>
        <?php e($html->link($html->image(SITE_URL.'img/email-logo.png',array('style'=>'padding-top:5px;','alt'=>'logo')),SITE_URL,array('escape'=>false))); ?>
        </p>
        <p>
        Twitter :&nbsp;<?php e($html->link('@mentorsguild','http://www.twitter.com/@mentorsguild',array('style'=>"color:#f23034;",'target'=>'blank'))); ?><br>
        Phone : 808.729.5850<br>
        eMail :&nbsp;<?php e($html->link('help@mentorsguild.com',"mailto:help@mentorsguild.com",array('style'=>"color:#f23034;",'target'=>'blank'))); ?>
        </p>        
        </td>
    </tr>   
</table>