<style type="text/css" media="screen, tv, projection">
/*<![CDATA[*/

/* - - - ADxMenu: BASIC styles - - - */

/* remove all list stylings */
/*.menu, .menu ul {
	margin: 0;
	padding: 0;
	border: 0;
	list-style-type: none;
	display: block;
}*/
.menu, .menu ul li {
	background-image: url("<?=SITE_URL;?>img/nav_div.gif");
	background-position:left top;
	background-repeat:no-repeat;
    float: left;   
	display: block;
	list-style: none outside none;
	margin: 0;
	padding: 0;
	border: 0;
}

.submenu, .submenu ul li {
	background-color:#13488A;
    float: left;   
    padding-left: 0px !important;
	display: block;
	list-style: none outside none;
	margin: 0;
	/*padding: 0;*/
	border: 0;
	font-size:10px;
	padding-right:0px !important;
	width:150px !important;
	background-image:none !important;
}

/* move all list items into one row, by floating them */
.menu li {
	margin: 0;
	padding: 0;
	border: 0;
	display: block;
	float: left;
}

/* define new starting point for the nested UL, thus making positioning it a piece of cake */
.menu li:hover {
	position: relative;
}

/* force the submenu items into separate rows, while still keeping float:left (which resolves IE6 white-gap problem) */
.menu li li {
	width: 100%;
}

/* fix the position for 2nd level submenus.
	first make sure no horizontal scrollbars are visible on initial page load by sliding them all into top-left corner  */
.menu li li ul {
	top: 0;
	left: 0;
}

/* ...and then place the submenu where it should be when shown */
.menu li li:hover ul {
	left: 100%;
}

/* initialy hide all sub menus */
.menu ul {
	display: none;
	position: absolute;
	z-index: 10;
}

/* display them on hover */
.menu li:hover>ul {
	display: block;
}

/* -- float.clear -- force containment of floated LIs inside of UL */
.menu:after, .menu ul:after {
	content: ".";
	height: 0;
	display: block;
	visibility: hidden;
	overflow: hidden;
	clear: both;
}
/* IE7 float clear: */
.menu, .menu ul {
	min-height: 0;
}
/* -- float.clear.END --  */

/* sticky submenu: it should not disappear when your mouse moves a bit outside the submenu
	YOU SHOULD NOT STYLE the background of the ".menu UL" or this feature may not work properly!
	if you do it, make sure you 110% know what you do */
.menu ul {
	/*background-image: url(empty.gif);*//* required for sticky to work in IE6 and IE7 - due to their (different) hover bugs */
	padding: 10px 10px 10px 10px;
	/*margin: -10px 0 0 -30px;*/
	/*background: #f00;*/		/* uncomment this if you want to see the "safe" area. you can also use to adjust the safe area to your requirement */
}


/* - - - ADxMenu: DESIGN styles - - - */

.menu, .menu ul li {
	color: #eee;
	/*background: #354D9E;*/
}

.menu ul {
	width: 11em;
}

/*.menu li:hover>a {
	color: #fc3;
}

.menu a {
	text-decoration: none;
	color: #eee;
	padding: .4em 1em;
	display: block;
}

.menu a:hover {
	color: #fc3;
}*/

/* Fix for IE5/Mac \*//*/
.menu a {
	float: left;
}
.menu {
	display: inline-block;
}
/* End Fix */

/*]]>*/
</style>
<!--[if lte IE 6]>
<style type="text/css" media="screen, tv, projection">
/*<![CDATA[*/

/* - - - ADxMenu: BASIC styles - - - */
.menu, .menu ul {
	height: 1%;
}

.menu ul {
	display: block;
	visibility: hidden;
}

/* this style must be exact copy of: ".menu li li:hover ul" style above  */
.menu li li.adxmhover ul {
	left: 100%;
}

/* the following two rules simulate li:hover>ul selector
	repeat enough times to cover all nested levels
	look at www.aplus.co.yu/adxmenu/trouble/ if some of your submenus do now show up */
.adxmhover ul,
.adxmhover .adxmhover ul {
	visibility: visible;
}
.adxmhover li ul,
.adxmhover .adxmhover li ul {
	visibility: hidden;
}

/* - - - ADxMenu: DESIGN styles - - - */
.menu ul a {
	height: 1%;
}

.adxmhover a,
.adxmhover .adxmhover a {
	color: #fc3;
}
.adxmhover li a,
.adxmhover .adxmhover li a {
	color: #eee;
}

/*]]>*/
</style>

<script type="text/javascript" src="../code/ADxMenu.v4.02.js"></script>
<![endif]-->
<strong>
<? echo $this->Html->link($this->Html->image('spacer.gif', array('width'=>'340','height'=>'64','border'=>'0')),SITE_URL,array('escape' => false)); ?>	
</strong>
<div class="smlinks"> 	
	<?php 
	
		
	if($session->read('Auth.User.id'))
	{
	?>
		<div class="welcome_msg">Welcome <?php e($session->read('Auth.User.screen_name')); ?></div><br />
		<?
		
		//echo '<pre>';print_r($_SESSION['user_arr']['User']);echo '</pre>';
		if($session->read('Auth.User.screen_name') != EXPERT)
		{
		?>
		<?php echo $this->Html->link(__('Join as an Expert', true), '/users/expert_register'); ?> | 
		<?
		}
		?>
		<?php echo $this->Html->link(__('Sign Out', true), '/users/logout'); ?> | 
	<?
	}else{	?>
		<?php echo $this->Html->link(__('Sign In', true), '/users/login'); ?> | 
		<?php echo $this->Html->link(__('Sign Up', true), '/users/register'); ?> | 
	<?	
	}
	?>
	<?php echo $this->Html->link(__('Support', true), '/static/pagename/support'); ?> | 
	<?php echo $this->Html->link(__('Contact us', true), '/static/pagename/contactus'); ?>
</div>
<div class="top_nav">
	<ul class="adxm menu">
    <li><a href="javascript:;" title="Products">Products</a>
      <ul class="submenu">
        <li><a href="javascript:;"><!--Product -->Overview</a></li>
        <li><a href="javascript:;">LP Chat</a></li>
        <li><a href="javascript:;">LP Voice</a></li>
		<li><a href="javascript:;">LP Marketer</a></li>
      </ul>
    </li>
	<li><a href="javascript:;" title="Business Solutions">Business Solutions</a>
      <ul class="submenu">
        <li><a href="javascript:;"><!--Business -->Overview</a></li>
        <li><a href="javascript:;">Sales<!-- Solutions--></a></li>
        <li><a href="javascript:;">Service<!-- Solutions--></a></li>
		<li><a href="javascript:;">Customers</a></li>
      </ul>
    </li>
	<li><a href="javascript:;" title="Live Engage Plateform">Live Engage</a>
      <ul class="submenu">
        <li><a href="javascript:;"><!--Live Engage -->Overview</a></li>
       <!-- <li><a href="javascript:;">Plateform APIs</a></li>
        <li><a href="javascript:;">Apps Marketplace</a></li>-->
      </ul>
    </li>
	<li><a href="javascript:;" title="Community">Community</a>
      <ul class="submenu">
        <li><a href="javascript:;">Overview</a></li>
        <li><a href="javascript:;">Customer<!-- Community--></a></li>
        <li><a href="javascript:;">Developer<!-- Community--></a></li>
		<li><a href="javascript:;">Expert<!-- Community--></a></li>
      </ul>
    </li>
	<li><a href="javascript:;" title="Company">Company</a>
      <ul class="submenu">
        <li><a href="javascript:;">AboutUs</a></li>
        <li><a href="javascript:;">News<!-- & Events--></a></li>
        <li><a href="javascript:;">Investor<!-- Relations--></a></li>
		<li><a href="javascript:;">Partners</a></li>
		<li><a href="javascript:;">Careers</a></li>
		<li><a href="javascript:;">Contact Us</a></li>
      </ul>
    </li>
	<li><a href="javascript:;" title="Expert Advice">Expert Advice</a>
      <ul class="submenu">
        <li><a href="javascript:;">Overview</a></li>
        <li><a href="javascript:;">Technology</a></li>
        <li><a href="javascript:;">Counselling</a></li>
		<li><a href="javascript:;">Education</a></li>
		<li><a href="javascript:;">Health</a></li>
		<li><a href="javascript:;">Business</a></li>
		<li><a href="javascript:;">Arts</a></li>
		<li><a href="javascript:;">Spirituality</a></li>
      </ul>
    </li>
	<?
	if(isset($_SESSION['user_arr']) && !empty($_SESSION['user_arr']))
	{
	?>
	<li><?php echo $this->Html->link(__('My Account', true), '/users/myaccount',array('class'=>$this->params['controller']=='users'?'active':'')); ?>
      <ul class="submenu">
        <li><a href="javascript:;">Mailbox</a></li>
        <li><?php echo $this->Html->link(__('Profile', true), '/users/myprofile'); ?></li>
        <li><a href="javascript:;">Earnings</a></li>
		<li><a href="javascript:;">Payments</a></li>
		<li><a href="javascript:;">History</a></li>
		<!--<li><a href="javascript:;">My Clients</a></li>
		<li><a href="javascript:;">Account Details</a></li>
		<li><a href="javascript:;">Client account activity</a></li>
		<li><a href="javascript:;">My Experts</a></li>
		<li><a href="javascript:;">Expert Forums</a></li>
		<li><a href="javascript:;">Answer Posted Requests</a></li>
		<li><a href="javascript:;">Email Notifications</a></li>
		<li><a href="javascript:;">Download Software</a></li>-->
      </ul>
    </li>
	<?
	}
	?>
    <!--
	<li><a href="http://www.aplus.co.yu/adxmenu/" title="Nested fly-out menu, standard-compliant">Business Solutions</a>
      <ul>
        <li><a href="http://www.aplus.co.yu/adxmenu/">Overview</a></li>
        <li><a href="http://www.aplus.co.yu/adxmenu/instructions/">Instructions</a></li>
        <li><a href="http://www.aplus.co.yu/adxmenu/examples/">Examples</a>
          <ul>
            <li><a href="http://www.aplus.co.yu/adxmenu/examples/htb/">Top to bottom</a></li>
            <li><a href="http://www.aplus.co.yu/adxmenu/examples/hbt/">Bottom to top</a></li>
            <li><a href="http://www.aplus.co.yu/adxmenu/examples/vlr/">Left to right</a></li>
            <li><a href="http://www.aplus.co.yu/adxmenu/examples/vrl/">Right to left</a></li>
          </ul>
        </li>
        <li><a href="http://www.aplus.co.yu/adxmenu/trouble/">Troubleshooting</a></li>
      </ul>
    </li>
    <li><a href="http://www.aplus.co.yu/wch/" title="Windowed Controls Hider, for Win IE">WCH</a>
      <ul>
        <li><a href="http://www.aplus.co.yu/wch/">Overview</a></li>
        <li><a href="http://www.aplus.co.yu/wch/instructions/">Instructions</a></li>
        <li><a href="http://www.aplus.co.yu/wch/examples/">Examples</a></li>
        <li><a href="http://www.aplus.co.yu/wch/trouble/">Troubleshooting</a></li>
      </ul>
    </li>
    <li><a href="http://www.aplus.co.yu/lab/" title="Reusable web techniques">Lab</a>
      <ul>
        <li><a href="http://www.aplus.co.yu/css/z-pos">z-index tutorial</a></li>
        <li><a href="http://www.aplus.co.yu/css/forms/">Styling forms</a></li>
        <li><a href="http://www.aplus.co.yu/css/cfl/">Centered frame layout</a></li>
        <li><a href="http://www.aplus.co.yu/css/tabs2/">Tabs with variable height</a></li>
        <li><a href="http://www.aplus.co.yu/css/nestedtabs2/">2-level navigation</a></li>
        <li><a href="http://www.aplus.co.yu/css/ow/">Tabs: Overlapping Windows</a></li>
        <li><a href="http://www.aplus.co.yu/scripts/windowopen/">Unobtrusive window.open</a></li>
        <li><a href="http://www.aplus.co.yu/scripts/fif/">Floating iFrame</a></li>
      </ul>
    </li>
    <li><a href="http://www.aplus.co.yu/deliver/" title="Various sites I (co-)did">Delivered</a>
      <ul>
        <li><a href="http://www.aplus.co.yu/deliver/sites/">Sites &amp; proof of concepts</a></li>
        <li><a href="http://www.aplus.co.yu/deliver/wp/">WordPress goodies</a></li>
      </ul>
    </li>
    <li><a href="http://www.aplus.co.yu/about/" title="Relevant info about me">Colophon</a></li>
    <li><a href="http://www.aplus.co.yu/about/contact/">Contact me</a></li>-->
  </ul>
  <!--<ul>
    <li><?php echo $this->Html->link(__('Products', true), 'javascript:;'); ?></li>
    <li><?php echo $this->Html->link(__('Business Solutions', true), 'javascript:;'); ?></li>
    <li><?php echo $this->Html->link(__('Developers', true), 'javascript:;'); ?></li>
    <li><?php echo $this->Html->link(__('Community', true), 'javascript:;'); ?></li>
    <li><?php echo $this->Html->link(__('Company', true), 'javascript:;'); ?></li>
    <li><?php echo $this->Html->link(__('Expert Advice', true), 'javascript:;'); ?></li>
  </ul>-->
</div>