<table border="0" class="Admin2Table" width="100%">
	<tr class="AddProFrm">
		<td colspan="2"><h3>Account Information</h3></td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">Email as username <span class="input_required">*</span></td>
		<td><?php e($form->input('User.username', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
	</tr>
	<?php if(0){?>
	<tr>
		<td valign="middle" class="Padleft26">Password <span class="input_required">*</span></td>
		<td><?php e($form->input('User.password2', array("type" => "password", 'div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">Confirm Password <span class="input_required">*</span></td>
		<td><?php e($form->input('User.confirm_password', array("type" => "password", 'div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
	</tr>
	<?php } ?>
	<tr class="AddProFrm">
		<td colspan="2"><h3>Contact & Personal Information</h3></td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">First Name <span class="input_required">*</span></td>
		<td><?php e($form->input('UserReference.first_name', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>
		</td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">Last  Name <span class="input_required">*</span></td>
		<td><?php e($form->input('UserReference.last_name', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>
		</td>
	</tr>	
	<tr>
		<td valign="middle" class="Padleft26">Professional headline<span class="input_required">*</span></td>
		<td>
			<?php  e($form->input('UserReference.headline', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>		
		</td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">ZIP / Postal code<span class="input_required">*</span> </td>
		<td>
		<?php  e($form->input('UserReference.zipcode', array('id'=>'keyword','div'=>false, 'label'=>false, "class" => "Testbox5")));?>		
		<?php e($html->image('loading.gif',array('id'=>'loading')));?>
		<div id="ajax_response"></div>
		</td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">Background Summary<span class="input_required">*</span></td>
		<td>
			<?php  e($form->input('UserReference.background_summary', array('div'=>false, 'label'=>false, "class" => "Testbox")));?>		
		</td>
	</tr>	
	<tr>
		 <td valign="middle" class="Padleft26">Status</td>
		 <td>
		 <?php e($form->input('User.status', array('options'=>Configure::read('Status'),'div'=>false, 'label'=>false, "class" => "TextBox5")));?>
		 </td>
        </tr>
       
	<tr>
          <td valign="middle" class="Padleft26">Plan type</td>
       <td>
               <?php $options = array('Basic'=>'Basic','Pro'=>'Pro','Enterprise'=>'Enterprise'); ?>
		 <?php e($form->input('User.plan_type', array('options'=>$options,'div'=>false, 'label'=>false, "class" => "TextBox5")));?>
		 </td>
     </tr>
     <tr>
          <td valign="middle" class="Padleft26">Client type</td>
       <td>
               <?php $options = array('1'=>'Admin','0'=>'Client under plan'); ?>
		 <?php e($form->input('User.isPlanAdmin', array('options'=>$options,'div'=>false, 'label'=>false, "class" => "TextBox5")));?>
		 </td>
     </tr>
	<tr class="AddProFrm">
		<td colspan="2"><h3>Upload Profile Image</h3></td>
	</tr>	
	<?php 
	if($this->params['action'] == 'admin_edit' && isset($this->data['UserImage'][0]['image_name'])){?>
		<tr>		
			<td><?php e($form->file('UserImage1.image_name')); ?></td>
			<td>
			<?php 			
			e($html->image(MENTEES_IMAGE_PATH.DS.$this->data['User']['id'].DS.$this->data['UserImage'][0]['image_name'],array('alt'=>'profile image'))); 
			?>
			</td>			
		</tr>	
		<?php 
	}else{?>
		<tr>		
			<td colspan="2"><?php e($form->file('UserImage1.image_name')); ?></td>
		
		</tr>			
		<?php 
	}?>
	<?php
	if($this->params['action'] == 'admin_edit' && isset($this->data['Resume'][0]['id'])){?>	
		<?php e($form->hidden('Resume1.0.id',array('value'=>$this->data['Resume'][0]['id']))); ?>
		<?php
	}?>
	
	<tr class="AddProFrm">
		<td colspan="2"><h3>Upload Resume</h3></td>
	</tr>	
	<tr>		
		<td colspan="2"><?php e($form->file('Resume1.resume_location')); ?></td>			
	</tr>	
	
</table>
