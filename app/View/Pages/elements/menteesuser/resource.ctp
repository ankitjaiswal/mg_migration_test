<?php
$count = 0;
$resourceData = array();
if(isset($this->data['Source'])){	
	foreach($this->data['Source'] as $key=>$value){		
		if($value['resource_title'] !='' || $value['resource_location'] !=''){
			$count++;
			$resourceData[] = array('title'=>$value['resource_title'],'location'=>$value['resource_location']);			
		}		
	}	
}
if(!$count){
	$count = 1;
	$resourceData[] = array('title'=>'','location'=>'');	
}
?>
<tr class="AddProFrm">
	<td colspan="2"><h3>Add Resources</h3></td>
</tr>

<tr>	
	<td colspan="2">
	<div id="p_resources">
		<?php
		//echo count($this->data['Source']);		
		for($key=0;$key<$count;$key++){?>
			<p>
			<label><input type="text" id="Source<?php e($key); ?>ResourceTitle" size="30" name="data[Source][<?php e($key); ?>][resource_title]" value="<?php e($resourceData[$key]['title']); ?>" placeholder="Title" /></label>
			<label><input type="text" id="Source<?php e($key); ?>ResourceLocation" size="30" name="data[Source][<?php e($key); ?>][resource_location]" value="<?php e($resourceData[$key]['location']); ?>" placeholder="Addlink or Upload file" /></label>
		<?php
			if($key ==($count-1)){				
				?>
				<span id="addResource" class="addmore">[ + ]</span>
				<?php
			}else{?>
				<span class="remResource addmore">&nbsp;[ - ]</span>						
				<?php
			}?>
			<label><?php e($form->file('Source.'.$key.'.resource_location_file',array('div'=>false,'label'=>false)));?></label>
			</p>
			<?php 		
			
		}?>		
	</div>		
	</td>					
</tr>

<script>
jQuery(document).ready(function(){

	jQuery(function() {
			var scntDiv = jQuery('#p_resources');
			var i = jQuery('#p_resources p').size() + 1;
			
			jQuery('#addResource').live('click', function() {
					var title = '<label>';
						title +='<input type="text" id="Source'+i+'ResourceTitle" size="30" name="data[Source]['+i+'][resource_title]" value="" placeholder="Title" />';
						title +='</label>';
					var location = '<label>';
						location +='&nbsp;<input type="text" id="Source'+i+'ResourceLocation" size="30" name="data[Source]['+i+'][resource_location]" value="" placeholder="Addlink or Upload file" />';
						location +='</label>';
						
					var remove = '<span  class="remResource addmore">&nbsp;[ - ]</span>';

					var  resourcefile= '<label>';
					resourcefile +='&nbsp;<input type="file" id="Source'+i+'resource_location_file" name="data[Source]['+i+'][resource_location_file]" />';
					resourcefile +='</label>';
					
					jQuery('<p>'+title+location+remove+resourcefile+'</p>').appendTo(scntDiv);
					i++;
					return false;
			});			
			jQuery('.remResource').live('click', function() { 
					if( i > 2 ) {
							jQuery(this).parents('p').remove();
							i--;
					}
					return false;
			});
	});

});
</script>