	<div class="adminrightinner">
		<?php echo($this->Form->create($modelName, array('url' => array('controller' => $controllerName, 'action' => 'edit'))));?>
		<?php echo($this->Form->input('id'));?>
		<div class="tablewapper2 AdminForm"><?php echo($this->Element('Setting/admin_edit'));?></div>
		<div class="buttonwapper">
			<div><input type="submit" value="Submit" class="submit_button" /></div>
			<div class="cancel_button"><?php echo $this->Html->link("Cancel", "/admin/settings/index/", array("title"=>"", "escape"=>false)); ?></div>
		</div>
		<?php  echo($this->Form->end());	?>	
	</div>

