	<div class="SearchRight">
		<?php echo($this->Form->create(Setting, array('url'=>array('controller' => $controllerName, 'action' => 'admin_searchvariables'))));?>
		<div class="input text"><label>Search by Setting</label> <?php echo($this->Form->input('name', array('label' => false, 'div'=>false,'class'=>'InputBox'))); ?> <?php echo($this->Form->submit('Search', array('div'=>false)));?></div>
		<div class="SearchRightAction"></div>
		<?php echo($this->Form->end());?>
	</div>
	<div class="adminrightinner">
		<div class="tablewapper2">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">
				<tr class="head">
					<td width="28%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:19px;">Setting Name</td>					
					<td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:19px;">Value</td>
					<td align="center" valign="middle" class="Bdrrightbot Padtopbot6">Action</td>
				</tr>
				<?php 
				foreach($data as $value){?>					
					<tr>
						<td align="left" valign="middle" class="Bdrrightbot" style="padding-left:19px;"><strong><?php echo($value['Setting']['name']);?></strong></td>
						<td align="left" valign="middle" class="Bdrrightbot" style="padding-left:19px;"><?php echo($value['Setting']['value']);?></td>
						<td align="center" valign="middle" class="Bdrbot ActionIcon"><?php echo($this->Admin->getActionImage(array('edit'=>array('controller'=>$controllerName, 'action'=>'admin_searchvariableedit')), $value['Setting']['id'])); ?></td>
					</tr>
					<?php
				}?>
			</table>
		</div>		
	</div>
	<div class="clr"></div>
