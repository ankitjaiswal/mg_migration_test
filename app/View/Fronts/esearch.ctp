<script type="text/javascript">
mixpanel.track("Browse Search", {"URL": document.URL});
</script>

<?php header("Cache-control: public"); 

$cachefile = WWW_ROOT."cache/cache-".$cache_industry."-".$cache_subindustry."-".$cache_keyword."-".$cache_adCity."-".$cache_adstate.".html";

if (file_exists($cachefile)) {
	include($cachefile); 
} else {
	ob_start();
	
?>

<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
      <div id="financial-management">
       
           <p>
		<span><?php 
		$memCount = count($data);
		$dirCount = count($dUsersFinal);
              $totCount = $memCount + $dirCount;
              $key = "experts";
              $i ='0'; 
              if((strpos($adCity, " ")!== false))
              {
               $city = explode(" ", $adCity); // splitting the city
               $i ='1';
              }
	      if($keyword == '' && $adSubIndustry == 'all categories' && $adIndustry != 'all industries'){
               
               echo ( ucfirst($key).' in '.ucfirst($adIndustry));
		}

	      elseif($keyword == '' && $adSubIndustry != 'all categories'){
               
               echo ( ucfirst($key).' in '.ucfirst($adSubIndustry));
		}

              else if($adSubIndustry != '' && $adSubIndustry != 'all categories'){
             
		echo ( ucfirst($keyword).' experts in '.ucfirst($adSubIndustry));
		}

              else if($adIndustry == 'all industries' && $adSubIndustry == 'all categories' && $keyword != ''){

               echo ( ucfirst($key).' in '.ucfirst($keyword));

              }
              else if($adIndustry == 'all industries' && $adSubIndustry == 'all categories' && $keyword == ''){

               echo ( 'Nearby '.($key));
              }

              else{

               echo ( ucfirst($keyword).' in '.ucfirst($adIndustry));
              }
		?></span> 
		<span style="float: right; font-weight: normal;"> <?php echo($totCount." experts");?></span>
		</p>
      </div>
       
      <div id="mentors-content">
		<?php
			echo($this->Element('Front/search'));
		?>
      </div>
    </div>
  </div> 
<?php 
if(empty($data)){?>
 <script type="text/javascript">
	jQuery(window).bind('load resize',function(){
		if(jQuery("body").height() < jQuery(window).height()){
			jQuery("#footer-wrapper").css({'position':'fixed'})
		}
		
	});
 </script>
 <?php
}?>
<?php
	$fp = fopen($cachefile, 'w'); 
	
// save the contents of output buffer to the file
fwrite($fp, ob_get_contents()); 
// close the file
fclose($fp); 
// Send the output to the browser
ob_end_clean();

include($cachefile); 

}?>
