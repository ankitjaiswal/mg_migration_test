<div id="inner-content-wrapper" class="comman-page-boby consult-page">
    <div id="inner-content-box" class="pagewidth">
        <div class="container">
            <div class="inner_wrapper">
                <div class="underline">
                    <h1>Request a free initial consultation with <?php echo($dUSer['DirectoryUser']['first_name']." ".$dUSer['DirectoryUser']['last_name']);?></h1>
                </div>
                <div class="">
                    <?php
                    echo $this->Form->create(null,array('url'=>array('controller'=>'fronts','action'=>'consult'),'id'=>'mentorshipForm'));
                    ?>
                    <input type="hidden" name="data[Directory_user][id]" value="<?php echo($dUSer['DirectoryUser']['id']);?>" id="hidId"/>
                    <div>
                        <p>
                            <label>What do you need solved? <span class="characters-text">(<span id="noOfChar">4000</span>  characters remaining)</span></label>
                            <br />
                            <?php echo($this->Form->textarea('Mentorship.mentee_need',array('rows'=>10,'div'=>false,'label'=>false,'class'=>"textarea big",'style'=>'', 'id'=>'questionTextBox'))); ?>
                        </p>
                    </div>
                    <?php if($this->Session->read('Auth.User.id') == '') {?>
                    <div id="oldButtonDiv">
                        <span style="float:left;padding:11px 18px;"> <a href ="javascript:void(0);" onclick="window.parent.TINY.box.hide();">Cancel</a></span>
                        <input id="openButton" type="button" value="Send request" class="mgButton">
                    </div>
                    <?php echo($this->Form->end()); ?>
                    <div style="display: none;" id="regDivText">
                        <span><img src="<?php echo(SITE_URL);?>img/media/arrow.png"></img> To continue, Register or Submit as a guest</span>
                    </div>
                    <div id="linkedinDiv" class="pagewidth" style="display: none;">
                        <div class="linkedinDiv-left">
                            <div>
                                <div class="submitProfile">
                                    
                                    <?php echo($this->Html->image('linked-in.png',array('alt'=>'Linked in','style'=>'cursor:pointer;'))); ?>
                                </div>
                            </div>
                            <div class="text">GUILD will <span style="font-weight: bold;">never </span>post to your LinkedIn account without your permission.</div>
                        </div>
                        <div class="vrtLINE"></div>
                        <div class="bottom-form-right">
                            <div>
                                <?php
                                    echo $this->Form->create(null,array('url'=>array('controller'=>'fronts','action'=>'visitor_consultation_request',$dUSer['DirectoryUser']['id']),'id'=>'newmentorshipForm'));
                                ?>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td class="label-text">Name</td>
                                            <td>
                                                <input name="data[Mentorship][firstname]" type="text" class="forminput half-input" id="userfname" value="" maxlength="120" placeholder="First">
                                                <input name="data[Mentorship][lastname]" type="text" class="forminput half-input" id="userlname" value="" maxlength="120" placeholder="Last">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label-text">Work email</td>
                                            <td>
                                                <input name="data[Mentorship][email]" type="text" class="forminput full-input" placeholder = "email@yourcompany.com" id="useremail" value="" maxlength="250">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label-text">Phone</td>
                                            <td>
                                                <input name="data[Mentorship][phone]" type="text" class="forminput full-input" placeholder = "(xxx) xxx-xxxx" id="userphone" value="" maxlength="250">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                                <input name="data[Mentorship][mentee_need]" type="hidden" value="" id="myanswer">
                                
                            </div>
                            <div style="float: right;" >
                                <span style="float:left;padding:11px 18px;">
                                    <a href ="javascript:void(0);" onclick="window.parent.TINY.box.hide();">Cancel</a>
                                </span>
                                <input id="sendButton" type="submit" value="Send request" class="mgButton" style="float: right;">
                            </div>
                        </form>
                    </div>
                </div>
                <?php } else {?>
                <?php  echo($this->Form->submit('Send request',array('class'=>'mentorshipBtn', 'style'=>'float:right; margin-right: 11px;')));?>
                <span style="float:right;padding:11px 18px;">
                    <a href ="javascript:void(0);" onclick="window.parent.TINY.box.hide();">Cancel</a>
                </span>
                <?php }?>
                <?php echo($this->Form->end()); ?>
            </div>
        </div>
    </div>
    
</div>
</div>
<script type="text/javascript">
jQuery(document).ready(function(){

function updateCount ()
{
var qText = jQuery("#questionTextBox").val();
if(qText.length < 4000) {
jQuery("#noOfChar").html(4000 - qText.length);
} else {
jQuery("#noOfChar").html(0);
jQuery("#questionTextBox").val(qText.substring(0,4000));
}
}
jQuery("#questionTextBox").keyup(function () {
updateCount();
});
jQuery("#questionTextBox").keypress(function () {
updateCount();
});
jQuery("#questionTextBox").keydown(function () {
updateCount();
});
        jQuery("#openButton").click(function() {
            document.getElementById('linkedinDiv').style.display = 'inline-block';
            document.getElementById('oldButtonDiv').style.display = 'none';
            document.getElementById('regDivText').style.display = 'block';
            jQuery('html, body').animate({
        scrollTop: jQuery("#regDivText").offset().top
        }, 1000);
        });
            jQuery("#sendButton").click(function() {
var flag = 0;
                
        var answer = jQuery.trim(jQuery("#questionTextBox").val());
        if(jQuery.trim(jQuery("#questionTextBox").val()) == '') {
jQuery('#questionTextBox').css('border-color','#F00');
jQuery('#questionTextBox').focus();
flag++;
}else {
jQuery('#questionTextBox').css('border-color','');
}
        if(jQuery.trim(jQuery("#userfname").val()) == '') {
jQuery('#userfname').css('border-color','#F00');
jQuery('#userfname').focus();
flag++;
}else {
jQuery('#userfname').css('border-color','');
}
        if(jQuery.trim(jQuery("#userlname").val()) == '') {
jQuery('#userlname').css('border-color','#F00');
jQuery('#userlname').focus();
flag++;
}else {
jQuery('#userlname').css('border-color','');
}
        if(jQuery.trim(jQuery("#useremail").val()) == '') {
jQuery('#useremail').css('border-color','#F00');
jQuery('#useremail').focus();
flag++;
}else {
jQuery('#useremail').css('border-color','');
}
        if(jQuery.trim(jQuery("#userphone").val()) == '') {
jQuery('#userphone').css('border-color','#F00');
jQuery('#userphone').focus();
flag++;
}else {
jQuery('#userphone').css('border-color','');
}
            if(flag > 0) {
                return false;
            } else {
                jQuery("#myanswer").val(answer);
                jQuery("#newmentorshipForm").submit();
}
        });
jQuery(".mentorshipBtn").click(function(){
    var flag = 0;
    if(jQuery.trim(jQuery("#questionTextBox").val()) == '') {
            jQuery('#questionTextBox').css('border-color','#F00');
            jQuery('#questionTextBox').focus();
            flag++;
        } else {
            jQuery('#questionTextBox').css('border-color','');
        }
        if(flag > 0) {
            return false;
        } else {
            jQuery('#mentorshipForm').submit();
        }
});

jQuery(".submitProfile").click(function(){
    var flag = 0;
    if(jQuery.trim(jQuery("#questionTextBox").val()) == '') {
            jQuery('#questionTextBox').css('border-color','#F00');
            jQuery('#questionTextBox').focus();
            flag++;
        } else {
            jQuery('#questionTextBox').css('border-color','');
        }
        if(flag > 0) {
            return false;
        } else {
            var hidId = jQuery('#hidId').val();
            var message = jQuery.trim(jQuery("#questionTextBox").val());
        jQuery.ajax({
                    url:SITE_URL+'fronts/consult',
                    type:'post',
                    dataType:'json',
                    data: 'message='+ message + '&hidId=' + hidId,
                    success:function(id){
                        window.location.href = SITE_URL+'linkedin/signuplinkclientpopup.php?directoryConsultationId='+id;
                    }
                    });
            return false;
            
        }
        
});
});
</script>