<div class="container create-section pad0">
	<div style="margin-top:130px;margin-bottom:150px;">
		<div class="progresbar" style="margin: 0 0 30px;">
			<div class="col-sm-8 col-sm-offset-2">
				<ul>
					<li class="active">
						<span>1</span>
						<div class="title">
							<p>Requirement submitted</p>
						</div>
					</li>
					<li>
						<span>2</span>
						<div class="title">
							<p>Project approved</p>
						</div>
					</li>
					<li>
						<span>3</span>
						<div class="title">
							<p>Proposal accepted</p>
						</div>
					</li>
					<li>
						<span>4</span>
						<div class="title">
							<p>Project started</p>
						</div>
					</li>
					<li>
						<span>5</span>
						<div class="title">
							<p>Project completed</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
                <?php 
                  $url = "https://calendly.com/guild/guild-client-partner-call?name=".$fname."%20".$lname."&email=".$email;
                  
                ?>
		<center><h2 class="project-heading">Schedule Client Partner Call</h2></center>
		<!-- Calendly inline widget begin -->
		<div class="calendly-inline-widget" data-url="<?php echo($url);?>" style="min-width:320px;height:580px;"></div>

		<!-- Calendly inline widget end -->
	</div>






	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>

</div>