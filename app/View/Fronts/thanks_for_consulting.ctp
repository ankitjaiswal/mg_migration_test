<div class="container create-section pad0">
	<div style="margin-top:130px;margin-bottom:150px;">
		<div class="progresbar" style="margin: 0 0 30px;">
			<div class="col-sm-8 col-sm-offset-2">
				<ul>
					<li class="active">
						<span>1</span>
						<div class="title">
							<p>Project Created</p>
						</div>
					</li>
					<li>
						<span>2</span>
						<div class="title">
							<p>Project approved</p>
						</div>
					</li>
					<li>
						<span>3</span>
						<div class="title">
							<p>Proposal accepted</p>
						</div>
					</li>
					<li>
						<span>4</span>
						<div class="title">
							<p>Project started</p>
						</div>
					</li>
					<li>
						<span>5</span>
						<div class="title">
							<p>Project completed</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
               
		<center><h2 class="project-heading">Thanks! Your Client Partner will soon send you an agenda for the call. We look forward to talking to you.</h2>
             </br></br>

           <?php  $for_creator = ClassRegistry::init('User'); 	
                      $creator = $for_creator->find('first', array('conditions' => array('User.id' => $this->Session->read('Auth.User.id'))));       	  
                      if($creator['UserReference']['zipcode'] == '' ||  $creator['UserReference']['headline'] = ''){?>			
			

								<button class="btn btn-default get-started" onclick="window.location.href='<?php echo (SITE_URL."clients/my_account"); ?>';">
									Complete Your GUILD Profile
								</button>
              <?php }else{?>

								<button class="btn btn-default get-started" onclick="window.location.href='<?php echo (SITE_URL."clients/my_account"); ?>';">
									Done
								</button>

             <?php }?>
            </center>

	</div>
	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/script6.js"></script>

</div>

