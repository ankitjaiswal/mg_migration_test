<div id="inner-content-wrapper" class="comman-page-boby claim-profile-page">
	<div id="inner-content-box" class="pagewidth">
		<div class="container">
			<div class="inner_wrapper">
				<h2>Claim your free GUILD profile</h2>
				<?php
					echo($this->Form->create('Client',array('url'=>array('controller'=>'fronts','action'=>'claim_profile'),'id'=>'feedbackForm')));
					echo $this->Form->hidden('DirectoryUser.id',array('value'=>$dUSer['DirectoryUser']['id'], 'id'=>'hidId'));
				?>
				<div class="feedbackcontent">
					<div class="form_row">
						<ul>
							<li>- Enhance your online presence</li>
							<li>- Add profile details, case studies</li>
							<li>- Answer questions from potential clients</li>
							<li>- Leverage our sales engine and admin support</li>
						</ul>
					</div>
					<div  class="styled-form">
						<input type="checkbox" class="group1" name="catCheck[]" id="an"  placeholder=""><label for="an"><span> </input>
						I confirm that I am <span style="font-weight:bold;"><?php echo($dUSer['DirectoryUser']['first_name']." ".$dUSer['DirectoryUser']['last_name'])?></span>, and I accept GUILD's <a href="<?php echo(SITE_URL.'fronts/terms');?>" target="_blank;"> Terms of Service</a> and <a href="<?php echo(SITE_URL.'fronts/privacy');?>" target="_blank;"> Privacy policy</a></label>
					</div>
				</div>
				<?php if($this->Session->read('Auth.User.id') != '') {?>
				<div id="Apply">
					<input id="apply" class="submitProfile1 LTSpreview" type="submit" value="Submit" >
				</div>
				<?php } else {?>
				<div class="submitprofile-full">
					<div class="pagewidth">
						<div class="submitProfile" style="cursor:pointer;">
							<?php echo($this->Html->image('linked-in.png',array('alt'=>'Linked in','style'=>'cursor:pointer; float: right;'))); ?>
							<div class="submitprofile-box">
								<span>To claim your profile </span>
								<img class="down-a" src="<?php echo(SITE_URL);?>img/media/arrow.png"></img>
							</div>
						</div>
					</div>
					<div class="text-info">GUILD will use LinkedIn to validate your identity. GUILD will <b style="font-weight: bolder; font-family: Ubuntu,arial,vardana;">never</b> post to your LinkedIn account without your permission.</div>
				</div>
				<?php }?>
				<?php
					echo($this->Form->end());
				?>
			</div>
		</div>
	</div>
</div>

	<div class="modal fade claimmodal member-edit-popup" id="claimmodal" tabindex="-1" role="dialog" aria-labelledby="claimmodal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<h3 class="modal-title" id="myModalLabel">Confirm this</h3>
				</div>
				<form id="frmedit-profile-m">
					<div class="modal-body edit-profile-m">
						<div class="col-sm-12 form-group">
							<p>Please Check the box to accept the GUILD Terms of service and Privacy Policy</p>
						</div>						

					</div>
				</form>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>




<script type="text/javascript">
jQuery(document).ready(function(){
function OpenCheckboxAlert(){
                             jQuery("#claimmodal").modal();
                        
}
 
function updateCount ()
{
var qText = jQuery("#questionTextBox").val();
if(qText.length < 4000) {
jQuery("#noOfChar").html(4000 - qText.length);
} else {
jQuery("#noOfChar").html(0);
jQuery("#questionTextBox").val(qText.substring(0,4000));
}
}
jQuery("#questionTextBox").keyup(function () {
updateCount();
});
jQuery("#questionTextBox").keypress(function () {
updateCount();
});
jQuery("#questionTextBox").keydown(function () {
updateCount();
});

jQuery(".submitProfile").click(function(){
	var matches = [];
jQuery(".group1:checked").each(function() {
matches.push(this.value);
});
if(matches == 'on'){
		var id = jQuery('#hidId').val();
			window.location.href = SITE_URL+'linkedin/OAuth2.php';
}
else{
javascript:OpenCheckboxAlert();
}
});
});
</script>