<?php echo($this->Html->script(array('fancybox/jquery.fancybox-1.3.4.pack', 'fancybox/jquery.mousewheel-3.0.4.pack'), false)); ?>
<?php //e($html->css(array('jquery.fancybox-1.3.4')), false); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.addAttribute').fancybox();
    });		
</script>


<div class="fieldset">
    <h3 class="legend">
		Members
        <div class="total" style="float:right"> Total Members : <?php echo(count($data)); ?>
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td  width="10%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Name</td>
                        <td  width="10%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Email</td>
			<td  width="10%" align="left" valign="middle" class="Bdrrightbot Padtopbot6">Consultant Name</td>
                        <td  width="10%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Company</td>
                        <td  width="5%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Phone</td>
		        <td  width="15%" align="left" valign="middle" class="Bdrrightbot Padtopbot6">Answer</td>
			<td  width="15%" align="left" valign="middle" class="Bdrrightbot Padtopbot6">Type</td>
                        <td  width="15%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Date</td>
                        <td  width="10%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Action</td>

                    </tr>
                    <?php 
                    if(!empty($data) && count($data)>0)
                    { 
                        foreach(array_reverse($data) as $key => $value)
                        {    
                        
                        ?>
                        <tr>
                            <td  width="10%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo(ucwords($value['Prospect']['firstname']. ' '.$value['Prospect']['lastname'])); ?></td>
                            <td  width="10%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo($value['Prospect']['email']); ?></td>    
                            <?php {   $menu2 = ClassRegistry::init('UserReference');  //for class load in view
                                             $username = $menu2->find('first', array('conditions' => array('UserReference.user_id' => $value['Prospect']['member_id'])));}?>

                            <td  width="10%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo($username['UserReference']['first_name']);?> <?php echo($username['UserReference']['last_name']); ?></td>
                            <td  width="10%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo($value['Prospect']['company']); ?></td>
                            <td  width="5%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo($value['Prospect']['phone']); ?></td>
                            <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo($value['Prospect']['answer']); ?></td>
                            <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo($value['Prospect']['prospect_type']); ?></td>



                            <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo($value['Prospect']['submitted']); ?></td>
                            <td  width="10%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">
                           		<?php 
                           		if($value['Prospect']['isApproved'] == 0) {
                           		echo($this->Admin->getActionImage(array(
                           			'approve' => array('controller' => 'fronts', 'action' => 'admin_prospective_approve'), 
                           			'delete' => array('controller' => 'fronts', 'action' => 'admin_prospective_delete')), 
                           			$value['Prospect']['id']));
                           			}
                           			?>
                           </td> 

                        </tr>	
                   <?php
                        }
                    }else{ ?>
                   <tr>
                        <td colspan="3" width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Record not found</td>           
                    </tr>
                   <?php } ?>   
                </table>
            </div>
     <?php if (!empty($data)) { ?>
            <?php
            echo($this->Form->end());
        } else {
            ?>
            <div style="color:blue, font-size:20; padding-bottom:30px;padding-top:30px;" align="center" ><strong>No Records Found.</strong></div>
        <?php } ?>

    </div>
</div>
<div class="clr"></div>
	 