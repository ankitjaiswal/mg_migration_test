<?php 

    $city_name = $keyword = "";

	
	if($this->Session->read('Auth.User.id') =='' || $this->Session->read('Auth.User.id') ==1) {
		if(trim($city_name) == "" || trim($city_name) == ","){
			$city_name	=	'City, ST';	
		}		
	}else{
		if(trim($city_name) == "" || trim($city_name) == ","){
                     if($loggedInUsercityData['City']['city_name'] != "" && $loggedInUsercityData['City']['state'] != ""){
			$city_name	=	$loggedInUsercityData['City']['city_name'].', '.$loggedInUsercityData['City']['state'];
                    }
                    else{
                    $city_name = 'New York, NY';
                    }
		}	
	}
	
?>

<div class="industries-category-page">
    <div class="industries-category-head">
        <div class="container">
            <div class="industries-category-search">
                <h1>Proven Expertise for Your Organization</h1>
                <p>Get specialized help to address critical business needs and key talent gaps.</p>
                <form>
                    <div class="dropdown-section">
                        <a class="dropdown-btn" href="javascript:void(0);" id="industrydropdown">SEARCH ALL INDUSTRIES</a>
                        <div class="dropdown-box">
                            <ul>
                                <li><a class="category-name" href="javascript:void(0);" value="financial-services/all-categories">Financial Services</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="financial-services/banking">Banking</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="financial-services/capital-markets">Capital Markets</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="financial-services/insurance">Insurance</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="financial-services/investment-banking">Investment Banking</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="financial-services/investment-management">Investment Management</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="financial-services/venture-capital-&-private-equity">Venture Capital & Private Equity</a></li>
                            </ul>
                            <ul>
                                <li><a class="category-name" href="javascript:void(0);" value="Food/all-categories">Food</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="Food/dairy">Dairy</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="Food/food-&-beverages">Food & Beverages</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="Food/restaurants">Restaurants</a></li>
                            </ul>
                            <ul>
                                <li><a class="category-name" href="javascript:void(0);" value="Healthcare/all-categories">Healthcare</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="Healthcare/biotechnology">Biotechnology</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="Healthcare/health,-wellness-and-fitness">Health, Wellness and Fitness</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="Healthcare/hospital">Hospital</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="Healthcare/insurance">Insurance</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="Healthcare/medical-devices">Medical Devices</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="Healthcare/medical-practice">Medical Practice</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="Healthcare/pharmaceuticals">Pharmaceuticals</a></li>
                            </ul>
                            <ul>
                                <li><a class="category-name" href="javascript:void(0);" value="industrial/all-categories">Industrial</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="industrial/business-supplies-&-equipment">Business Supplies & Equipment</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="industrial/defense-&-space">Defense & Space</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="industrial/electrical/electronic-manufacturing">Electrical/Electronic Manufacturing</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="industrial/industrial-automation">Industrial Automation</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="industrial/plastics">Plastics</a></li>
                            </ul>
                            <ul>
                                <li><a class="category-name" href="javascript:void(0);" value="other-services/all-categories">Other Services</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="other-services/consumer-services">Consumer Services</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="other-services/e-learning">E-Learning</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="other-services/education-management">Education Management</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="other-services/hospitality">Hospitality</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="other-services/market-research">Market Research</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="other-services/outsourcing-&-offshoring">Outsourcing & Offshoring</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="other-services/security-and-investigations">Security and Investigations</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="other-services/think-tanks">Think Tanks</a></li>
                            </ul>
                            <ul>
                                <li><a class="category-name" href="javascript:void(0);" value="professional-services/all-categories">Professional Services</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="professional-services/accounting-&-hr">Accounting & HR</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="professional-services/consulting-&-coaching">Consulting & Coaching</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="professional-services/legal-services">Legal Services</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="professional-services/marketing-and-advertising">Marketing & Advertising</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="professional-services/public-relations-and-communications">Public Relations & Communications</a></li>
                            </ul>
                            <ul>
                                <li><a class="category-name" href="javascript:void(0);" value="public-sector-&-non-profit/all-categories">Public Sector & Non-profit</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="public-sector-&-non-profit/fund-raising">Fund-Raising</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="public-sector-&-non-profit/government-administration">Government Administration</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="public-sector-&-non-profit/nonprofit-management">Nonprofit Management</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="public-sector-&-non-profit/public-policy">Public Policy</a></li>
                            </ul>
                            <ul>
                                <li><a class="category-name" href="javascript:void(0);" value="retail-&-wholesale/all-categories">Retail & Wholesale</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="retail-&-wholesale/apparel-&-fashion">Apparel & Fashion</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="retail-&-wholesale/consumer-goods">Consumer Goods</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="retail-&-wholesale/luxury-goods-&-jewelry">Luxury Goods & Jewelry</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="retail-&-wholesale/sporting-goods">Sporting Goods</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="retail-&-wholesale/supermarkets">Supermarkets</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="retail-&-wholesale/warehousing">Warehousing</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="retail-&-wholesale/wholesale">Wholesale</a></li>
                            </ul>
                            <ul>
                                <li><a class="category-name" href="javascript:void(0);" value="technology/all-categories">Technology</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="technology/computer-&-network-security">Computer & Network Security</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="technology/computer-hardware">Computer Hardware</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="technology/computer-software">Computer Software</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="technology/consumer-electronics">Consumer Electronics</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="technology/information-technology-&-services">Information Technology & services</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="technology/startups">Startups</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="technology/telecommunications">Telecommunications</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="technology/wireless">Wireless</a></li>
                            </ul>
                            <ul>
                                <li><a class="category-name" href="javascript:void(0);" value="travel-&-transportation/all-categories">Travel & Transportation</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="travel-&-transportation/automotive">Automotive</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="travel-&-transportation/Leisure,-travel-&-tourism">Leisure, Travel & Tourism</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="travel-&-transportation/logistics-and-supply-chain">Logistics and Supply chain</a></li>
                            </ul>
                            <ul>
                                <li><a class="category-name" href="javascript:void(0);" value="other-services/all-categories">Other</a></li>
                                <?php /**<li><a class="category-name" href="javascript:void(0);" value="building/real-estate">Real Estate</a></li>**/?>
                                <li><a class="category-name" href="javascript:void(0);" value="energy/oil-&-gas">Oil & Gas</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="energy/renewables-&-environment">Renewables & Environment</a></li>
                                <li><a class="category-name" href="javascript:void(0);" value="sports,-arts-&-entertainment/sports" style="padding-bottom:10px;">Sports</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="search-box" id="inputbox">

 					<div style="display:none;">
					    <?php
						if($this->Session->read('Auth.User.id') =='' || $this->Session->read('Auth.User.id') ==1) {
						    echo($this->Form->input('City.city_name',array('id'=>'userlocation','div'=>false,'label'=>false,'class'=>'newyork','onclick'=>'select();','type'=>'text','placeholder'=>'City, ST','value'=>$city_name)));
						}else{
						    echo($this->Form->input('City.city_name',array('id'=>'userlocation','div'=>false,'label'=>false,'onclick'=>'select();',
								    'placeholder'=>'City, ST','value'=>$city_name)));
						}
					    ?>
					    <?php echo($this->Html->image('loading.gif',array('id'=>'loading','width'=>'18px','height'=>'18px','alt'=>'Loading','style'=>'margin-top:-30px;float:right;display:none;')));?>
																								
					</div>

                        <?php 	echo($this->Form->input('City.keyword',array('id'=>'enterkeyword','autofocus'=>'autofocus','onkeypress'=>'searchKeyPress(event);','div'=>false,'label'=>false,'class'=>'form-control','type'=>'text',"placeholder"=>"Enter an area of expertise e.g. Business Strategy",'value'=>$keyword)))	?>
                        
                        <a class="btn" id="btnSearch" href="javascript:void(0);" onclick="sendingSearch();"><img src="<?php echo SITE_URL?>/yogesh_new1/images/search-icon.png">Search</a>
                    </div>
                </form>
                <div class="most-box">
					<?php 
                                         $Search1 = Configure::read('popularSearch1');
                                         $Search2 = Configure::read('popularSearch2');
                                         $Search3 = Configure::read('popularSearch3');
                                         $popularSearch1 = Configure::read('popularSearch1');
					 $popularSearch2 = Configure::read('popularSearch2');
					 $popularSearch3 = Configure::read('popularSearch3');
                                         $popularSearch1 = str_replace(" ","-",$popularSearch1);
                                         $popularSearch2 = str_replace(" ","-",$popularSearch2);
                                         $popularSearch3 = str_replace(" ","-",$popularSearch3);
                                         if($city_name == "City, ST")
                                         $city_name = "New York, NY";

                                  
                                   $city_state=explode(',',$city_name);
                                   if(false !== stripos($city_state[0]," "))
                                   $city_state[0] = str_replace(" ","-",$city_state[0]);
                                   $city_state[0] = strtolower($city_state[0]);
                                   $city_state[1] = trim($city_state[1]," ");
                                   $city_name = $city_state[1].'/'.$city_state[0];
					
					?>

                    <label>Popular searches</label>

    <a href="<?php echo(SITE_URL."browse/search_result/all-industries/all-categories/".$popularSearch1.'/'.$city_name);?>" onclick="submitForm('<?php echo $popularSearch1 ?>');"  title="<?php echo($popularSearch1);?>"><?php echo $Search1; ?></a><span>,</span>&nbsp;<a href="<?php echo(SITE_URL."browse/search_result/all-industries/all-categories/".$popularSearch2.'/'.$city_name);?>" onclick="submitForm('<?php echo $popularSearch2 ?>');"  title="<?php echo($popularSearch2);?>"><?php echo $Search2; ?></a><span>,</span>&nbsp;<a href="<?php echo(SITE_URL."browse/search_result/all-industries/all-categories/".$popularSearch3.'/'.$city_name);?>" onclick="submitForm('<?php echo $popularSearch3 ?>');"  title="<?php echo($popularSearch3);?>"><?php echo $Search3; ?></a>
                </div>
            </div>
        </div>
    </div>

    <div class="featured-profile-section">
        <div class="container">
            <h2>Featured profiles</h2>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="featured-profile-box">
                         <div class="box-head">
                              <span></span>
                         </div>
                         <img class="box-bg-img" src="<?php echo SITE_URL?>/yogesh_new1/images/box-top.png">
                         <div class="meta-box">
                             <img src="<?php echo SITE_URL?>imgs/John-Baldoni-Guild.jpg" style="width:85px;">
                             <h6>John Baldoni</h6>
                         </div>
                         <div class="pro-post">
                             <p>Leadership Development</p>
                         </div>
                         <div class="pro-info">
                             <p>John Baldoni is an internationally recognized leadership consultant, coach, and author of more than dozen books that have been translated into 10 languages. John has taught what it means to inspire at the top of a mountain in the Canadian Rockies...</p>
                         </div>
                         <div class="box-bottom">
                             <a href="<?php echo SITE_URL?>john.baldoni">View Profile</a>
                         </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="featured-profile-box">
                         <div class="box-head">
                              <span></span>
                         </div>
                         <img class="box-bg-img" src="<?php echo SITE_URL?>/yogesh_new1/images/box-top.png">
                         <div class="meta-box">
                             <img src="<?php echo SITE_URL?>img/members/profile_images/870/Blumberg.jpg" style="width:85px;">
                             <h6>Michael Blumberg</h6>
                         </div>
                         <div class="pro-post">
                             <p>Aftermarket Service Industry Expert, Management Consultant, Entrepreneur, and Deal Maker</p>
                         </div>
                         <div class="pro-info">
                             <p>Michael Blumberg has 30+ years of professional experience with key focus on reverse logistics & aftermarket services. Proficient in analyzing and forecasting industry trends...</p>
                         </div>
                         <div class="box-bottom">
                             <a href="<?php echo SITE_URL?>michael.blumberg">View Profile</a>
                         </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="featured-profile-box">
                         <div class="box-head">
                              <span></span>
                         </div>
                         <img class="box-bg-img" src="<?php echo SITE_URL?>/yogesh_new1/images/box-top.png">
                         <div class="meta-box">
                             <img src="https://www.guild.im/img/members/profile_images/1495/Mark-Palmer-Guild.jpg" style="width:85px;">
                             <h6>Mark Palmer</h6>
                         </div>
                         <div class="pro-post">
                             <p>Experienced C-Level Interim Executive. Sales consultant, Author, Speaker and Entrepreneur</p>
                         </div>
                         <div class="pro-info">
                             <p>Mark Palmer focuses on the discovery and execution of the next steps to achieving your overall objective. He leads his clients to find the small incremental steps...</p>
                         </div>
                         <div class="box-bottom">
                             <a href="<?php echo SITE_URL?>mark.palmer">View Profile</a>
                         </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

        <!-- js link -->
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>


<script type="text/javascript">
  
    
    jQuery(document).ready(function() {
          jQuery('.dropdown-btn').click(function() {
            if(jQuery(".dropdown-box").hasClass("open")){
                jQuery(".dropdown-box").removeClass("open");
            }
            else{
                jQuery(".dropdown-box").addClass("open");
            }
          });
          jQuery(document).click(function(e) {

            if (jQuery(e.target).is('.dropdown-btn'))  return false;
            else jQuery(".dropdown-box").removeClass("open");
          }); 
        }); 
    jQuery(".category-name").click(function(){
        jQuery(".dropdown-btn").html(jQuery(this).html());
    });
 

</script>


<!--This Java Script is used only Autocomplte Searching Product name start-->
<script type="text/javascript">

function monkeyPatchAutocomplete() {

    jQuery.ui.autocomplete.prototype._renderItem = function( ul, item) {
        var re = new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + this.term + ")(?![^<>]*>)(?![^&;]+;)", "gi");
        var t = item.label.replace(re,"<span style='font-weight:bold;color:#434343;'>" + 
        		"$&" + 
                "</span>");
        return jQuery( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + t + "</a>" )
            .appendTo( ul );
    };
}

	function getKeywords(){

		var allKeywords = <?php echo json_encode($allKeywords); ?>;

		return allKeywords;
	}
	
	var URL = '<?php echo(SITE_URL); ?>fronts/search';

	jQuery(document).ready(function(){

	monkeyPatchAutocomplete();

	var userlocation = jQuery('#userlocation');

	userlocation.autocomplete({
	minLength    : 1,
	source        : URL
	});

	var enterkeyword = jQuery('#enterkeyword');

	enterkeyword.autocomplete({
	minLength    : 1,
	source        : getKeywords()
	});

	});

	// A custom jQuery method for placeholder text:

	jQuery.fn.defaultText = function(value){

	var element = this.eq(0);
	element.data('defaultText',value);

	element.focus(function(){
	if(element.val() == value){
	element.val('').removeClass('defaultText');
	}
	}).blur(function(){
	if(element.val() == '' || element.val() == value){
		element.addClass('defaultText').val(value);
	}
	});

	return element.blur();
	}


</script>


<script type="text/javascript">
jQuery(document).ready(function(){
jQuery('#ui-id-2').click(function() {
sendingSearch();
});
jQuery(".category-name").click(function(e) {

        var value = jQuery(e.target).attr("value");        
        jQuery('#industrydropdown').val(value);


    });
});
</script>
<script type="text/javascript">

   function htmlEntities(str) {
    return String(str).replace(/&/g, '_and').replace(/</g, 'lt').replace(/\//g, '_slash').replace(/-/g, '_or');
}
  function searchKeyPress(e)
    {
        // look for window.event in case event isn't passed in
        if (typeof e == 'undefined' && window.event) { e = window.event; }
        if (e.keyCode == 13)
        {
            document.getElementById('btnSearch').click();
        }
    }

function sendingSearch() {

	var searchWord = document.getElementById('enterkeyword').value.toLowerCase();
	var cityWord = document.getElementById('userlocation').value.toLowerCase();
        var indcatpair = document.getElementById('industrydropdown').value;

            if(indcatpair == undefined){
            indcatpair = "all-industries/all-categories";
            }

	 var cityWord1 = '';
         var searchWord = htmlEntities(searchWord);
    

		
	if(searchWord == '')
        {
    	searchWord = "browse all";
        }
  
    if(cityWord == "" || cityWord == "international"){
		cityWord1 = "new york";
                cityWord = "ny";
    }

   	if(cityWord.indexOf(",") !=-1){
       var split = cityWord.split(",");
       cityWord1 = split[0];
       cityWord = split[1];
    }
      
    if(searchWord.charAt(0) == " "){
    	searchWord = searchWord.trim();
    }
    
	searchWord = searchWord.replace(/ /gi, "-"); 


        if(cityWord1.indexOf("lana'i") !=-1){
           cityWord1 ="Honolulu";
           }

	cityWord1 = cityWord1.replace(/ /gi, "-"); 
        cityWord = cityWord.replace(/ /gi, "");
        cityWord = cityWord.toUpperCase();

	
	window.location.href=SITE_URL+'browse/search_result/'+indcatpair+'/'+searchWord+'/'+cityWord+'/'+cityWord1;
	
	return false;
}


</script>




