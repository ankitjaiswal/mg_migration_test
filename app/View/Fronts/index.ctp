<script defer type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
<div class="member-content">

	<div class="hero-section">
		<div class="container-wrap">
			<div class="row">
				<div class="col-sm-6">
					<h2>Targeted solutions for your critical business problems</h2>
                                        
					<p>When you have time-sensitive needs in strategy development, go-to-market, financial analysis, and more, GUILD delivers powerful solutions that combine deep industry expertise and sharp project management &#8212; <b>meet your new competitive advantage.</b></p>
					        
						<div class="hero-section-input">


							<input name="" type="submit" id="emailinputsubmit" class="btn-red btn" value="Post your project">
							<p class="hero-section-input-msg"></p>
						</div>
                                                
				</div>
				<div class="col-sm-6">
					<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/hero-img-new.jpg" alt="" />
				</div>
			</div>
		</div>
	</div>

	<div class="companyintro-section">
		<div class="container-wrap">
			<div class="row d-flex justify-content-center text-center">
				<div class="col-md-12 col-lg-8">
					<div class="section-title">
						<h2>Customized to your company's needs</h2>
						<p>When your company requires immediate expertise, GUILD can help.  Our consultants have deep domain experience and we work collaboratively with your internal staff for rapid results.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="company-slide">
				<div class="row d-flex align-items-center">
					<div class="col-xs-12 col-sm-12 col-lg-6">
						<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/img-1.svg" alt="" />
					</div>
					<div class="col-xs-12 col-sm-12 col-lg-6">
						<div class="intro-text">
							<h3>Implement Key <br class="visible-lg" />Performance Indicators (KPIs) </h3>
							<a href="<?php echo(SITE_URL)?>project/create" class="btn-red">What do you need solved?</a>
						</div>
					</div>
				</div>
				<div class="row d-flex align-items-center company-needs" style="display:none!important;">
					<div class="col-xs-12 col-sm-12 col-lg-6">
						<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/img-2.svg" alt="" />
					</div>
					<div class="col-xs-12 col-sm-12 col-lg-6">
						<div class="intro-text">
							<h3>Financial analysis to <br class="visible-lg" />understand and choose <br class="visible-lg" />between options</h3>
							<a href="<?php echo(SITE_URL)?>project/create" class="btn-red">What do you need solved?</a>
						</div>
					</div>
				</div>
				<div class="row d-flex align-items-center company-needs" style="display:none!important;">
					<div class="col-xs-12 col-sm-12 col-lg-6">
						<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/img-3.svg" alt="" />
					</div>
					<div class="col-xs-12 col-sm-12 col-lg-6">
						<div class="intro-text">
							<h3>Identify and implement <br class="visible-lg" />new systems to upgrade <br class="visible-lg" />your organization</h3>
							<a href="<?php echo(SITE_URL)?>project/create" class="btn-red">What do you need solved?</a>
						</div>
					</div>
				</div>
				<div class="row d-flex align-items-center company-needs" style="display:none!important;">
					<div class="col-xs-12 col-sm-12 col-lg-6">
						<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/img-4.svg" alt="" />
					</div>
					<div class="col-xs-12 col-sm-12 col-lg-6">
						<div class="intro-text">
							<h3>Research and execute <br class="visible-lg" />high-potential growth <br class="visible-lg" />ideas</h3>
							<a href="<?php echo(SITE_URL)?>project/create" class="btn-red">What do you need solved?</a>
						</div>
					</div>
				</div>
				<div class="row d-flex align-items-center company-needs" style="display:none!important;">
					<div class="col-xs-12 col-sm-12 col-lg-6">
						<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/img-5.svg" alt="" />
					</div>
					<div class="col-xs-12 col-sm-12 col-lg-6">
						<div class="intro-text">
							<h3>C-level industry specialist <br class="visible-lg" />to serve in an interim role</h3>
							<a href="<?php echo(SITE_URL)?>project/create" class="btn-red">What do you need solved?</a>
						</div>
					</div>
				</div>
				<div class="row d-flex align-items-center company-needs" style="display:none!important;">
					<div class="col-xs-12 col-sm-12 col-lg-6">
						<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/img-6.svg" alt="" />
					</div>
					<div class="col-xs-12 col-sm-12 col-lg-6">
						<div class="intro-text">
							<h3>Streamline processes <br class="visible-lg" />and improve staff <br class="visible-lg" />productivity</h3>
							<a href="<?php echo(SITE_URL)?>project/create" class="btn-red">What do you need solved?</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-wrap">
			<div class="row">
				<div class="col-sm-2">
					<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/img-2.svg" alt="" />
					<h4>Financial analysis to understand and choose between options</h4>
				</div>
				<div class="col-sm-2">
					<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/img-3.svg" alt="" />
					<h4>Identify and implement new systems to upgrade your organization</h4>
				</div>
				<div class="col-sm-2">
					<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/img-4.svg" alt="" />
					<h4>Research and execute high-potential growth ideas</h4>
				</div>
				<div class="col-sm-2">
					<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/img-5.svg" alt="" />
					<h4>C-level industry specialist to serve in an interim role</h4>
				</div>
				<div class="col-sm-2">
					<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/img-6.svg" alt="" />
					<h4>Streamline processes and improve staff productivity</h4>
				</div>
			</div>
		</div>
	</div>

	<div class="requirements-section">
		<div class="container-wrap">
			<div class="row d-flex justify-content-center text-center">
				<div class="col-md-12 col-lg-8">
					<div class="section-title">
						<h2>GUILD fits your unique requirements</h2>
						<p>Our solution is tailored to fit your project needs. Specialists working on your project are supported by a Client Partner who understands your organization and coordinates all activities for best results.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="service-box">
						<div class="service-icon">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/icon-1.svg" alt="" />
						</div>
						<div class="service-detail">
							<h4>Vetted experts</h4>
							<p>High performing subject-matter experts are matched to your unique needs.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-box">
						<div class="service-icon">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/icon-2.svg" alt="" />
						</div>
						<div class="service-detail">
							<h4>Built-in project management</h4>
							<p>A dedicated Client Partner facilitates success for each project with your company.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-box">
						<div class="service-icon">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/icon-3.svg" alt="" />
						</div>
						<div class="service-detail">
							<h4>Trust</h4>
							<p>Highest-in-industry repeat rate. Customer satisfaction drives GUILD growth.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row d-flex justify-content-center text-center">
				<div class="col-sm-12">
					<a href="<?php echo(SITE_URL)?>project/create" class="btn-red">What do you need solved?</a>
				</div>
			</div>
		</div>
	</div>

	<div class="m-testimonial review-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="member-detail mb-title">
						<h2>What our clients are saying about us</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="t-testimonial">

				    	<div class="item">
				            <div class="t-detail">
				            	<p>"GUILD [Client Partner] always ensures that both parties are very clear on expectations for the project before work commences. I am very pleased with the responsiveness, quality of work, high level of accountability, and ability to service us in a way that really puts himself in our shoes. I appreciate the connections he is able to make with other experts, which has brought valuable outside knowledge into our projects."</p>
				            	<span>Head of HR of a Leading B2B Services Organization</span>
				            </div>
				        </div>
			        	<div class="item">
			                <div class="t-detail">
			                	<p>"Objectivity, project management, and an informed independent voice."</p>
			                	<span>CEO of a Leader in Healthcare Education</span>
			                </div>
			            </div>
		            	<div class="item">
		                    <div class="t-detail">
		                    	<p>"GUILD does a great job at understanding how its clients operate and integrate [its solutions] into the operations."</p>
		                    	<span>President of a Fast-Growing IT Infrastructure Provider</span>
		                    </div>
		                </div>
	                	<div class="item">
	                        <div class="t-detail">
	                        	<p>"GUILD [Client Partner] was great at project management. He was very attuned to the needs of the customer and had a good sense of what they would accept and what they would not."</p>
	                        	<span>Head of Talent Development of an Innovative HR Services Organization</span>
	                        </div>
	                    </div>
	                    <div class="item">
	                            <div class="t-detail">
	                            	<p>"GUILD is a key partner in our operations, and gives us easy access to relevant business experts [which has been] so important to our growth..."</p>
	                            	<span>President of a Market Leader in Event Production and Event Staffing</span>
	                            </div>
	                     </div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="masters-section">
		<div class="container">
			<div class="row text-center">
				<div class="col-sm-12">
					<div class="section-title">
						<h2>Subject Matter Experts (SMEs)</h2>
						<p>Each GUILD consultant has over two decades of deep domain expertise. They are thoroughly vetted for their credentials, industry reputation, and client feedback. Further, before assigning them to your project, our internal Client Partner screens them for directly relevant experience, compatibility and budget fit.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12">
				    <div class="featured-profile-box-home featured-profile-box expert">
				         <div class="box-head">
				              <span></span>
				         </div>
				         <img class="box-bg-img" src="<?php echo SITE_URL?>/yogesh_new1/images/homepage_image/box-top.png">
				         <div class="meta-box">
				             <img src="https://www.guild.im/img/members/profile_images/3240/David-Williams-GUILD.png" style="width:85px;">
				             <div class="meta-info">
				             	<h6>David Williams</h6>
				             	<p>Boston, MA</p>
				             </div>
				         </div>
                         <div class="pro-post">
                            <p>Healthcare Strategy</p>
                         </div>
				         <div class="pro-info">
				             <p>I've translated my passion for healthcare business and policy into a career as a healthcare strategy consultant, entrepreneur and board member. Lately I've focused mainly on digital health, technology enabled healthcare services, and working with multi-stakeholder collaboratives...</p>
				         </div>
				    </div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
				    <div class="featured-profile-box-home featured-profile-box expert">
				         <div class="box-head">
				              <span></span>
				         </div>
				         <img class="box-bg-img" src="<?php echo SITE_URL?>/yogesh_new1/images/homepage_image/box-top.png">
				         <div class="meta-box">
				             <img src="https://www.guild.im/img/members/profile_images/2665/Dean-Small-GUILD.jpg" style="width:85px;">
				             <div class="meta-info">
				             	<h6>Dean Small</h6>
				             	<p>Costa Mesa, CA</p>
				             </div>
				         </div>
                         <div class="pro-post">
                      		<p>Restaurant Efficiency</p>
                         </div>
				         <div class="pro-info">
				             <p>As founder and Managing Partner of Synergy Restaurant Consultants, Dean Small has brought his passion for food and extensive experience within the industry together to strategically guide other restaurant owners. Although much of Dean's resume reads like a...</p>
				         </div>
				    </div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
				    <div class="featured-profile-box-home featured-profile-box expert">
				         <div class="box-head">
				              <span></span>
				         </div>
				         <img class="box-bg-img" src="<?php echo SITE_URL?>/yogesh_new1/images/homepage_image/box-top.png">
				         <div class="meta-box">
				             <img src="https://www.guild.im/img/members/profile_images/1775/Yohan-Jacob-GUILD.png" style="width:85px;">
				             <div class="meta-info">
				             	<h6>Yohan Jacob</h6>
				             	<p>Streamwood, IL</p>
				             </div>
				         </div>
                         <div class="pro-post">
                         	<p>Retail Marketing and Merchandizing</p>
                         </div>
				         <div class="pro-info">
				             <p>Yohan Jacob is the President of Retailbound, Inc., a Chicago-based retail marketing and merchandising consulting company. Prior to working at Retailbound, Inc., Mr. Jacob has spent time as a senior merchant at 2 large billion retailers, Sears Holdings and OfficeMax, as well as spent time as a regional sales...</p>
				         </div>
				    </div>
				</div>

			</div>
			<div class="row d-flex justify-content-center text-center">
				<div class="col-sm-12">
					<a href="<?php echo(SITE_URL)?>expertise_search" class="btn-red"><i class="fa fa-search"></i> Search for expertise</a>
				</div>
			</div>
		</div>
	</div>

	<div class="companys-section">
		<div class="container-wrap">
			<div class="row d-flex justify-content-center text-center">
				<div class="col-md-12 col-lg-8">
					<div class="section-title">
						<h2>Our consultants bring a wealth <br class="visible-lg">of experience from top-tier companies</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<ul class="d-flex justify-content-between">
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/company-1.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/company-2.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/company-3.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/company-4.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/company-5.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/company-6.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/company-7.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/company-8.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/company-9.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/company-10.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/company-11.png" alt="" /></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="requirements-section partnership-section">
		<div class="container-wrap">
			<div class="row d-flex justify-content-center text-center">
				<div class="col-sm-12">
					<div class="section-title">
						<h2>Start-to-finish partnership</h2>
						<p>GUILD works with you to define a project, develop a team that fits your project's unique needs, and support each project until it's complete.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="service-box">
						<div class="service-icon">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/icon-4.svg" alt="" />
						</div>
						<div class="service-detail">
							<p>Tell us your business need, and we'll help you define detailed project requirements.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-box">
						<div class="service-icon">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/icon-6.svg" alt="" />
						</div>
						<div class="service-detail">
							<p>Receive proposals from specialized professionals who fit your needs.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-box">
						<div class="service-icon">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/icon-8.svg" alt="" />
						</div>
						<div class="service-detail">
							<p>Your Client Partner will provide end-to-end project management support.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row d-flex justify-content-center text-center">
				<div class="col-sm-12">
					<p class="notes">GUILD's professionals support your project until you're satisfied.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="industry-section">
		<div class="container">
			<div class="row d-flex justify-content-center text-center">
				<div class="col-sm-12">
					<div class="section-title">
						<h2>At the intersection of industry and function</h2>
						<p>GUILD network includes highest-grade industry professionals to support any function you need.  GUILD experts and Client Partners bring deep training and experience to your company.</p>
					</div>
				</div>
			</div>
			<div class="row graphics-box">
				<div class="col-sm-12 d-flex align-items-center justify-content-center">
					<ul>
					    <li>Retail</li>
					    <li>Professional services</li>
					    <li>Education</li>
					    <li>Technology</li>
					</ul>
					<div class="function-img">
						<img src="<?php echo(SITE_URL)?>yogesh_new1/images/homepage_image/function-graph-new2.svg" alt="" />
					</div>
					<ul>
					    <li>Financial projections</li>
					    <li>Human resources</li>
					    <li>Supply chain</li>
					    <li>Executive coaching</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<!-- <p class="info">At the intersection of industry and function.</p> -->
					<a href="<?php echo(SITE_URL)?>expertise_search" class="btn-red"><i class="fa fa-search"></i> Search for expertise</a>
				</div>
			</div>
		</div>
	</div>

</div>


<script type="text/javascript">

    jQuery(document).ready(function(){
	    jQuery('.t-testimonial').slick({
	      dots: true,
	      arrows: false,
	      infinite: true,
	      speed: 300,
	      autoplaySpeed: 8000,
	      autoplay: true,
	      slidesToShow: 1,
	      adaptiveHeight: true,
	      draggable: false
	    });
	
	
		$(".company-needs").css("display","block");
	      jQuery('.company-slide').slick({
	      	 dots: false,
	       	arrows: false,
	       	fade: true,
	       	infinite: true,
	       	autoplay: true,
	       	autoplaySpeed: 3000,
	       	speed: 1200,
	       	slidesToShow: 1
	     }); 

    });
    jQuery('.Experts').slick({
      slidesToShow: 2,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 4000,
      responsive:[
      	{
  	      breakpoint: 992,
  	      settings: {
  	        slidesToShow: 1,
  	        slidesToScroll: 1,
  	      }
  	    }
      ]
    });
</script>

<script type="text/javascript">
jQuery(document).ready(function() {
jQuery("#emailinputsubmit").click(function(){

var destinationurl = "https://test.guild.im/project/create";

window.open(destinationurl,"_self");

/**if(loggedUserId !='')
{

var emailvalue = "<?php echo $this->Session->read('Auth.User.username'); ?>";

}else{

var emailvalue = document.getElementById('emailinput').value;
}
if(emailvalue == ''){

jQuery('#emailinput').css('border-color','#F00');
jQuery('.hero-section-input-msg').html("Please enter valid work email");

} else if(emailvalue != '' && emailvalue.indexOf('@') >= 0){
      
		       jQuery.ajax({
					url:SITE_URL+'fronts/emailsession',
					type:'post',
					dataType:'json',
					data: 'emailvalue='+emailvalue,
					success:function(id){
                                                window.open(destinationurl,"_self");					}
				});	
		       
		   		return false;
                              }
else{
jQuery('#emailinput').css('border-color','#F00');
jQuery('.hero-section-input-msg').html("Please enter valid work email");
}**/





    });
});
</script>

