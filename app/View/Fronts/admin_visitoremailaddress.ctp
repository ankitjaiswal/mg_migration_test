<?php echo($this->Html->script(array('fancybox/jquery.fancybox-1.3.4.pack', 'fancybox/jquery.mousewheel-3.0.4.pack'), false)); ?>
<?php //e($html->css(array('jquery.fancybox-1.3.4')), false); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.addAttribute').fancybox();
    });		
</script>


<div class="fieldset">
    <h3 class="legend">
		Members
        <div class="total" style="float:right"> Total Members : <?php echo(count($data)); ?>
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td  width="50%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Visitor Emailaddress</td>
                        <td  width="50%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Visit Date</td>

                    </tr>
                    <?php 
                    if(!empty($data) && count($data)>0)
                    { 
                        foreach(array_reverse($data) as $key => $value)
                        {    
                        
                        ?>
                        <tr>
                            <td  width="50%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo(ucwords($value['Emailprospect']['visitoremailid'])); ?></td>
                            <td  width="50%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo($value['Emailprospect']['created']); ?></td>  

                        </tr>	
                   <?php
                        }
                    }else{ ?>
                   <tr>
                        <td colspan="3" width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Record not found</td>           
                    </tr>
                   <?php } ?>   
                </table>
            </div>
     <?php if (!empty($data)) { ?>
            <?php
            echo($this->Form->end());
        } else {
            ?>
            <div style="color:blue, font-size:20; padding-bottom:30px;padding-top:30px;" align="center" ><strong>No Records Found.</strong></div>
        <?php } ?>

    </div>
</div>
<div class="clr"></div>
	 