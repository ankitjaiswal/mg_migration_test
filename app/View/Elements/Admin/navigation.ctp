<div id="navigation">
    <div class="glossymenu">
	
        <?php /*         * ************************** Admin Management Management End**************************** */ ?>			
        <?php echo($this->Html->link('Directory Management', array(), array('class' => 'menuitem submenuheader'))); ?>
        <div class="submenu">
            <ul>
                <li><?php echo($this->Html->link(__('List Directory Users', true), array("controller" => "members", "action" => "admin_directory_index"))); ?></li>
                <li>
                    <?php echo($this->Html->link(__('Clean Directory Data', true), array("controller" => "members", "action" => "admin_directory_clean"))); ?>
                </li> 
                <li>
                    <?php echo($this->Html->link(__('Add Directory User', true), array("controller" => "members", "action" => "admin_directory_add"))); ?>
                </li>                
            </ul>
        </div>
        <?php /*         * ************************** Mentors Management Start**************************** */ ?>			
        <?php echo($this->Html->link('Members Management', array(), array('class' => 'menuitem submenuheader'))); ?>
        <div class="submenu">
            <ul>
                <li><?php echo($this->Html->link(__('List Members', true), array("controller" => "members", "action" => "admin_index"))); ?></li>
                <li>
                    <?php echo($this->Html->link(__('Add Member', true), array("controller" => "members", "action" => "admin_add"))); ?>
                </li> 
                <li>
                    <?php echo($this->Html->link(__('Featured Member', true), array("controller" => "members", "action" => "admin_featured"))); ?>
                </li>
                <li>
                    <?php echo($this->Html->link(__('Pending Member', true), array("controller" => "members", "action" => "admin_pending"))); ?>
                </li> 
                <li>
                    <?php echo($this->Html->link(__('Search Member', true), array("controller" => "members", "action" => "admin_search"))); ?>
                </li>             
            </ul>
        </div>
        <?php /*         * ************************** Mentees Management Start**************************** */ ?>			
        <?php echo($this->Html->link('Clients Management', array(), array('class' => 'menuitem submenuheader'))); ?>
        <div class="submenu">
            <ul>
                <li><?php echo($this->Html->link(__('List Clients', true), array("controller" => "clients", "action" => "admin_index"))); ?></li>
                <li>
                    <?php echo($this->Html->link(__('Add Client', true), array("controller" => "clients", "action" => "admin_add"))); ?>
                </li>   
                <li>
                    <?php echo($this->Html->link(__('Prospective Client', true), array("controller" => "fronts", "action" => "admin_prospective"))); ?>
                </li>
                <li>
                    <?php echo($this->Html->link(__('Present prospective Client', true), array("controller" => "fronts", "action" => "admin_present_prospective"))); ?>
                </li>  
                <li>
                    <?php echo($this->Html->link(__('Visitor Emailaddress', true), array("controller" => "fronts", "action" => "admin_visitoremailaddress"))); ?>
                </li>                   
            </ul>
        </div>	
        <?php /*         * ************************** Answers Management Start**************************** */ ?>			
        <?php echo($this->Html->link('Answers Management', array(), array('class' => 'menuitem submenuheader'))); ?>
        <div class="submenu">
            <ul>
                <li>
                    <?php echo($this->Html->link(__('Member Categories', true), array("controller" => "qna", "action" => "admin_member_categories"))); ?>
                </li>   
                <li>
                    <?php echo($this->Html->link(__('Question Review', true), array("controller" => "qna", "action" => "admin_question_review"))); ?>
                </li> 
                <li>
                    <?php echo($this->Html->link(__('Media Record', true), array("controller" => "qna", "action" => "admin_media_record"))); ?>
                </li>
                <li>
                    <?php echo($this->Html->link(__('Add Media Query', true), array("controller" => "qna", "action" => "admin_media_add"))); ?>
                </li> 
                <li>
                    <?php echo($this->Html->link(__('Category Member Search', true), array("controller" => "qna", "action" => "admin_category_member_search"))); ?>
                </li>                    
            </ul>
        </div>		
        <?php echo($this->Html->link('Project Management', array(), array('class' => 'menuitem submenuheader'))); ?>
        <div class="submenu">
            <ul>
                <li>
                    <?php echo($this->Html->link(__('Project review', true), array('controller' => 'project', 'action' => 'admin_review'))); ?>
                </li> 
                <li>
                    <?php echo($this->Html->link(__('Proposal review', true), array('controller' => 'project', 'action' => 'admin_proposalreview'))); ?>
                </li> 
                <li>
                    <?php echo($this->Html->link(__('CaseStudy review', true), array('controller' => 'members', 'action' => 'admin_casestudyreview'))); ?>
                </li> 
            </ul>
        </div>		
        <?php echo($this->Html->link('Global Settings Management', array(), array('class' => 'menuitem submenuheader'))); ?>
        <div class="submenu">
            <ul>
                <li><?php echo($this->Html->link(__('Global Settings Management List', true), array( 'controller' => 'settings', 'action' => 'admin_index'))); ?></li>
                <li><?php echo($this->Html->link(__('Search Variable List', true), array( 'controller' => 'settings', 'action' => 'admin_searchvariables'))); ?></li>
                <?php /* <li><?php echo($this->Html->link(__('Global Email Template Settings', true), array('plugin' => null, 'controller' => 'email_templates', 'action' => 'index'))); ?></li> */?>
                <li><?php echo($this->Html->link(__('Static Pages Manger', true), array('controller' => 'static_pages', 'action' => 'admin_index'))); ?></li>
                <li>
                    <?php # echo($this->Html->link(__('Add Voucher', true), array("controller"=>"vouchers", "action"=>"add")));?>
                </li>     
                <li>
                    <?php echo($this->Html->link(__('Autocomplete expertise', true), array('controller' => 'reports', 'action' => 'admin_autocomplete'))); ?>
                </li> 
 
            </ul>
        </div>
        <?php echo($this->Html->link('Newsletters Management', array('controller' => 'pages', 'action' => 'index'), array('class' => 'menuitem submenuheader'))); ?>
        <div class="submenu">
            <ul>
                <li>
                    <?php echo($this->Html->link(__('List Subscriber Member', true), array('controller' => 'news_letters', 'action' => 'admin_index'))); ?>
                </li><?php /*?>
                <li>
                    <?php echo($this->Html->link(__('Add NewsLetter', true), array('controller' => 'news_letters', 'action' => 'add'))); ?>
                </li>                        <?php */?>
            </ul>
        </div>	
         <?php echo($this->Html->link('User Reports Management', array('controller' => 'pages', 'action' => 'index'), array('class' => 'menuitem submenuheader'))); ?>
         <div class="submenu">
            <ul>
                <li>
                    <?php echo($this->Html->link(__('Users list', true), array('controller' => 'reports', 'action' => 'admin_userlist'))); ?>
                </li>
                <li>
                    <?php echo($this->Html->link(__('Members list', true), array('controller' => 'reports', 'action' => 'admin_mentorlist'))); ?>
                </li>
                <li>
                    <?php echo($this->Html->link(__('Program list', true), array('controller' => 'reports', 'action' => 'admin_programlist'))); ?>
                </li>
                 <li>
                    <?php echo($this->Html->link(__('Program status', true), array('controller' => 'reports', 'action' => 'admin_weeklyprogramlist'))); ?>
                </li>
                <li>
                    <?php echo($this->Html->link(__('Invoice list', true), array('controller' => 'reports', 'action' => 'admin_invoicelist'))); ?>
                </li>
                <li>
                    <?php echo($this->Html->link(__('Message log users', true), array('controller' => 'reports', 'action' => 'admin_userlog'))); ?>
                </li>
                <li>
                    <?php echo($this->Html->link(__('Member statistics', true), array('controller' => 'reports', 'action' => 'admin_mentorstatics'))); ?>
                </li>
                <li>
                    <?php echo($this->Html->link(__('Search details', true), array('controller' => 'reports', 'action' => 'admin_search'))); ?>
                </li>
                <li>
                    <?php echo($this->Html->link(__('Referral details', true), array('controller' => 'reports', 'action' => 'admin_referral'))); ?>
                </li>
            </ul>
        </div>  		
        <?php /*         * ************************** Static Page Management Start**************************** */ ?>
        <?php //echo($this->Html->link('Static Page Management', array('controller'=>'pages', 'action'=>'index'), array('class'=>'menuitem submenuheader')));?>
        <!-- <div class="submenu">
<ul>
<li>
        <?php echo($this->Html->link(__('List Pages', true), array('controller' => 'static_pages', 'action' => 'index'))); ?>
						</li>
<li>
        <?php echo($this->Html->link(__('Add Page', true), array('controller' => 'static_pages', 'action' => 'add'))); ?>
						</li>                        
</ul>
				</div> -->	
        <?php /*         * ************************** Static Page Management End**************************** */ ?>


    </div>
</div>