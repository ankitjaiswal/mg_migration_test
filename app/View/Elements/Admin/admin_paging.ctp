<div class="PagingTable">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<?php 
		
		if($this->Paginator->request['paging'][$paging_model_name]['pageCount'] >= 2){?>
			<tr>
				<td class="paging">
					<?php
					echo $this->Paginator->prev($this->Html->image('/img/admin/prev.gif', array('border' => '0')), array('escape' => false));
					echo $this->Paginator->numbers();			           
					echo $this->Paginator->next($this->Html->image('/img/admin/next.gif', array('border' => '0')), array('escape' => false));
					?>				
				</td>
			</tr>
			<?php 
		}?>	
		<tr id="total_paging">
			<td>
				<strong> <?php echo "Total ".$total_title." : " . $this->request["paging"][$paging_model_name]["count"]; ?> </strong>
			</td>
		</tr>
	</table>
</div>