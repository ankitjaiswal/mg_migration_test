<style>
#footer-bottom {
    background: url('https://www.guild.im/img/media/footer-btm.png') repeat-x;
    height: 35px;
    padding-top: 20px;
    font-size: 13px;
}

#footer-wrapper {
    background: url("https://www.guild.im/img/media/footer-bg.png") repeat scroll 0 0 transparent;
    bottom: 0;
    position: relative;
    width: 100%;
}
</style>                                            
<?php
if(isset($back_creation)){?>
	<script type="text/javascript">
		jQuery("document").ready(function(){
			loginpopup();		
		});
		
	</script>
	<?php
}?>

<?php
if(isset($openloginpopup)){?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			loginpopup();
		});
	</script>
<?php
}?>

<?php
if(isset($OpenPlanPopup)){?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			var path=SITE_URL+'/pricing/password_setting_popup';
			OPENTINY.box.show({url:path,width:512,height:512,close:true})
		});
	</script>
<?php
}?>
<?php
if(isset($OpenProjectPopup)){?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			var path=SITE_URL+'/project/password_setting_popup';
			TINY.box.show({url:path,width:512,height:512,close:true})
		});
	</script>
<?php
}?>
<?php
if(isset($OpenSubscribePopup)){?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			var path=SITE_URL+'/members/subscribe_to_premium';
			TINY.box.show({url:path,width:500,height:280})
		});
	</script>
<?php
}?>
<?php
if(isset($OpenMemberRegisterPopup)){?>
	<script type="text/javascript">
		setTimeout(function(){
                     jQuery(document).ready(function(){
			var path=SITE_URL+'/members/password_setting_popup';
			TINY.box.show({url:path,width:512,height:512,close:true})
		});
              }, 15000);
	</script>
<?php
}?>

	<script type="text/javascript">
	function claimProfile() {
                     jQuery(document).ready(function(){
			var path=SITE_URL+'/members/password_setting_popup';
			TINY.box.show({url:path,width:512,height:512,close:true})
		});
            }
	</script>
<?php /**
<script type="text/javascript">

jQuery(document).ready(function(){
    window.onload=initOpen();
});

	function initOpen() {
		setTimeout(	function OpenPopUp(){
			jQuery.ajax({
				url:SITE_URL+'users/isOpenPopUp',
				type:'post',
				dataType:'json',
				success:function(res){
					if(res.value == 1) {
						register_popup();
					}
				}
			});	
		}, 240000);
	}

</script>
**/?>
<?php
if(isset($question_url) && $this->Session->read('Auth.User.id') ==''){?>

<?php
	echo($this->Form->hidden('question_url',array('value'=>$question_url,'id'=>'question_url')));	
?>	
	<script type="text/javascript">

		jQuery(document).ready(function(){
			var q_url = document.getElementById('question_url').value;
			loginpopup_question(q_url);
		});
	</script>
<?php
}?>

<?php
if(!$this->Session->read('Mentor.PasswordSet') && $this->Session->read('Auth.User.id') =='' && isset($OpenRegisterPopup)){?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			passwordSettingPopup();
		});
	</script>
<?php
}?>


<script type="text/javascript">
function twitterCallback2(twitters) {
  var statusHTML = [];
  for (var i=0; i<twitters.length; i++){
    var username = twitters[i].user.screen_name;
	
	if (twitters[i]['retweeted_status'] != null) {
    	twitters[i].text = "RT @"+twitters[i]['retweeted_status']['user']['screen_name']+" : " + twitters[i]['retweeted_status']['text'];
    }
    var status = twitters[i].text.replace(/((https?|s?ftp|ssh)\:\/\/[^"\s\<\>]*[^.,;'">\:\s\<\>\)\]\!])/g,
    function(url){
		return '<a href="'+url+'" target="_blank"><span>'+url+'</span></a>';
    }).replace(/\B@([_a-z0-9]+)/ig, function(reply) {
	    return  reply.charAt(0)+'<a href="https://twitter.com/'+reply.substring(1)+'" style="color:#EEEEEE;" target="_blank">'+reply.substring(1)+'</a>';
    });
    statusHTML.push('<p>'+status+'<br>');
  }
  statusHTML.push('</p>');
  document.getElementById('twitter_update_list').innerHTML = statusHTML.join('');
}
</script>

<div id="footer-wrapper">
	<div id="footer-box"  class="pagewidth">
		<div id="footer-top">
			<div id="footer-logo" style="text-align:center; margin-top:20px;">
				<?php echo($this->Html->link($this->Html->image('media/guild-footer1.png',array('alt'=>'Guild','style'=>'width:100px;height:103px;')),array('controller'=>'fronts','action'=>'index'),array('escape'=>false))); ?>
			</div>
			<div id="footer-about">
				<div id="left-box" style="width:40% !important;">
					<h2 style="color:#D03135; font-weight:bold; font-size:15px; font-family:ubuntu;">About</h2>
					<p><?php echo Configure::read('AboutFooterText');?></p>
					<p><span style="color:#EEEEEE !important;"><?php echo Configure::read('AboutFooterPhone');?></span><br/>
					<a href='mailto&#58;help@guild.im'><span>help@guild.im</span></a></p><!--VL 21-12 -->
				</div>
				<div id="right-box" >
					<h2 style="color:#D03135; font-weight:bold; font-size:15px; font-family:ubuntu;"><?php echo($this->Html->link('Tweets','https://twitter.com/GuildAlerts',array('target'=>'_blank'))); ?></h2>
					 <div id="twitter_update_list" >
					 </div>					
				</div>
			</div>
			<div id="footer-change">
				<h2 style="color:#D03135; font-weight:bold; font-size:15px; font-family:ubuntu;">Social</h2>
		        <?php $linkdinUrlFooter="https://www.linkedin.com/shareArticle?mini=true&url=https://guild.im&title=Guild&summary=https://guild.im GUILD is a community of experts, eager to share their expertise with others&source=https://guild.im"; ?>
				<p class="social-links">
                                   <span>
                                   
					<a  href="https://www.linkedin.com/company/guild-digital" title="Follow GUILD on LinkedIn" target="_blank" class="iconlinkedFooter" style="margin-bottom: 15px;padding-top:3px;"></a>
                                    <a  href="https://twitter.com/GuildAlerts" title="Follow GUILD on Twitter" target="_blank" class="icontwittFooter" rel="nofollow" style="margin-bottom: 15px;"></a>
	                             <a  href="https://plus.google.com/u/0/+MentorsguildLive" title="Follow GUILD on Google+" rel="publisher" target="_blank" class="icongoogleFooter" style="margin-bottom: 15px;"></a>
	                              </span>


				</p>	
			</div>
		</div>
		<div id="footer-bottom">
			<div id="privacy">
				<p><?php echo($this->Html->link('Privacy',array('controller'=>'fronts','action'=>'privacy'),array('style'=>"color:#777",'title'>'Privacy page'))); ?><span>|</span><?php echo($this->Html->link('Terms of service',array('controller'=>'fronts','action'=>'terms'),array('style'=>"color:#777",'title'>'Terms of service page'))); ?></p>
			</div>
			<div id="all-rights">
				<p>&copy; <?php echo date('Y'); ?> GUILD. All Rights Reserved</p>
			</div>
		</div>
	</div>
	<?php if(isset($_SESSION['User_type'])){
			if($this->Session->read('Auth.User.id') =='') {
				$this->Session->delete('User_type');
			} else {
		?>
			
		<a href="<?php echo SITE_URL?>members/switch_user/1">Switch to admin</a>
	<?php }
	}?>
</div>

<?php
if(isset($_SESSION['reqReschdule']) && $_SESSION['reqReschdule']=='user')
{
    unset($_SESSION['reqReschdule']); ?>
    <script type="text/javascript">loginpopup();</script>
<?php }
if(isset($_SESSION['menteeActEmail']) && $_SESSION['menteeActEmail']!=''):
?>
<script type="text/javascript">loginpopup();</script>			
<?php endif; 
if(isset($backreg)){?>
	<script type="text/javascript">	
		function reloadHome(){
				window.location.href = SiteUrl;
		}
		TINY.box.show({url:SiteUrl+'/users/register',width:500,fixed:false,closejs:function(){reloadHome()}});			
	</script>  	
	<?php
}
if(isset($account)){?>
	<script type="text/javascript">	
		function reloadHome(){
				window.location.href = SiteUrl;
		}
		TINY.box.show({url:SiteUrl+'/users/login',width:500,fixed:false,closejs:function(){reloadHome()}});			
	</script>  	
<?php
}

require_once('../webroot/TwitterAPIExchange.php');

/** Set access tokens here - see: https://dev.twitter.com/apps/ **/
$settings = array(
		'oauth_access_token' => "307588817-ri2kaoOZv7F5Ps2IWz0J2AMl3mn58aN91wGzSaeA",
		'oauth_access_token_secret' => "k5BQaVAAFrRPskC7SN8uunFnKj8L83pPHqpmwYpnswqVZ",
		'consumer_key' => "ZCBr3siWeVe889iD71gSCNwFh",
		'consumer_secret' => "dksVrAiUxUrBU4AQ1ISq5bY9ublSm8ZyftPBhk8euVSY8uNwsW"
);

$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
$getfield = '?screen_name=GuildAlerts&include_rts=true&count=2';
$requestMethod = 'GET';
$twitter = new TwitterAPIExchange($settings);
$twitters = $twitter->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest();

echo '<script type="text/javascript"> twitterCallback2('.$twitters.'); </script>';
  		
?>
<script type="text/javascript">
jQuery(document).ready(function(){

    flashClass = '';
    flashClass = jQuery('#flashMessage').attr('class');
    if(flashClass!='notclass')
    {
       jQuery('#flashMessage').fadeOut(7000);
    }
	Placeholders.init({
		live: true, //Apply to future and modified elements too
		//hideOnFocus: true //Hide the placeholder when the element receives focus
	});

    /*jQuery(function() {
    	jQuery("a").on('click','a',function(e){
	        var url = jQuery(this).attr("href");
	        var target = jQuery(this).attr("target");
	        // Console logs shows the domain name of the link being clicked and the current window
	        // console.log('e.currentTarget.host: ' + e.currentTarget.host);
	        // console.log('window.location.host: ' + window.location.host);
	        // If the domains names are different, it assumes it is an external link
	        // Be careful with this if you use subdomains
	        
	        if (e.currentTarget.host == '')
		    	return true;
		    
	        if (e.currentTarget.host != window.location.host) {
	             //console.log('external link click');
	            // Outbound link! Fires the Google tracker code.
	           // _gat._getTrackerByName()._trackEvent("Outbound Links", e.currentTarget.host.replace(':80',''), url, 0);
	            // Checks to see if the ctrl or command key is held down
	            // which could indicate the link is being opened in a new tab
	            if (e.metaKey || e.ctrlKey) {
	                //console.log('ctrl or meta key pressed');
	                var newtab = true;
	            }
	            // If it is not a new tab, we need to delay the loading
	            // of the new link for a just a second in order to give the
	            // Google track event time to fully fire
	            if (!newtab) {
	                //console.log('default prevented');
	                e.preventDefault();
	                //console.log('loading link after brief timeout');
	                if(target == '_blank')
	                	setTimeout(function(){window.open(url, '_blank')}, 100);
	                else
	                	setTimeout('document.location = "' + url + '"', 100);
	            }
	        }
	        else {
	            //console.log('internal link click');
	        }
	    });
	});*/
});
</script>
<script type="text/javascript">

function relative_time(time_value) {
  var values = time_value.split(" ");
  time_value = values[1] + " " + values[2] + " " + values[5] + " " + values[3];
  var parsed_date = new Date();
  parsed_date.setTime(Date.parse(time_value)); 
  var months = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
     'Sep', 'Oct', 'Nov', 'Dec');
  var m = parsed_date.getMonth();
  var postedAt = '';
  postedAt = months[m];
  postedAt += " "+ parsed_date.getDate();
  postedAt += ","
  postedAt += " "+ parsed_date.getFullYear();
  return postedAt;
}                                                 
</script>




<script type="text/javascript">
//function geolocate(timezone, cityPrecision, objectVar).
//If you rename your object name, you must rename 'visitorGeolocation' in the function
var visitorGeolocation = new geolocate(false, true, 'visitorGeolocation');

    
//Check for cookie and run a callback function to execute after geolocation is read either from cookie or IPInfoDB API
var callback = function(){
                //alert('Visitor country code : ' + visitorGeolocation.getField('regionName'))
 
               };
visitorGeolocation.checkcookie(callback);
 var conntry_code 		= visitorGeolocation.getField('countryCode');
  var geoip_city 	 	= visitorGeolocation.getField('cityName');	
  var geoip_region_name 	= visitorGeolocation.getField('regionName');
  var geoip_ipAddress 	= visitorGeolocation.getField('ipAddress');
  var city = '';
  var state = '';
     jQuery(document).ready(function(){

           jQuery.ajax({
			url:SITE_URL+'/fronts/getStateCode',
			type:'post',
			dataType:'json',
			data:'state_name='+geoip_region_name + '&country_code=' + conntry_code + '&city=' + geoip_city,
			success:function(res){
				if(res.value !=''){
					state_name = res.value;	
					if(conntry_code =='US'){
                                  
						jQuery(".newyork").val(geoip_city+', '+state_name);
                                                city = geoip_city;
                                                state = state_name;
					}else{
                                       
						jQuery(".newyork").val('International');
					}					
				}else{
					if(conntry_code =='US'){
                                     
						jQuery(".newyork").val(geoip_city+', '+geoip_region_name);
                                                city = geoip_city;
                                                state = state_name;
					}else{
                                      
						jQuery(".newyork").val('International');
					}
				}
			}
		});
              });
</script>


<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-38592072-2', 'auto');
  ga('require', 'linkid', 'linkid.js');
  ga('require', 'displayfeatures');
  ga('send', 'pageview');

</script>  
    <script type="application/ld+json">
{ "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "GUILD",
  "url" : "https://www.guild.im",
  
  "sameAs" : [ "https://www.linkedin.com/company/guild-digital",
    "https://www.twitter.com/guildalerts",
    "https://business.facebook.com/Guild.Digital",
    "https://plus.google.com/u/0/+MentorsguildLive"] 
}
</script>   
    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "url": "https://www.guild.im",
      "logo": "https://www.guild.im/imgs/mg_monogram1.png"
    }
    </script>  
    <script type="application/ld+json">
    {  "@context" : "http://schema.org",
       "@type" : "Organization",
       "name" : "GUILD",
       "alternateName" : "guild.im",
       "url" : "https://www.guild.im"
    }
    </script> 

    <script type="application/ld+json">
    {  "@context" : "http://schema.org",
       "@type" : "ProfessionalService",
       "name" : "GUILD",
       "alternateName" : "guild.im",
       "url" : "https://www.guild.im",
    "image":"https://www.guild.im/imgs/mg_monogram1.png",
    "address": "Manoa Innovation Center 2800 Woodlawn Drive,Honolulu, Hawaii",
    "telephone": "+1 866-511-1898"

    }
    </script> 
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "LocalBusiness",
  "address": {
    "@type": "PostalAddress",
    "addressLocality": "Manoa Innovation Center",
    "addressRegion": "Honolulu, Hawaii",
    "streetAddress": "2800 Woodlawn Drive"

  },
  "description": "GUILD offers business leaders proven expertise to solve hard business problems, as a cost-effective alternative to internal staffing or traditional consulting.",
  "name": "GUILD Business Consulting",
  "telephone": "+1 866-511-1898",
   "image":"https://www.guild.im/imgs/mg_monogram1.png"
}
</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "GUILD",
  "description": "GUILD offers business leaders proven expertise to solve hard business problems, as a cost-effective alternative to internal staffing or traditional consulting.",
  "telephone": "+1 866-511-1898"
  }
</script> 
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "address": {
    "@type": "PostalAddress",
    "addressLocality": "Honolulu, Hawaii",
    "postalCode": "96822",
    "streetAddress": "Manoa Innovation Center 2800 Woodlawn Drive"
  },
  "email": "help(at)guild.im",
  "faxNumber": "+1 866-511-1898",
  "name": "GUILD",
  "telephone": "+1 866-511-1898"
}
</script>


<!-- Facebook Pixel Code -->

     <script src="//cdn.optimizely.com/js/2182070636.js"></script>
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '944379115619828');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none" alt="FB-Page-View"
src="https://www.facebook.com/tr?id=944379115619828&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<script type="text/javascript"> 
var google_tag_params = { 
dynx_itemid: 'REPLACE_WITH_VALUE', 
dynx_itemid2: 'REPLACE_WITH_VALUE', 
dynx_pagetype: 'REPLACE_WITH_VALUE', 
dynx_totalvalue: 'REPLACE_WITH_VALUE', 
}; 
</script> 
<script type="text/javascript"> 
/* <![CDATA[ */ 
var google_conversion_id = 933439752; 
var google_custom_params = window.google_tag_params; 
var google_remarketing_only = true; 
/* ]]> */ 
</script> 
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"> 
</script> 
<noscript> 
<div style="display:inline;"> 
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/933439752/?value=0&amp;guid=ON&amp;script=0"/> 
</div> 
</noscript>
