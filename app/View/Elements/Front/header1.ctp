        <script type="text/javascript">
            jQuery.noConflict();
            var SiteUrl = "<?php echo SITE_URL; ?>";
            var SITE_URL = "<?php echo SITE_URL; ?>";	
	     var loggedUserId = '';
	     var memberId = '';
	     var mentee_url = '';

        </script>

<?php 

if($this->Session->read('Auth.User.id') !='' && $this->Session->read('Auth.User.id') !=1){
	 ?>
	<script type="text/javascript">
	loggedUserId = <?php echo $this->Session->read('Auth.User.id'); ?>
       </script>

<?php } ?>

<?php
				       $userPhoto = $this->General->FindUserImage($this->Session->read('Auth.User.id'));
                                       if($userPhoto !=''){
						if($this->Session->read('Auth.User.role_id')==2){
							$image_path = SITE_URL.'/img/members/profile_images/'.$this->Session->read('Auth.User.id').'/small/'.$userPhoto;
						}else{
							$image_path = SITE_URL.'/img/clients/profile_images/'.$this->Session->read('Auth.User.id').'/small/'.$userPhoto;
						}						
					}else{
						$image_path = SITE_URL.'media/small_Profile.png';
					}
?>

	<!-- Fixed navbar -->
	<nav class="navbar navbar-default navbar-fixed-top header-align">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
					<a class="navbar-brand" href="<?php echo(SITE_URL)?>"><img src="<?php echo(SITE_URL)?>/bala/images/logo.svg"></a>
				
            <?php if($this->Session->read('Auth.User.id') ==''){?>
				<ul class="list-unstyled shownav">
					<li>
						Call 1-866-511-1898
					</li>
					<li>
						<a href="">CONSULTANT MEMBERSHIP</a>
					</li>
				</ul>
           <?php }?>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
            <?php if($this->Session->read('Auth.User.id') ==''){?>
				<ul class="list-unstyled nav-top">
					<li>
						Call 1-866-511-1898
					</li>
					<li>
						<a href="">CONSULTANT MEMBERSHIP</a>
					</li>
				</ul>
              <?php }?>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="<?php echo(SITE_URL)?>">Search</a>
					</li>
					<li class="mhide">
						|
					</li>
                <?php if($this->Session->read('Auth.User.role_id')==2){ ?> 
					<li>
						<a href="<?php echo(SITE_URL)?>qanda">Share Your Insight</a>
					</li>
					<li class="mhide">
						|
					</li>
					<li>
						<a href="<?php echo(SITE_URL)?>project/project_proposals">Project Proposals</a>
					</li>
					<li class="mhide">
						|
					</li>
					<li>
						<a href="<?php echo(SITE_URL)?>members/advantage">Member Advantage</a>
					</li>
					<li class="mhide">
						|
					</li>
                 <?php } else if($this->Session->read('Auth.User.role_id')==3){ ?>  
					<li>
						<a href="<?php echo(SITE_URL)?>members/case_studies">Case Studies</a>
					</li>
					<li class="mhide">
						|
					</li>
					<li>
						<a href="<?php echo(SITE_URL)?>qanda">Ask An Expert</a>
					</li>
					<li class="mhide">
						|
					</li>
					<li>
						<a href="<?php echo(SITE_URL)?>project/project_proposals">Project Proposals</a>
					</li>
					<li class="mhide">
						|
					</li>
                  <?php }else{?>
					<li>
						<a href="<?php echo(SITE_URL)?>members/case_studies">Case studies</a>
					</li>
					<li class="mhide">
						|
					</li>
					<li>
						<a href="<?php echo(SITE_URL)?>qanda">Ask An Expert</a>
					</li>
					<li class="mhide">
						|
					</li>
					<li>
						<a href="<?php echo(SITE_URL)?>project/project_proposals">Project Proposals</a>
					</li>
					<li class="mhide">
						|
					</li>
                    <?php }?>
                    <?php if($this->Session->read('Auth.User.id') ==''){?>
					<li>
						<a href="#" class="mymodal" data-toggle="modal" data-target="#myModal">Login</a>
					</li>
                     <?php }else if($this->Session->read('Auth.User.role_id')==2){?>
					<li>
						<div class="dropdown profile-dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<div class="profile-img">
									<img src="<?php echo($image_path);?>">
								</div>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                               <?php if ($this->Session->read('Auth.User.access_specifier') =='publish'){?>             
								<li><?php echo($this->Html->link('Edit Profile',array('controller'=>'users','action'=>'edit_account'),array('alt'=>'edit_account')));?></li>
								<li><?php echo($this->Html->link('View Profile',array('controller'=>'users','action'=>'my_account'),array('alt'=>'my_account')));?></li>
								<li><?php echo($this->Html->link('My Resources',array('controller'=>'members','action'=>'resources'),array('alt'=>'My resources')));?></li>
								<li><?php echo($this->Html->link('My Account',array('controller'=>'members','action'=>'account_Setting'),array('alt'=>'My resources')));?></li>
								
                                                <?php }else{?>
								<li><?php echo($this->Html->link('My profile',array('controller'=>'members','action'=>'member_full_profile'),array('alt'=>'full')));  ?></li>
                                                <?php }?>
                                                              <li><?php echo($this->Html->link('Sign Out',array('controller'=>'users','action'=>'logout'),array('alt'=>'Sign Out')));?></li>
   
							</ul>
						</div>
					</li>

                    <?php }else if($this->Session->read('Auth.User.role_id')==3){?>

					<li>
						<div class="dropdown profile-dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<div class="profile-img">
									<img src="<?php echo($image_path);?>">
								</div>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                               <?php if ($this->Session->read('Auth.User.access_specifier') =='publish'){?>             
								<li><?php echo($this->Html->link('Edit Profile',array('controller'=>'clients','action'=>'edit_account'),array('alt'=>'edit_account')));?></li>
								<li><?php echo($this->Html->link('View Profile',array('controller'=>'clients','action'=>'my_account'),array('alt'=>'my_account')));?></li>
								<li><?php echo($this->Html->link('My Account',array('controller'=>'clients','action'=>'account_Setting'),array('alt'=>'My resources')));?></li>
								<li><?php echo($this->Html->link('Invite A Colleague','javascript:client_invitation_popup();',array('alt'=>'Client Invitation'))); ?></li>
								
                                                <?php }else{?>
								<li><?php echo($this->Html->link('My profile',array('controller'=>'users','action'=>'registration_step2'),array('alt'=>'full')));  ?></li>
                                                <?php }?>
                                                              <li><?php echo($this->Html->link('Sign Out',array('controller'=>'users','action'=>'logout'),array('alt'=>'Sign Out')));?></li>
   
							</ul>
						</div>
					</li>

                       <?php }?>

            

			
                 </ul>
			</div>
		</div>
	</nav>
	<!-- banner section -->


	<!-- login modal -->
	<div class="modal loginModal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title login-title" id="myModalLabel">
						Login
					</h4>
					<h4 class="modal-title forgetPassword-title hide" id="myModalLabel">
						Reset password
					</h4>
					<h4 class="modal-title Register-title hide" id="myModalLabel">
						Register as a client
					</h4>
				</div>
				<div class="modal-body">
					<div class="login-error hide">
						<label>Invalid password and username</label>
					</div>
					<!-- login-form -->
					<div class="login-form">
						<?php echo($this->Form->create('User',array('url'=>array('controller'=>'users','action'=>'login'),'id'=>'newloginForm'))); ?>
							<div class="form-group">
								<input type="text" name="email" id="email" class="form-control" placeholder="Enter Email Id" name="data[User][username]">
							</div>
							<div class="form-group">
								<input type="password" name="password" id="password" class="form-control" placeholder="Enter Your Password" name="data[User][password]">
							</div>
							<div class="forget-label">
								<label>
									Forgot Password ?
								</label>
							</div>
							<button class="banner-getstarted btn btn-default get-started">
								LOGIN
							</button>
							<div class="need-account text-center">
								<label>
									Need an account?
									<span data-toggle="modal" data-target=".bs-example-modal-lg">Register here</span>
								</label>
							</div>
						<?php echo($this->Form->end()); ?>
						<label class="or">Or Login With</label>
						<div class="login-with text-center">
							<a href="javascript:void(0); javascript:linkedin();">
								<img src="<?php echo(SITE_URL)?>/bala/images/linkedin.png">
							</a>
						</div>
					</div>
					<!-- forget password -->
					<div class="login-form forgetPassword-form hide">
						<form id="forgetProceess">
							<div class="form-group">
								<label class="forgetpassword-desc">
									Submit your email address and we'll send you a link to reset your password.
								</label>
								<input type="email" name="email" id="email" class="form-control" placeholder="Enter Email Id">
							</div>
							<button class="banner-getstarted btn btn-default get-started">
								LOGIN
							</button>
						</form>
					</div>
					<!-- client signup-form -->
					<div class="login-form signup-form hide">
						<form id="signupProceess">
							<div class="form-group">
								<input type="name" class="form-control" name="name" id="name" placeholder="Enter Your Full Name">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="email" id="email" placeholder="Enter Your Email Id">
							</div>
							<div class="form-group">
								<input type="password" name="password" id="password" class="form-control" placeholder="Enter Your Password">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="zipcode" id="zipcode" placeholder="Enter Your Zip Code">
							</div>
							<button class="banner-getstarted btn btn-default get-started">
								REGISTER
							</button>
							<div class="need-accounts loginHere text-center">
								<label>
									Already have an account?
									<span>Login here</span>
								</label>
							</div>
						</form>
						<label class="or">Or Login With</label>
						<div class="login-with text-center">
							<a href="javascript:void(0); javascript:linkedin();">
								<img src="<?php echo(SITE_URL)?>/bala/images/linkedin.png">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- signup process -->
	<div class="modal fade registerModel bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">
						<label>GUILd</label> is changing the way organizations leverage outside expertise. We enable our clients to address complex problems and tap fleeting opportunities, like never before.
					</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="consultant-section">
								<h1>Consultants</h1>
								<p>We invite experts from various business domains to offer fresh insights, hands-on expertise and coaching to our clients.</p>
								<p>All engagements are at the Consultant's discretion.</p>
								<a href="<?php echo SITE_URL;?>members/eligibility" class="banner-getstarted btn btn-default get-started">
									Apply Now
								</a>
							</div>
						</div>
						<div class="col-md-6">
							<div class="consultant-section">
								<h1>Clients</h1>
								<p>Our clients are the managers and directors of Small to Mid-sized businesses, Corporations and Non-profits.</p>
								<p>They leverage us for Growth, Process Improvements and Organization Development.</p>
								<button class="banner-getstarted register-client btn btn-default get-started">
									Get Started
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>