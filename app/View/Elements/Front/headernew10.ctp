

<?php 

if($this->Session->read('Auth.User.id') !='' && $this->Session->read('Auth.User.id') !=1){
	 ?>
	<script type="text/javascript">
	loggedUserId = <?php echo $this->Session->read('Auth.User.id'); ?>
       </script>

<?php } ?>
<?php 
if($this->Session->read('Auth.User.role_id') == 2 ){
	?>
	<script type="text/javascript">
		memberId = <?php echo $this->Session->read('Auth.User.id'); ?>
	</script>
<?php } ?>
<?php
				       $userPhoto = $this->General->FindUserImage($this->Session->read('Auth.User.id'));
                                       if($userPhoto !=''){
						if($this->Session->read('Auth.User.role_id')==2){
							$image_path = SITE_URL.'/img/members/profile_images/'.$this->Session->read('Auth.User.id').'/'.$userPhoto;
						}else{
							$image_path = SITE_URL.'/img/clients/profile_images/'.$this->Session->read('Auth.User.id').'/'.$userPhoto;
						}						
					}else{
						$image_path = SITE_URL.'img/media/small_profile.png';
					}
?>

	<!-- Fixed navbar -->
	<nav class="navbar navbar-default navbar-fixed-top header-align">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
					<a class="navbar-brand" href="<?php echo(SITE_URL)?>"><img src="<?php echo(SITE_URL)?>Guild_New_Logo/GUILD-Logo.svg"></a>
				
            <?php if($this->Session->read('Auth.User.id') ==''){?>
				<ul class="list-unstyled shownav">
					<li>
						Call 1-866-511-1898
					</li>
					<li>
						<a href="<?php echo(SITE_URL)?>members/advantage">CONSULTANT MEMBERSHIP</a>
					</li>
				</ul>
           <?php }?>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
            <?php if($this->Session->read('Auth.User.id') ==''){?>
				<ul class="list-unstyled nav-top">
					<li>
						Call 1-866-511-1898
					</li>
					<li>
						<a href="<?php echo(SITE_URL)?>members/advantage">CONSULTANT MEMBERSHIP</a>
					</li>
				</ul>
              <?php }?>
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="<?php echo(SITE_URL)?>project/create">Post a project</a>
					</li>
					<li class="mhide">
						|
					</li>
                <?php if($this->Session->read('Auth.User.role_id')==2){ ?> 
					<li>
						<a href="<?php echo(SITE_URL)?>qanda">Share your expertise</a>
					</li>
					<li class="mhide">
						|
					</li>
                                           <li>
							<a href="#" title="Resources"  id="resources">Resources </a><img src="<?php echo(SITE_URL)?>images/icon_sorting_dark.png" style ="width:16px;height:10px;" alt ="dropdown arrow" id="projectlist"></img> 
						
                                                <div id="dropdownbox">
                                                 	<table >
		            				<tr style=height:30px;">
                                                 <td > <a href="<?php echo(SITE_URL)?>expertise_search" alt="Search for expertise">Search for expertise</a> </td>
                                                 </tr> 
                                                  <tr style=height:30px;">
                                                 <td> <?php echo($this->Html->link('Proposals',array('controller'=>'project','action'=>'project_proposals'),array('alt'=>'Project Proposals','style'=>'font-size:16px;')));  ?> </td>
                                                 </tr>
                                                  <tr style=height:30px;">
                                                 <td > <?php echo($this->Html->link('Case studies',array('controller'=>'members','action'=>'case_studies'),array('alt'=>'Case Studies','style'=>'font-size:16px;')));  ?> </td>
                                                 </tr> 
 
                                                </table >
		            				
                                                </div>
                                            </li>
					<li class="mhide">
						|
					</li>
					<li>
						<a href="<?php echo(SITE_URL)?>members/advantage">Member advantage</a>
					</li>
					<li class="mhide">
						|
					</li>
                 <?php } else if($this->Session->read('Auth.User.role_id')==3){ ?>  
                                           <li>
							<a href="#" title="Resources"  id="resources">Resources </a><img src="<?php echo(SITE_URL)?>images/icon_sorting_dark.png" style ="width:16px;height:10px;" alt ="dropdown arrow" id="projectlist"></img> 
						
                                                <div id="dropdownbox">
                                                 	<table >
		            				<tr style=height:30px;">
                                                 <td > <a href="<?php echo(SITE_URL)?>expertise_search" alt="Search for expertise">Search for expertise</a>  </td>
                                                 </tr> 
		            			<tr style=height:30px;">
                                                 <td ><a href="<?php echo(SITE_URL)?>qanda">Ask an expert</a></td>
                                                  <tr style=height:30px;">
                                                 <td> <?php echo($this->Html->link('Proposals',array('controller'=>'project','action'=>'project_proposals'),array('alt'=>'Project Proposals','style'=>'font-size:16px;')));  ?> </td>
                                                 </tr>
                                                  
                                                  <tr style=height:30px;">
                                                 <td > <?php echo($this->Html->link('Case studies',array('controller'=>'members','action'=>'case_studies'),array('alt'=>'Case Studies','style'=>'font-size:16px;')));  ?> </td>
                                                 </tr> 

 
                                                </table >
		            				
                                                </div>
                                           </li>
   
					<li class="mhide">
						|
					</li>

                  <?php }else{?>



                                           <li>
							<a href="#" title="Resources"  id="resources">Resources </a><img src="<?php echo(SITE_URL)?>images/icon_sorting_dark.png" style ="width:16px;height:10px;" alt ="dropdown arrow" id="projectlist"></img> 
						
                                                <div id="dropdownbox" style="display:none;">
                                                 	<table >

		            				<tr style=height:30px;">
                                                 <td > <a href="<?php echo(SITE_URL)?>expertise_search" alt="Search for expertise">Search for expertise</a>  </td>
                                                 </tr> 
		            				<tr style=height:30px;">
                                                 <td > <a href="<?php echo(SITE_URL)?>qanda">Ask an expert</a> </td>
                                                 </tr> 

                                                  <tr style=height:30px;">
                                                 <td> <?php echo($this->Html->link('Proposals',array('controller'=>'project','action'=>'project_proposals'),array('alt'=>'Project Proposals','style'=>'font-size:16px;')));  ?> </td>
                                                 </tr>
                                                  <tr style=height:30px;">
                                                 <td > <?php echo($this->Html->link('Case studies',array('controller'=>'members','action'=>'case_studies'),array('alt'=>'Case Studies','style'=>'font-size:16px;')));  ?> </td>
                                                 </tr> 
 
                                                </table >
		            				
                                                </div>
                                              </li>
                                          
					<li class="mhide">
						|
					</li>


                    <?php }?>
                    <?php if($this->Session->read('Auth.User.id') ==''){?>
					<li>
						<a href="#" class="mymodal" data-toggle="modal" data-target="#signin-Modal">Login</a>
					</li>
                     <?php }else if($this->Session->read('Auth.User.role_id')==2){?>
					<li class="login">
						<div class="dropdown profile-dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<div id="headerProfileImage" class="profile-img">
									<img src="<?php echo($image_path);?>">
								</div>
								<span class="name"><?php echo($this->Session->read('Auth.User.url_key'));?></span>
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                               <?php if ($this->Session->read('Auth.User.access_specifier') =='publish'){?>             
								<li><?php echo($this->Html->link('Edit profile',array('controller'=>'users','action'=>'profile_edit'),array('alt'=>'edit_account')));?></li>
								
                                                                <li><a href="<?php echo(SITE_URL);?><?php echo($this->Session->read('Auth.User.url_key'));?>" alt='View Profile'>View profile</a></li>
                                                                <li><a href="<?php echo(SITE_URL);?>project/projectlistmember" alt='My Projects'>My projects</a></li>
								<li><?php echo($this->Html->link('My account',array('controller'=>'members','action'=>'account_setting'),array('alt'=>'My resources')));?></li>
								
                                                <?php }else{?>
								<li><?php echo($this->Html->link('My profile',array('controller'=>'members','action'=>'member_full_profile'),array('alt'=>'full')));  ?></li>
                                                <?php }?>
                                                              <li><?php echo($this->Html->link('Sign out',array('controller'=>'users','action'=>'logout'),array('alt'=>'Sign Out')));?></li>
   
							</ul>
						</div>
					</li>

                    <?php }else if($this->Session->read('Auth.User.role_id')==3){?>

					<li class="login">
						<div class="dropdown profile-dropdown">
							<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
								<div id="headerProfileImage" class="profile-img">
									<img src="<?php echo($image_path);?>">
								</div>
								
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                               <?php if ($this->Session->read('Auth.User.access_specifier') =='publish'){?>             
								<li><?php echo($this->Html->link('My profile',array('controller'=>'clients','action'=>'my_account'),array('alt'=>'My Profile')));?></li>
								<li><a href="<?php echo(SITE_URL);?>clients/my_account#projectlist">My projects</a></li>
								<li><?php echo($this->Html->link('Invite a colleague','#',array('data-toggle'=>'modal','data-target'=>'.inviteacolleague','alt'=>'Client Invitation'))); ?></li>

								
                                                <?php }else{?>
								<li><?php echo($this->Html->link('My profile',array('controller'=>'users','action'=>'registration_step2'),array('alt'=>'full')));  ?></li>
                                                <?php }?>
                                                              <li><?php echo($this->Html->link('Sign out',array('controller'=>'users','action'=>'logout'),array('alt'=>'Sign Out')));?></li>
   
							</ul>
						</div>
					</li>

                       <?php }?>

            

			
                 </ul>
			</div>
		</div>
	</nav>
	<!-- banner section -->


	<!-- login modal -->
	<div class="modal loginModal fade" id="signin-Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_icon"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title login-title" id="myModalLabel">
						Login
					</h4>
				</div>
				<div class="modal-body">
					<!-- login-form -->
					<div class="login-form">
						<div class="login-with text-center">
							<a href="javascript:void(0); javascript:linkedin();" id="linkedinclick">
								<img src="<?php echo(SITE_URL)?>yogesh_new1/images/linkedin.png">
							</a>
						</div>
						<label class="or">Or Login With</label>
						<div>

							<div class="form-group">
								<input type="text" name="data[User][username]" id="email" value="<?php echo($username);?>" class="form-control" placeholder="Email">
                                                                <div id="LoginEmailErr"  class="errormsg"></div>
							</div>
							<div class="form-group">
								<input type="password" name="data[User][password]" id="password" value="<?php echo($password);?>" class="form-control password_icon" placeholder="Password">
							</div>
							<div class="forget-label text-center">
							
							<button  class="banner-getstarted btn btn-default get-started" id="loginButtonclick">
								LOGIN
							</button>
								<label style="display:block" class="mymodal" data-dismiss="modal" data-toggle="modal" data-target="#resetpassword-Modal">
									Forgot Password ?
								</label>
							</div>
							<div class="need-account text-center">
								<label>
									Need an account?
									<span data-toggle="modal" data-dismiss="modal" data-target="#signup-process">Register here</span>
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Reset password modal -->
	<div class="modal loginModal fade" id="resetpassword-Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title forgetPassword-title" id="myModalLabel">
						Reset password
					</h4>
				</div>
				<div class="modal-body">
					<!-- forget password -->
					<div class="login-form forgetPassword-form">
						<form id="forgetProceess" method="post" action="<?php echo SITE_URL;?>users/forgot">
							<div class="form-group">
								<label class="forgetpassword-desc">
									Submit your email address and we'll send you a link to reset your password.
								</label>

								<input type="text" name="data[User][username]" id="email" class="form-control" placeholder="Email">
                                                                <div id="ForgotEmailErr" class="errormsg"></div>
							</div>
							<div class="text-center">
							<button class="banner-getstarted btn btn-default get-started" id="forgotbutton">
								SUBMIT
							</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- signup modal -->
	<div class="modal loginModal fade" id="signup-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title Register-title" id="myModalLabel">
						Register as a client
					</h4>
				</div>
				<div class="modal-body">
					<!-- client signup-form -->
					<div class="login-form signup-form">
						<form id="signupProceess" method="post" action="<?php echo SITE_URL;?>pricing/pricing_auth_popup">
			                      <?php if(isset($this->data['User']['id']) && isset($this->data['UserReference']['id']) && $id!=0){
				                    echo($this->Form->hidden('User.id',array('id'=>'popupId','div'=>false,'label'=>false)));
				                    echo($this->Form->hidden('UserReference.id',array('div'=>false,'label'=>false)));
			
			                      }?>
							<div class="login-with text-center">
								<a href="javascript:void(0); javascript:linkedin();">
									<img src="<?php echo(SITE_URL)?>yogesh_new1/images/linkedin.png">
								</a>
							</div>
							<label class="or">Or Register using email id</label>

							<div class="form-group">
								<input type="name" class="form-control" name="data[UserReference][first_name]" id="fname" placeholder="First Name">
                                                                <span class="errormsg" id="firstNameErr"></span>
							</div>
							<div class="form-group">
								<input type="name" class="form-control" name="data[UserReference][last_name]" id="lname" placeholder="Last Name">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="data[User][username]" id="email" placeholder="Email">
                                                                <div id="UserEmailErr" class="errormsg"></div>
							</div>
							<div class="form-group">
								<input type="password" name="data[User][password2]" id="password" class="form-control password_icon" placeholder="Password">
                                                                <div id="PassLengtherr" class="errormsg"></div>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="data[UserReference][zipcode]" id="zipcode" placeholder="Zip Code">
                                                                <div id="zipError" class="errormsg"></div>
							</div>
							<div class="text-center">
							<button style="margin-bottom:10px" class="banner-getstarted btn btn-default get-started" id="registerbutton">
								REGISTER
							</button>
							</div>
							<div class="need-accounts loginHere text-center">
								<label>
									Already have an account?
									<span class="mymodal" data-dismiss="modal" data-toggle="modal" data-target="#signin-Modal">Login here</span>
								</label>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- signup process -->
	<div class="modal fade registerModel bs-example-modal-lg" id="signup-process" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">
						<label>GUILD</label> is changing the way organizations leverage outside expertise. We enable our clients to address complex problems and tap fleeting opportunities, like never before.
					</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="consultant-section">
								<h1>Consultants</h1>
								<p>We invite experts from various business domains to offer fresh insights, hands-on expertise and coaching to our clients.</p>
								<p>All engagements are at the Consultant's discretion.</p>
								<a class="banner-getstarted btn btn-default get-started" href="<?php echo(SITE_URL)?>members/eligibility">
									Apply Now
								</a>
							</div>
						</div>
						<div class="col-md-6">
							<div class="consultant-section">
								<h1>Clients</h1>
								<p>Our clients are the managers and directors of Small to Mid-sized businesses, Corporations and Non-profits.</p>
								<p>They leverage us for Growth, Process Improvements and Organization Development.</p>
								<button class="banner-getstarted register-client btn btn-default get-started" data-toggle="modal" data-dismiss="modal" data-target="#signup-modal">
									Get Started
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<script type="text/javascript">
jQuery(document).ready(function(){

jQuery("#projectlist").on('click', function(){
/*var box = document.getElementById("dropdownbox");
    if(box.style.display=="block"){
    document.getElementById("dropdownbox").style.display="none";
    }else{
    document.getElementById("dropdownbox").style.display="block";
   }*/
   jQuery('#dropdownbox').toggle();
});

jQuery("#resources").on('click', function(){

   jQuery('#dropdownbox').toggle();
});


	/*jQuery("#resources").mouseenter(function() {
		document.getElementById("dropdownbox").style.display="block";
	});

	jQuery("#dropdownbox, #resources").mouseleave(function() {
		document.getElementById("dropdownbox").style.display="none";


	});*/
	

	$("#resources").hover(function() {
		document.getElementById("dropdownbox").style.display="block";
	});


jQuery(document).mouseup(function (e)
{
	var container = jQuery('#dropdownbox, #projectlist');	
	var containerTarget = jQuery('#dropdownbox');	

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        containerTarget.hide();
    }
});

jQuery(document).mouseup(function (e)
{
	var container = jQuery('#dropdownbox, #presources');	
	var containerTarget = jQuery('#dropdownbox');	

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        containerTarget.hide();
    }
});

jQuery("#password").keyup(function(event){
    if(event.keyCode == 13){
        $("#loginButtonclick").click();
    }
});

});
 </script>
