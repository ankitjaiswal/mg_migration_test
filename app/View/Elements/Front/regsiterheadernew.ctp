<?php 

if($this->Session->read('Auth.User.id') !='' && $this->Session->read('Auth.User.id') !=1){
	 ?>
	<script type="text/javascript">
	loggedUserId = <?php echo $this->Session->read('Auth.User.id'); ?>
       </script>

<?php } ?>
<?php 
if($this->Session->read('Auth.User.role_id') == 2 ){
	?>
	<script type="text/javascript">
		memberId = <?php echo $this->Session->read('Auth.User.id'); ?>
	</script>
<?php } ?>
<nav class="navbar navbar-default navbar-fixed-top header-align website">
	<div class="container">
		<div class="navbar-header">
			<a class="navbar-brand" href="<?php echo(SITE_URL)?>"><img src="<?php echo(SITE_URL)?>Guild_New_Logo/GUILD-Logo.svg"></a>
			<ul class="list-unstyled shownav header-show">

			</ul>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="list-unstyled nav-top website-header">

			</ul>
		</div>
	</div>
</nav>

	<!-- login modal -->
	<div class="modal loginModal fade" id="signin-Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_icon"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title login-title" id="myModalLabel">
						Login
					</h4>
				</div>
				<div class="modal-body">
					<!-- login-form -->
					<div class="login-form">
						<div class="login-with text-center">
							<a href="javascript:void(0); javascript:linkedin();" id="linkedinclick">
								<img src="<?php echo(SITE_URL)?>yogesh_new1/images/linkedin.png">
							</a>
						</div>
						<label class="or">Or Login With</label>
						<div>

							<div class="form-group">
								<input type="text" name="data[User][username]" id="email" value="<?php echo($username);?>" class="form-control" placeholder="Email">
                                                                <div id="LoginEmailErr"  class="errormsg"></div>
							</div>
							<div class="form-group">
								<input type="password" name="data[User][password]" id="password" value="<?php echo($password);?>" class="form-control password_icon" placeholder="Password">
							</div>
							<div class="forget-label">
								<label class="mymodal" data-dismiss="modal" data-toggle="modal" data-target="#resetpassword-Modal">
									Forgot Password ?
								</label>
							</div>
							<button class="banner-getstarted btn btn-default get-started" id="loginButtonclick">
								LOGIN
							</button>
							<div class="need-account text-center">
								<label>
									Need an account?
									<span data-toggle="modal" data-dismiss="modal" data-target="#signup-process">Register here</span>
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Reset password modal -->
	<div class="modal loginModal fade" id="resetpassword-Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title forgetPassword-title" id="myModalLabel">
						Reset password
					</h4>
				</div>
				<div class="modal-body">
					<!-- forget password -->
					<div class="login-form forgetPassword-form">
						<form id="forgetProceess" method="post" action="<?php echo SITE_URL;?>users/forgot">
							<div class="form-group">
								<label class="forgetpassword-desc">
									Submit your email address and we'll send you a link to reset your password.
								</label>

								<input type="text" name="data[User][username]" id="email" class="form-control" placeholder="Email">
                                                                <div id="ForgotEmailErr" class="errormsg"></div>
							</div>
							<button class="banner-getstarted btn btn-default get-started" id="forgotbutton">
								SUBMIT
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- signup modal -->
	<div class="modal loginModal fade" id="signup-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title Register-title" id="myModalLabel">
						Register as a client
					</h4>
				</div>
				<div class="modal-body">
					<!-- client signup-form -->
					<div class="login-form signup-form">
						<form id="signupProceess" method="post" action="<?php echo SITE_URL;?>pricing/pricing_auth_popup">
			                      <?php if(isset($this->data['User']['id']) && isset($this->data['UserReference']['id']) && $id!=0){
				                    echo($this->Form->hidden('User.id',array('id'=>'popupId','div'=>false,'label'=>false)));
				                    echo($this->Form->hidden('UserReference.id',array('div'=>false,'label'=>false)));
			
			                      }?>
							<div class="login-with text-center">
								<a href="javascript:void(0); javascript:linkedin();">
									<img src="<?php echo(SITE_URL)?>yogesh_new1/images/linkedin.png">
								</a>
							</div>
							<label class="or">Or Register using email id</label>

							<div class="form-group">
								<input type="name" class="form-control" name="data[UserReference][first_name]" id="fname" placeholder="First Name">
                                                                <span class="errormsg" id="firstNameErr"></span>
							</div>
							<div class="form-group">
								<input type="name" class="form-control" name="data[UserReference][last_name]" id="lname" placeholder="Last Name">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="data[User][username]" id="email" placeholder="Email">
                                                                <div id="UserEmailErr" class="errormsg"></div>
							</div>
							<div class="form-group">
								<input type="password" name="data[User][password2]" id="password" class="form-control password_icon" placeholder="Password">
                                                                <div id="PassLengtherr" class="errormsg"></div>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="data[UserReference][zipcode]" id="zipcode" placeholder="Zip Code">
                                                                <div id="zipError" class="errormsg"></div>
							</div>
							<button class="banner-getstarted btn btn-default get-started" id="registerbutton">
								REGISTER
							</button>
							<div class="need-accounts loginHere text-center">
								<label>
									Already have an account?
									<span class="mymodal" data-dismiss="modal" data-toggle="modal" data-target="#signin-Modal">Login here</span>
								</label>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- signup process -->
	<div class="modal fade registerModel bs-example-modal-lg" id="signup-process" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">
						<label>GUILD</label> is changing the way organizations leverage outside expertise. We enable our clients to address complex problems and tap fleeting opportunities, like never before.
					</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="consultant-section">
								<h1>Consultants</h1>
								<p>We invite experts from various business domains to offer fresh insights, hands-on expertise and coaching to our clients.</p>
								<p>All engagements are at the Consultant's discretion.</p>
								<a class="banner-getstarted btn btn-default get-started" href="<?php echo(SITE_URL)?>members/eligibility">
									Apply Now
								</a>
							</div>
						</div>
						<div class="col-md-6">
							<div class="consultant-section">
								<h1>Clients</h1>
								<p>Our clients are the managers and directors of Small to Mid-sized businesses, Corporations and Non-profits.</p>
								<p>They leverage us for Growth, Process Improvements and Organization Development.</p>
								<button class="banner-getstarted register-client btn btn-default get-started" data-toggle="modal" data-dismiss="modal" data-target="#signup-modal">
									Get Started
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>