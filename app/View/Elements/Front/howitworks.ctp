<div class="expBG" id="howItWorks" style="display: none;">
                     <div class="floatR" style="margin-right:10px;margin-left:1100px;" onclick="hideme();"><span style="font-size:40px;color:#d03135;cursor: pointer;">&times;</span></div>
        	<div id="inner-content-box" class="pagewidth" >
	        	<h2 style="color: rgb(41, 41, 41);font-family: Ubuntu; font-size: 20px; font-weight: bold; margin-top: 10px;">
	        		The Support You Need for Extraordinary Results

	        	</h2>
	        	<div class="HRLineExp"></div>
	        	<p style="margin-top:-10px;font-family: 'Ubuntu';">
	        		<?php echo Configure::read('howItWorksHeader');?>
				</p>
			
				<div class="pagewidth" style="margin-top: 25px;">
					<h2 style="color: rgb(41, 41, 41);font-family: Ubuntu; font-size: 20px; font-weight: bold;">
						Our Advantage
					</h2>
					<div class="HRLineExp"></div>
					<div class="leftColumn">
						<div class="pimgDiv">
							<div class="mgREDICON-InstantAccess"></div>
						</div>
						<div class="expheader">
							<h2 class="h2exp1">Instant Access</h2>
						</div>
						<div class="by3Div">
							<p style="font-family: 'Ubuntu';">
								<?php echo Configure::read('instantAccess');?>
							</p>
						</div>
					</div>
					<div class="centreColumn">
						<div class="pimgDiv">
							<div class="mgREDICON-ToValidatedExperts"></div>
						</div>
						<div class="expheader">
							<h2 class="h2exp1">To Validated Experts</h2>
						</div>
						<div class="by3Div">
							<p style="font-family: 'Ubuntu';">
								<?php echo Configure::read('toValidateExperts');?>
							</p>
						</div>
					</div>
					<div class="rightColumn">
						<div class="pimgDiv">
							<div class="mgREDICON-AnywhereInCountry"></div>
						</div>
						<div class="expheader">
							<h2 class="h2exp1">Anywhere in the country</h2>
						</div>
						<div class="by3Div">
							<p style="font-family: 'Ubuntu';">
								<?php echo Configure::read('anywhereInCountry');?>
							</p>
						</div>
					</div>
				</div>
			
				<div class="pagewidth" style="margin-top: 40px;">
					<h2 style="color: rgb(41, 41, 41);font-family: Ubuntu; font-size: 20px; font-weight: bold;">
						Get Started
					</h2>
					<div class="HRLineExp"></div>
					<div class="leftColumn">
						<div class="pimgDiv">
							<div class="mgREDICON-SearchShortlist"></div>
						</div>
						<div class="expheader">
							<h2 class="h2exp1">1. Search & Shortlist</h2>
						</div>
						<div class="by3Div">
							<p style="font-family: 'Ubuntu';">
								<?php echo Configure::read('searchNshortlist');?>
							</p>
						</div>
					</div>
					<div class="centreColumn">
						<div class="pimgDiv">
							<div class="mgREDICON-RegisterToSchedule"></div>
						</div>
						<div class="expheader">
							<h2 class="h2exp1">2. Register to Schedule</h2>
						</div>
						<div class="by3Div">
							<p style="font-family: 'Ubuntu';">
								<?php echo Configure::read('registerToSchecdule');?>
							</p>
						</div>
					</div>
					<div class="rightColumn">
						<div class="pimgDiv">
							<div class="mgREDICON-ChooseAnExpert"></div>
						</div>
						<div class="expheader">
							<h2 class="h2exp1">3. Choose an Expert</h2>
						</div>
						<div class="by3Div">
							<p style="font-family: 'Ubuntu';">
								<?php echo Configure::read('chooseAnExpert');?>
							</p>
						</div>
					</div>
				</div>
        	</div>
        <div class="thanksCENTER" style="padding-bottom: 20px; padding-top: 20px;">
    		<input class="expButton" value="Start your search" type="button" onclick="hideme();">
        </div>
    </div>