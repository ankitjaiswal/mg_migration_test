<?php
$location = $this->Session->read('Location');

if($location == "International"){
$location = "New york, NY";
}
if(false !== stripos($location,", "))
{

$combined = explode(',',$location);
$visitorcity = trim($combined[0]);
$visitorstate = trim($combined[1]);
echo($visitorcity);
if(false !== stripos($visitorcity," "))
{
$visitorcity= str_replace(" ","-",$visitorcity);
}
}
?>
<?php
$pageLimit = 3;
$data_flag = false;
//prd($data);
if(!empty($data)){
	$data_flag = true;
?>
<div id="results" class="search-results qna-search-results">
	<?php
	foreach($data as $key => $value){
		if($key%$pageLimit == 0){
			$num = floor($key/$pageLimit);
		}
	?>
	<div class="set_<?php echo($num); ?>">

			<div class="box1">
				<?php $urlk = ($value['User']['url_key']); $displink=SITE_URL.strtolower($urlk); $consultant_name = $value['UserReference']['first_name']." ".$value['UserReference']['last_name']; ?>
				<div class="box-head">
					<div class="fixed-head">
						<div class="content-left-img">
							<?php $urlk1 = ($value['User']['url_key']); $displink1=SITE_URL.strtolower($urlk1); ?>
							<?php
								if(!empty($value['UserImage'])){
									$consultant_name = $value['UserReference']['first_name']." ".$value['UserReference']['last_name'];
									if(file_exists(WWW_ROOT . 'img' . '/' .MENTORS_IMAGE_PATH.'/'.$value['User']['id'].'/'.$value['UserImage'][0]['image_name'])){
										echo($this->Html->link($this->Html->image(MENTORS_IMAGE_PATH.'/'.$value['User']['id'].'/'.$value['UserImage'][0]['image_name'], array('class'=>'desaturate','alt'=>$value['UserReference']['first_name']." ".$value['UserReference']['last_name'])),$displink1,array('escape'=>false, 'title'=>$consultant_name)));
									}else{
										echo($this->Html->link($this->Html->image('media/profile.png',array('alt'=>$value['UserReference']['first_name']." ".$value['UserReference']['last_name'])),$displink1,array('escape'=>false)));
									}						
								}else{
									echo($this->Html->link($this->Html->image('media/profile.png',array('alt'=>$value['UserReference']['first_name']." ".$value['UserReference']['last_name'])),$displink1,array('escape'=>false)));
								}
							?>
						</div>
						
					</div>
					<div class="content-right-text">
						<h1>
							<span><?php echo($this->Html->link(ucwords($value['UserReference']['first_name']).' '.$value['UserReference']['last_name'],$displink,array('escape'=>false, 'title'=>$consultant_name))); ?></span>


							<p><?php echo($value['UserReference']['City']['city_name'].', '.$value['UserReference']['City']['state']);?> | 
							<small class="user-headline"><?php echo($value['UserReference']['linkedin_headline']);?></small></p>
							<?php if($value['linkedinURL'] != '') {?>
							<?php }?>

						</h1>
						<div class="text-content-box">
							<p>
								<?php
									$strdata=strlen ($value['UserReference']['background_summary']);
									if($strdata < 700){
									echo $this->Text->truncate( $value['UserReference']['background_summary'], 600, array('ending' => '...', 'exact' => false));
									}else{
										echo $this->Text->truncate( $value['UserReference']['background_summary'], 600, array('ending' => '...', 'exact' => false));
									}
								?>
								<?php echo($this->Html->link('Read more',$displink,array('escape'=>false,'onclick' => 'setCookieReadMore();','title'=>'View consultant profile'))); ?>
							</p>						
						</div>
						<div class="expertise-header">
							<ul class="tag-list">
								<?php if(($value['UserReference']['area_of_expertise']) !=''){
								$topic_list = '';
														$topic_list = $value['UserReference']['area_of_expertise'];
														$stringExp = $topic_list;
														$Exp3 = explode(',',$stringExp);
														$allExp = array();
																						$count = 0;
																					for($i=0;$i<5;$i++){
															if(isset($Exp3[$i]) && $Exp3[$i] != '')
																$allExp[$count++] = ucfirst(trim($Exp3[$i]));
																			}
														$uniqueAry = array_unique($allExp);
											
											foreach($uniqueAry as $catValue){
										if((false !== stripos($catValue,"/")) || (false !== stripos($catValue,"&")) || (false !== stripos($catValue,"-"))){
									if(false !== stripos($catValue,"/")){
										$catValues = str_replace("/","_slash",$catValue);
										$catValues =  strtolower($catValues);}
									if(false !== stripos($catValue,"&")){
										$catValues = str_replace("&","_and",$catValue);
										$catValues =  strtolower($catValues);}
									if(false !== stripos($catValue,"-")){
										$catValues = str_replace("-","_or",$catValue);
									$catValues =  strtolower($catValues);}
										$catValues = str_replace(" ","-",$catValues);
								?>
								<li><?php echo($this->Html->link($catValue,SITE_URL.'browse/search_result/all-industries/all-categories/'.$catValues.'/'.$visitorstate.'/'.$visitorcity,array('escape'=>false))); ?></li>
								<?php } else{ $catValues = str_replace(" ","-",$catValue); ?>
								<li><?php echo($this->Html->link($catValue,SITE_URL.'browse/search_result/all-industries/all-categories/'.$catValues.'/'.$visitorstate.'/'.$visitorcity,array('escape'=>false))); ?></li>
								<?php }}}?>
								<?php if(($value['UserReference']['ind-cat']) !=''){
													$keyword = "browse-all";
													$topic_list = '';
													$topic_list = $value['UserReference']['ind-cat'];
													$stringExp = $topic_list;
													$Exp3 = explode(',',$stringExp);
													$allExp = array();
													$count = 0;
													
													for($i=0;$i<3;$i++){
													
														if(isset($Exp3[$i]) && $Exp3[$i] != '')
															$allExp[$count++] = ucfirst(trim($Exp3[$i]));
																						}
																				$uniqueAry = array_unique($allExp);
									foreach($uniqueAry as $catValue){
								if($catValue!= "ALL INDUSTRIES - ALL CATEGORIES" || $catValue!= "all industries - all categories"){
									if(!(false !== stripos($catValue,"ALL CATEGORIES"))) {
								
								
								$combined = explode(' - ',$catValue);
								$industry = $combined[0];
								$category = trim($combined[1]);
								$categorytoshow = $category;
								$industry = str_replace(" ","-",$industry);
								$category = str_replace(" ","-",$category);
								?>
								<li><?php echo($this->Html->link($categorytoshow,SITE_URL.'browse/search_result/'.$industry.'/'.$category.'/'.$keyword.'/'.$visitorstate.'/'.$visitorcity,array('escape'=>false))); ?></li>
								<?php } else{
								if(!(false !== stripos($catValue,"ALL INDUSTRIES"))) {
											$allcategories = "all-categories";
									$combined = explode(' - ',$catValue);
									$industry = $combined[0];
									$category = trim($combined[1]);
								$industrytoshow = $industry;
									$industry = str_replace(" ","-",$industry);
								
								?>
								<li><?php echo($this->Html->link($industrytoshow,SITE_URL.'browse/search_result/'.$industry.'/'.$allcategories.'/'.$keyword.'/'.$visitorstate.'/'.$visitorcity,array('escape'=>false))); ?></li>
								<?php }}}}}?>
							</ul>
						</div>
					</div>
				</div>
			</div>
	</div>
	<?php
	
	}?>
</div>
<?php echo($this->Element('Browse/directory'));?>
<div class="view-more-detail">
	<?php
		if($num >0){
			$style ="background: none repeat scroll 0 0 #D03135; color: #FFFFFF; cursor: pointer; font-weight: bold; height: 14px; text-align: center; margin:auto; padding: 10px; width:50%;";
	?>
	
	
	<div>
		<?php if($this->Session->read('Auth.User.id') ==''){?>
		<a href="#" class="mymodal" data-toggle="modal" data-target="#signin-Modal">See More Members</a>
		<?php }else{?>
		<a href="javascript:void(0);" class="mymodal" id="openRegisterNav">See More Members</a>
		<?php }?>
		&nbsp;&nbsp;&nbsp;<input type="button" value="Post a project" class="btn btn-primary search-btn" onclick="window.open('https://guild.im/project/create')"></div>
	</div>
	<?php
	}
	?>
</div>
<?php
}else{?>
<div class="box1 no-profile">
	<div class="row">
		<div class="col-sm-12">
			<h2 style="margin-bottom: 30px;">Sorry, no consultant profiles were found. Try one of these options.</h2>
		</div>
		<div class="col-lg-5 col-md-5 col-sm-12">
			<h4>Search Suggestions</h4>
			<ul class="premium-list">
				<li>Check your spelling</li>
				<li>Try different words that mean the same thing</li>
				<li><a href="<?php echo SITE_URL?>qanda" class="internal">Ask a question</a> and get answers from Experts</li>
				<li><a href="<?php echo SITE_URL?>project/create" class="internal">Submit a project</a></li>
			</ul>
		</div>
		<div class="col-lg-7 col-md-7 col-sm-12">
			<h4>Top Searches</h4>
			<div class="row">
				<div class="col-sm-4">
					<ul>
						<li>1. <a href="<?php echo SITE_URL?>john.baldoni">John Baldoni</a></li>
						<li>2. <a href="<?php echo SITE_URL?>mark.palmer">Mark Palmer</a></li>
						<li>3. <a href="<?php echo SITE_URL?>bill.fotsch">Bill Fotsch</a></li>
						<li>4. <a href="<?php echo SITE_URL?>judy.bardwick">Judy Bardwick</a></li>
						<li>5. <a href="<?php echo SITE_URL?>ivan.rosenberg">Ivan Rosenberg</a></li>
					</ul>
				</div>
				<div class="col-sm-4">
					<ul>
						<li>6. <a href="<?php echo SITE_URL?>browse/search_result/all-industries/all-categories/Leadership/NY/new-york">Leadership</a></li>
						<li>7. <a href="<?php echo SITE_URL?>browse/search_result/all-industries/all-categories/Customers/NY/new-york">Customers</a></li>
						<li>8. <a href="<?php echo SITE_URL?>browse/search_result/all-industries/all-categories/Interim-Management/NY/new-york">Interim Management</a></li>
						<li>9. <a href="<?php echo SITE_URL?>browse/search_result/all-industries/all-categories/Strategy/NY/new-york">Strategy</a></li>
						<li>10. <a href="<?php echo SITE_URL?>browse/search_result/Healthcare/all-categories/browse-all/NY/new-york">Healthcare</a></li>
					</ul>
				</div>
				<div class="col-sm-4">
					<ul>
						<li>11. <a href="<?php echo SITE_URL?>browse/search_result/all-industries/all-categories/executive-coaching/NY/new-york">Executive Coaching</a></li>
						<li>12. <a href="<?php echo SITE_URL?>browse/search_result/all-industries/all-categories/Retail">Retail</a></li>
						<li>13. <a href="<?php echo SITE_URL?>browse/search_result/all-industries/all-categories/Innovation">Innovation</a></li>
						<li>14. <a href="<?php echo SITE_URL?>browse/search_result/all-industries/all-categories/browse-all/CA/los-angeles">Los Angeles</a></li>
						<li>15. <a href="<?php echo SITE_URL?>browse/search_result/all-industries/all-categories/browse-all/NY/new-york">New York</a></li>
					</ul>
				</div>
			</div>			
		</div>
	</div>
</div>
<?php echo($this->Element('Browse/directory'));?>
<?php
} ?>
<?php
if($data_flag){?>
<?php echo($this->Form->hidden('num',array('value'=>$num,'id'=>'num'))); ?>
<?php echo($this->Form->hidden('countShow',array('id'=>'countShow'))); ?>
<?php
}else{?>
<?php echo($this->Form->hidden('num',array('value'=>'NOT','id'=>'num'))); ?>
<?php
}?>
<!-- js link -->
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/script12.js"></script>
<script type="text/javascript">
var openRegister = 0;
function createCookie(name,value,days){if(days){var date=new Date();date.setTime(date.getTime()+(days*24*60*60*1000));var expires="; expires="+date.toGMTString();}
else var expires="";document.cookie=name+"="+value+expires+"; path=/";}
function readCookie(name){var nameEQ=name+"=";var ca=document.cookie.split(';');for(var i=0;i<ca.length;i++){var c=ca[i];while(c.charAt(0)==' ')c=c.substring(1,c.length);if(c.indexOf(nameEQ)==0)return c.substring(nameEQ.length,c.length);}
return null;}
function eraseCookie(name){createCookie(name,"",-1);}
	function apply(url,mentor_id,ClickType){
var role_id = "<?php echo $this->Session->read('Auth.User.role_id'); ?>";
var email_send = "<?php echo $this->Session->read('Auth.User.email_send'); ?>";
var is_approved = "<?php echo $this->Session->read('Auth.User.is_approved'); ?>";
if(role_id==''){
//applyloginpopup(mentor_id);
applyregisterpopup();
}else if(role_id==2){
alert("Sorry, mentors are currently not allowed to apply for mentorship from other mentors.");
}else{
if(is_approved=='0')
{
location.href = SITE_URL+"users/logout";
}
else
{
if(ClickType == "request"){
requestMentorship(mentor_id);
}
else{
location.href = url+mentor_id;
}
}
}
}
// more mentor show functionality
//history.navigationMode = 'compatible';
jQuery(document).ready(function(){
var num_tot = parseInt(jQuery("#num").val(),10);
for(var iii = 0; iii <= num_tot; iii++){
if(iii != 0)
jQuery(".set_"+iii).hide();
}
var num = jQuery("#num").val();
if(num == 0) {
jQuery("#user-account").show();
}
if(num == 'NOT') {
jQuery("#user-account").show();
}
if(num !='NOT'){
jQuery(".set_0").show();
jQuery("#countShow").val(0);
}
var old_countShow = readCookie('cookie_countShow')
if (old_countShow) {
for(showCnt=0;showCnt<old_countShow;showCnt++){
	jQuery("#countShow").val(showCnt);
	paging_hack();
	}
	
	}
	eraseCookie('cookie_countShow');
	});
	jQuery("#openRegisterNav").click(function(){
	
	if(loggedUserId == ''){
	if(openRegister == 0) {
	
	loginpopup();
	return;
	}
	}
	
	var num = parseInt(jQuery("#num").val(),10);
	var countShow = parseInt(jQuery("#countShow").val(),10) + 1;
	
	if(num > countShow){
	jQuery(".set_"+countShow).show();
	jQuery("#countShow").val(countShow);
	}else if(num == countShow){
	jQuery(".set_"+countShow).show();
	jQuery("#countShow").val(countShow);
	jQuery("#openRegisterNav").hide();
	jQuery("#user-account").show();
	}
	});
	
	function paging_hack(){
	var num = parseInt(jQuery("#num").val(),10);
	var countShow = parseInt(jQuery("#countShow").val(),10) + 1;
	createCookie('cookie_countShow',countShow,0);
	
	if(num > countShow){
	jQuery(".set_"+countShow).show();
	jQuery("#countShow").val(countShow);
	}else if(num == countShow){
	jQuery(".set_"+countShow).show();
	jQuery("#countShow").val(countShow);
	jQuery("#openRegisterNav").hide();
	}
	}
	
	function setCookieReadMore(){
	var countShow = parseInt(jQuery("#countShow").val(),10) + 1;
	createCookie('cookie_countShow',countShow,0);
	}
	function showSideDiv(id){
	jQuery("#dUser_"+id).show();
	}
	function hideSideDiv(id){
	jQuery("#dUser_"+id).hide();
	}
	
	</script>
	<script type="text/javascript">
	function showonlyone(thechosenone) {
	jQuery('.openCONTENT').each(function(index) {
	if (jQuery(this).attr("id") == thechosenone) {
		jQuery(this).toggle(300); }
	else {
		jQuery(this).hide(600); }
	});
	}
	jQuery(document).mouseup(function (e) {
		var container = jQuery(".openCONTENT");
		if (!container.is(e.target) && container.has(e.target).length === 0){
			container.hide();
		}
		});
	</script>