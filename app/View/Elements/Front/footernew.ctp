
	<!-- footer section -->
	<div id="footer-section">
		<div class="container">
			<div class="pad0 col-md-2 col-xs-12">
				<a href="<?php echo(SITE_URL)?>"><img src="<?php echo(SITE_URL)?>Guild_New_Logo/footer-icon-new.png" style="width:100px;height:103px;" alt="GUILD"></a>
			</div>
			<div class="col-md-3 col-xs-12 about-footer">
				<h2>About</h2>
				<p>GUILD helps businesses leverage outside expertise.</p>
				<ul class="list-unstyled">
					<li>
						<a href="https://www.linkedin.com/company/guild-digital" target="_blank">
							Company Profile
						</a>
					</li>

					<li>
						<label>Call us :</label>
						<span><a href='tel:1-866-511-1898'><span>1-866-511-1898</span></a></span>
					</li>
					<li>
						<label>Email :</label>
						<a href='mailto&#58;help@guild.im'><span>help@guild.im</span></a>
					</li>
				</ul>
			</div>
			<div class="col-md-5 col-xs-12 tweets-footer">
				<h2>Tweets</h2>
				<ul class="list-unstyled">
					<li>
                                          <div id="twitter_update_list"></div>
					</li>

				</ul>
			</div>
			<div class="col-md-2 col-xs-12 social-footer">
				<h2>Social</h2>
				<ul class="list-unstyled">
					<li>
						<a href="https://www.linkedin.com/company/guild-digital" target="_blank">
							<!-- <img src="<?php echo(SITE_URL)?>/yogesh_new1/images/link_icon2.png"> -->
							<i class="fa fa-linkedin"></i>
						</a>
					</li>
					<li>
						<a href="https://twitter.com/GuildAlerts" target="_blank">
							<!-- <img src="<?php echo(SITE_URL)?>/yogesh_new1/images/twitt_icon2.png"> -->
							<i class="fa fa-twitter"></i>
						</a>
					</li>

				</ul>
			</div>
		</div>
	</div>
	<div id="subfooter-section">
		<div class="container">
			<ul class="pull-left list-unstyled">
				<li>
					<a href="<?php echo(SITE_URL)?>privacy">Privacy</a>
				</li>
				<li>
					|
				</li>
				<li>
					<a href="<?php echo(SITE_URL)?>terms">Terms of Service</a>
				</li>
			</ul>
			<label class="pull-right">
				&#169; 2019 GUILD. All Rights Reserved
			</label>
		</div>

	<?php if(isset($_SESSION['User_type'])){
			if($this->Session->read('Auth.User.id') =='') {
				$this->Session->delete('User_type');
			} else {
		?>
			
		<a href="<?php echo SITE_URL?>members/switch_user/1">Switch to admin</a>
	<?php }
	}?>


	</div>
	<!--  -->
	<!-- <div class="modal fade inviteacolleague member-edit-popup" tabindex="-1" role="dialog">
	    <div class="modal-dialog">
	    	<div class="modal-content">
	    		<div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	                <h3 class="modal-title"><i class="fa fa-envelope-o" aria-hidden="true"></i> Membership Invitation</h3>
	            </div>
	      		<div class="modal-body edit-profile-m">			      
		     		
                                <form id="Submitform" name="commentform" enctype="multipart/form-data" method="post" action="<?php echo SITE_URL;?>users/client_invitation" accept-charset="utf-8">
		     			<div class="aoe-box">
			     		    <div class="col-xs-12 form-group">
		     		            <input type="text" class="form-control" id="email" name="data[EmailId]" placeholder="Enter email addresses separated by commas"/>
			     		    </div>
			     		    <div class="col-xs-12 form-group">
		     		            <input type="text" class="form-control" id="subject" name="data[EmailSubject]" placeholder="Enter Subject"/>
			     		    </div>
			     		    <div class="col-xs-12 form-group">
		     		            <textarea rows="6" class="form-control" id="emailbody" name="data[EmailBody]" placeholder="Enter a message">
Hi,

I'd like you to join me on GUILD.

Membership is open to C-level business executives, who want easier access to outside expertise.
    
Thanks,
<?php echo(ucfirst($this->data['UserReference']['first_name']));?>

                                            </textarea>
			     		    </div>
		     		    </div>
		     		    <div class="button-set">
     		        		<a href="javascript:void(0);" class="btn btn-primary" id="apply" onclick="return validateInvitation();">Send</a>
		     		    </div>
		     		</form>

	            </div>
	    	</div>
	  	</div>
	</div>
     -->


	<!--  -->
	<div class="modal fade inviteamembercolleague member-edit-popup" tabindex="-1" role="dialog">
	    <div class="modal-dialog">
	    	<div class="modal-content">
	    		<div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
	                <h3 class="modal-title"><i class="fa fa-envelope-o" aria-hidden="true"></i> Membership Invitation</h3>
	            </div>
	      		<div class="modal-body edit-profile-m">			      
		     		
                                <form id="Submitform1" name="commentform" enctype="multipart/form-data" method="post" action="<?php echo SITE_URL;?>users/invitation" accept-charset="utf-8">
		     			<div class="aoe-box">
			     		    <div class="col-xs-12 form-group">
		     		            <input type="text" class="form-control" id="email" name="data[EmailId]" placeholder="Enter email addresses separated by commas"/>
			     		    </div>
			     		    <div class="col-xs-12 form-group">
		     		            <input type="text" class="form-control" id="subject" name="data[EmailSubject]" placeholder="Enter Subject" value="Invitation to join GUILD"/>
			     		    </div>
			     		    <div class="col-xs-12 form-group">
		     		            <textarea rows="6" class="form-control" id="emailbody" name="data[EmailBody]" placeholder="Enter a message">
									Hi,

									I would like you to join me as a member of GUILD.

									GUILD members include experienced business coaches & management consultants.

									URL: https://www.guild.im/members/56KOv57lNjSR9yI6q5Ps

									Thanks,
									<?php echo(ucfirst($this->data['UserReference']['first_name']));?>

                                </textarea>
			     		    </div>
		     		    </div>
		     		    <div class="button-set">
     		        		<a href="javascript:void(0);" class="btn btn-primary" id="apply" onclick="return validateInvitation1();">Send</a>
		     		    </div>
		     		</form>

	            </div>
	    	</div>
	  	</div>
	</div>
         <!--  -->



<!-- js link -->

	<!-- js link -->

	

        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/script.js?v=1.184"></script>
        <script src="<?php echo(SITE_URL)?>yogesh_new1/js/custome.js"></script>
        
	<!--  -->
<script type="text/javascript">
function twitterCallback2(twitters) {
  var statusHTML = [];
  for (var i=0; i<twitters.length; i++){
    var username = twitters[i].user.screen_name;
	
	if (twitters[i]['retweeted_status'] != null) {
    	twitters[i].text = "RT @"+twitters[i]['retweeted_status']['user']['screen_name']+" : " + twitters[i]['retweeted_status']['text'];
    }
    var status = twitters[i].text.replace(/((https?|s?ftp|ssh)\:\/\/[^"\s\<\>]*[^.,;'">\:\s\<\>\)\]\!])/g,
    function(url){
		return '<a href="'+url+'" target="_blank"><span>'+url+'</span></a>';
    }).replace(/\B@([_a-z0-9]+)/ig, function(reply) {
	    return  reply.charAt(0)+'<a href="https://twitter.com/'+reply.substring(1)+'" style="color:#EEEEEE;" target="_blank">'+reply.substring(1)+'</a>';
    });
    statusHTML.push('<p>'+status+'<br>');
  }
  statusHTML.push('</p>');
  document.getElementById('twitter_update_list').innerHTML = statusHTML.join('');
}
</script>

<?php
{

require_once('../webroot/TwitterAPIExchange.php');

/** Set access tokens here - see: https://dev.twitter.com/apps/ **/
$settings = array(
		'oauth_access_token' => "307588817-ri2kaoOZv7F5Ps2IWz0J2AMl3mn58aN91wGzSaeA",
		'oauth_access_token_secret' => "k5BQaVAAFrRPskC7SN8uunFnKj8L83pPHqpmwYpnswqVZ",
		'consumer_key' => "ZCBr3siWeVe889iD71gSCNwFh",
		'consumer_secret' => "dksVrAiUxUrBU4AQ1ISq5bY9ublSm8ZyftPBhk8euVSY8uNwsW"
);

$url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
$getfield = '?screen_name=GuildAlerts&include_rts=true&count=2';
$requestMethod = 'GET';
$twitter = new TwitterAPIExchange($settings);
$twitters = $twitter->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest();

echo '<script type="text/javascript"> twitterCallback2('.$twitters.'); </script>';
}  		
?>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-show-password/1.0.3/bootstrap-show-password.min.js"></script>

<script type="text/javascript">
function linkedin(type,id)
{
console.log(type);
if(typeof type=="undefined")
{window.location.href=SITE_URL+'linkedin/OAuth2.php';return true;}
if(type=='mentor')
{window.location.href=SITE_URL+'linkedin/signuplinkmember.php';return true;}
if(type=='mentorPopup')
{window.location.href=SITE_URL+'linkedin/OAuth2.php';return true;}
if(type=='menteePopup')
{window.location.href=SITE_URL+'linkedin/OAuth2.php?userId='+id;return true;}
if(type=='pricingPopUp')
{window.location.href=SITE_URL+'linkedin/OAuth2.php?planUserId='+id;return true;}
if(type=='sresult')
{window.location.href=SITE_URL+'linkedin/OAuth2.php?sresult='+id;return true;}
if(type=='pricingAuthPopUp')
{window.location.href=SITE_URL+'linkedin/OAuth2.php?plan_id='+id;return true;}
if(type=='projectPopUp')
{window.location.href=SITE_URL+'linkedin/OAuth2.php?projectUserId='+id;return true;}}

function validateInvitation()
{jQuery('#subject').css('border-color','#DDDDDD');jQuery('#emailbody').css('border-color','#DDDDDD');jQuery('#email').css('border-color','#DDDDDD');var mailformat=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;var flag=0;if(jQuery.trim(jQuery('#subject').val())==''){jQuery('#subject').css('border-color','#F00');flag++;}
if(jQuery.trim(jQuery('#emailbody').val())==''){jQuery('#emailbody').css('border-color','#F00');flag++;}
var str=jQuery.trim(jQuery('#email').val());if(str==''){jQuery('#email').css('border-color','#F00');flag++;}else{if(str.indexOf(",")!=-1){var expertArr=str.split(",");var length=expertArr.length;for(i=0;i<length;i++){if(expertArr[i]==''||!((jQuery.trim(expertArr[i])).match(mailformat))){jQuery('#email').css('border-color','#F00');flag++;}}}else{if(!(jQuery.trim(str).match(mailformat))){jQuery('#email').css('border-color','#F00');flag++;}}}
if(flag==0){jQuery("#Submitform").submit();}
else{return false;}}

function validateInvitation1()
{jQuery('#subject').css('border-color','#DDDDDD');jQuery('#emailbody').css('border-color','#DDDDDD');jQuery('#email').css('border-color','#DDDDDD');var mailformat=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;var flag=0;if(jQuery.trim(jQuery('#subject').val())==''){jQuery('#subject').css('border-color','#F00');flag++;}
if(jQuery.trim(jQuery('#emailbody').val())==''){jQuery('#emailbody').css('border-color','#F00');flag++;}
var str=jQuery.trim(jQuery('#email').val());if(str==''){jQuery('#email').css('border-color','#F00');flag++;}else{if(str.indexOf(",")!=-1){var expertArr=str.split(",");var length=expertArr.length;for(i=0;i<length;i++){if(expertArr[i]==''||!((jQuery.trim(expertArr[i])).match(mailformat))){jQuery('#email').css('border-color','#F00');flag++;}}}else{if(!(jQuery.trim(str).match(mailformat))){jQuery('#email').css('border-color','#F00');flag++;}}}
if(flag==0){jQuery("#Submitform1").submit();}
else{return false;}}

	$('.password_icon').password({
	    eyeClass: 'fa',
	    eyeOpenClass: 'fa-eye',
	    eyeCloseClass: 'fa-eye-slash'
	})

</script>

<script type="text/javascript">
//function geolocate(timezone, cityPrecision, objectVar).
//If you rename your object name, you must rename 'visitorGeolocation' in the function
var visitorGeolocation = new geolocate(false, true, 'visitorGeolocation');

    
//Check for cookie and run a callback function to execute after geolocation is read either from cookie or IPInfoDB API
var callback = function(){
                //alert('Visitor country code : ' + visitorGeolocation.getField('regionName'))
 
               };
visitorGeolocation.checkcookie(callback);
 var conntry_code 		= visitorGeolocation.getField('countryCode');
  var geoip_city 	 	= visitorGeolocation.getField('cityName');	
  var geoip_region_name 	= visitorGeolocation.getField('regionName');
  var geoip_ipAddress 	= visitorGeolocation.getField('ipAddress');
  var city = '';
  var state = '';
     jQuery(document).ready(function(){

           jQuery.ajax({
			url:SITE_URL+'/fronts/getStateCode',
			type:'post',
			dataType:'json',
			data:'state_name='+geoip_region_name + '&country_code=' + conntry_code + '&city=' + geoip_city,
			success:function(res){
				if(res.value !=''){
					state_name = res.value;	
					if(conntry_code =='US'){
                                  
						jQuery(".newyork").val(geoip_city+', '+state_name);
                                                city = geoip_city;
                                                state = state_name;
					}else{
                                       
						jQuery(".newyork").val('International');
					}					
				}else{
					if(conntry_code =='US'){
                                     
						jQuery(".newyork").val(geoip_city+', '+geoip_region_name);
                                                city = geoip_city;
                                                state = state_name;
					}else{
                                      
						jQuery(".newyork").val('International');
					}
				}
			}
		});
              });
</script>


 
    <script type="application/ld+json">
{ "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "GUILD",
  "url" : "https://www.guild.im",
  
  "sameAs" : [ "https://www.linkedin.com/company/guild-digital",
    "https://www.twitter.com/guildalerts",
    "https://business.facebook.com/Guild.Digital",
    "https://plus.google.com/u/0/+MentorsguildLive"] 
}
</script>   
    <script type="application/ld+json">
    {
      "@context": "http://schema.org",
      "@type": "Organization",
      "url": "https://www.guild.im",
      "logo": "https://www.guild.im/imgs/mg_monogram1.png"
    }
    </script>  
    <script type="application/ld+json">
    {  "@context" : "http://schema.org",
       "@type" : "Organization",
       "name" : "GUILD",
       "alternateName" : "guild.im",
       "url" : "https://www.guild.im"
    }
    </script> 

    <script type="application/ld+json">
    {  "@context" : "http://schema.org",
       "@type" : "ProfessionalService",
       "name" : "GUILD",
       "alternateName" : "guild.im",
       "url" : "https://www.guild.im",
    "image":"https://www.guild.im/imgs/mg_monogram1.png",
    "address": "Manoa Innovation Center 2800 Woodlawn Drive,Honolulu, Hawaii",
    "telephone": "+1 866-511-1898"

    }
    </script> 
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "LocalBusiness",
  "address": {
    "@type": "PostalAddress",
    "addressLocality": "Manoa Innovation Center",
    "addressRegion": "Honolulu, Hawaii",
    "streetAddress": "2800 Woodlawn Drive"

  },
  "description": "GUILD offers business leaders proven expertise to solve hard business problems, as a cost-effective alternative to internal staffing or traditional consulting.",
  "name": "GUILD Business Consulting",
  "telephone": "+1 866-511-1898",
   "image":"https://www.guild.im/imgs/mg_monogram1.png"
}
</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "GUILD",
  "description": "GUILD offers business leaders proven expertise to solve hard business problems, as a cost-effective alternative to internal staffing or traditional consulting.",
  "telephone": "+1 866-511-1898"
  }
</script> 
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "address": {
    "@type": "PostalAddress",
    "addressLocality": "Honolulu, Hawaii",
    "postalCode": "96822",
    "streetAddress": "Manoa Innovation Center 2800 Woodlawn Drive"
  },
  "email": "help(at)guild.im",
  "faxNumber": "+1 866-511-1898",
  "name": "GUILD",
  "telephone": "+1 866-511-1898"
}
</script>