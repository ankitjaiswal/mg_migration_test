<?php 
$facecm = '';
$onlinecm = '';
if(isset($this->data['Communication'][0]['mode_type']) && $this->data['Communication'][0]['mode_type']){
	$facecm = 'checked';
}
if(isset($this->data['Communication'][1]['mode_type']) && $this->data['Communication'][1]['mode_type']){
	$onlinecm = 'checked';
}
?>
<tr class="AddProFrm">
	<td colspan="2"><h3>Add Mode</h3></td>
</tr>
<tr>
	<td colspan="2">
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%">
				<input type="checkbox" name="data[Communication][0][mode_type]" id="Communication0ModeType" value="face_to_face" <?php echo($facecm); ?> />
				Face to face
				</td>
				<td><?php echo($this->Form->input('Communication.0.mode',array('div'=>false,'label'=>false,'style'=>'width:430px;','placeholder'=>'Eg.Honololu downtown')));?></td>					
			</tr>
			<tr>
				<td>
				<input type="checkbox" name="data[Communication][1][mode_type]" id="Communication1ModeType" value="online" <?php echo($onlinecm); ?> />
				Online
				</td>
				<td><?php echo($this->Form->input('Communication.1.mode',array('div'=>false, 'label'=>false,'style'=>'width:430px;','placeholder'=>'Eg.Skype')));?></)));?></td>					
			</tr>			
		</table>
	</td>
</tr>