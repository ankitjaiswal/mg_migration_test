<?php $this->layout= 'defaultnew';?>

<?php 
$showBlock = 2;
$ApplyRequestData = $this->General->MenteeLog();
if(count($ApplyRequestData)>0 && !empty($ApplyRequestData)){ ?> 
<h4>Status</h4>	
    <div class="user-status" id="user-status">	
    			  
		  <?php $col3='';
				foreach($ApplyRequestData as $apply)
				{
					if($apply[0]['applicationType'] == '1' || $apply[0]['inv_date'] != null){
				    $userInfo = $this->General->getUserReferenceData($apply[0]['mentor_id']);
                    $mentorshipCount = $this->General->getMenteeStatusInitalRegular($apply[0]['mentor_id']);
                    if($apply[0]['inv_title']!=''){
                        $title = ucfirst($apply[0]['inv_title']);
	                    if(strlen($title) > 25) {
	                    	$title = substr($title, 0, 25);
	                    	$title = $title."...";
	                    }
                    }
                    else
                        $title = '';
                    if($apply[0]['inv_date']!='')
                        $invDate = date('d-M-y',$apply[0]['inv_date']);
                    else
                        $invDate = '';
                    if($apply[0]['inv_time']!='')
                    {
                       // $invTime = str_replace(',',' ',$apply['0']['inv_time']);
                        $TimeData = explode(":",$apply['0']['inv_time']);
                        $invTime = $TimeData[0].":".$TimeData[1]." ".$TimeData[2];
                    }
                    else
                        $invTime = '';
                    
					if($apply[0]['inv_timezone'] == 1){
                    	$invTimeZone = 'Pacific';
                    }else if($apply[0]['inv_timezone'] == 2){
                    	$invTimeZone = 'Alaska Time';
                    }else if($apply[0]['inv_timezone'] == 3){
                    	$invTimeZone = 'Hawaii';
                    }else if($apply[0]['inv_timezone'] == 4){
                    	$invTimeZone = 'Mountain';
                    }else if($apply[0]['inv_timezone'] == 5){
                    	$invTimeZone = 'Central';
                    }else if($apply[0]['inv_timezone'] == 6){
                    	$invTimeZone = 'Eastern';
                    }
                        
					if($apply[0]['applicationType']=='1')
						$col3='Consultation request sent on '.date('dS M Y',strtotime($apply[0]['created']));
                    
					if($apply[0]['applicationType']=='3')
						$col3=$title.' proposed';
                    
					if($apply[0]['applicationType']=='4')
						$col3='Request to reschedule';
                    
					if($apply[0]['applicationType']=='5')
						$col3=$this->Html->link($title,array('controller'=>'invoice','action'=>'invoice_view/'.$apply[0]['inv_id']),array('class'=>'delete')).' completed';
                    
					if($apply[0]['applicationType']=='6')
						$col3='Application reject';
                    
                    if($apply[0]['applicationType']=='7')
                        $col3=$this->Html->link($title,array('controller'=>'invoice','action'=>'invoice_view/'.$apply[0]['inv_id']),array('class'=>'delete')).' confirmed<br/>'.$invDate.", ".$invTime." (" .$invTimeZone.")";
		  ?>
					<div class="user-status-account">
					  <div class="col1"><div class="minus_user"></div><a href="<?php echo SITE_URL."users/my_account/".strtolower($userInfo['UserReference']['url_key']);?>"><?php echo ucfirst(strtolower($userInfo['UserReference']['first_name'])).' '.ucfirst(strtolower($userInfo['UserReference']['last_name']));?></a></div>
					  <div class="col2">
					  
					  <?php if($apply[0]['applicationType']=='1'):   ?>
								      &nbsp;
					  <?php else: 
					  
					  $unreadMessagesCount = $this->General->GetUnreadLogs($apply[0]['id']);

					  if($unreadMessagesCount[0] > 0){
						?>
					  	<a href="<?php echo SITE_URL."clients/private_log/".$apply[0]['id'];?>"> Log </a>
					  	<?php 
					  }
					  else {
					  	?>
					  	<a href="<?php echo SITE_URL."clients/private_log/".$apply[0]['id'];?>">Log</a>
					  	<?php 
					  }
					  ?>
					 <?php endif; ?></div>
					  <div class="col3"><?php echo $col3;?></div>
					 <?php if($apply[0]['applicationType']=='1'):   
							$hideShow = 'display:block;';
							if($this->Session->check('hide_reminder_link') && $this->Session->read('hide_reminder_link') == $apply[0]['id']){
								$hideShow = 'display:none;';
								$this->Session->delete('hide_reminder_link');
							}
					 
					 ?>
									<div class="col4" style="<?php echo $hideShow;?>"><a class="send_reminder_hide" href="<?php echo SITE_URL."invoice/send_reminder/".$apply[0]['id']."/client/application";?>">Send Reminder</a></div>
								<?php elseif($apply[0]['applicationType']=='3'): ?>
											<div class="col4"><a href="<?php echo SITE_URL."invoice/order_confirm/".$apply[0]['inv_id'];?>">Review</a></div>
								<?php 	elseif($apply[0]['applicationType']=='4'): 
								
									$hideShow = 'display:block;';
									if($this->Session->check('hide_reminder_link') && $this->Session->read('hide_reminder_link') == $apply[0]['id']){
										$hideShow = 'display:none;';
										$this->Session->delete('hide_reminder_link');
									}
								
								?>
											<div class="col4" style="<?php echo $hideShow;?>"><a class="send_reminder_hide" href="<?php echo SITE_URL."invoice/send_reminder/".$apply[0]['id']."/client/reschedule";?>">Send Reminder</a></div>
								<?php 		elseif($apply[0]['applicationType']=='5'): ?>
												<div class="col4">
												<?php if($mentorshipCount>=1){ ?>
												    <a class="registeration" alt="Registration" href="javascript:feedbackform_popup('<?php echo $apply[0]['mentor_id']; ?>','<?php echo $apply[0]['id']; ?>');">Give Feedback</a>
												 <?php } 
												 
													$hideShow = 'display:inline-block;';
													if($this->Session->check('hide_reportissue_link') && $this->Session->read('hide_reportissue_link') == $apply[0]['id']){
														$hideShow = 'display:none;';
														$this->Session->delete('hide_reportissue_link');
													}
												 if($mentorshipCount>=1){ ?>
													<span style="<?php echo $hideShow;?>" id="pipe_<?php echo $apply[0]['id']; ?>">&nbsp;|&nbsp;</span>
													<?php } ?>
													<a style="<?php echo $hideShow;?>" class="report_issue_hide_<?php echo $apply[0]['id']; ?>" alt="Registration" href="javascript:reportissue_popup('<?php echo $apply[0]['id']; ?>','mentee');">
													Report an issue</a>
												</div>
								<?php 			elseif($apply[0]['applicationType']=='6'): ?>
												    <div class="col4">Reject</div>
								<?php 				elseif($apply[0]['applicationType']=='7'): 
									$hideShow = 'display:inline-block;';
									if($this->Session->check('hide_reportissue_link') && $this->Session->read('hide_reportissue_link') == $apply[0]['id']){
										$hideShow = 'display:none;';
										$this->Session->delete('hide_reportissue_link');
									}
								
								?>
                                                        <div class="col4"><a style="<?php echo $hideShow;?>" class="report_issue_hide_<?php echo $apply[0]['id']; ?>" alt="Registration" href="javascript:reportissue_popup('<?php echo $apply[0]['id']; ?>','mentee');">Report an issue</a></div>							
								<?php 	         endif; ?>
					</div>
		 <?php }} ?>
		  <div class="clear"></div>
</div><?php } ?>