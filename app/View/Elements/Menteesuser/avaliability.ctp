<?php
$hoursOption = array();
for($i=1;$i<=23;$i++){
	if($i<10){
		$index = '0'.$i;
		$hoursOption[$index] = $index;
	}else{
		$hoursOption[$i] = $i;
	}
	
}
$minutesOption = array();
for($i=0;$i<=59;$i++){
	
	if($i<10){
		$index = '0'.$i;
		$minutesOption[$index] = $index;
	}else{
		$minutesOption[$i] = $i;
	}	
	
	
}
$dayOptions =array(
    'Sunday'=>'Sunday',
    'Monday'=>'Monday',
    'Tuesday'=>'Tuesday',
    'Wednesday'=>'Wednesday',
    'Thursday'=>'Thursday',
    'Friday'=>'Friday',
    'Saturday'=>'Saturday',
);
?>
<tr class="AddProFrm">
	<td colspan="2"><h3>Add Avaliability</h3></td>
</tr>		
<tr>
	<td colspan="2">
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%">
				From:
				<?php echo($this->Form->input('Availablity1.hour_from',array('options'=>$hoursOption,'div'=>false,'label'=>false,'class'=>'select4')));?>&nbsp;:&nbsp;
				<?php echo($this->Form->input('Availablity1.minute_from',array('options'=>$minutesOption,'div'=>false,'label'=>false,'class'=>'select4')));?>
				</td>
				<td width="5%">To:</td>
				<td>				
				<?php echo($this->Form->input('Availablity1.hour_to',array('options'=>$hoursOption,'div'=>false,'label'=>false,'class'=>'select4')));?>&nbsp;:&nbsp;
				<?php echo($this->Form->input('Availablity1.minute_to',array('options'=>$minutesOption,'div'=>false,'label'=>false,'class'=>'select4')));?>
				</td>
			</tr>
			<tr>			
				<td colspan="3">
				Days:
				<?php echo($this->Form->input('Availablity1.day',array('multiple' => 'checkbox','type'=>'select','options'=>$dayOptions,'div'=>false,'label'=>false))); ?>
				</td>
			</tr>			
		</table>
	</td>
	
</tr>
	