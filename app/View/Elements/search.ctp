<?php
  echo($this->Html->script(array('prototype','jquery/jquery.min', 'product'), false));  				
?>
<?php
   echo($this->Form->create('Product', array('url'=>'/search','name'=>'search', 'id'=>'search', 'onsubmit'=>'return searchProduct()')));
?>
<div class="SearchBlock">
<div class="InputBlock">
<?php
    echo($this->Form->input('Product.search', array('value'=>'Search here', 'div'=>false, 'label'=>false, 'onfocus'=>"this.value==this.defaultValue?this.value='':null" , 'onblur'=>"this.value==''?this.value=this.defaultValue:null;")));
?>
</div>
<div class="FloatLeft">
   <?php echo($this->Form->submit('search_icone.jpg'));?>
</div>
<div class="Clear"></div>
</div>
<?php
  echo($this->Form->end());
?>