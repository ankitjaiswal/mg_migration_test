<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<?php 
$facecm = '';
$phonecm = '';
$videocm = '';
$facevalue;
$phonevalue;
$videovalue;
if(isset($this->data['Communication'][0])){
		if($this->data['Communication'][0]['mode_type'] == 'face_to_face'){
			$facecm = 'checked';
			$facevalue = $this->data['Communication'][0]['mode'];
		}else if($this->data['Communication'][0]['mode_type'] == 'phone'){
			$phonecm = 'checked';
			$phonevalue = $this->data['Communication'][0]['mode'];
		}else if($this->data['Communication'][0]['mode_type'] == 'video'){
			$videocm = 'checked';
			$videovalue = $this->data['Communication'][0]['mode'];
		}
}
if(isset($this->data['Communication'][1]['mode_type'])){
		if($this->data['Communication'][1]['mode_type'] == 'face_to_face'){
			$facecm = 'checked';
			$facevalue = $this->data['Communication'][1]['mode'];
		}else if($this->data['Communication'][1]['mode_type'] == 'phone'){
			$phonecm = 'checked';
			$phonevalue = $this->data['Communication'][1]['mode'];
		}else if($this->data['Communication'][1]['mode_type'] == 'video'){
			$videocm = 'checked';
			$videovalue = $this->data['Communication'][1]['mode'];
		}
}
if(isset($this->data['Communication'][2]['mode_type'])){
		if($this->data['Communication'][2]['mode_type'] == 'face_to_face'){
			$facecm = 'checked';
			$facevalue = $this->data['Communication'][2]['mode'];
		}else if($this->data['Communication'][2]['mode_type'] == 'phone'){
			$phonecm = 'checked';
			$phonevalue = $this->data['Communication'][2]['mode'];
		}else if($this->data['Communication'][2]['mode_type'] == 'video'){
			$videocm = 'checked';
			$videovalue = $this->data['Communication'][2]['mode'];
		}
}
if(isset($this->data['Communication'][0]['id'])){
	echo($this->Form->hidden('Communication.0.id'));
}
if(isset($this->data['Communication'][1]['id'])){
	echo($this->Form->hidden('Communication.1.id'));
}
if(isset($this->data['Communication'][2]['id'])){
	echo($this->Form->hidden('Communication.2.id'));
}
?>
<div id="face-mode" style="padding-top:20px;">
    <div id="mode">
        <p><b>Mode</b></p>
    </div>
  
    <div id="face-box">
        <p class="styled-form">
            <input type="checkbox" name="data[Communication][0][mode_type]" id="a2" value="face_to_face" <?php echo($facecm); ?> />
            <label for="a2"><img src="<?php echo(SITE_URL)?>/imgs/locator-pin.png" style="margin-right:2px;width: 16px;height: 16px;">In person</label>
        </p>
        <p class="styled-form">
            <input type="checkbox" name="data[Communication][1][mode_type]" id="a1" value="phone" <?php echo($phonecm); ?> />
            <label for="a1"><img src="<?php echo(SITE_URL)?>/imgs/phone.png" style="margin-right:2px;width: 16px;height: 16px;">Phone</label>
        </p>
       <p class="styled-form">
            <input type="checkbox" name="data[Communication][2][mode_type]" id="a3" value="video" <?php echo($videocm); ?> />
            <label for="a3"><img src="<?php echo(SITE_URL)?>/imgs/computer.png" style="margin-right:4px;width: 16px;height: 16px;">Online</label>
        </p>  

       
    </div>
    <div id="face-text">
    	<input name="data[Communication][0][mode]" maxlength="35" class="forminput1" placeholder="e.g. New York, NY" type="text"  id="Communication0Mode" value= "<?php echo ($facevalue)?>">
    	<input name="data[Communication][1][mode]" maxlength="35" class="forminput1" placeholder="Business phone number" type="text"  id="Communication1Mode" value= "<?php echo ($phonevalue)?>">
    	<input name="data[Communication][2][mode]" maxlength="35" class="forminput1" placeholder="e.g. Skype, Google Hangout" type="text"  id="Communication2Mode" value= "<?php echo ($videovalue)?>">
    </div>
</div>