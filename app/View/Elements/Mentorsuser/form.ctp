<table border="0" class="Admin2Table" width="100%">
	<tr class="AddProFrm">
		<td colspan="2"><h3>Account Information</h3></td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">Email as username <span class="input_required">*</span></td>
		<td><?php echo($this->Form->input('User.username', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">Phone Extension</td>
		<td><?php echo($this->Form->input('User.extension', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
	</tr>
	<?php if(0){?>
	<tr>
		<td valign="middle" class="Padleft26">Password <span class="input_required">*</span></td>
		<td><?php echo($this->Form->input('User.password2', array("type" => "password", 'div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">Confirm Password <span class="input_required">*</span></td>
		<td><?php echo($this->Form->input('User.confirm_password', array("type" => "password", 'div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
	</tr>
	<?php } ?>
	<tr class="AddProFrm">
		<td colspan="2"><h3>Contact & Personal Information</h3></td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">First Name <span class="input_required">*</span></td>
		<td><?php echo($this->Form->input('UserReference.first_name', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>
		</td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">Last  Name</td>
		<td><?php echo($this->Form->input('UserReference.last_name', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>
		</td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">ZIP / Postal code<span class="input_required">*</span> </td>
		<td>
		<?php  echo($this->Form->input('UserReference.zipcode', array('id'=>'keyword','maxlength'=>'5','div'=>false, 'label'=>false, "class" => "Testbox5")));?>		
		</td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">Phone<span class="input_required">*</span></td>
		<td>
			<?php  echo($this->Form->input('UserReference.phone', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>		
		</td>
	</tr>
        <tr>
		<td valign="middle" class="Padleft26">Headline<span class="input_required">*</span></td>
		<td>
			<?php  echo($this->Form->input('UserReference.linkedin_headline', array('div'=>false, 'label'=>false, "class" => "Testbox")));?>		
		</td>
	</tr>
	<tr>
		 <td valign="middle" class="Padleft26">Accepting new clients? </td>
		 <td>
		 <?php echo($this->Form->radio('UserReference.accept_application', array('Y'=>'Yes','N'=>'No'),array('div'=>false,'default'=>'Y','label'=>false,'legend'=>false)));?>
		 </td>
	</tr> 
	<tr>
		<td valign="middle" class="Padleft26">Background Summary<span class="input_required">*</span></td>
		<td>
			<?php  echo($this->Form->input('UserReference.background_summary', array('div'=>false, 'label'=>false, "class" => "Testbox")));?>		
		</td>
	</tr>
        <tr>
		<td valign="middle" class="Padleft26">Engagement overview<span class="input_required">*</span></td>
		<td>
			<?php  echo($this->Form->input('UserReference.feeNSession', array('div'=>false, 'label'=>false, "class" => "Testbox")));?>		
		</td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">Past clients</td>
		<td>
			<?php  echo($this->Form->input('UserReference.past_clients', array('div'=>false, 'label'=>false, "class" => "Testbox")));?>		
		</td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">Initial consultation<span class="input_required">*</span></td>
		<td>
			<?php  echo($this->Form->input('UserReference.fee_first_hour', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>		
		</td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">Regular Sessions Fee <span class="input_required">*</span></td>
		<td>
			<?php  echo($this->Form->input('UserReference.fee_regular_session', array('div'=>false, 'label'=>false, "class" => "Testbox5" ,'style' => 'width:225px')));?> / 
			<?php
				$options = array('1' => 'Hour', '2' => 'Month', '3' => 'Engagement', '4' => 'Day');
				echo $this->Form->select('UserReference.regular_session_per',$options,array('div' =>false,'style' => 'color:#292929;width:70px;','empty' => false));
			?>	

			
		</td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">Paypal Merchant Account</td>
		<td>
			<?php  echo($this->Form->input('UserReference.paypal_account', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>		
		</td>
	</tr>
	<tr>
		 <td valign="middle" class="Padleft26">Type of Member</td>
		 <td>
		 <?php echo($this->Form->input('User.mentor_type', array('options'=>Configure::read('MentorType'),'div'=>false, 'label'=>false, "class" => "TextBox5")));?>
		 </td>
	</tr>	
	<tr>
		 <td valign="middle" class="Padleft26">Status</td>
		 <td>
		 <?php echo($this->Form->input('User.status', array('options'=>Configure::read('Status'),'div'=>false, 'label'=>false, "class" => "TextBox5")));?>
		 </td>
	</tr>
	<tr>
		 <td valign="middle" class="Padleft26">Feedback Link</td>
		 <td>
		 <?php echo($this->Form->input('UserReference.feedback_link', array('div'=>false, 'label'=>false, "class" => "TextBox5")));?>
		 </td>
	</tr>
       <tr>
		 <td valign="middle" class="Padleft26">Intro Video Link</td>
		 <td>
		 <?php echo($this->Form->input('UserReference.intro_link', array('div'=>false, 'label'=>false, "class" => "TextBox5")));?>
		 </td>
	</tr>
	<tr class="AddProFrm">
		<td colspan="2"><h3>Areas of Expertise</h3></td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">Areas of Expertise</td>
		<td>
			<?php  echo($this->Form->input('UserReference.area_of_expertise', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>		
		</td>
	</tr>		
	<tr>
		<td valign="middle" class="Padleft26">Autocomplete expertise</td>
		<td>
			<textarea id="UserReferenceCategoriesTags" class="Testbox" rows="6" cols="30" name="data[UserReference][categories_tags]"><?php echo($this->data['UserReference']['categories_tags'])?></textarea>
		</td>
	</tr>	
       <tr>
		<td valign="middle" class="Padleft26">Business Website</td>
		<td>
			<?php  echo($this->Form->input('UserReference.business_website', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>		
		</td>
	</tr>	
	<?php echo($this->Element('Mentorsuser/social_link')); ?>	
	<?php echo($this->Element('Mentorsuser/resource')); ?>
	<tr class="AddProFrm">
		<td colspan="2"><h3>Desired clients profile</h3></td>
	</tr>
	<tr>
		<td valign="middle" class="Padleft26">Desired clients profile</td>
		<td>
			<?php  echo($this->Form->input('UserReference.desire_mentee_profile', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>		
		</td>
	</tr>	
	
	<?php echo($this->Element('Mentorsuser/mentorship')); ?>
	<?php echo($this->Element('Mentorsuser/communication_mode')); ?>	
	<?php echo($this->Element('Mentorsuser/avaliability')); ?>
	<tr class="AddProFrm">
		<td colspan="2"><h3>Upload Profile Image</h3></td>
	</tr>
	<?php 
	if($this->params['action'] == 'admin_edit' && isset($this->data['UserImage'][0]['image_name'])){?>
		<tr>		
			<td><?php echo($this->Form->file('UserImage1.image_name')); ?></td>
			<td>
			<?php 			
			echo($this->Html->image(MENTORS_IMAGE_PATH.DS.$this->data['User']['id'].DS.$this->data['UserImage'][0]['image_name'],array('alt'=>'profile image','width'=>150,'height'=>150))); 
			?>
			</td>			
		</tr>	
		<?php 
	}else{?>
		<tr>		
			<td colspan="2"><?php echo($this->Form->file('UserImage1.image_name')); ?></td>
		
		</tr>			
		<?php 
	}?>	
	<?php /*?><tr class="AddProFrm">
		<td colspan="2"><h3>Upload Resume</h3></td>
	</tr>
	
	<tr>		
		<td colspan="2"><?php echo($this->Form->file('Resume1.resume_location')); ?></td>			
	</tr>	<?php */?>
	
</table>
