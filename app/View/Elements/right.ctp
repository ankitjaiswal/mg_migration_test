<div class="full expert">
  <h5><span>Expert Advice</span>  
  <?php echo $this->Html->image('green_down_arrow.gif', array('border'=>'0')); ?>
  </h5>
  <div class="full pad4"><?php echo $this->Html->image('expert_img.jpg', array('border'=>'0')); ?></div>
  <h6 class="green mrg1">Advice. Information. Expertise. On Demand.</h6>
  <p>Affordable advice from over 30,000 experts available in real time. Choose from one of hundreds of categories or <a href="javascript:;">search for an expert today!</a></p>
</div>
<div class="full featured">
  <h5><span>Expert Advice</span>
  <?php echo $this->Html->image('blue_down_arrow.gif', array('border'=>'0')); ?>
  </h5>
  <ul>
    <li> 
		<?php echo $this->Html->image('img01.jpg', array('border'=>'0','class'=>'img')); ?>
      	<div class="featured_contant">
			<p>Business &amp; Finance</p>
			<?php echo $this->Html->link(__('Chat Live', true), '/experts/listing/2/Business-&-Finance'); ?>			
		</div>
    </li>
    <li>		
		<?php echo $this->Html->image('img02.jpg', array('border'=>'0','class'=>'img')); ?>
      <div class="featured_contant">
        <p>Computers &amp; Programming</p>
        <?php echo $this->Html->link(__('Chat Live', true), '/experts/listing/4/Computers-&-Programming'); ?> 
	  </div>
    </li>
    <li> 
		<?php echo $this->Html->image('img03.jpg', array('border'=>'0','class'=>'img')); ?>
      <div class="featured_contant">
        <p>Coaching &amp; Personal Development</p>
        <?php echo $this->Html->link(__('Chat Live', true), '/experts/listing/3/Coaching-&-Personal-Development'); ?>  
	  </div>
    </li>
    <li> 
		<?php echo $this->Html->image('img04.jpg', array('border'=>'0','class'=>'img')); ?>
      <div class="featured_contant">
        <p>Education &amp; Tutoring</p>
        <?php echo $this->Html->link(__('Chat Live', true), '/experts/listing/5/Education-&-Tutoring'); ?>
	  </div>
    </li>
  </ul>
</div>
<div class="full featured">
  <!--<div class="menu_right"> -->
  <div >   
    <ul>
    	<li>
				<a class="hide" href="#"></a>
				<ul><li id="ddf">
				<?php		
echo $form->select('parent_id',$parentCategories,array(''), array('escape'=>false,'empty'=>'All Categories'),false); 
					?>
					
				</li></ul>
		</li>
    </ul>    
    </div>
</div>