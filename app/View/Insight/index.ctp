<?php
$pageLimit = 5;
$data_flag = true;
//$num = 0;
?>

    <!-- middle part -->
    <div id="inner-content-box" class="pagewidth relatiVE">

        <div class="orMIDDLE">Or</div>
        <div class="btLINE"></div>
    </div>

    <div id="inner-content-box" class="pagewidth headH2"> 

	 
	<?php $qCount = count($insights); if($qCount > 0){ ?>
            

     <div style="margin-top: 20px;float:right;">

         <input id="applyNext" class="mgButton" style="width: 200px;" type="Button" value="Share your insight" onclick='javascript:open_form();'></td>
        </div>

                          <div  style="display:none;"  id ="insightform">
        			<div class="clear"></div>
        				<div class="subheading supreFREE" id="heading">
          					<h2>Share your insight <sub style="font-size: 11px; font-weight: normal; font-family: Ubuntu">Advice for business leaders</sub></h2>
        				</div>
        			<div class="clear"></div>

                 	     	
					<div id="feedFetchDiv" style="display: inline-block;">
						<div class="expertise" style="margin-top: 15px;">
			                     <p><input id="feed_url" class="inpT" type="text" style="width:938px;" placeholder="Paste a URL..." name="data[url]"/></p>
			                     </div>
			                     <div class="editSUB" style="margin-top: 0px;">
                                              <a style="float: left; margin-left: 725px;" href="javascript: void(0)" id="cancelLink" onClick="on_cancel();">Cancel</a>
					       <input class="feed_btn" type="button" style="width:151px !important;" value="Fetch Content" onclick="fetch_data();">
						</div>
					</div>

					<div id="feedSaveDiv" style="display:none;">
						<div class="expertise" style="margin-top:-15px;">
			                    
                                             <p>
                                                <input id="feed_favicon" class="inpT" type="hidden" name="data[favicon]" value=""/>
                                               <img id="feed_image" src="<?php echo(SITE_URL);?>img/<?php echo($image_path);?>" style="height:200px;"/></p>
                                               </br>
                                                <h1>Title <span style="float:right; font-weight: normal;"></h1>
			                      <p><input id="feed_title" class="inpT" type="text"style="width:651px;" placeholder="Title" name="data[title]" value=""/></p>
                                           
			                     </div>

                                              <span style="font-size: 15px;color: #2a2a2a;">Your insight</span> <span style="float:right;"id="noSpan" >(Minimum <span id="noOfCharInsight"><?php echo(500 - strlen($Insight_insight))?></span> more characters)</span>
			                     <div class="QHINTarea">
			                      <p><textarea id="feed_summary" class="txtAR" rows="4" style="height:100px;width:659px;" type="text"  placeholder="Enter your commentary here..." name="data[summary]" value=""/></textarea></p>
			                     </div>

			                    <div class="editSUB" style="margin-top: 0px;">
                                         
                                         <a href="javascript: void(0)" id="cancelLink" onClick="on_cancel();">Cancel</a>
                                         <input class="mgButton" id="applyPreviewIn" type="submit" style="width:150px !important; margin-top: -11px;" value="Preview" onclick="save_feed_data();" disabled="disabled">
                                         
	                                  </div>

					</div>  
      			</div>

     <div id="recent-insight">
   
<div class="profile-nav" style="margin-top: 50px;">
		<ul>
		    <li id="bg1" class="profile-nav-current"><a href="#recent"  onclick="tog1();" style="font-size:16px;font-weight:bold;">Recent</a></li>
		    <li id="bg2" class="profile-hover-current"><a href="#popular" onclick="tog2();" style="font-size:16px;font-weight:bold;">Popular</a></li>
</ul>
    
    
    <div id="inner-content-box" class="pagewidth headH2">



			<div id="inner-content-box" class="pagewidth headH2" style="padding-bottom: 0px;margin-top:20px;">
				<table id="results" style="width:100% !important;">

    <?php $countA = 1; foreach ($insights as $key => $insight) {
                       

			if($key%$pageLimit == 0){
				$num = floor($key/$pageLimit);
			}

            ?>
                                       
                                         <?php if (strpos($insight['Insight']['post_url'], "www")!=false){
                                     $pos = strpos($insight['Insight']['post_url'], '.');
                                   
                                      $pos1 = strpos($insight['Insight']['post_url'], '/');

                                      $pos2 = strpos($insight['Insight']['post_url'],'/', $pos1 + strlen('/'));

                                      $pos3 = strpos($insight['Insight']['post_url'],'/', $pos2 + strlen('/'));

                                      $final =$pos3 - $pos;

                                      $url = substr($insight['Insight']['post_url'],$pos+1,$final-1);
                                       }
                                      else{
                                          $pos1 = strpos($insight['Insight']['post_url'], '/');
                                         $pos2 = strpos($insight['Insight']['post_url'],'/', $pos1 + strlen('/'));
                                        $pos3 = strpos($insight['Insight']['post_url'],'/', $pos2 + strlen('/'));
                                        $final =$pos3 - $pos2;
                                      $url = substr($insight['Insight']['post_url'],$pos2+1,$final-1);
                                      }
                                    ?>

					<tr class="set_<?php echo($num); ?>">
						<td>
							<div class="box1">
						 		<div class="content-left-img QnAconsult" style="width: 130px;"> 
                 <?php 
            		if(isset($insight['User']['user_image'])){
            			$image_path = MENTORS_IMAGE_PATH.DS.$insight['Insight']['user_id'].DS.$insight['User']['user_image'];?>
            			<a href="<?php echo(SITE_URL.strtolower($insight['User']['url_key']));?>">
            				<img src="<?php echo(SITE_URL.'img/'.$image_path)?>" width="100" height="100" alt="<?php echo($insight['User']['first_name']." ".$insight['User']['last_name']);?>" title="<?php echo($insight['User']['first_name']." ".$insight['User']['last_name']);?>"/>
            			</a>
            		<?php } else {
            			$image_path = 'profile.png';?>
            			<a href="<?php echo(SITE_URL.strtolower($insight['User']['url_key']));?>">
            				<img src="<?php echo(SITE_URL.'img/media/'.$image_path)?>" width="100" height="100" alt="<?php echo($insight['User']['first_name']." ".$insight['User']['last_name']);?>" title="<?php echo($insight['User']['first_name']." ".$insight['User']['last_name']);?>"/>
            			</a>
            		<?php }
                 ?>

						  				<br/>
						  					<input id="button<?php echo($insight['Insight']['id']) ?>" class="mgButton" type="button" value="Upvote | <?php echo($insight['Insight']['upvotes']);?>" style="margin-top: 5px; width: 100px; font-weight: normal" onclick="upVote(<?php echo($insight['Insight']['id']) ?>);">
						  			</div>
					
								<div class="content-right-text" style="width: 824px;margin-top:30px;">
            <p>
                <a href="<?php echo(SITE_URL.'insight/insight/'.$insight['Insight']['url_key'])?>" class="questionA"><?php echo($insight['Insight']['title'])?> </a>
               <?php  echo($this->Html ->link("Read more",SITE_URL.'insight/insight/'.$insight['Insight']['url_key'],array('escape'=>false,'class'=>'readMore','title'=>'View insight')));?>
            </p>
            <div class="QnAbutton">
            	<?php foreach ($insight['Insight_category'] as $catValue) {

                                                            $str = ($categories[$catValue['category_id']]);
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"&"))
                                                            $str = str_replace("&","_and",($categories[$catValue['category_id']]));
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"/"))
                                                            $str = str_replace("/","_slash",($categories[$catValue['category_id']]));
                                                            $str = str_replace(" ","-",$str);
            		 echo($this->Html ->link($categories[$catValue['category_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false,'title'=>$categories[$catValue['category_id']])));
            		
                 }?>
                <br/><br/>
                    <?php 

                        $date = new DateTime($insight['Insight']['created']);
                        $result = $date->format('Y-m-d');
                        
                        $path = explode("-", $result); // splitting the path
                        $month = $path[1]; // get the value of the last element 
                        $monthName = date("F", mktime(0, 0, 0, $month, 10));
                        ?>
                  <img src="<?php echo($insight['Insight']['feed_favicon'])?>" style="height:16px;"> &nbsp;<span style="font-size:12px;color:gray;"><?php echo($url);?> | <?php echo($monthName);?>&nbsp;<?php echo($path[2]);?>,&nbsp;<?php echo($path[0]);?></span>
            </div>



						</td>
					</tr>
                                <?php $countA++; }?>
				</table>
			</div>

                
            
            <?php if($key == ($qCount - 1)) { ?>
            <div class="HRLine" id="pagewidth" style="border-top: 0px;"></div>
            <?php }else {?>
            <div class="HRLine" id="pagewidth"></div>
            <?php }?>
        </div>
		

    <div style="width:81%; float:right; padding-bottom:30px; text-align:center">
	<?php
		if($num >0){
			$style ="background: none repeat scroll 0 0 #D03135; color: #FFFFFF; cursor: pointer; font-weight: bold; height: 14px; text-align: center; margin:auto; padding: 10px; width:50%;";
			?>
				<div id="openRegisterNav" class="more-button-search">
		        <input type="button" value="MORE INSIGHTS"></div>
			<?php
		}
	?>
	</div>
	</div>
	<?php }?>
</br></br></br>
    </div>
</div>
</div>
<?php
if($data_flag){?>	
	<?php echo($this->Form->hidden('num',array('value'=>$num,'id'=>'num'))); ?>
	<?php echo($this->Form->hidden('countShow',array('id'=>'countShow'))); ?>
	<?php
}else{?>
	<?php echo($this->Form->hidden('num',array('value'=>'NOT','id'=>'num'))); ?>
	<?php
}?>

<script type="text/javascript">
    function open_form(){

 document.getElementById("insightform").style.display="block";

}

function on_cancel(){
 document.getElementById("insightform").style.display="none";
 document.getElementById('applyNext').style.display="block"; 
 document.getElementById('recent-insight').style.display="block";
 document.getElementById('applyNext').scrollIntoView();

}

</script>

<script>
jQuery(document).ready(function(){

	var qText = jQuery("#feed_summary").val();
	if(qText.length > 500) {
		 jQuery("#noSpan").hide();
         document.getElementById('applyPreviewIn').disabled = false;
	}
	

	    

	    function updateCountInsight()
	    {
	        var qText = jQuery("#feed_summary").val();

	       if(qText.length < 500) {
	    	   jQuery("#noOfCharInsight").html(500 - qText.length);
	    	   jQuery("#noSpan").show();
	    	   document.getElementById('applyPreviewIn').disabled = true;
	       } else {
	           jQuery("#noSpan").hide();
	           document.getElementById('applyPreviewIn').disabled = false;
	       }
	    }

	    jQuery("#feed_summary").keyup(function () {
	    	updateCountInsight();
	    });

 });
</script>
<script type="text/javascript">
var openRegister = 0;

   	function fetch_data(){
		var feed_url = jQuery("#feed_url").val();

		jQuery(document).ready(function(){
			jQuery.ajax({
				url:SITE_URL+'insight/activity_feed',
				type:'post',
				dataType:'json',
				data:'feed_url='+feed_url,
				success:function(res){
					if(res.title !=''){
						document.getElementById('feedFetchDiv').style.display="none";  
                                                document.getElementById('applyNext').style.display="none"; 
                                                document.getElementById('recent-insight').style.display="none";

						document.getElementById('feedSaveDiv').style.display="inline-block";  
                                          var logo = document.getElementById('feed_image');
                                          logo.setAttribute('src', res.image);

                                          
						jQuery('#feed_title').val(res.title);
                                                jQuery('#feed_favicon').val(res.favicon_url);
                                                document.getElementById('heading').scrollIntoView();

					}else{
						alert("Failure!!!");
					}
				}
			});
		});
		
   	}




    function save_feed_data(){
		var feed_url = jQuery("#feed_url").val();
		var str = jQuery("#feed_title").val();
	        var feed_favicon = jQuery("#feed_favicon").val();

            
                var feed_image = document.getElementById("feed_image").src;
              
                if(str.indexOf("|") !=-1) {
                  var split = str.split("|"); 
                  var feed_title = split[0];
                 }
               else
                  {
                var feed_title = jQuery("#feed_title").val();
                 }
               var feed_summary = jQuery("#feed_summary").val();


             
		jQuery(document).ready(function(){
			jQuery.ajax({
				url:SITE_URL+'insight/save_feed',
				type:'post',
				dataType:'json',
				data:'feed_url='+feed_url + '&feed_title=' + feed_title + '&feed_summary=' + feed_summary + '&feed_image=' + feed_image + '&feed_favicon=' + feed_favicon,
				success:function(res){
					if(res.id !=''){
                                        window.location.href = SITE_URL+'insight/preview';


					}else{
						alert("Failure!!!");
					}
				}
			});
		});
		
   	}


function upVote(id){
	
	jQuery.ajax({
				url:SITE_URL+'insight/upvote/'+id,
				type:'post',
				dataType:'json',
				success:function(res){
					if(res.value == -1){
						loginpopup();
					} else
						jQuery('#button'+id).val('Upvote | ' + res.value);
				}
			});	
}

	// more mentor show functionality
	//history.navigationMode = 'compatible';
	jQuery(document).ready(function(){
		
		var num_tot = parseInt(jQuery("#num").val(),10);
		
		for(var iii = 0; iii <= num_tot; iii++){
			if(iii != 0)
				jQuery(".set_"+iii).hide();
		}

		var num = jQuery("#num").val();	
		if(num == 0) {
			jQuery("#user-account").show();
		}

		if(num == 'NOT') {
			jQuery("#user-account").show();
		}
		
		if(num !='NOT'){			
			jQuery(".set_0").show();
			jQuery("#countShow").val(0);	
		}
		var old_countShow = readCookie('cookie_countShow')
		if (old_countShow) {
			for(showCnt=0;showCnt<old_countShow;showCnt++){
				jQuery("#countShow").val(showCnt);
				paging_hack();		
			}
			
		}	
		eraseCookie('cookie_countShow');
	});

		jQuery("#openRegisterNav").click(function(){
                
		if(loggedUserId == ''){
			if(openRegister == 0) {
 
				loginpopup();
				return;
			}
		} 
		
		var num = parseInt(jQuery("#num").val(),10);	
		var countShow = parseInt(jQuery("#countShow").val(),10) + 1;
		
		if(num > countShow){			
			jQuery(".set_"+countShow).show();
			jQuery("#countShow").val(countShow);	
		}else if(num == countShow){
			jQuery(".set_"+countShow).show();
			jQuery("#countShow").val(countShow);
			jQuery("#openRegisterNav").hide();
			jQuery("#user-account").show();
		}
	});
	
	function paging_hack(){
		var num = parseInt(jQuery("#num").val(),10);	
		var countShow = parseInt(jQuery("#countShow").val(),10) + 1;
		createCookie('cookie_countShow',countShow,0);
		
		if(num > countShow){			
			jQuery(".set_"+countShow).show();
			jQuery("#countShow").val(countShow);	
		}else if(num == countShow){
			jQuery(".set_"+countShow).show();
			jQuery("#countShow").val(countShow);
			jQuery("#openRegisterNav").hide();
		}
	}

</script>
<style>
#pagewidth {
    width: 960px;
    margin: 0 auto;
    clear: both;
}
</style>