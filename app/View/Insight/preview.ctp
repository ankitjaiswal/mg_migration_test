<?php if(isset($userData['UserImage'][0]['image_name']) && $userData['UserImage'][0]['image_name'] !=''){
$image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$userData['User']['id'].DS.$userData['UserImage'][0]['image_name'];
} else {
$image_path = SITE_URL.'img/media/profile.png';
}?>
<script type="text/javascript">
    var topicVal = [-1,-1,-1];
    var topicCount = 0;
    		
    function monkeyPatchAutocomplete() {
    
        jQuery.ui.autocomplete.prototype._renderItem = function( ul, item) {
            var re = new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + this.term + ")(?![^<>]*>)(?![^&;]+;)", "gi");
            var t = item.label.replace(re,"<span style='font-weight:bold;color:#434343;'>" + 
            		"$&" + 
                    "</span>");
            return jQuery( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a>" + t + "</a>" )
                .appendTo( ul );
        };
    }
    
    function getKeywords(){
    
    		var allKeywords = <?php echo json_encode($allKeywords); ?>;
    
    		return allKeywords;
    	}
    	
    function deleteThis(element) {
    	console.log("Delete This");
    	
    	topicVal[element] = -1;
    	
    	for( var i = 0 ; i < 3 ; i ++) {
    		
    		if(topicVal[i] == -1) {
    			jQuery('#div'+i).css('display','none');
    			jQuery('#topic'+i).html('');
    		} else {
    			jQuery('#div'+i).css('display','inline-block');
    			jQuery('#topic'+i).html(topicVal[i]);
    		}
    	}
    }
    	
    jQuery(document).ready(function(){
    
    
    		var hidVal = jQuery('#hiddenSelect').val();
    
    		if(hidVal != ''){
    			var hidValArr = hidVal.split(",");
    			var total = 0;
    			for(var i = 0; i < hidValArr.length; i++){
    				topicVal[i] = hidValArr[i];
    				jQuery('#div'+i).css('display','inline-block');
    				jQuery('#topic'+i).html(hidValArr[i]);
                                    total++;
    			}
    
    		}
    
    	
    		monkeyPatchAutocomplete();
    		
    		var CityKeyword = jQuery('#CityKeyword');
    	
    		CityKeyword.autocomplete({
    		minLength    : 1,
    		source        : getKeywords(),
    		source		: function(request, response) {
    			
    		        var results = jQuery.ui.autocomplete.filter(getKeywords(), request.term);
    		        
    		        results = results.slice(0, 3);
                            results.push('Add <b>"' + jQuery("#CityKeyword" ).val() + '"</b>');
    		        response(results); //show 3 items.
    		    },
    		focus: function( event, ui ) {
    			
    			if(ui.item.value[0] == "A")
    				return false;
    	       //jQuery("#CityKeyword" ).val( ui.item.value );
    	       return false;
    	     },
    	     select: function( event, ui ) {
    
    			     var toAdd = ui.item.value;
                                  if(ui.item.value[1] == "d"){
                                    var total = toAdd.length;
                                    var final = total-5;
    				toAdd = toAdd.substring(8,final);
                                     }
    
    				
    			var changed = -1;
    			
    			for (var i = 0; i < 5; i++) {
    				
    				if(topicVal[i] == -1) {
    					changed = 1;
    					topicVal[i] = toAdd;
    					break;
    				}
    			}
    			
    			if(changed == -1)
    				alert("You can only select 3 values");
    			
    			for( var i = 0 ; i < 3 ; i ++) {
    		
    				if(topicVal[i] == -1) {
    					jQuery('#div'+i).css('display','none');
    					jQuery('#topic'+i).html('');
    				} else {
    					jQuery('#div'+i).css('display','inline-block');
    					jQuery('#topic'+i).html(topicVal[i]);
    				}
    			}
    			
    	        jQuery("#CityKeyword" ).val('');
    	          
    	        return false;
    	       }
    		});
    	});
    
</script>
<div id="proposal-page" class="inner-content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
	            <form action="<?php echo SITE_URL ?>insight/add_insight" method="post" id="Insightformpreview">
	                <input type="hidden" name="data[Insight][title]" value="<?php echo($Insight_title);?>"/>
	                <input type="hidden" name="data[Insight][id]" id="qId" value="<?php echo($Insight_id);?>"/>
	                <input type="hidden" name="data[Insight][image]" id="qImage" value="<?php echo($Insight_image);?>"/>
	                <input type="hidden" name="data[Insight][posturl]" id="qURL" value="<?php echo($Insight_posturl);?>"/>
	                <input type="hidden" name="data[Insight][favicon]" id="qfavicon" value="<?php echo($Insight_favicon);?>"/>

                        <?php
                        //$date = date('Y-m-d');
                        $result = date('Y-m-d');
                        
                        $path = explode("-", $result); // splitting the path
                        $month = $path[1]; // get the value of the last element
                        $monthName = date("F", mktime(0, 0, 0, $month, 10));
                        ?>

	                <?php if (strpos($Insight_posturl, "www")!=false){
	                    $pos = strpos($Insight_posturl, '.');
	                    
	                    $pos1 = strpos($Insight_posturl, '/');
	                    
	                    $pos2 = strpos($Insight_posturl,'/', $pos1 + strlen('/'));
	                    
	                    $pos3 = strpos($Insight_posturl,'/', $pos2 + strlen('/'));
	                    
	                    $final =$pos3 - $pos;
	                    
	                    $url = substr($Insight_posturl,$pos+1,$final-1);
	                     }
	                    else{
	                        $pos1 = strpos($Insight_posturl, '/');
	                       $pos2 = strpos($Insight_posturl,'/', $pos1 + strlen('/'));
	                      $pos3 = strpos($Insight_posturl,'/', $pos2 + strlen('/'));
	                      $final =$pos3 - $pos2;
	                    $url = substr($Insight_posturl,$pos2+1,$final-1);
	                    }
	                    ?>
	                <!-- <div class="box-wrap">
	                    <h2 class="preview-title">Insight Preview</h2>
	                </div> -->
	                <div class="preview-banner">
	                	<div class="preview-img-banner">
	                	<?php if(strpos($Insight_image, "guild.im")!=false){?>
	                		<div class="img-gradient">
                                <img class="img-gradient-img" src="<?php echo($Insight_image);?>" name="data[Insight][image]">
	                		</div>
	                		<?php }else{?>
	                		<div class="banner-photo" style="background:url('<?php echo($Insight_image);?>');" name="data[Insight][image]"></div>
	                		<?php }?>
	                		<div class="banner-content img">
	                		    <div  class="banner-title" >
	                		        <h5><a href="javascript:void(0);">Preview: <?php echo($Insight_title) ?></a></h5>
                                        <div class="org-date">
                                            <span><?php echo($monthName);?>&nbsp;<?php echo($path[2]);?>,&nbsp;<?php echo($path[0]);?></span>
	                		        <span class="hbr"><img src="<?php echo($Insight_favicon);?>">&nbsp;&nbsp;<a href="<?php echo($Insight_posturl);?>" target="_blank"><?php echo($url);?></a></span>
	                		    </div>
	                		</div>
                                       </div>
	                	</div>
	                </div>
	                
	                <div class="project-overview box-wrap">
	                        <div class="user-img">
                                    <a href="<?php echo(SITE_URL.strtolower($userData['User']['url_key']));?>">
                                        <img class="desaturate" src="<?php echo($image_path);?>" alt="<?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?>" title="<?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?>"/>
                                    </a>
                            </div>
                            <div class="user-info">
                                <h2 class="title-medium">
                                    <a href="<?php echo(SITE_URL.strtolower($userData['User']['url_key']));?>" title="<?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?>"><?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?></a>
                                </h2>
                                <p><?php  echo(ucfirst($userData['UserReference']['linkedin_headline']));?></p>
                                <div class="answer">
                                    
                                </div>
                            </div>

                            <hr>
	                        <p><?php { 
	                            $editthis = '<div class="ql-editor" contenteditable="true"';
	                            $editby= '<div class="ql-editor" contenteditable="false"';
	                            $answer = str_replace($editthis, $editby, $Insight_insight);
	                            
	                            $editthis1 = 'target="_blank" style="color: rgba(0, 0, 0, 0.8);"';
	                            $editby1 = 'target="_blank" style="color: #D03135;"';
	                            $answer = str_replace($editthis1, $editby1, $answer);
	                            
	                            echo (nl2br(htmlspecialchars_decode($answer)));}?>
                            </p>
	                    <div class="form-group" data-placement="right" data-toggle="DescPopover" data-container="body">
	                        <input type="text" id="CityKeyword" class="form-control" placeholder="Add topics for industry, function or skills" name="data[City][keyword]">
	                    </div>
	                    <div id="div0" class="form-group pop" style="display: none;">
	                        <span id='close' onclick='deleteThis(0); return false;' style="display:inline-block;">x</span>
	                        <p id="topic0" style="display: inline-block"></p>
	                    </div>
	                    <div id="div1" class="form-group pop" style="display: none;">
	                        <span id='close' onclick='deleteThis(1); return false;' style="display:inline-block;">x</span>
	                        <p id="topic1" style="display: inline-block""></p>
	                    </div>
	                    <div id="div2" class="form-group pop last" style="display: none;">
	                        <span id='close' onclick='deleteThis(2); return false;' style="display:inline-block;">x</span>
	                        <p id="topic2" style="display: inline-block"></p>
	                    </div>

	                    <?php echo($this->Form->hidden('Insight.select',array('value'=>'', 'id'=>'hiddenSelect')));?>
                        <div class="e-button">
                            <label>
                            <a href="<?php echo(SITE_URL.'insight/edit/')?>">Edit</a>
                            </label>
                            <button class="mgButton btn btn-primary" id="mgButton">
                            Publish your insight
                            </button>
                        </div>
                    </div>
	        	</form>
            </div>
        </div>
    </div>
</div>
<!-- js link -->
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
	
<script type="text/javascript">
    jQuery(document).ready(function(){
    	jQuery("#mgButton").click(function(){
    
    
        	var flag = 0;
    
    		var myValues;
    	
    		for (var i = 0 ; i < 3 ; i++) {
    			
    			if(topicVal[i] != "-1") {
    				if( i == 0)
    					myValues = topicVal[i];
    				else
    					myValues = myValues + "," + topicVal[i];
    			}
    			
    		}
    		
    		jQuery('#hiddenSelect').val(myValues);
    
    		if(flag > 0) {
    			return false;
    		} else {
    			jQuery("#Insightformpreview").submit();
    			return true;
    		}
        });
    });
</script>
