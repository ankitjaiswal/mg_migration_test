<?php if(isset($Insight_id)){?>
	                	<input type="hidden" name="data[Insight][id]" value="<?php echo($Insight_id);?>"/>
                                <input type="hidden" name="data[Insight][favicon]" value="<?php echo($Insight_favicon);?>"/>
                                <input type="hidden" name="data[Insight][posturl]" value="<?php echo($Insight_posturl);?>"/>
                                <input type="hidden" name="data[Insight][image]" value="<?php echo($Insight_image);?>"/>
<?php }?>

	<div class="container-fluid" id="apply-section">
		<div class="container create-section pad0">
			<div class="col-md-8 pad40">
				<div class="apply-heading">
					<h5>Share your insight</h5>
					<label class="free-c"> ( Advice for business leaders )</label>
				</div>
				<div class="apply-form">
					<form action="<?php echo SITE_URL ?>insight/preview" method="post" id="AddformIn">
						<div class="insight-img">
							<img id="feed_image" src="<?php echo($Insight_image);?>">
						</div>
						<div class="form-group">
							<label>Title</label>
							<span class="pull-right char-limit">(<span id="noOfCharIn"><?php echo(128 - strlen($Insight_title))?></span> characters remaining)</span>
							<input type="text" class="form-control" placeholder="Be creative and clear" id="titleTextBox" name="data[Insight][title]" value="<?php echo(htmlspecialchars($Insight_title))?>">
						</div>
                        <div class="form-group" data-placement="right" data-toggle="DescPopover" data-container="body">
                            <label>Your insight</label>
							<span class="pull-right char-limit" id="noSpan">(Minimum <span id="noOfCharInsight"><?php echo(500 - strlen($Insight_insight))?></span> more characters)</span>
                            <input type="hidden" name="data[Insight][insight]"  id="insighttext" value="<?php echo(htmlspecialchars($Insight_insight))?>"/>
                            <div id="editor-container"></div>
                            <div id="popover-content1" class="hide">
								<p>1. Present the idea in a nutshell.</p>
								<p>2. Draw upon your past experience. </p>
								<p>3. Keep it simple and practical.</p>
							</div>
                        </div>
						<div class="e-button pull-right">
							<label>
								<a href="<?php echo(SITE_URL)?>qanda">
									Cancel
								</a>
							</label>
							<button class="banner-getstarted btn btn-default get-started" id="applyPreviewIn">
								Preview
							</button>
						</div>
					</form>
				</div>
			</div>

			<div class="col-md-4 clearboth create-right">
				<div class="right-apply">
					<div class="apply-heading">
				         <h5>Thought leadership made easier</h5>
					</div>
					<div class="apply-desc">
						<div class="cr-desc">
							<h5>1. Leverage great content</h5>
							<p>Paste the URL of trusted business content to "pull".</p>
						</div>
						<div class="cr-desc">
							<h5>2. Add your voice</h5>
							<p>Summarize in your words the content you want to share.</p>
						</div>
						<div class="cr-desc">
							<h5>3. Promote your insights</h5>
							<p>Use social media to promote your insights, and we will do the same.</p>
						</div>
					</div>


				</div>
			</div>

		</div>
	</div>
	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/quill.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>sanchit/js/insighteditscript.js"></script>


	