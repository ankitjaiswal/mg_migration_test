<?php
if(isset($opennewloginpopup)){?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
		jQuery('#signin-Modal').modal();
		});
	</script>
<?php
}?>
<?php $completelink = SITE_URL."insight/insight/".($insight['Insight']['url_key']);?>
<?php $tinyUrl = $this->General->get_tiny_url(urlencode($completelink));?>
<?php $atMentorsGuild = "@GuildAlerts: "?>
<?php $atsybmol = "@"?>
<?php $MentorsGuild = "GUILD: "?>
<?php $compTitle = $insight['Insight']['title'];?>
<?php $linkTitle = $insight['Insight']['title'];?>
<?php if(isset($userData['UserImage'][0]['image_name']) && $userData['UserImage'][0]['image_name'] !=''){
$image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$userData['User']['id'].DS.$userData['UserImage'][0]['image_name'];
} else {
$image_path = SITE_URL.'img/media/profile.png';
}?>
<?php
    if(false !== stripos($compTitle,"'")){
    $compTitle = str_replace("'"," ",$compTitle);
}
    if(false !== stripos($compTitle,'"')){
    $compTitle = str_replace('"'," ",$compTitle);
}
    if(false !== stripos($compTitle,'&')){
    $compTitle = str_replace('&',"and",$compTitle);
}
?>
<div id="inner-content-wrapper" class="inner-content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="inner-content-box" class="pagewidth">
                    <div id="content-submit" class="content-submit">
                        <?php
                        $date = new DateTime($insight['Insight']['created']);
                        $result = $date->format('Y-m-d');
                        
                        $path = explode("-", $result); // splitting the path
                        $month = $path[1]; // get the value of the last element
                        $monthName = date("F", mktime(0, 0, 0, $month, 10));
                        ?>
                        <?php if (strpos($insight['Insight']['post_url'], "www")!=false){
                        $pos = strpos($insight['Insight']['post_url'], '.');
                        
                        $pos1 = strpos($insight['Insight']['post_url'], '/');
                        $pos2 = strpos($insight['Insight']['post_url'],'/', $pos1 + strlen('/'));
                        $pos3 = strpos($insight['Insight']['post_url'],'/', $pos2 + strlen('/'));
                        $final =$pos3 - $pos;
                        $url = substr($insight['Insight']['post_url'],$pos+1,$final-1);
                        }
                        else{
                        $pos1 = strpos($insight['Insight']['post_url'], '/');
                        $pos2 = strpos($insight['Insight']['post_url'],'/', $pos1 + strlen('/'));
                        $pos3 = strpos($insight['Insight']['post_url'],'/', $pos2 + strlen('/'));
                        $final =$pos3 - $pos2;
                        $url = substr($insight['Insight']['post_url'],$pos2+1,$final-1);
                        }
                        ?>
                        <?php if ($url == ''){
                        $url = "guild.im";
                        }
                        if($insight['Insight']['feed_favicon'] == ''){
                        $favicon = "https://test.guild.im/imgs/mg_monogram1.png";
                        }
                        else{
                        $favicon = $insight['Insight']['feed_favicon'];
                        }
                        if($insight['Insight']['image'] =='' || (strpos($insight['Insight']['image'], "guild.im")!=false)){
                        $image = $image_path;
                        }else{
                        $background_image = $insight['Insight']['image'];
                        
                        }?>
                        <!-- Contact modal -->
						<div class="modal fade" id="squarespaceModalinsight" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
						  <div class="modal-dialog question">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
									<h3 class="modal-title" id="lineModalLabel">Share an insight</h3>
								</div>
								<form action="<?php echo SITE_URL ?>insight/share_insight" method="post" id="Shareinsightform">
								<div class="modal-body">
						            <!-- content goes here -->
									
						              	<div class="form-group first">
						              	  <input type="first-name" class="form-control" id="firstname" placeholder="First name" name="data[FName]">
						              	</div>
						              	<div class="form-group last">
						              	  <input type="last-name" class="form-control" id="lastname" placeholder="Last name" name="data[LName]">
						              	</div>
						              	<div class="form-group">
						              	  <input type="email" class="form-control" id="emailaddress" placeholder="Enter email" name="data[EAddress]">
						              	</div>
						              	<div class="form-group">
						              	  <input type="text" class="form-control" id="emailsubject" placeholder="Email subject" name="data[ESubject]" value="<?php echo ($loggedinuser['UserReference']['first_name']." ".$loggedinuser['UserReference']['last_name']);?> has shared a business insight with you">
						              	</div>										
						              	<div class="form-group">
										  <input type="hidden" name="data[EText]"  id="qemailcontent"/>
						              	  <div class="form-control editor" name="comment" form="usrform"  placeholder="Type your message here..." id="qemaildiv" onClick="this.contentEditable='true';">

										 	</br>
										     <b><?php echo ($loggedinuser['UserReference']['first_name']." ".$loggedinuser['UserReference']['last_name']);?> has shared a business insight on GUILD with you.</b>
                                                                                     </br></br>
											 GUILD @guildalerts: <b><?php echo($insight['Insight']['title']) ?></b>
                                                                                         </br></br>
										     <?php echo($this->Html->link("Read the insight",SITE_URL.'insight/insight/'.$insight['Insight']['url_key'],array('escape'=>false,'class'=>'btn que-link','title'=>'Insight Link')));?>.
										  
										  </div>										  
						              	</div>
                                        <span style="color:#CF3135;display:none;" id="captchaerror">Captcha validation is required</span>
						              	<div class="g-recaptcha" data-sitekey="6LePDikUAAAAAJBAS-DW3cEDGsr3liRIBvX7M9w5"></div> 
						            

								</div>
								<div class="modal-footer">
									<a class="send" type="submit" href="#" data-action="save" name="submit" id="shareinsightbutton">Send</a>
								</div>
								</form>
							</div>
						  </div>
						</div>
                        <div class="insight-banner">
                            <div class="insight-img-banner">
                                <?php if(strpos($insight['Insight']['image'], "guild.im")!=false || $insight['Insight']['image'] ==''){?>



                                <div class="banner-photo banner-is-guild" style="background:url('<?php echo($image);?>');"></div>
                                <div class="banner-content img">
                                    <a class="back-post" href="<?php echo(SITE_URL);?>qanda">&#8592; Back to Posts</a>
                                    <div class="banner-title" >
                                        <h5><a href="<?php echo($insight['Insight']['post_url']);?>" target="_blank"><?php echo($insight['Insight']['title']) ?></a></h5>
                                        <div class="org-date">
                                            <span><?php echo($monthName);?>&nbsp;<?php echo($path[2]);?>,&nbsp;<?php echo($path[0]);?></span>
                                            <span class="hbr">
                                                <img src="<?php echo($favicon);?>">&nbsp;&nbsp;<a href="<?php echo($insight['Insight']['post_url']);?>" target="_blank"><?php echo($url);?></a>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="QnAbutton">
                                        <?php
                                        foreach ($insight['Insight_category'] as $catValue) {
                                        $str = ($categories[$catValue['topic_id']]);
                                        if(false !== stripos(($categories[$catValue['topic_id']]),"&"))
                                        $str = str_replace("&","_and",($categories[$catValue['topic_id']]));
                                        if(false !== stripos(($categories[$catValue['topic_id']]),"/"))
                                        $str = str_replace("/","_slash",($categories[$catValue['topic_id']]));
                                        $str = str_replace(" ","-",$str);
                                        echo($this->Html->link($categories[$catValue['topic_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false, 'title'=>$categories[$catValue['topic_id']],'style'=>'')));
                                        }?>
                                    </div>
                                </div>
                            </div>
                        </div>

                                <?php }
                                else{?>
                                <div class="banner-photo" style="background:url('<?php echo($background_image);?>');"></div>
                                <div class="banner-content img">
                                    <a class="back-post" href="<?php echo(SITE_URL);?>qanda">&#8592; Back to Posts</a>
                                    <div class="banner-title" >
                                        <h5><a href="<?php echo($insight['Insight']['post_url']);?>" target="_blank"><?php echo($insight['Insight']['title']) ?></a></h5>
                                        <div class="org-date">
                                            <span><?php echo($monthName);?>&nbsp;<?php echo($path[2]);?>,&nbsp;<?php echo($path[0]);?></span>
                                            <span class="hbr">
                                                <img src="<?php echo($favicon);?>">&nbsp;&nbsp;<a href="<?php echo($insight['Insight']['post_url']);?>" target="_blank" ><?php echo($url);?></a>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="QnAbutton">
                                        <?php
                                        foreach ($insight['Insight_category'] as $catValue) {
                                        $str = ($categories[$catValue['topic_id']]);
                                        if(false !== stripos(($categories[$catValue['topic_id']]),"&"))
                                        $str = str_replace("&","_and",($categories[$catValue['topic_id']]));
                                        if(false !== stripos(($categories[$catValue['topic_id']]),"/"))
                                        $str = str_replace("/","_slash",($categories[$catValue['topic_id']]));
                                        $str = str_replace(" ","-",$str);
                                        echo($this->Html->link($categories[$catValue['topic_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false, 'title'=>$categories[$catValue['topic_id']],'style'=>'')));
                                        }?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php }?>
                    </div>
                </div>   
                <div id="mentors-content" class="mentors-content">
                    <div id="results">
                        <div class="set_0?>">
                            <div class="box-wrap-insight">
                                <div class="user-img">
                                    <a href="<?php echo(SITE_URL.strtolower($userData['User']['url_key']));?>">
                                        <img class="desaturate" src="<?php echo($image_path);?>" alt="<?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?>" title="<?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?>"/>
                                    </a>
                                </div>
                                <div class="user-info">
                                    <!-- <div class="user-answer"> -->
                                        <h2 class="title-medium">
                                            <a href="<?php echo(SITE_URL.strtolower($userData['User']['url_key']));?>" title="<?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?>"><?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?></a>
                                        </h2>
                                        <p><?php  echo(ucfirst($userData['UserReference']['linkedin_headline']));?></p>
                                        <div class="answer">
                                            <?php
                                            $disp=SITE_URL.strtolower($userData['User']['url_key'])."#insights";
                                            if($userData['ans_count'] >1)
                                                echo($this->Html->link($userData['ans_count']." insights",$disp,array('escape'=>false, 'style'=>'')));
                                            else if($userData['ans_count'] == 1)
                                                echo($this->Html->link($userData['ans_count']." insight",$disp,array('escape'=>false, 'style'=>'')));
                                            else { ?>
                                            <span style="font-size: 14px;">&nbsp;</span>
                                            <?php }?>
                                        </div>
										<?php if($this->Session->read('Auth.User.id')!=''){?>
                                        <a class="mail-btn" onclick="insightshare();" href="javascript:void(0)" data-toggle="modal" ><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mail_dark.svg" alt="mail" data-pin-nopin="true"></a>
										<?php }else{?>
									    <a class="mail-btn" href="javascript:void(0);" data-toggle="modal" data-target="#signin-Modal"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mail_dark.svg" alt="mail" data-pin-nopin="true"></a>	
							            <?php }?>
                                        <div class="share_icons social more-icon pull-right">
                                            <div class="share-button">
                                                <!-- <input class="create_btn" type="button" value="Share" onclick='javascript:doSomething();',onmouseover = 'doSomethingMouseOver('.$key.');'> -->
                                                <a class="social-button" href="javascript:void(0);">
                                                    <img src="<?php echo (SITE_URL);?>yogesh_new1/images/share_dark.svg" alt="share" data-pin-nopin="true">
                                                </a>     
                                            </div>

                                            <?php if($userData['User']['id'] == $this->Session->read('Auth.User.id')) {?>

                                            <div class="shareicon" id="shareicon">

                                                <?php }else {?>
                                                <div id="shareicon" class="inactive hide">
                                                    <?php }?>
                                                    <ul>
                                                 <?php 

                                                       $socialCount=0;
                                   
                                                       while(count($userData['Social'])>$socialCount){
					                                   if(($pos =strpos($userData['Social'][$socialCount]['social_name'],'twitter.com'))!==false)
						                                {
							                            $twitterURL = $userData['Social'][$socialCount]['social_name'];
                                                        $path = explode("/", $twitterURL);
                                                        $twitter_handle = end($path); 
                                                           
						                                }
                                                      $socialCount++;
						                               } 

                                                       $login = "o_4urqen5mkq";
                                                       $appkey = "R_7aa87ace460abde4cfc31fa31b2e59ef";
                                                       $completelink = SITE_URL."insight/insight/".($insight['Insight']['url_key']);
                                                       $answerid = $insight['Insight']['id'];
                                                       $bitlyUrl =  $this->General->get_bitly_url($completelink, $answerid,$login, $appkey);


								                         $linkdinUrl="https://www.linkedin.com/shareArticle?mini=true&url=".$completelink."&title=".$MentorsGuild.$userData['UserReference']['first_name']." ". $userData['UserReference']['last_name']." " .$linkTitle;
								                         $tweet="https://twitter.com/intent/tweet?text=".$atMentorsGuild. $userData['UserReference']['first_name']." ". $userData['UserReference']['last_name'] ." ".$compTitle." " .$tinyUrl;

                                                        if($twitter_handle ==''){
                                                          $tweet="https://twitter.com/intent/tweet?text=".$userData['UserReference']['first_name']." ". $userData['UserReference']['last_name'] ."&#58; ".$compTitle." " .$bitlyUrl;
								                         } else{
                                                          $tweet="https://twitter.com/intent/tweet?text=".$atsybmol.$twitter_handle." ".$compTitle." " .$bitlyUrl;
                                                        }
							                            ?>													

							<li>
								<a href="javascript:void(0);" onclick="javascript:window.open('<?php echo ($tweet);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')" ">
									<!-- <img src="<?php echo(SITE_URL)?>yogesh_new1/images/twit_icon.png" alt="Twitter Share" data-pin-nopin="true"> -->
									<i class="fa fa-twitter" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" onclick="javascript:window.open('<?php echo ($linkdinUrl);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
									<!-- <img src="<?php echo(SITE_URL)?>yogesh_new1/images/logo_linkedin.png" alt="LinkedIn Share"> -->
									<i class="fa fa-linkedin" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a title="Share this post/page"href="javascript:window.open('https://www.facebook.com/sharer.php?s=100&p[url]=<?php echo $completelink; ?>&p[title]=<?php echo($atMentorsGuild. $userData['UserReference']['first_name']." ".$userData['UserReference']['last_name'] ." ".$compTitle);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
									<!-- <img src="<?php echo(SITE_URL)?>yogesh_new1/images/fb_icon.png" alt="Facebook Share"> -->
									<i class="fa fa-facebook" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="javascript:window.open('https://plus.google.com/share?url=<?php echo($completelink);?>&title=<?php echo($atMentorsGuild. $userData['UserReference']['first_name']." ".$userData['UserReference']['last_name'] .$compTitle);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
									<!-- <img src="<?php echo(SITE_URL)?>yogesh_new1/images/g_plus_icon.png" alt="Google+ Share"> -->
									<i class="fa fa-google-plus" aria-hidden="true"></i>
								</a>
							</li>
                                                    </ul>                                                    
                                                </div>
                                                <!-- Share on social media end -->
                                            </div>
                                        </div>
                                   <!--  </div> -->
                                        
                                    <hr>
                                    <div class="description">
                                        <p>


                                            <?php {

                                              $posturl = $insight['Insight']['post_url'];

                                              if(($pos =strpos($posturl,'http'))!==false)
						{
							$posturl = $posturl;
						}
						else
						{
							$posturl ="http://".$posturl;
						}


                                                    $editthis = '<div class="ql-editor" contenteditable="true"';
                                                    $editby= '<div class="ql-editor" contenteditable="false"';
                                                    $answer = str_replace($editthis, $editby, $insight['Insight']['insight']);
                                                    $editthis1 = 'target="_blank" style="color: rgba(0, 0, 0, 0.8);"';
                                                    $editby1 = 'target="_blank" style="color: #CF3135;"';
                                                    $answer = str_replace($editthis1, $editby1, $answer);													
                                                    $editthis2 = '<?php echo($postlink);?>';
                                                    $editby2= $posturl;
                                                    $answer = str_replace($editthis2, $editby2, $answer);
                                                    $editthis3 = 'style="color: rgb(10, 92, 163);"';
                                                    $editby3 = 'style="color: #CF3135;"';
                                                    $answer = str_replace($editthis3, $editby3, $answer);													
                                                    echo nl2br(htmlspecialchars_decode($answer));
                                                    }?>
                                                   <?php if(false === stripos($answer,"Read Full Post")){?>
                                                    <a href="<?php echo($posturl);?>" target="_blank">Read Full Post</a>
                                                  <?php }?>
                                                    
                                        </p>
                                        <div class="btm-info">
                                            <div class="upvote">                  
                                                <input id="button<?php echo($insight['Insight']['id']) ?>" class="mgButton btn btn-primary" type="button" value="Upvote | <?php echo($insight['Insight']['upvotes']);?>" onclick="upVote(<?php echo($insight['Insight']['id']) ?>);">
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
					<!-- Include all compiled plugins (below), or include individual files as needed -->
					<script src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
					<!-- <script src="<?php echo(SITE_URL)?>yogesh_new1/js/editor.js"></script> -->
					<!-- Google Re-captcha Code -->
					<script src='https://www.google.com/recaptcha/api.js'></script>
					<!-- Include the Quill library -->
					<script src="https://cdn.quilljs.com/1.0.0/quill.js"></script>
					<!-- Include yogesh JS code -->
					<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
					<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
					<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
					<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
					<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
					<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
					<script type="text/javascript" src="<?php echo(SITE_URL)?>sanchit/js/insightscript.js"></script>
					
						