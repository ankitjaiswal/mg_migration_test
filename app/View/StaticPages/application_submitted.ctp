<div class="content-wrap white-bg error-page">
	<div class="container" style="margin-top:30px;margin-bottom:50px;">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				
				<h3>Congratulations! Your membership application has been submitted.</h3>
                                <p>We will contact you as a part of our review process.</br></br>
                                Thanks for your interest!</p>
								<button class="btn btn-default get-started" onclick="window.location.href='<?php echo (SITE_URL."fronts/index"); ?>';">
									Done
								</button>                               
                                
			</div>
		</div>
	</div>
</div>
	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>