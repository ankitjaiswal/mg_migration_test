<div class="fieldset">
    <h3 class="legend">Page Listing
			<div class="total" style="float:right"> Total Pages : <?php echo($this->request["paging"]['StaticPage']["count"]);?>
			</div>
		</h3>
		<div class="adminrightinner" style="padding:0px;">
			
				<div class="tablewapper2">	
					<table width="100%" border="0" cellpadding="0" cellspacing="0" class="User2Table">	
						<tr class="head">
                        <td width="20%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:19px;"><?php echo('S.No.')?>
							<td width="20%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:19px;"><?php echo($this->Paginator->sort('Title', 'StaticPage.title'))?>
							</td>
							<td width="50%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:19px;"><?php echo($this->Paginator->sort('Created','StaticPage.created'))?></td>
							<td width="20%"align="center" valign="middle" class="Bdrrightbot Padtopbot6">Action</td>
						</tr>	
						<?php
						$i=0;foreach($pages as $value){	$i++;?>
						
							<tr>
                            	<td align="left" valign="middle" class="Bdrrightbot Padtopbot6" style="padding-left:19px;"><?php echo $i;?></td>
								<td align="left" valign="middle" class="Bdrrightbot Padtopbot6" style="padding-left:19px;"><?php echo $value['StaticPage']['title'];?></td>
								<td align="left" valign="middle" class="Bdrrightbot" style="padding-left:19px;"> <?php echo $value['StaticPage']['created'];?>	</td>
								<td align="center" valign="middle" class="Bdrbot ActionIcon">
								<?php echo  $this->Html->link($this->Html->image('admin/edit_icon.jpg',array('title'=>'Edit')), array('controller' => 'static_pages', 'action' => 'edit', $value['StaticPage']['id']), array('escape' => false,'class'=>'viewstatusimg1'));?>
								</td>
							</tr>	
							<?php
						}?>
					</table>
				</div>
				<?php
			?>
			<?php  if(empty($pages)){ ?>
				<div style="color:blue, font-size:20; padding-bottom:30px;padding-top:30px;padding-left:280px" ><strong>No Records Found.</strong></div>
				<?php 
			}?>
		</div>
		<div class="clr"></div>
		<?php echo $this->Element('Admin/admin_paging', array("paging_model_name"=>"StaticPage","total_title"=>"Pages")); ?>	 
	</div>
	
<style>
.PagingTable td{
    border-top:none !important;
}
</style>