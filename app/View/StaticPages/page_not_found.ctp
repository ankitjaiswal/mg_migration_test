<div class="content-wrap white-bg error-page">
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<h1>Page not found</h1>
				<h3>The page you are looking for might have been moved or renamed.</h3>
				<p>We apologize for any inconvenience. Please double-check the URL or try the following:</p>
				<p><a href="javascript:;">Search</a> our site for keywords of interest.</p>
				<p>Visit our <a href="<?php echo (SITE_URL); ?>">home page</a> or use the navigation menu to find what you need.</p>
				<p><a href="javascript:;">Contact us</a> if you're still having a problem.</p>
			</div>
		</div>
	</div>
</div>