<?php echo $this->Html->script('tinymce/jscripts/tiny_mce/tiny_mce');?>
<script type="text/javascript">
tinyMCE.init({
		// General options
		mode : "textareas",
		theme : "advanced",
		skin : "o2k7",
		content_css : SITE_URL+'app/webroot/css/front.css',
		height:500,
		width :800
});
</script>
<div class="adminrightinner">
     <?php echo($this->Form->create('StaticPage', array('url' => array('controller' => 'static_pages', 'action' => 'edit'))));?>    
	 <div class="tablewapper2 AdminForm">
	  <h3 class="legend1">Edit Page </h3>
		<table border="0" class="Admin2Table" width="100%">				   
			  <tr>
				 <td valign="middle" class="Padleft26">Page Title <span class="input_required">*</span></td>
				 <td><?php echo($this->Form->input('title', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
                 <?php echo($this->Form->input('id'));?>
			  </tr>
			   <tr>
				 <td valign="middle" class="Padleft26">Page Content <span class="input_required">*</span></td>
				 <td><?php echo($this->Form->textarea('description', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>
                 <?php echo $this->Form->error('description');?>
                 </td>
			  </tr>
			   <tr>
				
			 		 
			 
		</table>
      </div>
      <div class="buttonwapper">
				<div><input type="submit" value="Submit" class="submit_button" /></div>
				<div class="cancel_button"><?php echo $this->Html->link("Cancel", "/static_pages/admin_index/", array("title"=>"", "escape"=>false)); ?></div>
		</div>
		<?php
		   echo($this->Form->end());
		?>	
</div>


         