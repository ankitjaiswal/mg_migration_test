<div class="fieldset">
    <h3 class="legend">
		Member-Client Log (Share link : <?php if(isset($shareLink['MessageLink']['link_text'])): echo $shareLink['MessageLink']['link_text']; else: echo "No link here"; endif; ?>)
        <div class="total" style="float:right">
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Date</td>
                        <td  width="20%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">From</td>
                        <td  width="20%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Message</td>
                    </tr>
                    <?php 
                    if(!empty($data) && count($data)>0)
                    { 
                        foreach($data as $msg)
                        {    
                        
                        ?>
                        <tr>
                            <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo date('D, m/d/Y',strtotime($msg['Message']['created'])); ?></td>
                            <?php if($msg['Message']['sent_by']==2){?>
                            <td width="20%" align="left" valign="middle" class="Bdrrightbot"><?php echo ucfirst(strtolower($msg['Mentor']['first_name']))." ".ucfirst(strtolower($msg['Mentor']['last_name'])).' (Mentor)'; ?></td>
                            <?php } else{ ?>
                                <td width="20%" align="left" valign="middle" class="Bdrrightbot"><?php echo ucfirst(strtolower($msg['Mentee']['first_name']))." ".ucfirst(strtolower($msg['Mentee']['last_name'])).' (Mentee)'; ?></td>
                           <?php } ?>
                            <td  width="20%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo nl2br(strip_tags($msg['Message']['message'])); ?></td>           
                        </tr>	
                   <?php
                        }
                    }else{ ?>
                   <tr>
                        <td colspan="3" width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Record not found</td>           
                    </tr>
                   <?php } ?>   
                </table>
            </div>
            <?php
     //   }
        ?>

    </div>
</div>
<div class="clr"></div>
<?php //echo $this->Element('Admin/admin_paging', array("paging_model_name" => "NewsLetter", "total_title" => "NewsLetter")); ?>	 