<?php echo($this->Html->script(array('fancybox/jquery.fancybox-1.3.4.pack', 'fancybox/jquery.mousewheel-3.0.4.pack'), false)); ?>
<?php //echo($this->Html->css(array('jquery.fancybox-1.3.4')), false); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.addAttribute').fancybox();
    });		
</script>

<div class="fieldset">
    <h3 class="legend">
		Users list
        <div class="total" style="float:right"> Total users : <?php echo($this->request["paging"]['User']["count"]); ?>
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <?php echo($this->Form->create('User', array('name' => 'Admin', 'url' => array('controller' => 'members', 'action' => 'process')))); ?>    
        <input type="hidden" name="pageAction" id="pageAction"/>	 
        <?php
        if (!empty($data)) {
            $exPaginator->options = array('url' => $this->passedArgs);
            $this->Paginator->options(array('url' => $this->passedArgs));
            ?> 

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo($this->Paginator->sort('User ID', 'User.id')) ?></td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo($this->Paginator->sort('User name', 'UserReference.first_name')) ?></td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo($this->Paginator->sort('Account Type', 'User.role_id')) ?></td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo($this->Paginator->sort('Start Date', 'User.created')) ?></td>
                        <td width="5%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Status</td>
                    </tr>	
                    <?php
                    foreach ($data as $value) {
                        ?>
                        <tr>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                             <?php
                                if($value['User']['role_id']=='2'){
                                    $formatIDMentor = "M".str_pad($value['User']['id'],4,0,STR_PAD_LEFT);
                                    $userLink = $this->Html->link($formatIDMentor,array('controller' => 'members', 'action' => 'edit/'.$value['User']['id']) , array("title" => "", "escape" => false));
                                }else{
                                    $formatIDMentor = "S".str_pad($value['User']['id'],4,0,STR_PAD_LEFT);
                                    $userLink = $this->Html->link($formatIDMentor,array('controller' => 'clients', 'action' => 'edit/'.$value['User']['id']) , array("title" => "", "escape" => false));
                                }
                                echo  $userLink; ?></td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php echo(ucfirst(strtolower($value['UserReference']['first_name'])) . ' ' . ucfirst(strtolower($value['UserReference']['last_name']))); ?></td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php
                                if ($value['User']['role_id'] == '2') {
                                    echo $value['User']['mentor_type'];
                                }
                                if($value['User']['role_id']=='3'){
                                    echo 'Mentee';
                                }
                                ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php echo($value['User']['created']); ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php
                                if ($value['User']['status'] == '1') {
                                    echo $this->Html->image("active.png", array("title" => "Active User", "alt" => "Active", "border" => 0));
                                } else {
                                    echo $this->Html->image("deactive.png", array("title" => "Deactive User", "alt" => "Deactive", "border" => 0));
                                }
                                ?>
                            </td>
                        </tr>	
                        <?php
                    }
                    ?>
                </table>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<div class="clr"></div>
<?php echo $this->Element('Admin/admin_paging', array("paging_model_name" => "User", "total_title" => "User")); ?>	 