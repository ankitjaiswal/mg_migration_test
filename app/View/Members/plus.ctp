
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth" style="margin-bottom:50px;">
 	    <div id="user-account">
	      <div class="account-form">	    
		  	<div class="onbanner">
		          <h1>                 
		            <div style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;"><span style="text-decoration: underline;">Guild Plus</span></div></h1>
                        
				  <div style="position:relative;top:80px;"><p><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal;font-size: 18px;">Guild helps experienced consultants get more business and be more productive.</span></div>	
			</div>

             <?php if($this->Session->check('invited_member') && $this->Session->read('invited_member') != '')
                  {
                    $invited_visitor = $this->Session->read('invited_member');
              }?>

                  
             
<div style="height:60px;">&nbsp;</div>


<div style="height:10px;">&nbsp;</div>

<table style="border 1px solid #BEBEBE;">
<tbody >
<tr> 

<td style="vertical-align: middle; border: none; width: 480px;height:250px;border-bottom:1px solid #BEBEBE;" align="center">
<div class ="signbox" contenteditable="true">
  <table width="100%" border="0" cellpadding="0">
    <tbody>
      <tr>
        <td align="left" valign="top" width="10">

          <p style="margin-right:10px;font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:14px">



            <a style="text-decoration:none" href="<?php echo($displink1);?>" target="_blank" >
                <img src="<?php echo SITE_URL?>imgs/John-Baldoni-MentorsGuild1.jpg" alt="John Baldoni" border="0" height="80" width="80">
            </a>

          </p>
        </td>
        <td align="left">
          <p style="font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:14px;color:rgb(33,33,33)"><span style="font-weight:bold;color:rgb(33,33,33);display:inline">John Baldoni</span>
         
      <span style="display:inline"></br></span>
      <a href="mailto:<?php echo($this->data['User']['username']);?>" style="color:rgb(208,49,53);text-decoration:none;display:inline" target="_blank">John@johnbaldoni.com</a><span style="color:rgb(33,33,33)"></span></p>
    <p style="font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:14px">
        <span style="font-weight:bold;color:rgb(33,33,33);display:inline; margin-top:-15px;">Leadership Development</span>


        <span style="display:inline"><br></span>
         <span style="color:rgb(33,33,33);display:inline">(xxx) xxx-xxxx</span> 


         <span style="display:block"></span> <span style="color:rgb(33,33,33);display:inline">Ann Arbor, MI</span>


        <span style="display:inline"><br></span>
        <a href="http://www.johnbaldoni.com/" style="color:rgb(208,49,53);text-decoration:none;display:inline" target="_blank">http://www.johnbaldoni.com/</a>

    </p>

<p style="margin-top:-20px;">
<?php { $link = "https://www.linkedin.com/in/jbaldoni";
       	     
      $displink1 = "http://www.guild.im/john.baldoni";
      $twitterURL = "https://twitter.com/johnbaldoni";
      $youtubeURL = "https://www.youtube.com/user/jbaldoni52";
    } ?>

<a style="text-decoration:none;display:inline;" href="<?php echo($link);?>" target="_blank">
  <img width="24" style="margin-bottom:5px;border:none;display:inline" height="24" src="<?php echo(SITE_URL)?>sign_imgs/linkedin.png" alt="Linkedin">
</a>

<a style="text-decoration:none" href="<?php echo($displink1);?>">
  <img width="24" style="margin-bottom:5px;border:none;" height="24" src="<?php echo(SITE_URL)?>sign_imgs/mg_monogram1.png" alt="Guild Profile">
</a>
<a style="text-decoration:none;display:inline" href="<?php echo($twitterURL);?>" target="_blank">
  <img width="24" style="margin-bottom:5px;border:none;display:inline" height="24" src="<?php echo(SITE_URL)?>sign_imgs/twitter.png" alt="Twitter">
</a>

<a style="text-decoration:none;display:inline" href="<?php echo($youtubeURL);?>" target="_blank">
  <img width="24" style="margin-bottom:5px;border:none;display:inline" height="24" src="<?php echo(SITE_URL)?>sign_imgs/youtube.png" alt="Youtube">
</a>
</p>
        </td>
      </tr>


     <tr>
        <td colspan="2"><p style="font-family:Helvetica,Arial,sans-serif;color:rgb(146,146,146);font-size:12px;line-height:12px">NOTICE: The contents of this email and any attachments are confidential. They are intended for the named recipient(s) only. If you have received this email by mistake, please notify the sender immediately.</p></td>
      </tr>

    </tbody>
  </table>
</div>

</td>
<td style="padding: 10px;width: 480px;height:200px;border-bottom:1px solid #BEBEBE;">
<span style="font-family: Ubuntu,arial,vardana;font-size: 16px;line-height: 20px;"><strong>Email signature &#8212;</strong> Branded and ready to use.
<?php if($this->Session->read('Auth.User.id') =='' && $invited_visitor !='' ){?>
<a href="javascript:void(0)" onclick='javascript:claimProfile();' title="Badges">Learn More</a>.</span>
<?php }elseif($this->Session->read('Auth.User.id') ==''){?>
<a href="javascript:void(0)" onclick='javascript:loginpopup();' title="Badges">Learn More</a>.</span>
<?php }else{?>
<a href="<?php echo SITE_URL?>members/my_badges" title="Badges">Learn More</a>.</span>
<?php }?>
</td>
</tr>
</tr>

<tr style="border-bottom: 1px solid #BEBEBE;">
<td style="vertical-align: middle; border: none; width: 480px;height:200px;border-bottom:1px solid #BEBEBE;" align="center"><a href="<?php echo(SITE_URL)?>project/home" title="Sub-Contract a Project" target="_blank"><img src="<?php echo(SITE_URL)?>press_material/subcontract-image.svg" alt="Sub-Contract a Project" style="width:120px;height:120px;"></a></td>
<td style="padding: 10px; width: 480px;height:200px;border-bottom:1px solid #BEBEBE;">
<span style="font-family: Ubuntu,arial,vardana;font-size: 16px;line-height: 20px;"><strong>Sub-Contract a Project &#8212;</strong> Easy access to relevant experts among your fellow members. 
<?php if($this->Session->read('Auth.User.id') =='' && $invited_visitor !='' ){?>
<a href="javascript:void(0)" onclick='javascript:claimProfile();' title="Sub-Contract a Project">Learn More</a>.</span>
<?php }elseif($this->Session->read('Auth.User.id') ==''){?>
<a href="javascript:void(0)" onclick='javascript:loginpopup();' title="Sub-Contract a Project">Learn More</a>.</span>
<?php }else{?>
<a href="<?php echo(SITE_URL)?>project/home" title="Sub-Contract a Project">Learn More</a>.</span>
<?php }?>
</td>
</tr>

<tr style="border-bottom: 1px solid #BEBEBE;">
<td style="vertical-align: middle; border: none; width: 480px;height:200px;border-bottom:1px solid #BEBEBE;" align="center"><a href="<?php echo(SITE_URL)?>members/invitation_eligibility" title="Send Membership Invitations" target="_blank"><img src="<?php echo(SITE_URL)?>press_material/membership-invitation.svg" alt="Send Membership Invitations" style="width:120px;height:120px;"></a></td>
<td style="padding: 10px; width: 480px;height:200px;border-bottom:1px solid #BEBEBE;">
<span style="font-family: Ubuntu,arial,vardana;font-size: 16px;line-height: 20px;"><strong>Send Membership Invitations &#8212;</strong> Invite other experienced consultants to join as fellow members. 
<?php if($this->Session->read('Auth.User.id') =='' && $invited_visitor !='' ){?>
<a href="javascript:void(0)" onclick='javascript:claimProfile();' title="Send Membership Invitations">Learn More</a>.</span>
<?php }elseif($this->Session->read('Auth.User.id') ==''){?>
<a href="javascript:void(0)" onclick='javascript:loginpopup();' title="Send Membership Invitations">Learn More</a>.</span>
<?php }else{?>
<a href="<?php echo(SITE_URL)?>members/invitation_eligibility" title="Send Membership Invitations">Learn More</a>.</span>
<?php }?>
</td>
</tr>

</tr>




<tr> 

<td style="vertical-align: middle;  width: 480px;height:200px;border-bottom:1px solid #BEBEBE;"><div style="margin-left:60px;"><iframe src="https://www.guild.im/members/badges/?id=Mark.Palmer&type=answers1" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe></div></td>
<td style="padding: 10px;width: 480px;height:200px;border-bottom:1px solid #BEBEBE;">
<span style="font-family: Ubuntu,arial,vardana;font-size: 16px;line-height: 20px;"><strong>Guild badges &#8212;</strong> Send visitors to your profile where they can review information about you and contact you for their business needs. 
<?php if($this->Session->read('Auth.User.id') =='' && $invited_visitor !='' ){?>
<a href="javascript:void(0)" onclick='javascript:claimProfile();' title="Badges">Learn More</a>.</span>
<?php }elseif($this->Session->read('Auth.User.id') ==''){?>
<a href="javascript:void(0)" onclick='javascript:loginpopup();' title="Badges">Learn More</a>.</span>
<?php }else{?>
<a href="<?php echo SITE_URL?>members/my_badges" title="Badges">Learn More</a>.</span>
<?php }?>
</td>
</tr>

<tr>
<td style="vertical-align: middle; border: none; width: 480px;height:200px;border-bottom:1px solid #BEBEBE;" align="center"><a href="<?php echo SITE_URL?>members/flevy" title="Flevy documents"><img src="<?php echo(SITE_URL)?>/press_material/flevy-logo.png" alt="Flevy Logo"></a></td>
<td style="padding: 10px; width: 480px;height:200px;border-bottom:1px solid #BEBEBE;">
<span style="font-family: Ubuntu,arial,vardana;font-size: 16px;line-height: 20px;"><strong>Flevy documents</strong> include business frameworks & methodologies, presentation templates, financial models, and more. In particular, there is a wealth of business frameworks. <a href="<?php echo SITE_URL?>members/flevy" title="Flevy documents">Learn More</a>.</span>
</td>
</tr>
<tr>
<td style="vertical-align: middle; border: none; width: 480px;height:200px;border-bottom:1px solid #BEBEBE;" align="center"><a href="https://www.insightbee.com/resources/mentorsguild" title="InsightBee Offers" target="_blank"><img src="<?php echo(SITE_URL)?>/press_material/insightbee-logo.jpg" alt="insightbee Logo"></a></td>
<td style="padding: 10px; width: 480px;height:200px;border-bottom:1px solid #BEBEBE;">
<span style="font-family: Ubuntu,arial,vardana;font-size: 16px;line-height: 20px;">Walk into a client meeting with superior insights on their business and market. Access high quality, personalized reports available so far only to the top consulting firms, at a discounted rate. 
<?php if($this->Session->read('Auth.User.id') =='' && $invited_visitor !='' ){?>
<a href="javascript:void(0)" onclick='javascript:claimProfile();' title="InsightBee Offers">Learn More</a>.</span>
<?php }elseif($this->Session->read('Auth.User.id') ==''){?>
<a href="javascript:void(0)" onclick='javascript:loginpopup();' title="InsightBee Offers">Learn More</a>.</span>
<?php }else{?>
<a href="https://www.insightbee.com/resources/mentorsguild" title="InsightBee Offers" target="_blank">Learn More</a>.</span>
<?php }?>
</td>
</tr>
<tr>
<td style="vertical-align: middle; border: none; width: 480px;height:200px;border-bottom:1px solid #BEBEBE;" align="center"><a href="<?php echo SITE_URL?>members/ninelenses" title="9Lenses documents"><img src="<?php echo(SITE_URL)?>/press_material/9lenses-logo1.png" alt="9lenses Logo"></a></td>
<td style="padding: 10px; width: 480px;height:200px;border-bottom:1px solid #BEBEBE;">
<span style="font-family: Ubuntu,arial,vardana;font-size: 16px;line-height: 20px;"><strong>9Lenses</strong> provides a secure, connected way to manage your engagements and protect your IP.</br></br> Our platform combines a rigorous interview structure with intuitive software and exceptional analytics so you can have a deeper, faster method for advising your clients. <a href="<?php echo SITE_URL?>members/ninelenses" title="9Lenses documents">Learn More</a>.</span>
</td>
</tr>
</tbody>
</table>

</br></br></br>

              <?php if($this->Session->read('Auth.User.role_id') != 2){?>

             <div class="mgbutton">CALL 1-866-511-1898 FOR INFORMATION</div>
           <?php }?>


</div>
</div>
</div>
</div>


<?php echo($this->Html->css(array('plans'))); ?>

<style>
hr { 
    display: block;
    margin-top: 1.5em;

    margin-left: auto;
    margin-right: auto;
    border-style: inset;
    border-width: 1px;
}


.signbox{
    background-color: #fff;
    width: 440px;
    height: 235px;
    padding: 10px;
    border: 1px solid #DDDDDD;
   margin: 12px;
}
a:hover {
    cursor: pointer;
    cursor: hand;
}
</style>






















