<style>	
    @media screen and (min-width: 1400px){
    	.navbar .container,#footer-section .container,#subfooter-section .container{
	        max-width: 1500px;
	        display: table;
	        width: 100%;
	        margin: 0 auto;
	        position: relative;
	        padding-right: 0;
	        padding-left: 0;
	    }
	    .navbar > .container .navbar-brand, .navbar > .container-fluid .navbar-brand{
	    	margin-left: -5px;
	    }
	    .navbar-right{
	    	margin-right: 0px;
	    }
    }
    @media screen and (max-width: 1400px){
    	.navbar .container,#footer-section .container,#subfooter-section .container{
	        max-width: 1170px;
	        padding-right: 15px;
	        padding-left: 15px;
	    }
    }
    @media screen and (max-width: 1200px){
    	.navbar .container,#footer-section .container,#subfooter-section .container{
    		max-width: 970px;
    	}
    }
    @media screen and (max-width: 992px){
    	.navbar .container,#footer-section .container,#subfooter-section .container{
    		max-width: 750px;
    	}
    }
</style>
<div class="member-content">

	<div class="hero-section">
		<div class="container-wrap">
			<div class="row">
				<div class="col-sm-6">
					<h2>Targeted solutions for your critical business problems</h2>
					<p>Stuck with a complex business issue? GUILD can help. Designed to support the needs of small and medium-sized businesses, GUILD gives you immediate access to proven experts, rated top in their functions and industries.</p>
					<p>GUILD's dedicated Client Partners provide world-class project management and rapid integration with your internal operations. Immediate access to subject-matter experts, in a supported environment that meets your unique needs.</p>
				</div>
				<div class="col-sm-6">
					<img src="<?php echo(SITE_URL)?>yogesh_new1/images/hero-img.jpg" alt="" />
				</div>
			</div>
		</div>
	</div>

	<div class="requirements-section">
		<div class="container-wrap">
			<div class="row d-flex justify-content-center text-center">
				<div class="col-md-12 col-lg-8">
					<div class="section-title">
						<h2>GUILD fits your unique requirements</h2>
						<p>GUILD resources can be configured to fit your project needs. Each subject-matter expert is supported by an internal Client Partner, and our experts can fit into any existing team.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="service-box">
						<div class="service-icon">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/icon-1.png" alt="" />
						</div>
						<div class="service-detail">
							<h4>Vetted experts</h4>
							<p>High performing subject-matter experts are matched to your unique needs</p>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-box">
						<div class="service-icon">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/icon-2.png" alt="" />
						</div>
						<div class="service-detail">
							<h4>Built-in project management</h4>
							<p>A dedicated Client Partner facilitates success for each project with your company.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-box">
						<div class="service-icon">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/icon-3.png" alt="" />
						</div>
						<div class="service-detail">
							<h4>Trust</h4>
							<p>Highest-in-industry repeat rate. Customer satisfaction drives GUILD growth.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row d-flex justify-content-center text-center">
				<div class="col-sm-12">
					<a href="javascript:;" class="btn-red">What do you need solved?</a>
				</div>
			</div>
		</div>
	</div>

	<div class="companyintro-section">
		<div class="container-wrap">
			<div class="row d-flex justify-content-center text-center">
				<div class="col-md-12 col-lg-8">
					<div class="section-title">
						<h2>Customized to your company's needs</h2>
						<p>When your company requires immediate expertise, GUILD can help.  Our consultants have deep domain expertise and we work collaboratively with your internal staff for rapid results.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row d-flex align-items-center">
				<div class="col-xs-12 col-sm-12 col-lg-6">
					<img src="<?php echo(SITE_URL)?>yogesh_new1/images/img-1.png" alt="" />
				</div>
				<div class="col-xs-12 col-sm-12 col-lg-6">
					<div class="intro-text">
						<h3>Develop and implement <br class="visible-lg" />next stage processes and <br class="visible-lg" />metrics.</h3>
					</div>
				</div>
			</div>
		</div>
		<div class="container-wrap">
			<div class="row d-flex flex-wrap">
				<div class="col-sm-2">
					<img src="<?php echo(SITE_URL)?>yogesh_new1/images/img-2.png" alt="" />
					<h4><a href="javascript:;">Financial analysis to understand and choose between options</a></h4>
				</div>
				<div class="col-sm-2">
					<img src="<?php echo(SITE_URL)?>yogesh_new1/images/img-3.png" alt="" />
					<h4><a href="javascript:;">Identify complex systems or tools to upgrade, implement, and train</a></h4>
				</div>
				<div class="col-sm-2">
					<img src="<?php echo(SITE_URL)?>yogesh_new1/images/img-4.png" alt="" />
					<h4><a href="javascript:;">Research and execute high-potential growth ideas</a></h4>
				</div>
				<div class="col-sm-2">
					<img src="<?php echo(SITE_URL)?>yogesh_new1/images/img-5.png" alt="" />
					<h4><a href="javascript:;">C-level industry or function specialist to step in immediately</a></h4>
				</div>
				<div class="col-sm-2">
					<img src="<?php echo(SITE_URL)?>yogesh_new1/images/img-6.png" alt="" />
					<h4><a href="javascript:;">Develop processes to improve productivity, train team, and support implementation</a></h4>
				</div>
			</div>
		</div>
	</div>

	<div class="m-testimonial review-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="member-detail mb-title">
						<h2>What our clients are saying about us</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="t-testimonial">
						<div class="item">
					        <div class="t-img">
					        	<img src="<?php echo(SITE_URL)?>yogesh_new1/images/Judy-bardwick.jpg" alt="Judy bardwick">
					        </div>
					        <div class="t-detail">
					        	<p>Some do community service as a way to constructively use their time, taking on pleasant, perhaps even challenging, projects to help their favorite nonprofit. Mark Palmer has the reputation of diving..</p>
					        	<h4>Judy Bardwick</h4>
					        	<span>Leading Organizational Psychologist</span>
					        </div>
					    </div>
						<div class="item">
					        <div class="t-img">
					        	<img src="<?php echo(SITE_URL)?>/imgs/Mark-Palmer-Guild1.jpg" alt="Mark Palmer">
					        </div>
					        <div class="t-detail">
					        	<p>Some do community service as a way to constructively use their time, taking on pleasant, perhaps even challenging, projects to help their favorite nonprofit. Mark Palmer has the reputation of diving..</p>
					        	<h4>Mark Palmer</h4>
					        	<span>Founder/ Principal, Focus LLC</span>
					        </div>
					    </div>
						<div class="item">
					        <div class="t-img">
					        	<img src="<?php echo(SITE_URL)?>/imgs/Chip-Evans-Guild.jpg" alt="Chip Evans">
					        </div>
					        <div class="t-detail">
					        	<p>Some do community service as a way to constructively use their time, taking on pleasant, perhaps even challenging, projects to help their favorite nonprofit. Mark Palmer has the reputation of diving...</p>
					        	<h4>Chip Evans, Ph.D</h4>
					        	<span>President & Founder at The Evans Group LLC</span>
					        </div>
					    </div>
						<div class="item">
					        <div class="t-img">
					        	<img src="<?php echo(SITE_URL)?>/imgs/Roza-Rojdev-Guild.png" alt="Roza Rojdev">
					        </div>
					        <div class="t-detail">
					        	<p>Some do community service as a way to constructively use their time, taking on pleasant, perhaps even challenging, projects to help their favorite nonprofit. Mark Palmer has the reputation of diving...</p>
					        	<h4>Roza S. Rojdev, PsyD</h4>
					        	<span>Managing Partner at Xecutive Metrix</span>
					        </div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="masters-section">
		<div class="container">
			<div class="row text-center">
				<div class="col-sm-12">
					<div class="section-title">
						<h2>Subject-matter experts (SMEs)</h2>
						<p>For exceptional results, GUILD works with professionals with at least two decades of deep domain expertise. We vet their track records based on client feedback, credentials and industry reputation. Several are recognized thought leaders in their respective areas. Further, before recommending them for a project we evaluate them for cultural compatibility, direct prior experience and budget-fit. Getting good client feedback is essential for our SMEs to work on future projects.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="service-box">
						<div class="service-icon">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/master-1.jpg" alt="" />
						</div>
						<div class="service-detail">
							<h4>Tom Northup</h4>
							<p>Leadership, management and strategy coach developing sustainable profitability and unfair competitive advantage</p>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-box">
						<div class="service-icon">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/master-2.jpg" alt="" />
						</div>
						<div class="service-detail">
							<h4>Ettie Shapiro</h4>
							<p>Immunity To Change Faciilitator, Winslow Assessment Coach, Catalyst For Enduring Change </p>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-box">
						<div class="service-icon">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/master-3.jpg" alt="" />
						</div>
						<div class="service-detail">
							<h4>Jason Kitayama</h4>
							<p>Providing insights into the customer experience to create a better product development process.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row d-flex justify-content-center text-center">
				<div class="col-sm-12">
					<a href="javascript:;" class="btn-red"><i class="fa fa-search"></i> Search for expertise</a>
				</div>
			</div>
		</div>
	</div>

	<div class="companys-section">
		<div class="container-wrap">
			<div class="row d-flex justify-content-center text-center">
				<div class="col-md-12 col-lg-8">
					<div class="section-title">
						<h2>Our consultants bring a wealth of experience from <br class="visible-lg">top-tier companies</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<ul class="d-flex justify-content-between">
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/company-1.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/company-2.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/company-3.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/company-4.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/company-5.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/company-6.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/company-7.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/company-8.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/company-9.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/company-10.png" alt="" /></li>
					    <li><img src="<?php echo(SITE_URL)?>yogesh_new1/images/company-11.png" alt="" /></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="requirements-section partnership-section">
		<div class="container-wrap">
			<div class="row d-flex justify-content-center text-center">
				<div class="col-sm-12">
					<div class="section-title">
						<h2>Start-to-finish partnership</h2>
						<p>GUILD works with you to develop a team that fits your project's unique needs, and supports the project until it's complete.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="service-box">
						<div class="service-icon">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/icon-4.png" alt="" />
						</div>
						<div class="service-detail">
							<p>Tell us your business needs, and we'll help you identify and define your project.</p>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-box">
						<div class="service-icon">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/icon-5-new.png" alt="" />
						</div>
						<div class="service-detail">
							<p>Receive proposals from a vetted selection of experts who fit your project needs. Use our Free Consultations to select an expert. </p>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="service-box">
						<div class="service-icon">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/icon-6-new.png" alt="" />
						</div>
						<div class="service-detail">
							<p>Your expert will be supported by a dedicated Client Partner who provides project management support and integration with your company.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row d-flex justify-content-center text-center">
				<div class="col-sm-12">
					<p class="notes">GUILD's professionals support your project until you're satisfied.</p>
				</div>
			</div>
		</div>
	</div>

	<div class="industry-section">
		<div class="container">
			<div class="row d-flex justify-content-center text-center">
				<div class="col-sm-12">
					<div class="section-title">
						<h2>At the intersection of industry and function</h2>
						<p>The GUILD network includes highest-grade industry professionals to support any function you need.  GUILD experts and Client Partners bring deep training and experience to your company.</p>
					</div>
				</div>
			</div>
			<div class="row graphics-box">
				<div class="col-sm-12 d-flex align-items-center justify-content-center">
					<ul>
					    <li>Retail</li>
					    <li>Professional services</li>
					    <li>Education</li>
					    <li>Technology</li>
					</ul>
					<div class="function-img">
						<img src="<?php echo(SITE_URL)?>yogesh_new1/images/function-graph.png" alt="" />
					</div>
					<ul>
					    <li>Financial projections</li>
					    <li>Human resources</li>
					    <li>Supply chain</li>
					    <li>Executive coaching</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 text-center">
					<p class="info">When you need subject matter expertise.</p>
					<a href="javascript:;" class="btn-red"><i class="fa fa-search"></i> Search for expertise</a>
				</div>
			</div>
		</div>
	</div>

</div>

<!-- js link -->
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery_validate_min_js.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jssor.slider-22.0.15.mini.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/script12.js"></script>

<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/slick.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
	    jQuery('.t-testimonial').slick({
	      dots: false,
	      arrows: true,
	      infinite: true,
	      speed: 300,
	      slidesToShow: 1,
	      adaptiveHeight: true,
	      draggable: false
	    });
    });
</script>