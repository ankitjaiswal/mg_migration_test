<div class="content-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="inner-content-wrapper" class="premium-website">
                    <div id="inner-content-box" class="pagewidth">
                        <div class="inner_wrapper">
                            <div class="premium-title">
                                <h3>Cancel premium website</h3>
                            </div>
                            <div class="premium-user-info">
                                <?php 
                                    $urlk1 = ($this->data['User']['url_key']);
                                    $displink=SITE_URL.strtolower($urlk1); 
                                          ?>
                                <div class="premium-user-img">
                                    <?php
                                    $flag = 0; $defaultQuestion=0;
                                    if (isset($this->data['UserImage'][0]['image_name']) && $this->data['UserImage'][0]['image_name'] != '') {
                                        $file = file_exists(WWW_ROOT . 'img' . '/' . MENTORS_IMAGE_PATH . '/' . $this->data['User']['id'] . '/' . $this->data['UserImage'][0]['image_name']);
                                        if ($file == 0) {
                                            $flag++;
                                        }
                                    } else {
                                        $flag++;
                                    }
                                    if ($flag == 0) {
                                        echo($this->Html->link($this->Html->image(MENTORS_IMAGE_PATH . '/' . $this->data['User']['id'] . '/' . $this->data['UserImage'][0]['image_name'],array('alt' => $this->data['UserReference']['first_name'],'class'=>'thumb')),$displink,array('escape'=>false,'style'=>'border: 0px;')));
                                  } else {
                                        echo($this->Html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name'],'class'=>'thumb')));
                                    }
                                  ?>
                                </div>
                                <div class="premium-user-detail">  
                                    <h2><?php echo($this->Html->link(ucwords($this->data['UserReference']['first_name']).' '.($this->data['UserReference']['last_name']),$displink,array('escape'=>false)));?></h2>
                                    <?php
                                        $disp="guild.im/".strtolower($this->data['User']['url_key']);
                                        $completelink=SITE_URL.strtolower($this->data['User']['url_key']);
                                        $displink=SITE_URL.strtolower($this->data['User']['url_key']);
                                        ?>
                                        <a href="<?php echo($displink);?>"><?php echo($disp); ?></a>
                                    <ul>
                                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Boost your online traffic</li>
                                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Engage your visitors</li>
                                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Convert visitors to prospects</li>
                                    </ul>
                                    <p><i class="fa fa-check-square-o" aria-hidden="true"></i>Premium website costs <span>$199 per month</span>. You can cancel any time.</p>
                                </div>
                                <div class="premium-user-btm">
                                    <span class="premium-btn"><?php echo($this->Html->link('Cancel my premium membership',SITE_URL.'members/striperecurrcancel',array('class'=>'reset','style'=>""))); ?></span>
                                    <div class="pull-right">
                                     <input class="banner-getstarted btn btn-default get-started" type="button"  value="Keep my premium membership" onclick="window.location.href='<?php echo SITE_URL."users/my_account" ?>';">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>




