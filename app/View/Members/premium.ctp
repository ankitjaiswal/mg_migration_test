<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="google-site-verification" content="UroIkuqOwc9A-KngERWfg2Mam9Fw_0UmVECjjujMYMY" />
        <script type="text/javascript">var NREUMQ=NREUMQ||[];NREUMQ.push(["mark","firstbyte",new Date().getTime()]);</script>
	 <meta name="format-detection" content="telephone=no" />	
        <link rel="icon" href="/favicon.ico?v=2" type="image/x-icon" />
        <link rel="shortcut icon" href="/favicon.ico?v=2" type="image/x-icon" />

        </head>

<div class="premium-content one">
    <div class="pagewidth">
        <img src ="<?php echo(SITE_URL)?>/imgs/stars.png">
        <h1>Guild Premium</h1>

		<h2>Marketing best-practices for experienced business consultants &#8212; Starting at $99/ Month.</h2>
    </div>


</br></br><h2 style="text-decoration: underline;">Featured Work</h2>
</br></br></br>
<div class="pagewidth">
<div style="height: 180px; width: 230px;float: left; text-align: center;">
<a href="<?php echo(SITE_URL)?>press_material/Strategies_For_Unlocking_Hidden_Value_Within_Your_Supply_Chain.pdf" target="_blank"><img src ="<?php echo(SITE_URL)?>press_material/book.png" style="width:120px;height:120px;"></a>
<div style="padding-top:10px;"><a href="<?php echo(SITE_URL)?>press_material/Strategies_For_Unlocking_Hidden_Value_Within_Your_Supply_Chain.pdf" style="text-align: center; font-size: 16px;color:#fff;" target="_blank">Ebook</a></div>
</div>
<div style="height: 180px; width: 230px;float: left; text-align: center;margin-left:10px;">
<a href="http://www.slideshare.net/iqbash1/qualify-your-sales-pipeline" target="_blank"><img src ="<?php echo(SITE_URL)?>press_material/default_icon-webinar.png" style="width:150px;height:150px;margin-top:-20px;"></a>
<div style="padding-top:0px;"><a href="http://www.slideshare.net/iqbash1/qualify-your-sales-pipeline" style="text-align: center; font-size: 16px;color:#fff;" target="_blank">Webinar</a></div>
</div>
<div style="height: 180px; width: 230px;float: left; text-align: center;margin-left:10px;">
<a href="https://www.linkedin.com/company/comprehensive-learning-solutions" target="_blank"><img src ="<?php echo(SITE_URL)?>press_material/icon-linkedin.png" style="width:110px;height:110px;"></a>
<div style="padding-top:20px;"><a href="https://www.linkedin.com/company/comprehensive-learning-solutions" style="text-align: center; font-size: 16px;color:#fff;" target="_blank">Linkedin Company Page</a></div>
</div>
<div style="height: 180px; width: 230px;float: left; text-align: center;margin-left:10px;">
<a href="http://www.expertclick.com/NRWire/Releasedetails.aspx?id=67833" target="_blank"><img src ="<?php echo(SITE_URL)?>press_material/press-release-icon.png" style="width:110px;height:110px;"></a>
<div style="padding-top:20px;"><a href="http://www.expertclick.com/NRWire/Releasedetails.aspx?id=67833" style="text-align: center; font-size: 16px;color:#fff;" target="_blank">Press Release</a></div>
</div>
</div>



</div>


<div class="premium-content two dark">
    <div class="pagewidth">
    <div class="float-l"><img src="<?php echo(SITE_URL)?>/imgs/profile.png" style="padding-top:4px;"></div>
    <h3>Make a Great First Impression</h3>
    <p>Guild Premium helps you stand out in a crowded marketplace.</p>

    <ul class="premium-list">
        

        <li class="">Be featured on the homepage and rank higher in our search listings.</li>
<li class="">Personalize your profile with a <a href="#" onclick="TINY.box.show({iframe:'//www.youtube.com/embed/j6b3dYBYX_k',width:860,height:460,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">short video introduction</a>.</li>
        <li class="">Optimize your Guild and social media profiles.</li>
    </ul>
</div>
</div>

<div class="premium-content three">
    <div class="pagewidth">
		<div class="float-r"><img src="<?php echo(SITE_URL)?>/imgs/promotion.png"></div>
        <h3>Amplify Your Marketing</h3>
        <p>Online marketing is important, but it takes time away from your clients. Guild Premium fills that gap.</p>
        <ul class="premium-list">
            <li class=""><a href="<?php echo(SITE_URL)?>members/pr" target="_blank">Connect with journalists or send a press release</a>.</li>
            
            <li class="">Let us manage and grow your social media accounts.</li>
            <li class="">Tap ghostwriters for regular, high-quality content.</li>

        </ul>
    </div>
</div>

<div class="premium-content four dark">
    <div class="pagewidth">
        <div class="float-l"><img src="<?php echo(SITE_URL)?>/imgs/prospecting.png"></div>
		<h3>Fill Your Funnel</h3>
        <p>Relationships and consistency drive sales. We help you achieve both through professionally managed email campaigns.</p>
        <ul class="premium-list">
            <li class="">Leverage best practices in email marketing and newsletters.</li>
            <li class="">Focus your prospecting efforts on those who engage with the campaign.</li>

        </ul>
    </div>
</div>

<div class="premium-content five">
    <div class="pagewidth">
	<h3 class="header">Grow Your Business</h3>
        <p>Stand out among your peers, extend your reach, and shorten your sales cycle.</p>


<div id='carousel_container' class="pagewidth">
   <div id='left_scroll'><a href='javascript:slide("left");' class="buttons prev"><img src='<?php echo(SITE_URL)?>/press_material/new_left_nav.png' /></a></div> 
    <div id='carousel_inner'>  
        <ul id="carousel_ul">


          <li>

                <div class="mg_quote2_container" onclick="window.open('<?php echo(SITE_URL);?>john.baldoni','mywindow');" style="cursor: hand;width:650px;">
		    <div class="mg_photo">
		        <img src="<?php echo(SITE_URL)?>/imgs/John-Baldoni-MentorsGuild1.jpg">
		    </div>
		    <div class="mg_quote_box">

			    <div class="premium-testimonial-content">
				<div class="premium-testimonial2-quote">Over last several months, Guild has been instrumental in building my personal brand and influence on Linked In platform.</div>
				<div class="premium-testimonial2-name" style="margin-top:83px;">John Baldoni</div>
				<div class="premium-testimonial2-org">Chair, Leadership Development at N2Growth</div>

		    
           </li>


          <li>

                <div class="mg_quote2_container" onclick="window.open('<?php echo(SITE_URL);?>roza.rojdev','mywindow');" style="cursor: hand;width:650px;">
		    <div class="mg_photo">
		        <img src="<?php echo(SITE_URL)?>/imgs/Roza.png">
		    </div>
		    <div class="mg_quote_box">

			    <div class="premium-testimonial-content">
				<div class="premium-testimonial2-quote">As a partner to Xecutive Metrix, the Guild team is offering services that help our firm increase our level of exposure to new markets, connect us with other value added consultants in the network and provide us with peace of mind that our marketing efforts are running smoothly. Now we can focus on what we do best&mdash;Executive Leadership Development.</div>
				<div class="premium-testimonial2-name" style="margin-top:30px;">Roza S. Rojdev, PsyD</div>
				<div class="premium-testimonial2-org">Managing Partner at Xecutive Metrix</div>

		    
           </li>

          <li>
	    <div class="mg_quote1_container" onclick="window.open('<?php echo(SITE_URL);?>mark.palmer','mywindow');" style="cursor: hand;width:650px;">
		    <div class="mg_photo">
		        <img src="<?php echo(SITE_URL)?>/imgs/mark_palmer.jpg">
		    </div>
		    <div class="mg_quote_box">

			    <div class="premium-testimonial-content">
				<div class="premium-testimonial2-quote">By listing only the best consultants Guild is well on its way of marketing to businesses from a place of trust.</div>
				<div class="premium-testimonial2-name" style="margin-top:85px;">Mark Palmer</div>
				<div class="premium-testimonial2-org">Founder/ Principal, Focus LLC</div>
		    </div></div>
		</div>

           </li>
           <li>
	    <div class="mg_quote2_container" onclick="window.open('<?php echo(SITE_URL);?>judy.bardwick','mywindow');" style="cursor: hand;width:650px;">
		    <div class="mg_photo">
		        <img src="<?php echo(SITE_URL)?>/imgs/Judy_Bardwick.png">
		    </div>
		    <div class="mg_quote_box">

			    <div class="premium-testimonial-content">
				<div class="premium-testimonial2-quote" >I am delighted and amazed by the work Guild has done for me. It is personal, accurate, honest and original. The website they created is client-focused, eye-catching, and simultaneously captures the essence of what I deliver to Clients. And, the speed of the team's responsiveness and their attention to every detail <span style="font-style:italic;">is the best I have ever experienced</span>.</div>
				<div class="premium-testimonial2-name" style="margin-top:29px;">Judy Bardwick</div>
				<div class="premium-testimonial2-org">Leading Organizational Psychologist</div>
		    </div></div>
		</div>
             </li>
           </ul>

     </div>
  <div id='right_scroll'><a href='javascript:slide("right");' class="buttons next"><img src='<?php echo(SITE_URL)?>/press_material/new_right_nav.png' /></a></div>
  <input type='hidden' id='hidden_auto_slide_seconds' value=0 /> 
    </div>

    


              <?php if($this->Session->read('Auth.User.role_id') == 2 && $this->Session->read('Auth.User.mentor_type') != 'Premium Member')
              { ?> 
		<div class="mg-button"><a href="<?php echo(SITE_URL)?>pricing/striperecurr/">Start Your Guild Premium MEMBERSHIP</a></div>
              <?php } elseif($this->Session->read('Auth.User.role_id') == 2 && $this->Session->read('Auth.User.mentor_type') == 'Premium Member')
             {?>
             <div class="mg-button"><a href="<?php echo(SITE_URL)?>pricing/cancel_plan/">Cancel Your Guild Premium MEMBERSHIP</a></div>
             <?php }else{?>
             <div class="mg-button1" id="onclick"><a href="javascript:void(0);">I&#39;m interested &mdash; Tell me more!</a></div>
           <?php }?>

<div id="formbox" style="margin-top:20px;display:none;">
<?php echo $this->Form->create(null,array('url'=>array('controller'=>'members','action'=>'premium'),'id'=>'roundtableForm'));?>
<div id="name-box">                   
                    <div id="country">
    <div style="margin-left:220px;font-family:ubuntu;">
                    
	      							<table>
	      								<tbody>
	      									<tr>
	      										<td style="float: right; margin-top: 16px; margin-right: 10px;font-size:16px;">Name</td>
	      										<td>
	      											<input name="data[Mentorship][firstname]" type="text" class="forminput" id="userfname" value="" maxlength="120" placeholder="First" style="width:153px;">
													<input name="data[Mentorship][lastname]" type="text" class="forminput" id="userlname" value="" maxlength="120" placeholder="Last" style="width:155px;margin-left:-10px;">
	      										</td>
	      									</tr>
	      									<tr>
	      										<td style="float: right; margin-top: 25px; margin-right: 10px;font-size:16px;">Email</td>
	      										<td>
													<input name="data[Mentorship][email]" type="text" class="forminput" placeholder = "email@yourcompany.com" id="useremail" value="" maxlength="250" style="width:332px;margin-top: 10px;">      										
												</td>
	      									</tr>
	      									<tr style="display:none;">
	      										<td style="float: right; margin-top: 16px; margin-right: 10px;">Organization</td>
	      										<td>
													<input name="data[Mentorship][company]" type="text" class="forminput"  id="usercompany" value="Guild Premium request" maxlength="250" style="width:332px;">      										
												</td>
	      									</tr>
	      									<tr>
	      										<td style="float: right; margin-top: 24px; margin-right: 8px;font-size:16px;">Phone</td>
	      										<td>
													<input name="data[Mentorship][phone]" type="text" class="forminput" placeholder = "(xxx) xxx-xxxx" id="userphone" value="" maxlength="250" style="width:332px;margin-top: 10px;">      										
												</td>
	      									</tr>
                                                                      <tr>
                                                                        				<td style="float: right;margin-top: 45px;"></td>

			                                 <td>   <input id="sendButton" type="submit" value="Submit" class="mgButton" style="margin-bottom: 20px;margin-top: 15px; float: right;margin-right: 12px;">
						                             </td>
	      								</tbody>
	      							</table>


</div>



</div>

                 
</div>
</form>
</div>

        <div class="signup" style="margin-top: 50px;padding-left:15px;"><p>Guild Premium offers 5 hours of marketing support for  <span class="emphasize">$99 per month</span>. Additional hours are available at competitive rates.</p></div>
    </div>
</div>


<div class="six">
    <div class="pagewidth">

	</div>
	<div class="six">
        <div class="mg-link" style="font-size: 1.5em;line-height: 1.9;font-family: 'proximanova semibold',Ubuntu;font-weight: normal;">Have questions or suggestions about Guild Premium? Call us at 1-866-511-1898.</div>
        </div>
    </div>
</div>
 <script type="text/javascript">
    jQuery(document).ready(function(){
       jQuery("#onclick").click(function() {
       if(loggedUserId == ''){
        document.getElementById('formbox').style.display="block";
         }else{
		       jQuery.ajax({
					url:SITE_URL+'roundtable/registered_request',
					type:'post',
					dataType:'json',
					data: 'loggedUserId='+loggedUserId,
					success:function(loggedUserId){
						window.location.href = SITE_URL+'roundtable/thanks';
					}
				});	
		       
		   		return false;
		}
                   });
    });

</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>press_material/js/jquery.tinycarousel.min.js"></script>
 
<script type="text/javascript">
jQuery(document).ready(function(){
jQuery('#slider1').tinycarousel({
start: 1, // where should the carousel start?
display: 1, // how many blocks do you want to move at a time?
axis: 'x', // vertical or horizontal scroller? 'x' or 'y' .
controls: true, // show left and right navigation buttons?
pager: true, // is there a page number navigation present?
interval: true, // move to the next block on interval.
intervaltime: 6000, // interval time in milliseconds.
animation: true, // false is instant, true is animate.
duration: 700, // how fast must the animation move in milliseconds?
callback: null, // function that executes after every move
});
});
</script> 
<style>
.mg-button1 {
    color: #fff;
    background-color: #D03135;
    border-style: none;
    padding: 10px 10px 10px 10px;
    margin: auto;
    text-align: center;
    width: 100%;
    display: block;
    position: relative;
    right: 36px;
    margin-bottom: 10px;
}
.mg-button1 a {
    text-decoration: none;
    color: #fff;
    font-size: 26px;
    border-style: none;
    font-family: Ubuntu;
    font-weight: regular;
    padding: 10px 10px 10px 10px;
    margin: auto;
    text-align: center;
    }

.premium-testimonial2-quote{
margin-bottom:14px;

}
.premium-testimonial2-name{
margin-bottom:-2px;

}

#carousel_container{

overflow: hidden;  /* important (hide the items outside the div) */  
 
  margin-left: 80px;

}
#carousel_inner {  
float: left;
width: 670px;
height: 300px;
overflow: hidden;
//position: relative;
text-align:center;
}  
  
#carousel_ul {  
list-style: none;
position: relative;
padding: 0;
margin: 0;
width: 9999px;
left:-670px;
}  
  
#carousel_ul li{  
float: left;
//margin: 0 20px 0 0;
//padding: 1px;
height: 300px;
width: 670px;
//margin-left:10px; 
}  
  
#carousel_container .buttons {
margin: 60px 5px 0 0;
float: left;
}
#carousel_container .next {
margin: 60px 0 0 5px;
}
</style>

<script type="text/javascript">
jQuery(document).ready(function() {  
  
        //options( 1 - ON , 0 - OFF)  
        var auto_slide = 1;  
            var hover_pause = 1;  
        var key_slide = 1;  
  
        //speed of auto slide(  
        var auto_slide_seconds = 6000;  
        /* IMPORTANT: i know the variable is called ...seconds but it's 
        in milliseconds ( multiplied with 1000) '*/  
  
        /*move the last list item before the first item. The purpose of this is 
        if the user clicks to slide left he will be able to see the last item.*/  
        jQuery('#carousel_ul li:first').before(jQuery('#carousel_ul li:last'));  
  
        //check if auto sliding is enabled  
        if(auto_slide == 1){  
            /*set the interval (loop) to call function slide with option 'right' 
            and set the interval time to the variable we declared previously */  
            var timer = setInterval('slide("right")', auto_slide_seconds);  
  
            /*and change the value of our hidden field that hold info about 
            the interval, setting it to the number of milliseconds we declared previously*/  
            jQuery('#hidden_auto_slide_seconds').val(auto_slide_seconds);  
        }  
  
        //check if hover pause is enabled  
        if(hover_pause == 1){  
            //when hovered over the list  
            jQuery('#carousel_ul').hover(function(){  
                //stop the interval  
                clearInterval(timer)  
            },function(){  
                //and when mouseout start it again  
                timer = setInterval('slide("right")', auto_slide_seconds);  
            });  
  
        }  
  
        //check if key sliding is enabled  
        if(key_slide == 1){  
  
            //binding keypress function  
            jQuery(document).bind('keypress', function(e) {  
                //keyCode for left arrow is 37 and for right it's 39 '  
                if(e.keyCode==37){  
                        //initialize the slide to left function  
                        slide('left');  
                }else if(e.keyCode==39){  
                        //initialize the slide to right function  
                        slide('right');  
                }  
            });  
  
        }  
  
  });  
  
//FUNCTIONS BELLOW  
  
//slide function  
function slide(where){  
  
            //get the item width  
            var item_width = jQuery('#carousel_ul li').outerWidth() + 0;  

            /* using a if statement and the where variable check 
            we will check where the user wants to slide (left or right)*/  
            if(where == 'left'){  
                //...calculating the new left indent of the unordered list (ul) for left sliding  
                var left_indent = parseInt(jQuery('#carousel_ul').css('left')) + item_width; 

            }else{  
                //...calculating the new left indent of the unordered list (ul) for right sliding  
                var left_indent = parseInt(jQuery('#carousel_ul').css('left')) - item_width; 

  
            }  
  
            //make the sliding effect using jQuery's animate function... '  
            jQuery('#carousel_ul:not(:animated)').animate({'left' : left_indent},700,function(){  
  
                /* when the animation finishes use the if statement again, and make an ilussion 
                of infinity by changing place of last or first item*/  
                if(where == 'left'){  
                    //...and if it slided to left we put the last item before the first item  
                    jQuery('#carousel_ul li:first').before(jQuery('#carousel_ul li:last'));  
                }else{  
                    //...and if it slided to right we put the first item after the last item  
                    jQuery('#carousel_ul li:last').after(jQuery('#carousel_ul li:first'));  
                }  
  
                //...and then just get back the default left indent  
                jQuery('#carousel_ul').css({'left' : '-670px'});  
            });  
  
}  
</script>