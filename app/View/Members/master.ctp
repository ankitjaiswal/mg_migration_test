
<?php 

$name = $this->data['UserReference']['first_name'].''.$this->data['UserReference']['last_name'];
$email = $this->data['User']['username'];

			  $modeTypeFaceToFace=$modeTypePhone=$modeTypeVideo='FALSE' ;
			  $modeValue1=$modeValue2=$modeValue3='';
			  if(isset($this->data['Communication'][0]['mode_type']) && $this->data['Communication'][0]['mode_type'] != '')
			  {
			  		
					if($this->data['Communication'][0]['mode_type']=='face_to_face')
				{
						$modeTypeFaceToFace='TRUE';
			  			$modeValue1=$this->data['Communication'][0]['mode'];
					}
					else if($this->data['Communication'][0]['mode_type']=='phone')
						{
						  $modeTypePhone='TRUE';
			  			  $modeValue2=$this->data['Communication'][0]['mode'];
			  			}
			  			
			  			else
			  			{
			  			   $modeTypeVideo='TRUE';
			  			   $modeValue3=$this->data['Communication'][0]['mode'];
					 }
					
	
				
			}
			  if(isset($this->data['Communication'][1]['mode_type']) && $this->data['Communication'][1]['mode_type'] != '')
			  {
			  		
					if($this->data['Communication'][1]['mode_type']=='face_to_face')
				{
						$modeTypeFaceToFace='TRUE';
			  			$modeValue1=$this->data['Communication'][1]['mode'];
					}
					else if($this->data['Communication'][1]['mode_type']=='phone')
						{
						   $modeTypePhone='TRUE';
			  		       $modeValue2=$this->data['Communication'][1]['mode'];
			  			}
			  			
			  			else
			  			{
			  			   $modeTypeVideo='TRUE';
			  			   $modeValue3=$this->data['Communication'][1]['mode'];
					 }
					
		
				
			}
			 if(isset($this->data['Communication'][2]['mode_type']) && $this->data['Communication'][2]['mode_type'] != '')
			  {
			  		if($this->data['Communication'][2]['mode_type']=='face_to_face')
					{
						$modeTypeFaceToFace='TRUE';
			  			$modeValue1=$this->data['Communication'][2]['mode'];
					}
						else if($this->data['Communication'][2]['mode_type']=='phone')
						{
						   $modeTypePhone='TRUE';
			  		       $modeValue2=$this->data['Communication'][2]['mode'];
			  			}
			  			
			  			else
			  			{
			  			   $modeTypeVideo='TRUE';
			  			   $modeValue3=$this->data['Communication'][2]['mode'];
					 }
					
					
			  }

                      if($modeTypePhone=='TRUE' && trim($modeValue2)!=''){

                       $phone = $modeValue2;

                      }

?>


	<div id="banner-section"  data-parallax="scroll" data-image-src="<?php echo(SITE_URL)?>/yogesh_new1/images/master/banner.jpg">
		<div class="col-md-12 pad0 text-center banner-content">
			<img src="<?php echo(SITE_URL)?>/yogesh_new1/images/master/stars.png" class="start-img">
			<h1>GUILD Master</h1>
			<p>Digital marketing for experienced business consultants &#8211; Starting at $999/ month.</p>
			<button class="banner-getstarted btn btn-default get-started">
				Get started
			</button>
			
		</div>
	</div>
	<!-- portfolio section -->
	<div id="portfolio-section">
		<div class="container">
			<div class="testimonial-heading text-center">
				<h1>Featured works</h1>

			</div>
			<div class="portfolio-section">
				<div class="featured-work">
					<div class="col-md-3 col-sm-3 col-xs-6 mpad0 featured-desc text-center">
						<a href="<?php echo(SITE_URL)?>press_material/Strategies_For_Unlocking_Hidden_Value_Within_Your_Supply_Chain.pdf" target="_blank">
							<img src="<?php echo(SITE_URL)?>/yogesh_new1/images/master/ebook.png">
							<label>Ebook</label>
						</a>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6 mpad0 featured-desc text-center">
						<a href="http://www.slideshare.net/iqbash1/qualify-your-sales-pipeline" target="_blank">
							<img src="<?php echo(SITE_URL)?>/yogesh_new1/images/master/webniar.png">
							<label>Webinar</label>
						</a>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6 mpad0 featured-desc text-center">
						<a href="https://www.linkedin.com/company/comprehensive-learning-solutions" target="_blank">
							<img src="<?php echo(SITE_URL)?>/yogesh_new1/images/master/linkedin.png">
							<label>Linkedin company page</label>
						</a>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6 mpad0 featured-desc text-center">
						<a href="http://www.expertclick.com/NRWire/Releasedetails.aspx?id=67833" target="_blank">
							<img src="<?php echo(SITE_URL)?>/yogesh_new1/images/master/press.png">
							<label>Press release</label>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- impression section -->
	<div id="impression-section">
		<div class="container">
			<div class="col-md-3 mpad0 pad0 text-center">
				<img src="<?php echo(SITE_URL)?>/yogesh_new1/images/master/profile.png">
			</div>
			<div class="col-md-9 mpad0 impression-desc">
				<h1>Make a great first impression</h1>
				<p>GUILD Master helps you stand out in a crowded marketplace.</p>
				<ul>
					<li>Enhance social profiles for yourself and your firm on LinkedIn, GUILD, Facebook and Twitter.</li>
					<li>Get featured on the GUILD homepage, and rank higher in our search listings.</li>
					<li>Includes a subscription to our fully-managed GUILD Websites service.</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- impression section -->
	<div id="impression-section" class="ampliy-section">
		<div class="container">
			<div class="col-md-3 mpad0 text-center mshow">
				<img src="<?php echo(SITE_URL)?>/yogesh_new1/images/master/promotion.png">
			</div>
			<div class="col-md-9 mpad0 impression-desc">
				<h1>Amplify Your Marketing</h1>
				<p>Manage your online presence without taking time away from your clients.</p>
				<ul>
					<li>Expand your reach through professional management of your social media accounts.</li>
					<li>Save time with a weekly, ghostwritten LinkedIn article or blog post.</li>
					<li>Nurture clients and prospects through value-added newsletters produced monthly.</li>
				</ul>
			</div>
			<div class="col-md-3 mpad0 text-center mhide">
				<img src="<?php echo(SITE_URL)?>/yogesh_new1/images/master/promotion.png">
			</div>
		</div>
	</div>
	<!-- impression section -->
	<div id="impression-section">
		<div class="container">
			<div class="col-md-3 mpad0 pad0 text-center">
				<img src="<?php echo(SITE_URL)?>/yogesh_new1/images/master/prospecting.png">
			</div>
			<div class="col-md-9 mpad0 impression-desc">
				<h1>Fill Your Funnel</h1>
				<p>Professionally managed campaigns help you build relationships and drive sales.</p>
				<ul>
					<li>Leverage best practices in email marketing and ad campaigns.</li>
					<li>Focus your prospecting efforts on those who engage with the campaigns.</li>
					<li>Showcase your expertise through a collaboratively produced ebook, whitepaper or presentation, every quarter.</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- testimonial section -->
	<div id="testimonial-section">
		<div class="container">
			<div class="testimonial-heading text-center">
				<h1>Grow your business</h1>
				<p>Stand out among your peers, extend your reach, and shorten your sales cycle.</p>
			</div>
			<div class="testimonial-container">
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner" role="listbox">
                             <div class="item active">
				<div class="tesimonial-part">
					<div class="person-img">
						<img src="<?php echo(SITE_URL)?>imgs/John-Baldoni-Guild.jpg">
					</div>
					<div class="person-desc">
						<p>
							Over last several months, GUILD has been instrumental in building my personal brand and influence on LinkedIn platform.
						</p>
						<label>John Baldoni</label>
						<span>President, Baldoni Consulting LLC</span>
					</div>
				</div>
                             </div>
                             <div class="item">
				<div class="tesimonial-part">
					<div class="person-img">
						<img src="<?php echo(SITE_URL)?>imgs/Roza-Rojdev-Guild.png">
					</div>
					<div class="person-desc">
						<p>
							As a partner to Xecutive Metrix, the GUILD team is offering services that help our firm increase our level of exposure to new markets, connect us with other value added consultants in the network and provide us with peace of mind that our marketing efforts are running smoothly. Now we can focus on what we do best&mdash;Executive Leadership Development.
						</p>
						<label>Roza S. Rojdev, PsyD</label>
						<span>Managing Partner at Xecutive Metrix</span>
					</div>
				</div>
                              </div>
                             <div class="item">
				<div class="tesimonial-part">
					<div class="person-img">
						<img src="<?php echo(SITE_URL)?>imgs/Mark-Palmer-Guild1.jpg">
					</div>
					<div class="person-desc">
						<p>
							By listing only the best consultants GUILD is well on its way of marketing to businesses from a place of trust.
						</p>
						<label>Mark Palmer</label>
						<span>Founder/ Principal, Focus LLC</span>
					</div>
				</div>
                              </div>
                             <div class="item">
				<div class="tesimonial-part">
					<div class="person-img">
						<img src="<?php echo(SITE_URL)?>imgs/Judy-Bardwick-Guild.png">
					</div>
					<div class="person-desc">
						<p>
							I am delighted and amazed by the work GUILD has done for me. It is personal, accurate, honest and original. The website they created is client-focused, eye-catching, and simultaneously captures the essence of what I deliver to Clients. And, the speed of the team's responsiveness and their attention to every detail <span style="font-style:italic;">is the best I have ever experienced</span>.
						</p>
						<label>Judy Bardwick</label>
						<span>Leading Organizational Psychologist</span>
					</div>
				</div>
			</div>
                      </div>
					<!-- Controls -->
					<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						<span class="fa fa-angle-left fa-4x" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						<span class="fa fa-angle-right fa-4x" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
                     </div>
                    </div>
		</div>
	</div>
	<!-- talkto us -->
	<div id="interested-section">
		<div class="container text-center" style="margin-top:60px;">
			<div class="form-section">
				<button class="btn btn-default get-started" id="tell-more">
					I'm interested &#8211; Tell me more!
				</button>
				<form class="form-group form-interest hide" id="contact-form" autocomplete="off">
					<div class="form-group">
						<input type="text" class="form-control" name="name" id="name" placeholder="Enter your name" value="<?php echo($name);?>">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="email_id" id="email_id" placeholder="Enter your email" value="<?php echo($email);?>">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="contact_no" id="contact_no" placeholder="Enter your phone" value="<?php echo($phone);?>">
					</div>
					<div style="display:none;">
						<input type="text" class="form-control" name="purpose" id="purpose" value="Interest in GUILD Master">
					</div>
					<button class="btn btn-default get-started">
						Submit
					</button>
				</form>
				<!-- success message -->
				<div class="form-success hide">
					<p>Thank you for your interest! We will get back to you soon.</p>
				</div>
				<!-- success message -->
				<span>Have questions or suggestions about GUILD Master? <br> Call us at 1-866-511-1898</span>
			</div>
		</div>
	</div>

	<!-- js link -->
	<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>-->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>-->

	<script type="text/javascript">
		// parallax
		if($(window).width() >991){
			$('#banner-section').parallax();
		}
		</script>

