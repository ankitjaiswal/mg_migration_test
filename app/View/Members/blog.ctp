<div class="container-fluid" style="margin-top: 90px;overflow-x:hidden">
	<div class="row">
		<div class="hero-background-blog well-lg" >
			<div class="container well-lg">
				<div class="col-md-12 well-lg text-center">
					<h2 style="margin-top:11%;">The Technical Elements Every <br> Modern Website Needs</h2>
					
					
					
                                     

				</div>
				
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="row" >
					
			<div class="container">
				<div class="col-md-12 well-lg modern">
						

					
					<p>There are a million things you can do to perfect your website and create a larger online presence. In terms of technical aspects, to adding content and keywords, to simply
						promoting your URL to receive more clicks, the ways to increase web traffic are practically
						endless.</p>
					<p>Many of these ideals, of course, come with personal preference. Others, however, can result in actual errors, diverting traffic away from your site without you ever realizing it.</p>
					<p>To ensure your website is driving as much traffic as possible... and that you aren't introducing technical errors that could hurt your traffic levels, look at these five elements.
						Each is absolutely crucial in boosting website clout and traffic.</p>
				</div>
			
			</div>	
		</div>
 
		<div class="row" style="background-color: #f4f4f4;">
					
			<div class="container">
				<div class="col-md-12 well-lg modern">
						<h2 class="text-center text-uppercase"style="font-size: 36px;margin-top: 50px;"> Mobile Responsive</h2>
						<hr class="head-hr" align="center" width="20%;">

					<img src="<?php echo(SITE_URL)?>sanchit/images/alld.png" style="float:right;width:500px;" />
					<p>Do your clients have mobile phones? (It's 2019, who doesn't?!) Then they are looking at your website on their phone. In fact, more than 50% of website traffic is now driven from mobile devices.</p>
					<p>To accommodate this, your website needs to have a mobile responsive design, which means images and text will automatically rearrange to display an optimal layout regardless of the size of the screen. Without a design that responds to screen size, websites can show errors, take too long to load, or even cut off important information and links.</p>

					<p>Google will even penalize your site if it does not render correctly on mobile devices by lowering your ranking in search engine results. This is one website aspect you don't want to overlook.</p>
				</div>
			
			</div>	
		</div>
			

		<div class="row" >
					
			<div class="container">
				<div class="col-md-12 well-lg modern">
						<h2 class="text-center text-uppercase"style="font-size: 36px;margin-top: 50px;"> Security (SSL Certificate)</h2>
						<hr class="head-hr" align="center" width="20%">

					
					<p>An SSL Certificate (secure socket layer) provides encrypted communication between your website and the browser, ensuring that information entered into an online form is not compromised.</p>
					<p>This is obviously important for websites collecting things like credit card numbers or other personal information. It's also reassuring when filling out contact forms before sharing your name and email. However, an SSL certificate is now important for any professional website, even if you are not collecting any information from your visitors.</p> 
					<p>Because Google deemed website security to be very important back in 2017 people using Google's Chrome browser began to see Not Secure when visiting websites that do not have an active SSL Certificate.<img src="<?php echo(SITE_URL)?>sanchit/images/secure.png" style="float:right;" /></p>
					<p>This is not exactly the best first impression when building confidence in a potential client.</p>
					<p>And just like sites without mobile responsive design, Google penalizes sites without SSL certificates in search results.</p>
					<img src="<?php echo(SITE_URL)?>sanchit/images/secreen5.png" />
					<img src="<?php echo(SITE_URL)?>sanchit/images/secreen7.png" />
				</div>
			
			</div>	
		</div>

		<div class="row" style="background-color: #f4f4f4;">
					
			<div class="container">
				<div class="col-md-12 well-lg modern">
						<h2 class="text-center text-uppercase"style="font-size: 36px;margin-top: 50px;"> Load Speed</h2>
						<hr class="head-hr" align="center" width="20%">

					
					<p>Studies have shown that if a website doesn't load quickly, more than half will give up in less than four seconds. (Four seconds is incredibly quick, yet it makes a HUGE difference in visitor retention.) This is also a factor for which Google will hand out penalties.</p>
					<p><a href="https://www.pingdom.com/" target="_blank">Pingdom</a> has a free tool you can use to measure your site's load speed.</p> 
					<p>Remember that the main culprits in slow loading website are large images and poorly implemented code. There are plenty of tools developers can use to improve the performance of their code. Meanwhile, a great tool for reducing the size of your images is <a href="https://tinypng.com/" target="_blank">TinyPNG</a>.</p>
					<p>Another factor in load speed is the quality of your hosting. Who you host your website with matters because the quality and configuration of the servers they use directly affect the performance of your website. (In other words, cheap hosting can cause your website to load very slowly.) </p>
					<p>Additionally, a quality hosting provider will have better support, security, and uptime. <a href="https://wpengine.com/" target="_blank">WP Engine</a> is an example of hosting solution that can have specific benefits for websites running on WordPress.</p>
					<p>Finally, images can be helped by using a CDN (content delivery network) which will load the image from a server closest to the viewer thereby speeding up delivery. Most premium web hosting companies will have the availability of a CDN.</p>
					
				</div>
			
			</div>	
		</div>

		<div class="row" >
					
			<div class="container">
				<div class="col-md-12 well-lg modern">
						<h2 class="text-center text-uppercase"style="font-size: 36px;margin-top: 50px;"> Meta Tags</h2>
						<hr class="head-hr" align="center" width="20%">

					
					<p>Meta tags are pieces of code that provide information about your website to search engines, and your visitors.</p>
					<p>One example of a meta tag that is valuable to both your visitors and search engines is the title tag. In the images below you can see the title tag, "Expertise On Demand" is displayed on the browser tab for the GUILD.im home page that appears in search results, helping searchers quickly identify the website they are looking for. </p> 
					
					<p class="text-center"><img src="<?php echo(SITE_URL)?>sanchit/images/screen3.png" /></p>
					<p>The meta description, usually no more than 160 characters, is your opportunity to pique the interest of a potential visitor with a short description of what they can expect to find on your website.</p>
					<p class="text-center"><img src="<?php echo(SITE_URL)?>sanchit/images/screen4.png" /></p>
					<p>Fixing these few technical issues can help raise your website's status with Google and make it easier for your potential clients to find you online. If you'd like our help with implementing these changes and want to learn more about our services, contact us today. </p>
					<p class="text-center"><a href="<?php echo(SITE_URL)?>members/website"><button class="btn-lg btn-danger">VIEW WEBSITE SERVICES</button></a></p>	

				</div>
			
			</div>	
		</div>





<style>

.hero-background-blog h2 {
    font-weight: 800;
    color: #fff;
    font-size: 75px !important;
}
.hero-background-blog
{
background:url('https://test.guild.im/sanchit/images/blog-hero.png');
background-repeat: no-repeat;
background-position-y:80%;
background-size: cover;
height:450px;
}
ul li {
    list-style-type: disc;
}
.Guild-list-center li {

    font-size: 14px;

}
.Guild-list-center
{
margin: 0 auto;
left: 0;
right: 0;
display: inline-block;
padding-left: 55px;
padding-bottom: 120px;
}
.red
{
color : #cf3135
}
.bold
{
font-weight:bold;
}
.head-hr
{
border-top:6px solid #000;
width: 19%;
display: block;
margin: 0 auto;
}

.modern p {
    font-size: 20px;
    margin-top: 35px;
}
.modern a {
    //border-bottom: 1px solid;
    }
.matched p {
    margin: 25px 0 78px 0px;
}
.row.guild-features .head-hr {

    margin-bottom: 40px;

}
h1, h2, h3, h4, h5, h6, .btn {

    font-family: opensansregular !important;

}
.mobile-banner
{
display:none;
}
@media only screen and (max-width: 640px)
{
.hero-background-blog, .hero-background-blog .well-lg
{
padding:0px !important;
}
.hero-background-blog h2
{
font-size:22px!important;
margin-top:55px !important;
}
.text-center.text-uppercase
{
font-size:35px !important;
}
.container-fluid
{
padding-left:20px;
padding-right:20px; 
}
.modern img 
{
    margin: 50px 0px 50px 0px!important;
}
.hero-background-blog {
    background: url(https://test.guild.im/sanchit/images/blog-background.png);
    background-repeat: no-repeat;
    background-size: cover; 
    height: 172px;
    background-position: center;
}
}
.row.schedule-guild h3 {

    line-height: 37px;

}
.modern img {
    margin: 63px 27px 50px 100px;
}
button.btn-lg.btn-danger {
    border: none;
    margin-top: 20px;
}
@media only screen and (min-width:641px) and (max-width:990px)
{
.hero-background-blog h2 {
    font-weight: 800;
    color: #fff;
    font-size: 59px !important;
}
}
@media only screen and (min-width:988px) and (max-width:1199px)
{
.hero-background-blog h2 {
    font-weight: 800;
    color: #fff;
    font-size: 61px !important;
}
}
.btn-lg.btn-danger {
    background-color: #d03135;
}
</style>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/slick.min.js"></script>
<script type="text/javascript">
	    jQuery(document).ready(function(){
		    jQuery('.t-testimonial').slick({
		      dots: true,
		      arrows: false,
		      infinite: true,
		      speed: 300,
		      autoplaySpeed: 8000,
		      autoplay: true,
		      slidesToShow: 1,
		      adaptiveHeight: true,
		      draggable: false
		    });
		});
	</script>