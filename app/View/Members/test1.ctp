<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="form-group">
                    <input class="form-control" type="text" name="" placeholder="Project name" />
                </div>
                <div class="form-group">
                    <textarea class="form-control" placeholder="Invoice description" rows="7"></textarea>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type="name" name="" placeholder="Client name" />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type="email" name="" placeholder="Client email" />
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" type="text" name="" placeholder="From" />
                        </div>
                    </div>
                </div>
                <div class="text-right button-set right-btnset mt-30">
                    <a href="javascript:void(0);" class="reset" style="">Preview</a>
                    <input class="btn btn-primary" value="Save" type="submit" />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="invoice-box">
                    <h5>New invoice</h5>
                    <ul>
                        <li>
                            <label>Price</label>
                            <span><input type="text" value="$100.00" /></span>
                        </li>
                        <li>
                            <label>Discount</label>
                            <span><input type="text" value="$0.0" /></span>
                        </li>
                        <li>
                            <label>Tax</label>
                            <span><input type="text" value="$0.0" /></span>
                        </li>
                        <li>
                            <label>Total</label>
                            <span class="total">$100</span>
                        </li>
                    </ul>
                </div>
                <div class="invoice-box">
                    <h5>Earnings Calculator</h5>
                    <ul>
                        <li>
                            <label>My Share</label>
                            <span>$85.00</span>
                        </li>
                        <li>
                            <label>GUILD fee</label>
                            <span>$15.0</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- js link -->
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery_validate_min_js.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jssor.slider-22.0.15.mini.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/script12.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/slick.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap-notify.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/quill.min.js"></script>
<script type="text/javascript"></script>