<style>
	.table>thead>tr>th{
		border-bottom: none;
	}
	.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
		border-top: none;
		padding: 16px 25px;
	}
	@media screen and (max-width: 991px){
		.invoice-title span{
			margin-right: 70px !important;
		    font-size: 46px !important;
		    padding: 0 20px !important;
		    line-height: 35px !important;
		}
		.invoice-location p{
			font-size: 15px !important;
		}
		.invoice-logo{
			width: 240px !important;
		}
		.invoice-title{
			margin-top: 25px !important;
		}
		.invoice-head{
			margin: 0 0 30px !important;
		}
		.invoice-content h4{
			font-size: 24px !important;
		}
		.invoice-to strong, .invoice-info li{
			font-size: 16px !important;
		}
		.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
			padding: 14px 22px;
			font-size: 15px;
		}
		.table td{
			line-height: 24px !important;
		}
		.thanks-bar{
			padding: 20px 0 0 !important;
			margin: 30px 0 0 !important;
		}
		.thanks-bar .thanks{
			font-size: 30px !important;
		}
	}
	@media screen and (max-width: 600px){
		.invoice-table{
			margin: 30px 0 20px !important;
		}
		.invoice-total-box, .invoice-conditions{
			width: 100% !important;
		}
		.invoice-total-box span{
			min-width: 80px;
			float: right;
		}
		.invoice-table{
			overflow: scroll;
		}
		.invoice-table table{
			width: 570px;
		}
	}
	@media screen and (max-width: 480px){
		.invoice-logo {
		    width: 150px !important;
		}
		.invoice-location{
			float: left !important;
			text-align: left !important;
			margin-top: 10px !important;
			width: 100%;
		}
		.invoice-title span {
		    margin-right: 50px !important;
		    font-size: 32px !important;
		    padding: 0 12px !important;
		    line-height: 23px !important;
		}
		.invoice-content h4 {
		    font-size: 20px !important;
		}
		.invoice-title {
		    margin-top: 20px !important;
		}
		.invoice-head {
		    margin: 0 0 20px !important;
		}
		.invoice-info{
			width: 100% !important;
			float: left !important;
			margin: 10px 0 0;
		}
		.invoice-to{
			width: 100% !important;
		}
		.invoice-table {
		    margin: 20px 0 20px !important;
		}
		.invoice-total-box label{
			margin: 0 !important;
		}
		.thanks-bar {
		    padding: 15px 0 0 !important;
		    margin: 15px 0 0 !important;
		}
		.thanks-bar .thanks {
		    font-size: 24px !important;
		}
	}
</style>

<div class="page-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="invoice-head" style="width: 100%;display: inline-block;vertical-align: top;margin: 0 0 50px;">
                    <a href="javascript:;" class="invoice-logo" style="display: inline-block;vertical-align: top;">
                        <img style="display: inline-block;vertical-align: top;" src="/yogesh_new1/images/invoice-logo.jpg" alt="" />
                    </a>
                    <div class="invoice-location" style="float: right;text-align: right;">
                        <p style="font-size: 18px;font-family:'UbuntuM';margin: 0;">Manoa Innovation Center <br />2800 Innovation Dr. Suite 100 <br />Honolulu, HI 96822 <br />Toll Free: (866) 511-1898</p>
                    </div>
                    <div class="invoice-title" style="width: 100%;display: inline-block;vertical-align: top;background: #D03336;margin-top: 30px;"><span style="margin-right: 105px;float: right;background: #fff;color: #333333;font-size: 72px;text-transform: uppercase;font-family:'UbuntuM';padding: 0 30px;line-height: 55px;">Invoice</span></div>
                </div>
                <div class="invoice-content" style="width: 100%; display: inline-block; vertical-align: top;">
                    <h4 style="font-size: 30px;margin: 0 0 10px;">Invoice to:</h4>
                    <div class="invoice-to" style="float: left;">
                        <p style="font-size: 14px;font-weight: 500;margin: 0;color: #666666;line-height: 23px;"><strong style="font-family: UbuntuM;font-size: 18px;font-weight: 500;color: #333;">Ankit Jaiswal</strong> <br />24 Dummy Street Area,<br />Location, Lorem Ipsun<br />570xx59x</p>
                    </div>
                    <div class="invoice-info" style="float: right;">
                        <ul style="list-style: none; margin: 0; padding: 0;">
                            <li style="font-size: 18px;margin: 0 0 5px;">
                                <label style="font-family:'UbuntuM';margin: 0 15px 0 0;">Invoice No:</label>
                                <span style="float: right;color: #666666;font-weight: 600;">IN0000-00</span>
                            </li>
                            <li style="font-size: 18px;">
                                <label style="font-family: UbuntuM;margin: 0 15px 0 0;">Invoice Date:</label>
                                <span style="float: right;color: #666666;font-weight: 600;">Mar 06, 2018</span>
                            </li>
                        </ul>
                    </div>
                    <div class="invoice-table" style="margin: 45px 0 30px;display: inline-block;width: 100%;">
                    	<table class="table table-striped" style="margin: 0;">
                    	    <thead>
                    	      <tr style="color: #CCCCCC;font-size: 18px;text-transform: capitalize;background: #333333">
                    	        <th style="border-right: 1px solid #CCCCCC;">SL.</th>
                    	        <th style="border-right: 1px solid #CCCCCC;">Invoice Description</th>
                    	        <th>Price</th>
                    	      </tr>
                    	    </thead>
                    	    <tbody style="color: #333333;font-size: 16px;">
                    	      <tr style="border-bottom: 1px solid #CCCCCC">
                    	        <td style="font-weight: 600;border-right: 1px solid #CCCCCC;">1</td>
                    	        <td style="line-height: 27px;border-right: 1px solid #CCCCCC;">Consulting Services Consulting Services Consulting Services Consulting Services Consulting Services Consulting Services </td>
                    	        <td style="font-weight: 600;">$300.00</td>
                    	      </tr>
                    	    </tbody>
                    	  </table>
                    </div>
                    <div class="invoice-total-section">
                    	<div class="invoice-total-box" style="float: right;width: auto;">
                    		<ul>
                    			<li style="margin: 0 0 10px;font-size: 16px;padding: 0 20px;">
                    				<label style="font-family: UbuntuM;margin: 0 65px 0 0;min-width: 110px;text-align: right;">Sub-total</label>
                    				<span style="color: #666666;font-weight: 600;">$550</span>
                    			</li>
                    			<li style="margin: 0 0 10px;font-size: 16px;padding: 0 20px;">
                    				<label style="font-family: UbuntuM;margin: 0 65px 0 0;min-width: 110px;text-align: right;">Discount</label>
                    				<span style="color: #666666;font-weight: 600;">$100</span>
                    			</li>
                    			<li style="margin: 0 0 10px;font-size: 16px;padding: 0 20px;">
                    				<label style="font-family: UbuntuM;margin: 0 65px 0 0;min-width: 110px;text-align: right;">Tax</label>
                    				<span style="color: #666666;font-weight: 600;">$50</span>
                    			</li>
                    			<li class="" style="color: #fff;background: #D03135;margin: 0;padding: 15px 20px;font-size: 16px;">
                    				<label style="font-family: UbuntuM;margin: 0 65px 0 0;min-width: 110px;text-align: right;">Total Payable</label>
                    				<span style="font-weight: 600;">$50</span>
                    			</li>
                    		</ul>
                    	</div>                    	
                    </div>
                    
                    <div class="thanks-bar" style="padding: 25px 0 0;margin: 40px 0 0;border-top: 1px solid #ccc;display: inline-block;width: 100%;">
                    	<p style="color: #666;font-size: 12px;line-height: 18px;margin: 0 0 10px; text-align: center;"><strong style="color: #333333;font-size: 16px;font-family: UbuntuM;font-weight: 500;display: inline-block;width: 100%;margin: 0 0 5px 0;">Terms & Conditions</strong><br>Please read our <a style="color: #D03135" href="#">Terms of Service.</a> <br>Contact <a style="color: #D03135" href="mailto:help@guild.im">help@guild.im</a> or call 1 (808) 729-5850 <br>for any questions or clarifications.</p>
                    	<p class="thanks" style="color: #333333;font-size: 36px;font-weight: 600;margin: 0;width: 100%;display: inline-block;text-align: center;">Thank You!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- js link -->
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery_validate_min_js.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jssor.slider-22.0.15.mini.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/script12.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/slick.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap-notify.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/quill.min.js"></script>
<script type="text/javascript"></script>