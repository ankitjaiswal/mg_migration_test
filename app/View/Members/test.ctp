

<div class="row">
<div class="row">
<div class="container">
   <!--<div class="col-md-12">-->
      <div class="col-md-8 pull-left pad40" id="projectnew">
         <div class="container-fluid" style="margin-top: 112px;padding:0">
            <div class="col-md-12 progresbar" style="padding: 0;">
               <ul style="-webkit-justify-content: space-between;display: flex;-moz-justify-content: space-between;">
                  <li class="active">
                     <span>1</span>
                     <div class="title">
                        <p>Project created</p>
                     </div>
                  </li>
                  <li class="active">
                     <span>2</span>
                     <div class="title">
                        <p>Project approved</p>
                     </div>
                  </li>
                  <li class="active">
                     <span>3</span>
                     <div class="title">
                        <p>Proposal accepted</p>
                     </div>
                  </li>
                  <li>
                     <span>4</span>
                     <div class="title">
                        <p>Project started</p>
                     </div>
                  </li>
                  <li>
                     <span>5</span>
                     <div class="title">
                        <p>Project completed</p>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
         <div class="project-overview" style="margin-bottom: 50px;">
            <div class="overview-desc">
               <h5>Project title</h5>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.</p>
            </div>
            <div class="overview-desc">
               <h5>Project details</h5>
               <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
            </div>
            <div class="overview-desc">
               <h5>Start</h5>
               <p>Within 1 month</p>
            </div>
            <div class="overview-desc">
               <h5>Budget</h5>
               <p>More than $50K</p>
            </div>
            <div class="overview-desc">
               <h5>Desired qualification</h5>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
            </div>
            <div class="overview-desc">
               <h5>industry</h5>
               <p>Lorem ipsum dolor sit amet </p>
            </div>
            <div class="overview-desc">
               <h5>Business need</h5>
               <p>Lorem ipsum dolor sit amet </p>
            </div>
            <div class="overview-desc">
               <h5>Solution type</h5>
               <p>Lorem ipsum dolor sit amet </p>
            </div>
            <div class="overview-desc">
               <h5>Location prefrence</h5>
               <p>Lorem ipsum dolor sit amet </p>
            </div>
            <div class="overview-desc">
               <h5>Estimated duration</h5>
               <p>Lorem ipsum dolor sit amet </p>
            </div>
            <!--<button type="button" class="btn btn-danger" style="margin-top:30px;margin-left:20px">Lorem Ipsum</button>-->
         </div>
         <div class="project-overview" style="margin-bottom: 10px;">
            <div class="overview-desc">
               <h5>Consultants</h5>
               <p><span style="float:left;"><img style="width:70px !important" src="<?php echo(SITE_URL)?>sanchit/images/2019-06-23.png"><span style="color:#CF3135">Robert Davis</span></span><span><span style="float:right"><button type="button" class="btn btn-danger" style="margin-top:30px;margin-left:20px">Lorem Ipsum</button></span></p>
            </div>
         </div>
		 <!--Show interest btn proposal for consultant-->
		 <div  style="margin-bottom: 10px;text-align:right">
		 <a  style="font-size: 18px;color: #CF3135;cursor:pointer;">Ignore</a>
                     <button type="button" class="btn btn-danger">Show Interest in this project</button>
		 </div>
      </div>
   <!--</div>-->
   <div class="col-md-4 col-sm-12 col-xs-12 pull-right clearboth create-right">
      <div class="right-apply">
         <div class="project-overview">
            <div class="overview-desc created-by  part-2">
               <h5>Client Partner</h5>
               <div class="created-person-img">
                  <img src="https://test.guild.im/press_material/Iqbal-Ashraf-GUILD-Client-Partner.jpg">
               </div>
               <br>
               <h5 style="color:#CF3135;border:none;">Iqbal Ashraf</h5>
               <h5 style="border:none;"><i class="fa fa-phone" aria-hidden="true"></i>
                  <a href="tel:1-808-729-5850">1-808-729-5850</a>
               </h5>
               <h5 style="border:none;"><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:iqbal@guild.im">iqbal@guild.im</a></h5>
            </div>
         </div>
         <div class="project-overview" style="margin-top:0">
            <div class="overview-desc created-by  part-3">
               <h5 >Consultant</h5>
               <ul>
			   <!--Consultant who has submitted the proposal-->
                  <li>
                     <div class="created-person-img last1" style="width: 70px !important;">
                        <img src="<?php echo(SITE_URL)?>sanchit/images/2019-06-23.png">
                     </div>
                     <h5 style="color:#CF3135;border:none;">
                        <p style="color:#CF3135;display: inline-block;margin-bottom: 0;" class="davis1">Robert Davis</p>
                        <img src="<?php echo(SITE_URL)?>sanchit/images/View Proposal icon.png" style="
                           width: 13%;padding-left: 15px;">
                     </h5>
					 <!--Ignore accept btn-->
                     <a  style="font-size: 18px;color: #CF3135;cursor:pointer;margin-right: 7%;">Ignore</a>
                     <button type="button" class="btn btn-danger">Accept</button>
                  </li>
				  <!--Consultant who has not submitted the proposal-->
                  <li>
                     <div class="created-person-img last1" style="width: 70px !important;">
                        <img src="<?php echo(SITE_URL)?>sanchit/images/2019-06-23 (1).png">
                     </div>
                     <h5 style="color:#CF3135;border:none;">
                        <p style="color:#CF3135;display: inline-block;margin-bottom: 0;" class="davis1">Mike Dorman</p>
                     </h5>
					 <!--Ignore accept btn-->
                     <a  style="font-size: 18px;color: #CF3135;cursor:pointer;margin-right: 7%;">Ignore</a>
                     <button type="button" class="btn btn-danger">Accept</button>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- js link -->
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
<style>
   body {
   font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
   font-size: 14px;
   line-height: 1.42857143;
   color: #333;
   background-color: #efefef;
   }
   .panel-title span {
   height: 40px;
   width: 40px;
   display: block;
   margin: 0 auto;
   border-radius: 50%;
   line-height: 40px;
   font-size: 14px;
   color: #000;
   text-align: center;
   z-index: 2;
   position: relative;
   background-color: #ddd;
   }
   .panel-title {
   width: 20%;
   float: left;
   }
   .panel-title p {
   text-align: center;
   margin-top: 10px;
   font-size: 13px;
   }
   .panel-title span::after {
   position: absolute;
   content: '';
   left: 40px;
   right: 30px;
   top: 20px;
   height: 2px;
   background: #DDDDDD;
   z-index: 0;
   width: 124px;
   }
   .panel-title-1 span::after {
   content: '';
   background: transparent!important;
   }
   .project span {
   background: #888;
   }
   .project1 span {
   background: #888;
   }
   .project span::after {
   background: #888;
   }
   /*.title h5 {
   border-bottom: 1px solid #ddd;
   padding-bottom: 10px;
   font-size: 16px;
   color: #000;
   }*/
   #projectnew .overview-desc, #projectnew-view .overview-desc {
   margin-bottom: 0;
   padding: 20px;
   padding-bottom: 0;
   }
   .overview-desc h5 {
   font-family: opensansregular,sans-serif;
   font-size: 16px;
   color: #000;
   border-bottom: 1px solid #ddd;
   padding-bottom: 10px;
   font-weight: bold;
   }
   .overview-desc p {
   font-family: opensansregular,sans-serif;
   font-size: 16px;
   color: #333;
   line-height: 24px;
   margin-bottom: 20px;
   }
   #projectnew .project-overview {
   margin-top: 30px;
   padding-bottom: 20px;
   background-color: #fff;
   width: 100%;
   display: inline-block;
   }
   @media only screen and (min-width: 601px) {
   .project-overview {
   margin-top: 217px;
   }
   }
   @media only screen and (max-width:600px)
   {
   .panel-title span::after {
   position: absolute;
   content: '';
   left: 40px;
   right: 30px;
   top: 20px;
   height: 2px;
   background: #DDDDDD;
   z-index: 0;
   width: 56px;
   }
   }
   .part-2 a {
   color: #d03135!important;
   }
   .part-2 h5 i {
   padding-right: 15px;
   }
   .davis {
   float: left;
   margin-bottom: 0;
   color: #d03135!important;
   }
   button.btn.btn-link:hover {
   text-decoration: none;
   border: 1px solid;
   border-radius: 4px;
   padding: 2px 10px;
   }
   .created-person-img {
   width: 120px !important;
   }
   .btn-danger {
   color: #fff;
   background-color: #d03135;
   border-color: #d03135;
   font-size: 17px;
   padding: 2px 15px;
   }
   button.btn.btn-link {
   padding-left: 0
   }
   .last1
   {
   margin-top: 30px;
   }
   .davis1 {
   font-size: 16px;
   font-weight: bold;
   }
   /*.part-3 {
   padding-top: 35px;
   }*/
   .panel-title .stepcenter
   {
   margin:0 auto;
   }
   .panel-title .step
   {
   margin:0 !important;
   }
</style>

