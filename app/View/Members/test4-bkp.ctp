<div class="member-content">

	<!-- Member Advantage Begin -->
	<div class="member-advantage">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="member-detail">
						<h2>Member Advantage</h2>
						<span>GUILD connects 8,000+ member consultants to business opportunities across the nation.</span>
						<div class="button-set">
							<a href="javascript:;" class="btn btn-primary">Register Now</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Member Advantage Close -->

	<!-- Membership Benefits Begin -->
	<div class="md-p-100 membership">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="member-detail title">
						<h2>Membership Benefits</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
					<div class="mb-detail-left">
						<p>GUILD is a membership platform for seasoned industry experts, coaches and consultants, connecting them to clients across industries, every day.</p>
						<p>Clients come to us with complex problems and project needs that require specialized expertise. GUILD connects these clients to relevant members, and works collaboratively to craft solutions that is both effective and cost-effective.</p>
						<p>In addition, GUILD directly helps our members market their expertise online, combining their years of domain experience with our experience in digital marketing of consulting services.</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="md-detail-right">
						<ul>
							<li><i class="fa fa-check-square-o" aria-hidden="true"></i>GUILD Profile</li>
							<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Consultant Promotional Tools</li>
							<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Access to GUILD Partner Products</li>
							<li><a href="javascript:;">View Eligibility</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="button-set">
					<a href="javascript:;" class="btn btn-primary">Register Now</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Membership Benefits Close -->

	<!-- Testmonial Begin -->
	<div class="md-p-100 m-testimonial">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="member-detail mb-title">
						<h2>Testimonials</h2>
						<p>What our member consultants are saying</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12">

					<div class="t-testimonial">
						<div class="item">
					        <div class="t-img">
					        	<img src="<?php echo(SITE_URL)?>yogesh_new1/images/Judy-bardwick.jpg" alt="Judy bardwick">
					        </div>
					        <div class="t-detail">
					        	<p>I am delighted and amazed by the work GUILD has done for me. It is personal, accurate, honest and original. The website they created is client-focused, eye-catching, and simultaneously captures the essence of what I deliver to Clients. And, the speed of the team's responsiveness and their attention to every detail is the best I have ever experienced.</p>
					        	<h4>Judy Bardwick</h4>
					        	<span>Leading Organizational Psychologist</span>
					        </div>
					    </div>
						<div class="item">
					        <div class="t-img">
					        	<img src="<?php echo(SITE_URL)?>/imgs/Mark-Palmer-Guild1.jpg" alt="Mark Palmer">
					        </div>
					        <div class="t-detail">
					        	<p>By listing only the best consultants GUILD is well on its way of marketing to businesses from a place of trust.</p>
					        	<h4>Mark Palmer</h4>
					        	<span>Founder/ Principal, Focus LLC</span>
					        </div>
					    </div>
						<div class="item">
					        <div class="t-img">
					        	<img src="<?php echo(SITE_URL)?>/imgs/Chip-Evans-Guild.jpg" alt="Chip Evans">
					        </div>
					        <div class="t-detail">
					        	<p>GUILD was extraordinary in how they represented us to the client looking for consulting guidance, and in organizing and following up to ensure the consulting times and description of work were clear.</br>I'd highly recommend working with GUILD and am very pleased with our relationship.</p>
					        	<h4>Chip Evans, Ph.D</h4>
					        	<span>President & Founder at The Evans Group LLC</span>
					        </div>
					    </div>
						<div class="item">
					        <div class="t-img">
					        	<img src="<?php echo(SITE_URL)?>/imgs/Roza-Rojdev-Guild.png" alt="Roza Rojdev">
					        </div>
					        <div class="t-detail">
					        	<p>As a partner to Xecutive Metrix, the GUILD team is offering services that help our firm increase our level of exposure to new markets, connect us with other value added consultants in the network and provide us with peace of mind that our marketing efforts are running smoothly. Now we can focus on what we do best&mdash;Executive Leadership Development.</p>
					        	<h4>Roza S. Rojdev, PsyD</h4>
					        	<span>Managing Partner at Xecutive Metrix</span>
					        </div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Testmonial Close -->

	<!-- Features begin -->
	<div class="md-p-100 member-feature">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="member-detail mb-title">
						<h2>Features</h2>
						<p>Marketing and productivity tools for experienced consultants.</p>
					</div>
				</div>
			</div>
			<div class="row flex">
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="feature-img ninety-img">
							<img src="https://test.guild.im/img/members/profile_images/380/John-Baldoni-Guild.jpg" alt="john-baldoni">
						</div>
						<div class="feature-detail">
							<h4>John Baldoni</h4>
							<a href="javascript:;">john@companyemail.com</a>
							<p>Leadership Development<br>(xxx) xxx-xxxx | Ann Arbor, MI<br><a href="javascript:;">http://www.johnbaldoni.com/</a></p>
							<ul>
								<li><a href="javascript:;"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-linked.svg" alt="linked"></a></li>
								<li><a href="javascript:;"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mg-guild.svg" alt="guild"></a></li>
								<li><a href="javascript:;"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-twitter.svg" alt="twitter"></a></li>
								<li><a href="javascript:;"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-youtube.svg" alt="youtube"></a></li>
							</ul>
						</div>
						<div class="f-first f-center">
							<p>Your professional email signature. Just copy and paste to your email account. </p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="mb-profile">
							<div class="feature-img ninety-img">
								<img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-mark-parler.png" alt="mb-mark-parler">
							</div>
							<div class="feature-detail">
								<h4><a href="javascript:;">Mark Palmer</a></h4>
								<p>Sacramento, CA</p>
								<span>
									<div class="mb-gm-logo">
										<img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-gluid-member.svg" alt="mb-gluid-member">
									</div>
									<div class="button-set">
										<a href="javascript:;" class="btn btn-primary">View Profile</a>
									</div>
								</span>
							</div>
						</div>
						<div class="f-second f-center">
							<p>Post a badge on your website or blog for branding and prospecting. <a href="javascript:;">Learn more</a></p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/f-subcontract-1.png" alt="f-subcontract-1">
							<span>SubContract</span>
						</div>
						<div class="f-third f-center">
							<p>Post a badge on your website or blog for branding and prospecting. <a href="javascript:;">Learn more</a></p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/f-invite-1.png" alt="f-invite-1">
							<span>Invite Colleagues</span>
						</div>
						<div class="f-third f-center">
							<p>Invite other experienced consultants to join as fellow members. <a href="javascript:;">View Eligibility</a></p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/f-case-1.png" alt="f-case-1">
							<span>Case Study</span>
						</div>
						<div class="f-third f-center">
							<p>Share your past successes and position yourself as an authority. <a href="javascript:;">View Case tudies</a> or <a href="javascript:;">Submit Your Case Study</a></p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/f-file-manager-1.png" alt="f-file-manager-1">
							<span>Project Proposal</span>
						</div>
						<div class="f-third f-center">
							<p>Help our team market your expertise to your target customers. <a href="javascript:;">View Project Proposals</a> or <a href="javascript:;">Submit Your Project Proposal</a></p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/f-public-1.png" alt="f-public-1">
							<span>Public Relations</span>
						</div>
						<div class="f-third f-center">
							<p>Get mentioned in leading media outlets by sending a press release or by replying to queries by top reporters. <a href="javascript:;">Upgrade now</a></p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/f-customshare-2.png" alt="f-customshare-2">
							<span>Custom Share</span>
						</div>
						<div class="f-third f-center">
							<p>CustomShare — A simple tool to drive traffic to your profile with every link you share. <a href="javascript:;">Learn More.</a></p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/flevy-1.png" alt="flevy-1">
						</div>
						<div class="f-third f-center">
							<p>Get access to business frameworks, presentation templates, fina ncial models and other tools used by top consultancies. <a href="javascript:;">Learn More.</a></p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/insightbee.png" alt="insightbee">
						</div>
						<div class="f-third f-center">
							<p>Walk into a client meeting with superior insights on their business and market. Access high quality, personalized market research. <a href="javascript:;">Learn More.</a></p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/9lenses.png" alt="9lenses">
						</div>
						<div class="f-third f-center">
							<p>Assess business needs and manage data on a single digital platform to better advise clients. <a href="javascript:;">Learn More.</a></p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- Features Close -->

	<!-- Premium Membership Plans Begin -->
	<div class="md-p-100 premium-mp">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="member-detail mb-title">
						<h2>Premium Membership Plans</h2>
						<p>You have so much to offer to your clients. Do what you do best, and leave the rest to your GUILD team!</p>
					</div>
				</div>
			</div>
			<div class="row flex">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-100">
					<div class="premium-plan">
						<div class="premium-logo">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/p-gluid-website-logo1.jpg" alt="p-gluid-website-logo1">
						</div>
						<div class="premium-price">
							<h2>$199</h2>
							<span>Per Month</span>
						</div>
						<div class="premium-detail">
							<p>High performance websites for experienced consultants.</p>
							<ul>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>GUILD BASIC + </li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Website development and hosting</li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Fully managed services</li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Monthly traffic reviews and content changes </li>
							</ul>
						</div>
						<div class="button-set">
							<a href="javascript:;" class="btn btn-primary">Learn more</a>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-100">
					<div class="premium-plan">
						<div class="premium-logo master">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/p-gluid-master-logo2.jpg" alt="p-gluid-master-logo2">
						</div>
						<div class="premium-price">
							<h2>$999</h2>
							<span>Per Month</span>
						</div>
						<div class="premium-detail">
							<p>Marketing support to help you stand out in the crowd.</p>
							<ul>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>GUILD BASIC + </li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Social media management</li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Weekly LinkedIn/ blog posts</li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Monthly newsletters</li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Quarterly whitepapers</li>
							</ul>
						</div>
						<div class="button-set">
							<a href="javascript:;" class="btn btn-primary">Learn more</a>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="premium-plan">
						<div class="premium-logo premier">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/p-gluid-premier-logo1.jpg" alt="p-gluid-premier-logo1">
						</div>
						<div class="premium-price">
							<h2>$2,999</h2>
							<span>Per Month</span>
						</div>
						<div class="premium-detail">
							<p>All-in-one marketing solution for unique member needs.</p>
							<ul>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Full service, customized marketing support for your firm</li>
							</ul>
						</div>
						<div class="button-set">
							<a href="javascript:;" class="btn btn-primary">Call 1-866-511-1898</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- Premium Membership Plans Close -->

	<!-- Customized Plan Begin -->
	<div class="md-p-100 customized-plan">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="member-detail mb-title">
						<h2>Customized Plan</h2>
						<p>Select the features you want to include in your custom plan and submit a quote you are willing to pay.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="plan-box">
						<div class="c-box">
							<ul>
								<li class="styled-form">
									<input id="gw" type="checkbox" name="" value="">
									<label for="gw">GUILD Website</label>
								</li>

								<li class="styled-form">
									<input id="la" type="checkbox" name="" value="">
									<label for="la">LinkedIn Articles</label>
									<span class="qtr"><input id="" type="text" name="" value="" class="form-control">/Qtr</span>
								</li>
								<li class="styled-form">
									<input id="da" type="checkbox" name="" value="">
									<label for="da">Digital advertising</label>
								</li>
								<li class="styled-form">
									<input id="spo" type="checkbox" name="" value="">
									<label for="spo">Social profile optimization</label>
								</li>
								<li class="styled-form">
									<input id="dsm" type="checkbox" name="" value="">
									<label for="dsm">Daily social media management</label>
								</li>
							</ul>
						</div>
						<div class="c-box c-right">
							<ul>
								<li class="styled-form">
									<input id="ew" type="checkbox" name="" value="">
									<label for="ew">Ebook/Whitepaper</a></label>
									<span class="qtr"><input id="" type="text" name="" value="" class="form-control">/Qtr</span>
								</li>
								<li class="styled-form">
									<input id="dia" type="checkbox" name="" value="">
									<label for="dia">Digital advertising</label>
								</li>
								<li class="styled-form">
									<input id="ws" type="checkbox" name="" value="">
									<label for="ws">Webinar support</label>
									<span class="qtr"><input id="" type="text" name="" value="" class="form-control">/Qtr</span>
								</li>
								<li class="styled-form">
									<input id="b6w" type="checkbox" name="" value="">
									<label for="b6w">Blog (600 words)</label>
									<span class="qtr"><input id="" type="text" name="" value="" class="form-control">/Qtr</span>
								</li>
							</ul>
						</div>
						<div class="plan-box-btm">
							<span>$</span>
							<input type="text" name="" value="" class="form-control"><span> /Month</span>
							<div class="button-set">
								<a href="javascript:;" class="btn btn-primary">Submit Quote</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Customized Plan Close -->
</div>
	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/script12.js"></script>

<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/slick.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
	    jQuery('.t-testimonial').slick({
	      dots: false,
	      arrows: true,
	      infinite: true,
	      speed: 300,
	      slidesToShow: 1,
	      adaptiveHeight: true,
	      draggable: false
	    });
    });
</script>
