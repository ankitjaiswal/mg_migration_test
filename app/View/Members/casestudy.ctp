<?php $completelink = SITE_URL."members/casestudy".($casestudy['casestudy_url']);?>
<?php $tinyUrl = $this->General->get_tiny_url(urlencode($completelink));?>
<?php $atMentorsGuild = "@GuildAlerts: "?>
<?php $atsybmol = "@"?>
<?php $MentorsGuild = "GUILD: "?>
<?php $compTitle = $casestudy['CaseStudy']['title'];?>
<?php $linkTitle = $casestudy['CaseStudy']['title'];?>
<?php if(isset($user['UserImage'][0]['image_name']) && $user['UserImage'][0]['image_name'] !=''){
      $image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$user['User']['id'].DS.$user['UserImage'][0]['image_name'];
 } else {
     $image_path = SITE_URL.'img/media/profile.png';
}?>
<?php 

	    if(false !== stripos($compTitle,"'")){
        	$compTitle = str_replace("'"," ",$compTitle);
}
	    if(false !== stripos($compTitle,'"')){
        	$compTitle = str_replace('"'," ",$compTitle);
}
	    if(false !== stripos($compTitle,'&')){
        	$compTitle = str_replace('&',"and",$compTitle);
}

?>
    <div id="proposal-page">
        <div class="container">
            <div class="col-md-12 pp-page">
                <div class="back-page">
                    <label>
                        <a href="<?php echo(SITE_URL);?>members/case_studies">
                            &#8592; All case studies
                        </a>
                    </label>
                </div>
                <div class="pp-heading text-center">
                    <h5>Case study: <?php echo($casestudy['CaseStudy']['title']);?></h5>
                </div>
                <div class="pp-profile">
                    <div class="pp-pic">
                        <a href="<?php echo(SITE_URL.strtolower($user['User']['url_key']));?>"><img src="<?php echo($image_path);?>" alt="<?php echo($user['UserReference']['first_name']." ".$user['UserReference']['last_name']);?>"></a>
                    </div>
                    <div class="pp-rightside">
                        <h5><a href="<?php echo(SITE_URL.strtolower($user['User']['url_key']));?>" title="<?php echo($user['UserReference']['first_name']." ".$user['UserReference']['last_name']);?>"><?php echo($user['UserReference']['first_name']." ".$user['UserReference']['last_name']);?></a>
                           <?php if($user['UserReference']['guarantee'] == 'Y') {?>

                           <span class="gg-img">
                                <a href="<?php echo(SITE_URL);?>/fronts/guarantee" target="_blank"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/gurantee.png" alt="Client Satisfaction Guarantee">
                            </span>
                         <?php }?>
                        </h5>
                        <ul class="list-unstyled">
                            <li>

                                       <?php if($city != '') {
                                         $citylink = str_replace(" ","-",$city);
                                         $keyword = "browse-all";?>
					<a href="<?php echo(SITE_URL.'browse/search_result/all-industries/all-categories/'.$keyword.'/'.$state.'/'.$citylink);?>" ><?php echo($city) ;?>, <?php echo($state) ;?></a>
				<?php } else {
					echo($this->data['UserReference']['zipcode']);
				}?>

                            </li>
                            <li class="dashTo">|</li>
                         <?php
				$disp="guild.im/".strtolower($user['User']['url_key']);
				$completelink=SITE_URL.strtolower($user['User']['url_key']);
				?>
                            <li><a href = "<?php echo($completelink); ?>" ><?php echo($disp); ?></a></li>
                        </ul>
                        <label class="ll-data"><?php echo(ucfirst($user['UserReference']['linkedin_headline']));?></label>

                    </div>
                    <div class="share-section">
                        <ul class="list-unstyled hide inactive">
                                                 <?php 

                                                       $socialCount=0;
                                   
                                                       while(count($user['Social'])>$socialCount){
                                                         // echo(1);
					               if(($pos =strpos($user['Social'][$socialCount]['social_name'],'twitter.com'))!==false)
						      {
							$twitterURL = $user['Social'][$socialCount]['social_name'];
                                                        $path = explode("/", $twitterURL);
                                                        $twitter_handle = end($path); 
                                                           
						      }
					               if(($pos =strpos($user['Social'][$socialCount]['social_name'],'linkedin.com'))!==false)
						      {
							$linkedinURL = $user['Social'][$socialCount]['social_name'];                                                           
						      }
                                                      $socialCount++;
						      } 

                                                       $login = "o_4urqen5mkq";
                                                       $appkey = "R_7aa87ace460abde4cfc31fa31b2e59ef";
                                                       $completelink = SITE_URL."members/casestudy/".($casestudy['CaseStudy']['casestudy_url']);
                                                       $answerid = $casestudy['CaseStudy']['id'];
                                                       $bitlyUrl =  $this->General->get_bitly_url($completelink, $answerid,$login, $appkey);


								 $linkdinUrl="http://www.linkedin.com/shareArticle?mini=true&url=".$completelink."&title=".$MentorsGuild.$user['UserReference']['first_name']." ". $user['UserReference']['last_name']." " .$linkTitle;
								 $tweet="http://twitter.com/intent/tweet?text=".$atMentorsGuild. $user['UserReference']['first_name']." ". $user['UserReference']['last_name'] ." ".$compTitle." " .$tinyUrl;

                                                        if($twitter_handle ==''){
                                                          $tweet="http://twitter.com/intent/tweet?text=".$user['UserReference']['first_name']." ". $user['UserReference']['last_name'] ."&#58; ".$compTitle." " .$bitlyUrl;
								} else{
                                                          $tweet="http://twitter.com/intent/tweet?text=".$atsybmol.$twitter_handle." ".$compTitle." " .$bitlyUrl;
                                                        }
							?>

                            <li>
                                <a href="javascript:void(0);" onclick="javascript:window.open('<?php echo ($tweet);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" onclick="javascript:window.open('<?php echo ($linkdinUrl);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a title="Share this post/page" href="javascript:void(0);" onclick="javascript:window.open('https://www.facebook.com/sharer.php?s=100&p[url]=<?php echo $completelink; ?>&p[title]=<?php echo($atMentorsGuild.$user['UserReference']['first_name']." ".$user['UserReference']['last_name']." ".$compTitle);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" onclick="javascript:window.open('https://plus.google.com/share?url=<?php echo $completelink ;?>&title=<?php echo($atMentorsGuild. $user['UserReference']['first_name']." ".$user['UserReference']['last_name']." ".$compTitle);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
                                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                                </a>
                            </li>
                        </ul>
                        <button class="btn btn-default get-started share-datas nn-btn" id="ss-btn">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/share_dark.svg" alt="Share Icon">
						</button>
                        <button class="btn btn-default get-started nn-btn" onclick="window.open('<?php echo SITE_URL."members/casestudy_pdf_preview/".$casestudy['CaseStudy']['id']; ?>');" id="pp-btn">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/print_dark.svg" alt="Print Icon">
						</button>
                    </div>
                </div>
                <div class="pp-label">
                    <h5>Client</h5>
                    <p><?php echo(nl2br(strip_tags($casestudy['CaseStudy']['client_details'])));?></p>
                </div>

                <div class="pp-label">
                    <h5>Problem</h5>
                    <p><?php {
                                $editthis = '<div class="ql-editor" contenteditable="true"';
                                $editby= '<div class="ql-editor" contenteditable="false"';
                                $casestudysituation = str_replace($editthis, $editby, $casestudy['CaseStudy']['situation']);

                                $editthis1 = 'target="_blank" style="color: rgba(0, 0, 0, 0.8);"';
                                $editby1 = 'target="_blank" style="color: #D03135;"';
                                $casestudysituation = str_replace($editthis1, $editby1, $casestudysituation);
                                echo(nl2br(htmlspecialchars_decode($casestudysituation)));}?>
                    </p>
                </div>

                <div class="pp-label">
                    <h5>Solution</h5>
                    <p><?php {
                                $editthis = '<div class="ql-editor" contenteditable="true"';
                                $editby= '<div class="ql-editor" contenteditable="false"';
                                $casestudyactions = str_replace($editthis, $editby, $casestudy['CaseStudy']['actions']);

                                $editthis1 = 'target="_blank" style="color: rgba(0, 0, 0, 0.8);"';
                                $editby1 = 'target="_blank" style="color: #D03135;"';
                                $casestudyactions = str_replace($editthis1, $editby1, $casestudyactions);
                                echo(nl2br(htmlspecialchars_decode($casestudyactions)));}?>
                    </p>
                </div>

                <div class="pp-label">
                    <h5>Result</h5>
                    <p><?php {
                                $editthis = '<div class="ql-editor" contenteditable="true"';
                                $editby= '<div class="ql-editor" contenteditable="false"';
                                $casestudyresult = str_replace($editthis, $editby, $casestudy['CaseStudy']['result']);

                                $editthis1 = 'target="_blank" style="color: rgba(0, 0, 0, 0.8);"';
                                $editby1 = 'target="_blank" style="color: #D03135;"';
                                $casestudyresult = str_replace($editthis1, $editby1, $casestudyresult);
                                echo(nl2br(htmlspecialchars_decode($casestudyresult)));}?>
                    </p> 
                    
                    <div class="cc-data tag-lists">
                        <h4>Expertise:</h4>
                        <ul class="list-unstyled">

            	<?php foreach ($casestudy['CasestudyTopic'] as $catValue) {
                                                            $str = ($categories[$catValue['topic_id']]);
                                                            if(false !== stripos(($categories[$catValue['topic_id']]),"&"))
                                                            $str = str_replace("&","_and",($categories[$catValue['topic_id']]));
                                                            if(false !== stripos(($categories[$catValue['topic_id']]),"/"))
                                                            $str = str_replace("/","_slash",($categories[$catValue['topic_id']]));
                                                            $str = str_replace(" ","-",$str);?>

                 
                            <li>
									<a href="<?php echo(SITE_URL);?>members/case_studies/<?php echo($str);?>">
										<?php echo($categories[$catValue['topic_id']]);?>
									</a>
                            </li>
                 <?php }?>

                        </ul>
                    </div>
                </div>
      <?php if($fromproject !="true"){?> 
                    <?php if($this->Session->read('Auth.User.id') != '' && $this->Session->read('Auth.User.role_id')== "2"){?>
                    <div class="not-quite">
                       <?php if($edit == "yes"){?>
                                             <a href="javascript:delCaseStudy(<?php echo $case_study['id']?>);" style="margin:10px;">Delete</a>

                        <button class="banner-getstarted btn btn-default get-started" onclick="window.location.href='<?php echo(SITE_URL."members/edit_casestudy/".$casestudy['CaseStudy']['id']);?>'">
							Edit this case study
			 			</button>
                        <?php }else{?>

                        <button class="banner-getstarted btn btn-default get-started" onclick="window.location.href='<?php echo(SITE_URL."members/add_casestudy/");?>'">
							Submit a new case study
			<?php }?>			</button>

                    </div>

      <?php }else{?>

                    <div class="not-quite">
                        <label>Do you have a similar requirement in your organization?</label>
                        <button class="banner-getstarted btn btn-default get-started" onclick="window.location.href='<?php echo(SITE_URL."project/create/");?>'">
							Submit your business need
						</button>
                    </div>
       <?php }}?>
                </div>
            </div>
			  <div class="dd-data text-center">

				<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
                                <script type="IN/MemberProfile" data-id="<?php echo($linkedinURL);?>" data-format="inline" data-related="false"></script>
			</div>
        </div>
    </div>

	<!-- js link -->
	<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>-->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>-->


    <script>
        $('.share-datas').on("click", function() {
            if ($(this).parent().find("ul").hasClass("inactive")) {
                $(".share-section ul").removeClass("inactive").addClass("active");
                $(".share-section ul").removeClass("hide");
            } else {
                $(".share-section ul").addClass("inactive").removeClass("active");
                $(".share-section ul").addClass("hide");
            }
        });

        // share mouseout and in
        $("#ss-btn img").mouseover(function() {
            $(this).attr("src","https://test.guild.im/yogesh_new1/images/share_red.svg")
        });
         $("#ss-btn img").mouseout(function() {
            $(this).attr("src","https://test.guild.im/yogesh_new1/images/share_dark.svg")
        });

         // print mouseout and in
        $("#pp-btn img").mouseover(function() {
            $(this).attr("src","https://test.guild.im/yogesh_new1/images/print_red.svg")
        });
         $("#pp-btn img").mouseout(function() {
            $(this).attr("src","https://test.guild.im/yogesh_new1/images/print_dark.svg")
        });
    </script>
<script>
function delCaseStudy(id)
{if(confirm('Are you sure you want to delete this Case Study?')==true)
{window.location.href=SITE_URL+'members/delete_casestudy/'+id;}}
</script>
<style>
.ql-editor {
padding:0px;
}
a {
    color: #d03135;
    
}
</style>