<!-- Media Opportunities -->
<div class="media-opportunities">
    <div class="media-hero">
        <div class="container">
            <h2>PR & Media opportunities for members</h2>
            <p>Want to send a Press release? Send an email to <a href="mailto:pr@guild.im">pr@guild.im</a></p>
            <a href="http://www.expertclick.com/NRWire/Releasedetails.aspx?id=67833" target="_blank" class="btn btn-primary">Press release sample</a>
        </div>
    </div>
    <div class="container">
        <div class="media-opportunities-head">
            <p class="head-left">Media opportunities (<a href="http://www.fastcompany.com/3044055/what-to-do-when-youre-passed-over-for-a-promotion" target="_blank">Past success</a>)</p>
        </div>
        
        <div class="media-opportunities-box">
            <?php
            $currentTime = strtotime("now");
            $count =0;
            foreach($mediaupdates as $value){
            if(strtotime($value['MediaRecords']['deadline']) >= $currentTime){
            $count++;
            $total = $count;
            }
            }
                foreach($mediaupdates as $value)
                                    if(strtotime($value['MediaRecords']['deadline']) >= $currentTime){
            ?>
            <!-- 1 -->
            <div class="media-opportunities-info">
                <label>Deadline</label>
                <div class="media-opportunities-info-text">
                    <p><?php $date = explode(' ',$value['MediaRecords']['deadline']);
                    echo($date[0]);?></p>
                </div>
            </div>
            <!-- 2 -->
            <div class="media-opportunities-info">
                <label>Media outlet</label>
                <div class="media-opportunities-info-text">
                    <p><?php echo($value['MediaRecords']['media_outlet']);?></p>
                </div>
            </div>
            <!-- 3 -->
            <div class="media-opportunities-info">
                <label>Topics</label>
                <div class="media-opportunities-info-text">
                    <p>           <?php if($value['MediaRecords']['category1'] !='' && $value['MediaRecords']['category2'] !='' && $value['MediaRecords']['category3'] !=''){
                        echo($value['MediaRecords']['category1'].' , '.$value['MediaRecords']['category2'].' , '.$value['MediaRecords']['category3']);
                        }else if($value['MediaRecords']['category1'] !='' && $value['MediaRecords']['category2'] !='' && $value['MediaRecords']['category3'] ==''){
                        echo($value['MediaRecords']['category1'].' , '.$value['MediaRecords']['category2']);
                        }else if($value['MediaRecords']['category1'] !='' && $value['MediaRecords']['category2'] =='' && $value['MediaRecords']['category3'] ==''){
                        echo($value['MediaRecords']['category1']);
                        }else{
                        echo(" ");
                        }?>
                    </p>
                </div>
            </div>
            <!-- 4 -->
            <div class="media-opportunities-info">
                <label>Subject</label>
                <div class="media-opportunities-info-text">
                    <p><?php
                        $subject = str_replace('  ', '&nbsp;&nbsp;',$value['MediaRecords']['subject']);
                                        echo $this->General->make_links(nl2br($subject));
                        ?>
                    </p>
                </div>
            </div>
            <!-- 5 -->
            <div class="media-opportunities-info">
                <label>Reporter query</label>
                <div class="media-opportunities-info-text">
                    <p>
                        <?php
                        $reporter_query = str_replace('  ', '&nbsp;&nbsp;',$value['MediaRecords']['reporter_query']);
                                    echo $this->General->make_links(nl2br($reporter_query));
                        ?>
                    </p>
                </div>
            </div>
            <!-- 6 -->
            <div class="media-opportunities-info">
                <label>Requirement</label>
                <div class="media-opportunities-info-text">
                    <p><?php
                        $requirement = str_replace('  ', '&nbsp;&nbsp;', $value['MediaRecords']['requirement']);
                                    echo $this->General->make_links(nl2br($requirement));
                        ?>
                    </p>
                </div>
            </div>
            <!-- Respond btn -->
            <div class="media-opportunities-btn">
                <?php if($this->Session->read('Auth.User.id') !=''){?>
                <button class="btn-open response">Respond</button>
                <?php }else{?>
                <button  href="javascript:void(0);" data-toggle="modal" data-target="#signin-Modal">Respond</button>
                <?php }?>
                <div class="txt-form" style="display: none;">
                    <div class="media-opportunities-form">
                        <form  id ="Addform<?php echo($value['MediaRecords']['id']);?>" name="Submitform"  method="post" action="<?php echo SITE_URL;?>fronts/media_response" >
                            <div class="media-opportunities-form-box">
                                <input type="hidden" id="queryId"  name="data[QueryId]" value="<?php echo($value['MediaRecords']['id']);?>">
                                <textarea class="form-control" placeholder="Your response" id ="questionContextBox<?php echo($value['MediaRecords']['id']);?>" name="data[EmailBody]"></textarea>
                            </div>
                            <div class="media-opportunities-btn" id="apply<?php echo($value['MediaRecords']['id']);?>">
                                <button class="close-textarea btn btn-primary" onClick="return on_can(<?php echo($value['MediaRecords']['id']);?>);">Submit</button>
                                <button class="cancel-btn close-textarea">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- border -->
            <div class="media-opportunities-bdr">
                <hr>
            </div>
            <!-- Form Group -->
            <?php }?>
        </div>
    </div>
</div>

<div class="respond-popup">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    <div class="popup-inner">
        <p>Regular members can reply to 1 media query per day. To respond to unlimited media queries, please <a href="<?php echo(SITE_URL)?>members/master">upgrade to Premium Membership</a></p>
    </div>
</div>

<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
<script type="text/javascript">
    jQuery(".btn-open").on('click', function(event){
        event.stopPropagation();
        event.stopImmediatePropagation();
        jQuery(".txt-form").css('display','none');
        jQuery(".response").css('display','block    ');
        jQuery(this).css('display','none');
        jQuery(this).parent(".media-opportunities-btn").find(".txt-form").css('display','block');
    });
    jQuery(document).ready(function() {
        jQuery(document).on('click', ".close-textarea", function(event){
            event.preventDefault();
            jQuery(".txt-form").css('display','none');
            jQuery(".response").css('display','block');
        });
    });
</script>

<script type="text/javascript">
    if(window.location.href === "https://test.guild.im/members/pr/OpenSubscribePopup%3A1") {
        jQuery(".respond-popup").show();
            jQuery(".respond-popup .close").click(function() {
                jQuery(".respond-popup").hide();
            });
    }

</script>

<script type="text/javascript">
    function on_can(Id) {
        var flag = 0;
        if(jQuery.trim(jQuery('#questionContextBox'+Id).val()) == '') {
                jQuery('#questionContextBox'+Id).css('border-color','#F00');
                jQuery('#questionContextBox'+Id).focus();
                flag++;
        } else {
            jQuery('#questionContextBox'+Id).css('border-color','');
        }
        if(flag > 0) {
            return false;
        } else {
            document.getElementById("apply"+Id).style.display="none";
            jQuery('#Addform'+Id).submit();
            

            return true;
        }
    }
</script>