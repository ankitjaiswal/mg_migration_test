<?php
$error = '';
if(isset($_SESSION['pass_error']))
{
	$error  = $_SESSION['pass_error'];
	unset($_SESSION['pass_error']);
}?>

<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<?php echo($this->Form->create('User', array('url' => array('controller' => 'members', 'action' => 'account_setting'),'id'=>'accountSettingForm')));?>
				<?php echo($this->Form->hidden('UserReference.id',array('value'=>$userData['UserReference']['id']))); ?>

				<ul class="left-tab">
					<li data-tab="tab-account" class="active"><a href="javascript:void(0);">Account type</a></li>				    
				    <li data-tab="tab-notification"><a href="javascript:void(0);">Preferences</a></li>
                                    <li data-tab="tab-projects"><a href="javascript:void(0);">My projects</a></li>
                                    <?php if(!empty($invoicedata) && count($invoicedata)>0) {?>
                                    <li data-tab="tab-invoices"><a href="javascript:void(0);">My invoices</a></li>
                                    <?php }?>				    
				    <li data-tab="tab-setting"><a href="javascript:void(0);">Change password</a></li>
				</ul>
				<div class="left-tab-content">					

					<div class="account-info" id="tab-notification">
						<h3>Email notifications</h3>
						<ul>
						    <li class="styled-form">
					    		<?php if(isset($userData['UserReference']['email_notification']) && $userData['UserReference']['email_notification'] == 'W') {?>
									<input id="m-update" class="group1" type="checkbox" name="catCheck[]" value="email_notification" checked></input>
									<label for="m-update">Member updates</label>
								<?php } else {?>
									<input id="m-update" class="group1" type="checkbox" name="catCheck[]" value="email_notification" ></input>
									<label for="m-update">Member updates</label>
								<?php }?>
							</li>
							<li class="styled-form">
								<?php if(isset($userData['UserReference']['question_notification']) && $userData['UserReference']['question_notification'] == 'Y') {?>
									<input id="userq" class="group1" type="checkbox" name="catCheck[]" value="question_notification" checked></input>
									<label for="userq">User questions</label>
								<?php } else {?>
									<input for="userq" id="userq" class="group1" type="checkbox" name="catCheck[]" value="question_notification"></input>
									<label>User questions</label>
								<?php }?>
					    	</li>
				    	</ul>
				    	<br /><br />
				    	<h3>Accepting new clients?</h3>
                                    <?php if(isset($userData['UserReference']['accept_application']) && $userData['UserReference']['accept_application'] == 'Y') {?>
				    	<ul>
				    	    <li class="styled-form">
					    		<input id="a-yes" name="accept-client" type="radio" value="Y" checked></input>
								<label for="a-yes">Yes</label>
							</li>
				    	    <li class="styled-form">
					    		<input id="a-no" name="accept-client" type="radio" value="N"></input>
								<label for="a-no">No</label>
							</li>
				    	</ul>
                                    <?php } else {?>
				    	<ul>
				    	    <li class="styled-form">
					    		<input id="a-yes" name="accept-client" type="radio" value="Y"></input>
								<label for="a-yes">Yes</label>
							</li>
				    	    <li class="styled-form">
					    		<input id="a-no" name="accept-client" type="radio" value="N" checked></input>
								<label for="a-no">No</label>
							</li>
				    	</ul>
                                     <?php }?>
				    	<div class="button-set">
							<?php echo($this->Html->link('Cancel','javascript:void(0);',array('class'=>'reset','style'=>""))); ?>
							<input name="" type="submit" id="submitAccount" onclick="return validAccountSetting();" class="btn btn-primary" value="Save">
						</div>
					</div>
					<div class="account-info" id="tab-setting">
						<h3>Change password</h3>
						<ul>
						    <li class="full">
						    	<div class="fom-group">
						    		<label>Email</label>
                                                                <input class="form-control" id="hiddenusername" value="<?php echo($userData['User']['username']);?>" type="hidden" />
						    		<input class="form-control" name="data[User][username]" id="username" value="<?php echo($userData['User']['username']);?>" type="text" onchange="return ChangeUsername();"/>
                                                                <span id="Emailalready" class="errormsg" style="display:inline; float:left; display:none;"><?php echo $error; ?></span>
						    	</div>
					    	 </li>
						    <li class="full">
						    	<div class="fom-group">
						    		<label>Old password</label>
						    		<?php echo($this->Form->password('oldpass', array('maxlength'=>20,'div'=>false, 'label'=>false, "class" => "form-control", "placeholder" => "Old password")));?>
									<span id="OldNotMatch" class="errormsg" style="display:inline; float:left; display:none;"><?php echo $error; ?></span>
						    	</div>
					    	</li>
					    	<li class="full">
						    	<div class="fom-group">
						    		<label>New password</label>
						    		<?php echo($this->Form->password('newpass', array('maxlength'=>20,'div'=>false, 'label'=>false, "class" => "form-control", "placeholder" => "New password")));?>
						    	</div>
					    	</li>
					    	<li class="full">
						    	<div class="fom-group">
						    		<label>Confirm password</label>
						    		<?php echo($this->Form->password('confpass', array('maxlength'=>20,'div'=>false, 'label'=>false, "class" => "form-control", "placeholder" => "Confirm password"))); ?>
									<span id="commonerror" class="errormsg" style="display:inline; float:left; display:none;"></span>
						    	</div>
						    </li>
						</ul>

						<div class="button-set">
							<?php echo($this->Html->link('Cancel','javascript:void(0);',array('class'=>'reset','style'=>""))); ?>
							<input name="" type="submit" id="submitAccount" onclick="return validAccountSetting();" class="btn btn-primary" value="Save">
						</div>
					</div>

					<div class="account-info p-members-table" id="tab-invoices">
						<h3>My invoices</h3>
						<table class="table table-striped smartresponsive-table">
							<thead>
								<tr>
									<th>Invoice No</th>
									<th>Date</th>
									<th>Project title</th>
									<th>Price</th>
									<th>Status</th>
									<th>Action</th>
									
								</tr>
							</thead>
							<tbody>
								<?php if(!empty($invoicedata) && count($invoicedata)>0) {
								foreach($invoicedata as $invoice){?>
								<tr>
									<td data-th="Invoice No">
										<?php echo($this->Html->link($invoice['ProjectInvoice']['invoiceno'],array('controller'=>'project','action'=>'invoice_detail/'.$invoice['ProjectInvoice']['invoiceno']),array('class'=>'delete','title'=>'View invoice'))); ?>
									</td>
									<td data-th="Date"><?php echo $this->Time->format('d-m-Y',$invoice['ProjectInvoice']['created']); ?></td>

                                                                        <?php if($invoice['ProjectInvoice']['inv_mentor_id'] != $this->Session->read('Auth.User.id')){?>
									<td data-th="Session title"><b>Subcontract -</b> <?php echo $invoice['ProjectInvoice']['title']; ?></td>
                                                                        <?php }else{?>
                                                                        <td data-th="Session title"><?php echo $invoice['ProjectInvoice']['title']; ?></td>
                                                                        <?php }?>

									<?php 
											$total =  '$'.number_format($invoice['ProjectInvoice']['total'],2);
									?>
									<td data-th="Price"><?php echo $total; ?></td>
                                                                        <?php if($invoice['ProjectInvoice']['inv_client_id']== $this->Session->read('Auth.User.id')){?>

									<?php if($invoice['ProjectInvoice']['invoicestatus']=='PAID'){?>
									<td data-th="Status"><?php echo($invoice['ProjectInvoice']['invoicestatus']);?></td>
									<?php }else{?>
									<td data-th="Status"><?php echo($this->Html->link('Pay Now',array('controller'=>'project','action'=>'invoice_detail/'.$invoice['ProjectInvoice']['invoiceno']),array('class'=>'delete','title'=>'Pay Now'))); ?></td>
									<?php }?>
                                                                        <?php }else{?>
									<?php if($invoice['ProjectInvoice']['invoicestatus']=='PAID'){?>
									<td data-th="Status"><?php echo($invoice['ProjectInvoice']['invoicestatus']);?></td>
									<?php }else{?>
									<td data-th="Status"><?php echo($invoice['ProjectInvoice']['invoicestatus']);?></td>
									<?php }?>
                                                                        <?php }?>

 
                                                                        
									<td data-th="Print">
										
                                                                                <?php echo($this->Html->link('Print',array('controller'=>'project','action'=>'invoice_pdf/'.$invoice['ProjectInvoice']['invoiceno']),array('class'=>'delete','title'=>'Print','target'=>'_blank'))); ?>
									</td>
								</tr>
								<?php } } else{?>
								<tr><td colspan="7" style="text-align:center;">No record found</td></tr>
								<?php }?>

							</tbody>
						</table>
					</div>

					<div class="account-info p-members-table" id="tab-projects">
						<h3>My projects</h3>
						<table class="table table-striped smartresponsive-table">
							<thead>
								<tr>
									<th>Created by</th>
									<th>Date</th>
                                                                        
									<th>Project title</th>
									<th>Project status</th>
									<th>Action</th>
									<th></th>
								
								</tr>
							</thead>
							<tbody>

						<?php if(!empty($data) && count($data)>0)
						{
						$i=1;
						foreach($data as $project){
						
							$for_creator = ClassRegistry::init('User');
						$creator = $for_creator->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));
						if($creator['User']['role_id'] == 2)
						$displink1=SITE_URL.strtolower($creator['User']['url_key']);
						else
						$displink1=SITE_URL."clients/my_account/".strtolower($creator['User']['url_key']);
						?>
						<tr>
                                                        <?php if(($project['Project']['status'] == 8 || $project['Project']['status'] == 9) && $creator['User']['id'] != $this->Session->read('Auth.User.id')){?>
							<td data-th="Created by"><?php  echo($this->Html->link(ucfirst($creator['UserReference']['first_name'])." ".ucfirst($creator['UserReference']['last_name']),$displink1,array('escape'=>false,'target'=>'_blank')));?></td>
                                                        <?php }else if($creator['User']['id'] == $this->Session->read('Auth.User.id')){?>
							<td data-th="Created by"><?php  echo($this->Html->link(ucfirst($creator['UserReference']['first_name'])." ".ucfirst($creator['UserReference']['last_name']),$displink1,array('escape'=>false,'target'=>'_blank')));?></td>
                                                        <?php }else{?>
							<td data-th="Created by">GUILD Client Partner</td>
                                                        <?php }?>


							<td data-th="Date"><?php echo date("m-d-Y", strtotime($project['Project']['created']));?></td>
							<td data-th="Project title"><?php echo $project['Project']['title']; ?></td>

									<?php if($project['Project']['status'] == 1 || $project['Project']['status'] == 2 || $project['Project']['status'] == 3) {   ?>
									<td data-th="Action">Project Created</td>
									<td data-th="Action"><?php echo($this->Html->link('View',array('controller'=>'project','action'=>'index/'.$project['Project']['project_url']),array('class'=>'delete','title'=>'View project'))); ?></td>
									<?php }
									else if($project['Project']['status'] == 0) {?>
									<td data-th="Action">Project created. To be posted</td>
									<td data-th="Action"><?php echo($this->Html->link('Post now',array('controller'=>'project','action'=>'view/'.$project['Project']['id']),array('class'=>'delete','title'=>'Edit and post project')));?></td>
									<?php } else if($project['Project']['status'] == 4 || $project['Project']['status'] == 5 || $project['Project']['status'] == 6) { ?>
									<td data-th="Action">Project approved.</td>
									<td data-th="Action"><?php echo($this->Html->link('View',array('controller'=>'project','action'=>'index/'.$project['Project']['project_url']),array('class'=>'delete','title'=>'View project'))); ?></td>
									<?php } else if($project['Project']['status'] == 7) { ?>
									<td data-th="Action">Proposal accepted</td>
									<td data-th="Action"><?php echo($this->Html->link('View',array('controller'=>'project','action'=>'index/'.$project['Project']['project_url']),array('class'=>'delete','title'=>'View project'))); ?></td>
									<?php } else if($project['Project']['status'] == 8) { ?>
									<td data-th="Action">Project started</td>
									<td data-th="Action"><?php echo($this->Html->link('View',array('controller'=>'project','action'=>'index/'.$project['Project']['project_url']),array('class'=>'delete','title'=>'View project'))); ?></td>
									<?php } else if($project['Project']['status'] == 9) { ?>
									<td data-th="Action">Project completed</td>
									<td data-th="Action"><?php echo($this->Html->link('View',array('controller'=>'project','action'=>'index/'.$project['Project']['project_url']),array('class'=>'delete','title'=>'View project'))); ?></td>
									<?php }?>
                                                        <?php if($creator['User']['url_key'] != $this->Session->read('Auth.User.url_key') && $project['Project']['status'] == 8){?>

                                                        <td data-th="Send Invoice"><a href="javascript:void(0);" onclick="window.location.href='<?php echo SITE_URL."project/invoice_create/".$project['Project']['id']; ?>';" class="delete" title="Send Invoice">Send Invoice</a></td>
                                                        <?php }else{?>

                                                        <td data-th="Send Invoice"></td>

                                                        <?php }?>
						</tr>
						<?php  $i++;  }?>
						
						
						<?php
						} else{?>
						<tr style="text-align: center;">
							<td colspan="7">No record found</td>
						</tr>
						<?php } ?>




							</tbody>
						</table>
					</div>

					<div class="account-info active" id="tab-account">
						<h3>Account type</h3>
						<?php if($this->Session->read('Auth.User.mentor_type')== 'Premium Member'){?>
							<a href="<?php echo(SITE_URL)?>members/premium_membership">Premium Member</a>
						<?php } else echo $this->Session->read('Auth.User.mentor_type');?>

						<div class="card-btn">
							<form action="stripe" method="POST">
								<input type="hidden" name="FEE" value="<?php echo($centFee);?>">
			  					<script
    			                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    			                    data-key="pk_live_vg2OVxfTe0Iw5FGSMhitLWE7"
    			                    data-image="https://test.guild.im/imgs/guild_monogram.png"
    			                    data-name="GUILD"
                                    data-panel-label="Update Card Details"
                                    data-label="Update Card Details"
                                    data-allow-remember-me=false
                                    data-locale="auto">
			 		 			</script>
							</form>
						</div>

						<ul>
						    <li class="styled-form">
						    	<?php if(isset($userData['UserReference']['guarantee']) && $userData['UserReference']['guarantee'] == 'Y') {?>
									<input id="guarantee" type="checkbox" name="catCheck[]" value="guarantee" checked style="margin-right: 5px;"></input>
									<label for="guarantee">
										<?php echo($this->Html->link('Offer Client Satisfaction Guarantee <img src="/img/media/gurantee_shield28.png" alt="" />',SITE_URL.'fronts/guarantee',array('escape'=>false, 'target'=>'_blank')));?>
									</label>
								<?php } else {?>
									<input id="guarantee" type="checkbox" name="catCheck[]" value="guarantee" style="margin-right: 5px;"></input>
									<label for="guarantee">
										<?php echo($this->Html->link('Offer Client Satisfaction Guarantee <img src="/img/media/gurantee_shield28.png" alt="" />',SITE_URL.'fronts/guarantee',array('escape'=>false, 'target'=>'_blank')));?>
									</label>
								<?php }?>
						    </li>
						</ul>

						<div class="buttons">


							<a href="javascript:delAccount();" class="link"><i class="fa fa-trash-o"></i>Delete my account</a>
						</div>
					</div>


				</div>
				
	<!-- js link -->
	<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>-->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>-->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/script6.js"></script>				
				<script type="text/javascript">
					$(document).ready(function(){						
						$('ul.left-tab li').click(function(){
							var tab_id = $(this).attr('data-tab');
							$('ul.left-tab li').removeClass('active');
							$('.account-info').removeClass('active');
							$(this).addClass('active');
							$("#"+tab_id).addClass('active');
						});
					});
				</script>

				
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
function delAccount()
{if(confirm('Are you sure you want to delete your account?')==true)
{jQuery.ajax({url:SITE_URL+'/members/status_change',type:'post',dataType:'json',data:'',success:function(res){var path=SITE_URL+'members/delAccount/';TINY.box.show({url:SITE_URL+'members/delaccount',width:387,height:77,closejs:function(){window.location.href=SITE_URL;}});}});}}

function ChangeUsername(){
          var username=jQuery.trim(jQuery('#username').val());
          var hiddenusername=jQuery.trim(jQuery('#hiddenusername').val());
           if(hiddenusername != username){
			$.ajax({
			url:SITE_URL+'/members/checkUsername',
			type:'post',
                        dataType:'json',
			data:'email='+username,
			success:function(res)
			 {
				if(res.value){

                          jQuery('#username').css('border-color','#F00');jQuery('#Emailalready').html('This email address has been already registered.');
                          jQuery('#Emailalready').show();
                          //flag++;
                          }
                           else
                          {
                          jQuery('#username').css('border-color','');jQuery('#Emailalready').hide();
                          }
			 }

			});
                    }
             else{
                     jQuery('#username').css('border-color','');jQuery('#Emailalready').hide();
                    
                  }

}

function validAccountSetting()
{
var flag=0;var pass=jQuery.trim(jQuery('#UserNewpass').val());jQuery('#UserOldpass').css('border-color','');jQuery('#OldNotMatch').css('display','none');if(jQuery.trim(jQuery('#UserOldpass').val())!=''||jQuery.trim(jQuery('#UserNewpass').val())!=''||jQuery.trim(jQuery('#UserConfpass').val())!='')
{if(jQuery.trim(jQuery('#UserOldpass').val())=='')
{jQuery('#UserOldpass').css('border-color','#F00');flag++;}
else
jQuery('#UserPassword').css('border-color','');if(jQuery.trim(jQuery('#UserNewpass').val())=='')
{jQuery('#UserNewpass').css('border-color','#F00');jQuery('#commonerror').hide();flag++;}
else
{if(pass.length<6||pass.length>20)
{jQuery('#UserNewpass').css('border-color','#F00');jQuery('#commonerror').html('Password should be between 6 and 20 characters in length');jQuery('#commonerror').show();flag++;}
else
{jQuery('#UserNewpass').css('border-color','');jQuery('#commonerror').hide();}}
if(jQuery.trim(jQuery('#UserConfpass').val())=='')
{jQuery('#UserConfpass').css('border-color','#F00');flag++;}
else
jQuery('#UserConfpass').css('border-color','');if(jQuery.trim(jQuery('#UserNewpass').val())!=jQuery.trim(jQuery('#UserConfpass').val())!='')
{jQuery('#commonerror').html('New & confirm passwords do not match');jQuery('#UserNewpass').css('border-color','#F00');jQuery('#UserConfpass').css('border-color','#F00');jQuery('#commonerror').show();flag++;}
if(flag>0)
{
	return false;}
else
{
	jQuery("#accountSettingForm").submit();}}
if(flag==0)
{
	jQuery("#accountSettingForm").submit();}
return true;}
</script>