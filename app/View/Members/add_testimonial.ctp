<link href="https://cdn.quilljs.com/1.1.5/quill.snow.css" rel="stylesheet">
<?php echo($this->Html->css(array('opentip'))); ?>
<?php echo($this->Html->script(array('opentip-jquery')));?>

<div class="widthsize">
	<div class="margPAD">
		<form action="<?php echo SITE_URL ?>members/add_testimonial" method="post" id="Addform">
		<h2>Testimonial</h2>
		<div class="hr"></div>
		<div class="clear"></div>
		<div class="caseLEFT">
		

            <div class="expertise">

                <p>


					<input type="hidden" name="data[Testimonial][testimonial_detail]"  id="testimonialdetailtext"/>

                                           <div id ="testimonialdetail"  style="height:200px;width:620px; background-color:#fff; " ></div> 


            </p>
            </div>

			<div class="expertise" style="margin-top: 15px;">
                <h1>Client Name</h1>
                <p><input id="client_name" class="inpT" type="text"  placeholder="Client name" name="data[Testimonial][client_name]"/></p>
            </div>

            <div class="expertise">
                <h1>Designation</h1>
                <p><input id="client_designation" class="inpT"  type="text" placeholder="Client designation" name="data[Testimonial][client_designation]" /></p>
            </div>

            <div class="expertise">
                <h1>Organization</h1>
                <p><input id="client_organization" class="inpT"  type="text" placeholder="Client organization" name="data[Testimonial][client_organization]" /></p>
            </div>
            



            

            

			
			<div class="editSUB" style="margin-bottom:40px;margin-top:-1px;">
				<a href="<?php echo SITE_URL?>users/edit_account#testimonials">Cancel</a> 
				<input type="submit" value="Save" class="submitForm" style="width: 150px;">
			</div>	
                        				
		</div>
		
		<div class="clear"></div>
		</form>
	</div>	
</div>

<script src="https://cdn.quilljs.com/1.1.5/quill.min.js"></script>

<script>
var toolbarOptions = [
  ['bold', 'italic', 'underline','link'],        // toggled buttons


  [{ 'list': 'ordered'}, { 'list': 'bullet' }],


  ['clean']



];

</script>

<script type="text/javascript">

jQuery(document).ready(function(){



	new Opentip("#testimonialdetail", "Enter client feedback here<br/>", { style: "myErrorStyle" } );
        new Opentip("#client_name", "Enter initials to anonymize<br/>", { style: "myErrorStyle" } );
        new Opentip("#client_organization", "Enter industry and size of organization to anonymize<br/>", { style: "myErrorStyle" } );






     



      var quillsolution = new Quill('#testimonialdetail', {
      modules: {
      toolbar: toolbarOptions
      },
      placeholder: 'Share the client testimonials',
      theme: 'snow'
     });
   
    
});

jQuery(".submitForm").click(function(){

	var goingID = '';
    var flag= 0;
    var url_reg  = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;


	var testimonialdetailEntered = jQuery("#testimonialdetail").html();

        var result1 = testimonialdetailEntered.split('<div class="ql-clipboard"');
        testimonialdetailEntered = result1[0];
	testimonialdetailEntered = testimonialdetailEntered.replace(/  /gi, '&nbsp;&nbsp;');
        
        jQuery("#testimonialdetailtext").val(testimonialdetailEntered);




    
      if(jQuery.trim(jQuery("#testimonialdetail").text()) == '') {
        jQuery('#testimonialdetail').css('border-color','#F00');
        jQuery('#testimonialdetail').css('border-width','<?php echo(ERROR_BORDER);?>px');
        jQuery('#testimonialdetail').focus();
        goingID = 'testimonialdetail';
        flag++;
       }else {

            jQuery('#testimonialdetail').css('border-color','');    
            jQuery('#testimonialdetail').css('border-width','<?php echo(NORMAL_BORDER);?>px');
        	
        }
	
	if(jQuery.trim(jQuery("#client_name").val()) == '') {
        jQuery('#client_name').css('border-color','#F00');
        jQuery('#client_name').css('border-width','<?php echo(ERROR_BORDER);?>px');
        jQuery('#client_name').focus();
        flag++;
       }
        else
        {
            jQuery('#client_name').css('border-color','');     
            jQuery('#client_name').css('border-width','<?php echo(NORMAL_BORDER);?>px');
        }

	if(jQuery.trim(jQuery("#client_designation").val()) == '') {
        jQuery('#client_designation').css('border-color','#F00');
        jQuery('#client_designation').css('border-width','<?php echo(ERROR_BORDER);?>px');
        jQuery('#client_designation').focus();
        flag++;
       }
        else
        {
            jQuery('#client_designation').css('border-color','');     
            jQuery('#client_designation').css('border-width','<?php echo(NORMAL_BORDER);?>px');
        }	
   
	if(jQuery.trim(jQuery("#client_organization").val()) == '') {
        jQuery('#client_organization').css('border-color','#F00');
        jQuery('#client_organization').css('border-width','<?php echo(ERROR_BORDER);?>px');
        jQuery('#client_organization').focus();
        flag++;
       }
        else
        {
            jQuery('#client_organization').css('border-color','');     
            jQuery('#client_organization').css('border-width','<?php echo(NORMAL_BORDER);?>px');
        }



	 if(flag == 0){
		 jQuery("#Addform").submit();
     }else{
         if(goingID != ""){
             jQuery('#'+goingID).focus();
         }
         return false;
     }

	 
		 
});
</script>



