<!doctype html>
<html class="no-js full-height" lang="">
    <head>
        <?php $title = $this->Session->read('title'); 
              $description_text = $this->Session->read('description'); 
              if($description_text =='')
                 $description ="Embed takeaway into the articles you share.  Promote yourself as you share Forbes, TechCrunch, Mashable, etc.";       
              else
               $description = $description_text;

          ?>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo($title);?></title>
        <meta name="description" content="<?php echo($description);?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://test.guild.im/yogesh_new1/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <style type="text/css" media="screen">
        	@font-face{font-family:'UbuntuM';src:url('https://test.guild.im/yogesh_new1/fonts/UbuntuM.eot');src:url('https://test.guild.im/yogesh_new1/fonts/UbuntuM.eot') format('embedded-opentype'),url('https://test.guild.im/yogesh_new1/fonts/UbuntuM.woff2') format('woff2'),url('https://test.guild.im/yogesh_new1/fonts/UbuntuM.woff') format('woff'),url('https://test.guild.im/yogesh_new1/fonts/UbuntuM.ttf') format('truetype'),url('https://test.guild.im/yogesh_new1/fonts/UbuntuM.svg#UbuntuM') format('svg');}
        	@font-face{font-family:'OpenSansRegular';src:url('https://test.guild.im/yogesh_new1/fonts/OpenSansRegular.eot');src:url('https://test.guild.im/yogesh_new1/fonts/OpenSansRegular.eot') format('embedded-opentype'),url('https://test.guild.im/yogesh_new1/fonts/OpenSansRegular.woff2') format('woff2'),url('https://test.guild.im/yogesh_new1/fonts/OpenSansRegular.woff') format('woff'),url('https://test.guild.im/yogesh_new1/fonts/OpenSansRegular.ttf') format('truetype'),url('https://test.guild.im/yogesh_new1/fonts/OpenSansRegular.svg#OpenSansRegular') format('svg');}
        </style>
        <link rel="stylesheet" href="<?php echo(SITE_URL)?>yogesh_new1/css/style.css?v=1.74">
    </head>
    <body class="full-height">
	
	     <?php 
             $link = $this->Session->read('link');
              $this->Session->delete('link');

             $takeawayinput = $this->Session->read('takeawayinput');
              $this->Session->delete('takeawayinput');

             $user = $this->Session->read('user');
              $this->Session->delete('user');

             $comment = $this->Session->read('comment');
              $this->Session->delete('comment');

             $title = $this->Session->read('title');
              $this->Session->delete('title');

             $takeawayshortlink = $this->Session->read('takeawayshortlink');
             $this->Session->delete('takeawayshortlink');

             $checkbox = $this->Session->read('checkbox');
             $this->Session->delete('checkbox');

             $destinationurl = $this->Session->read('destinationurl');
              //echo($destinationurl);
             $this->Session->delete('destinationurl');

             $buttontext = $this->Session->read('buttontext');
             $this->Session->delete('buttontext');
             if($buttontext == '')
             {
              $buttontext = "Click Here";
             }


                    if($destinationurl != '')
				   {
				   
					if(($pos =strpos($destinationurl,'http'))!==false || ($pos =strpos($destinationurl,'https'))!==false)
						{
							$destinationurl=$destinationurl;
						}
						else
						{
							$destinationurl="http://".$destinationurl;
						}

			} else{

					  	$urlk1 = ($this->data['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1); 

                                           if($this->data['UserReference']['business_website'] !=''){
                                                                                     $destinationurl = $this->data['UserReference']['business_website'];

										   if(($pos =strpos($destinationurl,'http'))!==false || ($pos =strpos($destinationurl,'https'))!==false)
						                                      {
							                                    $destinationurl=$this->data['UserReference']['business_website'];
						                                      }
						                                    else
						                                      {
						                                     	$destinationurl="http://".$this->data['UserReference']['business_website'];
						                                      }
                                                 
                                                                                      $consultation_link = SITE_URL."fronts/consultation_request/".$urlk1; 
                                           } else{

                                              $destinationurl = SITE_URL.strtolower($urlk1);
                                              $consultation_link = SITE_URL."fronts/consultation_request/".$urlk1;
					
					    }
		     }	
         ?>
                   <input type="hidden" name="Language" value="<?php echo($this->data['User']['id'])?>" id="member_detail">
                   <input type="hidden" name="Language" value="<?php echo($takeawayshortlink)?>" id="takeawayshortlink">
                   <input type="hidden" name="Language" value="<?php echo($takeawayinput)?>" id="takeawayinput">
                  <input type="hidden" name="Language" value="<?php echo($destinationurl)?>" id="destinationurl">
				   
		<iframe class="preview_iframe stretch no-border" src="<?php echo($link);?>"></iframe>
		
		<div class="email-optup">
			<div class="email-optup-logo">

		    <?php if ($user == ""){?>
                             
				<a href="<?php echo (SITE_URL);?>">
					<img src="<?php echo(SITE_URL)?>imgs/guild_monogram.png" alt="GUILD" />
				</a>
                                <a class="creator-name" href="<?php echo(SITE_URL)?>" target="_blank">GUILD</a>
		    <?php }else{
                       
                if(isset($this->data['UserImage'][0]['image_name']) && $this->data['UserImage'][0]['image_name'] !=''){
                $image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$this->data['User']['id'].DS.$this->data['UserImage'][0]['image_name'];
                } else {
                $image_path = SITE_URL.'img/media/profile.png';	
                 }				
				?>
				<img src="<?php echo($image_path)?>" alt=<?php echo($this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name']);?> />
	                        <a class="creator-name" href="<?php echo($consultation_link);?>" target="_blank"><?php echo($this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name']);?></a>
	   
	            <?php }?>
	            	   
			</div>
			<div class="email-optup-field">
				<span><?php echo($comment);?></span>
                                <?php if($checkbox == "Yes" && $user !=""){?>
				<input id="prospect_client" placeholder="email@company.com" name="data[Case_Study][client_details]" onfocus="this.placeholder=''" onblur="this.placeholder='email@company.com'" type="text" class="form-control" />
				<input type="submit" value="Click Here" class="btn btn-primary" onclick='save_db();'/>
                                <?php }else{?>
                                <input type="submit" value="<?php echo($buttontext);?>" class="btn btn-primary" onclick='window.open("<?php echo ($destinationurl);?>");'/>
                                <?php }?>
                                    
                               
			</div>
		</div>


    	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->

		<!-- <script src="<?php echo(SITE_URL)?>yogesh_new1/js/editor.js"></script> -->
		<!-- Google Re-captcha Code -->
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<!-- Include the Quill library -->
		<script src="https://cdn.quilljs.com/1.0.0/quill.js"></script>
		<!-- Include yogesh JS code -->
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>

		<script type="text/javascript">
			var emailheight = $(".email-optup").outerHeight();
			var windowheight = $(window).height();
			var preview_iframe = windowheight - emailheight;
			$(".preview_iframe").css('height', preview_iframe);
		</script>
<script type="text/javascript">
   	function save_db(){
		var url = jQuery("#takeawayshortlink").val();
              var member_detail = jQuery("#member_detail").val();
              var link = jQuery("#takeawayinput").val();
              var client = jQuery("#prospect_client").val();
              var destinationurl = jQuery("#destinationurl").val();
              
		jQuery(document).ready(function(){
			jQuery.ajax({
				url:'https://test.guild.im/members/prospect_client',
				type:'post',
				dataType:'json',
				data:'url='+ url + '&link=' + link + '&client=' + client +'&member_detail=' +member_detail,
				success:function(res){
					if(res.title !=''){
						 

                                          window.open(destinationurl, '_blank');
                                          
                                          
					}else{
						alert("Failure!!!");
					}
				}
			});
		});
		

		
   	}



 </script>		
    </body>
</html>