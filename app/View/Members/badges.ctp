<head>
           <?php {
             $name = explode(".", $this->data['User']['url_key']);
            }?>
           <meta property="og:title" content="<?php echo(ucfirst($name[0]." ".$name[1]));?>'s Badge - GUILD" />
           <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, GUILD, management consultant" />
	    <meta name="description"  property="og:description" content="GUILD is a trusted advisor to smart businesses. Consult leading experts to save time and drive results." />

	     <title><?php echo(ucfirst($name[0]." ".$name[1]));?> Badge - GUILD</title>
</head>
<div id="inner-content-wrapper" >
    <div id="inner-content-box">
     <?php if($this->Session->read('badgetype') == 'answers1'){
             $type = $this->Session->read('badgetype');
            }
          elseif($this->Session->read('badgetype') == 'answers2'){
           $type = $this->Session->read('badgetype');
          }
           elseif($this->Session->read('badgetype') == 'roundtable1'){
           $type = $this->Session->read('badgetype');
          }
          else{
           $type = $this->Session->read('badgetype');
          }
         ?>
      <?php if($type == 'answers1'){?>
    <div class="mg_answers1container">
    <div class="mg_photo">
              <?php 
					  	$urlk1 = ($this->data['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1); ?>
					 <?php 
					 if(!empty($this->data['UserImage'])){
						if(file_exists(WWW_ROOT . 'img' . '/' .MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'])){ 
							echo($this->Html->link($this->Html->image(MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'], array('class'=>'desaturate','alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank')));                       					
						}else{				
							echo($this->Html->link($this->Html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  
						}
						
					}else{
						echo($this->Html->link($this->Html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  

					}
					?>
    </div>
    <div class="mg_content">
        <div class="mg_name">
          <a href ="<?php echo(SITE_URL);?><?php echo(strtolower($this->data['User']['url_key']));?>"style="text-decoration:none;color:#fff;"target="_blank"><?php echo(ucfirst($this->data['UserReference']['first_name']));?> <?php echo($this->data['UserReference']['last_name']); ?></a>
        </div>
        <div class="mg_answers1subhead">
            <?php
				$disp="guild.im/".strtolower($this->data['User']['url_key']);
				$completelink=SITE_URL.strtolower($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($this->data['User']['url_key']);
                            ?>
             <?php echo ($city);?>, <?php echo ($state);?>
	</div>
        <div class="mg_logo">
            <a href ="<?php echo(SITE_URL);?><?php echo(strtolower($this->data['User']['url_key']));?>"style="text-decoration:none;"target="_blank"><img src="<?php echo(SITE_URL)?>/imgs/lightbadge-logo2.png"></a>
        </div>
         <div class="mg_answerbutton">
           <a style="text-decoration:none;color:white;background-color:#D03135;font-family:'proximanova', Ubuntu;font-weight:normal;font-size:14px;text-align:center;border-style:none;" target="_blank" type="button" href="<?php echo(SITE_URL);?><?php echo(strtolower($this->data['User']['url_key']));?>">View profile</a>
        </div>
    </div>
</div>

<?php } elseif($type == 'answers2'){?>
   <div class="mg_answers2container">
    <div class="mg_photo">
            <?php 
					  	$urlk1 = ($this->data['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1); ?>
					 <?php 
					 if(!empty($this->data['UserImage'])){
						if(file_exists(WWW_ROOT . 'img' . '/' .MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'])){ 
							echo($this->Html->link($this->Html->image(MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'], array('class'=>'desaturate','alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank')));                       					
						}else{				
							echo($this->Html->link($this->Html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  
						}
						
					}else{
						echo($this->Html->link($this->Html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  

					}
					?>
    </div>
    <div class="mg_content">
        <div class="mg_name">
           <a href ="<?php echo(SITE_URL);?><?php echo(strtolower($this->data['User']['url_key']));?>"style="text-decoration:none;color:#333;"target="_blank"><?php echo(ucfirst($this->data['UserReference']['first_name']));?> <?php echo($this->data['UserReference']['last_name']); ?></a>
        </div>
        <div class="mg_answers2subhead">
            <?php
				$disp="guild.im/".strtolower($this->data['User']['url_key']);
				$completelink=SITE_URL.strtolower($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($this->data['User']['url_key']);
                            ?>
                  <?php echo ($city);?>, <?php echo ($state);?>
	</div>
        <div class="mg_logo">
            <a href ="<?php echo(SITE_URL);?><?php echo(strtolower($this->data['User']['url_key']));?>"style="text-decoration:none;"target="_blank"><img src="<?php echo(SITE_URL)?>/imgs/darkbadge-logo1.png"></a>
        </div>
         <div class="mg_answerbutton">
           <a style="text-decoration:none;color:white;background-color:#D03135;font-family:'proximanova', Ubuntu;font-weight:normal;font-size:14px;text-align:center;border-style:none;" target="_blank" type="button" href="<?php echo(SITE_URL);?><?php echo(strtolower($this->data['User']['url_key']));?>">View profile</a>
          </div>
    </div>
</div>

<?php }else if($type == 'logo1'){?>

<div class="mg_logo1container">
    <div class="mg_content">
        <div class="mg_logo1">
            <a href="<?php echo(SITE_URL);?><?php echo(strtolower($this->data['User']['url_key']));?>" target="_blank""><img src="<?php echo(SITE_URL)?>/imgs/darkbadge-logo1.png" title="company logo 1" alt="company logo 1"></a>
        </div>

    </div>
</div>


<?php }else if($type == 'logo2'){?>

<div class="mg_logo2container">
    <div class="mg_content">
        <div class="mg_logo1">
            <a href="<?php echo(SITE_URL);?><?php echo(strtolower($this->data['User']['url_key']));?>" target="_blank""><img src="<?php echo(SITE_URL)?>/imgs/lightbadge-logo2.png" title="company logo 2" alt="company logo 2"></a>
        </div>

    </div>
</div><?php }else{?>
<?php }?>
</div>
</div>
<style>
@import url(https://fonts.googleapis.com/css?family=Ubuntu:300);
.mg_answers1container
{
    width:320px;
    height:90px;
    padding:10px;
    background-color:#333; /* #eee (light) */
    color:#fff; /* #333 (light) */
    font-family:Ubuntu, Arial, vardana;
}

.mg_answers1subhead
{
    font-family:'proximanova light', Ubuntu;
    font-size:13px;
    margin-bottom:27px;
    line-height: 14px;
}
.mg_answers2container
{
    width:320px;
    height:90px;
    padding:10px;
    background-color:#eee; /* #eee (light) */
    color:#333; /* #333 (light) */
    font-family:Ubuntu, Arial, vardana;
}
.mg_content
{
    display:block;
    padding-top:0;
    position:relative;
    bottom:0;
}
.mg_photo img
{
    display:block;
    width:90px;
    height:90px;
    float:left;
    margin-right:12px;
}
.mg_name
{
    font-family:'proximanova semibold', Ubuntu;
    font-weight:bold;
    font-size:15px;
    line-height: 17px;
}
.mg_answers2subhead
{
    font-family:'proximanova light', Ubuntu;
    font-size:13px;
    margin-bottom:27px;
    line-height: 14px;
}
.mg_logo img
{
    width:115.57px;
    height:47px;
    margin-top: -8px;
    float:left;
    margin-left: -10px;
}
.mg_answerbutton
{
    color:white;
    background-color:#D03135;
    border-style:none;
    font-family:'proximanova', Ubuntu;
    font-weight:normal;
    font-size:14px;
    padding:8px 4px 4px 4px;
    text-align:center;
    width:90px;
    height:20px;
    float:right;
}
  .mg_logo1container
    {
        width:150px;
        height:61px;
        padding:10px;
        background-color:#eee; /* #eee (light) */
        color:#333; /* #333 (light) */
        font-family:Ubuntu, Arial, vardana;
    }
 .mg_logo1 img
    {
        width:150px;
        height:61px;
        float:left;
        //margin-right:10px;
    }
.mg_logo2container
    {
        width:150px;
        height:61px;
        padding:10px;
        background-color:#333; /* #eee (light) */
        color:#fff; /* #333 (light) */
        font-family:Ubuntu, Arial, vardana;
    }
.mg_answers_1_vcontainer
    {
        width:120px;
        height:auto;
        padding:10px;
        background-color:#333; /* #eee (light) */
        color:#fff; /* #333 (light) */
        font-family:Ubuntu, Arial, vardana;
    }    
      .mg_1_vcontent
    {
        display:block;
        padding-top:6px;
        position:relative;
        bottom:0;
        clear:both;
    }
    .mg_1_vphoto img
    {
        display:block;
        width:120px;
        height:120px;
        margin-right:12px;
    }
    .mg_1_vname
    {
        font-family:'proximanova semibold', Ubuntu;
        font-weight:bold;
        font-size:16px;
    }
    .mg_1_vsubhead
    {
        font-family:'proximanova light', Ubuntu;
        font-size:13px;
        margin-bottom:12px;
        margin-top:6px;
    }
    .mg_roundtable_vlogo img
    {
        width:100px;
        height:30px;
    }
    .mg_answers_vlogo img
    {
      margin-left:10px;
      width:100px;
      height:40px;
      
    }
    .mg_1_vbutton
    {
        color:white;
        background-color:#D03135;
        border-style:none;
        font-family:'proximanova', Ubuntu;
        font-weight:normal;
        font-size:14px;
        padding:8px 4px 4px 4px;
        text-align:center;
        width:112px;
        height:20px;
        clear:both;
        margin-top:12px;
    }
.mg_answers_2_vcontainer
    {
        width:120px;
        height:auto;
        padding:10px;
        background-color:#eee; /* #eee (light) */
        color:#333; /* #333 (light) */
        font-family:Ubuntu, Arial, vardana;
    }    
   
</style>