<?php echo($this->Html->script(array('autocomplete/jquery.coolautosuggest')));?>
<?php echo($this->Html->css(array('autocomplete/jquery.coolautosuggest')));?>
	<div class="adminrightinner">
		<?php 
		echo($this->Form->create('Directory_user', array('url' => array('controller' => 'members', 'action' => 'admin_directory_edit'),'type'=>'file')));
		echo($this->Form->hidden('Directory_user.id'));
		
		?>    
		<div class="tablewapper2 AdminForm">
			<h3 class="legend1">Edit Directory User  </h3>
		<table border="0" class="Admin2Table" width="100%">
			<tr>
				<td valign="middle" class="Padleft26">Email as username <span class="input_required">*</span></td>
				<td><?php echo($this->Form->input('DirectoryUser.username', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">First Name <span class="input_required">*</span></td>
				<td><?php echo($this->Form->input('DirectoryUser.first_name', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>
				</td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Last  Name</td>
				<td><?php echo($this->Form->input('DirectoryUser.last_name', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>
				</td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Linkedin<span class="input_required">*</span> </td>
				<td>
				<?php  echo($this->Form->input('DirectoryUser.linkedin_url', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>		
				</td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Website<span class="input_required">*</span></td>
				<td>
					<?php  echo($this->Form->input('DirectoryUser.website', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>		
				</td>
			</tr>
			<tr>
				 <td valign="middle" class="Padleft26">Accepting emails? </td>
				 <td>
				 <?php echo($this->Form->radio('DirectoryUser.sendEmail', array('1'=>'Yes','0'=>'No'),array('div'=>false,'default'=>'1','label'=>false,'legend'=>false)));?>
				 </td>
			</tr> 
			<tr>
				 <td valign="middle" class="Padleft26">Quality</td>
				 <td>
				 <?php $options = array('1'=>'1','2'=>'2','3'=>'3'); echo($this->Form->input('DirectoryUser.quality', array('options'=>$options,'div'=>false, 'label'=>false, "class" => "TextBox5")));?>
				 </td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Areas of Expertise</td>
				<td>
					<?php  echo($this->Form->input('DirectoryUser.area_of_expertise', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>		
				</td>
			</tr>		
</table>
		</div>
		<div class="buttonwapper">
			<div>
				<input type="submit" value="Submit" class="submit_button" />
			</div>
			<div class="cancel_button">
				<?php echo $this->Html->link("Cancel", "/members/directory_index/", array("title"=>"", "escape"=>false)); ?>
			</div>
		</div>
		<?php echo($this->Form->end()); ?>
		
	</div>

