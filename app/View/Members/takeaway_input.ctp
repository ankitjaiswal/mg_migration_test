    <div id="inner-content-box" class="pagewidth relatiVE">
      <div id="user-account">
          	<div class="account-form">
                    <div class ="expertise">
                    <h2 style="font-family:,Ubuntu;margin-top: 10px;">My Takeaway  <span style="font-size:18px;">(<a href="<?php echo Configure::read('takeawaysample');?>" target="_blank" style="font-family: Ubuntu, arial, vardana;font-weight: normal;font-size: 14px;">View sample</a>)</span></h2>
                     </div>
          		<div class="column first" style="width: 44%;margin-top: 50px;">           
        			<div class="subheading">
 
          				<h2>1. Enter URL of the article</h2>
        			       </div>
                                   <div class="clear"></div>
                                   <div class="QHINTarea">
			              <textarea id="url" style="margin-top: 5px; height: 75px;width:97%;" placeholder="Enter the link you want to share" name="data[url]" value=""/></textarea>
                                      </div>
			                     
                                      <div class="clear"></div>
			                 <div class="subheading" style="margin-top: 30px;">
                                          <h2>2. My takeaway<span style="float:right; color: rgb(41, 41, 41);  font-size: 12px; font-weight: normal;">(<span id="noOfChar" style="padding-left: 0px; color: rgb(41, 41, 41); font-size: 12px; font-weight: normal;"><?php echo(80)?></span> characters remaining)</span></h2>
                                      </div>
                                      <div class="clear"></div> 
                                       <div class="QHINTarea">              
			                 <textarea id="comment"  style="margin-top: 5px; height: 75px;width:97%" type="text"  placeholder="Enter your comment" name="data[comment]" value=""/></textarea>
			                  </div>
                                    <?php if($this->Session->read('Auth.User.role_id') != '2') {?>
                                      <div class="clear"></div>
			                 <div class="subheading" style="margin-top: 30px;">
                                          <h2>3. Button URL</h2>
                                      </div>
                                      <div class="clear"></div> 
                                       <div class="QHINTarea">              
			                 <textarea id="destinationurl"  style="margin-top: 5px; height: 75px;width:97%" type="text"  placeholder="Enter url of your website" name="data[destinationurl]" value=""/></textarea>
			                  </div>
                                       <?php }?>

               <div class="styled-form" style="padding-top:17px;">
                          
<input type="checkbox" name="checkbox" id="an"  placeholder=""  checked>                            
                            <label for="an"><span style="font-family: Ubuntu, arial, vardana;
font-size: 14px;"> Get prospect email</span>
</label></div>

                                       <div class="editSUB" style="margin-top: 50px;" id="button">
                                         <input class="create_btn" type="button" style="width:150px !important; margin-top: -11px;" value="Create link" onclick="create_url();">
	                                  </div>
                                     </br></br></br>


                                     </div>
                                     
					<div class="column last" style="margin-top: 50px;margin-right:3px;">
                                     <div class="clear"></div>
						<div class="subheading">
			                     <h2>Share this<sub style="font-size: 11px; font-weight: normal; font-family: Ubuntu">&nbsp;</sub></h2>
                                          </div>

                                           <div class="clear"></div>

                                           <div class="QHINTarea">
			                      <p><textarea id="link"  type="text" style="margin-top: 5px;width:96%; height: 75px;font-size:17px;background-color : #CCCCCC;" placeholder="http://bit.ly" name="data[link]" value=""/></textarea></p>

		                     </div>




 
        
         
        
                    
			
			<div class="share_icons" onmouseout="doSomethingMouseOut();"  onmouseover="doSomethingMouseOver()" id = "share_icon" style="width: 220px; float: right;display:none;">
			<div class="editSUB" style="float:right; padding-right: 6px;" >
                      <input class="create_btn" type="button" style="width:77px !important;height:25px;margin-top: -40px;" value="Share" onclick='javascript:doSomething();',onmouseover = 'doSomethingMouseOver();'>

			
			</div>

						
<div id="shareicon" style="float:right ;visibility:hidden">						

                               
                                <input type="hidden" name="Language" value="<?php echo($completelink)?>" id="completelinkJS">
                                <input type="hidden" name="Language" value="<?php echo($compTitle)?>" id="compTitleJS">

                                <a href="#"><img src="/img/images/g_plus_icon.png" alt="Google" height="25" width="27" id="google_share"></a>
                                <a href="#"><img src="/img/images/fb_icon.png" alt="Facebook" height="25" width="27" id="facebook_share"></a>
                                <a href="#"><img src="/img/images/logo_linkedin.png" alt="LinkedIn" height="25" width="27" id="linkedin_share"></a>
                                <a href="#"><img src="/img/images/twit_icon.png" alt="twitter" height="25" width="27" id="twitter_share"></a>




			</div>			
			</div>     
                                   </div>
                                         
            <div class="clear"></div>
<?php if($this->Session->read('Auth.User.id') !='' && $this->Session->read('Auth.User.role_id') == '2') {?>
            <div class="tpLINE" style="margin-top: 80px;height: 330px;"></div>
 <?php }else{?>
             <div class="tpLINE" style="margin-top: 80px;height: 500px;"></div>
 <?php }?>
					</div>


</div>
</div>
</div>
<script type="text/javascript">

jQuery(document).ready(function(){

		 var qText = jQuery("#comment").val();
    	        jQuery("#noOfChar").html(80 - qText.length);
    	
		 function updateCount ()
		    {
		        var qText = jQuery("#comment").val();

		       if(qText.length < 80) {
		           jQuery("#noOfChar").html(80 - qText.length);
		       } else {
		           jQuery("#noOfChar").html(0);
		           jQuery("#comment").val(qText.substring(0,80));
		       }
		    }

		    jQuery("#comment").keyup(function () {
		        updateCount();
		    });
		    jQuery("#comment").keypress(function () {
		        updateCount();
		    });
		    jQuery("#comment").keydown(function () {
		        updateCount();
		    });
               });
</script>
<script type="text/javascript">
   	function create_url(){
		var url = jQuery("#url").val();
              var comment = jQuery("#comment").val();
              var destinationurl = jQuery("#destinationurl").val();

          if(document.getElementById('an').checked) {
    var checkbox = "Yes";
  } else {
    var checkbox = "No";
   }

		jQuery(document).ready(function(){
			jQuery.ajax({
				url:SITE_URL+'members/takeaway_input',
				type:'post',
				dataType:'json',
				data:'url='+ url + '&comment=' + comment + '&destinationurl=' + destinationurl + '&checkbox=' + checkbox,
				success:function(res){
                                    
					if(res.title !=''){
						 
						 
                                          var link = res.link;
                                          var title = res.title;
                                          if(title != null){
                                          var combined = title+ ' ' + link;
                                          }
                                          else{
                                          var combined = ' ' + link;
                                          }
                                         
                                          document.getElementById('share_icon').style.display="inline-block"; 
						jQuery('#link').val(combined);
                                          jQuery('#completelinkJS').val(link);
                                          jQuery('#compTitleJS').val(title);
                                          window.open(res.link, '_blank');
                                          
                                          
					}else{
						alert("Failure!!!");
					}
				}
			});
		});
		

		
   	}

		jQuery('#twitter_share').click(function(){
			
			var compTitle = jQuery('#compTitleJS').val();
			var completelink = jQuery('#completelinkJS').val();
			
			var tweetURL ="https://twitter.com/intent/tweet?text="+compTitle+" "+completelink;
			window.open(tweetURL, '_blank');
			
			
		});

		jQuery('#linkedin_share').click(function(){
			
			var compTitle = jQuery('#compTitleJS').val();
			var completelink = jQuery('#completelinkJS').val();
			
			var linkedinURL ="https://www.linkedin.com/shareArticle?mini=true&url="+completelink+"&title="+compTitle;
			window.open(linkedinURL, '_blank');
			
			
		});

		jQuery('#google_share').click(function(){
			
			var compTitle = jQuery('#compTitleJS').val();
			var completelink = jQuery('#completelinkJS').val();
			
			var googleURL ="https://plus.google.com/share?url="+completelink+"&title="+compTitle;
			window.open(googleURL, '_blank');
			
			
		});
		jQuery('#facebook_share').click(function(){
			
			var compTitle = jQuery('#compTitleJS').val();
			var completelink = jQuery('#completelinkJS').val();
			
			var facebookURL ="https://www.facebook.com/sharer.php?s=100&p[url]="+completelink+"&p[title]="+compTitle;
			window.open(facebookURL, '_blank');					
								
		});

 </script>

<script>
var clickTrack = 0;
function doSomethingMouseOver(){
	if(clickTrack == 0){
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
		vis.visibility='visible';
		vis.position='relative';
	}
}

function doSomethingMouseOut(){
	if(clickTrack == 0){
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
		vis.visibility='hidden';
		vis.position='absolute';
	}
}

function doSomething()
{
		
		//alert(clickTrack);	
		var id_div=document.getElementById("shareicon");
		var vis = id_div.style;
	if ( (vis.visibility=='visible' || vis.visibility=='hidden') && clickTrack == 0)
	{
		clickTrack = 1;
		vis.visibility='visible';
		vis.position='relative';
	}
	else if (vis.visibility=='visible' && clickTrack == 1)
	{
		clickTrack = 0;
		vis.visibility='hidden';
		vis.position='absolute';
		
	}
}
</script>
