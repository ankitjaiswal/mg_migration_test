	<div class="container-fluid" id="apply-section">
		<div class="container create-section pad0">
			<div class="col-md-8 pad40">
				<div class="apply-heading">
					<h5>Case studies</h5>
				</div>
				<div class="apply-form">
					<form action="<?php echo SITE_URL ?>members/edit_casestudy" method="post" id="Addform">
                                           <?php echo($this->Form->hidden("CaseStudy.id",array('value'=>$case_study['CaseStudy']['id']))); ?>
						<div class="form-group" data-placement="right" data-toggle="DescPopover" data-container="body">
							<label>Title</label>
							<span class="pull-right char-limit">(<span id="noOfChar"><?php echo(128 - strlen($case_study['CaseStudy']['title']))?></span> characters remaining)</span>
							<input type="text" id="title" class="form-control" placeholder="Title" name="data[CaseStudy][title]" value="<?php echo htmlspecialchars($case_study['CaseStudy']['title']);?>">
							<div id="popover-content" class="hide">
								<p>Keep it short & interesting (128 characters).</p>
								<p>e.g. Driving Transformation in a Family Owned Business</p>
								
							</div>
						</div>
						<div class="form-group" data-placement="right" data-toggle="DescPopover1" data-container="body">
							<label>Client</label>
							<input type="text" id="client_details" class="form-control" placeholder="Client name, or industry & size" name="data[CaseStudy][client_details]" value="<?php echo htmlspecialchars($case_study['CaseStudy']['client_details']);?>">
							<div id="popover-content1" class="hide">
								<p>e.g. $250 Million retail franchise</p>
								
							</div>
						</div>
						<div class="form-group" data-placement="right" data-toggle="DescPopover2" data-container="body">
							<label>Situation</label>
							<span class="pull-right char-limit">(<span id="noOfCharSituations"><?php echo(1250 - strlen($case_study['CaseStudy']['situation']))?></span> characters remaining)</span>
                                                        <input type="hidden" name="data[CaseStudy][situation]"  id="situationtext"  value="<?php echo htmlspecialchars($case_study['CaseStudy']['situation']); ?>"/>
							<div id="editor-container"></div>
							<div id="popover-content2" class="hide">
								<p>Capture reader's interest (1250 characters)</p>
								<p>- What does the client do?</p>
								<p>- What was the context/ environment?</p>
								<p>- What were the client needs?</p>
								<p>- What was the complexity?</p>
							</div>
						</div>
						<div class="form-group" data-placement="right" data-toggle="DescPopover3" data-container="body">
							<label>Actions</label>
							<span class="pull-right char-limit">(<span id="noOfCharActions"><?php echo(1250 - strlen($case_study['CaseStudy']['actions']))?></span> characters remaining)</span>
                                                        <input type="hidden" name="data[CaseStudy][actions]"  id="actionstext" value="<?php echo htmlspecialchars($case_study['CaseStudy']['actions']); ?>"/>
							<div id="editor-container1"></div>
							<div id="popover-content3" class="hide">
								<p>Get specific (1250 characters)</p>
								<p>- Where did you begin?</p>
								<p>- What were the different steps/ milestones?</p>
								<p>- What actions did you take, for how long?</p>
								<p>- Show how external intervention helps</p>
								<p>- The 'solution' should not be obvious</p>
							</div>
						</div>
						<div class="form-group" data-placement="right" data-toggle="DescPopover4" data-container="body">
							<label>Results</label>
							<span class="pull-right char-limit">(<span id="noOfCharResults"><?php echo(1250 - strlen($case_study['CaseStudy']['result']))?></span> characters remaining)</span>
                                                        <input type="hidden" name="data[CaseStudy][result]"  id="resultstext" value="<?php echo htmlspecialchars($case_study['CaseStudy']['result']); ?>"/>
							<div id="editor-container2"></div>
							<div id="popover-content4" class="hide">
								<p>Talk ROI (1250 characters)</p>
								<p>- Use numbers or tangible benefits, if possible</p>
								<p>- Both short term and long term benefits</p>
                                                                <p>- Finish your story</p>
							</div>
						</div>
						<div class="e-button pull-right">
							<label>

                                                                  <?php if($case_study['CaseStudy']['specifier']=="Publish"){
								 echo($this->Html->link("Cancel",SITE_URL.'members/casestudy/'.$case_study['CaseStudy']['casestudy_url']));
                                                                }else{

                                                               echo($this->Html->link("Cancel",SITE_URL.'members/casestudy_preview/'.$case_study['CaseStudy']['id']));

                                                             } ?>

							</label>
							<button class="banner-getstarted btn btn-default get-started" id="submitForm">
								Preview
							</button>
						</div>
					</form>
				</div>
			</div>

			<div class="col-md-4 clearboth create-right">
				<div class="right-apply">
					<div class="apply-heading">
				         <h5>Showcase past projects</h5>
					</div>
					<div class="apply-desc">
						<div class="cr-desc">
							<h5>1. Clearly state the business problem</h5>
							<p>Allow other organizations to identify with your customer and then you've got them hooked for the solutions.</p>
						</div>
						<div class="cr-desc">
							<h5>2. Identify the solution and celebrate your results</h5>
							<p>What was your approach, and the business impact &#8211; real or expected.</p>
						</div>
						<div class="cr-desc">
							<h5>3. Promote your case studies</h5>
							<p>Use social media to promote your case studies on GUILD, and we will do the same.</p>
						</div>
					</div>
					<div class="apply-heading">
						<h5>Not ready to post yet?</h5>
					</div>
					<div class="apply-desc">
						<div class="cr-desc mt10">
							<p>Call us to discuss your business needs. </br>Call <a href='tel:+18665111898'>1-866-511-1898</a>.</p>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>

	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/quill.min.js"></script>

	<script type="text/javascript">
		$(function() {
			$( ".data-picker" ).datepicker();
		});
		$(".hide-fields").on("click",function (e) {
			e.preventDefault();
			$(".optional-fields").addClass("hide");
			$(this).addClass("hide");
			$(".show-fields").removeClass("hide");
		});
		$(".show-fields").on("click",function (e) {
			e.preventDefault();
			$(".optional-fields").toggleClass("hide");
			$(this).addClass("hide");
			$(".hide-fields").removeClass("hide");
		});
		if($(window).width() > 991){
			$("[data-toggle=DescPopover]").popover({
				html: true, 
				trigger : 'hover',
				content: function() {
					return $('#popover-content').html();
				}
			});
			$("[data-toggle=DescPopover1]").popover({
				html: true, 
				trigger : 'hover',
				content: function() {
					return $('#popover-content1').html();
				}
			});
			$("[data-toggle=DescPopover2]").popover({
				html: true, 
				trigger : 'hover',
				content: function() {
					return $('#popover-content2').html();
				}
			});
			$("[data-toggle=DescPopover3]").popover({
				html: true, 
				trigger : 'hover',
				content: function() {
					return $('#popover-content3').html();
				}
			});
			$("[data-toggle=DescPopover4]").popover({
				html: true, 
				trigger : 'hover',
				content: function() {
					return $('#popover-content4').html();
				}
			});

		}



        var limit = 1250;
        var situationeditmode= jQuery("#situationtext").val();
        var actionseditmode= jQuery("#actionstext").val();
        var resultseditmode= jQuery("#resultstext").val();

    if(situationeditmode !=''){

    jQuery("#editor-container").html(situationeditmode);

     var quillsolution = new Quill('#editor-container', {
    	modules: {
    	toolbar: [
      ['bold', 'italic', 'underline','link'], 
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      ['clean']
    	]
    	},
    	placeholder: 'Tell a story your ideal prospects can relate to, in their language',
    	theme: 'snow' 
    });

     quillsolution.on('text-change', function (delta, old, source) {

    if (quillsolution.getLength() > limit) {
     quillsolution.deleteText(limit, quillsolution.getLength());
     }
    });

  }else{

      var quillsituation = new Quill('#editor-container', {
      modules: {
    	toolbar: [
      ['bold', 'italic', 'underline','link'], 
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      ['clean']
      ]
      },
      placeholder: 'Tell a story your ideal prospects can relate to, in their language',
      theme: 'snow'
     });
     


     quillsituation.on('text-change', function (delta, old, source) {

     if (quillsituation.getLength() > limit) {
     quillsituation.deleteText(limit, quillsituation.getLength());
     }
    });


  }

   if(actionseditmode !=''){

     jQuery("#editor-container1").html(actionseditmode);

    var quillactions = new Quill('#editor-container1', {
    	modules: {
    	toolbar: [
      ['bold', 'italic', 'underline','link'], 
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      ['clean']
      ]
    	},
    	placeholder: 'Where did you come in and what did you do?',
    	theme: 'snow' 
    });

     quillactions.on('text-change', function (delta, old, source) {

     if (quillactions.getLength() > limit) {
     quillactions.deleteText(limit, quillactions.getLength());
    }
    });

  }else{ 

      var quillactions = new Quill('#editor-container1', {
      modules: {
    	toolbar: [
      ['bold', 'italic', 'underline','link'], 
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      ['clean']
      ]
      },
      placeholder: 'Where did you come in and what did you do?',
      theme: 'snow'
     });
     


     quillactions.on('text-change', function (delta, old, source) {

     if (quillactions.getLength() > limit) {
     quillactions.deleteText(limit, quillactions.getLength());
     }
    });
 
 }

    if(resultseditmode !=''){

     jQuery("#editor-container2").html(resultseditmode);

    var quillresults = new Quill('#editor-container2', {
    	modules: {
    	toolbar: [
      ['bold', 'italic', 'underline','link'], 
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      ['clean']
       ]
    	},
    	placeholder: 'What was the outcome of your intervention?',
    	theme: 'snow' 
    });

     quillresults.on('text-change', function (delta, old, source) {

     if (quillresults.getLength() > limit) {
     quillresults.deleteText(limit, quillresults.getLength());
    }
    });

     }else{

      var quillresults = new Quill('#editor-container2', {
      modules: {
    	toolbar: [
      ['bold', 'italic', 'underline','link'], 
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      ['clean']
       ]
      },
      placeholder: 'What was the outcome of your intervention?',
      theme: 'snow'
     });
     


     quillresults.on('text-change', function (delta, old, source) {

     if (quillresults.getLength() > limit) {
     quillresults.deleteText(limit, quillresults.getLength());
     }
    });

   }

</script>

<script type="text/javascript">

	function updateCount ()
    {
        var qText = jQuery("#title").val();

       if(qText.length < 128) {
           jQuery("#noOfChar").html(128 - qText.length);
       } else {
           jQuery("#noOfChar").html(0);
           jQuery("#title").val(qText.substring(0,128));
       }
    }

    jQuery("#title").keyup(function () {
        updateCount();
    });
    jQuery("#title").keypress(function () {
        updateCount();
    });
    jQuery("#title").keydown(function () {
        updateCount();
    });


	function updateCountSituation()
     {

        var qText = jQuery("#editor-container").text().length;

        var text = jQuery("#editor-container").val();

       jQuery("#noOfCharSituations").html(1250 - qText);
        
       if(qText<= 1250) {


            jQuery('#noOfCharSituations').css('color','#000');

       } else {

           jQuery("#noOfCharSituations").html(0);
   
       }
    }

    jQuery("#editor-container").keyup(function () {
    	updateCountSituation();
    });
    jQuery("#editor-container").keypress(function () {
    	updateCountSituation();
    });
    jQuery("#editor-container").keydown(function () {
    	updateCountSituation();
    });


	function updateCountAction()
     
      {
         var qText = jQuery("#editor-container1").text().length;


        jQuery("#noOfCharActions").html(1250 - qText);
        
       if(qText <= 1250) {
            jQuery('#noOfCharActions').css('color','#000');
       } else {
            jQuery('#noOfCharActions').html(0);
       }
    }

    jQuery("#editor-container1").keyup(function () {
    	updateCountAction();
    });
    jQuery("#editor-container1").keypress(function () {
    	updateCountAction();
    });
    jQuery("#editor-container1").keydown(function () {
    	updateCountAction();
    });


	function updateCountResult()
    {

       var qText = jQuery("#editor-container2").text().length;
       
       jQuery("#noOfCharResults").html(1250 - qText);
        
       if(qText <= 1250) {
           jQuery('#noOfCharResults').css('color','#000');
       } else {
           jQuery('#noOfCharResults').html(0);
       }
    }

    jQuery("#editor-container2").keyup(function () {
    	updateCountResult();
    });
    jQuery("#editor-container2").keypress(function () {
    	updateCountResult();
    });
    jQuery("#editor-container2").keydown(function () {
    	updateCountResult();
    });


</script>

<script type="text/javascript">

jQuery(document).ready(function(){

   jQuery("#submitForm").click(function(){


	var goingID = '';
        var flag= 0;



	var situationEntered = jQuery("#editor-container").html();

        var result1 = situationEntered.split('<div class="ql-clipboard"');
        situationEntered = result1[0];
	situationEntered = situationEntered.replace(/  /gi, '&nbsp;&nbsp;');
        
        jQuery("#situationtext").val(situationEntered);


	var actionsEntered = jQuery("#editor-container1").html();

        var result2 = actionsEntered.split('<div class="ql-clipboard"');
        actionsEntered = result2[0];
	actionsEntered = actionsEntered.replace(/  /gi, '&nbsp;&nbsp;');
        
        jQuery("#actionstext").val(actionsEntered);

	var resultsEntered = jQuery("#editor-container2").html();

        var result3 = resultsEntered.split('<div class="ql-clipboard"');
        resultsEntered = result3[0];
	resultsEntered = resultsEntered.replace(/  /gi, '&nbsp;&nbsp;');
        
        jQuery("#resultstext").val(resultsEntered);

    
     if(jQuery.trim(jQuery("#title").val()) == '') {
        jQuery('#title').css('border-color','#F00');
        jQuery('#title').css('border-width','<?php echo(ERROR_BORDER);?>px');
        jQuery('#title').focus();
        goingID = 'title';
        flag++;
      }else {
        if(jQuery("#title").val().length>128)
        {
           jQuery('#title').css('border-color','#F00');
           jQuery('#title').css('border-width','<?php echo(ERROR_BORDER);?>px');
           if(goingID==''){ goingID = 'title';   }  
           flag++;  
        }
        else
        {
            jQuery('#title').css('border-color','');    
            jQuery('#title').css('border-width','<?php echo(NORMAL_BORDER);?>px');
        }	
     }
	

	if(jQuery.trim(jQuery("#client_details").val()) == '') {
        jQuery('#client_details').css('border-color','#F00');
        jQuery('#client_details').css('border-width','<?php echo(ERROR_BORDER);?>px');
        jQuery('#client_details').focus();
        goingID = 'client_details';

        flag++;
     }
        else
        {
            jQuery('#client_details').css('border-color','');     
            jQuery('#client_details').css('border-width','<?php echo(NORMAL_BORDER);?>px');
        }	
   



	if(jQuery.trim(jQuery("#editor-container").text()) == '') {

        jQuery('#editor-container').css('border-color','#F00');
        jQuery('#editor-container').css('border-width','<?php echo(ERROR_BORDER);?>px');
        jQuery('#editor-container').focus();
        goingID = 'editor-container';
        flag++;
    }

	if(jQuery.trim(jQuery("#editor-container1").text()) == '') {
        jQuery('#editor-container1').css('border-color','#F00');
        jQuery('#editor-container1').css('border-width','<?php echo(ERROR_BORDER);?>px');
        jQuery('#editor-container1').focus();
        goingID = 'editor-container1';
        flag++;
    }

	if(jQuery.trim(jQuery("#editor-container2").text()) == '') {
        jQuery('#editor-container2').css('border-color','#F00');
        jQuery('#editor-container2').css('border-width','<?php echo(ERROR_BORDER);?>px');
        jQuery('#editor-container2').focus();
        goingID = 'editor-container2';
        flag++;
    }

	 if(flag == 0){
		 jQuery("#Addform").submit();
     }else{
         if(goingID != ""){
             jQuery('#'+goingID).focus();
         }
         return false;
     }

	 
		 
});

});
</script>