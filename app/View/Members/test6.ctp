<div class="content-wrap post-project" id="apply-section">
	<div class="container create-section">
		<div class="row">
			<div class="col-sm-8">
				<div class="apply-heading">
					<h5>Post a project</h5>
					<label class="free-c"> (Free & confidential)</label>
				</div>
				<div class="project-form">
					<div class="form-group">
						<label>Describe your need</label>
						<span class="pull-right char-limit">5000 characters remaining</span>
						<textarea rows="8" placeholder="Please provide all relevant details about your project." class="form-control"></textarea>
					</div>
					<div class="form-group">
						<span class="pull-right char-limit">128 characters remaining</span>
						<input type="text" placeholder="Project Title" class="form-control" />
					</div>
					<div class="row">
						<div class="col-sm-6">
							<select class="form-control active">
								<option>Target Start Date</option>
								<option>Immediate</option>
								<option>Within 1 month</option>
								<option>Other/ Not sure</option>
							</select>
						</div>
						<div class="col-sm-6">
							<select class="form-control active">
								<option>Approximate Budget</option>
								<option><$10K</option>
								<option>$10K to $50K</option>
								<option>$50K</option>
							</select>
						</div>
					</div>
				</div>
				<div class="project-form extra-fields">
					<div class="form-group">
						<label>Describe the ideal professional for your project</label>
						<span class="pull-right char-limit">5000 characters remaining</span>
						<textarea rows="8" placeholder="Specify industry experience, key expertise, etc." class="form-control"></textarea>
					</div>
					<div class="form-group">
						<span class="pull-right char-limit">128 characters remaining</span>
						<input type="text" placeholder="Project Title" class="form-control" />
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<select class="form-control active">
									<option>Select Industry</option>
									<option>Select Industry 1</option>
									<option>Select Industry 2</option>
									<option>Select Industry 3</option>
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<select class="form-control active">
									<option>Select Category</option>
									<option>Select Category 1</option>
									<option>Select Category 2</option>
									<option>Select Category 3</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<select class="form-control active">
									<option>My Business Need</option>
									<option>My Business Need 1</option>
									<option>My Business Need 2</option>
									<option>My Business Need 3</option>
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<select class="form-control active">
									<option>Solution Type</option>
									<option>Solution Type 1</option>
									<option>Solution Type 2</option>
									<option>Solution Type 3</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<select class="form-control active">
									<option>Estimated Duration</option>
									<option>10 hours</option>
									<option>50 hours</option>
									<option>80 hours</option>
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
							<input type="text" placeholder="Location Preference" class="form-control" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-attachment">
							<input type="file" />
						</div>
					</div>
				</div>
				<div class="e-button pull-right">
					<label>
						<a href="javascript:;" class="show-fields">Hide optional fields</a>
						<a href="javascript:;" class="hide-fields hide">Show optional fields</a>
					</label>
					<button class="banner-getstarted btn btn-default get-started">Save and preview</button>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="apply-heading">
					<h5>How does it work?</h5>
				</div>
				<div class="cr-desc">
					<h5>1. Specify Business Need</h5>
					<p>Specify your requirement using our 1-page form. We will contact you within 1 business day to review your needs.</p>
				</div>
				<div class="cr-desc">
					<h5>2. Review Proposals</h5>
					<p>Receive proposals from relevant experts. Use free consultations to select the candidate of your choice.</p>
				</div>
				<div class="cr-desc">
					<h5>3. Get work done</h5>
					<p>Get started with your chosen professional, while our admin staff keeps project overheads to a minimum.</p>
				</div>
				<div class="apply-heading">
					<h5>Not ready to post yet?</h5>
				</div>
				<div class="cr-desc mt0">
					<p>Call us to discuss your business needs.</p>
					<p>Call <a href="tel:1-866-511-1898">1-866-511-1898</a>.</p>
				</div>
			</div>

			<!-- js link -->			
			<script type="text/javascript">
				jQuery(document).ready(function(){
					jQuery(function() {
						jQuery('select').change(function() {
							var old_type = this.id.substring(7);
							type = old_type.substring(0,8);							
							if(type == 'Industry') {
			                   	console.log(1);
								var id = old_type.substring(8);								
								var industry = this.value;								
								var select_id = "#ProjectCategory"+id;								
								var item = jQuery(select_id);							
								item.find('option').remove();
								if(industry != -1) {
									 console.log(2);
									var cats = document.getElementById(industry);
									var cat_array = cats.value.split("|");
									    //alert(cat_array);
		                                var i = cat_array.indexOf("ALL CATEGORIES");
					                    if(i != -1) {
				                        	cat_array.splice(i, 1);
	                                 	}					                                 
                                     	cat_array =  cat_array.sort();
                                     	cat_array.push('ALL CATEGORIES');
									for (i = 0; i < cat_array.length; i++) {
										item.append('<option value="' + cat_array[i] + '">' + cat_array[i] + '</option>');
							        }
						      } else {
						      	item.append('<option value="-1"> </option>');	
						      }						          
					        }          
						});
					});
					jQuery(".show-fields").click(function () {
			            jQuery(".extra-fields").hide("slow");
			            jQuery(".hide-fields").removeClass("hide");
			            jQuery(".show-fields").addClass("hide");
			        });
					jQuery(".hide-fields").click(function () {
			            jQuery(".extra-fields").show("slow");
			            jQuery(".hide-fields").addClass("hide");
			            jQuery(".show-fields").removeClass("hide");
			        });
				});
			</script>
		</div>
	</div>
</div>