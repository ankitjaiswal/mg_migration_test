
	<div id="banner-section" class="lenses-page f-page" data-parallax="scroll" data-image-src="<?php echo(SITE_URL)?>/yogesh_new1/images/banner9lenses.jpg">
		<div class="col-md-12 pad0 text-center banner-content">
			<h1>Business Documents by Flevy</h1>
			<p>Business frameworks & methodologies, presentation templates, financial models, and more.</p>
		</div>
	</div>
	<div class="lenses-section flevy-page container pad0">
		<div class="lenses-heading">
			<h1>Business documents by</h1>
			<label>
				<img src="<?php echo(SITE_URL)?>/yogesh_new1/images/flevy-logo.png">
			</label>
		</div>
		<div class="l-desc">
			<p>Flevy documents include business frameworks & methodologies, presentation templates, financial models, and more. In particular, there is a wealth of business frameworks.</p>
			<p>Business frameworks are in-depth business training guides—the same used by top global consulting firms when they deploy their consulting project teams to Fortune 500 companies.</p>
			<h5>Free documents</h5>
                        <?php if($this->Session->read('Auth.User.id') !='' && $this->Session->read('Auth.User.role_id') == '2') {?>
			<ul>
				<li>
					<div class = "solTitle"><a href="<?php echo(SITE_URL)?>imgs/Powerpoint_toolkit.ppt" id ="Powerpoint toolkit">
						Powerpoint toolkit
					</a></div>
				</li>
				<li>
					<div class = "solTitle"><a href="<?php echo(SITE_URL)?>imgs/Strategy_frameworks.ppt" id ="Strategy frameworks">
						Strategy frameworks
					</a></div>
				</li>
				<li>
					<div class = "solTitle"><a href=<?php echo(SITE_URL)?>imgs/Introduction_to_Strategy.pptx" id ="Introduction to strategy">
						Introduction to strategy
					</a></div>
				</li>
				<li>
					<div class = "solTitle"><a href="<?php echo(SITE_URL)?>imgs/Lean_thinking_101.pdf" id ="Lean thinking 101">
						Lean thinking-101
					</a></div>
				</li>
				<li>
					<div class = "solTitle"><a href="<?php echo(SITE_URL)?>imgs/Operational_Excellence.pptx" id ="Operational excellence">
						Operational excellence
					</a></div>
				</li>
				<li>
					<div class = "solTitle"><a href="<?php echo(SITE_URL)?>imgs/Risk_management.pdf" id ="Risk management">
						Risk management
					</a></div>
				</li>
			</ul>

                    <?php }else{?>

			<ul>
				<li>
					<a class="mymodal" data-toggle="modal" data-target="#myModal">
						Powerpoint toolkit
					</a>
				</li>
				<li>
					<a class="mymodal" data-toggle="modal" data-target="#myModal">
						Strategy frameworks
					</a>
				</li>
				<li>
					<a class="mymodal" data-toggle="modal" data-target="#myModal">
						Introduction to strategy
					</a>
				</li>
				<li>
					<a class="mymodal" data-toggle="modal" data-target="#myModal">
						Lean thinking-101
					</a>
				</li>
				<li>
					<a class="mymodal" data-toggle="modal" data-target="#myModal">
						Operational excellence
					</a>
				</li>
				<li>
					<a class="mymodal" data-toggle="modal" data-target="#myModal">
						Risk management
					</a>
				</li>
			</ul>
                     <?php }?>
		</div>
		<div class="lenses-discount">
			<h1>All documents on <a href="https://flevy.com/" target="_blank">Flevy.com</a></h1>
			<label>
				(10% off for Regular Members and 15% off for premium members.)
			</label>
		</div>
		<div class="l-desc">
                     <?php if($this->Session->read('Auth.User.id') ==''){?>
			<p>Discount codes are available to member consultants.
				<a href="#" class="mymodal" data-toggle="modal" data-target="#signin-Modal">Login to view</a>.
			</p>
                      <?php } elseif($this->Session->read('Auth.User.role_id') == 2 && $this->Session->read('Auth.User.mentor_type') == 'Premium Member'){?>
			<p>Your promo code:
				<a href="https://flevy.com/" target="_blank">PREMG15</a>
			</p>
                      <?php } elseif($this->Session->read('Auth.User.role_id') == 2 && $this->Session->read('Auth.User.mentor_type') != 'Premium Member'){?>
			<p>Your promo code:
				<a href="https://flevy.com/" target="_blank">REGMG10</a>
			</p>
                      <?php }?>
		</div>
	</div>



	<!-- js link -->
	<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>-->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>-->

	<script type="text/javascript">
		// parallax
		if($(window).width() >991){
			$('#banner-section').parallax();
		}
		</script>

<script type="text/javascript">
jQuery(document).ready(function(){


jQuery(".solTitle a").click(function(e){
e.preventDefault();
 var title = jQuery(this).attr('id');
 var url =jQuery(this).attr("href");

  var member = '<?php echo ($this->Session->read('Auth.User.id')) ;?>';
  
		       jQuery.ajax({
					url:SITE_URL+'members/flevy_email',
					type:'post',
					dataType:'json',
					data: 'title='+ title + '&member=' + member + '&url=' + url,
					success:function(url){
                                           window.location.href= url;
						
                                        
					}
				});	

 
});
});
</script>