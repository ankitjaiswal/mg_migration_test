<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="fieldset">
    <h3 class="legend">
		Apply to be a Member Detail
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <input type="hidden" name="pageAction" id="pageAction"/>	 
        
            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr>
                        <td>
                           <b>Name</b>
                        </td>
                        <td>
                            <?php echo ucwords($mentor_data['MentorApply']['firstname']) . ' ' . $mentor_data['MentorApply']['lastname']; ?>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <b> Email</b>
                        </td>
                        <td>
                            <?php echo $mentor_data['MentorApply']['email']; ?>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <b>Background summary</b>
                        </td>
                        <td>
                           <?php echo $mentor_data['MentorApply']['background_summary']; ?> 
                       
						</td>
                    </tr>
                    <tr>
                        <td>
                            <b>Linked In profile URL</b>
                        </td>
                        <td>
                           <?php echo $mentor_data['MentorApply']['linkedin_url']; ?> 
                       
						</td>
                    </tr>
                    <tr>
                        <td>
                            <b>Business website URL</b>
                        </td>
                        <td>
                           <?php echo $mentor_data['MentorApply']['business_website']; ?> 
                       
						</td>
                    </tr>
                    <tr>
                        <td>
                           <b>Phone</b>
                        </td>
                        <td>
                           <?php echo $mentor_data['MentorApply']['phone']; ?> 
                        </td>
                    </tr>
                </table>
            </div>
    </div>
</div>
<div class="buttonwapper">
	<div class="cancel_button">
		<?php echo $this->Html->link("Back", "/admin/members/pending/", array("title"=>"", "escape"=>false)); ?>
	</div>
</div>
<script>
jQuery(document).ready(function(){
	jQuery(".saveDate").click(function(){
		var hrefvalue = jQuery(this).attr('title')		
		window.location.href = hrefvalue;
	})

});

</script>