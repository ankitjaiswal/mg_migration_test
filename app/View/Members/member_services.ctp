
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth" style="margin-bottom:50px;">
 	    <div id="user-account">
	      <div class="account-form">	    
		  	<div class="onbanner">
		          <h1>                 
		            <div style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;"><span style="text-decoration: underline;">Member Services</span></div></h1>
                        
				  <div style="position:relative;top:80px;"><p><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal;font-size: 18px;">Guild helps experienced consultants get more business and be more productive.</span></div>	
			</div>

                     <br><br><br><br>
                    <div class ="expertise">
                    <h2 style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">1. <a href="<?php echo SITE_URL?>members/pr" style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;" title="PR Opportunities">PR Opportunities</a></h2>
                    <p style="font-size:16px;">Send press releases and get media mentions. <a href="<?php echo SITE_URL?>members/pr" title="PR Opportunities">Learn More</a>.</p>
                   </div>
                   <div class ="expertise">
                    <h2 style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">2. <a href="<?php echo SITE_URL?>members/premium_membership" style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;" title="Premium Membership" >Premium Membership</a></h2>
                    <p style="font-size:16px;">Increase your visibility on Guild, Linked In and beyond. <a href="<?php echo SITE_URL?>members/premium_membership" title="Premium Membership">Learn More</a>.</p>
                   </div>
                   <div class ="expertise">
                    <h2 style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">3. <a href="<?php echo SITE_URL?>members/premium_website"style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;" title="Premium Website">Premium Website</a></h2>
                    <p style="font-size:16px;">Get more visitors on your business website, increase their engagement level and convert them to prospective clients. <a href="<?php echo SITE_URL?>members/premium_website" title="Premium Website" >Learn More</a>.</p>
                   </div>

                   <div class ="expertise">
                    <h2 style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">4. <a href="<?php echo SITE_URL?>project/create" style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;" title="Post a Project">Post a Project</a></h2>
                    <p style="font-size:16px;">Subcontract projects to your fellow member consultants. <a href="<?php echo SITE_URL?>project/create" title="Post a Project">Learn More</a>.</p>
                   </div>
                   <?php if($this->Session->read('Auth.User.id') !='' && $this->Session->read('Auth.User.role_id') == '2') {?>
                   <div class ="expertise">
                    <h2 style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">5. Special Features & Partner Discounts</h2>
                    <ul>
                    
                    <li style="font-size:16px;margin-top:12px;">a. Access business frameworks, templates and financial models from <span style="font-weight:bold;">Flevy</span>. <a href="<?php echo SITE_URL?>members/flevy" title="Flevy documents">Learn More</a>.</li>
                    <li style="font-size:16px;">b. Free up your valuable time spent in doing online research. Get high-quality, tailor-made research from <span style="font-weight:bold;">InsightBee</span>. <a href="https://www.insightbee.com/resources/mentorsguild" title="InsightBee Offers" target="_blank">Learn More</a>.</li>
                    <li style="font-size:16px;">c. A cloud-based platform for rapid client discovery and analysis from <span style="font-weight:bold;">9Lenses</span>. <a href="<?php echo SITE_URL?>members/nine_lenses" title="Nine-Lenses">Learn More</a>.</li>
                    <li style="font-size:16px;">d. Badges, email signature, etc. <a href="<?php echo SITE_URL?>members/my_badges" title="Badges">Learn More</a>.</li>
                    <li style="font-size:16px;margin-bottom:20px;">e. Attach a call-to-action to every link you share with your followers. <a href="<?php echo SITE_URL?>members/takeaway_input" title="Give takeaway">Learn More</a>.</li>
                    </ul>
                    
                   </div>
                   <?php }else{?>
                   <div class ="expertise">
                    <h2 style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">5. Special Features & Partner Discounts</h2>
                    <ul>
                    
                    <li style="font-size:16px;margin-top:12px;">a. Access business frameworks, templates and financial models from <span style="font-weight:bold;">Flevy</span>. <a href="<?php echo SITE_URL?>members/flevy" title="Flevy documents">Learn More</a>.</li>
                    <li style="font-size:16px;">b. Free up your valuable time spent in doing online research. Get high-quality, tailor-made research from <span style="font-weight:bold;">InsightBee</span>. <a href="https://www.insightbee.com/resources/mentorsguild" title="InsightBee Offers" target="_blank">Learn More</a>.</li>
                    <li style="font-size:16px;">c. A cloud-based platform for rapid client discovery and analysis from <span style="font-weight:bold;">9Lenses</span>. <a href="<?php echo SITE_URL?>members/nine_lenses" title="Nine-Lenses">Learn More</a>.</li>
                    <li style="font-size:16px;">d. Badges, email signature, etc. <a href="javascript:void(0)" onclick='javascript:loginpopup();' title="Badges">Learn More</a>.</li>
                    <li style="font-size:16px;margin-bottom:20px;">e. Attach a call-to-action to every link you share with your followers. <a href="<?php echo SITE_URL?>members/takeaway_input" title="Give takeaway">Learn More</a>.</li>
                    </ul>
                   </div>
                     <?php }?>
                   <div class ="expertise">
                    <h2 style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;">6. <a href="<?php echo SITE_URL?>members/invitation_eligibility" style="color:#333333;font-size: 18px;font-family: 'proximanova semibold',Ubuntu;" title="Invite a member">Send membership invitations</a></h2>
                    <p style="font-size:16px;">Invite other qualified consultants to join Guild. <a href="<?php echo SITE_URL?>members/invitation_eligibility" title="Invite a member">Learn More</a>.</p>
                   </div>
                  <br/>
                  <div class="mg_quote4_container" onclick="window.open('<?php echo(SITE_URL);?>chip.evans','mywindow');" style="cursor: hand;">
		    <div class="mg_photo">
		        <img src="<?php echo(SITE_URL)?>/imgs/ChipEvans.jpg" alt="Chip Evans">
		    </div>

		    <div class="mg_quote_box">

			    <div class="premium-testimonial-content">
				<div class="premium-testimonial2-quote">Guild provided my consulting company, The Evans Group LLC, with an opportunity to work with and consult for Mr. Dave Evans (no relation to our firm), who contracted with me for counsel on startup companies, mergers & acquisitions, and venture & angel capital infusion.</br></br>Guild was extraordinary in how they represented the client looking for consulting guidance and in organizing and following up to ensure the consulting times and description of work were clear.</br>
</br>I'd highly recommend working with Guild and am very pleased with our relationship.</div>
				<div class="premium-testimonial2-name">Chip Evans, Ph.D</div>
				<div class="premium-testimonial2-org">President & Founder at The Evans Group LLC</div>
		    </div></div>
		</div>



</div>
</div>
</div>
</div>


<?php echo($this->Html->css(array('plans'))); ?>
























