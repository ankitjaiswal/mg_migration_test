<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="google-site-verification" content="UroIkuqOwc9A-KngERWfg2Mam9Fw_0UmVECjjujMYMY" />
        <script type="text/javascript">var NREUMQ=NREUMQ||[];NREUMQ.push(["mark","firstbyte",new Date().getTime()]);</script>
	 <meta name="format-detection" content="telephone=no" />	
        <link rel="icon" href="/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

        </head>

<div class="premium-content one">
    <div class="pagewidth">
        <img src ="<?php echo(SITE_URL)?>/imgs/stars.png">
        <h1>Premium Membership</h1>

		<h2>Spend more time with clients while we increase your visibility, promotion, and leads.</h2>
    </div>
</div>

<div class="premium-content two dark">
    <div class="pagewidth">
    <div class="float-l"><img src="<?php echo(SITE_URL)?>/imgs/profile.png" style="padding-top:4px;"></div>
    <h3>Make a Great First Impression</h3>
    <p>Premium membership helps you stand out in a crowded marketplace.</p>

    <ul class="premium-list">
        
        <li class="">Gain prominence in our search listings.</li>
        <li class="">Get more exposure as a featured member on our homepage.</li>
<li class="">Personalize your profile with a <a href="#" onclick="TINY.box.show({iframe:'//www.youtube.com/embed/otq68WCXnjE',width:860,height:460,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">short video introduction</a>.</li>
        <li class="">Keep your profiles optimized. Linked In, Mentors Guild, resume... you get the picture.</li>
    </ul>
</div>
</div>

<div class="premium-content three">
    <div class="pagewidth">
		<div class="float-r"><img src="<?php echo(SITE_URL)?>/imgs/promotion.png"></div>
        <h3>Amplify Your Marketing</h3>
        <p>Online marketing is important, but it takes time away from your clients. Premium Membership fills that gap.</p>
        <ul class="premium-list">
            <li class="">Connect with journalists looking for your expertise.</li>
            <li class="">Send monthly press releases to Google News and major search engines.</li>
            <li class="">Get more mileage from your content. We’ll promote it on our social media channels, blog and partner sites.</li>
            <li class="">Improve your search engine ranking with our SEO expertise.</li>

        </ul>
    </div>
</div>

<div class="premium-content four dark">
    <div class="pagewidth">
        <div class="float-l"><img src="<?php echo(SITE_URL)?>/imgs/prospecting.png"></div>
		<h3>Fill Your Funnel</h3>
        <p>Relationships and consistency drive sales. We help you achieve both through professionally managed email campaigns.</p>
        <ul class="premium-list">
            <li class="">Leverage best practices in email marketing and newsletters.</li>
            <li class="">Focus your prospecting efforts on those who engage with the campaign.</li>
            <li class="">We’ll work with you on special promotions and even make first contact on your behalf.</li>
        </ul>
    </div>
</div>

<div class="premium-content five">
    <div class="pagewidth">
	<h3 class="header">Grow Your Business</h3>
        <p>Stand out among your peers, extend your reach, and shorten your sales cycle.</p>




        
	    <div class="mg_quote1_container" onclick="window.open('<?php echo(SITE_URL);?>mark.palmer','mywindow');" style="cursor: hand;">
		    <div class="mg_photo">
		        <img src="<?php echo(SITE_URL)?>/imgs/mark_palmer.jpg">
		    </div>
		    <div class="mg_quote_box">

			    <div class="premium-testimonial-content">
				<div class="premium-testimonial-quote">By listing only the best consultants Mentors Guild is well on its way of marketing to businesses from a place of trust.</div>
				<div class="premium-testimonial-name">Mark Palmer</div>
				<div class="premium-testimonial-org">Founder/ Principal, Focus LLC</div>
		    </div></div>
		</div>
          <?php /**
                <div class="mg_quote2_container" onclick="window.open('<?php echo(SITE_URL);?>roza.rojdev','mywindow');" style="cursor: hand;">
		    <div class="mg_photo">
		        <img src="<?php echo(SITE_URL)?>/imgs/Roza.png">
		    </div>
		    <div class="mg_quote_box">

			    <div class="premium-testimonial-content">
				<div class="premium-testimonial2-quote">As a partner to Xecutive Metrix, the Mentors Guild team is offering services that help our firm increase our level of exposure to new markets, connect us with other value added consultants in the network and provide us with peace of mind that our marketing efforts are running smoothly. Now we can focus on what we do best&mdash;Executive Leadership Development.</div>
				<div class="premium-testimonial2-name">Roza S. Rojdev, PsyD</div>
				<div class="premium-testimonial2-org">Managing Partner at Xecutive Metrix</div>
		    </div></div>
		</div>
               **/?>                
           
               

<br/><br/>
              <?php if($this->Session->read('Auth.User.role_id') == 2 && $this->Session->read('Auth.User.mentor_type') != 'Premium Member')
              { ?> 
		<div class="mg-button"><a href="<?php echo(SITE_URL)?>pricing/striperecurr/">Start Your Premium Membership</a></div>
              <?php } elseif($this->Session->read('Auth.User.role_id') == 2 && $this->Session->read('Auth.User.mentor_type') == 'Premium Member')
             {?>
             <div class="mg-button"><a href="<?php echo(SITE_URL)?>pricing/striperecurrcancel/">Cancel Your Premium Membership</a></div>
             <?php }else{?>
             <div class="mgbutton">CALL 1-866-511-1898 FOR INFORMATION</div>
           <?php }?>
        <div class="signup" style="margin-top: 50px"><p>Introductory price is <span class="emphasize">$45 per month</span> (no contract) and Mentors Guild fee per engagement is reduced to <span class="emphasize">just 7.5%</span>.</p></div>
    </div>
</div>


<div class="six">
    <div class="pagewidth">

	</div>
	<div class="six">
        <div class="mg-link">Have questions or suggestions about Premium Membership? Call us at 1-866-511-1898.</div>
        </div>
    </div>
</div>
