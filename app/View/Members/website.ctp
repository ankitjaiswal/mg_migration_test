<div class="container-fluid" style="margin-top: 90px;overflow-x:hidden">
	<div class="row">
		<div class="hero-background well-lg" >
			<div class="container well-lg">
				<div class="col-md-6 well-lg">
					<h2 style="margin-top: 10px;">
					<p class="support1"><span class="bold">Digital Marketing</span> for<br> Business Consultants</p>
					</h2>
					<p class="support">	
					Expand your digital presence and earn<br> new customer with a <span class="bold1">custom website</span> and<br> <span class="bold1">marketing support</span> from the <span class="bold1">industry experts</span>
					</p>
					<img style="width: 50%;" class="mobile-banner" src="<?php echo(SITE_URL)?>sanchit/images/banner-left2.png" />
					<br> 
                                     <?php if($this->Session->read('Auth.User.id') == '')
                                      { ?>
					<button class="btn-lg btn-danger" class="mymodal" data-toggle="modal" data-target="#signin-Modal">Get Started</button>
                                      <?php }elseif(($this->Session->read('Auth.User.role_id') == 2 || $this->Session->read('Auth.User.role_id') == 3) && $this->data['User']['premium_website'] != 1){?>
                                         <button class="btn-lg btn-danger" onClick="window.location.href='<?php echo(SITE_URL)?>members/striperecurr/';">Get Started</button>
                                      <?php }?>
                                      
				</div>
				<div class="col-md-6 center-block well-lg">
					<img class="banner-left" src="<?php echo(SITE_URL)?>sanchit/images/banner-left2.png" />
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
<!--testimonial-start-->
<div id="" class="testimonial-section">
<div class="container">
<div class="row">
<div class="col-md-10 col-md-offset-1">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
<div class="carousel-inner" role="listbox">
<div class="item active">
<div class="tesimonial-part">
<div class="person-img">
<img src="<?php echo(SITE_URL)?>/imgs/Mike-Dorman-Guild.jpg" alt="Mike Dorman">
</div>
<div class="person-desc">
<p>
Website management is not my skill, but I was often bothered by what I knew I should be doing to maintain it vs. what I did. Working with GUILD has been outstanding. GUILD redesigned the site to take advantage of the latest features. The best is the monthly meeting to together evaluate visitor traffic and discuss changes. I count this as one of my better decisions as a business person.
</p>
<label>Mike Dorman</label>
<span>President at The Third Zone LLC</span>
</div>
</div>
</div>
<div class="item">
<div class="tesimonial-part">
<div class="person-img">
<img src="<?php echo(SITE_URL)?>/imgs/Ed-Stephenson-GUILD.png" alt="Edward Stephenson" style="width:150px;height:150px;">
</div>
<div class="person-desc">
<p id="edcomment">
It's great! It's very cost-effective.
</p>
<label>Edward Stephenson</label>
<span>Healthcare Consultant </span>
</div>
</div>
</div>
<div class="item">
<div class="tesimonial-part">
<div class="person-img">
<img src="<?php echo(SITE_URL)?>/imgs/Judy-Bardwick-Guild.png" alt="Judy Bardwick">
</div>
<div class="person-desc">
<p>
I am delighted and amazed by the work GUILD has done for me. It is personal, accurate, honest and original. The website they created is client-focused, eye-catching, and simultaneously captures the essence of what I deliver to Clients. And, the speed of the team's responsiveness and their attention to every detail is the best I have ever experienced.
</p>
<label>Judy Bardwick</label>
<span>Organizational Psychologist</span>
</div>
</div>
</div>
</div>
<a class="left-arrow button" href="#carousel-example-generic" role="button" data-slide="prev">
<span><img src="<?php echo(SITE_URL)?>/yogesh_new1/images/testimonial-left-arrow.png"></span>
<span class="sr-only">Previous</span>
</a>
<a class="right-arrow button" href="#carousel-example-generic" role="button" data-slide="next">
<span><img src="<?php echo(SITE_URL)?>/yogesh_new1/images/testimonial-right-arrow.png"></span>
<span class="sr-only">Next</span>
</a>
</div>
</div>
</div>
</div>
</div>
<!--testimonial-stop-->
		<div class="choose-guild row">
			<div class="container">
				<p class="text-center text-uppercase"style="font-size: 40px;margin-top: 50px;"> monthly service package includes</p>
				<hr class="head-hr" align="center" width="20%">
				<div class="col-md-4">
					<div class="text-center">
					<img  src="<?php echo(SITE_URL)?>sanchit/images/digitalPresence.png" class="block-image" />
					</div>
					<h4 class="text-center">Website Development</h4>
					<hr>
					<p style="padding: 0px 38px 0 60px;">We craft websites for experienced business consultants that drive results. You get our indepth industry insight and best practices in an affordable monthly service.</p>
				</div>
				<div class="col-md-4">
					<div class="text-center">
					<img  src="<?php echo(SITE_URL)?>sanchit/images/support1.png" class="block-image" />
					</div>
					<h4 class="text-center">Marketing Strategy</h4>
					<hr>
					<p style="padding: 0px 38px 0 60px;">With monthly strategy calls we keep you upto date on the metrics that impact your online marketing and help you make informed decisions that benefit your practice.</p>
				</div>
				<div class="col-md-4">
					<div class="text-center">
					<img  src="<?php echo(SITE_URL)?>sanchit/images/pricing.png" class="block-image" />
					</div>
					<h4 class="text-center">Support & Maintenance</h4>
					<hr>
					<p style="padding: 0px 38px 0 60px;">Always up-to-date and secure. We handle all the maintenace that is necessary to keep your website up and running.</p>
					
				</div>

			</div>
		</div>
		<div class="row guild-screens">
			<div class="container">
				<p class="text-center text-uppercase"style="font-size: 40px;margin-top: 50px;"> websites that look great on all <br>size screens</p>
				<hr class="head-hr" align="center" width="20%">
				<div class="all-device-image text-center">
					<img src="<?php echo(SITE_URL)?>sanchit/images/alld.png" />
				</div>
			</div>	
		</div>
		<div class="row guild-features">
			<div class="container well-lg">
				<p class="text-center text-uppercase"style="font-size: 40px;margin-top: 50px;">features</p>
				<hr class="head-hr" align="center" width="20%">
				<div class="text-center">
					<p><b style="font-size:26px;">Focus your valuable time on your clients and let us help manage your online presence.</b></p>
					<div class="feature-points">
						<ul class="col-md-6 text-left">
							<li>Mobile responsive</li>
							<li>Secure hosting with full backups</li>
							<li>SSL security certificates</li>
							<li>Keyword optimized for search engines</li>
							<li>Social media integration</li>
						</ul>
						<ul class="col-md-6 text-left">
							<li>Dedicated account specialist</li>
							<li>Ongoing maintenance and support</li>
							<li>Unlimited change requests</li>
							<li>Monthly strategy reports</li>
							<li>Zero startup fees</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="library-guild row" >
			<div class="container">
				<p class="text-center text-uppercase" style="font-size:40px">jump start your design from our<br> extensive library of templates</p>
				<hr class="head-hr">
				<div class="text-center matched">
					<div class="text-center">
						<img src="<?php echo(SITE_URL)?>sanchit/images/templates3.png" />
					</div>	

                                     <?php if($this->Session->read('Auth.User.id') == '')
                                      { ?>
					<button class="btn-lg btn-danger" class="mymodal" data-toggle="modal" data-target="#signin-Modal">Start Your 30-day FREE Trial Now!</button>
                                      <?php }elseif(($this->Session->read('Auth.User.role_id') == 2 || $this->Session->read('Auth.User.role_id') == 3) && $this->data['User']['premium_website'] != 1){?>
                                         <button class="btn-lg btn-danger" onClick="window.location.href='<?php echo(SITE_URL)?>members/striperecurr/';">Start Your 30-day FREE Trial Now!</button>
                                      <?php }else{?>
                                        <button class="btn-lg btn-danger" onClick="window.location.href='<?php echo(SITE_URL)?>members/cancel_plan';">Cancel Your Premium Website</button>
                                      <?php }?>				
				</div>
			</div>
		</div>
		<div class="row schedule-guild">
			<div class="container well-lg">
				<h3 class="text-center">Need something special or have a question? Let's chat.<br> <span class="red"><a href="https://calendly.com/guild/15-min" target="_blank">Click HERE to schedule an appointment time</a> </span> that works for you.</h3> 
			</div>
		</div>
	</div>
</div>

<style>
.hero-background
{
background:url('https://www.guild.im/sanchit/images/heroimage.png');
background-repeat: no-repeat;

background-size: cover;
height:470px;
}
ul li {
    list-style-type: disc;
}
.Guild-list-center li {

    font-size: 14px;

}
.Guild-list-center
{
margin: 0 auto;
left: 0;
right: 0;
display: inline-block;
padding-left: 55px;
padding-bottom: 120px;
}
.red
{
color : #cf3135
}
.bold
{
font-weight:bold;
}
.head-hr
{
border-top:6px solid #000;
width: 19%;
display: block;
margin: 0 auto;
}
.choose-guild.row .col-md-4 hr {
    border-top: 5px solid #7a7a7a;
	width: 70%;

margin-left: 17%;
}
.choose-guild.row {
    background: #f4f4f4;
padding-bottom: 131px;
}
.choose-guild.row .head-hr
{
margin-bottom: 90px;
}
.matched p {
    margin: 25px 0 78px 0px;
}
#footer-section ul li
{
	list-style-type:none;
}
.block-image
{
width:140px;
}
.banner-left
{
padding-top: 35px;
width:390px;
padding-left: 50px;
margin-left: 90px;
}
.library-guild
{
background-color:#f4f4f4;
padding-bottom:60px;
padding-top: 40px;
border-top:1px solid #c7c2c2;
}
.row.schedule-guild {
    padding-top: 100px;
    padding-bottom: 110px;
}
.all-device-image
{
margin-top:50px;
}
.feature-points
{
padding:30px 5%;
left: 0;
right: 0;
margin: 0 auto;
display: inline-block;
}
.row.guild-features {

    padding-bottom: 70px;

}
.testimonial-section .carousel
{
position: relative;

margin: 45px 0 0;

padding: 0 0 45px;
}
.row.guild-features .head-hr {

    margin-bottom: 40px;

}
.btn-lg.btn-danger {

    background-color: #cf3135;
	padding: 10px 25px;
	border: none;
}
.library-guild.row img {
    margin-top: 30px;
    margin-bottom: 30px;
}
h1, h2, h3, h4, h5, h6, .btn {

    font-family: opensansregular !important;

}
.mobile-banner
{
display:none;
}
@media only screen and (max-width: 640px)
{
.mobile-banner
{
display:block;
padding:5px 0px;
width:80% !important;
}
.hero-background .col-md-6.center-block.well-lg
{
display:none;
}
.hero-background, .hero-background .well-lg
{
padding-top:15px !important;
}
.hero-background h2
{
font-size:22px;
}
.text-center.text-uppercase
{
font-size:35px !important;
}
.container-fluid
{
padding-left:20px;
padding-right:20px; 
}
.bold {
    font-weight: bold;
   /* color: #cf3135;*/
    font-size: 20px!important;
    line-height: unset!important;
}
.bold1 {
    font-weight: bold;
    /*color: #cf3135;*/
    font-size: 12px!important;
    
}
p.support {
    font-size: 12px!important;
    margin-top: 0px!important;
}
.support1{
	font-size: 20px!important;
}
.hero-background {
    background: url(https://www.guild.im/sanchit/images/heroimage.png);
    background-repeat: no-repeat;
    /* background-size: cover; */
    height: 470px;
    background-position-x: center;
    background-position-y: top;
}
}
.row.schedule-guild h3 {

    line-height: 37px;

}
.feature-points ul li {
    font-size: 20px;
}
.bold {
    font-weight: bold;
   /* color: #cf3135;*/
    font-size: 42px;
    line-height: 2;
}
.bold1 {
    font-weight: bold;
   /* color: #cf3135;*/
    font-size: 20px;
    
}
p.support {
    font-size: 20px;
    margin-top: 40px;
}
.support1{
	font-size: 41px;
}
@media only screen and (min-width:641px) and (max-width: 990px)
{
.mobile-banner
{
display:block;
padding:5px 0px;
}
.hero-background .col-md-6.center-block.well-lg
{
display:none;
}
.hero-background {
    background: url(https://www.guild.im/sanchit/images/heroimage.png);
    background-repeat: no-repeat;
    /* background-size: cover; */
    height: 630px;
    background-position-x: center;
}
}


</style>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>


<script type="text/javascript">
	    jQuery(document).ready(function(){
		    jQuery('.t-testimonial').slick({
		      dots: true,
		      arrows: false,
		      infinite: true,
		      speed: 300,
		      autoplaySpeed: 8000,
		      autoplay: true,
		      slidesToShow: 1,
		      adaptiveHeight: true,
		      draggable: false
		    });
		});
	</script>