<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="fieldset">
    <h3 class="legend">
		List
        <div class="total" style="float:right">Total Pending Members : <?php echo($this->request["paging"]['MentorApply']["count"]); ?>
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <input type="hidden" name="pageAction" id="pageAction"/>	 
        <?php
        if (!empty($data)) {

            $this->ExPaginator->options = array('url' => $this->passedArgs);
            $this->Paginator->options(array('url' => $this->passedArgs));
            ?> 

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">	
                        <td width="30%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo($this->ExPaginator->sort( 'MentorApply.firstname','Name')) ?></td>
                        <td width="30%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo($this->ExPaginator->sort('MentorApply.email','Email')) ?></td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo($this->ExPaginator->sort('MentorApply.submitted','Created Date')) ?></td>
						<td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo($this->ExPaginator->sort('MentorApply.isApproved','Status')) ?></td>                       
					   <td width="10%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Action</td>
                    </tr>	
                    <?php
                    foreach ($data as $value) {
                        ?>
                        <tr>		
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php echo $this->Html->link(ucwords($value['MentorApply']['firstname']) . ' ' . $value['MentorApply']['lastname'], array('controller'=>'members','action'=>'detail',$value['MentorApply']['id']),array('escape'=>false, "style" => "text-decoration:underline;")); ?></td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php echo($value['MentorApply']['email']); ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php echo($value['MentorApply']['submitted']); ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php 
								if($value['MentorApply']['isApproved'] ==1){
									echo('Approved');
								}elseif($value['MentorApply']['isApproved'] ==2){
									
									echo('Declined');
								}elseif($value['MentorApply']['isApproved'] ==0){
									echo('Pending');
								}else{
									echo('Undefined');
								}								
								?>
                            </td>							
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php
                                   if($value['MentorApply']['isApproved'] ==0){
                                    echo $this->Html->link($this->Html->image("MailIcon.png", array("title" => "Accept Member Request", "alt" => "mail", "border" => 0)),array('controller'=>'members','action'=>'activateMentor',$value['MentorApply']['id']),array('escape'=>false));
                                    
										echo $this->Html->link($this->Html->image("erase01.png", array("title" => "Decline Member Request", "alt" => "mail", "border" => 0)),array('controller'=>'members','action'=>'declineMentor',$value['MentorApply']['id']),array('escape'=>false));
                					}
									
				                ?>
                            </td>
                        </tr>	
                        <?php
                    }
                    ?>
                </table>
            </div>
            <?php
        }else{?>
			No Records Found
			<?php
		}?>

    </div>
</div>
<div class="clr"></div> 