<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
   


<div id="content-submit" class="content_sunbmit eligible-section"> 
    <div class="container">
        <h1>Who is eligible to be a member?</h1>
        <div id="text-content" class="text_content"><!--VL 24/12-->
                <p>GUILD offers its member consultants a premium online marketing platform, and facilitates consulting engagements in a high-trust environment. <a href="https://www.guild.im/members/advantage" target="_blank">Here is an overview</a>.</p>
                <p>Membership is open to seasoned business consultants and executive coaches. We invite those who meet the following criteria.</p>
            <br />
            <div class="text-content_row">
                <div class="text-content_left"><span>Experience</span></div>
                <div class="text-content_right">
                    <p>Two decades or more of relevant professional experience, at least five years of which is in working as a business advisor, an executive coach, or a management consultant.</p>
                </div>
            </div>
            <div class="text-content_row">
                <div class="text-content_left"><span>Publications</span></div>
                <div class="text-content_right">
                    <p>Contributions to books, journals, leading magazines, noteworthy articles or respected industry blogs.</p>
                </div>
            </div>
            <div class="text-content_row">
                <div class="text-content_left"><span>Others</span></div>
                <div class="text-content_right">
                    <p>A Masters or higher level of education. Relevant professional certifications and credentials are a plus, but not required.</p>
                </div>
            </div>
            <div class="text-content_row">
                <p>Currently, we accept consultant members <span style="text-decoration: underline;">in US only</span>.</p>
                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                <p style="text-align: center; font-weight: bold;">PLEASE ONLY INVITE INDIVIDUALS WHO MEET THESE CRITERIA</p>
            </div>
            <div id ="app-button" class="invitation-btn"> 
                <?php  echo($this->Html->link('Cancel',array('controller'=>'users','action'=>'my_account'),array('class'=>'backRes'))); ?>

                <?php if($this->Session->read('Auth.User.id') ==''){?>
                      <input type="button" value="Send Invitation" data-toggle="modal" data-target="#signin-Modal">
                    <?php }else{?>
                      
                        <input type="button" value="Send Invitation" data-toggle="modal" data-dismiss="modal" data-target=".inviteamembercolleague">                  
                 <?php }?>  
                    
            </div>
        </div>
    </div>
</div>

	


