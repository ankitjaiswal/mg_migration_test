             <?php $imagelink = (SITE_URL."img/members/profile_images".'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name']);
			 
			  $modeTypeFaceToFace=$modeTypePhone=$modeTypeVideo='FALSE' ;
			  $modeValue1=$modeValue2=$modeValue3='';
			  if(isset($this->data['Communication'][0]['mode_type']) && $this->data['Communication'][0]['mode_type'] != '')
			  {
			  		
					if($this->data['Communication'][0]['mode_type']=='face_to_face')
				{
						$modeTypeFaceToFace='TRUE';
			  			$modeValue1=$this->data['Communication'][0]['mode'];
					}
					else if($this->data['Communication'][0]['mode_type']=='phone')
						{
						  $modeTypePhone='TRUE';
			  			  $modeValue2=$this->data['Communication'][0]['mode'];
			  			}
			  			
			  			else
			  			{
			  			   $modeTypeVideo='TRUE';
			  			   $modeValue3=$this->data['Communication'][0]['mode'];
					 }
					
	
				
			}
			  if(isset($this->data['Communication'][1]['mode_type']) && $this->data['Communication'][1]['mode_type'] != '')
			  {
			  		
					if($this->data['Communication'][1]['mode_type']=='face_to_face')
				{
						$modeTypeFaceToFace='TRUE';
			  			$modeValue1=$this->data['Communication'][1]['mode'];
					}
					else if($this->data['Communication'][1]['mode_type']=='phone')
						{
						   $modeTypePhone='TRUE';
			  		       $modeValue2=$this->data['Communication'][1]['mode'];
			  			}
			  			
			  			else
			  			{
			  			   $modeTypeVideo='TRUE';
			  			   $modeValue3=$this->data['Communication'][1]['mode'];
					 }
					
		
				
			}
			 if(isset($this->data['Communication'][2]['mode_type']) && $this->data['Communication'][2]['mode_type'] != '')
			{
			  		if($this->data['Communication'][2]['mode_type']=='face_to_face')
					{
						$modeTypeFaceToFace='TRUE';
			  			$modeValue1=$this->data['Communication'][2]['mode'];
					}
						else if($this->data['Communication'][2]['mode_type']=='phone')
						{
						   $modeTypePhone='TRUE';
			  		       $modeValue2=$this->data['Communication'][2]['mode'];
			  			}
			  			
			  			else
			  			{
			  			   $modeTypeVideo='TRUE';
			  			   $modeValue3=$this->data['Communication'][2]['mode'];
					 }
					
					
			}
			
			

			

					$socialCount=0;
					$linkedinURL = '';
                                   $twitterURL ='';
                                   $googleURL ='';
                                   $facebookURL ='';
                                   $youtubeURL ='';
					while(count($this->data['Social'])>$socialCount)
					{
					
					    if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'http'))!==false)
						{
							$link=$this->data['Social'][$socialCount]['social_name'];
						}
						else
						{
							$link="https://".$this->data['Social'][$socialCount]['social_name'];
						}
						
						$icon = $this->General->getIcon($this->data['Social'][$socialCount]['social_name']);
						if($icon == 'media/linkedin.png')
							$linkedinURL = $link;
                        if($icon == 'media/twitter.png')
							$twitterURL = $link;
                        if($icon == 'media/Google+.png')
							$googleURL = $link;
                        if($icon == 'media/facebook.png')
							$facebookURL = $link;
						if($icon == 'media/youtube.png')
							$youtubeURL = $link;
						$socialCount++;
					}?>


<div class="member-content">

	<!-- Member Advantage Begin -->
	<div class="member-advantage">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="member-detail">
						<h2>Member Advantage</h2>
						<span>GUILD connects 8,000+ member consultants to business opportunities across the nation.</span>
                                                <?php if($this->Session->read('Auth.User.id') ==''){?>
						<div class="button-set">
							<a href="<?php echo SITE_URL;?>members/eligibility" target="_blank" class="btn btn-primary">Apply for consultant membership</a>
						</div>
                                               <?php }?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Member Advantage Close -->

	<!-- Membership Benefits Begin -->
	<div class="md-p-100 membership">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="member-detail title">
						<h2>GUILD membership for consultants</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="mb-detail-left">
						<p>GUILD membership is open to seasoned industry experts, executive coaches and other business consultants. Membership is free and by-invitation only.</p>
						<p>Client organizations come to us with complex problems that require specialized expertise. GUILD connects these clients to relevant members, and works collaboratively with both parties to create effective consulting engagements. GUILD staff stays involved in every project to the extent needed for the best possible outcome for both &#8212; our client and the member consultant. For its services, GUILD retains a variable percentage of every engagement based on its degree of involvement, typically around 30% of the total engagement fee.</p>
						<p>In addition to delivering consulting projects, GUILD directly helps its member consultants market their expertise online, combining their years of domain experience with our experience in digital marketing of consulting services. Some of these services come with the free GUILD membership and others are available for a fee.</p>
					</div>
				</div>

			</div>
                       <?php if($this->Session->read('Auth.User.id') ==''){?>
			<div class="row">
				<div class="button-set">
					<a href="<?php echo SITE_URL;?>members/eligibility" target="_blank" class="btn btn-primary">Apply for consultant membership</a>
				</div>
			</div>
                       <?php }?>
		</div>
	</div>
	<!-- Membership Benefits Close -->

		<!-- testimonial section -->
		 <div id="" class="testimonial-section">
			<div class="container">
				<div class="testimonial-heading text-center">
					<h2>Testimonials</h2>
					<p>What our member consultants are saying</p>
				</div>
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner" role="listbox">
								<div class="item active">
									<div class="tesimonial-part">
										<div class="person-img">
											<img src="<?php echo(SITE_URL)?>yogesh_new1/images/Judy-bardwick.jpg" alt="Judy bardwick">
										</div>
										<div class="person-desc">
											<p>
												I am delighted and amazed by the work GUILD has done for me. It is personal, accurate, honest and original. The website they created is client-focused, eye-catching, and simultaneously captures the essence of what I deliver to Clients. And, the speed of the team's responsiveness and their attention to every detail is the best I have ever experienced.
											</p>
											<label>Judy Bardwick</label>
											<span>Leading Organizational Psychologist</span>
										</div>
									</div>
								</div>
								<div class="item ">
									<div class="tesimonial-part">
										<div class="person-img">
											<img src="<?php echo(SITE_URL)?>/imgs/Mark-Palmer-Guild1.jpg" alt="Mark Palmer">
										</div>
										<div class="person-desc">
											<p id="markcomment">
												By listing only the best consultants GUILD is well on its way of marketing to businesses from a place of trust.
											</p>
											<label>Mark Palmer</label>
											<span>Founder/ Principal, Focus LLC</span>
										</div>
									</div>
								</div>
								<div class="item ">
									<div class="tesimonial-part">
										<div class="person-img">
											<img src="<?php echo(SITE_URL)?>/imgs/Chip-Evans-Guild.jpg" alt="Chip Evans">
										</div>
										<div class="person-desc">
											<p id="chipcomment">
												GUILD was extraordinary in how they represented us to the client looking for consulting guidance, and in organizing and following up to ensure the consulting times and description of work were clear.</br>I'd highly recommend working with GUILD and am very pleased with our relationship.
											</p>
											<label>Chip Evans, Ph.D</label>
											<span>President & Founder at The Evans Group LLC</span>
										</div>
									</div>
								</div>
								<div class="item ">
									<div class="tesimonial-part">
										<div class="person-img">
											<img src="<?php echo(SITE_URL)?>/imgs/Roza-Rojdev-Guild.png" alt="Roza Rojdev">
										</div>
										<div class="person-desc">
											<p>
												As a partner to Xecutive Metrix, the GUILD team is offering services that help our firm increase our level of exposure to new markets, connect us with other value added consultants in the network and provide us with peace of mind that our marketing efforts are running smoothly. Now we can focus on what we do best&mdash;Executive Leadership Development.
											</p>
											<label>Roza S. Rojdev, PsyD</label>
											<span>Managing Partner at Xecutive Metrix</span>
										</div>
									</div>
								</div>
							</div>

							<a class="left-arrow button" href="#carousel-example-generic" role="button" data-slide="prev">
								<span><img src="<?php echo(SITE_URL)?>/yogesh_new1/images/testimonial-left-arrow.png"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right-arrow button" href="#carousel-example-generic" role="button" data-slide="next">
								<span><img src="<?php echo(SITE_URL)?>/yogesh_new1/images/testimonial-right-arrow.png"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div> 


		<!-- testimonial section End-->

	<!-- Features begin -->
	<div class="md-p-100 member-feature" id="features">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="member-detail mb-title">
						<h2>Features and partnerships</h2>
						<p>Marketing and productivity tools for experienced consultants.</p>
					</div>
				</div>
			</div>
			<div class="row flex">
	<?php if(($this->Session->read('Auth.User.id') !='' && $this->Session->read('Auth.User.id') == $this->data['User']['id'])){?>			
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="feature-img ninety-img">
							<img src="<?php echo($imagelink);?>" alt="<?php echo(ucfirst($this->data['UserReference']['first_name']));?> <?php echo($this->data['UserReference']['last_name']); ?>">
						</div>
							
						<div class="feature-detail">
							<h4><?php echo(ucfirst($this->data['UserReference']['first_name']));?> <?php echo($this->data['UserReference']['last_name']); ?></h4>
							<a href="mailto:<?php echo($this->data['User']['username']);?>"><?php echo($this->data['User']['username']);?></a>
							<p><?php echo(ucfirst($this->data['UserReference']['linkedin_headline']));?><br><?php if($modeTypePhone=='TRUE' && trim($modeValue2)!='') {?><?php echo $modeValue2; ?> |<?php }?> <?php if($city != '') {?><?php echo($city) ;?>, <?php echo($state) ;?><?php }?><br>

							</p>
							<ul class="socialul">

                                                        <?php if(($this->data['UserReference']['business_website']) !=''){
                                                            $link ='';
                                                            $url ='';
                                                             if(($pos =strpos($this->data['UserReference']['business_website'],'http'))!==false)
                                                                {
                                                                $link=$this->data['UserReference']['business_website'];
                                                                }
                                                                else
                                                                {
                                                                  $link="https://".$this->data['UserReference']['business_website'];
                                                                }?>
                                            
                                                         <li><a class="bussness-link" href="<?php echo($link);?>" target="_blank">
                                                          <img width="24" height="24" src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-business-new.svg" alt="mb-business">
                                                         </a></li>
                                                       <?php }?>
							<?php if($linkedinURL !=''){
                                                         if(($pos =strpos($linkedinURL,'http'))!==false)
						             {
							          $link=$linkedinURL;
						             }
						              else
						             {
							          $link="https://".$linkedinURL;
						             }?>
									 
								<li><a href="<?php echo($link);?>" target="_blank"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-linked1.svg" alt="Linkedin"></a></li>
								<?php }?>
                                                                <?php $urlk1 = ($this->data['User']['url_key']);
						                $displink1=SITE_URL.strtolower($urlk1);
                                                                ?>								
								<li><a href="<?php echo($displink1);?>"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mg-guild-new.svg" alt="GUILD Profile"></a></li>
								<?php if($twitterURL !=''){?>
								<li><a href="<?php echo($twitterURL);?>" target="_blank"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-twitter.svg" alt="Twitter"></a></li>
								<?php }?>
								<?php if($facebookURL !=''){?>
								<li><a href="<?php echo($facebookURL);?>" target="_blank"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-facebook.svg" alt="Facebook"></a></li>
								<?php }?>
								<?php if($googleURL!=''){?>
								<li><a href="<?php echo($googleURL);?>" target="_blank"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-google.svg" alt="Google Plus"></a></li>
								<?php }?>
								<?php if($youtubeURL !=''){?>
								<li><a href="<?php echo($youtubeURL);?>" target="_blank"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-youtube.svg" alt="Youtube"></a></li>
								<?php }?>								
							</ul>
						</div>
						<div class="f-first f-center">
							<p>Your professional email signature. Just copy and paste to your email account.</p>
						</div>
					</div>
				</div>			

	<?php }else {?>	
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="feature-img ninety-img">
							<img src="<?php echo(SITE_URL)?>imgs/John-Baldoni-Guild.jpg" alt="john-baldoni">
						</div>
						<div class="feature-detail">
							<h4>John Baldoni</h4>
							<a href="mailto:john@companyemail.com">john@companyemail.com</a>
							<p>Leadership Development<br>(xxx) xxx-xxxx | Ann Arbor, MI</p>
							
									<?php { 
									   $link = "https://www.linkedin.com/in/jbaldoni";
       	                               //$displink1 = "test.guild.im/john.baldoni";
                                       $twitterURL = "https://twitter.com/johnbaldoni";
                                       $youtubeURL = "https://www.youtube.com/user/jbaldoni52";
                                     } ?>							
							<ul>        
                                                                <li><a href="http://www.johnbaldoni.com/" target="_blank"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-business-new.svg" alt="linked"></a></li>
								<li><a href="<?php echo($link);?>" target="_blank"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-linked1.svg" alt="linked"></a></li>
								<li><a href="https://test.guild.im/john.baldoni" target="_blank"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mg-guild-new.svg" alt="GUILD"></a></li>
								<li><a href="<?php echo($twitterURL);?>" target="_blank"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-twitter.svg" alt="twitter"></a></li>
								<li><a href="<?php echo($youtubeURL);?>" target="_blank"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-youtube.svg" alt="youtube"></a></li>
							</ul>
						</div>
						<div class="f-first f-center">
							<p>Your professional email signature. Just copy and paste to your email account.</p>
						</div>
					</div>
				</div>		
		
		
	<?php }?>			

	<?php if(($this->Session->read('Auth.User.id') !='' && $this->Session->read('Auth.User.id') == $this->data['User']['id'])){?>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="mb-profile">
							<div class="feature-img ninety-img">
									 <?php $profileurl = ($this->data['User']['url_key']);
						               $profileurl=SITE_URL.strtolower($profileurl);
                                      ?>							
								<a href="<?php echo($profileurl);?>" target="_blank"><img src="<?php echo($imagelink);?>" alt="<?php echo(ucfirst($this->data['UserReference']['first_name']));?> <?php echo($this->data['UserReference']['last_name']); ?>">
							</div>
							<div class="feature-detail">
								<h4><a href="<?php echo($profileurl);?>" target="_blank" ><?php echo(ucfirst($this->data['UserReference']['first_name']));?> <?php echo($this->data['UserReference']['last_name']); ?></a></h4>
								<p><?php echo($city) ;?>, <?php echo($state) ;?></p>
								<span>
									<div class="mb-gm-logo">
										<a href="<?php echo($profileurl);?>" target="_blank"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-gluid-member-new3.png" alt="<?php echo(ucfirst($this->data['UserReference']['first_name']));?> <?php echo($this->data['UserReference']['last_name']); ?> profile"></a>
									</div>
									<div class="button-set">
										<a class="btn btn-primary" href="<?php echo($profileurl);?>" target="_blank">View Profile</a>
									</div>
								</span>
							</div>
						</div>
						<div class="f-second f-center">
							<p>Post a badge on your website or blog for branding and prospecting. 
							<?php if($this->Session->read('Auth.User.id') ==''){?>
							<a href="javascript:void(0);" data-toggle="modal" data-target="#signin-Modal" title="Badges">Learn more</a>.
							<?php }else{?>
							<a href="<?php echo(SITE_URL)?>members/my_badges" title="Badges">Learn more</a>.
							<?php }?>
							</p>
						</div>
					</div>
				</div>				
				
	<?php }	else{?>	

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="mb-profile">
							<div class="feature-img ninety-img">
									 <?php $profileurl = "mark.palmer";
						               $profileurl=SITE_URL.strtolower($profileurl);
                                      ?>							
								<a href="<?php echo($profileurl);?>" target="_blank"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-mark-parler.png" alt="Mark Palmer"></a>
							</div>
							<div class="feature-detail">
								<h4><a href="<?php echo($profileurl);?>" target="_blank">Mark Palmer</a></h4>
								<p>Sacramento, CA</p>
								<span>
									<div class="mb-gm-logo">
										<a href="<?php echo($profileurl);?>" target="_blank"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-gluid-member-new3.png" alt="Mark Palmer Profile"></a>
									</div>
									<div class="button-set">
										<a class="btn btn-primary" href="<?php echo($profileurl);?>" target="_blank">View profile</a>
									</div>
								</span>
							</div>
						</div>
						<div class="f-second f-center">
							<p>Post a badge on your website or blog for branding and prospecting. 
							<?php if($this->Session->read('Auth.User.id') ==''){?>
							<a href="javascript:void(0);" data-toggle="modal" data-target="#signin-Modal" title="Badges">Learn more</a>.
							<?php }else{?>
							<a href="<?php echo(SITE_URL)?>members/my_badges" title="Badges">Learn more</a>.
							<?php }?>							
							</p>
						</div>
					</div>
				</div>	

	<?php }?>			

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/f-subcontract-1.png" alt="f-subcontract-1">
							<span>SubContract</span>
						</div>
						<div class="f-third f-center">
							<p>Subcontract to vetted consultants.
							<?php if($this->Session->read('Auth.User.id') ==''){?>
							<a href="javascript:void(0);" data-toggle="modal" data-target="#signin-Modal" title="Sub-Contract a Project">Submit your requirement here</a>.
							<?php }else{?>
							<a href="<?php echo(SITE_URL)?>project/create" title="Sub-Contract a Project">Submit your requirement here</a>.
							<?php }?>
							</p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/f-invite-1.png" alt="f-invite-1">
							<span>Invite colleagues</span>
						</div>
						<div class="f-third f-center">
							<p>Invite other experienced consultants to join as fellow members.
							<?php if($this->Session->read('Auth.User.id') ==''){?>
							<a href="javascript:void(0);" data-toggle="modal" data-target="#signin-Modal" title="Send Membership Invitations">View eligibility</a>.
							<?php }else{?>
							<a href="<?php echo(SITE_URL)?>members/invitation_eligibility" title="Send Membership Invitations">View eligibility</a>.
							<?php }?>
							</p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/f-case-1.png" alt="f-case-1">
							<span>Case study</span>
						</div>
						<div class="f-third f-center">
							<p>Share your past successes and position yourself as an authority. <a href="<?php echo(SITE_URL)?>members/case_studies" title="View Case Studies" target="_blank">View case studies</a> or 
							<?php if($this->Session->read('Auth.User.id') ==''){?>
							<a href="javascript:void(0);" data-toggle="modal" data-target="#signin-Modal" title="Submit Your Case Study">Submit your case study</a>.
							<?php }else{?>
							<a href="<?php echo(SITE_URL)?>members/add_casestudy" title="Submit Your Case Study">Submit your case study</a>.
							<?php }?>
							</p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/f-file-manager-1.png" alt="f-file-manager-1">
							<span>Project proposal</span>
						</div>
						<div class="f-third f-center">
							<p>Help our team market your expertise to your target customers. <a href="<?php echo(SITE_URL)?>project/project_proposals" title="View Project Proposals" target="_blank">View project proposals</a> or 
							<?php if($this->Session->read('Auth.User.id') ==''){?>
							<a href="javascript:void(0);" data-toggle="modal" data-target="#signin-Modal" title="SSubmit Your Project Proposal">Submit your project proposal</a>.
							<?php }else{?>
							<a href="<?php echo(SITE_URL)?>project/proposal_create" title="Submit Your Project Proposal">Submit your project proposal</a>.
							<?php }?>
							</p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/f-public-1.png" alt="f-public-1">
							<span>Public relations</span>
						</div>
						<div class="f-third f-center">
							<p>Get mentioned in leading media outlets by sending a press release or by replying to queries by top reporters. <a href="<?php echo(SITE_URL)?>members/master" title="Upgrade now">Upgrade now</a>.</p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/f-customshare-2.png" alt="f-customshare-2">
							<span>Custom share</span>
						</div>
						<div class="f-third f-center">
							<p>CustomShare — A simple tool to drive traffic to your profile with every link you share. <a href="<?php echo SITE_URL?>members/customshare_input" title="CustomShare">Learn more</a>.</p>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/flevy-2.png" alt="flevy-1">
						</div>
						<div class="f-third f-center">
							<p>Get access to business frameworks, presentation templates, financial models and other tools used by top consultancies. <a href="<?php echo SITE_URL?>members/flevy" title="Flevy documents">Learn more</a>.</p>
						</div>
					</div>
				</div>



				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-sxs-12">
					<div class="feature-box">
						<div class="f-box">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/9lenses.png" alt="9lenses">
						</div>
						<div class="f-third f-center">
							<p>Assess business needs and manage data on a single digital platform to better advise clients. <a href="<?php echo SITE_URL?>members/ninelenses" title="9Lenses documents">Learn more</a>.</p>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- Features Close -->

	<!-- Premium Membership Plans Begin -->
	<div class="md-p-100 premium-mp">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="member-detail mb-title">
						<h2>Premium membership plans</h2>
						<p>You have so much to offer to your clients. Do what you do best, and leave the rest to your GUILD team!</p>
					</div>
				</div>
			</div>
			<div class="row flex">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-100">
					<div class="premium-plan">
						<div class="premium-logo">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/p-gluid-website-logo6.png" alt="p-gluid-website-logo5">
						</div>
						<div class="premium-price">
							<h2>$199</h2>
							<span>Per month</span>
						</div>
						<div class="premium-detail">
							<p>High performance websites for experienced consultants.</p>
							<ul>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>GUILD BASIC + </li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Website development and hosting</li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Fully managed services</li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Monthly traffic reviews and content changes </li>
							</ul>
						</div>
						<div class="button-set">
							<a class="btn btn-primary" href="<?php echo SITE_URL;?>members/website" target="_blank">Learn more</a>
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 col-100">
					<div class="premium-plan">
						<div class="premium-logo master">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/p-gluid-master-logo3.png" alt="p-gluid-master-logo2">
						</div>
						<div class="premium-price">
							<h2>$999</h2>
							<span>Per month</span>
						</div>
						<div class="premium-detail">
							<p>Marketing support to help you stand out in the crowd.</p>
							<ul>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>GUILD WEBSITES + </li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Social media management</li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Weekly LinkedIn/ blog posts</li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Monthly newsletters</li>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Quarterly whitepapers</li>
							</ul>
						</div>
						<div class="button-set">
						<a class="btn btn-primary" href="<?php echo SITE_URL;?>members/master" target="_blank">Learn more</a>							
						</div>
					</div>
				</div>

				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="premium-plan">
						<div class="premium-logo premier">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/p-gluid-premier-logo3.png" alt="p-gluid-premier-logo3">
						</div>
						<div class="premium-price">
							<h2>$2,999+</h2>
							<span>Per month</span>
						</div>
						<div class="premium-detail">
							<p>All-in-one marketing solution for unique member needs.</p>
							<ul>
								<li><i class="fa fa-check-square-o" aria-hidden="true"></i>Full service, customized marketing support for your firm</li>
							</ul>
						</div>
						<div class="button-set">
                                                        <a class="btn btn-primary" href="tel:+18665111898">Call 1-866-511-1898</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- Premium Membership Plans Close -->
      <?php if($this->Session->read('Auth.User.id') !=''){?>
	<!-- Customized Plan Begin -->
	<div class="md-p-100 customized-plan">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="member-detail mb-title">
						<h2>Customized plan</h2>
						<p>Select the features you want to include in your custom plan and submit a quote you are willing to pay.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="plan-box">
						<div class="c-box">
							<ul>
								<li class="styled-form">
									<input id="gw" type="checkbox" name="" value="" checked>
									<label for="gw">GUILD Website</label>
								</li>

								<li class="styled-form">
									<input id="la" type="checkbox" name="" value="" checked>
									<label for="la">LinkedIn articles</label>
									<span class="qtr"><input id="larticlesinput" type="text" name="" value="1" class="form-control">/Qtr</span>
								</li>
								<li class="styled-form">
									<input id="seo" type="checkbox" name="" value="" checked>
									<label for="seo">SEO support</label>
								</li>
								<li class="styled-form">
									<input id="spo" type="checkbox" name="" value="" checked>
									<label for="spo">Social profile optimization</label>
								</li>
								<li class="styled-form">
									<input id="dsm" type="checkbox" name="" value="">
									<label for="dsm">Daily social media management</label>
								</li>
							</ul>
						</div>
						<div class="c-box c-right">
							<ul>
								<li class="styled-form">
									<input id="ew" type="checkbox" name="" value="" checked>
									<label for="ew">Ebook/Whitepaper</a></label>
									<span class="qtr"><input id="ebookinput" type="text" name="" value="1" class="form-control">/Qtr</span>
								</li>
								<li class="styled-form">
									<input id="dia" type="checkbox" name="" value="">
									<label for="dia">Digital advertising</label>
								</li>
								<li class="styled-form">
									<input id="ws" type="checkbox" name="" value="">
									<label for="ws">Webinar support</label>
									<span class="qtr"><input id="wsupportinput" type="text" name="" value="" class="form-control">/Qtr</span>
								</li>
								<li class="styled-form">
									<input id="b6w" type="checkbox" name="" value="" checked>
									<label for="b6w">Blog (600 words)</label>
									<span class="qtr"><input id="bloginput" type="text" name="" value="4" class="form-control">/Qtr</span>
								</li>
							</ul>
						</div>
						<div class="plan-box-btm">
							<span>$</span>
							<input type="text" name="" id="quoteAmount" class="form-control" placeholder="Enter your quote here" value="695" onblur="if(this.value == '') {  this.value='695'}" onfocus="if (this.value == '695') {this.value=''}"><span> /Month</span>
							<div class="button-set">
								<input name="" type="submit" id="submit" class="btn btn-primary" value="Submit quote">
							</div>
						</div>
			              <div class="form-success hide">
				           <p>Thank You for Submitting your Quotes! we will get back asap.</p>
			              </div>							
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Customized Plan Close -->
        <?php }?>
</div>
	<!-- js link -->
	<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>-->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>


<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/slick.min.js"></script>-->
<script type="text/javascript">
    jQuery(document).ready(function(){
	    jQuery('.t-testimonial').slick({
	      dots: false,
	      arrows: true,
	      infinite: true,
	      speed: 300,
	      autoplay: true,
	      autoplaySpeed: 10000,
	      slidesToShow: 1,
	      adaptiveHeight: true,
	      draggable: false
	    });
    });
</script>

<script type="text/javascript">
jQuery(document).ready(function() {


jQuery("#submit").click(function(){

if(jQuery("#gw").is(':checked')){
var website = "yes";
}else{
var website = "no";
}
if(jQuery("#ew").is(':checked')){
var ebook = "yes";
var ebooknumber = jQuery("#ebookinput").val();
}else{
var ebook = "no";
var ebooknumber = "no";
}
if(jQuery("#la").is(':checked')){
var larticles = "yes";
var larticlesnumber = jQuery("#larticlesinput").val();
}else{
var larticles = "no";
var larticlesnumber = "no";
}
if(jQuery("#seo").is(':checked')){
var ssupport = "yes";
}else{
var ssupport = "no";
}
if(jQuery("#dia").is(':checked')){
var dsmmgmt = "yes";
}else{
var dsmmgmt = "no";
}
if(jQuery("#ws").is(':checked')){
var wsupport = "yes";
var wsupportnumber = jQuery("#wsupportinput").val();
}else{
var wsupport = "no";
var wsupportnumber = "no";
}
if(jQuery("#b6w").is(':checked')){
var blog = "yes";
var blognumber = jQuery("#bloginput").val();
}else{
var blog = "no";
var blognumber = "no";
}
if(jQuery("#spo").is(':checked')){
var spoptimization = "yes";
}else{
var spoptimization = "no";
}
if(jQuery("#dsm").is(':checked')){
var dmarketing = "yes";
}else{
var dmarketing = "no";
}

var quoteamount = jQuery("#quoteAmount").val();

		if(loggedUserId =='') {
			alert("To submit a customized plan request need to first login or register.");
		} else {
		       jQuery.ajax({
					url:SITE_URL+'members/customplan_request',
					type:'post',
					dataType:'json',
					data: 'website='+website + '&ebook=' + ebook + '&ebooknumber=' + ebooknumber + '&larticles=' + larticles + '&larticlesnumber=' + larticlesnumber + '&ssupport=' + ssupport + '&dsmmgmt=' + dsmmgmt + '&wsupport=' + wsupport + '&wsupportnumber=' + wsupportnumber + '&blog=' + blog + '&blognumber=' + blognumber + '&spoptimization=' + spoptimization + '&dmarketing=' + dmarketing + '&quoteamount=' + quoteamount,
					success:function(id){
                                                $(".form-success").removeClass("hide");					}
				});	
		       
		   		return false;
                              }

});

var flag = 0;


jQuery('#gw').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#ew').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#la').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#seo').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#dia').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#ws').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#b6w').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#spo').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#dsm').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#ebookinput').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#larticlesinput').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});

jQuery('#bloginput').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#wsupportinput').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#quoteAmount').focus(function(){
   jQuery(this).removeAttr('placeholder');
});
          
});
</script>

<style>
@media screen and (min-width: 767px){

.testimonial-section .item .tesimonial-part .person-img{
    float: left;
    margin: 17px 43px 0 37px;

}

#chipcomment{

margin-top:12px;
}


#markcomment{
margin-top:35px;
}

</style>