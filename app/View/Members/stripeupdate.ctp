<div id="inner-content-wrapper">
<div id="inner-content-box" class="pagewidth">
  <div class="inner_wrapper">
    <div class="underline">
      <h1 style="font-size: 20px;">Update Card Details for Premium Membership Service</h1>
    </div>
    <div class="">
        <?php 
				$urlk1 = ($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($urlk1); 
              ?>
      <div class="figure-left align-left-botttom ">
        <?php
        $flag = 0; $defaultQuestion=0;
        if (isset($this->data['UserImage'][0]['image_name']) && $this->data['UserImage'][0]['image_name'] != '') {
            $file = file_exists(WWW_ROOT . 'img' . '/' . MENTORS_IMAGE_PATH . '/' . $this->data['User']['id'] . '/' . $this->data['UserImage'][0]['image_name']);
            if ($file == 0) {
                $flag++;
            }
        } else {
            $flag++;
        }
        if ($flag == 0) {
            echo($this->Html->link($this->Html->image(MENTORS_IMAGE_PATH . '/' . $this->data['User']['id'] . '/' . $this->data['UserImage'][0]['image_name'],array('alt' => $this->data['UserReference']['first_name'],'class'=>'thumb')),$displink,array('escape'=>false,'style'=>'border: 0px;')));
      } else {
            echo($this->Html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name'],'class'=>'thumb')));
        }
      ?>      
        <div class="figcaption" style="margin-top: 114px; position: static;">        
         <h2><span><?php echo($this->Html->link(ucwords($this->data['UserReference']['first_name']).' '.($this->data['UserReference']['last_name']),$displink,array('escape'=>false)));?></span></h2>
            
             <?php
				$disp="guild.im/".strtolower($this->data['User']['url_key']);
				$completelink=SITE_URL.strtolower($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($this->data['User']['url_key']);
				?>
				<span><?php echo($disp); ?></span>
        </div>
      </div>
      <div>
        
        <ul>
          <li>Stand out among your peers</li>
          <li>Extend your reach, and</li>
          <li>Shorten your sales cycle</li>
        </ul>
        </br>
	 
        <br></br>

        <div style="float: right; margin-bottom:80px;" >
		<span style="float:left;padding:11px 18px;"><?php echo($this->Html->link('Cancel',SITE_URL.'members/website',array('class'=>'reset','style'=>""))); ?></span>
		<div style="float:right">	
        		<form action="stripe" method="POST">
				<input type="hidden" name="FEE" value="<?php echo($centFee);?>">
			  		<script
			    			src="https://checkout.stripe.com/checkout.js" class="stripe-button"
			    			data-key="pk_live_vg2OVxfTe0Iw5FGSMhitLWE7"
			    			data-image="https://www.guild.im/img/media/guild-footer1.png"
			    			data-name="GUILD"
                                                data-panel-label="Update Card Details"
                                                data-label="Update Card Details"
                                                data-allow-remember-me=false
                                                data-locale="auto">
			 		 </script>
			</form>
		</div>
		</div>	
    </div>
  </div>
</div>
</div>
