<!doctype html>
<html class="no-js full-height" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Custom Link Creator | GUILD</title>
    <meta name="description" content="CustomShare  A simple tool to drive traffic to your profile with every link you share.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://test.guild.im/yogesh_new1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css" media="screen">
    @font-face {
        font-family: 'UbuntuM';
        src: url('https://test.guild.im/yogesh_new1/fonts/UbuntuM.eot');
        src: url('https://test.guild.im/yogesh_new1/fonts/UbuntuM.eot') format('embedded-opentype'), url('https://test.guild.im/yogesh_new1/fonts/UbuntuM.woff2') format('woff2'), url('https://test.guild.im/yogesh_new1/fonts/UbuntuM.woff') format('woff'), url('https://test.guild.im/yogesh_new1/fonts/UbuntuM.ttf') format('truetype'), url('https://test.guild.im/yogesh_new1/fonts/UbuntuM.svg#UbuntuM') format('svg');
    }

    @font-face {
        font-family: 'OpenSansRegular';
        src: url('https://test.guild.im/yogesh_new1/fonts/OpenSansRegular.eot');
        src: url('https://test.guild.im/yogesh_new1/fonts/OpenSansRegular.eot') format('embedded-opentype'), url('https://test.guild.im/yogesh_new1/fonts/OpenSansRegular.woff2') format('woff2'), url('https://test.guild.im/yogesh_new1/fonts/OpenSansRegular.woff') format('woff'), url('https://test.guild.im/yogesh_new1/fonts/OpenSansRegular.ttf') format('truetype'), url('https://test.guild.im/yogesh_new1/fonts/OpenSansRegular.svg#OpenSansRegular') format('svg');
    }
    </style>
    <link rel="stylesheet" href="https://test.guild.im/yogesh_new1/css/style.css?v=3.29">
</head>

<body class="full-height" >
    <div class="container-fluid full-height">
        <div class="row full-height">
            <div class="col-md-4 col-sm-5 full-height scroll-y creator-sidebar">
                <div class="row">
                    <div class="col-sm-12 creator-heading">
                        <h4 id="header_heading">Create your custom link</h4>
                        <a href="javascript:void(0);" class="close-button" id="closebutton">&#120;</a>
                    </div>
                </div>
                <div class="row">
                    <div class="snip-wrap">
                        <div class="create-snip-box" id="firstscreen">
                            <ul>
                                <li class="unsnippable-row">
                                    <label>Enter URL of the article</label>
                                    <input class="form-control" type="text" placeholder="Enter the link you want to share" id="urlbox" />
                                    <div class="unsnippable-message">Error message <i data-placement="top" data-toggle="tooltip" data-original-title="GUILD uses iframing to display your CTA but this page doesn't support iframes." class="fa fa-question-circle unsnippable-tooltip"></i></div>
                                    <div class="unsnippable-message1">Error message <i data-placement="top" data-toggle="tooltip" data-original-title="Due to GUILD secure https website we can't load content of http website." class="fa fa-question-circle unsnippable-tooltip"></i></div>
                                    <i class="fa fa-exclamation"></i>
                                </li>
                                <li>
                                    <label>My comments <span>(<div id="commentcount">80</div> characters remaining)</span></label>
                                    <textarea class="form-control" placeholder="Enter your comment" id="commentbox"></textarea>
                                </li>
                                <?php if($this->Session->read('Auth.User.role_id') != '2') {?>
                                <li>
                                    <label>Enter button text</label>
                                    <input class="form-control" type="text" placeholder="Enter the Button text" id="buttontext" />
                                </li>
                                <li>
                                    <label>Button URL</label>
                                    <input class="form-control" type="text" placeholder="Enter url of your website" id="calltoaction" />
                                </li>
                                <?php }?>
                               <?php if($this->Session->read('Auth.User.role_id') == '2') {?>
                                <li class="styled-form">
                                    <input name="checkbox" id="an" placeholder="" unchecked="" type="checkbox">
                                    <label for="an">Get prospect email</label>
                                </li>
                                <?php }else{?> 
                                <li class="styled-form" style="display:none;">
                                    <input name="checkbox" id="an" placeholder="" unchecked="" type="checkbox">
                                    <label for="an">Get prospect email</label>
                                </li>
                                <?php }?>
                                <li>
                                    <a href="javascript:void(0);" class="btn btn-primary visit-btn">CREATE LINK</a>
                                </li>
                            </ul>
                        </div>
                        <div class="sniped-box" style="display: none;" id="secondscreen">
                            <div class="creator-info text-center">
                                <h3>Share your custom link</h3>
                                <?php if($this->Session->read('Auth.User.id') == '') {
                                $loggedin ="no";
                                }else{
                                $loggedin ="yes";
                                }?>
                                <p>Anyone clicking on this link will see the page with your call-to-action inside.</p>
                            </div>
                            <div class="creator-link">
                                <div class="input-group">
                                    <input type="text" class="form-control" value="" id="sharebox" />
                                    <p id="shareboxtext" style="disply:none;"></p>
                                    <span class="input-group-btn">
								<a href="javascript:void(0);" class="btn btn-default copy-button" onclick="javascript:copyToClipboard('#shareboxtext');">COPY</a>
										</span>
                                </div>
                            </div>
                            <a href="" class="btn btn-primary visit-btn" target="_blank" id="visitlinkbutton">VISIT LINK</a>
                            <div class="social-assets">
                                <div class="center-line"><span class="center-line-text">SHARE TO</span></div>
                                <ul class="social-list-square">

                                    <?php


                                       $name = $this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'];
                                       

							
								
                              $socialCount=0;
                                   
                               while(count($this->data['Social'])>$socialCount){
                                                     
					        if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'twitter.com'))!==false)
						      {
							$twitterURL = $this->data['Social'][$socialCount]['social_name'];
                                                        $path = explode("/", $twitterURL);
                                                        $twitter_handle = end($path); 
                                                           
						      }
					               if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'linkedin.com'))!==false)
						      {
							$linkedinURL = $this->data['Social'][$socialCount]['social_name'];                                                           
						      }
                                                      $socialCount++;
						      } 


								$linkdinUrl="https://www.linkedin.com/shareArticle?mini=true&url=".$completelink."&title=".$compTitle;
								if($twitter_handle ==''){
								$tweet="https://twitter.com/intent/tweet?text=".$this->data['UserReference']['first_name']." ". $this->data['UserReference']['last_name'] ."&#58; ".$compTitle." " .$bitlyUrl;
								} else{
								$tweet="https://twitter.com/intent/tweet?text=".$atsybmol.$twitter_handle." ".$compTitle." " .$bitlyUrl;
								}
								?>

<input type="hidden" name="Language" value="<?php echo($loggedin)?>" id="loggedin">
<input type="hidden" name="Language" value="<?php echo($twitter_handle)?>" id="twitterhandle">
<input type="hidden" name="Language" value="<?php echo($name);?>" id="consultant_name">
<input type="hidden" name="Language"  id="urlhiddenbox">



                                                               <li>
                                                                <a id="twittershare" href="javascript:void(0);">
										<i class="fa fa-twitter " aria-hidden="true "></i>
								</a>

								</li>
								<li>

									<a id="linkedinshare" href="javascript:void(0);">
										<i class="fa fa-linkedin " aria-hidden="true "></i>
									</a>
								</li>
								<li>
								    <a id="facebookshare" href="javascript:void(0);">
										<i class="fa fa-facebook " aria-hidden="true "></i>
									</a>
								</li>
								<li>
									<a id="googleshare" href="javascript:void(0); ">
										<i class="fa fa-google-plus " aria-hidden="true "></i>
									</a>
								</li>



									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-8 col-sm-7 hidden-xs full-height hide-overflow pagesection ">
					<div class="stretch-overlay "></div>
					<iframe id="preview_frame" class="stretch no-border " scrolling="yes " src=" "></iframe>
					<div class="creator-user ">
						<div class="creator-user-img ">
						
              <?php if($this->Session->read('Auth.User.role_id') == '2') {

					  	$urlk1 = ($this->data['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1); 
                                           if($this->data['UserReference']['business_website'] !=''){
                                                $link = $this->data['UserReference']['business_website'];  
                                                $consultation_link = SITE_URL."fronts/consultation_request/ ".$urlk1; 
                                           } else{
                                              $link = SITE_URL.strtolower($urlk1);
                                              $consultation_link = SITE_URL."fronts/consultation_request/ ".$urlk1;
                                           }
					 if(!empty($this->data['UserImage'])){
						if(file_exists(WWW_ROOT . 'img' . '/' .MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'])){ 
							echo($this->Html->link($this->Html->image(MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'], array('class'=>'desaturate','alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank')));       
                					
						}else{				
							echo($this->Html->link($this->Html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])),$displink1,array('escape'=>false)));  
						}}}?>
						
					<?php if($this->Session->read('Auth.User.role_id') != '2'){?>
						<a href="<?php echo (SITE_URL);?>" ><img src="<?php echo(SITE_URL)?>imgs/guild_monogram.png"></a>
                                            <?php }?>
                            </div>
                            <div class="creator-user-info">
                                <a href="javascript" class="creator-user-logo"></a>
                                <?php if($this->Session->read('Auth.User.role_id') == '2'){?>
                                <a href="<?php echo($consultation_link);?>" class="creator-user-name" target="_blank"><?php echo(ucfirst($this->data['UserReference']['first_name']));?> <?php echo($this->data['UserReference']['last_name']); ?></a>
                                <?php }else{?>
                                <a href="<?php echo (SITE_URL);?>" class="creator-user-name">GUILD</a>
                                <?php }?>
                                <div class="creator-user-btn">
                                    <span id="message">Enter your comment</span>
                                    <?php if($this->Session->read('Auth.User.role_id') == '2' && $this->data['UserReference']['business_website'] !=''){
					              if(($pos =strpos($this->data['UserReference']['business_website'],'http'))!==false)
						          {
							      $websitelink=$this->data['UserReference']['business_website'];
						          }
						           else
						          {
							      $websitelink="http://".$this->data['UserReference']['business_website'];
						           }								
								?>
                                    <a href="<?php echo ($websitelink);?>" class="btn btn-primary" target="_blank">Click Here</a>
                                    <?php }else if($this->Session->read('Auth.User.role_id') == '2' && $this->data['UserReference']['business_website'] ==''){
								$urlk1 = ($this->data['User']['url_key']);
								$consultation_link = SITE_URL."fronts/consultation_request/".$urlk1; 
								?>
                                    <a href="<?php echo ($consultation_link);?>" class="btn btn-primary" target="_blank">Click Here</a>
                                    <?php }else{?>
                                    <a href="javascript:void(0);" class="btn btn-primary" target="_blank" id="calltoactionbutton">Click Here</a>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
            <!-- <script src="<?php echo(SITE_URL)?>yogesh_new1/js/editor.js"></script> -->
            <!-- Google Re-captcha Code -->
            <script src='https://www.google.com/recaptcha/api.js'></script>
            <!-- Include the Quill library -->
            <script src="https://cdn.quilljs.com/1.0.0/quill.js"></script>
            <!-- Include Yogesh JS code -->
           <!-- <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
            <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
            <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>-->
            <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
            <!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
            <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>-->
			<script type="text/javascript" src="<?php echo(SITE_URL)?>sanchit/js/custominputsharescript.js"></script>
            


            <!--<script type="text/javascript">


            $("#urlbox").focus(function() {

            }).blur(function() {
                $(".unsnippable-row").removeClass('show-icon');
	        $(".unsnippable-message").hide();
                $(".unsnippable-message1").hide();
                var url = jQuery("#urlbox").val();
          if (url != '') 
             {

                   $.getJSON("https://test.guild.im/fetch.php?url="+url, function (data) {

                    if(url.indexOf('linkedin.com') !== -1)
                    {
                         error();
                    }


                             url = data.httpcode['url'];
                             $("#urlhiddenbox").val(url);


                if (data.error) { 

	              error();
    
                } else { 



                    if(url.indexOf('http://') !== -1)
                    {
                        error1();

                    }
	           var iframe = document.getElementById('preview_frame')

	           iframe.src = url;
                }
             });
           }
          });

            $('[data-toggle="tooltip"]').tooltip();
            $("#calltoaction").focus(function() {

            }).blur(function() {

                var url = jQuery("#calltoaction").val();
                if (url != '') {
                    if (!(url.indexOf('http://') === 0 || url.indexOf('https://') === 0)) {
                        url = 'https://' + url;
                    }
                    document.getElementById('calltoactionbutton').href = url;
                }
            });

            $("#buttontext").focus(function() {
               
            }).blur(function() {
                var text = jQuery("#buttontext").val();
                var defaulttext = 'Click Here';
                    if(text !=''){
                    $("#calltoactionbutton").html(text);
                    }else{
                    $("#calltoactionbutton").html(defaulttext);
                    } 
                
            });



                         function error() {
                              console.log('error');
                         $(".unsnippable-message").show();
                	 $(".unsnippable-row").addClass('show-icon');
                           }

                        function error1() {
                         console.log('error1');
                         $(".unsnippable-message1").show();
                         $(".unsnippable-row").addClass('show-icon');
                         }



            function copyToClipboard(element) {

                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val($(element).text()).select();
                document.execCommand("copy");
                $temp.remove();
            }
            </script>
            <script type="text/javascript">


            function updateMessage() {
                var comment = jQuery("#commentbox").val();


                jQuery("#message").html(comment);

            }

            jQuery("#commentbox").keyup(function() {
                updateMessage();
            });
            jQuery("#commentbox").keypress(function() {
                updateMessage();
            });
            jQuery("#commentbox").keydown(function() {
                updateMessage();
            });

            function updateCount() {
                var count = jQuery("#commentbox").val();

                if (count.length < 80) {
                    jQuery("#commentcount").html(80 - count.length);
                } else {
                    jQuery("#commentcount").html(0);
                    jQuery("#commentbox").val(count.substring(0, 80));
                }
            }

            jQuery("#commentbox").keyup(function() {
                updateCount();
            });
            jQuery("#commentbox").keypress(function() {
                updateCount();
            });
            jQuery("#commentbox").keydown(function() {
                updateCount();
            });
            </script>
            <script type="text/javascript">
            $("#closebutton").on('click', function() {
            	if ($('#firstscreen').css('display') == 'none') {
					$('#secondscreen').hide();
					$('#firstscreen').show();
            	}
            	else{
            		window.location = "https://test.guild.im";
            	}


            });
            </script>

          <script type="text/javascript">

            $(".create-snip-box .visit-btn").on('click', function() {
                var loggedin = $("#loggedin").val();
                var twitterhandle = $("#twitterhandle").val();
                var consultant_name = $("#consultant_name").val();
                var url = $("#urlhiddenbox").val();
                var comment = $("#commentbox").val();
                var destinationurl = $("#calltoactionbutton").attr('href');
                var buttontext = $("#calltoactionbutton").html();

                if(buttontext === undefined)
                { 
                  buttontext ="Click Here";
                }

                if (document.getElementById('an').checked) {
                    var checkbox = "Yes";
                } else {
                    var checkbox = "No";
                }




              if(url !='' && comment !=''){
                $(document).ready(function() {

                    $.ajax({
                        url: 'https://test.guild.im/members/customshare_process',
                        type: 'post',
                        dataType: 'json',
                        data: 'url=' + url + '&comment=' + comment + '&destinationurl=' + destinationurl + '&checkbox=' + checkbox + '&buttontext=' + buttontext,
                        success: function(res) {
                            if (res.title != '') {
                                var link = res.link;
                                var title = res.title;
                                if (title != null) {
                                    var combined = title + ' ' + link;
                                } else {
                                    var combined = ' ' + link;
                                }
                                $(".create-snip-box").hide();
                                $(".sniped-box").show();
                                $('#sharebox').val(link);
                                $("#shareboxtext").html(link);
                                $("#shareboxtext").hide();
                                $("#header_heading").html("Share your custom link");
                                document.getElementById("visitlinkbutton").href = link;


	if(title.indexOf("'") > -1 ) {
	title = title.replace("'", " ");
        }
	if(title.indexOf('"') > -1 ) {
	title = title.replace('"', " ");
        }
	if(title.indexOf('&') > -1 ) {
	title = title.replace('&', "and");
        }
	if(title.indexOf('(') > -1 ) {
	title = title.replace('(', " ");
        }
	if(title.indexOf(')') > -1 ) {
	title = title.replace(')', " ");
        }


                               var guildalert ="@GuildAlerts:";
                               var atsybmol = "@";

                               if(loggedin =="no"){

                                 var tweetshare ="javascript:window.open('https://twitter.com/intent/tweet?text="+ guildalert +" "+ title +" " + link +"','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')";
                                 document.getElementById("twittershare").setAttribute('onclick',tweetshare);


                                 var linkedinshare ="javascript:window.open('https://www.linkedin.com/shareArticle?mini=true&url="+ link +"&title="+ title +"','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')";
                                 document.getElementById("linkedinshare").setAttribute('onclick',linkedinshare);

                                 var facebookshare ="javascript:window.open('https://www.facebook.com/sharer.php?s=100&p[url]="+ link +"&p[title]="+ guildalert +" "+ title +"','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')";
                                 document.getElementById("facebookshare").setAttribute('onclick',facebookshare);
                                

                                 var googleshare ="javascript:window.open('https://plus.google.com/share?url="+ link +"&title="+ guildalert +" "+ title +"','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')";
                                 document.getElementById("googleshare").setAttribute('onclick',googleshare);

                             }else{

			         if(twitterhandle ==''){
                                 var tweetshare ="javascript:window.open('https://twitter.com/intent/tweet?text="+ consultant_name + ": "+ title +" " + link +"','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')";
				 } else{
				 var tweetshare ="javascript:window.open('https://twitter.com/intent/tweet?text="+ atsybmol + twitterhandle +": "+ title +" " + link +"','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')";
				 }


                                 document.getElementById("twittershare").setAttribute('onclick',tweetshare);


                                 var linkedinshare ="javascript:window.open('https://www.linkedin.com/shareArticle?mini=true&url="+ link +"&title="+ title +"','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')";
                                 document.getElementById("linkedinshare").setAttribute('onclick',linkedinshare);

                                 var facebookshare ="javascript:window.open('https://www.facebook.com/sharer.php?s=100&p[url]="+ link +"&p[title]="+ guildalert + " " + consultant_name + ":  "+ title +"','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')";
                                 document.getElementById("facebookshare").setAttribute('onclick',facebookshare);
                                

                                 var googleshare ="javascript:window.open('https://plus.google.com/share?url="+ link +"&title="+ guildalert + " " + consultant_name + ":  "+ title +"','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')";
                                 document.getElementById("googleshare").setAttribute('onclick',googleshare);

                                 }


                            } else {
                                alert("Failure!!!");
                            }
                        }
                    });


                });
				
              }else{
			  
			   alert("Please enter URL and comment to create custom link !!");
			  
			  }

            });

           </script>-->
</body>

</html>