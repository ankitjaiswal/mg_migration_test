	<div class="container-fluid" id="apply-section">
		<div class="container create-section pad0">
			<div class="col-md-8 pad40">
				<div class="apply-heading">
					<h5>Add a case study</h5>
				</div>
				<div class="apply-form">
					<form action="<?php echo SITE_URL ?>members/add_casestudy" method="post" id="Addform">
						<div class="form-group" data-placement="right" data-toggle="DescPopover" data-container="body">
							<label>Title</label>
							<span class="pull-right char-limit">(<span id="noOfChar">128</span> characters remaining)</span>
							<input type="text" id="title" class="form-control" placeholder="Title of your case study" name="data[CaseStudy][title]">
							<div id="popover-content" class="hide">
								<p>Keep it short & interesting (128 characters).</p>
								<p>e.g. Driving Transformation in a Family Owned Business</p>
								
							</div>
						</div>
						<div class="form-group" data-placement="right" data-toggle="DescPopover1" data-container="body">
							<label>Client</label>
							<input type="text" id="client_details" class="form-control" placeholder="Client name, or industry & size" name="data[CaseStudy][client_details]">
							<div id="popover-content1" class="hide">
								<p>e.g. $250 Million retail franchise</p>
								
							</div>
						</div>
						<div class="form-group" data-placement="right" data-toggle="DescPopover2" data-container="body">
							<label>Situation</label>
							<span class="pull-right char-limit">(<span id="noOfCharSituations">1250</span> characters remaining)</span>
                                                        <input type="hidden" name="data[CaseStudy][situation]"  id="situationtext"/>
							<div id="editor-container"></div>
							<div id="popover-content2" class="hide">
								<p>Capture reader's interest (1250 characters)</p>
								<p>- What does the client do?</p>
								<p>- What was the context/ environment?</p>
								<p>- What were the client needs?</p>
								<p>- What was the complexity?</p>
							</div>
						</div>
						<div class="form-group" data-placement="right" data-toggle="DescPopover3" data-container="body">
							<label>Actions</label>
							<span class="pull-right char-limit">(<span id="noOfCharActions">1250</span> characters remaining)</span>
                                                        <input type="hidden" name="data[CaseStudy][actions]"  id="actionstext"/>
							<div id="editor-container1"></div>
							<div id="popover-content3" class="hide">
								<p>Get specific (1250 characters)</p>
								<p>- Where did you begin?</p>
								<p>- What were the different steps/ milestones?</p>
								<p>- What actions did you take, for how long?</p>
								<p>- Show how external intervention helps</p>
								<p>- The 'solution' should not be obvious</p>
							</div>
						</div>
						<div class="form-group" data-placement="right" data-toggle="DescPopover4" data-container="body">
							<label>Results</label>
							<span class="pull-right char-limit">(<span id="noOfCharResults">1250</span> characters remaining)</span>
                                                        <input type="hidden" name="data[CaseStudy][result]"  id="resultstext"/>
							<div id="editor-container2"></div>
							<div id="popover-content4" class="hide">
								<p>Talk ROI (1250 characters)</p>
								<p>- Use numbers or tangible benefits, if possible</p>
								<p>- Both short term and long term benefits</p>
                                                                <p>- Finish your story</p>
							</div>
						</div>
						<div class="e-button pull-right">
							<label>
								<a href="<?php echo SITE_URL?>users/edit_account#case_studies">
									Cancel
								</a>
							</label>
							<button class="banner-getstarted btn btn-default get-started" id="submitForm">
								Preview
							</button>
						</div>
					</form>
				</div>
			</div>

			<div class="col-md-4 clearboth create-right">
				<div class="right-apply">
					<div class="apply-heading">
				         <h5>Showcase past projects</h5>
					</div>
					<div class="apply-desc">
						<div class="cr-desc">
							<h5>1. Clearly state the business problem</h5>
							<p>Allow other organizations to identify with your customer and then you've got them hooked for the solutions.</p>
						</div>
						<div class="cr-desc">
							<h5>2. Identify the solution and celebrate your results</h5>
							<p>What was your approach, and the business impact &#8211; real or expected.</p>
						</div>
						<div class="cr-desc">
							<h5>3. Promote your case studies</h5>
							<p>Use social media to promote your case studies on GUILD, and we will do the same.</p>
						</div>
					</div>
					<div class="apply-heading">
						<h5>Not ready to post yet?</h5>
					</div>
					<div class="apply-desc">
						<div class="cr-desc mt10">
							<p>Call us to discuss your business needs. </br>Call <a href='tel:+18665111898'>1-866-511-1898</a>.</p>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>

	<!-- js link -->
	<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>-->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>-->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/quill.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>sanchit/js/addcasestudyscript.js"></script>