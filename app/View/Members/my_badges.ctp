<script type="text/javascript" src="<?php echo(SITE_URL)?>js/mg_iframe.js"></script>
<link rel="stylesheet" type="text/css" href="https://test.guild.im/css/tiny/style.css">
<div id="inner-content-wrapper" class="my-badges-content">
    <div class="container">
      <div class="row bs-flex">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
          <div class="signbox" contenteditable="true">
              <div class="signbox-img">
                  <?php $link = (SITE_URL."img/members/profile_images".'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name']);
                      $displink1=$this->data['UserReference']['business_website'];
                            ?>
                  <a style="text-decoration:none" href="<?php echo($displink1);?>" target="_blank">
                  <img src="<?php echo($link);?>" alt="GUILD" border="0" height="80" width="80">
                  </a>
              </div>
              <div class="signbox-content">
                <p style="font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:14px;color:rgb(33,33,33);margin: 0 0 13px 0;"><span style="font-weight:bold;color:rgb(33,33,33);display:inline"><?php echo(ucfirst($this->data['UserReference']['first_name']));?> <?php echo($this->data['UserReference']['last_name']); ?></span>
                    <span style="display:inline"></br></span>
                    <a href="mailto:<?php echo($this->data['User']['username']);?>" style="color:rgb(208,49,53);text-decoration:none;display:inline" target="_blank"><?php echo($this->data['User']['username']);?></a><span style="color:rgb(33,33,33)"></span>
                </p>
                <p style="font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:14px">
                    <span style="font-weight:bold;color:rgb(33,33,33);display:inline"><?php echo(ucfirst($this->data['UserReference']['linkedin_headline']));?></span>
                    <?php 
                        $modeTypeFaceToFace=$modeTypePhone=$modeTypeVideo='FALSE' ;
                        $modeValue1=$modeValue2=$modeValue3='';
                        if(isset($this->data['Communication'][0]['mode_type']) && $this->data['Communication'][0]['mode_type'] != '')
                        {
                            
                          if($this->data['Communication'][0]['mode_type']=='face_to_face')
                        {
                            $modeTypeFaceToFace='TRUE';
                              $modeValue1=$this->data['Communication'][0]['mode'];
                          }
                          else if($this->data['Communication'][0]['mode_type']=='phone')
                            {
                              $modeTypePhone='TRUE';
                                $modeValue2=$this->data['Communication'][0]['mode'];
                              }
                              
                              else
                              {
                                 $modeTypeVideo='TRUE';
                                 $modeValue3=$this->data['Communication'][0]['mode'];
                           }
                          
                        
                        
                        }
                        if(isset($this->data['Communication'][1]['mode_type']) && $this->data['Communication'][1]['mode_type'] != '')
                        {
                            
                          if($this->data['Communication'][1]['mode_type']=='face_to_face')
                        {
                            $modeTypeFaceToFace='TRUE';
                              $modeValue1=$this->data['Communication'][1]['mode'];
                          }
                          else if($this->data['Communication'][1]['mode_type']=='phone')
                            {
                               $modeTypePhone='TRUE';
                                   $modeValue2=$this->data['Communication'][1]['mode'];
                              }
                              
                              else
                              {
                                 $modeTypeVideo='TRUE';
                                 $modeValue3=$this->data['Communication'][1]['mode'];
                           }
                          
                        
                        
                        }
                        if(isset($this->data['Communication'][2]['mode_type']) && $this->data['Communication'][2]['mode_type'] != '')
                        {
                            if($this->data['Communication'][2]['mode_type']=='face_to_face')
                          {
                            $modeTypeFaceToFace='TRUE';
                              $modeValue1=$this->data['Communication'][2]['mode'];
                          }
                            else if($this->data['Communication'][2]['mode_type']=='phone')
                            {
                               $modeTypePhone='TRUE';
                                   $modeValue2=$this->data['Communication'][2]['mode'];
                              }
                              
                              else
                              {
                                 $modeTypeVideo='TRUE';
                                 $modeValue3=$this->data['Communication'][2]['mode'];
                           }
                          
                          
                        }
                        
                        
                         ?>
                    <?php if($modeTypePhone=='TRUE' && trim($modeValue2)!='') {?>
                    <span style="display:inline"><br></span>
                    <span style="color:rgb(33,33,33);display:inline"><?php echo $modeValue2; ?></span>
                    <?php }?>
                    <?php if($city != '') {?>
                    <span style="display:block"></span> <span class="sc-location"><?php echo($city) ;?>, <?php echo($state) ;?></span>
                    <?php }?>

                </p>
                <?php
                    $socialCount=0;
                    $linkedinURL = '';
                                             $twitterURL ='';
                                             $googleURL ='';
                                             $facebookURL ='';
                                             $youtubeURL ='';
                    while(count($this->data['Social'])>$socialCount)
                    {
                    
                    if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'http'))!==false)
                      {
                        $link=$this->data['Social'][$socialCount]['social_name'];
                      }
                      else
                      {
                        $link="https://".$this->data['Social'][$socialCount]['social_name'];
                      }
                      
                      $icon = $this->General->getIcon($this->data['Social'][$socialCount]['social_name']);
                      if($icon == 'media/linkedin.png')
                        $linkedinURL = $link;
                                                    if($icon == 'media/twitter.png')
                        $twitterURL = $link;
                                                    if($icon == 'media/Google+.png')
                        $googleURL = $link;
                                                    if($icon == 'media/facebook.png')
                        $facebookURL = $link;
                      if($icon == 'media/youtube.png')
                        $youtubeURL = $link;
                      $socialCount++;
                      }?>
                <p style="font-family:Helvetica,Arial,sans-serif;font-size:12px;line-height:14px">
                    <?php if(($this->data['UserReference']['business_website']) !=''){
                        $link ='';
                        $url ='';
                          if(($pos =strpos($this->data['UserReference']['business_website'],'http'))!==false)
                        {
                        $link=$this->data['UserReference']['business_website'];
                        }
                        else
                        {
                        $link="https://".$this->data['UserReference']['business_website'];
                        }
                                            ?>
                    <a class="bussness-link" href="<?php echo($link);?>" target="_blank">
                      <img width="24" height="24" src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-business-new.svg" alt="mb-business">
                    </a>
                    <?php }?>
                    <?php if($linkedinURL !=''){
                        if(($pos =strpos($linkedinURL,'http'))!==false)
                        {
                        $link=$linkedinURL;
                        }
                        else
                        {
                        $link="https://".$linkedinURL;
                        }?>
                    <a href="<?php echo($link);?>" target="_blank">
                    <img width="24" height="24" src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-linked1.svg" alt="Linkedin">
                    </a>
                    <?php }?>
                    <?php               $urlk1 = ($this->data['User']['url_key']);
                        $displink1=SITE_URL.strtolower($urlk1);
                                                    ?>
                    <a href="<?php echo($displink1);?>">
                    <img width="24" height="24" src="<?php echo(SITE_URL)?>yogesh_new1/images/mg-guild-new.svg" alt="GUILD Profile">
                    </a>
                    <?php if($twitterURL !=''){?>
                    <a style="text-decoration:none" href="<?php echo($twitterURL);?>" target="_blank">
                    <img width="24" height="24" src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-twitter.svg" alt="Twitter">
                    </a>
                    <?php }?>
                    <?php if($facebookURL !=''){?>
                    <a href="<?php echo($facebookURL);?>" target="_blank">
                    <img width="24" height="24" src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-facebook.svg" alt="Facebook">
                    </a>
                    <?php }?>
                    <?php if($googleURL !=''){?>
                    <a href="<?php echo($googleURL);?>" target="_blank">
                    <img width="24" height="24" src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-google.svg" alt="Google Plus">
                    </a>
                    <?php }?>
                    <?php if($youtubeURL !=''){?>
                    <a href="<?php echo($youtubeURL);?>" target="_blank">
                    <img width="24" height="24" src="<?php echo(SITE_URL)?>yogesh_new1/images/mb-youtube.svg" alt="Youtube">
                    </a>
                    <?php }?>
                </p>
              </div>
              <div class="singbox-notice">
                NOTE: The contents of this email and any attachments are confidential. They are intended for the named recipient(s) only. If you have received this email by mistake, please notify the sender immediately.
              </div>
          </div>
          <div>
          <div class="my-badges-iframe">
              <div class="container_box con-box">
                  <h3><strong>GUILD Member Logo</strong> (light)</h3>
                  <iframe frameborder="0" height="80" marginheight="0" marginwidth="0" scrolling="no" src="<?php echo(SITE_URL)?>members/badges/?id=<?php echo($this->data['User']['url_key']);?>&type=logo1" width="200"></iframe>
                  <div class="overlay" onclick="TINY.box.show({html:document.getElementById('mg-div').innerHTML,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">
                      <p>Click for code snippet.</p>
                  </div>
              </div>
              <div class="container_box con-box">
                  <h3><strong>GUILD Member Logo</strong> (dark)</h3>
                  <iframe id="dark_bedge" onload="doIt()" frameborder="0" height="80" marginheight="0" marginwidth="0" scrolling="no" src="<?php echo(SITE_URL)?>members/badges/?id=<?php echo($this->data['User']['url_key']);?>&type=logo2" width="200"></iframe>
                  <div class="overlay" onclick="TINY.box.show({html:document.getElementById('mg_r-div').innerHTML,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">
                      <p>Click for code snippet.</p>
                  </div>
              </div>
          </div>
          <div class="my-badges-iframe">
              <div class="container_box">
                  <h3><strong>GUILD Badge</strong> (light)</h3>
                  <iframe src="<?php echo(SITE_URL)?>members/badges/?id=<?php echo($this->data['User']['url_key']);?>&type=answers2" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="0" marginwidth="0" scrolling="no" allowtransparency="true"></iframe>
                  <div class="overlay-lg" onclick="TINY.box.show({html:document.getElementById('ans-div').innerHTML,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">
                      <p>Click for code snippet.</p>
                  </div>
              </div>
              <div class="container_box">
                  <h3><strong>GUILD Badge</strong> (dark)</h3>
                  <iframe src="<?php echo(SITE_URL)?>members/badges/?id=<?php echo($this->data['User']['url_key']);?>&type=answers1" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="0" marginwidth="0" scrolling="no" allowtransparency="true"></iframe>
                  <div class="overlay-lg" onclick="TINY.box.show({html:document.getElementById('ans_r-div').innerHTML,fixed:true,mask:true,maskid:'blackmask',maskopacity:90,close:true})">
                      <p>Click for code snippet.</p>
                  </div>
              </div>
          </div>
          <div class="my-badges-iframe">
            <div class="hidden" id="mg-div">
                <div class="container_box">
                    <h3><strong>GUILD Member Logo</strong> (light)</h3>
                    <iframe frameborder="0" height="80" marginheight="5" marginwidth="5" scrolling="no" src="<?php echo(SITE_URL)?>members/badges/?id=<?php echo($this->data['User']['url_key']);?>&type=logo1" width="200"></iframe>
                    <textarea cols="40" class="txtarea" rows="5" id="first"><iframe frameborder="0" height="80" marginheight="5" marginwidth="5" scrolling="no" src="<?php echo(SITE_URL)?>members/badges/?id=<?php echo($this->data['User']['url_key']);?>&type=logo1" width="200"></iframe></textarea>
                    <span class="tiny-popup-button">
                      <a href="javascript:void(0);" class="btn btn-primary" onclick="javascript:copyToClipboard('#first');">Copy Code</a>
                    </span>
                </div>
            </div>
            <div class="hidden" id="mg_r-div">
                <div class="container_box">
                    <h3><strong>GUILD Member Logo</strong> (dark)</h3>
                    <iframe frameborder="0" height="80" marginheight="5" marginwidth="5" scrolling="no" src="<?php echo(SITE_URL)?>members/badges/?id=<?php echo($this->data['User']['url_key']);?>&type=logo2" width="200"></iframe>
                    <textarea cols="40" class="txtarea" rows="5" id="second"><iframe frameborder="0" height="80" marginheight="5" marginwidth="5" scrolling="no" src="<?php echo(SITE_URL)?>members/badges/?id=<?php echo($this->data['User']['url_key']);?>&type=logo2" width="200"></iframe></textarea>
                    <span class="tiny-popup-button">
                           <a href="javascript:void(0);" class="btn btn-primary" onclick="javascript:copyToClipboard('#second');">Copy Code</a>
                    </span>
                </div>
            </div>
            <div class="hidden" id="ans-div">
                <div class="container_box">
                    <h3><strong>GUILD Badge</strong> (light)</h3>
                    <iframe src="<?php echo(SITE_URL)?>members/badges/?id=<?php echo($this->data['User']['url_key']);?>&type=answers2" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe>
                    <textarea cols="40" class="txtarea" rows="5" id="third"><iframe src="<?php echo(SITE_URL)?>members/badges/?id=<?php echo($this->data['User']['url_key']);?>&type=answers2" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe></textarea>
                    <span class="tiny-popup-button">
                         <a href="javascript:void(0);" class="btn btn-primary" onclick="javascript:copyToClipboard('#third');">Copy Code</a>
                    </span>
                </div>
            </div>
            <div class="hidden" id="ans_r-div">
                <div class="container_box">
                    <h3><strong>GUILD Badge</strong> (dark)</h3>
                    <iframe src="<?php echo(SITE_URL)?>members/badges/?id=<?php echo($this->data['User']['url_key']);?>&type=answers1" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe>
                    <textarea cols="40" class="txtarea" rows="5" id="fourth"><iframe src="<?php echo(SITE_URL)?>members/badges/?id=<?php echo($this->data['User']['url_key']);?>&type=answers1" frameborder="0" height="120" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="no" allowtransparency="true"></iframe></textarea>
                    <span class="tiny-popup-button">
                        <a href="javascript:void(0);" class="btn btn-primary" onclick="javascript:copyToClipboard('#fourth');">Copy Code</a>
                    </span>
                </div>
            </div>
          </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <div class="badge-sidebar">
              <div class="bs-box">
                <h2>How do I add signature to my email?</h2>
                <p>1. You can make minor changes on the left (don't use Safari browser)</p>
                <p>2. Once final, just Select and Copy</p>
                <p>3. Paste in the Signature setting of your email service</p>
              </div>
              <div class="bs-box">
                <h2>How do I add the badge to my website or blog?</h2>
                <p>Click on the image you want to embed, then copy and paste the code into the HTML of your site.</p>
              </div>
              <div class="bs-box">
                <h2>Need help?</h2>
                <p>Call 1-866-511-1898 or email us at <a href="mailto:help@guild.im">help@guild.im</a>.</p>
              </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript" src="https://test.guild.im/js/tiny/tinybox.js"></script>

<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>-->
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>-->




<script type="text/javascript">

function copyToClipboard(element) {

    var $temp = $("<textarea>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    $temp.remove();
}
function doIt(){
  console.log("test");
}
</script>
  <style>
.txtarea {
    font-size: 12px;
    color: #444;
    background-color: #ddd;
    margin-left: 5px;
    padding: 8px;
    width: 341px;
    border: 2px dotted #ccc;
    margin-top: 4px;
}
</style>