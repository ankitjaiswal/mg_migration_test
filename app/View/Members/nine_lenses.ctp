
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth" style="margin-bottom:50px;">
 	    <div id="user-account">
	      <div class="account-form">	    
		  	<div class="onbanner">
		          <h1>                 
		            <div style="position: absolute;width: 100%;text-align: center;margin: auto;padding: 10px;"><span style="text-decoration: underline;">A Smarter Way to Engage Clients</span></div></h1>
                        
				  <div style="position:relative;top:80px;"><p><span style="font-family: 'proximanova semibold',Ubuntu;font-weight: normal;font-size: 18px;">A Cloud-Based Platform for Consultants.</span></div>	

                          <div class="more-button-search" style="text-align:center;">
		    <input  type="button"style="margin-top:60px;"
	           id="onclick" value="REQUEST A DEMO" type="button" onclick="window.open('http://www.9lenses.com/capture-insights')">
                   </div>
			</div>

                     <br/><br/><br/><br/>
                     <div class ="expertise">

                    <h2 style="color:#333333;font-size: 18px;font-family:Ubuntu;font-weight: bold;"><span style="padding-top:25px;">Software for Consultants by  &nbsp;</span>  <img src ="<?php echo(SITE_URL)?>imgs/9lenses-logo1.png" style="width:135px;height:26px;"></h2>
                     <p style="font-size:16px;text-align: left;">9Lenses provides a secure, connected way to manage your engagements and protect your IP. Our platform combines a rigorous interview structure with intuitive software and exceptional analytics so you can have a deeper, faster method for advising your clients.</p>
                     </div>
                
                   <table  width="100%">
<tbody>

<tr>
 <td><p style="font-size:16px;"><span style="font-weight:bold;">The Challenge:</span> Traditional, manual data collection and analysis methods result in disconnected data and fail to efficiently track your best questions and frameworks. At the same time, clients are demanding more value from their engagements with faster turnaround.</p></td>
<td width="10%"></td>
<td rowspan="4"><iframe width="350" height="250" src="https://www.youtube.com/embed/X8ka4ENNEyE" frameborder="0" allowfullscreen></iframe></td>
 
</tr>




<tr>
 <td><p style="font-size:16px;"><span style="font-weight:bold;">The 9Lenses Solution:</span> Independent consultants are using 9Lenses to conduct rapid client discovery and analysis while determining and scaling the best methods for solving their clients' business problems.</p></td>
<td width="10%"></td> 
<td></td>
</tr>
</tbody>
</table>


        
        
    

<br/><br/>
             <div class ="expertise">        
             <h2 style="color:#333333;font-size: 18px;font-family:Ubuntu;font-weight: bold;">Discount Available! <span style="font-weight:normal;font-size: 14px;">(10% off for Regular Members and 20% off for Premium Members)</span></h2>
              <?php if($this->Session->read('Auth.User.role_id') == Configure::read('App.Role.Mentor') && $this->Session->read('Auth.User.mentor_type') != 'Premium Member')
              { ?>
            <p style="font-size:14px;">Your promo code: <a href="http://www.9lenses.com/consulting-software" target="_blank">10OFF</a></p>
              <?php } elseif($this->Session->read('Auth.User.role_id') == Configure::read('App.Role.Mentor') && $this->Session->read('Auth.User.mentor_type') == 'Premium Member')
             {?>
            <p style="font-size:14px;">Your promo code: <a href="http://www.9lenses.com/consulting-software" target="_blank">20OFF</a></p>
            <?php }else{?>
             
             <p style="font-size:14px;">Discount codes are available to member consultants. <a href="javascript:void(0)" onclick='javascript:loginpopup();' title="Login to view">Login to view</a>.</p>
            <?php }?>
            </div>




</div>
</div>
</div>
</div>


<?php echo($this->Html->css(array('plans'))); ?>

<style>
.onbanner {
    text-align: center;
    overflow: hidden;
    color: white;
    font-size: 19px;
    font-weight: bold;
    background: url(../imgs/home-banner9lenses.jpg);
    background-repeat: no-repeat;
    height: 229px;
    line-height: 60px;
    margin-bottom: 25px;
}

.more-button-search input[type=button] {
    background: none repeat scroll 0 0 #D03135;
    color: #FFFFFF;
    cursor: pointer;
    font-weight: bold;
    height: 35px;
    text-align: center;
    margin: 0px;
    padding: 0px;
    width: 20%;
    -webkit-appearance: none;
    -webkit-border-radius: 0;
}

</style>




















