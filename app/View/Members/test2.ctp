<div class="page-content">
    <div class="container">
    	<div class="row">
	        <div class="col-sm-12">
	        	<h2 class="title-h2">Invoice Payment</h2>
	        </div>
	        <div class="col-lg-9 col-md-8">
	            <div class="invoice-box lg">
	                <ul>
	                    <li>
	                        <label>Project name:</label>
	                        <span>Consulting Services</span>
	                    </li>
	                    <li>
	                        <label>To:</label>
	                        <span>Sumit Jaiswal</span>
	                    </li>
	                    <li>
	                        <label>From:</label>
	                        <span>Ankit Jaiswal</span>
	                    </li>
	                    <li>
	                        <label>Invoice description:</label>
	                        <span>Testing for payment invoice</span>
	                    </li>
	                </ul>
	            </div>
	            <div class="text-right button-set right-btnset mt-30">
	                <input class="btn btn-primary" value="Pay Invoice" type="submit" />
	            </div>
	        </div>
	        <div class="col-lg-3 col-md-4">
	            <div class="invoice-box">
	                <h5>Invoice: IN0276-00</h5>
	                <ul>
	                    <li>
	                        <label>Price</label>
	                        <span>$500.00</span>
	                    </li>
	                    <li>
	                        <label>Discount</label>
	                        <span>$0.0</span>
	                    </li>
	                    <li>
	                        <label>Tax</label>
	                        <span>$0.0</span>
	                    </li>
	                    <li>
	                        <label>Total</label>
	                        <span>$500.00</span>
	                    </li>
	                </ul>
	            </div>
	        </div>
	    </div>
    </div>
</div>

<!-- js link -->
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery_validate_min_js.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jssor.slider-22.0.15.mini.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/script12.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/slick.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap-notify.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/quill.min.js"></script>
<script type="text/javascript"></script>