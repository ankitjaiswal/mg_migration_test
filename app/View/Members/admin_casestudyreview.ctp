	
	<div class="fieldset" style="display: block;">
    <div class="adminrightinner" style="padding:0px;">

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td width="5%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Id</td>
                        <td  width="40%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">CaseStudy Title</td>
                        <td  width="15%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">CaseStudy Status</td>


                         <td width="25%" align="center" valign="middle" class="Bdrrightbot Padtopbot6">CaseStudy Author</td>
                        <td width="15%" align="center" valign="middle" class="Bdrrightbot Padtopbot6">Action</td>
                    </tr>
                    <?php 
                    if(!empty($caseStudies) && count($caseStudies)>0)
                    { 
                        foreach($caseStudies as $msg)
                        {    
                     $casestudy_url = $msg['CaseStudy']['casestudy_url'];
 
                        ?>
                        <tr>
                            <td width="5%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo $msg['CaseStudy']['id']; ?></td>

                            <td  width="40%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo($this->Html->link($msg['CaseStudy']['title'],SITE_URL.'members/casestudy/'.$casestudy_url,array('escape'=>true, ))); ?></td>
                           <?php if($msg['CaseStudy']['specifier'] == 'Publish'){?>
                             <td  width="15%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Published</td>

                            <?php }else{?>
                              <td  width="15%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Draft</td>
                             <?php }?>
                              <?php 
                              $menu = ClassRegistry::init('UserReference');  //for class load in view
                              $author = $menu->find('first', array('conditions' => array('UserReference.user_id' => $msg['CaseStudy']['user_id'])));
                              $authorname = $author['UserReference']['first_name'].' '.$author['UserReference']['last_name'];
                              ?>
                             <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><a href="<?php echo(SITE_URL);?><?php echo($msg['User']['url_key']);?>"><?php echo($authorname);?></a></td>
                                                   
                            <td width="15%" align="center" valign="middle" class="Bdrbot ActionIcon">
                             <?php echo($this->Admin->getActionImage(array('edit' => array('controller' => 'members', 'action' => 'casestudy_adminedit')),$msg['CaseStudy']['id'])); ?>
                              <?php echo($this->Admin->getActionImage(array('delete' => array('controller' => 'members', 'action' => 'admin_deletecasestudy', 'token' => 'delete')), $msg['CaseStudy']['id'])); ?></td>
                            
                        </tr>	
                   <?php
                        }
                    }else{ ?>
                   <tr>
                        <td colspan="3" width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Record not found</td>           
                    </tr>
                   <?php } ?>   
                </table>
            </div>
            <?php
     //   }
        ?>

    </div>
</div>
<div class="clr"></div>
<?php //echo $this->element('admin/admin_paging', array("paging_model_name" => "NewsLetter", "total_title" => "NewsLetter")); ?>	 