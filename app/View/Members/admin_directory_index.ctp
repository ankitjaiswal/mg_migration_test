<?php echo($this->Html->script(array('fancybox/jquery.fancybox-1.3.4.pack', 'fancybox/jquery.mousewheel-3.0.4.pack'), false)); ?>
<?php //echo($this->Html->css(array('jquery.fancybox-1.3.4')), false); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.addAttribute').fancybox();
    });		
</script>

<div class="adminrightinner">
    <div class="tablewapper2 AdminForm">
        <h3 class="legend1">Search</h3>
        <?php echo($this->Form->create('DirectoryUser', array('url' => array('admin' => true, 'controller' => 'members', 'action' => 'admin_directory_index')))); ?>
        <table border="0" width="100%" class="Admin2Table">
            <tbody>
                <tr>
                    <td width="18%" valign="middle" class="Padleft26">Member's Name :</td>
                    <td><?php echo($this->Form->input('first_name', array('label' => false, 'div' => false, 'class' => 'InputBox'))); ?>  </td>
                    <td width="18%" valign="middle" class="Padright26">Member's Expertise :</td>
                    <td><?php echo($this->Form->input('area_of_expertise', array('label' => false, 'div' => false, 'class' => 'InputBox'))); ?>  </td>
       
         </tr>
                <tr>
                    <td valign="middle" class="Padleft26">&nbsp;</td>
                    <td align="left"> 

                        <?php echo($this->Form->submit('Search', array('div' => false, 'class' => 'submit_button'))); ?>                 
                    </td>
                    <td valign="middle" class="Padright26">&nbsp;</td>
                    <td align="right"> 

                        <?php echo($this->Form->submit('Search', array('div' => false, 'class' => 'submit_button'))); ?>                 
                    </td>

                </tr>              
            </tbody>
        </table> 
        <?php echo($this->Form->end()); ?>    
        <div style="clear: both;"></div>
    </div>
</div>

<div class="fieldset">
    <h3 class="legend">
		Directory Users
        <div class="total" style="float:right"> Total Members : <?php echo($this->request["paging"]['DirectoryUser']["count"]); ?>
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <?php echo($this->Form->create('DirectoryUser', array('name' => 'Admin', 'url' => array('controller' => 'members', 'action' => 'process')))); ?>    
        <input type="hidden" name="pageAction" id="pageAction"/>	 


<?php
        if (!empty($data)) {
            $exPaginator->options = array('url' => $this->passedArgs);
            $this->Paginator->options(array('url' => $this->passedArgs));
            ?> 

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td width="5%" align="center" valign="middle" class="Bdrrightbot Padtopbot6">
                            <input name="chkbox_n" id="chkbox_id" type="checkbox" value="" class="Chkbox" onclick="javascript:check_uncheck('Admin')" />
                        </td>	    

                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo($this->Paginator->sort('DirectoryUser.first_name', 'Name' )) ?></td>
                        <td width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo($this->Paginator->sort('DirectoryUser.username', 'Email' )) ?></td>
                        <td width="30%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Autocomplete expertise</td>
						<td width="10%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Quality</td>
						<td width="10%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Send Email</td>
                        <td  width="15%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Action</td>
                    </tr>	
                    <?php
                    foreach ($data as $value) {
                        ?>
                        <tr>
                            <td align="center" valign="middle" class="Bdrrightbot Padtopbot6">
        <?php echo($this->Form->checkbox('DirectoryUser.' . $value['DirectoryUser']['id'], array("class" => "Chkbox", 'value' => $value['DirectoryUser']['id']))) ?>
                            </td>		

                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php echo(ucwords($value['DirectoryUser']['first_name']) . ' ' . $value['DirectoryUser']['last_name']); ?></td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
        <?php echo($value['DirectoryUser']['username']); ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php echo($value['DirectoryUser']['area_of_expertise']); ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php echo($value['DirectoryUser']['quality']); ?>
                            </td>
							<td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
								<?php echo($value['DirectoryUser']['sendEmail']); ?>
							</td> 
                            <td align="center" valign="middle" class="Bdrbot ActionIcon">
        					<?php echo($this->Admin->getActionImage(array('edit' => array('controller' => 'members', 'action' => 'directory_edit'), 'delete' => array('controller' => 'members', 'action' => 'directory_delete', 'token' => $this->params['_Token']['key'])), $value['DirectoryUser']['id'])); ?>
                            </td>
                        </tr>	
                        <?php
                    }
                    ?>
                </table>
            </div>
            <?php
        }
        ?>
<?php if (!empty($data)) {
            echo($this->Form->end());
        } else {
            ?>
            <div style="color:blue, font-size:20; padding-bottom:30px;padding-top:30px;" align="center" ><strong>No Records Found.</strong></div>
        <?php } ?>
    </div>
</div>
<div class="clr"></div>
<?php echo $this->Element('Admin/admin_paging', array("paging_model_name" => "DirectoryUser", "total_title" => "User")); ?>	 