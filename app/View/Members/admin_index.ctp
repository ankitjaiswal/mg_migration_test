<?php echo($this->Html->script(array('fancybox/jquery.fancybox-1.3.4.pack', 'fancybox/jquery.mousewheel-3.0.4.pack'), false)); ?>
<?php //echo($this->Html->css(array('jquery.fancybox-1.3.4')), false); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.addAttribute').fancybox();
    });		
</script>

<div class="adminrightinner">
    <div class="tablewapper2 AdminForm">
        <h3 class="legend1">Search</h3>
        <?php echo($this->Form->create('UserReference', array('url' => array('admin' => true, 'controller' => 'members', 'action' => 'admin_index')))); ?>
        <table border="0" width="100%" class="Admin2Table">
            <tbody>
                <tr>
                    <td width="18%" valign="middle" class="Padleft26">Member's Name :</td>
                    <td><?php echo($this->Form->input('first_name', array('label' => false, 'div' => false, 'class' => 'InputBox'))); ?>  </td>
                </tr>
                <tr>
                    <td valign="middle" class="Padleft26">&nbsp;</td>
                    <td align="left"> 

                        <?php echo($this->Form->submit('Search', array('div' => false, 'class' => 'submit_button'))); ?>                 
                    </td>
                </tr>              
            </tbody>
        </table> 
        <?php echo($this->Form->end()); ?>    
        <div style="clear: both;"></div>
    </div>
</div>


<div class="fieldset">
    <h3 class="legend">
		Members
        <div class="total" style="float:right"> Total Members : <?php echo(count($data)); ?>
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                     <td width="5%" align="center" valign="middle" class="Bdrrightbot Padtopbot6">
                            <input name="chkbox_n" id="chkbox_id" type="checkbox" value="" class="Chkbox" onclick="javascript:check_uncheck('Admin')" />
                        </td>
                        <td  width="3%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Id</td>
                        <td  width="12%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Name</td>
                        <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Email</td>
                        <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Created Date</td>
                        <td  width="5%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">Status</td>
		          <td width="15%" align="left" valign="middle" class="Bdrrightbot Padtopbot6" style="padding-left:9px;">Access Specifier</td>
			   <td width="15%" align="left" valign="middle" class="Bdrrightbot Padtopbot6" style="padding-left:9px;">Type of Member</td>
                        <td  width="15%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Action</td>
                    </tr>
                    <?php 
                    if(!empty($data) && count($data)>0)
                    { 
                        foreach(array_reverse($data) as $key => $value)
                        {    
                        
                        ?>
                        <tr>
                            <td width="5%" align="center" valign="middle" class="Bdrrightbot Padtopbot6" style="padding-left:9px;"><?php echo($this->Form->checkbox('User.' . $value['User']['id'], array("class" => "Chkbox", 'value' => $value['User']['id']))) ?></td>
                            <td  width="3%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo($value['User']['id']); ?></td>
                            <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo(ucwords($value['UserReference']['first_name']) . ' ' . $value['UserReference']['last_name']); ?></td>
                            <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo($value['User']['username']); ?></td>  
                            <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo($value['User']['created']); ?></td>   
                            <td  width="5%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6"><?php
                                if ($value['User']['status'] == '1') {
                                    echo $this->Html->image("active.png", array("title" => "Active User", "alt" => "Active", "border" => 0));
                                } else {
                                    echo $this->Html->image("deactive.png", array("title" => "Deactive User", "alt" => "Deactive", "border" => 0));
                                }
                                ?></td>  
                            <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">                                
                                              <?php
								if($value['User']['access_specifier']=='draft')
									$accSpec = 'Draft';
								else
									$accSpec = 'Published';
								echo $this->Html->link($accSpec, array('controller' => 'members', 'action' => 'specifierChange',$value['User']['id'],$value['User']['access_specifier']), array("title" => "Change access specifier", "escape" => false));
                                ?></td>
                            <td  width="15%"  align="left" valign="middle" class="Bdrrightbot Padtopbot6">								
                                                      <?php
									echo $value['User']['mentor_type'];
								?></td>
                            <td  width="15%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">
                           <?php echo($this->Admin->getActionImage(array('edit' => array('controller' => 'members', 'action' => 'edit'), 'delete' => array('controller' => 'members', 'action' => 'delete', 'token' => $this->request['_Token']['key']), 'changepassword' => array('controller' => 'members', 'action' => 'changepassword', 'token' => $this->request['_Token']['key'])), $value['User']['id'])); ?>
        			<a href="<?php echo SITE_URL?>members/switch_user/<?php echo $value['User']['id']?>">
				<img class="viewstatusimg1" width="18" height="18" alt="SwitchUser" title="Switch User " src="<?php echo SITE_URL?>img/admin/cake.icon.png"></img>
				</a>
                           </td>    
                        </tr>	
                   <?php
                        }
                    }else{ ?>
                   <tr>
                        <td colspan="3" width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Record not found</td>           
                    </tr>
                   <?php } ?>   
                </table>
            </div>
     <?php if (!empty($data)) { ?>
            <div class="buttonwapper">
                <div class="Addnew_button">
                <?php echo $this->Html->link("Add New", array('controller' => 'members', 'action' => 'add'), array("title" => "", "escape" => false)); ?>
                </div>
                <?php echo($this->Form->submit('Activate', array('name' => 'activate', 'class' => 'cancel_button', "type" => "button", "onclick" => "javascript:return validateChk('Admin', 'activate');"))); ?>
                <?php echo($this->Form->submit('Deactivate', array('name' => 'deactivate', 'class' => 'cancel_button', "type" => "button", "onclick" => "javascript:return validateChk('Admin', 'deactivate');"))); ?>
            <?php echo($this->Form->submit('Delete', array('name' => 'delete', 'class' => 'cancel_button', "type" => "button", "onclick" => "javascript:return validateChk('Admin', 'delete');"))); ?>
            </div>
            <?php
            echo($this->Form->end());
        } else {
            ?>
            <div style="color:blue, font-size:20; padding-bottom:30px;padding-top:30px;" align="center" ><strong>No Records Found.</strong></div>
            <div class="Addnew_button" style=" margin-top: 10px;">
            <?php echo $this->Html->link("Add New", array('controller' => 'members', 'action' => 'add'), array("title" => "", "escape" => false)); ?>
            </div>
        <?php } ?>

    </div>
</div>
<div class="clr"></div>
	 