<?php 
$facecm = '';
$phonecm = '';
$videocm = '';
$facevalue;
$phonevalue;
$videovalue;
if(isset($this->data['Communication'][0])){
		if($this->data['Communication'][0]['mode_type'] == 'face_to_face'){
			$facecm = 'checked';
			$facevalue = $this->data['Communication'][0]['mode'];
		}else if($this->data['Communication'][0]['mode_type'] == 'phone'){
			$phonecm = 'checked';
			$phonevalue = $this->data['Communication'][0]['mode'];
		}else if($this->data['Communication'][0]['mode_type'] == 'video'){
			$videocm = 'checked';
			$videovalue = $this->data['Communication'][0]['mode'];
		}
}
if(isset($this->data['Communication'][1]['mode_type'])){
		if($this->data['Communication'][1]['mode_type'] == 'face_to_face'){
			$facecm = 'checked';
			$facevalue = $this->data['Communication'][1]['mode'];
		}else if($this->data['Communication'][1]['mode_type'] == 'phone'){
			$phonecm = 'checked';
			$phonevalue = $this->data['Communication'][1]['mode'];
		}else if($this->data['Communication'][1]['mode_type'] == 'video'){
			$videocm = 'checked';
			$videovalue = $this->data['Communication'][1]['mode'];
		}
}
if(isset($this->data['Communication'][2]['mode_type'])){
		if($this->data['Communication'][2]['mode_type'] == 'face_to_face'){
			$facecm = 'checked';
			$facevalue = $this->data['Communication'][2]['mode'];
		}else if($this->data['Communication'][2]['mode_type'] == 'phone'){
			$phonecm = 'checked';
			$phonevalue = $this->data['Communication'][2]['mode'];
		}else if($this->data['Communication'][2]['mode_type'] == 'video'){
			$videocm = 'checked';
			$videovalue = $this->data['Communication'][2]['mode'];
		}
}
if(isset($this->data['Communication'][0]['id'])){
	echo($this->Form->hidden('Communication.0.id'));
}
if(isset($this->data['Communication'][1]['id'])){
	echo($this->Form->hidden('Communication.1.id'));
}
if(isset($this->data['Communication'][2]['id'])){
	echo($this->Form->hidden('Communication.2.id'));
}
?>


<?php 

 $topicarray = array();
if(empty($this->data['MemberIndustryCategory']['topic1'])){
 $topicarray = '';
}else{

if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] != -1 && $this->data['MemberIndustryCategory']['topic4'] != -1 && $this->data['MemberIndustryCategory']['topic5'] != -1){
 $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']].",".$categories[$this->data['MemberIndustryCategory']['topic3']].",".$categories[$this->data['MemberIndustryCategory']['topic4']].",".$categories[$this->data['MemberIndustryCategory']['topic5']];

}
else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] != -1 && $this->data['MemberIndustryCategory']['topic4'] != -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
 $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']].",".$categories[$this->data['MemberIndustryCategory']['topic3']].",".$categories[$this->data['MemberIndustryCategory']['topic4']];

}
else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] != -1 && $this->data['MemberIndustryCategory']['topic4'] == -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
 $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']].",".$categories[$this->data['MemberIndustryCategory']['topic3']];

}
else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] == -1 && $this->data['MemberIndustryCategory']['topic4'] == -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
 $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']];

}
else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] == -1 && $this->data['MemberIndustryCategory']['topic3'] == -1 && $this->data['MemberIndustryCategory']['topic4'] == -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
 $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']];

}
}
?>
<?php echo($this->Form->hidden('UserReference.select',array('value'=>$topicarray, 'id'=>'hiddenSelect')));?>
<?php

    $ioptions = array();
    $count = 0;
    $options[-1] = '';


	$industry_options = array();
	$category_options = array();
	$industry_options[-1] = '';
	
    foreach ($industry_categories as $value) {
        
        $ioptions[$value['IndustryCategory']['id']] = $value['IndustryCategory']['category'];
		
		if($value['IndustryCategory']['parent'] == 0) 
			$industry_options[$value['IndustryCategory']['category']] = $value['IndustryCategory']['category'];
    }
    
	foreach ($industry_categories as $value) {
		
		if($value['IndustryCategory']['parent'] > 0)  {
					
			if($category_options[$ioptions[$value['IndustryCategory']['parent']]] == '')
				$category_options[$ioptions[$value['IndustryCategory']['parent']]] = $value['IndustryCategory']['category'];
			else
				$category_options[$ioptions[$value['IndustryCategory']['parent']]] = $category_options[$ioptions[$value['IndustryCategory']['parent']]].'|'.$value['IndustryCategory']['category'];
		}
    }
						
	foreach ($category_options as $key => $value) {
			?>
		<input type="hidden" name="<?php echo($key); ?>" value="<?php echo($value);?>" id="<?php echo($key); ?>" />
		<?php
	}
	$industry_options = array_unique($industry_options);
	
	$industry1 = '';
	$industry2 = '';
	$industry3 = '';
	$category1_options = '';
	$category1_options = '';
	$category1_options = '';
	
	if(isset($this->data['MemberIndustryCategory']) && isset($this->data['MemberIndustryCategory']['industry1']) ) {
			
		$industry1 = $ioptions[$this->data['MemberIndustryCategory']['industry1']];
		$industry2 = $ioptions[$this->data['MemberIndustryCategory']['industry2']];
		$industry3 = $ioptions[$this->data['MemberIndustryCategory']['industry3']];
						
		$category1 = $ioptions[$this->data['MemberIndustryCategory']['category1']];
		$category2 = $ioptions[$this->data['MemberIndustryCategory']['category2']];
		$category3 = $ioptions[$this->data['MemberIndustryCategory']['category3']];
		
		$category11_options = explode("|",$category_options[$industry1]);
		$category22_options = explode("|",$category_options[$industry2]);
		$category33_options = explode("|",$category_options[$industry3]);
		
		foreach ($category11_options as $key => $value) {
			$category1_options[$value] = $value;
		}
		
		foreach ($category22_options as $key => $value) {
			$category2_options[$value] = $value;
		}
			
		foreach ($category33_options as $key => $value) {
			$category3_options[$value] = $value;
		}
	}
?>


<div class="main-wizard">
    <div class="container">
        <div class="row">
            <?php echo($this->Form->create('User', array('url' => array('controller' => 'members', 'action' => 'test3'),'type'=>'file','id'=>'profileForm')));?>
            <?php echo($this->Form->hidden('User.id')); ?>
            <?php echo($this->Form->hidden('User.username')); ?>
            <?php echo($this->Form->hidden('UserReference.id')); ?>			
            <div class="col-lg-12">			
                <div class="wizard-content">
                    <div class="wizard-title">
                        <h2>Profile Creation</h2>
                    </div>
                    <div class="wizard-body">
                        <div class="acc-btn" id="profile1">
                            <h5 class="selected">1</h5>
                            <span>Personal Information</span>
                        </div>
                        <div class="acc-content open">
                            <div class="acc-content-inner">
                                <div id="profileImage" class="personal-img">
									<?php
									if(isset($this->data['User']['id'])):
										$user_name = ucwords($this->data['UserReference']['first_name']).' '.ucwords($this->data['UserReference']['last_name']);
									else:
										$user_name = '';
									endif;
									if(isset($this->data['UserImage'][0]['image_name']) && $this->data['UserImage'][0]['image_name'] !=''){
										if($this->data['User']['role_id']==2){
											$image_path = MENTORS_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'];
											echo($this->Html->image($image_path,array('alt'=>$user_name)));	
										}else{
											$image_path = MENTEES_IMAGE_PATH.'/'.$this->data['User']['id'].'/'.$this->data['UserImage'][0]['image_name'];
											echo($this->Html->image($image_path,array('alt'=>$user_name)));	
										}	
										echo($this->Form->hidden($this->data['UserImage'][0]['image_name'],array('value'=>$this->data['UserImage'][0]['image_name'],'id'=>'image_name')));
									}else{?>
										<img src="<?php echo(SITE_URL)?>yogesh_new1/images/personal-user.jpg" alt="personal-user">
										<?php echo($this->Form->hidden('profile.png',array('value'=>'profile.png','id'=>'image_name')));
									}?>
						
                                    <div class="btn btn-black"><?php echo($this->Element('Mentorsuser/add_image'));?></div>
                                </div>
                                <div class="personal-detail">
                            		<div class="w100per">
	                                    <div class="form-group">
	                                        <input type="text" name="firstname" value="<?php echo($this->data['UserReference']['first_name']);?>" class="form-control" placeholder="First Name" id="firstname">
	                                    </div>
	                                    <div class="form-group acc-padding">
	                                        <input type="text" name="lastname" value="<?php echo($this->data['UserReference']['last_name']);?>" class="form-control" placeholder="Last Name" id="lastname">
	                                    </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="zipcode" value="<?php echo($this->data['UserReference']['zipcode']);?>" class="form-control" placeholder="ZIP Code" id="zipcodevalue">
                                        <?php  echo($this->Form->hidden('UserReference.country', array('value'=>'US')));?>
                                    </div>
                                    <div class="form-group headline">
                                        <span>(<span id="headlinenoOfChars"><?php echo(140 - strlen($this->data['UserReference']['linkedin_headline']))?></span> characters remaining)</span>
                                        <textarea name="profileheadline" class="form-control" placeholder="Your professional headline" id="profileheadline"><?php echo($this->data['UserReference']['linkedin_headline']);?></textarea>
                                    </div>
                                </div>
                                <div class="publish-btm">
                                    <div class="button-publish"> 
                                        <a class="rest" id="continuepersonalinfo" href="javascript:saveasdraft();">Continue</a>
                                    </div>
                                    <div class="form-group sample-profile">
                                        <span>Sample Profile:</span>
                                        <ul>
                                            <li><a href="<?php echo(SITE_URL)?>mark.palmer" target="_blank">guild.im/mark.palmer</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="acc-btn" id="profile2">
                            <h5>2</h5>
                            <span>Professional Summary</span>
                        </div>
                        <div class="acc-content profile2">
                            <div class="acc-content-inner">
                                <div class="form-group professional-summary">
                                    <span>(<span id="psummarynoOfChars"><?php echo(3000 - strlen($this->data['UserReference']['background_summary']));?></span> characters remaining)</span>
                                    <textarea name="professionalsummary" class="form-control" placeholder="Professional Summary" id="professionalsummary"><?php echo($this->data['UserReference']['background_summary']);?></textarea>
                                </div>

                                <div class="p-food-row" id="indCatid1">
	                                <div class="form-group food summary-food">
	                                    <label>Industry</label>
	                                    <?php echo $this->Form->select('UserReference.industry1',$industry_options,array('div' =>false,'class'=>'form-control','empty' => false,'value'=>$industry1));?>
	                                </div>
	                                <div class="form-group farming summary-farming">
	                                    <label>Category</label>
	                                    <?php echo $this->Form->select('UserReference.category1',$category1_options,array('div' =>false,'class'=>'form-control','empty' => false,'value'=>$category1));?> 
	                                </div>
	                                <div class="form-group social pull-right">
	                                    <label>&nbsp;</label>
	                                    <span><a id="id1" href="javascript:void(0);" onclick="addbox();">[+]</a></span>
	                                </div>
                                </div>

                                <div class="p-food-row" id="indCatid2" style="display: none;">
                                <div class="form-group food">
                                    <label>Industry</label>
                                    <?php echo $this->Form->select('UserReference.industry2',$industry_options,array('div' =>false,'class'=>'form-control','empty' => false,'value'=>$industry2));?>
                                </div>
                                <div class="form-group farming">
                                    <label>Category</label>
                                    <?php echo $this->Form->select('UserReference.category2',$category2_options,array('div' =>false,'class'=>'form-control','empty' => false,'value'=>$category2));?> 
                                </div>
                                <div class="form-group social pull-right">
                                    <label>&nbsp;</label>
                                    <span><a id="id2" href="javascript:void(0);" onclick="removebox(this);">[-]</a></span>
                                </div>
                            	</div>

                                <div class="p-food-row" id="indCatid3" style="display: none;">
                                <div class="form-group food">
                                    <label>Industry</label>
                                    <?php echo $this->Form->select('UserReference.industry3',$industry_options,array('div' =>false,'class'=>'form-control','empty' => false,'value'=>$industry3));?>
                                </div>
                                <div class="form-group farming">
                                    <label>Category</label>
                                    <?php echo $this->Form->select('UserReference.category3',$category3_options,array('div' =>false,'class'=>'form-control','empty' => false,'value'=>$category3));?> 
                                </div>
                                <div class="form-group social pull-right food-last">
                                    <label>&nbsp;</label>
                                    <span><a id="id3" href="javascript:void(0);" onclick="removebox(this);">[-]</a></span>
                                </div>
                            	</div>

                            	

                                <div class="form-group client">
                                    <label>Areas of Expertise</label>
                                    <span class="pull-right">(<span id="noOftopics"></span> Topics remaining)</span>
                                    <input type="text" name="data[City][keyword]" id="CityKeyword" value="" class="form-control" placeholder="Choose Topics">
							  		<div id="div0" style="display: none; border-bottom:1px solid #BEBEBE;margin-bottom:20px;padding-bottom:10px; background:#ccc; border-radius:5px;">
								        <span id='close' onclick='deleteThis(0); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
								        <p id="topic0" style="padding-top: 10px; padding-bottom: 0px; padding-left:8px; font-weight:normal;display: inline-block"></p>
									</div>
									<div id="div1" style="display: none; border-bottom:1px solid #BEBEBE;margin-bottom:20px;padding-bottom:10px; background:#ccc; border-radius:5px;">
								        <span id='close' onclick='deleteThis(1); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
								        <p id="topic1" style="padding-top: 10px; padding-bottom: 0px; padding-left:8px; font-weight:normal;display: inline-block""></p>
									</div>
									<div id="div2" style="display: none; border-bottom:1px solid #BEBEBE;margin-bottom:20px;padding-bottom:10px; background:#ccc; border-radius:5px;">
								        <span id='close' onclick='deleteThis(2); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
								        <p id="topic2" style="padding-top: 10px; padding-bottom: 0px;padding-left:8px; font-weight:normal;display: inline-block""></p>
									</div>
									<div id="div3" style="display: none; border-bottom:1px solid #BEBEBE;margin-bottom:20px;padding-bottom:10px; background:#ccc; border-radius:5px;">
								        <span id='close' onclick='deleteThis(3); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
								        <p id="topic3" style="padding-top: 10px; padding-bottom: 0px;padding-left:8px; font-weight:normal;display: inline-block""></p>
									</div>
									<div id="div4" style="display: none; border-bottom:1px solid #BEBEBE;margin-bottom:20px;padding-bottom:10px; background:#ccc; border-radius:5px;">
								        <span id='close' onclick='deleteThis(4); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
								        <p id="topic4" style="padding-top: 10px; padding-bottom: 0px;padding-left:8px; font-weight:normal;display: inline-block""></p>
									</div>
                                </div>
                                <div class="publish-btm mtop0">
                                    <div class="button-publish"> 
                                        <a class="rest" id="continueprofessionalsummary" href="javascript:saveasdraft();">Continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="acc-btn" id="profile3">
                            <h5>3</h5>
                            <span>Additional Info</span>
                        </div>
                        <div class="acc-content">
                            <div class="acc-content-inner topic">
                                <div class="form-group client">
                                    <label>Client List</label>
                                    <textarea name="pastclients" class="form-control" cols="50" placeholder="List your past and present clients here. If a client name is confidential, indicate industry and size" id="pastclients"><?php echo($this->data['UserReference']['past_clients']);?></textarea>
                                </div>                                
                                <div class="row">
                                	<div class="col-sm-12">
                                		<div class="form-group client">
		                                    <input type="text" name="businesswebsite" value="<?php echo($this->data['UserReference']['business_website']);?>" class="form-control" placeholder="Enter your business website URL here" id="businesswebsite">
		                                </div>
                                	</div>
                                </div>
                                <div class="fieldset-box">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group client">
					                         	<?php if(isset($this->data['Availablity']['id'])){
							                        echo($this->Form->hidden('Availablity.id'));
					                          	}?>
                                                <label class="avability">Availability</label>
                                                <input type="text" name="availablitytime" value="<?php echo($this->data['Availablity']['day_time']);?>" class="form-control" placeholder="e.g. Business hours (ET). By appointment" id="availablitytime">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4 styled-form">
                                        	<div class="form-group client">
                                             <input  id="a1" class="group1" type="checkbox" name="communication1checkbox" value="face_to_face" <?php echo($facecm); ?> /> 
                                            <label class="social-contact-left" for="a1"><i class="fa fa-map-marker"></i><span> In Person</span></label>
                                            <input type="text" name="communication1" id="communication1" value= "<?php echo ($facevalue)?>"  class="form-control" placeholder="e.g. New York, NY">
                                        	</div>
                                        </div>
                                        <div class="col-sm-4 styled-form">
                                        	<div class="form-group client">	
                                            <input  id="a2" class="group1" type="checkbox" name="communication2checkbox" value="phone" <?php echo($phonecm); ?> />
                                            <label class="social-contact-left" for="a2"><i class="fa fa-phone"></i><span> Phone</span></label>
                                            <input type="text" name="communication2" id="communication2" value= "<?php echo ($phonevalue)?>"  class="form-control" placeholder="Business Phone Number">
                                        	</div>
                                    	</div>
                                        <div class="col-sm-4 styled-form">
                                        	<div class="form-group client">	
                                            <input  id="a3" class="group1" type="checkbox" name="communication3checkbox" value="video" <?php echo($videocm); ?> />
                                            <label class="social-contact-left" for="a3"><i class="fa fa-television"></i><span> Online</span></label>
                                            <input type="text" name="communication3" id="communication3" value= "<?php echo ($videovalue)?>"  class="form-control" placeholder="e.g. Skype, Google Hangouts">
                                        	</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="publish-btm">
                                    <div class="button-publish"> 
                                        <a class="rest" id="continueadditionalinfo" href="javascript:saveasdraft();">Continue</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="acc-btn" id="profile4">
                            <h5>4</h5>
                            <span>Social Profile</span>
                        </div>
                        <div class="acc-content">
                            <div class="acc-content-inner social" id="links">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group social-add socialinputbox" id="socialinputbox">
				                         	<?php
					                            $count = 0;
					                            $socialData = array();
					                            if (isset($this->data['Social'])) {
					                            	foreach ($this->data['Social'] as $key => $value) {
					                              		if ($value != '') {
							                               $socialData[] = $value;
							                               $count++;
					                               		}
					                              	}
					                               $this->request->data['Social'] = $socialData;
				                             	} if (!$count) {
													$count = 1;
													$socialData[] = array('social_name' => '');
			                              	}?>										

				                          	<?php  for ($key = 0; $key < $count; $key++) {
							                   	if(isset($this->data['Social'][$key]['id'])){
								                	echo($this->Form->hidden('Social.' . $key . '.id'));}
						                	?>
						             
				   			              	<?php echo($this->Form->input('Social.' . $key . '.social_name', array('value' => $socialData[$key]['social_name'],'label' => false, 'div' => false,'class'=>'form-control','placeholder'=>'e.g. twitter.com/name')));
				   			              	?>

							                <?php if ($key == 0) {?>
				                                    	<span><a href="javascript:;" id="addSocial">[+]</a></span>
								                    <?php } else { ?>
				                                    	<span><a href="javascript:;" id="remscociallink[1]">[-]</a></span>
							                 <?php } ?>
			                            </div>
	     		                            
     		                            </div>

                                  		<?php } ?>
                                </div>
                                <div class="publish-btm mtop0">
                                    <div class="button-publish"> 
                                        <a class="rest" href="javascript:saveasdraft();" id="continuesocialprofiles">Continue</a>
                                    </div>
                                </div>                              
                            </div>
                        </div>
                        <div class="acc-btn" id="profile5">
                            <h5>5</h5>
                            <span>Engagement Info</span>
                        </div>
                        <div class="acc-content">
                            <div class="acc-content-inner topic">
                                <div class="form-group typical">
                                    <label>Fee (Will not be made public)</label>
                                    <input type="text" name="consultationfee" value="<?php echo($this->data['UserReference']['fee_regular_session']);?>" class="form-control" placeholder="Typical Engagement fee $" id="engfee">
                                    <span>Per</span>
                                    <select class="form-control" name="regularsessionper" value="<?php echo($this->data['UserReference']['regular_session_per']);?>" id="engper">
                                        <option value="1">Hour</option>
                                        <option value="2">Month</option>
                                        <option value="3">Engagement</option>
									<option value="4">Day</option>
                                    </select>
                                </div>
                                <div class="form-group client">
                                    <span class="pull-right">(<span id="eoverviewnoOfChar"><?php echo(1000 - strlen($this->data['UserReference']['feeNSession']))?></span> Characters remaining)</span>
                                    <textarea name="engoverview" class="form-control" id="engoverview" cols="50" placeholder="What does a typical engagement look like? Indicate duration, methodology, etc."><?php echo($this->data['UserReference']['feeNSession']);?></textarea>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="data[User][access_specifier]"  value="draft" id="uaccessspecifier" />


                        <div class="publish-btm last">
                            <div class="button-publish pull-right">
                                <a class="rest" href="javascript:saveasdraft();">Save as Draft</a>
                                <!-- <a class="btn btn-primary" href="javascript:void(0);" id="submitProfile">Preview</a> -->
                                <input id="submitProfile" type="submit" class="btn btn-primary submitProfile" value="Publish" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php echo($this->Form->end()); ?>
        </div>
    </div>
</div>


	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/script12.js"></script>

	<?php //echo($this->Html->css(array('opentip'))); ?>
	<?php //echo( $this->Html->script(array('opentip-jquery')));?>

	<script type="text/javascript">
	    var animTime = 300,
	        clickPolice = false;
	 
	     
	        //return this;

	     //jQuery('.acc-content').eq(3).center();

	    jQuery(".acc-btn").click(function() {
	        if(!clickPolice){
	            clickPolice = true;
	            var currIndex = jQuery(this).index('.acc-btn'),
	                targetHeight = jQuery('.acc-content-inner').eq(currIndex+1).outerHeight();
	            jQuery('.acc-btn h5').removeClass('selected');
	            jQuery(this).find('h5').addClass('selected');
	            jQuery('.acc-content').removeClass('selected');
	            jQuery(this).next('div').addClass('selected');

	           jQuery('.acc-content').stop().animate({ height: 0}, animTime);
	           jQuery('.acc-content').eq(currIndex).animate({ height: targetHeight}, animTime);
	            window.scrollTo(250, 250);
				var d = jQuery('.acc-content').eq(currIndex);			
	        	setTimeout(function(){ clickPolice = false; }, animTime);
	        }
	    });
	    jQuery("#continuepersonalinfo").click(function(){
	    	var personalinfo = ['firstname', 'lastname', 'zipcode', 'profileheadline'];

			$.each(personalinfo, function( index, value ) {
			  jQuery('#profileForm').data('bootstrapValidator').revalidateField(value);
			});

			var result = [];
			$.each(personalinfo, function( index, value ) {
				result.push( jQuery('#profileForm').data('bootstrapValidator').isValidField(value));
			});

			if ($.inArray(false, result) === -1)
			{
			   	jQuery("#profile2").click();
			  	return true; 
			}
	    });
	    jQuery("#continueprofessionalsummary").click(function(){
	  			var personalinfo =  ['professionalsummary'];

	  			$.each(personalinfo, function( index, value ) {
	  			  jQuery('#profileForm').data('bootstrapValidator').revalidateField(value);
	  			});

	  			var result = [];
	  			$.each(personalinfo, function( index, value ) {
	  				result.push( jQuery('#profileForm').data('bootstrapValidator').isValidField(value));
	  			});

			 	if ($.inArray(false, result) === -1)
			 	{
			 	   	jQuery("#profile3").click();
			 	  	return true; 
			 	}
	    });
	    jQuery("#continueadditionalinfo").click(function(){

	    	var additonalinfo = ['availablitytime','communication1', 'communication2', 'communication3'];
	    	var jsarray = [];
	    	

	    	if(jQuery("#a1").prop("checked") == true || jQuery("#a2").prop("checked") == true || jQuery("#a3").prop("checked") == true){
	    		var bootstrapValidator = $('#profileForm').data('bootstrapValidator');
	    		bootstrapValidator.enableFieldValidators('communication1', false);
	    		bootstrapValidator.enableFieldValidators('communication2', false);
	    		bootstrapValidator.enableFieldValidators('communication3', false);
	    		  if(jQuery("#a1").prop("checked") == true){
	    		  	jsarray.push('communication1');
	    		  	bootstrapValidator.enableFieldValidators('communication1', true);
	    		  }
	    		  if(jQuery("#a2").prop("checked") == true){
	    		  	jsarray.push('communication2');
	    		  	bootstrapValidator.enableFieldValidators('communication2', true);
	    		  }
	    		  if(jQuery("#a3").prop("checked") == true){
	    		  	jsarray.push('communication3');
	    		  	bootstrapValidator.enableFieldValidators('communication3', true);
	    		  }
					jsarray.push('availablitytime');
					console.log(jsarray);

					$.each(jsarray, function( index, value ) {
					jQuery('#profileForm').data('bootstrapValidator').revalidateField(value);
					});

					var result = [];
					$.each(jsarray, function( index, value ) {
					result.push( jQuery('#profileForm').data('bootstrapValidator').isValidField(value));
					});

					console.log(result);
					if ($.inArray(false, result) === -1)
					{
						jQuery("#profile4").click();
						return true; 
					}

	    	} else {
		  			$.each(additonalinfo, function( index, value ) {
		  			  jQuery('#profileForm').data('bootstrapValidator').revalidateField(value);
		  			});

		  			var result = [];
		  			$.each(additonalinfo, function( index, value ) {
		  				result.push( jQuery('#profileForm').data('bootstrapValidator').isValidField(value));
		  			});

		  			console.log(result);
				 	if ($.inArray(false, result) === -1)
				 	{
				 	   	jQuery("#profile4").click();
				 	  	return true; 
				 	}
	    	}	
  
	    });
	    jQuery("#continuesocialprofiles").click(function(){
			  	jQuery("#profile5").click();
			 	return true; 
	    });
	</script>

	<script type="text/javascript">

		Array.prototype.remove = function() {
		    var what, a = arguments, L = a.length, ax;
		    while (L && this.length) {
		        what = a[--L];
		        while ((ax = this.indexOf(what)) !== -1) {
		            this.splice(ax, 1);
		        }
		    }
		    return this;
		};
		console.log(1);
		var topicVal = [-1,-1,-1,-1,-1];
		var topicCount = 0;
				
		function monkeyPatchAutocomplete() {

		    jQuery.ui.autocomplete.prototype._renderItem = function( ul, item) {
		        var re = new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + this.term + ")(?![^<>]*>)(?![^&;]+;)", "gi");
		        var t = item.label.replace(re,"<span style='font-weight:bold;color:#434343;'>" + 
		        		"$&" + 
		                "</span>");
		        return jQuery( "<li></li>" )
		            .data( "item.autocomplete", item )
		            .append( "<a>" + t + "</a>" )
		            .appendTo( ul );
		    };
		}

		function getKeywords(){

				var allKeywords = <?php echo json_encode($allKeywords); ?>;
				
				return allKeywords;
			}
			
		function deleteThis(element) {
			var topic = jQuery('#noOftopics').html();
			topicVal[element] = -1;
			
			for( var i = 0 ; i < 5 ; i ++) {
				
				if(topicVal[i] == -1) {
					jQuery('#div'+i).css('display','none');
					jQuery('#topic'+i).html('');
		                        jQuery("#noOftopics").html(parseInt(topic) + parseInt(1));
				} else {
					jQuery('#div'+i).css('display','inline-block');
					jQuery('#topic'+i).html(topicVal[i]);
		                        jQuery("#noOftopics").html(parseInt(topic) + parseInt(1));
				}
			}
		}
			
		jQuery(document).ready(function(){
			        jQuery("#noOftopics").html(5);
				var hidVal = jQuery('#hiddenSelect').val();
				if(hidVal != ''){
					var hidValArr = hidVal.split(",");
			                var total = 0;
					for(var i = 0; i < hidValArr.length; i++){
						topicVal[i] = hidValArr[i];
						jQuery('#div'+i).css('display','inline-block');
						jQuery('#topic'+i).html(hidValArr[i]);
		                                total++;
					}
		                 jQuery("#noOftopics").html(5-total);
				}
				monkeyPatchAutocomplete();
				
				var CityKeyword = jQuery('#CityKeyword');
			
				CityKeyword.autocomplete({
				minLength    : 1,
				source        : getKeywords(),
				source		: function(request, response) {
					
				        var results = jQuery.ui.autocomplete.filter(getKeywords(), request.term);
				        
				       	for ( var x = 0 ; x < 5 ; x++) {
				        	if(topicVal[x] != -1)
				        		results.remove(topicVal[x]);
				        }
				        
				        results = results.slice(0, 5);
				        results.push('Add <b>"' + jQuery("#CityKeyword" ).val() + '"</b>');
				        response(results); //show 3 items.
				    },
				focus: function( event, ui ) {
					
					if(ui.item.value[0] == "A")
					return false;

			     },
			     select: function( event, ui ) {
		                             //var t = jQuery("#noOftopics").val();
					     var toAdd = ui.item.value;
		                              if(ui.item.value[1] == "d"){
		                                var total = toAdd.length;
		                                var final = total-5;
						toAdd = toAdd.substring(8,final);
		                                 }

					var changed = -1;
					
					for (var i = 0; i < 5; i++) {
						
						if(topicVal[i] == -1) {
							changed = 1;

							topicVal[i] = toAdd;
		                                        jQuery("#noOftopics").html(4-i);
							break;
						}
					}
					
					if(changed == -1)
						alert("You can select up to 5 values");
					
					for( var i = 0 ; i < 5 ; i ++) {
				                 
						if(topicVal[i] == -1) {
							jQuery('#div'+i).css('display','none');
							jQuery('#topic'+i).html('');
		                                        
						} else {
							jQuery('#div'+i).css('display','inline-block');
							jQuery('#topic'+i).html(topicVal[i]);
		                                        
						}
		                              
					}
					
			        jQuery("#CityKeyword" ).val('');
			          
			        return false;
			       }
				});
			});


		jQuery(function() {
			jQuery('select').change(function() {
				var old_type = this.id.substring(13);

				type = old_type.substring(0,8);
				
				if(type == 'Industry') {

					var id = old_type.substring(8);
					
					var industry = this.value;
					
					var select_id = "#UserReferenceCategory"+id;
					
					var item = jQuery(select_id);
				
					item.find('option').remove();
						
									
					if(industry != -1) {
						
						var cats = document.getElementById(industry); 
						var cat_array = cats.value.split("|");
						    //alert(cat_array);
		                                var i = cat_array.indexOf("ALL CATEGORIES");
		                                if(i != -1) {
			                        cat_array.splice(i, 1);
		                                 }
		                                 
		                                     cat_array =  cat_array.sort();
		                                     cat_array.push('ALL CATEGORIES');
						

						for (i = 0; i < cat_array.length; i++) {
							item.append('<option value="' + cat_array[i] + '">' + cat_array[i] + '</option>');
				        }
			      } else {
			      	item.append('<option value="-1"> </option>');	
			      }
			          
		        }          
			});
		});
	</script>

	<script type="text/javascript">
		jQuery(document).ready(function(){

			var headTexts = jQuery("#profileheadline").val();
			jQuery("#headlinenoOfChars").html(140 - headTexts.length);
	    	
		 	function updateCountss (){
	        	var headTexts = jQuery("#profileheadline").val();

		       	if(headTexts.length < 140) {
					jQuery("#headlinenoOfChars").html(140 - headTexts.length);
		       	} else {
					jQuery("#headlinenoOfChars").html(0);
					jQuery("#profileheadline").val(headTexts.substring(0,140));
		       	}
		    }

		    jQuery("#profileheadline").keyup(function () {
		        updateCountss();
		    });
		    jQuery("#profileheadline").keypress(function () {
		        updateCountss();
		    });
		    jQuery("#profileheadline").keydown(function () {
		        updateCountss();
		    });
			var pTexts = jQuery("#professionalsummary").val();
			jQuery("#psummarynoOfChars").html(3000 - pTexts.length);
		 	function updateCounts (){
	    		var pTexts = jQuery("#professionalsummary").val();

		       	if(pTexts.length < 3000) {
					jQuery("#psummarynoOfChars").html(3000 - pTexts.length);
		       	} else {
					jQuery("#psummarynoOfChars").html(0);
					jQuery("#professionalsummary").val(pTexts.substring(0,3000));
		       	}
		    }

		    jQuery("#professionalsummary").keyup(function () {
		        updateCounts();
		    });
		    jQuery("#professionalsummary").keypress(function () {
		        updateCounts();
		    });
		    jQuery("#professionalsummary").keydown(function () {
		        updateCounts();
		    });
		 	var qText = jQuery("#engoverview").val();
	      	jQuery("#eoverviewnoOfChar").html(1000 - qText.length);
	    	
		 	function updateCount (){
	        	var qText = jQuery("#engoverview").val();

		       	if(qText.length < 1000) {
		           jQuery("#eoverviewnoOfChar").html(1000 - qText.length);
		       	} else {
		           jQuery("#eoverviewnoOfChar").html(0);
		           jQuery("#engoverview").val(qText.substring(0,1000));
		       	}
	    	}

		    jQuery("#engoverview").keyup(function () {
		        updateCount();
		    });
		    jQuery("#engoverview").keypress(function () {
		        updateCount();
		    });
		    jQuery("#engoverview").keydown(function () {
		        updateCount();
		    });
		});
	</script>



	<script src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrapValidator.min.js" type="text/javascript"></script>
	<script>
		jQuery(document).ready(function(){
			jQuery('#profileForm').bootstrapValidator({
			    trigger: 'blur',
			    fields: {
			        firstname: {
			            validators: {
			                notEmpty: {
			                    message: 'Do not leave blank'
			                },
			                regexp: {
			                    regexp: /^[a-zA-Z ]+$/,
			                    message: 'Only alphabets are allowed'
			                }
			            }
			        },
			        lastname: {
			            validators: {
			                notEmpty: {
			                    message: 'Do not leave blank'
			                },
			                regexp: {
			                    regexp: /^[a-zA-Z ]+$/,
			                    message: 'Only alphabets are allowed'
			                }
			            }
			        },
					zipcode: {
			            validators: {
			                notEmpty: {
			                    message: 'Do not leave blank'
			                },
							stringLength: { 
							message: 'ZIP should be maximum 5 digits', 
							max: function (value, validator, $field) { 
							return 5 - (value.match(/\r/g) || []).length; 
							} 
							},							
			                regexp: {
			                    regexp: /^\d+$/,
			                    message: 'ZIP should be 5 digits and numbers only'
			                }
			            }
			        },
					profileheadline: {
			            validators: {
			                notEmpty: {
			                    message: 'Do not leave blank'
			                }
			            }
			        },
					professionalsummary: {
			            validators: {
			                notEmpty: {
			                    message: 'Your background summary is required'
			                },
							stringLength: { 
							message: '1. Use 4-6 short paragraphs</br>2. Open with a 2-3 lines of overview</br>3. Focus on your past consulting work</br>4. Mention related experience, speaking engagements, publications</br>5. End with education, credentials, affiliations</br>(Minimum 500 characters)', 
							min: function (value, validator, $field) { 
							return 500 - (value.match(/\r/g) || []).length; 
							} 
							}

			            }
			        },
			        availablitytime: {
			            validators: {
			                notEmpty: {
			                    message: 'Do not leave blank'
			                }

			            }
			        },
			        communication1: {
			        	enabled : false,
			            validators: {
			                notEmpty: {
			                    message: 'Do not leave blank'
			                }

			            }
			        },
			        communication2: {
			        	enabled : false,
			            validators: {
			                notEmpty: {
			                    message: 'Do not leave blank'
			                }

			            },							
			              regexp: {
			                    regexp: /^\d+$/,
			                    message: 'Phone number should be in numbers only'
			           }
			        },
			        communication3: {
			        	enabled : false,
			            validators: {
			                notEmpty: {
			                    message: 'Do not leave blank'
			                }

			            }
			        },
					consultationfee: {
			            validators: {
			                notEmpty: {
			                    message: 'Engagement fee is kept private. We use this information while making recommendations to potential clients. Engagements cannot be FREE'
			                },
			                regexp: {
			                    regexp: /^\d+$/,
			                    message: 'Engagement fee should have numbers only'
			                }
			            }
			        },
					engoverview: {
			            validators: {
			                notEmpty: {
			                    message: 'Do not leave blank'
			                }
			            }
			        }					
					
			    },
                                  onSuccess: function(e, data) {
                                 document.getElementById('uaccessspecifier').value='publish';
                               alert('Success');
                            }
			})
			.on('error.field.bv', '[name="phone"]', function(e, data){
				jQuery(e.target).attr('data-bv-trigger','keydown');
			    data.bv.destroy();
			    jQuery('#profileForm').bootstrapValidator(data.bv.getOptions());
			    jQuery("#uaccessspecifier").val('publish');
			})
			.on('click','#a1',function(e) {
				jQuery('#profileForm').bootstrapValidator('enableFieldValidators', 'communication1');

			})
			.on('click','#a2',function(e) {
				jQuery('#profileForm').bootstrapValidator('enableFieldValidators', 'communication2');
			})
			.on('click','#a3',function(e) {
				jQuery('#profileForm').bootstrapValidator('enableFieldValidators', 'communication3');
			})
			.on('error.field.bv',function(e, data) {
				console.log(e.target.id);
				if(jQuery("#a1").prop("checked") == true || jQuery("#a2").prop("checked") == true || jQuery("#a3").prop("checked") == true){
					console.log("In now");
					var bootstrapValidator = $('#profileForm').data('bootstrapValidator');
				
					if(jQuery("#a1").prop("checked") == true){
						bootstrapValidator.enableFieldValidators('communication1', true);
					}
					if(jQuery("#a2").prop("checked") == true){
						bootstrapValidator.enableFieldValidators('communication2', true);
					}
					if(jQuery("#a3").prop("checked") == true){
						bootstrapValidator.enableFieldValidators('communication3', true);
					} 
				} else{
					var bootstrapValidator = $('#profileForm').data('bootstrapValidator');
					bootstrapValidator.enableFieldValidators('communication1', true);
					bootstrapValidator.enableFieldValidators('communication2', true);
					bootstrapValidator.enableFieldValidators('communication3', true);
				}
			})
		});
		

	</script>

<script type="text/javascript">

        function saveasdraft(){

        var fname = jQuery('#firstname').val();
        var lname = jQuery('#lastname').val();
        var zipcode = jQuery('#zipcodevalue').val();
        var profileheadline = jQuery('#profileheadline').val();
        var professionalsummary = jQuery('#professionalsummary').val();
        var pastclient = jQuery('#pastclients').val();
        var websiteaddress = jQuery('#businesswebsite').val();
        var availabititytime = jQuery('#availablitytime').val();
        var engfee = jQuery('#engfee').val();
        var engper = jQuery('#engper').val();
        var engoverview = jQuery('#engoverview').val();
        var topics = [-1,-1,-1,-1,-1];
        var topics = topicVal;

        var UserReferenceIndustry1 = jQuery('#UserReferenceIndustry1').val();
        var UserReferenceCategory1 = jQuery('#UserReferenceCategory1').val();
        var UserReferenceIndustry2 = jQuery('#UserReferenceIndustry2').val();
        var UserReferenceCategory2 = jQuery('#UserReferenceCategory2').val();
        var UserReferenceIndustry3 = jQuery('#UserReferenceIndustry3').val();
        var UserReferenceCategory3 = jQuery('#UserReferenceCategory3').val();

	var social1= jQuery('#Social0SocialName').val();
	var social2 = jQuery('#Social1SocialName').val();
	var social3 = jQuery('#Social2SocialName').val();
	var social4 = jQuery('#Social3SocialName').val();
	var social5 = jQuery('#Social4SocialName').val();
	var social6 = jQuery('#Social5SocialName').val();
	var social7 = jQuery('#Social6SocialName').val();


        if(jQuery('#a1').is(':checked') == true){
        var communicationcheckbox1 = jQuery('#a1').val();
        var communication1 = jQuery('#communication1').val();
        }
        if(jQuery('#a2').is(':checked') == true){
        var communicationcheckbox2 = jQuery('#a2').val();
        var communication2 = jQuery('#communication2').val();
        }
        if(jQuery('#a3').is(':checked') == true){
        var communicationcheckbox3 = jQuery('#a3').val();
        var communication3 = jQuery('#communication3').val();
        }

       
        console.log("Console data");
	console.log(social1);
        console.log(topic1);
        console.log(topic2);
        console.log(topic3);
        console.log(topic4);
        console.log(topic5);
        console.log(topics);
	console.log(UserReferenceIndustry1);
	console.log(UserReferenceCategory1);
	console.log(UserReferenceIndustry2);
	console.log(UserReferenceCategory2);
	console.log(communicationcheckbox1);
	console.log(communicationcheckbox2);
	console.log(communication2);
	console.log(communication1);
	console.log(communication3);
 
			jQuery.ajax({
				url:'https://test.guild.im/members/save_draft_profile',
				type:'post',
				dataType:'json',
				data: 'fname='+fname + 
				'&lname=' + lname + 
				'&zipcode=' + zipcode + 
				'&profileheadline=' + profileheadline + 
				'&professionalsummary=' + professionalsummary+
				'&pastclient=' + pastclient+
				'&websiteaddress=' + websiteaddress +
				'&availabititytime=' + availabititytime +
				'&engfee=' + engfee +
				'&engper=' + engper+
				'&engoverview=' + engoverview+
                                '&topics=' + topics+
                                '&sociallink1=' + social1+
                                '&sociallink2=' + social2+
                                '&sociallink3=' + social3+
                                '&sociallink4=' + social4+
                                '&sociallink5=' + social5+
                                '&sociallink6=' + social6+
                                '&sociallink7=' + social7+
                                '&communicationcheckbox1=' + communicationcheckbox1+
                                '&communication1=' + communication1+
                                '&communicationcheckbox2=' + communicationcheckbox2+
                                '&communication2=' + communication2+
                                '&communicationcheckbox3=' + communicationcheckbox3+
                                '&communication3=' + communication3+
				'&UserReferenceIndustry1=' + UserReferenceIndustry1+
				'&UserReferenceCategory1=' + UserReferenceCategory1+
				'&UserReferenceIndustry2=' + UserReferenceIndustry2+
				'&UserReferenceCategory2=' + UserReferenceCategory2+
				'&UserReferenceIndustry3=' + UserReferenceIndustry3+
				'&UserReferenceCategory3=' + UserReferenceCategory3,
				success:function(){

                                          //alert("your profile save as a draft mode");
				}
			});
                        return true;

        }
         </script>


	<script>
		function Removethiselement(currentelement){
			jQuery(currentelement).parent().parent().parent(".col-sm-4").remove();
			return false;
		}

	    jQuery(document).ready(function(){
	    	jQuery('#addSocial').click(function() {
	    		var socialDiv = jQuery('#links');
	    		var i = jQuery('#links .socialinputbox').size();
	    		if(i == 7){
	    			alert("You can enter up to 7 Profile links.");
	    			return false;
	    		}				
	    		var title ='<div class="col-sm-4 socialinputbox"><div class="form-group social-add"><input type="text" id="Social'+i+'SocialName" name="data[Social]['+i+'][social_name]" value="" class="form-control"/>';

	    		var remove = '<span style="cursor:pointer;"><a href="javascript:void()" id="remscociallink['+i+']" class="removecurrentelement" onclick="Removethiselement(this);">[ - ]</a></span></div></div>';
	    		jQuery(title+remove).appendTo("#links > .row");
	    		return false;
	    	});

			/** Click handler when add row
			$(document).on('click', '#professional_summary', function(){ 
				if(jQuery("#food_row_second").attr('style') == 'display: none;')
				{
					jQuery("#food_row_second").attr('style', 'display:block');
					
				}
			
				// var i = jQuery(".p-food-row").size();
				// if(i == 7){
				// 	alert("You can enter up to 7 Profile links.");
				// 	return false;
				// }
				// $(".p-food-row:last").clone().insertAfter(".p-food-row:last").attr("id", "p-food-row-"+[i]+"");
				// $(".p-food-row:last").find(".pull-right").html();
				// var removeicon = "<label>&nbsp;</label><span><a href='javascript:;' class='remove-food-row'>[-]</a></span>";
				// $(".p-food-row:last").find(".pull-right").html(removeicon);
			}); 
			$(document).on('click', '#food_row_second', function(){ 
				
				if(jQuery("#food_row_third").attr('style') == 'display: none;')
				{
					jQuery("#food_row_third").attr('style', 'display:block');
				}
				
			});
			$(document).on('click', '#food_row_third', function(){ 
				
				alert("You can enter up to 3 Profile links.");
				
			});
			// Click handler when remove row
			$(document).on('click', '.remove-food-row', function(){ 
				jQuery(this).parent().parent().parent(".p-food-row").remove();
			}); 

			// When click on continue
			jQuery('.rest').click(function(){
				
			});**/	
	   }); 
	</script>

<script type="text/javascript">

function addbox(){

var box1 = document.getElementById("indCatid1");
var box2 = document.getElementById("indCatid2");
var box3 = document.getElementById("indCatid3");

if(box2.style.display=="block" && box3.style.display=="block"){
alert("For now you can enter up to 3 Industry Category.");

}
    if(box2.style.display=="none"){

    document.getElementById("indCatid2").style.display="block";

    }else{

    document.getElementById("indCatid3").style.display="block";
    }

}

function removebox(e){
var id = jQuery(e).attr("id");

   if(id == "id2"){
   jQuery('#UserReferenceIndustry2').val("-1");

   var select_id = "#UserReferenceCategory2";

   var item = jQuery(select_id);

   item.find('option').remove();
   item.append('<option value="-1"> </option>');
   }else{
   jQuery('#UserReferenceIndustry3').val("-1");
   var select_id = "#UserReferenceCategory3";

   var item = jQuery(select_id);

   item.find('option').remove();
   item.append('<option value="-1"> </option>');
   }

 document.getElementById("indCat"+id).style.display="none";

}
</script>
