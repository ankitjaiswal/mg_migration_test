<?php echo($this->Html->script(array('autocomplete/jquery.coolautosuggest')));?>
<?php echo($this->Html->css(array('autocomplete/jquery.coolautosuggest')));?>
<div class="adminrightinner">
	<?php echo($this->Form->create('User', array('url' => array('controller' => 'members', 'action' => 'admin_add'),'type'=>'file')));?>     
	<div class="tablewapper2 AdminForm">
		<h3 class="legend1">Add Users</h3>
		<?php echo($this->Element('Mentorsuser/form'));?>
	</div>
	<div class="buttonwapper">
		<div><input type="submit" value="Submit" class="submit_button" /></div>
		<div class="cancel_button"><?php echo $this->Html->link("Cancel", "/admin/members/admin_search/", array("title"=>"", "escape"=>false)); ?>
		</div>
	</div>
	<?php echo($this->Form->end()); ?>	
</div>

