<?php 
$firstname = '';$lastname = '';$username = '';$backgroundsummary = '';$socialId = '';$social='';$profileurl='';
if(isset($_SESSION['linkedinArr']))
{
	$firstname = ucfirst($this->Session->read('linkedinArr.first_name'));
	$lastname = ucfirst($this->Session->read('linkedinArr.last_name'));
	$username = $this->Session->read('linkedinArr.username');
	$backgroundsummary = $this->Session->read('linkedinArr.summary');
	$social =  $this->Session->read('linkedinArr.socialId');
        $profileurl =  $this->Session->read('linkedinArr.profile_url');
	unset($_SESSION['linkedinArr']);
}
$this->layout= 'defaultnew';
?>
	<div class="container-fluid" id="apply-section">
		<div class="container pad0">
			<div class="col-md-8">
				<div class="apply-heading">
					<h5>Apply to be a member</h5>
				</div>
				<div class="login-with text-center">
					<a href="javascript:void(0); javascript:linkedin();">
						<img src="<?php echo(SITE_URL)?>/img/linkedin-apply.png">
					</a>
				</div>
				<label class="or">Or fill the form below</label>
				<div class="apply-form">
					<form id="Applyform" method="post" action="<?php echo SITE_URL;?>members/apply">
						<div class="form-group width49">
							<input type="text" class="form-control " id="regFirstName" placeholder="First Name" name="data[MentorApply][firstname]" value="<?php echo($firstname);?>">
                                                        <span id="regNameErr" class="errormsg"></span>
						</div>
						<div class="form-group width49">
							<input type="text" class="form-control" id="regLastName" placeholder="Last Name" name="data[MentorApply][lastname]" value="<?php echo($lastname);?>">
                                                        <span id="regNameErr" class="errormsg"></span>
						</div>
						<div class="form-group width49">
							<input type="email" class="form-control " id="regUserName" placeholder="Email" name="data[MentorApply][email]" value="<?php echo($username);?>">
                                                        <span id="regEmailErr" class="errormsg"></span>
						</div>
						<div class="form-group width49">
							<input type="text" class="form-control" id="regPhone" name="data[MentorApply][phone]" placeholder="Phone Number">
                                                        <span id="regphoneErr" class="errormsg"></span>
						</div>
						<h5>Background summary</h5>
						<textarea class="form-control" rows="8" id="text-area" name="data[MentorApply][background_summary]" placeholder=""><?php echo($backgroundsummary);?></textarea>
						<div class="form-group width100">
							<input type="text" class="form-control" id="social1" name="data[MentorApply][linkedin_url]" placeholder="LinkedIn profile URL" value="<?php echo($profileurl);?>">
						</div>
						<div class="form-group width100">
							<input type="text" class="form-control" id="social2" name="data[MentorApply][business_website]" placeholder="Business website URL">
						</div>
						<div class="styled-form" id="checkboxsection" style="margin-top:15px;">
							<input type="checkbox" id="an">
							<label  for="an">
								 I agree to the <a href="<?php echo(SITE_URL)?>fronts/terms" target="_blank">Terms of service</a> and the <a href="<?php echo(SITE_URL)?>fronts/privacy"target="_blank">Privacy policy</a>
							</label>
						</div>
						<button class="banner-getstarted btn btn-default get-started" style="margin-top: 15px;" id="submitApply" onclick="if(!this.form.checkbox.checked){alert("Check the box to accept the Terms of service and the Privacy policy.")}">
							Apply now
						</button>
					</form>
				</div>
			</div>
			<div class="col-md-4 clearboth">
				<div class="right-apply">
					<div class="apply-heading">
						<h5>Member Advantage</h5>
					</div>
					<div class="apply-desc">
						<p>GUILD helps great consultants get more business.</p>
						<label>
							<a href="https://test.guild.im/members/advantage">Learn more</a>
						</label>
					</div>
				</div>
			</div>
		</div>
	</div>



	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>



           <script type="text/javascript">

		$("#submitApply").click(function(){


			var flag = 0;
			var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;			
			var onlyLetters = /^[a-zA-Z _!@#"$%\^\-:.<>?=&'()*\+,\/;\[\\\]\^_`{|}~]+$/.test($.trim($("#regFirstName").val()));
			var lastNameonlyLetters = /^[a-zA-Z _!@#"$%\^\-:.<>?=&'()*\+,\/;\[\\\]\^_`{|}~]+$/.test($.trim($("#regLastName").val()));
			var onlyNumbers = /^[0-9-]*$/;

			if($.trim($("#regFirstName").val()) == '') {

				$('#regFirstName').css('border-color','#F00');
				$('#regNameErr').html('');	
				$('#regFirstName').focus();				
				flag++;
			}else if(!onlyLetters){

				$('#regFirstName').css('border-color','#F00');
				$('#regNameErr').html('Only alphabet & special characters are required.');
				$('#regFirstName').focus();
				flag++;
			}else {

				$('#regFirstName').css('border-color','');
				$('#regNameErr').html('');	
			}

                        if(!$("#an").is(':checked')){
  
                           $('#checkboxsection').css('border','1px solid #F00');
			   $('#checkboxsection').focus();	
				flag++;
                         }else {
                            $('#checkboxsection').css('border-color','');    
                        }
                   

			//for last name
			if($.trim($('#regLastName').val()) == '') {
 
				$('#regLastName').css('border-color','#F00');
				$('#reglastNameErr').html('');	
				$('#regLastName').focus();	
				flag++;
			}else if(!lastNameonlyLetters){

				$('#regLastName').css('border-color','#F00');
				$('#reglastNameErr').html('Only alphabet & special characters are requried.');
				$('#regLastName').focus();
				flag++;
			} else {

				$('#regLastName').css('border-color','');
				$('#reglastNameErr').html('');	
			}
			

			//for BackgroundSummary
			if($.trim($("#text-area").val()) == '') {

				$('#text-area').css('border-color','#F00');
				$('#text-area').focus();	
				flag++;
			 
                         } else {
                         
                             $('#text-area').css('border-color','');     
                         }	
			         			

			
			// for email
			if($.trim($('#regUserName').val()) == '') {

				$('#regEmailErr').hide();	
				$('#regUserName').css('border-color','#F00');			
				flag++;
			} else if(!$('#regUserName').val().match(mailformat)) {

				$('#regUserName').css('border-color','#F00');
				$('#regEmailErr').html('Invalid Email Id');
				$('#regEmailErr').css('color','#F00');
				$('#regEmailErr').show();
				flag++;
			}  else {
				var email = $('#regUserName').val();



				$.ajax({

					url:SITE_URL+'/users/checkEmail',
					type:'post',
					dataType:'json',
					data:'email='+email,
					success:function(res){
						if(res.value || res.value1){
                                                        if(res.status==0){
                                                         var text ="You already applied for GUILD membership and its pending for approval by Admin";
                                                         }else if(res.status==1){  
                                                         var text ="You already applied for GUILD membership and its approved by Admin";
                                                         }else if(res.status==2){
                                                         var text ="You already applied for GUILD membership and its declined by Admin";
                                                         }else{
                                                         var text ="Email already exists.";
                                                         }
							$('#regUserName').css('border-color','#F00');
							$('#regEmailErr').html(text);
							$('#regEmailErr').css('color','#F00');
							$('#regEmailErr').show();
							flag++;
						}else{

							$('#regEmailErr').html('');
							$('#regUserName').css('border-color','');
							$('#regEmailErr').html('');
							$('#regEmailErr').hide();
						}
						if(flag > 0) {	
 						
							return false;
						} else {


						     $("#Applyform").submit();
                                                     return true;
						}					
						
					}
				});

				
			}
                        return false;		
		});
</script>