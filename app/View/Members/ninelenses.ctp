
	<div id="banner-section" class="lenses-page" data-parallax="scroll" data-image-src="<?php echo(SITE_URL)?>/yogesh_new1/images/banner9lenses.jpg">
		<div class="col-md-12 pad0 text-center banner-content">
			<h1>Smarter Way to Engage Clients</h1>
			<p>A Cloud-Based Platform for Consultants.</p>
			<a href="https://offers.9lenses.com/request-demo" target="_blank" class="banner-getstarted btn btn-default get-started">
				Request a demo
			</a>
		</div>
	</div>
	<div class="lenses-section container pad0">
		<div class="lenses-heading">
			<h1>Software for Consultants by</h1>
			<label>
				<img src="<?php echo(SITE_URL)?>/yogesh_new1/images/9lenses.png">
			</label>
		</div>
		<div class="l-desc">
			<p>9Lenses provides a secure, connected way to manage your engagements and protect your IP. Our platform combines a rigorous interview structure with intuitive software and exceptional analytics so you can have a deeper, faster method for advising your clients.</p>
		</div>
		<div class="col-md-12 pad0 margint20">
			<div class="col-md-7 pad0">
				<div class="l-desc">
					<p>
						<label>The challenge: </label> traditional, manual data collection and analysis methods result in disconnected data and fail to efficiently track your best questions and frameworks. At the same time, clients are demanding more value from their engagements with faster turnaround.
					</p>
					<p>
						<label>The 9Lenses solution:</label> independent consultants are using 9Lenses to conduct rapid client discovery and analysis while determining and scaling the best methods for solving their clients' business problems.
					</p>
				</div>
			</div>
			<div class="col-md-5">
				<div class="embed-responsive embed-responsive-4by3">
                                         <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/gP4CqyjmmUs" frameborder="0" allowfullscreen></iframe>

				</div>
			</div>
		</div>
		<div class="lenses-discount">
			<h1>Discount Available!</h1>
			<label>
				 (10% off for Regular members and 20% off for premium members)
			</label>
		</div>
		<div class="l-desc">
            <?php if($this->Session->read('Auth.User.id') ==''){?>
			<p>Discount codes are available to member consultants.
				<a href="#" class="mymodal" data-toggle="modal" data-target="#signin-Modal">Login to view</a>
			</p>
            <?php }elseif($this->Session->read('Auth.User.role_id') == 2 && $this->Session->read('Auth.User.mentor_type') != 'Premium Member'){ ?>
              
			<p>Your promo code:
				<a href="http://www.9lenses.com/consulting-software" target="_blank">10OFF</a>
			</p>
            <?php } elseif($this->Session->read('Auth.User.role_id') == 2 && $this->Session->read('Auth.User.mentor_type') == 'Premium Member'){?>
             		<p>Your promo code:
				<a href="http://www.9lenses.com/consulting-software" target="_blank">20OFF</a>
			</p>
            <?php }?>
		</div>
	</div>
	<!-- js link -->
	<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>-->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>-->
	<script type="text/javascript">
		// parallax
		if($(window).width() >991){
			$('#banner-section').parallax();
		}
		</script>