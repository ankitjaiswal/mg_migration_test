<?php 
ob_clean();
App::import('Vendor','tcpdf/tcpdf/tcpdf');
//App::uses('Vendor','tcpdf/xtcpdf');
//echo $html->css(array('style'));

class MYPDF extends TCPDF {

    //Page header
    public function Header() {

        $image_file = 'img/media/GUILD-PDF-NEW-Logo.svg';
        $this->ImageSVG($image_file, 10, 10, 68, 23, 'SVG', '', 'T', false, 72, '', false, false, 0, false, false, false);
             $this->SetFont('times', 10);
             $this->SetTextColor(142,142,142); 
             $footer_text = '1-866-511-1898 | info@guild.im';               
            $this->writeHTMLCell(100, 50, 144, 18, $footer_text, 0, 0, 0, true, 'L', true);

 }

  public function AcceptPageBreak() {
  $left_margin = 10;
  $top_margin = 40;
  $right_margin =10;
         if (1 == $this->PageNo()) {
                $this->SetMargins($left_margin, $top_margin, $right_margin, true);
         }
        if ($this->num_columns > 1) {
            // multi column mode
            if ($this->current_column < ($this->num_columns - 1)) {
                // go to next column
                $this->selectColumn($this->current_column + 1);
            } elseif ($this->AutoPageBreak) {
                // add a new page
                $this->AddPage();
                // set first column
                $this->selectColumn(0);
            }
            // avoid page breaking from checkPageBreak()
            return false;
        }
        return $this->AutoPageBreak;
    }


}


$tcpdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$textfont = 'ubuntu';

$tcpdf->SetAuthor("Julia Holland");
$tcpdf->SetAutoPageBreak(true);
 
//$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(false);
 
//$tcpdf->SetTextColor(56, 150, 100);
$tcpdf->SetFont($textfont,'',12);

$tcpdf->SetMargins(10.0, 36, 10.0); // left = 2.5 cm, top = 4 cm, right = 2.5cm
$tcpdf->SetFooterMargin(1.5);  // the bottom margin has to be set with SetFooterMargin


$tcpdf->SetAutoPageBreak(TRUE, 15);

$title = $casestudydata['CaseStudy']['title'];
$imgPath = 'press_material/guild-pdflogo.png';
$userimgPath = 'img/'.MENTORS_IMAGE_PATH.DS.$user['User']['id'].DS.$user['UserImage'][0]['image_name'];
$username = ucfirst(strtolower($user['UserReference']['first_name'])).' '.ucfirst(strtolower($user['UserReference']['last_name']));
$userheadline = $user['UserReference']['linkedin_headline'];
$clientdetails = nl2br(strip_tags($casestudydata['CaseStudy']['client_details']));
$situation = nl2br(strip_tags($casestudydata['CaseStudy']['situation']));
$actions = nl2br(strip_tags($casestudydata['CaseStudy']['actions']));
$result = nl2br(strip_tags($casestudydata['CaseStudy']['result']));
$city = $city;
$state = $state;
$tcpdf->AddPage();


$htmlcontent = <<<EOF
<div style="width: 800px; margin-top:20px;" >
 <br/>

 <table cellpadding="6" cellspacing="0" style="border-collapse: collapse; float: right;text-align:centr;background-color:#000;color:white;">
<tr><td>CASE STUDY</td></tr>
</table>


<br/>



<br/><br/>
       <table cellpadding="6" cellspacing="0" style="border-collapse: collapse; float: right; ">
                <tr><td style="width:20%">
<div style="border-right:2px solid #eee;margin-left:10px;">
<img src="$userimgPath"/ style="width:80px; height:80px;"> 
<br/>
<span style="font-size: 12pt; font-color:#000;">$username</span>
<br/>
$city, $state


<br/>
<span style="font-size: 9pt; font-color:#333333;">$userheadline</span>
</div>
</td>
                 <td style="width:80%"><h1 style="font-weight: 500; font-size: 24pt;font-color:#000000;">$title</h1>





<br/>
 <table cellpadding="6" cellspacing="0" style="border-collapse: collapse; float: right;background-color:#eee;font-weight: 500; font-size: 12pt; font-color:#000000;">
<tr><td>Client</td></tr>
</table>
<br/>
<span style="font-weight: 300; font-size: 12pt; font-color:#333333;">$clientdetails</span>
</td>

                </tr>


            </table>
 <table cellpadding="6" cellspacing="0" style="border-collapse: collapse; float: right;background-color:#eee;font-weight: 500; font-size: 12pt;font-color:#000000;">
<tr><td>Problem</td></tr>
</table>
<br/>
<span style="font-weight: 300; font-size: 12pt; font-color:#333333;">$situation</span><br/><br/>
 <table cellpadding="6" cellspacing="0" style="border-collapse: collapse; float: right;background-color:#eee;font-weight: 500; font-size: 12pt; font-color:#000000;">
<tr><td>Solution</td></tr>
</table>
<br/>
<span style="font-weight: 300; font-size: 12pt; font-color:#333333;">$actions</span><br/><br/>
 <table cellpadding="6" cellspacing="0" style="border-collapse: collapse; float: right;background-color:#eee;font-weight: 500; font-size: 12pt;font-color:#000000;">
<tr><td>Result</td></tr>
</table>
<br/>
<span style="font-weight: 300; font-size: 12pt; font-color:#333333;">$result</span><br/><br/><br/>
</div>


EOF;

// output the HTML content
$tcpdf->writeHTML($htmlcontent, true, 0, true, 0);
$filename = $username."_Case_Study_Guild";
$tcpdf->Output($filename.".pdf", 'I');
 ?>