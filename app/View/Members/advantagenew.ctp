<link rel="stylesheet" href="<?php echo(SITE_URL)?>css/advantagenew.css">
    <div class="advantageBanner">
		<div class="innerwrapper">
			<h1>Member Advantage</h1>
			<h5>Guild connects 8,000+ members consultants to business opportunities across the nation.</h5>
			<div class="advantagePoints">
				<ul>
					<li>
						<span class="establish"></span>
						<h3>Establish your brand</h3>
					</li>
					<li>
						<span class="generate"></span>
						<h3>Generate more leads</h3>
					</li>
					<li>
						<span class="serve"></span>
						<h3>Serve more clients</h3>
					</li>
				</ul>
				<div class="clear"></div>
			</div>
			<a href="javascript:void(0);" class="membershipBtn" onclick="window.location.href='<?php echo SITE_URL;?>members/eligibility'">Start your membership today!</a>
			<span class="membershipNote">Guild membership is free and invite-only. Guild retains 15% of the fee for any consulting engagement we generate for our members.</span>
			<p>Already a member? <a href="javascript:loginpopup();">Login</a></p>
		</div>
    </div>
	<div class="memberPlanContainer">
		<div class="membershipInfo">
			<p>You have spent years building your consulting practice. Guild can help you build a brand.</p>
			<p>The new digital world is enabling unprecedented business relationships. Guild offers you the best returns on your marketing dollars.</p>
			<p>Do what you do best, and leave the rest to your Guild team!</p>
		</div>
		<div class="innerwrapper">
			<div class="planDetails">
				<ul>
					<li>
						<div class="planTitleContainer">
							<div >
								<h3 style="text-align:center;font-size:24px;margin-top:56px;font-weight: 400;">GUILD BASIC</h3>
								
							</div>
						</div>
                                                <div class="planInfo" style="height:334px;background: #ededed;color:#202020;">
						<p style="font-size: 12.5px;">Guild Basic solution to get you started</p>
						<h2>Free</h2>
						<p>Guild profile</p>
						<p>Guild promotion</p>
						<p>Business leads</p>
						<p>Productivity tools</p>
						<a href="">Register</a>
                                                </div>
					</li>
					<li>
						<div class="planTitleContainer" style="height:80px;">
							<div class="websiteTitle">
								<h3></h3>
								
							</div>
						</div>
                                                <div class="planInfo" style="height:334px;background: #ededed;">
						<p style="font-size: 12.5px;">High performance websites for experienced consultants</p>
						<h2>$199/mo</h2>
						<p>GUILD BASIC +</p>
						<p>Website development and hosting</p>
						<p>Fully managed services</p>
						<p>Monthly traffic reviews and changes</p>
						<a href="">Learn more</a>
                                                </div>
					</li>
					<li class="premium">
						
						<div class="planTitleContainer" style="background: transparent;border-bottom: solid 1px #d76363;">
							<div class="masterTitle">
								<h3></h3>
								
							</div>
						</div>
						<div class="planInfo" style="height:334px;background: #ededed;">
							<p style="font-size: 12.5px;margin-top:8px;">Marketing support to help you stand out in a crowded marketplace</p>
							<h2>$999/mo</h2>
							<p>GUILD WEBSITE +</p>
							<p>Social media management</p>
							<p style="width:208px;">Weekly Linked In articles or blogs</p>
							<p>Monthly newsletters</p>
							<p>Quarterly ebooks/whitepapers</p>
							<a href="">Learn more</a>
						</div>
					</li>
					<li>
						<div class="planTitleContainer">
							<div class="premierTitle" >
								<h3></h3>
								
							</div>
						</div>
                                                 <div class="planInfo" style="height:334px;background: #ededed;">
						<p style="font-size: 12.5px;color:#202020;">All-in-one marketing solution for unique member needs</p>
						<h2>$2,999+/mo</h2>
						<p>Full service, customized marketing support for your firm</p>
						<div class="callDetails">Call 1-866-511-1898</div>
                                                </div>
					</li>
				</ul>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="customPlanContainer">
		<h4>Or choose a customized plan focusing on your requirements</h4>

		<div class="innerwrapper">
			<div class="customPlanOptions">
				<h3 style="text-align:center;">customized membership plan</h3>
				<p style="text-align:center;font-size:16px;">Select the features you want to include in your custom plan and submit a quote you are willing to pay</p>
				<ul class="customPlanItems">
					<li style="padding-top: 13px;">
						<input type="checkbox" id="test1" checked/>
						<label for="test1">Guild Website</label>
					</li>
					<li>
						<input type="checkbox" id="test2" checked/>
						<label for="test2">Ebook/Whitepaper <input type="text" style="height: 10px;text-align:center;" id="ebookinput" value ="1"/>&nbsp;/qtr</label>
					</li>
					<li>
						<input type="checkbox" id="test3" checked/>
						<label for="test3">LinkedIn Articles <input type="text" style="height: 10px;text-align:center;" id="larticlesinput" value ="1"/>&nbsp;/mo</label>
					</li>
				</ul>
				<div class="clear"></div>
				<ul class="customPlanItems">
					<li style="padding-top: 12px;">
						<input type="checkbox" id="test4" checked/>
						<label for="test4">SEO Support</label>
					</li>
					<li style="padding-top: 12px;">
						<input type="checkbox" id="test5" />
						<label for="test5">Daily social media management</label>
					</li>
					<li>
						<input type="checkbox" id="test6" />
						<label for="test6">Webinar support <input type="text" style="height: 10px;text-align:center;" id="wsupportinput"/>&nbsp;/qtr</label>
					</li>
				</ul>
				<div class="clear"></div>
				<ul class="customPlanItems">
					<li style="padding-top: 3px;">
						<input type="checkbox" id="test7" checked/>
						<label for="test7">Blog (600 words) <input type="text" style="height: 10px;text-align:center;" id="bloginput" value ="4"/>&nbsp;/mo</label>
					</li>
					<li style="padding-top: 12px;">
						<input type="checkbox" id="test8" checked/>
						<label for="test8">Social profile optimization</label>
					</li>
					<li style="padding-top: 12px;">
						<input type="checkbox" id="test9" />
						<label for="test9">Digital advertising</label>
					</li>
				</ul>
				<div class="clear"></div>
			</div>
			<div class="quoteContainer">
				<p>$ <input type="text" id="quoteAmount" value="695"  style="color: #202020;" onblur="if(this.value == '') {  this.value='695'}" onfocus="if (this.value == '695') {this.value=''}"/> /mo</p>
				<button name="submit" id="submit">Submit Quote</button>
			</div>
		</div>
	</div>
	<div class="testimonails">
		<h3 style="font-weight: 400;text-transform: uppercase;">Testimonails</h3>
                 <p style="text-align:center;font-size:16px;">What our member consultants are saying</p>
                 </br>
		<div id="testimonailCarousel">
			<div id="buttons">
				<a href="javascript:void(0);" id="prev" style="margin-top: 60px;">prev</a>
				<a href="javascript:void(0);" id="next" style="margin-top: 60px;">next</a>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<div id="slides"> 
				<ul>
					<li>
						<div class="memImg"><img src="<?php echo(SITE_URL)?>/imgs/Judy_Bardwick.png" style="width:150px;height:150px;"></div>
						<div class="testimonailText">
							<p>I am delighted and amazed by the work Guild has done for me. It is personal, accurate, honest and original. The website they created is client-focused, eye-catching, and simultaneously captures the essence of what I deliver to Clients. And, the speed of the team's responsiveness and their attention to every detail <span style="font-style:italic;">is the best I have ever experienced</span>.</p>
							<div class="memName">
								<p>Judy Bardwick</p>
								<p>Leading Organizational Psychologist</p>
							</p>
						</div>
						<div class="clear"></div>
					</li>
					<li>
						<div class="memImg"><img src="<?php echo(SITE_URL)?>/imgs/mark_palmer.jpg" style="width:150px;height:150px;"></div>
						<div class="testimonailText">
							<p>By listing only the best consultants Guild is well on its way of marketing to businesses from a place of trust.</p>
							<div class="memName">
								<p>Mark Palmer</p>
								<p>Founder/ Principal, Focus LLC</p>
							</p>
						</div>
						<div class="clear"></div>
					</li>
					<li>
						<div class="memImg"><img src="<?php echo(SITE_URL)?>/imgs/ChipEvans.jpg" style="width:150px;height:150px;"></div>
						<div class="testimonailText">
							<p>Guild was extraordinary in how they represented us to the client looking for consulting guidance, and in organizing and following up to ensure the consulting times and description of work were clear.
                               
                               </br> </br>I'd highly recommend working with Guild and am very pleased with our relationship.</p>
							<div class="memName">
								<p>Chip Evans, Ph.D</p>
								<p>President & Founder at The Evans Group LLC</p>
							</p>
						</div>
						<div class="clear"></div>
					</li>

					<li>
						<div class="memImg"><img src="<?php echo(SITE_URL)?>/imgs/Roza.png" style="width:150px;height:150px;"></div>
						<div class="testimonailText">
							<p>As a partner to Xecutive Metrix, the Guild team is offering services that help our firm increase our level of exposure to new markets, connect us with other value added consultants in the network and provide us with peace of mind that our marketing efforts are running smoothly. Now we can focus on what we do best&mdash;Executive Leadership Development.</p>
							<div class="memName">
								<p>Roza S. Rojdev, PsyD</p>
								<p>Managing Partner at Xecutive Metrix</p>
							</p>
						</div>
						<div class="clear"></div>
					</li>
				</ul>
				<div class="clear"></div>
			</div>

		</div>
	</div>

<script type="text/javascript">
		jQuery(document).ready(function() {

		//rotation speed and timer
		var speed = 7000;
		var run = setInterval('rotate()', speed);	
		
		//grab the width and calculate left value
		var item_width = jQuery('#slides li').outerWidth(); 
		var left_value = item_width * (-1); 
			
		//move the last item before first item, just in case user click prev button
		jQuery('#slides li:first').before(jQuery('#slides li:last'));
		
		//set the default item to the correct position 
		jQuery('#slides ul').css({'left' : left_value});

		//if user clicked on prev button
		jQuery('#prev').click(function() {

			//get the right position            
			var left_indent = parseInt(jQuery('#slides ul').css('left')) + item_width;

			//slide the item            
			jQuery('#slides ul').animate({'left' : left_indent}, 700,function(){    

				//move the last item and put it as first item            	
				jQuery('#slides li:first').before(jQuery('#slides li:last'));           

				//set the default item to correct position
				jQuery('#slides ul').css({'left' : left_value});
			
			});

			//cancel the link behavior            
			return false;
				
		});

	 
		//if user clicked on next button
		jQuery('#next').click(function() {
			
			//get the right position
			var left_indent = parseInt(jQuery('#slides ul').css('left')) - item_width;
			
			//slide the item
			jQuery('#slides ul').animate({'left' : left_indent}, 700, function () {
				
				//move the first item and put it as last item
				jQuery('#slides li:last').after(jQuery('#slides li:first'));                 	
				
				//set the default item to correct position
				jQuery('#slides ul').css({'left' : left_value});
			
			});
					 
			//cancel the link behavior
			return false;
			
		});        
		
		//if mouse hover, pause the auto rotation, otherwise rotate it
		jQuery('#slides').hover(
			
			function() {
				clearInterval(run);
			}, 
			function() {
				run = setInterval('rotate()', speed);	
			}
		); 
			
	});

	//a simple function to click next link
	//a timer will call this function, and the rotation will begin :)  
	function rotate() {
		jQuery('#next').click();
	}

  </script>

<script type="text/javascript">
jQuery(document).ready(function() {
//alert(loggedUserId);
jQuery("#submit").click(function(){

if(jQuery("#test1").is(':checked')){
var website = "yes";
}else{
var website = "no";
}
if(jQuery("#test2").is(':checked')){
var ebook = "yes";
var ebooknumber = jQuery("#ebookinput").val();
}else{
var ebook = "no";
var ebooknumber = "no";
}
if(jQuery("#test3").is(':checked')){
var larticles = "yes";
var larticlesnumber = jQuery("#larticlesinput").val();
}else{
var larticles = "no";
var larticlesnumber = "no";
}
if(jQuery("#test4").is(':checked')){
var ssupport = "yes";
}else{
var ssupport = "no";
}
if(jQuery("#test5").is(':checked')){
var dsmmgmt = "yes";
}else{
var dsmmgmt = "no";
}
if(jQuery("#test6").is(':checked')){
var wsupport = "yes";
var wsupportnumber = jQuery("#wsupportinput").val();
}else{
var wsupport = "no";
var wsupportnumber = "no";
}
if(jQuery("#test7").is(':checked')){
var blog = "yes";
var blognumber = jQuery("#bloginput").val();
}else{
var blog = "no";
var blognumber = "no";
}
if(jQuery("#test8").is(':checked')){
var spoptimization = "yes";
}else{
var spoptimization = "no";
}
if(jQuery("#test9").is(':checked')){
var dmarketing = "yes";
}else{
var dmarketing = "no";
}

var quoteamount = jQuery("#quoteAmount").val();

		if(loggedUserId =='') {
			alert("To submit a customized plan request need to first login or register.");
		} else {
		       jQuery.ajax({
					url:SITE_URL+'members/customplan_request',
					type:'post',
					dataType:'json',
					data: 'website='+website + '&ebook=' + ebook + '&ebooknumber=' + ebooknumber + '&larticles=' + larticles + '&larticlesnumber=' + larticlesnumber + '&ssupport=' + ssupport + '&dsmmgmt=' + dsmmgmt + '&wsupport=' + wsupport + '&wsupportnumber=' + wsupportnumber + '&blog=' + blog + '&blognumber=' + blognumber + '&spoptimization=' + spoptimization + '&dmarketing=' + dmarketing + '&quoteamount=' + quoteamount,
					success:function(id){
                                                location.reload();
						alert("Thanks for submitting customized plan request.");
					}
				});	
		       
		   		return false;
                              }

});

var flag = 0;


jQuery('#test1').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#test2').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#test3').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#test4').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#test5').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#test6').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#test7').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#test8').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#test9').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#ebookinput').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#larticlesinput').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});

jQuery('#bloginput').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#wsupportinput').click(function(){
document.getElementById('quoteAmount').value = '';
document.getElementById('quoteAmount').placeholder = 'Enter your quote here';
});
jQuery('#quoteAmount').focus(function(){
   jQuery(this).removeAttr('placeholder');
});
          
});
</script>
<style>
.websiteTitle{
    
    margin: 0 auto;
    background: url(../newimages/guild-websites-logo.png) center no-repeat;
height: 79px;
    
}
.masterTitle
{
   
    margin: 0 auto;
    background: url(../newimages/guild-master-logo.png) center no-repeat;
height: 79px;
    
}
.premierTitle
{
   
    margin: 0 auto;
    background: url(../newimages/guild-premier-logo.png) center no-repeat;
height: 79px;
    
}
.planDetails li .planInfo h6, .planDetails li .planInfo h2, .planDetails li .planInfo p{ color:#202020;}
.planDetails li a {
    display: block;
    transition: background 0.5s ease;
    width: 207px;
    position: absolute;
    bottom: -25px;
    line-height: 44px;
    background: #d03135;
    font-size: 18px;
    font-weight: 500;
    color: #fff;
    text-align: center;
    margin-top: 15px;
}
.planDetails li .callDetails {
    width: 100%;
    background: #000000;
    position: absolute;
    text-align: center;
    bottom: -25px;
    line-height: 35px;
    height: 40px;
    font-size: 18px;
    color: #fff;
    font-weight: 500;
    padding-top: 4px;
    width: 207px;
}
</style>