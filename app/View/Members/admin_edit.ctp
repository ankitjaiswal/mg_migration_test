<?php echo($this->Html->script(array('autocomplete/jquery.coolautosuggest')));?>
<?php echo($this->Html->css(array('autocomplete/jquery.coolautosuggest')));?>
	<div class="adminrightinner">
		<?php 
		echo($this->Form->create('User', array('url' => array('controller' => 'members', 'action' => 'edit'),'type'=>'file')));
		echo($this->Form->hidden('User.id'));
		echo($this->Form->hidden('UserReference.id'));
		echo($this->Form->hidden('Availablity.id'));
		echo($this->Form->hidden('UserImage.0.id'));
		echo($this->Form->hidden('UserImage.0.image_name'));
		
		if(count($this->data['Communication']) >0){
			foreach($this->data['Communication'] as $key=>$value){
				if(isset($value['id']) && $value['id'] !=''){
					echo($this->Form->hidden('Communication.'.$key.'.id'));
				}
			}	
		}
		if(count($this->data['Question']) >0){
			foreach($this->data['Question'] as $key=>$value){
				if(isset($value['id']) && $value['id'] !=''){
					echo($this->Form->hidden('Question.'.$key.'.id'));
				}
			}	
		}	
		if(count($this->data['Source']) >0){
			foreach($this->data['Source'] as $key=>$value){
				if(isset($value['id']) && $value['id'] !=''){
					echo($this->Form->hidden('Source.'.$key.'.id'));
				}
			}	
		}		
		if(count($this->data['Social']) >0){
			foreach($this->data['Social'] as $key=>$value){
				if(isset($value['id']) && $value['id'] !=''){
					echo($this->Form->hidden('Social.'.$key.'.id'));
				}
			}	
		}
		
		?>    
		<div class="tablewapper2 AdminForm">
			<h3 class="legend1">Edit Member  </h3>
			<?php echo($this->Element('Mentorsuser/form'));?>
		</div>
		<div class="buttonwapper">
			<div>
				<input type="submit" value="Submit" class="submit_button" />
			</div>
			<div class="cancel_button">
				<?php echo $this->Html->link("Cancel", "/members/admin_search/", array("title"=>"", "escape"=>false)); ?>
			</div>
		</div>
		<?php echo($this->Form->end()); ?>
		
	</div>

