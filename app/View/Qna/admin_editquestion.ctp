<?php echo($this->Html->script(array('jquery/jquery','ui/jquery-ui-1.8.2.custom.min')));?>

<script type="text/javascript">

var topicVal = [-1,-1,-1];
var topicCount = 0;
		
function monkeyPatchAutocomplete() {

    jQuery.ui.autocomplete.prototype._renderItem = function( ul, item) {
        var re = new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + this.term + ")(?![^<>]*>)(?![^&;]+;)", "gi");
        var t = item.label.replace(re,"<span style='font-weight:bold;color:#434343;'>" + 
        		"$&" + 
                "</span>");
        return jQuery( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + t + "</a>" )
            .appendTo( ul );
    };
}

function getKeywords(){

		var allKeywords = <?php echo json_encode($allKeywords); ?>;
		
		return allKeywords;
	}
	
function deleteThis(element) {
	console.log("Delete This");
	
	topicVal[element] = -1;
	
	for( var i = 0 ; i < 3 ; i ++) {
		
		if(topicVal[i] == -1) {
			jQuery('#div'+i).css('display','none');
			jQuery('#topic'+i).html('');
		} else {
			jQuery('#div'+i).css('display','inline-block');
			jQuery('#topic'+i).html(topicVal[i]);
		}
	}
}
	
jQuery(document).ready(function(){
	
		var hidVal = jQuery('#hiddenSelect').val();
		
		if(hidVal != ''){
			var hidValArr = hidVal.split(",");
			
			for(var i = 0; i < hidValArr.length; i++){
				topicVal[i] = hidValArr[i];
				jQuery('#div'+i).css('display','inline-block');
				jQuery('#topic'+i).html(hidValArr[i]);
			}
		}
		
		
		monkeyPatchAutocomplete();
		
		var CityKeyword = jQuery('#CityKeyword');
	
		CityKeyword.autocomplete({
		minLength    : 1,
		source        : getKeywords(),
		source		: function(request, response) {
			
		        var results = jQuery.ui.autocomplete.filter(getKeywords(), request.term);
		        
		        results = results.slice(0, 5);
		        results.push('Add <b>' + jQuery("#CityKeyword" ).val() + '</b>');
		        response(results); //show 3 items.
		    },
		focus: function( event, ui ) {
			
			if(ui.item.value[0] == "A")
				return false;
	       //jQuery("#CityKeyword" ).val( ui.item.value );
	       return false;
	     },
	     select: function( event, ui ) {

			var toAdd = ui.item.value;
			if(ui.item.value[0] == "A")
				toAdd = toAdd.substring(7);
				
			var changed = -1;
			
			for (var i = 0; i < 3; i++) {
				
				if(topicVal[i] == -1) {
					changed = 1;
					topicVal[i] = toAdd;
					break;
				}
			}
			
			if(changed == -1)
				alert("You can only select 3 values");
			
			for( var i = 0 ; i < 3 ; i ++) {
		
				if(topicVal[i] == -1) {
					jQuery('#div'+i).css('display','none');
					jQuery('#topic'+i).html('');
				} else {
					jQuery('#div'+i).css('display','inline-block');
					jQuery('#topic'+i).html(topicVal[i]);
				}
			}
			
	        jQuery("#CityKeyword" ).val('');
	          
	        return false;
	       }
		});
	});
</script>

	<div class="adminrightinner">
		<?php echo($this->Form->create('QnaQuestion', array('url' => array('controller' => 'qna', 'action' => 'admin_editquestion'), 'id'=>'qForm') ));?>
		<?php echo($this->Form->input('id'));?>
		<div class="tablewapper2 AdminForm">
			<table border="0" class="Admin2Table" width="100%">			   
				<tr>
					<td valign="middle" class="Padleft26"><?php echo "Id :".$res['QnaQuestion']['id']; ?></td>
				</tr>
				<tr>
					<td><?php echo($this->Form->input($modelName.'.question_text', array('div'=>false, 'label'=>false, "class" => "TextBox5" , "style" => "width: 800px;")));?></td>
				</tr>
				<tr>
					<td><?php echo($this->Form->textarea($modelName.'.question_context', array('rows'=>6,'div'=>true, 'label'=>false, 'id'=>'text-area' , "class" => "forminput", "style"=>"width:800px; height:100px;font-family: Ubuntu, arial, vardana; font-size: 14px;")));?></td>
				</tr>
			</table>
		</div>
        <div style=" width: 100%; margin-left:40px; padding-bottom: 30px; display: "inline-block">
	  		<p>
	  			<div id="enter-text">
					<?php 	echo($this->Form->input('City.keyword',array('id'=>'CityKeyword','div'=>false,'label'=>false,'class'=>'forminput-new','type'=>'text',"placeholder"=>"Consultant's name -OR- Area of expertise",'value'=>'','style'=>'width:600px;')))	?>
				</div>
	  		</p>
	  		<div id="div0" style="display: none; border-bottom:1px solid #BEBEBE;margin-bottom:20px;padding-bottom:10px; background:#ccc; border-radius:5px;">
		        <span id='close' onclick='deleteThis(0); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
		        <p id="topic0" style="padding-top: 10px; padding-bottom: 0px; display: inline-block"></p>
			</div>
			<div id="div1" style="display: none; border-bottom:1px solid #BEBEBE;margin-bottom:20px;padding-bottom:10px; background:#ccc; border-radius:5px;">
		        <span id='close' onclick='deleteThis(1); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
		        <p id="topic1" style="padding-top: 10px; padding-bottom: 0px; display: inline-block""></p>
			</div>
			<div id="div2" style="display: none; border-bottom:1px solid #BEBEBE;margin-bottom:20px;padding-bottom:10px; background:#ccc; border-radius:5px;">
		        <span id='close' onclick='deleteThis(2); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
		        <p id="topic2" style="padding-top: 10px; padding-bottom: 0px; display: inline-block""></p>
			</div>
			<div id="div3" style="display: none; border-bottom:1px solid #BEBEBE;margin-bottom:20px;padding-bottom:10px; background:#ccc; border-radius:5px;">
		        <span id='close' onclick='deleteThis(3); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
		        <p id="topic3" style="padding-top: 10px; padding-bottom: 0px; display: inline-block""></p>
			</div>
			<div id="div4" style="display: none; border-bottom:1px solid #BEBEBE;margin-bottom:20px;padding-bottom:10px; background:#ccc; border-radius:5px;">
		        <span id='close' onclick='deleteThis(4); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
		        <p id="topic4" style="padding-top: 10px; padding-bottom: 0px; display: inline-block""></p>
			</div>

        </div>
        <?php echo($this->Form->hidden($modelName.'.select',array('value'=>$tags, 'id'=>'hiddenSelect')));?>



		<div class="buttonwapper">
			<div><input type="button" value="Submit" class="submit_button" id="submit_button"/></div>
			<div class="cancel_button"><?php echo $this->Html->link("Cancel", "/admin/qna/question_review", array("title"=>"", "escape"=>false)); ?></div>
		</div>
		<?php  echo($this->Form->end());	?>	
		
		<div style="display: block; position: absolute; margin-top: 60px; background-color: #fff; width: 950px; height: 200px;">
			<?php echo "Members in these categories:"?>
			<?php 
			$cCount = count($membersAttached);
			$count = 0;
			foreach ($membersAttached as $key =>$val) {
			
				echo $val['UserReference']['first_name'].' '.$val['UserReference']['last_name'];
				
				if($count != $cCount-1)
					echo ",";
				$count++;
			}?>
		</div>
		
	</div>
	
<script type="text/javascript">
<!--//--><![CDATA[//><!--
    jQuery(document).ready(function()
    {
    	jQuery('#submit_button').click(function()
        {
        	
        		var myValues;
	
				for (var i = 0 ; i < 3 ; i++) {
					
					if(topicVal[i] != "-1") {
						if( i == 0)
							myValues = topicVal[i];
						else
							myValues = myValues + "," + topicVal[i];
					}
					
				}
				console.log(myValues);
				//MultiSelect
				  jQuery('#hiddenSelect').val(myValues);
				//MultiSelect - End
	
    		jQuery(this).attr('disabled',true);
    		jQuery('#qForm').submit();
        });
    });
//--><!]]>
</script>

