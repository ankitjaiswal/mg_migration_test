<?php echo($this->Html->script(array('fancybox/jquery.fancybox-1.3.4.pack', 'fancybox/jquery.mousewheel-3.0.4.pack'), false)); ?>
<?php //e($html->css(array('jquery.fancybox-1.3.4')), false); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('.addAttribute').fancybox();
    });		
</script>
<div class="adminrightinner">
    <div class="tablewapper2 AdminForm">
        <h3 class="legend1">Search Member</h3>
        <?php echo($this->Form->create('UserReference', array('url' => array('admin' => true, 'controller' => 'qna', 'action' => 'admin_category_member_search')))); ?>
        <table border="0" width="100%" class="Admin2Table">
            <tbody>
                <tr>
 
                    <td width="18%" valign="middle" class="Padright26">Member's First Name :</td>
                    <td><?php echo($this->Form->input('first_name', array('label' => false, 'div' => false, 'class' => 'InputBox'))); ?>  </td>
                </tr>
                <tr>
                    <td valign="middle" class="Padleft26">&nbsp;</td>
                    <td align="left"> 

                        <?php echo($this->Form->submit('Search', array('div' => false, 'class' => 'submit_button'))); ?>                 
                    </td>

                </tr>             
            </tbody>
        </table> 
        <?php echo($this->Form->end()); ?>    
        <div style="clear: both;"></div>
    </div>
</div>


<div class="fieldset">
    <h3 class="legend">
		Member Categories
        <div class="total" style="float:right"> Total Members : <?php echo($this->request["paging"]['User']["count"]); ?> 
        </div>
    </h3>
    <div class="adminrightinner" style="padding:0px;">
        <?php echo($this->Form->create('User', array('name' => 'Admin', 'url' => array('controller' => 'qna', 'action' => 'admin_category_member_search')))); ?>    
        <input type="hidden" name="pageAction" id="pageAction"/>	 
        <?php
        if (!empty($data)) {
            ?> 

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td width="20%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Member Name</td>
                        <td width="11%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Expertise Tags</td>
                        <td width="23%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Category 1 Id</td>
						<td width="23%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Category 2 Id</td>
						<td width="23%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Category 3 Id</td>
                    </tr>	
               <?php

foreach ($data as $value) {

 $topicarray = array();


if($value['MemberIndustryCategory']['topic1'] != -1 && $value['MemberIndustryCategory']['topic2'] != -1 && $value['MemberIndustryCategory']['topic3'] != -1 && $value['MemberIndustryCategory']['topic4'] != -1 && $value['MemberIndustryCategory']['topic5'] != -1){
 $topicarray = $categories[$value['MemberIndustryCategory']['topic1']].",".$categories[$value['MemberIndustryCategory']['topic2']].",".$categories[$value['MemberIndustryCategory']['topic3']].",".$categories[$value['MemberIndustryCategory']['topic4']].",".$categories[$value['MemberIndustryCategory']['topic5']];

}
else if($value['MemberIndustryCategory']['topic1'] != -1 && $value['MemberIndustryCategory']['topic2'] != -1 && $value['MemberIndustryCategory']['topic3'] != -1 && $value['MemberIndustryCategory']['topic4'] != -1 && $value['MemberIndustryCategory']['topic5'] == -1){
 $topicarray = $categories[$value['MemberIndustryCategory']['topic1']].",".$categories[$value['MemberIndustryCategory']['topic2']].",".$categories[$value['MemberIndustryCategory']['topic3']].",".$categories[$value['MemberIndustryCategory']['topic4']];

}
else if($value['MemberIndustryCategory']['topic1'] != -1 && $value['MemberIndustryCategory']['topic2'] != -1 && $value['MemberIndustryCategory']['topic3'] != -1 && $value['MemberIndustryCategory']['topic4'] == -1 && $value['MemberIndustryCategory']['topic5'] == -1){
 $topicarray = $categories[$value['MemberIndustryCategory']['topic1']].",".$categories[$value['MemberIndustryCategory']['topic2']].",".$categories[$value['MemberIndustryCategory']['topic3']];

}
else if($value['MemberIndustryCategory']['topic1'] != -1 && $value['MemberIndustryCategory']['topic2'] != -1 && $value['MemberIndustryCategory']['topic3'] == -1 && $value['MemberIndustryCategory']['topic4'] == -1 && $value['MemberIndustryCategory']['topic5'] == -1){
 $topicarray = $categories[$value['MemberIndustryCategory']['topic1']].",".$categories[$value['MemberIndustryCategory']['topic2']];

}
else if($value['MemberIndustryCategory']['topic1'] != -1 && $value['MemberIndustryCategory']['topic2'] == -1 && $value['MemberIndustryCategory']['topic3'] == -1 && $value['MemberIndustryCategory']['topic4'] == -1 && $value['MemberIndustryCategory']['topic5'] == -1){
 $topicarray = $categories[$value['MemberIndustryCategory']['topic1']];

}
}?>

                    <?php 

                    $options = array();
                    $count = 0;
                    $options[-1] = '';

					$rank_options = array(0,1,2,3);

					$industry_options = array();
					$category_options = array();
					$parentss = array();
					$industry_options[-1] = '';
					
                    foreach ($industry_categories as $value) {
						$parents[$value['IndustryCategory']['id']] = $value['IndustryCategory']['parent'];
	                    
	                    $options[$value['IndustryCategory']['id']] = $value['IndustryCategory']['category'];
						
						if($value['IndustryCategory']['parent'] == 0) 
							$industry_options[$value['IndustryCategory']['category']] = $value['IndustryCategory']['category'];
                    }
                    
					foreach ($industry_categories as $value) {
						
						if($value['IndustryCategory']['parent'] > 0)  {
									
							if($category_options[$options[$value['IndustryCategory']['parent']]] == '')
								$category_options[$options[$value['IndustryCategory']['parent']]] = $value['IndustryCategory']['category'];
							else
								$category_options[$options[$value['IndustryCategory']['parent']]] = $category_options[$options[$value['IndustryCategory']['parent']]].','.$value['IndustryCategory']['category'];
						}
                    }
										
					foreach ($category_options as $key => $value) {
							?>
						<input type="hidden" name="<?php echo($key); ?>" value="<?php echo($value);?>" id="<?php echo($key); ?>" />
						<?php
					}
					$industry_options = array_unique($industry_options);
					
                    $count = 0;
                    foreach ($data as $value) {
                        ?>
                        <input type="hidden" name="data[User][Member_category][<?php echo($count)?>][user_id]" value="<?php echo($value['User']['id']);?>" id="0member_id" />
                        <tr>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php echo(ucwords($value['UserReference']['first_name']) . ' ' . $value['UserReference']['last_name']); ?>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <?php echo($this->Form->input("Member_category][".$count."][categories_tags]", array('div'=>false,'label'=>false, "style"=>"width:300px;", 'value'=>$topicarray)));
                                
                                if($value['MemberIndustryCategory']['category1'] != -1 || $value['MemberIndustryCategory']['category2'] != -1 || $value['MemberIndustryCategory']['category3'] != -1 ) {
                                	?>
                                	<p>
                                		<span>
                                			<b>Industry1 : </b><?php echo($options[$parents[$value['MemberIndustryCategory']['category1']]])  ?> <br>
                                			<b>Category1 :</b> <?php echo($options[$value['MemberIndustryCategory']['category1']])  ?><br>
                                		</span>
                                		<span>
                                			<b>Industry2 : </b><?php echo($options[$parents[$value['MemberIndustryCategory']['category2']]])  ?> <br>
                                			<b>Category2 : </b><?php echo($options[$value['MemberIndustryCategory']['category2']])  ?><br>
                                		</span>
                                		<span>
                                			<b>Industry3 : </b><?php echo($options[$parents[$value['MemberIndustryCategory']['category3']]])   ?> <br>
                                			<b>Category3 : </b><?php echo($options[$value['MemberIndustryCategory']['category3']])  ?><br>
                                		</span>
                                		
                                		Quality Weight
                                		<?php echo($this->Form->input("Member_category][".$count."][quality]", array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"industry_id".$count, "style"=>"width:160px;",'options'=>$rank_options, 'value'=>$value['MemberIndustryCategory']['quality'] )));?>


                                	</p>
                                <?php } ?>
                                
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                <p>
                                	Industry
                                	<?php echo($this->Form->input("Member_category][".$count."][industry_id1]", array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"industry_id1".$count, "style"=>"width:160px;",'options'=>$industry_options, 'value'=>'')));?>
                                	Category
                                	<?php echo($this->Form->input("Member_category][".$count."][ind_category_id1]", array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"ind_category_id1".$count, "style"=>"width:160px;",'options'=>'', 'value'=>'')));?>
                                </p>
                            </td>
                            <td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
                                                                <p>
                                	Industry
                                	<?php echo($this->Form->input("Member_category][".$count."][industry_id2]", array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"industry_id2".$count, "style"=>"width:160px;",'options'=>$industry_options, 'value'=>'')));?>
                                	Category
                                	<?php echo($this->Form->input("Member_category][".$count."][ind_category_id2]", array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"ind_category_id2".$count, "style"=>"width:160px;",'options'=>'', 'value'=>'')));?>
                                </p>
                            </td>
							<td align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">
							    <p>
                                	Industry
                                	<?php echo($this->Form->input("Member_category][".$count."][industry_id3]", array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"industry_id3".$count, "style"=>"width:160px;",'options'=>$industry_options, 'value'=>'')));?>
                                	Category
                                	<?php echo($this->Form->input("Member_category][".$count."][ind_category_id3]", array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"ind_category_id3".$count, "style"=>"width:160px;",'options'=>'', 'value'=>'')));?>
                                </p>
							</td> 
                        </tr>	
                        <?php
                        $count++;
                    }
                    ?>
                </table>
            </div>
            <div><input type="submit" value="Submit" class="submit_button" /></div>
            <?php
        }
        ?>
<?php if (!empty($data)) { ?>
            <?php
            echo($this->Form->end());
        } else {
            ?>
            <div style="color:blue, font-size:20; padding-bottom:30px;padding-top:30px;" align="center" ><strong>No Records Found.</strong></div>
        <?php } ?>

    </div>
</div>
<div class="clr"></div>
<?php echo $this->Element('Admin/admin_paging', array("paging_model_name" => "User", "total_title" => "User")); ?>
