<script src="https://www.google.com/recaptcha/api.js?render=6LeFKZwUAAAAAN-IYDev4akmb_96e-z3LdH1KYyf"></script>
<script type="text/javascript">

var topicVal = [-1,-1,-1];
var topicCount = 0;
		
function monkeyPatchAutocomplete() {

    jQuery.ui.autocomplete.prototype._renderItem = function( ul, item) {
        var re = new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + this.term + ")(?![^<>]*>)(?![^&;]+;)", "gi");
        var t = item.label.replace(re,"<span style='font-weight:bold;color:#434343;'>" + 
        		"$&" + 
                "</span>");
        return jQuery( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + t + "</a>" )
            .appendTo( ul );
    };
}

function getKeywords(){

		var allKeywords = <?php echo json_encode($allKeywords); ?>;
		
		return allKeywords;
	}
	
function deleteThis(element) {
	console.log("Delete This");
	
	topicVal[element] = -1;
	
	for( var i = 0 ; i < 3 ; i ++) {
		
		if(topicVal[i] == -1) {
			jQuery('#div'+i).css('display','none');
			jQuery('#topic'+i).html('');
		} else {
			jQuery('#div'+i).css('display','inline-block');
			jQuery('#topic'+i).html(topicVal[i]);
		}
	}
}
	
jQuery(document).ready(function(){
	
		monkeyPatchAutocomplete();
		
		var CityKeyword = jQuery('#CityKeyword');
	
		CityKeyword.autocomplete({
		minLength    : 1,
		source        : getKeywords(),
		source		: function(request, response) {
			
		        var results = jQuery.ui.autocomplete.filter(getKeywords(), request.term);
		        
		        results = results.slice(0, 5);
                        results.push('Add <b>"' + jQuery("#CityKeyword" ).val() + '"</b>');
		        response(results); //show 3 items.
		    },
		focus: function( event, ui ) {
			
			if(ui.item.value[0] == "A")
				return false;
	       //jQuery("#CityKeyword" ).val( ui.item.value );
	       return false;
	     },
	     select: function( event, ui ) {

			     var toAdd = ui.item.value;
                              if(ui.item.value[1] == "d"){
                                var total = toAdd.length;
                                var final = total-5;
				toAdd = toAdd.substring(8,final);
                                 }

				
			var changed = -1;
			
			for (var i = 0; i < 3; i++) {
				
				if(topicVal[i] == -1) {
					changed = 1;
					topicVal[i] = toAdd;
					break;
				}
			}
			
			if(changed == -1)
				alert("You can only select 3 values");
			
			for( var i = 0 ; i < 3 ; i ++) {
		
				if(topicVal[i] == -1) {
					jQuery('#div'+i).css('display','none');
					jQuery('#topic'+i).html('');
				} else {
					jQuery('#div'+i).css('display','inline-block');
					jQuery('#topic'+i).html(topicVal[i]);
				}
			}
			
	        jQuery("#CityKeyword" ).val('');
	          
	        return false;
	       }
		});
	});

</script>

    <div id="proposal-page">
        <div class="container">
            <div class="col-md-12 pp-page">
                        <form action="<?php echo SITE_URL ?>qna/add_question" method="post" id="Questionform">
        		<input type="hidden" name="data[QnaQuestion][question_text]" value="<?php echo($Question_text);?>"/>
                        <input type="hidden" name="data[QnaQuestion][question_context]" value="<?php echo($Question_context);?>"/>
                        <input type="hidden" name="data[QnaQuestion][id]" id="qId" value="<?php echo($Question_id);?>"/>
				<div class="pp-heading text-center">
					<h5>Review your question</h5>
					
				</div>
				<div class="project-overview">
					<div class="pp-label">
						<h5><?php echo($Question_text) ?></h5>
						<p><?php echo nl2br($Question_context);?></p>
					</div>

			<div class="pp-label" data-placement="right" data-toggle="DescPopover" data-container="body" style="margin-top:20px;margin-bottom:40px;">

			<input type="text" id="CityKeyword" class="form-control" placeholder="Add topics for industry, function or skills" name="data[City][keyword]">

			</div>
	  		<div id="div0" class="form-group" style="display: none; border-bottom:1px solid #BEBEBE; background:#ccc; border-radius:5px;margin-left:30px;">
		        <span id='close' onclick='deleteThis(0); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
		        <p id="topic0" style="padding-top: 10px; padding-bottom: 0px;padding-left:8px; display: inline-block"></p>
			</div>
			<div id="div1" class="form-group" style="display: none; border-bottom:1px solid #BEBEBE;; background:#ccc; border-radius:5px;">
		        <span id='close' onclick='deleteThis(1); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
		        <p id="topic1" style="padding-top: 10px; padding-bottom: 0px;padding-left:8px; display: inline-block""></p>
			</div>
			<div id="div2" class="form-group" style="display: none; border-bottom:1px solid #BEBEBE; background:#ccc; border-radius:5px;">
		        <span id='close' onclick='deleteThis(2); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
		        <p id="topic2" style="padding-top: 10px; padding-bottom: 0px;padding-left:8px; display: inline-block""></p>
			</div>

                        <?php echo($this->Form->hidden('QnaQuestion.select',array('value'=>'', 'id'=>'hiddenSelect')));?>



                                        <?php if($this->Session->read('Auth.User.id') == '') {?>
					<div class="e-button pull-right" id="visitorbutton" style="margin-top:30px;margin-bottom:60px;">
                                                       
						<label>
							<a href="<?php echo(SITE_URL.'qna/ask_question/')?>" class="hide-fields" style="margin-right:10px;">
								Edit
							</a>
						</label>
						
 
						<button class="banner-getstarted btn btn-default get-started" id="visitorformopen">
							Submit your question
						</button>
                                               <div class="g-recaptcha" data-sitekey="6Lf_JZsUAAAAAAujKT9Be6kS6M5_t1CrrCdBVXDh"></div>
					</div>
                                      <?php } else{?>


					<div class="e-button pull-right" style="margin-top:30px;margin-bottom:60px;">
						<label>
							<a href="<?php echo(SITE_URL.'qna/ask_question/')?>" class="hide-fields" style="margin-right:10px;">
								Edit
							</a>
						</label>
						<button class="banner-getstarted btn btn-default get-started" id="loggedinsubmit">
							Submit your question
						</button>
					</div>
                                      <?php } ?>

                      </form>
					  

		   </div>
		</div>




			<div class="col-md-12 guest-signup" id="visitorform" style="display:none;">
				<div class="g-heading text-center">
					<img src="<?php echo(SITE_URL)?>yogesh_new1/images/arrow.png">
					<label>To continue, Register or Submit as a guest</label>
				</div>
				<div class="col-md-12 pad0" id="guest-container">
					<div class="col-md-6 text-center">
						<div class="g-linkedin">
							<a href="#" id="linkedinbuttonclick">
								<img src="<?php echo(SITE_URL)?>yogesh_new1/images/linkedin.png">
							</a>
							<p>GUILD will never post to your LinkedIn account without your permission.</p>
						</div>
					</div>
					<div class="col-md-6 mpad0">
						<div class="apply-form">
							<form method="post" action="<?php echo (SITE_URL); ?>fronts/new_add_question" id="newqnaForm">
								<div class="form-group width49">
									<input type="text" name="data[Mentorship][firstname]" id="userfname" class="form-control" placeholder="First Name">
								</div>
								<div class="form-group width49">
									<input type="text" name="data[Mentorship][lastname]" id="userlname" class="form-control"  placeholder="Last Name">
								</div>
								<div class="form-group width49">
									<input type="email" name="data[Mentorship][email]" id="useremail" class="form-control " placeholder="email@yourcompany.com" onchange="return ChangeUsername();">
                                                                        <span id="Emailalready" class="errormsg" style="display:inline; float:left; display:none;"><?php echo $error; ?></span>
								</div>
                                                                <div class="form-group width49">
                                                                <input type="password" name="data[Mentorship][password]" id="userpassword" class="form-control" placeholder="Password">
                                                                </div>
								<div class="form-group width49">
									<input type="text" name="data[Mentorship][phone]" id="userphone" class="form-control" placeholder="Phone Number">
								</div>
									
									
							</form>
						</div>
					</div>
				</div>
				<div class="e-button pull-right" style="margin-top:60px;margin-bottom:60px;">
					<label>
						<a href="<?php echo(SITE_URL.'qna/ask_question/')?>" class="hide-fields" style="margin-right:20px;">
							EDIT
						</a>
					</label>
					<button class="banner-getstarted btn btn-default get-started" id="visitorsubmit">
						Submit your question
					</button>
				</div>
			</div>



		</div>
	</div>
	<!-- js link -->

	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>



<script type="text/javascript">
jQuery(document).ready(function(){

        var mailformat=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    	jQuery("#loggedinsubmit").click(function(){

        var flag = 0;
        
        var myValues;
	
		for (var i = 0 ; i < 3 ; i++) {
			
			if(topicVal[i] != "-1") {
				if( i == 0)
					myValues = topicVal[i];
				else
					myValues = myValues + "," + topicVal[i];
			}
			
		}
	
        jQuery('#hiddenSelect').val(myValues);
        

    	       if(flag > 0) {
    			 return false;
    		} else {
			jQuery("#Questionform").submit();
			return true;
		}

    });


	jQuery("#visitorformopen").click(function(){
           
         document.getElementById('visitorform').style.display = 'inline-block';
         document.getElementById('visitorbutton').style.display = 'none';
    	 jQuery('html, body').animate({
    		 scrollTop: jQuery("#visitorform").offset().top
         }, 2000);
         return false;
         
    });

	jQuery("#linkedinbuttonclick").click(function(){

		var myValues;
	
		for (var i = 0 ; i < 3 ; i++) {
			
			if(topicVal[i] != "-1") {
				if( i == 0)
					myValues = topicVal[i];
				else
					myValues = myValues + "," + topicVal[i];
			}
			
		}
	
       jQuery('#hiddenSelect').val(myValues);
        
       var qId = jQuery('#qId').val();
             
       jQuery.ajax({
			url:SITE_URL+'/qna/add_question',
			type:'post',
			dataType:'json',
			data: 'checkBoxes='+myValues + '&qId=' + qId,
			success:function(){
				console.log(qId);
				window.location.href = SITE_URL+'linkedin/OAuth2.php';
			}
		});	
	            return false;
      });

    		jQuery("#visitorsubmit").click(function() {

       	       var myValues;
	
		for (var i = 0 ; i < 3 ; i++) {
			
			if(topicVal[i] != "-1") {
				if( i == 0)
					myValues = topicVal[i];
				else
					myValues = myValues + "," + topicVal[i];
			}
			
		}
	
                jQuery('#hiddenSelect').val(myValues);
        

                var qId = jQuery('#qId').val();
                            
                jQuery.ajax({
			url:SITE_URL+'/qna/add_question',
			type:'post',
			dataType:'json',
			data: 'checkBoxes='+myValues + '&qId=' + qId,
			success:function(){
				
				var flag = 0;
				
		 if(jQuery.trim(jQuery("#userfname").val()) == '') {
                    jQuery('#userfname').css('border-color','#F00');
                    jQuery('#userfname').focus();                
                    flag++;
                 }else {
                    jQuery('#userfname').css('border-color','');                 
                 }
		         if(jQuery.trim(jQuery("#userlname").val()) == '') {
                    jQuery('#userlname').css('border-color','#F00');
                    jQuery('#userlname').focus();                
                    flag++;
                 }else {
                    jQuery('#userlname').css('border-color','');                 
                 }
		  if(jQuery.trim(jQuery("#useremail").val()) == '') {
                    
                    jQuery('#useremail').css('border-color','#F00');
                    jQuery('#useremail').focus();                
                    flag++;
                 }else if(!jQuery('#useremail').val().match(mailformat)){
                  
                    jQuery('#useremail').css('border-color','#F00');
                    jQuery('#useremail').focus();                
                    flag++;                    
                 } else {
                      
                    jQuery('#useremail').css('border-color','');                 
                 }
		      	 if(jQuery.trim(jQuery("#userpassword").val()) == '') {
                    jQuery('#userpassword').css('border-color','#F00');
                    jQuery('#userpassword').focus();                
                    flag++;
                 }else {
                    jQuery('#userpassword').css('border-color','');                 
                 }
		 if(jQuery.trim(jQuery("#userphone").val()) == '') {
                    jQuery('#userphone').css('border-color','#F00');
                    jQuery('#userphone').focus();                
                    flag++;
                 }else {
                    jQuery('#userphone').css('border-color','');                 
                 }
    			 if(flag > 0) {
    				return false;
    			 } else {


                     var username=jQuery.trim(jQuery('#useremail').val());

			$.ajax({
			url:SITE_URL+'/members/checkUsername',
			type:'post',
                        dataType:'json',
			data:'email='+username,
			success:function(res)
			 {
				if(res.value){

                          $('#useremail').css('border-color','#F00');jQuery('#Emailalready').html('This email is already registered. Please login instead.');
                          $('#Emailalready').show();
                          }
                           else
                          {
                          $('#useremail').css('border-color','');jQuery('#Emailalready').hide();
                          $("#newqnaForm").submit();
                          }
			 }

			});
					
                 }
			}
			});	
			return false;
    		});


 });   		
    		
</script>

<script type="text/javascript">

       function ChangeUsername(){
          var username=jQuery.trim(jQuery('#useremail').val());

			$.ajax({
			url:SITE_URL+'/members/checkUsername',
			type:'post',
                        dataType:'json',
			data:'email='+username,
			success:function(res)
			 {
				if(res.value){

                          jQuery('#useremail').css('border-color','#F00');jQuery('#Emailalready').html('This email is already registered. Please login instead.');
                          jQuery('#Emailalready').show();
                          }
                           else
                          {
                          jQuery('#useremail').css('border-color','');jQuery('#Emailalready').hide();
                          }
			 }

			});


        }

</script>
 <script>
  grecaptcha.ready(function() {
      grecaptcha.execute('6LeFKZwUAAAAAN-IYDev4akmb_96e-z3LdH1KYyf', {action: 'homepage'})
  });
  $(document).ready(function(){
  grecaptcha.execute();
  });
  </script>
