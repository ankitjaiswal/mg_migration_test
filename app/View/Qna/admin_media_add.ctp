<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<script type="text/javascript">

var topicVal = [-1,-1,-1];
var topicCount = 0;
		


function getKeywords(){
		var allKeywords = <?php echo json_encode($allKeywords); ?>;
		console.log(allKeywords);
		return allKeywords;
	}
	
function monkeyPatchAutocomplete() {

    jQuery.ui.autocomplete.prototype._renderItem = function( ul, item) {
        var re = new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + this.term + ")(?![^<>]*>)(?![^&;]+;)", "gi");
        var t = item.label.replace(re,"<span style='font-weight:bold;color:#434343;'>" + 
        		"$&" + 
                "</span>");
        return jQuery( "<li></li>" )
            .data( "item.autocomplete", item )
            .append( "<a>" + t + "</a>" )
            .appendTo( ul );
    };
}

function deleteThis(element) {
	console.log("Delete This");
	
	topicVal[element] = -1;
	
	for( var i = 0 ; i < 3 ; i ++) {
		
		if(topicVal[i] == -1) {
			jQuery('#div'+i).css('display','none');
			jQuery('#topic'+i).html('');
		} else {
			jQuery('#div'+i).css('display','inline-block');
			jQuery('#topic'+i).html(topicVal[i]);
		}
	}
}
	
jQuery(document).ready(function(){
	
		monkeyPatchAutocomplete();
		
		var CityKeyword = jQuery('#CityKeyword');
	         //alert(CityKeyword);
		CityKeyword.autocomplete({
		minLength    : 1,
		source        : getKeywords(),
		source		: function(request, response) {
			
		        var results = jQuery.ui.autocomplete.filter(getKeywords(), request.term);
		        
		        results = results.slice(0, 5);
                        results.push('Add <b>"' + jQuery("#CityKeyword" ).val() + '"</b>');
		        response(results); //show 3 items.
		    },
		focus: function( event, ui ) {
			
			if(ui.item.value[0] == "A")
				return false;
	       //jQuery("#CityKeyword" ).val( ui.item.value );
	       return false;
	     },
	     select: function( event, ui ) {

			     var toAdd = ui.item.value;
                              if(ui.item.value[1] == "d"){
                                var total = toAdd.length;
                                var final = total-5;
				toAdd = toAdd.substring(8,final);
                                 }

				
			var changed = -1;
			
			for (var i = 0; i < 3; i++) {
				
				if(topicVal[i] == -1) {
					changed = 1;
					topicVal[i] = toAdd;
					break;
				}
			}
			
			if(changed == -1)
				alert("You can only select 3 values");
			
			for( var i = 0 ; i < 3 ; i ++) {
		
				if(topicVal[i] == -1) {
					jQuery('#div'+i).css('display','none');
					jQuery('#topic'+i).html('');
				} else {
					jQuery('#div'+i).css('display','inline-block');
					jQuery('#topic'+i).html(topicVal[i]);
				}
			}
			
	        jQuery("#CityKeyword" ).val('');
	          
	        return false;
	       }
		});
	});

</script>

<div class="adminrightinner">
	<?php echo($this->Form->create(Null, array('url' => array('controller' => 'qna', 'action' => 'admin_media_add'),'type'=>'file')));?>     
	<div class="tablewapper2 AdminForm">
		<h3 class="legend1">Add Media Query</h3>
		<table border="0" class="Admin2Table" width="100%">
                     <?php $currentTime = date('Y-m-d 23:59:59');?>
			<tr>
				<td valign="middle" class="Padleft26">Deadline</td>
				<td><?php echo($this->Form->input('MediaRecords.deadline', array('div'=>false, 'label'=>false, "class" => "Testbox5",'value'=>$currentTime)));?></td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Reporter name</td>
				<td><?php echo($this->Form->input('MediaRecords.reporter_name', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Reporter email</td>
				<td><?php echo($this->Form->input('MediaRecords.reporter_email', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Media outlet</td>
				<td><?php echo($this->Form->input('MediaRecords.media_outlet', array('div'=>false, 'label'=>false, "class" => "Testbox5")));?>
				</td>
			</tr>
                     <tr>
				<td valign="middle" class="Padleft26">Subject</td>
				<td><?php echo($this->Form->input('MediaRecords.subject', array('div'=>false, 'label'=>false, "class" => "Testbox", "style" =>"width: 800px;height: 22px;")));?>
				</td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Reporter query</td>
				<td><?php echo($this->Form->input('MediaRecords.reporter_query', array('div'=>false, 'label'=>false, "class" => "Testbox", "style" =>"width: 800px;height: 140px;")));?>
				</td>
			</tr>
			<tr>
				<td valign="middle" class="Padleft26">Requirement</td>
				<td><?php echo($this->Form->input('MediaRecords.requirement', array('div'=>false, 'label'=>false, "class" => "Testbox", "style" =>"width: 800px;height: 140px;")));?>
				</td>
			</tr>




                            <tr>
                                  <td valign="middle" class="Padleft26">Ask an expert</td>
	                        	<td>     <?php $option = array('Yes'=>'Yes','No'=>'No'); ?>
		                          <?php echo($this->Form->input('MediaRecords.checkbox', array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','options'=>$option, "style"=>"width:50px; color: #000")));?>
                                </td>
                            </tr>
		</table>


           <div style=" width: 100%;  padding-bottom: 30px; display: "inline-block">
	  		<p>
	  			<div id="enter-text">
					<?php 	echo($this->Form->input('City.keyword',array('id'=>'CityKeyword','div'=>false,'label'=>false,'class'=>'forminput-new','type'=>'text',"placeholder"=>"Requested area of expertise",'value'=>'','style'=>'width:673px;')))	?>
				</div>
	  		</p>
	  		<div id="div0" style="display: none;margin-top:30px; border-bottom:1px solid #BEBEBE;margin-bottom:20px;padding-bottom:10px; background:#ccc; border-radius:5px;">
		        <span id='close' onclick='deleteThis(0); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
		        <p id="topic0" style="padding-top: 10px; padding-bottom: 0px;padding-left:8px; display: inline-block"></p>
			</div>
			<div id="div1" style="display: none;margin-top:30px; border-bottom:1px solid #BEBEBE;margin-bottom:20px;padding-bottom:10px; background:#ccc; border-radius:5px;">
		        <span id='close' onclick='deleteThis(1); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
		        <p id="topic1" style="padding-top: 10px; padding-bottom: 0px;padding-left:8px; display: inline-block""></p>
			</div>
			<div id="div2" style="display: none;margin-top:30px; border-bottom:1px solid #BEBEBE;margin-bottom:20px;padding-bottom:10px; background:#ccc; border-radius:5px;">
		        <span id='close' onclick='deleteThis(2); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
		        <p id="topic2" style="padding-top: 10px; padding-bottom: 0px;padding-left:8px; display: inline-block""></p>
			</div>


        </div>
        <?php echo($this->Form->hidden('MediaRecords.select',array('value'=>'', 'id'=>'hiddenSelect')));?>

	</div>
	<div class="buttonwapper">
		<div><input type="submit" value="Submit" class="submit_button" id="submit"/></div>
		<div class="cancel_button"><?php echo $this->Html->link("Cancel", "/admin/qna/media_record/", array("title"=>"", "escape"=>false)); ?>
		</div>
	</div>
	<?php echo($this->Form->end()); ?>	
</div>

<script type="text/javascript">
jQuery(document).ready(function(){
jQuery("#submit").click(function(){
        var myValues;
	
		for (var i = 0 ; i < 3 ; i++) {
			
			if(topicVal[i] != "-1") {
				if( i == 0)
					myValues = topicVal[i];
				else
					myValues = myValues + "," + topicVal[i];
			}
			
		}
	
        jQuery('#hiddenSelect').val(myValues);

});
});
</script>

