<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth" style=" margin-bottom:30px; min-height: 500px;">
    <div id="financial-management">
        <p>
		<span><?php echo($queryString);?></span> 
		</p>
    </div>
    <div id="mentors-content">
    <?php $countA = 1; $qCount = count($questionArray); foreach ($questionArray as $key => $qaValue) {?>
        <div class="TXT-LHight">
            <p>
                <a href="<?php echo(SITE_URL.'qna/question/'.$qaValue['QnaQuestion']['url_key'])?>" class="questionA"><?php echo($qaValue['QnaQuestion']['question_text'])?> </a>
               <?php  echo($this->Html->link("Read more",SITE_URL.'qna/question/'.$qaValue['QnaQuestion']['url_key'],array('escape'=>false,'class'=>'readMore')));?>
            </p>
            <div class="QnAbutton">
            	<?php foreach ($qaValue['QnaQuestionCategory'] as $catValue) {
                                                            $str = ($categories[$catValue['category_id']]);
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"&"))
                                                            $str = str_replace("&","_and",($categories[$catValue['category_id']]));
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"/"))
                                                            $str = str_replace("/","_slash",($categories[$catValue['category_id']]));

            		 echo($this->Html->link($categories[$catValue['category_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false)));
                 }?>
                <br/><br/>
            </div>
            <div class="TxTimg">
            
                 <?php foreach ($qaValue['User'] as $uValue) {
            		if(isset($uValue['user_image'])){
            			$image_path = MENTORS_IMAGE_PATH.DS.$uValue['member_id'].DS.$uValue['user_image'];?>
            			<a href="<?php echo(SITE_URL.strtolower($uValue['url_key']));?>">
            				<img src="<?php echo(SITE_URL.'img/'.$image_path)?>" width="30" height="30" alt="<?php echo($uValue['first_name']." ".$uValue['last_name']);?>" title="<?php echo($uValue['first_name']." ".$uValue['last_name']);?>"/>
            			</a>
            		<?php } else {
            			$image_path = 'profile.png';?>
            			<a href="<?php echo(SITE_URL.strtolower($uValue['url_key']));?>">
            				<img src="<?php echo(SITE_URL.'img/media/'.$image_path)?>" width="30" height="30" alt="<?php echo($uValue['first_name']." ".$uValue['last_name']);?>" title="<?php echo($uValue['first_name']." ".$uValue['last_name']);?>"/>
            			</a>
            		<?php }
                 }?>
                
            </div>
            <?php if($key == ($qCount - 1)) { ?>
            <div class="HRLine" style="border-top: 0px;"></div>
            <?php }else {?>
            <div class="HRLine"></div>
            <?php }?>
        </div>
		<?php $countA++;}?>
		<?php if($countA == 1) {?>
			<div class="TXT-LHight">
	            <p>No matching questions</p>
	        </div>
        <?php } ?> 
	</div>
	
	<div id="financial-management" style="margin-top: 40px;">
        <p>
		<span>Insights for <?php echo($queryString);?></span> 
		</p>
    </div>
    <div id="mentors-content">
    <?php $countA = 1; $qCount = count($insights); foreach ($insights as $key => $qaValue) {?>
        <div class="TXT-LHight">
            <p>
                <a href="<?php echo(SITE_URL.'insight/insight/'.$qaValue['Insight']['url_key'])?>" class="questionA"><?php echo($qaValue['Insight']['title'])?> </a>
               <?php  echo($this->Html->link("Read more",SITE_URL.'insight/insight/'.$qaValue['Insight']['url_key'],array('escape'=>false,'class'=>'readMore')));?>
            </p>
            <div class="QnAbutton">
            	<?php foreach ($qaValue['Insight_category'] as $catValue) {

                                                            $str = ($categories[$catValue['category_id']]);
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"&"))
                                                            $str = str_replace("&","_and",($categories[$catValue['category_id']]));
                                                            if(false !== stripos(($categories[$catValue['category_id']]),"/"))
                                                            $str = str_replace("/","_slash",($categories[$catValue['category_id']]));

            		 echo($this->Html->link($categories[$catValue['category_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false)));
            		
                 }?>
                <br/><br/>
            </div>
            <div class="TxTimg">
            
                 <?php 
            		if(isset($qaValue['User']['user_image'])){
            			$image_path = MENTORS_IMAGE_PATH.DS.$qaValue['Insight']['user_id'].DS.$qaValue['User']['user_image'];?>
            			<a href="<?php echo(SITE_URL.strtolower($qaValue['User']['url_key']));?>">
            				<img src="<?php echo(SITE_URL.'img/'.$image_path)?>" width="30" height="30" alt="<?php echo($qaValue['User']['first_name']." ".$qaValue['User']['last_name']);?>" title="<?php echo($qaValue['User']['first_name']." ".$qaValue['User']['last_name']);?>"/>
            			</a>
            		<?php } else {
            			$image_path = 'profile.png';?>
            			<a href="<?php echo(SITE_URL.strtolower($qaValue['User']['url_key']));?>">
            				<img src="<?php echo(SITE_URL.'img/media/'.$image_path)?>" width="30" height="30" alt="<?php echo($qaValue['User']['first_name']." ".$qaValue['User']['last_name']);?>" title="<?php echo($qaValue['User']['first_name']." ".$qaValue['User']['last_name']);?>"/>
            			</a>
            		<?php }
                 ?>
                
            </div>
            <?php if($key == ($qCount - 1)) { ?>
            <div class="HRLine" style="border-top: 0px;"></div>
            <?php }else {?>
            <div class="HRLine"></div>
            <?php }?>
        </div>
		<?php $countA++;}?>
		<?php if($countA == 1) {?>
			<div class="TXT-LHight">
	            <p>No matching insights</p>
	        </div>
        <?php } ?> 
		<a class="reset" href="<?php echo(SITE_URL.'qanda');?>" style="float: left; margin-bottom: 80px; margin-top: 30px;">Back to Q&A</a>
	</div>
	
	</div>
</div>
	