<?php if(isset($Insight_id)){?>
	                	<input type="hidden" name="data[Insight][id]" value="<?php echo($Insight_id);?>"/>
                                <input type="hidden" name="data[Insight][favicon]" value="<?php echo($Insight_favicon);?>"/>
                                <input type="hidden" name="data[Insight][posturl]" value="<?php echo($Insight_posturl);?>"/>
                                <input type="hidden" name="data[Insight][image]" value="<?php echo($Insight_image);?>"/>
<?php }?>

	<div class="container-fluid" id="apply-section">
		<div class="container create-section pad0">
			<div class="col-md-8 pad40">
				<div class="apply-heading">
					<h5>Have a question? Ask our experts</h5>
					<label class="free-c"> (Advice for business leaders)</label>
				</div>
                        <?php if(isset($Question_id)){?>
	                	<input type="hidden" name="data[QnaQuestion][id]" value="<?php echo($Question_id);?>"/>
	                <?php }?>

				<div class="apply-form">
					<form action="<?php echo SITE_URL ?>qna/question_preview" method="post" id="Qnaeditform">

						<div class="form-group">
							<label>Your question</label>
							<span class="pull-right char-limit">(<span id="noOfChar"><?php echo(128 - strlen($Question_text))?></span> characters remaining)</span>
							
                                                        <input id="questionTextBox" class="form-control" name="data[QnaQuestion][question_text]" type="text"  placeholder="Enter question e.g. How do I drive employee engagement?"  value="<?php echo(htmlspecialchars($Question_text))?>" />
						</div>
                        <div class="form-group" data-placement="right" data-toggle="DescPopover" data-container="body">
                            <label>Provide details</label>
                            <textarea id="questionContextBox" class="form-control" rows="14" name="data[QnaQuestion][question_context]"  placeholder="Enter details and background." ><?php echo(htmlspecialchars($Question_context))?></textarea>
                            <div id="popover-content1" class="hide">
								<p>1. Add all relevant information needed for a good answer.</p>
								<p>2. Read out loud to see if it makes sense. </p>
								<p>3. Remove any personally identifiable information (names, phone numbers, etc).</p>
                                                                <p>4. We will send you an email confirmation. </p>
							</div>
                        </div>
						<div class="e-button pull-right">
							<label>
								<a href="<?php echo(SITE_URL)?>qanda">
									Cancel
								</a>
							</label>
							<button class="banner-getstarted btn btn-default get-started" id="applyPreviewIn">
								Preview
							</button>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>
	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/quill.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/script12.js"></script>


	<script type="text/javascript">
	$(function() {
		$( ".data-picker" ).datepicker();
	});
	$(".hide-fields").on("click",function (e) {
		e.preventDefault();
		$(".optional-fields").addClass("hide");
		$(this).addClass("hide");
		$(".show-fields").removeClass("hide");
	});
	$(".show-fields").on("click",function (e) {
		e.preventDefault();
		$(".optional-fields").toggleClass("hide");
		$(this).addClass("hide");
		$(".hide-fields").removeClass("hide");
	});
	if($(window).width() > 991){
		$("[data-toggle=namePopover]").popover({
			html: true, 
			trigger : 'hover',
			content: function() {
				return $('#popover-content').html();
			}
		});
		$("[data-toggle=DescPopover]").popover({
			html: true, 
			trigger : 'hover',
			content: function() {
				return $('#popover-content1').html();
			}
		});
		$("[data-toggle=DescPopover1]").popover({
			html: true, 
			trigger : 'hover',
			content: function() {
				return $('#popover-content2').html();
			}
		});
		$("[data-toggle=DescPopover2]").popover({
			html: true, 
			trigger : 'hover',
			content: function() {
				return $('#popover-content3').html();
			}
		});
	}






</script>


<script type="text/javascript">
jQuery(document).ready(function(){


    function updateCount ()
    {
        var qText = jQuery("#questionTextBox").val();

       if(qText.length < 128) {
           jQuery("#noOfChar").html(128 - qText.length);
       } else {
           jQuery("#noOfChar").html(0);
           jQuery("#questionTextBox").val(qText.substring(0,128));
       }
    }

    jQuery("#questionTextBox").keyup(function () {
        updateCount();
    });
    jQuery("#questionTextBox").keypress(function () {
        updateCount();
    });
    jQuery("#questionTextBox").keydown(function () {
        updateCount();
    });

   
    jQuery("#applyPreview").click(function(){

    	var flag = 0;
    	if(jQuery.trim(jQuery("#questionTextBox").val()) == '') {
			jQuery('#questionTextBox').css('border-color','#F00');
			jQuery('#questionTextBox').focus();
			flag++;
		} else {
			jQuery('#questionTextBox').css('border-color',''); 
		}

    	var context = jQuery.trim(jQuery("#questionContextBox").val());
    	var words = countWords('questionContextBox');
    	if(context == '' ) {
			jQuery('#questionContextBox').css('border-color','#F00');
			jQuery('#questionContextBox').focus();
			flag++;
		} else {
			jQuery('#questionContextBox').css('border-color','');
		}

		if(flag > 0) {
			return false;
		} else {
			jQuery("#Qnaeditform").submit();
			return true;
		}
		
    });
});
</script>
