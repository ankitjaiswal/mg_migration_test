<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Guild</title>
		<!-- Favicon -->
		<link rel="icon" type="image/x-icon" href="favicon.ico"/>
		<link rel="shortcut icon" type="image/x-icon" href="qna/favicon.ico"/>
		<!-- Bootstrap -->
		<link type="text/css" href="../yogesh_new1/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<!-- Custom -->
		<!-- <link type="text/css" href="../yogesh_new1/css/editor.css" rel="stylesheet"> -->
		<link type="text/css" href="../yogesh_new1/css/style.css" rel="stylesheet">
		<!-- Font -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Ubuntu:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
		<!-- Include Quill stylesheet -->
		<link href="https://cdn.quilljs.com/1.0.0/quill.snow.css" rel="stylesheet">
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="page-wrap">
			<header class="site-header">
				<div class="container">
					<!-- /call -->
					<div class="right-info">
						
						<div class="call-info">
							<a href="tel:1-866-511-1898">Call 1-866-511-1898</a>
						</div>
						<div class="membership-btn">
							
							<a class="btn btn-primary-trans" href="#">Consultant Membership</a>
						</div>
					</div>
					<!-- /logo -->
					<div class="logo">
						<a href="index.html"><img src="../yogesh_new1/images/logo.svg" alt="logo" /></a>
						<!-- /nav-button -->
						<button type="button" class="nav-icon collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						</button>
					</div>
					
					<!-- /nav-callapse -->
					<div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
						
						<ul class="nav navbar-nav navbar-right">
							<li><a href="#">Search</a></li>
							<li><a href="#">Case Studie</a></li>
							<li><a href="#">Ask a Expert</a></li>
							<li><a href="#">Project Proposals</a></li>
							<li>
								
								<a href="#">
									<span class="user">
										
										<img width="34px" src="../yogesh_new1/images/Ankit-Jaiswal-Guild.jpg" alt="user" />
									</span>
								</a>
								
							</li>
						</ul>
					</div>
				</div>
			</header>
			<!-- /header -->
			<div class="content-wrap">
				<div class="container">
					
					<div class="row">
						
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							
							<div class="section-que box-wrap">
								<h2 class="main-title">Question: How do we deal with competitors, who are more advanced (funding, experience, design, longer, larger team etc.)</h2>
								<hr>
								<p>We have launched a disruptive startup in the last mile logistics. We are 6 months on the project but have achieved significant traction.</p>
								<p>Our competitors recently launched global PR campaign. They are far more advanced. 2 yrs, 30+ team we are 3), better product design, better funded. globally known names.</p>
								<p>We are not afraid but it seems they will outperform us. We have talked with them, but no cooperation options for now. <br>We have chosen to focus on b2b (they are b2c). What else we can do?</p>
								<p>They are developing hardware. We also do but We want to focus on business development more.</p>
								<div class="btm-info">
									
									<div class="categorie-info pull-left">
										
										<ul>
											
											<li><a href="#">Innovation</a></li>
											<li><a href="#">Small Business/Startup</a></li>
											
										</ul>
									</div>
									<div class="pull-right ans-btn">
										
										<p>Is this your area of expertise?</p> <a href="#" class="btn btn-primary">Add your answer</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							
							<div class="title-box">
								
								<h1>9 Expert answers</h1>
							</div>
							<!-- /heading -->
							<div class="section-ans box-wrap">
								<div class="user-img">
									
									<img src="../yogesh_new1/images/tom.jpg" alt="tom northup" />
								</div>
								<div class="user-info">
									
									<h2 class="title-medium">Tom Northup</h2>
									<p>Leadership, management and strategy coach developing</p>
									<a href="#">3 answers</a>
								</div>
								<hr>
								<div class="description">
									
									<p>You have already focused on b2b as a competitive differentiator. Focus on your client / customer base. <br>Don't look at what you do well but ask what do they want from your business category.<br>-What is most important to them? <br>-What will give them the most value?</p>
									<p>Then work to make these core competencies in your company. The stronger these core competencies the stronger your competitive advantage.<br>
									As a startup you have many competing issues for your time. Be focused, clear and strong on what is most important for your client base.</p>
									<div class="btm-info">
										<div class="ans-btn pull-left">
											
											<a href="#" class="btn btn-primary">UPVOTE | 12</a>
										</div>
										<div class="more-icon pull-right">
											
											<ul>
												
												<li class="mail-btn"><a href="#"><img src="../yogesh_new1/images/mail_red.svg" alt="mail" /></a></li>
												<li class="print-btn"><a href="#"><img src="../yogesh_new1/images/print_red.svg" alt="print" /></a></li>
												<li class="share-btn"><a href="#"><img src="../yogesh_new1/images/share_red.svg" alt="share" /></a></li>
												
											</ul>
										</div>
										<div class="review">
											
											<strong><span>Is this answer helpful?</span> <a href="#">Schedule a free consultation with Tom.</a></strong>
										</div>
									</div>
								</div>
							</div>
							<!-- /box -->
							<div class="section-ans box-wrap">
								<div class="user-img">
									
									<img src="../yogesh_new1/images/steve.jpg" alt="steve johnson" />
								</div>
								<div class="user-info">
									
									<h2 class="title-medium">Steve Johnson</h2>
									<p>The godfather of product management</p>
									<a href="#">8 answers</a>
								</div>
								<hr>
								<div class="description">
									
									<p>When competing with a better funded company, you need to focus on "not". What are you that they are not?<br>
									They are B2C; you're B2B. They are cheap; you are premium. It's a classic case of positioning yourself - and "slightly better" or "a little different" isn't enough. Where are you 10 times better? 100 times better?</p>
									<p>The answer is usually found in focus on a buyer type, a persona. What type of customer do you target? What are their challenges that are not being met by the competitors.</p>
									<div class="btm-info">
										<div class="ans-btn pull-left">
											
											<a href="#" class="btn btn-primary">UPVOTE | 8</a>
										</div>
										<div class="more-icon pull-right">
											
											<ul>
												
												<li class="mail-btn"><a href="#"><img src="../yogesh_new1/images/mail_red.svg" alt="mail" /></a></li>
												<li class="print-btn"><a href="#"><img src="../yogesh_new1/images/print_red.svg" alt="print" /></a></li>
												<li class="share-btn"><a href="#"><img src="../yogesh_new1/images/share_red.svg" alt="share" /></a></li>
												
											</ul>
										</div>
										<div class="review">
											
											<strong><span>Is this answer helpful?</span> <a href="#">Schedule a free consultation with Tom.</a></strong>
										</div>
									</div>
								</div>
							</div>
							<!-- /box -->
							<div class="section-ans box-wrap">
								<div class="user-img">
									
									<img src="../yogesh_new1/images/jason.jpg" alt="jason kitayama" />
								</div>
								<div class="user-info">
									
									<h2 class="title-medium">Jason Kitayama</h2>
									<p>Providing insights into the customer experience to create a xxxxxx </p>
									<a href="#">2 answers</a>
								</div>
								<hr>
								<div class="description">
									
									<p>Consider prioritizing your BD efforts toward areas of deep expertise / product differentiation and developing a product roadmap that strategically grows your offering into a more competitive space.</p>
									<p>Understanding that you are CURRENTLY over-matched can be used to a strong advantage as you temporarily niche yourself with a vision to grow into the priority areas.</p>
									<p>Hopefully you have begun this process with a good enough understanding of your potential customers to develop a more targeted approach to meet their needs. The target groups would be those with specialized needs and industry influence. Use this as the "thin edge of your wedge" for successful entry, and use feedback from your competitor's customers to unravel emerging market needs.</p>
									<div class="btm-info">
										<div class="ans-btn pull-left">
											
											<a href="#" class="btn btn-primary">UPVOTE | 8</a>
										</div>
										<div class="more-icon pull-right">
											
											<ul>
												
												<li class="mail-btn"><a href="#"><img src="../yogesh_new1/images/mail_red.svg" alt="mail" /></a></li>
												<li class="print-btn"><a href="#"><img src="../yogesh_new1/images/print_red.svg" alt="print" /></a></li>
												<li class="share-btn"><a href="#"><img src="../yogesh_new1/images/share_red.svg" alt="share" /></a></li>
												
											</ul>
										</div>
										<div class="review">
											
											<strong><span>Is this answer helpful?</span> <a href="#">Schedule a free consultation with Tom.</a></strong>
										</div>
									</div>
								</div>
							</div>
							<!-- /box -->
							<div class="section-ans box-wrap">
								<div class="user-img">
									
									<img src="../yogesh_new1/images/natalie.jpg" alt="natalie jenkins" />
								</div>
								<div class="user-info">
									
									<h2 class="title-medium">Natalie Jenkins</h2>
									<p>Director, Organizational Development | Innovation Guide</p>
									<a href="#">1 answers</a>
								</div>
								<hr>
								<div class="description">
									
									<p>You've got some good tips here to jump start your thinking. Every business is under constant pressure to perform and deliver value.</p>
									<p>Reading between the lines a bit it would seem that your number one challenge is business development. The fact that the competition you mentioned has a bigger budget, etc. is really just noise to you since you are targeting a different buyer. It'd be an interesting experiment for you to explore what might be all the things you would do if budget wasn't an issue. Once you explore that you could then ask yourselves, How to take action within the constraints of your budget.</p>
									<p>There are variety of ways to creatively explore how you might grow your business. Turning your problems into questions could be your first step to finding viable solutions. This article from Fast Company came across my desk today. It seemed perfect fodder for you http://bit.ly/1Wd7PQI</p>
									<div class="btm-info">
										<div class="ans-btn pull-left">
											
											<a href="#" class="btn btn-primary">UPVOTE | 3</a>
										</div>
										<div class="more-icon pull-right">
											
											<ul>
												
												<li class="mail-btn"><a href="#"><img src="../yogesh_new1/images/mail_red.svg" alt="mail" /></a></li>
												<li class="print-btn"><a href="#"><img src="../yogesh_new1/images/print_red.svg" alt="print" /></a></li>
												<li class="share-btn"><a href="#"><img src="../yogesh_new1/images/share_red.svg" alt="share" /></a></li>
												
											</ul>
										</div>
										<div class="review">
											
											<strong><span>Is this answer helpful?</span> <a href="#">Schedule a free consultation with Tom.</a></strong>
										</div>
									</div>
								</div>
							</div>
							<!-- /box -->
						</div>
					</div>
				</div>
				<!-- Comment Section begin -->
				<div class="container">
					<div class="section-ans box-wrap box-editor">
						<div class="user-img">
							<img src="../yogesh_new1/images/ankitimage.JPG" alt="ankitimage.jpg" />
						</div>
						<div class="user-info">
							<h2 class="title-medium">Ankit Jaiswal</h2>
							<a href="#">0 answers</a>
							<p>(2000 Characters Remaining)</p>
						</div>
						<div class="description">
							<div class="nopadding">
								<!-- Create the toolbar container -->
								<!-- <div id="toolbar">
									<button class="ql-bold">Bold</button>
									<button class="ql-italic">Italic</button>
									<button class="ql-underline">Italic</button>
									<button class="ql-link">Italic</button>
									<button class="ql-list">Italic</button>
									<button class="ql-list">Italic</button>
									<button class="ql-clean">Italic</button>
								</div> -->
								<!-- Create the editor container -->
								<div id="editor" style="height: 200px">
								</div>
							</div>
							<div class="box-btm ans-btn">
								<a href="#" class="btn btn-primary">Cancel</a>
								<a href="#" class="btn btn-primary">Preview</a>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="box-editor-btm">
							<div class="col-md-12">
								<h3>Similar questions</h3>
							</div>
							<div class="col-md-12">
								<div class="box">
									<p>
										Looking for a way to have more strategic executive meetings
										<a class="btn" href="JavaScript:void(0)">Read More</a>
									</p>
									<ul>
										<li><a href="JavaScript:void(0)">Innovation</a></li>
										<li><a href="JavaScript:void(0)">Leadership</a></li>
										<li><a href="JavaScript:void(0)">Small Business/Startup</a></li>
									</ul>
									<div class="box-btm">
										<a href="#">
											<img src="../yogesh_new1/images/Michael-Stratford-Guild.jpg" alt="Michael Stratford" title="Michael Stratford">
										</a>
										<a href="#">
											<img src="../yogesh_new1/images/Iva-Wilson-Guild.jpg" alt="Iva Wilson Guild" title="Iva Wilson Guild">
										</a>
										<a href="#">
											<img src="../yogesh_new1/images/Bill-Shirley-Guild.jpg" alt="Bill Shirley Guild" title="Bill Shirley Guild">
										</a>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="box">
									<p>
										How can IT leaders influence spend on innovation?
										<a class="btn" href="JavaScript:void(0)">Read More</a>
									</p>
									<ul>
										<li><a href="JavaScript:void(0)">Innovation</a></li>
										<li><a href="JavaScript:void(0)">Leadership</a></li>
									</ul>
									<div class="box-btm">
										<a href="#">
											<img src="../yogesh_new1/images/David-Patrishkoff-Guild.jpg" alt="David Patrishkoff Guild" title="David Patrishkoff Guild">
										</a>
										<a href="#">
											<img src="../yogesh_new1/images/David-Kemper-Guild.jpg" alt="David Kemper Guild" title="Davi -Kemper Guild">
										</a>
									</a>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="box">
								<p>
									Link Between "Failing Fast" and Productivity
									<a class="btn" href="JavaScript:void(0)">Read More</a>
								</p>
								<ul>
									<li><a href="JavaScript:void(0)">Innovation</a></li>
									<li><a href="JavaScript:void(0)">Leadership</a></li>
									<li><a href="JavaScript:void(0)">Small Business/Startup</a></li>
								</ul>
								<div class="box-btm">
									<a href="#">
										<img src="../yogesh_new1/images/Mark-Hurwich-Guild.jpg" alt="Mark Hurwich Guild" title="Mark Hurwich Guild">
									</a>
									<a href="#">
										<img src="../yogesh_new1/images/Gary-Brooks-Guild.jpg" alt="Gary Brooks Guild" title="Gary Brooks Guild">
									</a>
									<a href="#">
										<img src="../yogesh_new1/images/Classmates.com Profile Pic.jpg" alt="Classmates.com Profile Pic" title="Classmates.com Profile Pic">
									</a>
								</div>
							</div>
						</div>
					</div>

					<div class="box-editor-btm ask_expert">
						<div class="col-md-12">
							<h3>Ask an expert <sub>Free & anonymous advice for business leaders</sub></h3>
						</div>
						<div class="col-md-12">
							<div class="box">
								<label>Your question <span> (128 characters remaining)</span></label>
								<textarea placeholder="Enter question e.g. How do I drive employee engagement?"></textarea>
								<a href="javascript:;" class="btn btn-primary">Ask now</a>
							</div>
						</div>
					</div>

				</div>
			</div>
			<!-- Comment Section close -->
		</div>
	</div>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="../yogesh_new1/js/bootstrap.min.js"></script>
	<!-- <script src="../yogesh_new1/js/editor.js"></script> -->
	<!-- Include the Quill library -->
	<script src="https://cdn.quilljs.com/1.0.0/quill.js"></script>
	<script>
	// mail
	$(".mail-btn img").mouseover(function() {
	$(this).attr("src","../yogesh_new1/images/mail_dark.svg")
	});
	$(".mail-btn img").mouseout(function() {
	$(this).attr("src","../yogesh_new1/images/mail_red.svg")
	});
	// share
	$(".share-btn img").mouseover(function() {
	$(this).attr("src","../yogesh_new1/images/share_dark.svg")
	});
	$(".share-btn img").mouseout(function() {
	$(this).attr("src","../yogesh_new1/images/share_red.svg")
	});
	// print
	$(".print-btn img").mouseover(function() {
	$(this).attr("src","../yogesh_new1/images/print_dark.svg")
	});
	$(".print-btn img").mouseout(function() {
	$(this).attr("src","../yogesh_new1/images/print_red.svg")
	});
	</script>
	<!-- Initialize Quill editor -->
	<script>
	var toolbarOptions = [
	['bold', 'italic', 'underline', 'link'],        // toggled buttons\
	[{ 'list': 'ordered'}, { 'list': 'bullet' }],
	['clean']                                         // remove formatting button
	];
	var editor = new Quill('#editor', {
	modules: { toolbar: toolbarOptions },
	theme: 'snow',
	placeholder: 'Enter your advice here. Please avoid direct solicitation.'
	});
	</script>
</body>
</html>