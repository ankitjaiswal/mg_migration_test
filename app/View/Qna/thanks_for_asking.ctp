<div id="inner-content-wrapper" class="page-content">
    <div id="inner-content-box" class="container">
        <div id="content-submit" class="text-center thanks-content">
        	<h3>Thanks for asking!</h3>
        	<p>Your question has been submitted. You will receive an Email as soon as an expert answers.</p>
        	<br/>
        	<div class="button-set btn-inline">
        		<?php echo($this->Html->link("Back to Q&A",SITE_URL.'qanda',array('escape'=>false, 'class'=>"reset" )));?>
				<input type="submit" class="LTSpreview btn-red" value="Back to home" onclick="window.location.href='<?php echo SITE_URL?>'; " name="">
			</div>
	    </div>
    </div>
</div>
	
