<?php
if(isset($opennewloginpopup)){?>
	<script type="text/javascript">
		jQuery(document).ready(function(){
		jQuery('#signin-Modal').modal();
		});
	</script>
<?php
}?>

<?php $completelink = SITE_URL."qna/question/".($question['QnaQuestion']['url_key']);?>
<?php $tinyUrl = $this->General->get_tiny_url(urlencode($completelink));?>
<?php $atMentorsGuild = "@GuildAlerts:"?>
<?php $atsybmol = "@"?>
<?php $MentorsGuild = "GUILD: "?>
<?php $questionatguild = "Question at GUILD"?>
<?php $compTitle = $question['QnaQuestion']['question_text'];?>
<?php
	if(false !== stripos($compTitle,"'")){
	$compTitle = str_replace("'"," ",$compTitle);
    }
	if(false !== stripos($compTitle,'"')){
	$compTitle = str_replace('"'," ",$compTitle);
    }
	if(false !== stripos($compTitle,'&')){
	$compTitle = str_replace('&',"and",$compTitle);
    }

	if(false !== stripos($compTitle,'(')){
	$compTitle = str_replace('&'," ",$compTitle); 	
	}
	if(false !== stripos($compTitle,')')){
	$compTitle = str_replace('&'," ",$compTitle); 	
    }
?>
<?php if(isset($focusid) && $focusid != ''){?>
<input type="hidden" name="Language" value="<?php echo($focusid)?>" id="focusJS">
<?php }?>
<?php $linkTitle = $question['QnaQuestion']['question_text'];?>
<div class="content-wrap">
	<div class="container">
		
		<div class="row">
			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
				<div class="section-que box-wrap">
					<h2 class="main-title">Question: <?php echo($question['QnaQuestion']['question_text']); ?></h2>
					<div class="more-icon pull-right social-btn">
						<!-- Contact modal -->
						<div class="modal fade" id="squarespaceModalQuestion" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
						  <div class="modal-dialog question">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
									<h3 class="modal-title" id="lineModalLabel">Share A Question</h3>
								</div>
								<form action="<?php echo SITE_URL ?>qna/share_question" method="post" id="Sharequestionform">
								<div class="modal-body">
						            <!-- content goes here -->
									
						              	<div class="form-group first">
						              	  <input type="first-name" class="form-control" id="firstname" placeholder="First name" name="data[FName]">
						              	</div>
						              	<div class="form-group last">
						              	  <input type="last-name" class="form-control" id="lastname" placeholder="Last name" name="data[LName]">
						              	</div>
						              	<div class="form-group">
						              	  <input type="email" class="form-control" id="emailaddress" placeholder="Enter email" name="data[EAddress]">
						              	</div>
						              	<div class="form-group">
						              	  <input type="text" class="form-control" id="emailsubject" placeholder="Email subject" name="data[ESubject]" value="<?php echo ($loggedinuser['UserReference']['first_name']." ".$loggedinuser['UserReference']['last_name']);?> has shared a question on GUILD with you">
						              	</div>										
						              	<div class="form-group">
										  <input type="hidden" name="data[EText]"  id="qemailcontent"/>
						              	  <div class="form-control editor" name="comment" form="usrform"  placeholder="Type your message here..." id="qemaildiv" onClick="this.contentEditable='true';">
										 </br>
										  
										     <b><?php echo ($loggedinuser['UserReference']['first_name']." ".$loggedinuser['UserReference']['last_name']);?> has shared a question on GUILD with you.</b>
                                                                                     </br></br>
											 The question on GUILD @guildalerts: <b><?php echo($question['QnaQuestion']['question_text']); ?></b>
                                                                                         </br></br>
											 To see the question, visit: <?php echo($this->Html->link("Question Link",SITE_URL.'qna/question/'.$question['QnaQuestion']['url_key'],array('escape'=>false,'class'=>'btn que-link','title'=>'Question Link')));?>.
                                                                                      
                                                                                       
										  </div>
						              	  <div class="form-control editor" name="comment" form="usrform"  placeholder="Type your message here..." id="qemaildiv1" style="display:none;">
										 	</br>
										  
										     <b><?php echo ($loggedinuser['UserReference']['first_name']." ".$loggedinuser['UserReference']['last_name']);?> has shared a question on GUILD with you.</b>
                                                                                     </br></br>
											 The question on GUILD @guildalerts: <b><?php echo($question['QnaQuestion']['question_text']); ?></b>
                                                                                         </br></br>
											 To see the question, visit: <?php echo($this->Html->link("Question Link",SITE_URL.'qna/question/'.$question['QnaQuestion']['url_key'],array('escape'=>false,'class'=>'btn que-link','title'=>'Question Link')));?>.
                                                                                      
                                                                                       
										  </div>										  
						              	</div>
                                                                <span style="color:#CF3135;display:none;" id="captchaerror">Captcha validation is required</span>
						              	<div class="g-recaptcha" data-sitekey="6LePDikUAAAAAJBAS-DW3cEDGsr3liRIBvX7M9w5"></div> 
						            

								</div>
								<div class="modal-footer">
									<a class="send" type="submit" href="#" data-action="save" name="submit" id="sharequestionbutton">Send</a>
								</div>
								</form>
							</div>
						  </div>
						</div>
						<ul>
						    <input type="hidden" value="<?php echo($question['QnaQuestion']['question_text']); ?>"  id="questiontitle"/>
							<input type="hidden" value="<?php echo $completelink;?>"  id="questionshorturl"/>
							<li class="share-btn"><a class="social-button" href="javascript:void(0);"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/share_dark.svg" alt="share" /></a></li>
							<?php if($this->Session->read('Auth.User.id')!=''){?>							
							<li class="mail-btn"><a href="#" data-toggle="modal" onclick="quesshare();"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mail_dark.svg" alt="mail" /></a></li>
							<?php }else{?>
							<li class="mail-btn"><a href="javascript:void(0);" data-toggle="modal" data-target="#signin-Modal"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mail_dark.svg" alt="mail" /></a></li>	
							<?php }?>							

						</ul>
						<ul class="list-unstyled inactive hide">
							<?php
							$login = "o_4urqen5mkq";
							$appkey = "R_7aa87ace460abde4cfc31fa31b2e59ef";
							$completelink = SITE_URL."qna/question/".($question['QnaQuestion']['url_key']);
							$answerid = $answer['QnaAnswer']['id'];
							$bitlyUrl =  $this->General->get_bitly_url($completelink, $answerid,$login, $appkey);
						    $linkdinUrl="https://www.linkedin.com/shareArticle?mini=true&url=".$completelink."&title=".$questionatguild."&#58; " .$compTitle;
							$tweet="https://twitter.com/intent/tweet?text=".$questionatguild." ". $atMentorsGuild." ".$compTitle." " .$bitlyUrl;
							?>
							
							<li>
								<a href="javascript:void(0);" onclick="javascript:window.open('<?php echo ($tweet);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')" ">
									<!-- <img src="<?php echo(SITE_URL)?>yogesh_new1/images/twit_icon.png" alt="Twitter Share" data-pin-nopin="true"> -->
									<i class="fa fa-twitter" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" onclick="javascript:window.open('<?php echo ($linkdinUrl);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
									<!-- <img src="<?php echo(SITE_URL)?>yogesh_new1/images/logo_linkedin.png" alt="LinkedIn Share"> -->
									<i class="fa fa-linkedin" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" onclick="javascript:window.open('https://www.facebook.com/sharer.php?s=100&p[url]=<?php echo $completelink; ?>&p[title]=<?php echo($atMentorsGuild." ".$compTitle);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
									<!-- <img src="<?php echo(SITE_URL)?>yogesh_new1/images/fb_icon.png" alt="Facebook Share"> -->
									<i class="fa fa-facebook" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="javascript:void(0);" onclick="javascript:window.open('https://plus.google.com/share?url=<?php echo($completelink);?>&title=<?php echo($atMentorsGuild. $questionatguild .$compTitle);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
									<!-- <img src="<?php echo(SITE_URL)?>yogesh_new1/images/g_plus_icon.png" alt="Google+ Share"> -->
									<i class="fa fa-google-plus" aria-hidden="true"></i>
								</a>
							</li>
						</ul>
					</div>
					<hr>
					<p><?php echo nl2br($question['QnaQuestion']['question_context']); ?></p>
					<div class="btm-info">
						
						<div class="categorie-info pull-left">
							
							<ul>
								<?php
									foreach ($question['QnaQuestionCategory'] as $catValue) {
								$str = ($categories[$catValue['topic_id']]);
								if(false !== stripos(($categories[$catValue['topic_id']]),"&"))
								$str = str_replace("&","_and",($categories[$catValue['topic_id']]));
								if(false !== stripos(($categories[$catValue['topic_id']]),"/"))
								$str = str_replace("/","_slash",($categories[$catValue['topic_id']]));?>
								
								<li><?php echo($this->Html->link($categories[$catValue['topic_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false, 'title'=>$categories[$catValue['topic_id']])));?></li>
								
								<?php }?>
							</ul>
						</div>
						<div class="pull-right ans-btn" id="addYourAnswer" style="display:none;">
							
							<p>Is this your area of expertise?</p> <a href="#" id="href_addans" class="btn btn-primary">Add your insight</a>
						</div>
						<div class="pull-right ans-btn" id="addQuestion" style="display:none;">
							
							<p>Not quite what you were looking for?</p> <a href="#" id="href_addques" class="btn btn-primary">Add a question</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				
				<div class="title-box">
					<?php if(isset($answers) && $answers != '')
						
					$totalAnswers = count($answers);
					?>
					
					<h1><?php if($totalAnswers == 1){?>
					Expert Insight
					<?php }else{
					echo($totalAnswers);?> Expert Insights
					<?php }?></h1>
				</div>
				<!-- /heading -->
				<?php foreach($answers as $key => $answer){?>
				<div class="section-ans box-wrap" id="a<?php echo($answer['QnaAnswer']['id']);?>">
					
					<?php if(isset($answer['QnaAnswer']['user_image']) && $answer['QnaAnswer']['user_image'] !=''){
						$image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$answer['QnaAnswer']['member_id'].DS.$answer['QnaAnswer']['user_image'];
					} else {
						$image_path = SITE_URL.'img/media/profile.png';
					}?>
					<div class="user-img">
						<a href="<?php echo(SITE_URL.strtolower($answer['QnaAnswer']['url_key']));?>" style="border: 0px;">
							<img src="<?php echo($image_path);?>" alt="<?php echo($answer['QnaAnswer']['first_name']." ".$answer['QnaAnswer']['last_name']);?>" />
						</a>
					</div>
					<div class="user-info">
						
						<h2 class="title-medium">
						<a href="<?php echo(SITE_URL.strtolower($answer['QnaAnswer']['url_key']));?>" rel="author" title="<?php echo($answer['QnaAnswer']['first_name']." ".$answer['QnaAnswer']['last_name']);?>"><?php echo($answer['QnaAnswer']['first_name']." ".$answer['QnaAnswer']['last_name']);?></a>
						</h2>
						<p>
							<?php  echo(ucfirst($answer['QnaAnswer']['linkedin_headline']));?>
							
						</p>
						<div class="social more-icon pull-right">
							<!-- Contact modal -->
								<?php
								$login = "o_4urqen5mkq";
								$appkey = "R_7aa87ace460abde4cfc31fa31b2e59ef";
								$completelink = SITE_URL."qna/question/".($question['QnaQuestion']['url_key']);
								$answerid = $answer['QnaAnswer']['id'];
								$bitlyUrl =  $this->General->get_bitly_url($completelink, $answerid,$login, $appkey);
                                ?>							
							
						              	  <div class="form-control editor" name="comment" form="usrform" placeholder="Type your message here..." id="anemaildiv<?php echo($answer['QnaAnswer']['id']);?>" style="display:none;">
										    </br>
										  
										     <b><?php echo ($loggedinuser['UserReference']['first_name']." ".$loggedinuser['UserReference']['last_name']);?> has shared an answer on GUILD with you.</b>
                                                                                     <br/></br>
											 The question on GUILD @guildalerts: <b><?php echo($question['QnaQuestion']['question_text']); ?> </b>answered by <b><?php echo($answer['QnaAnswer']['first_name']." ".$answer['QnaAnswer']['last_name']);?></b>
                                                                                         <br/></br>
											 To see the answer, visit: <?php echo($this->Html->link("Answer Link",$bitlyUrl,array('escape'=>false,'class'=>'btn que-link','title'=>'Answer Link')));?>.
                                                                                      
                                                                                       
										  </div>
										  

							<ul>
								<li class="share-btn"><a class="social-button" href="javascript:void(0);"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/share_dark.svg" alt="share" /></a></li>
								<?php if($this->Session->read('Auth.User.id')!=''){?>
								<li class="mail-btn"><a href="#" data-toggle="modal"  id="answersharelink<?php echo($answer['QnaAnswer']['id']);?>" onclick="ansshare(<?php echo($answer['QnaAnswer']['id']) ?>);"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mail_dark.svg" alt="mail" /></a></li>
								<?php }else{?>
								<li class="mail-btn"><a href="javascript:void(0);" data-toggle="modal" data-target="#signin-Modal"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/mail_dark.svg" alt="mail" /></a></li>	
								<?php }?>
								
							</ul>
							<ul class="list-unstyled inactive hide">
								<?php
								$login = "o_4urqen5mkq";
								$appkey = "R_7aa87ace460abde4cfc31fa31b2e59ef";
								$completelink = SITE_URL."qna/question/".($question['QnaQuestion']['url_key']);
								$answerid = $answer['QnaAnswer']['id'];
								$bitlyUrl =  $this->General->get_bitly_url($completelink, $answerid,$login, $appkey);
								
								$linkdinUrl="https://www.linkedin.com/shareArticle?mini=true&url=".$completelink."&title=".$compTitle;
								if($answer['QnaAnswer']['social'] ==''){
								$tweet="https://twitter.com/intent/tweet?text=".$answer['QnaAnswer']['first_name']." ". $answer['QnaAnswer']['last_name'] ."&#58; ".$compTitle." " .$bitlyUrl;
																} else{
								$tweet="https://twitter.com/intent/tweet?text=".$atsybmol.$answer['QnaAnswer']['social']." ".$compTitle." " .$bitlyUrl;
								}
								?>
								
								<li>
									<a href="javascript:void(0);" onclick="javascript:window.open('<?php echo ($tweet);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')" ">
										<!-- <img src="<?php echo(SITE_URL)?>yogesh_new1/images/twit_icon.png" alt="Twitter Share" data-pin-nopin="true"> -->
										<i class="fa fa-twitter" aria-hidden="true"></i>
									</a>
								</li>
								<li>
									<a href="javascript:void(0);" onclick="javascript:window.open('<?php echo ($linkdinUrl);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
										<!-- <img src="<?php echo(SITE_URL)?>yogesh_new1/images/logo_linkedin.png" alt="LinkedIn Share"> -->
										<i class="fa fa-linkedin" aria-hidden="true"></i>
									</a>
								</li>
								<li>
								    <a href="javascript:void(0);" onclick="javascript:window.open('https://www.facebook.com/sharer.php?s=100&p[url]=<?php echo $completelink; ?>&p[title]=<?php echo($atMentorsGuild." ".$compTitle);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
										<!-- <img src="<?php echo(SITE_URL)?>yogesh_new1/images/fb_icon.png" alt="Facebook Share"> -->
										<i class="fa fa-facebook" aria-hidden="true"></i>
									</a>
								</li>
								<li>
									<a href="javascript:void(0);" onclick="javascript:window.open('https://plus.google.com/share?url=<?php echo($completelink);?>&title=<?php echo($atMentorsGuild. $answer['QnaAnswer']['first_name']." ".$answer['QnaAnswer']['last_name'] .$compTitle);?>','mywindowtitle','width=600,height=600,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')">
										<!-- <img src="<?php echo(SITE_URL)?>yogesh_new1/images/g_plus_icon.png" alt="Google+ Share"> -->
										<i class="fa fa-google-plus" aria-hidden="true"></i>
									</a>
								</li>
							</ul>
							
						</div>
						<?php $totalinsight = $answer['QnaAnswer']['ans_count'] + $answer['QnaAnswer']['insight_count'];
						$disp=SITE_URL.strtolower($answer['QnaAnswer']['url_key'])."#insights";
						if($totalinsight >1)
							echo($this->Html->link($totalinsight." insights",$disp,array('escape'=>false)));
						else
						echo($this->Html->link($totalinsight." insight",$disp,array('escape'=>false)));?>
					</div>
					<hr>
					<div class="description">
						<p>
							<?php if($this->Session->read('Auth.User.role_id') != 2){
							$user = ClassRegistry::init('User');
							$mentorlink = $user->find('first', array('conditions' => array('User.id' => $answer['QnaAnswer']['member_id'])));
							$disp = SITE_URL.'fronts/consultation_request/'.$mentorlink['User']['url_key'];
							$fname =($answer['QnaAnswer']['first_name']);
							$fanswer = str_replace('  ', '&nbsp;&nbsp;', $answer['QnaAnswer']['answer_text']);
							$fanswer = trim($fanswer);
							$editthis = '<div class="ql-editor" contenteditable="true"';
							$editby= '<div class="ql-editor" contenteditable="false"';
									$fanswer = str_replace($editthis, $editby, $fanswer);
									$editthis1 = 'target="_blank" style="color: rgba(0, 0, 0, 0.8);"';
									$editby1 = 'target="_blank" style="color: #D03135;"';
									$fanswer = str_replace($editthis1, $editby1, $fanswer);
									echo $this->General->make_links(nl2br(htmlspecialchars_decode($fanswer)));
									} else{
									$disp = $disp=SITE_URL.strtolower($answer['QnaAnswer']['url_key'])."#insights";
									$fname =($answer['QnaAnswer']['first_name']);
									$fanswer = str_replace('  ', '&nbsp;&nbsp;', $answer['QnaAnswer']['answer_text']);
									$fanswer = trim($fanswer);
									
									$editthis = '<div class="ql-editor" contenteditable="true"';
											$editby= '<div class="ql-editor" contenteditable="false"';
													$fanswer = str_replace($editthis, $editby, $fanswer);
													$editthis1 = 'target="_blank" style="color: rgba(0, 0, 0, 0.8);"';
													$editby1 = 'target="_blank" style="color: #D03135;"';
													$fanswer = str_replace($editthis1, $editby1, $fanswer);
													echo $this->General->make_links(nl2br(htmlspecialchars_decode($fanswer)));
													
									}?>
						</p>
						<div class="btm-info">
							<div class="ans-btn pull-left">
								
								
								<a href="javascript:void(0);" id="button<?php echo($answer['QnaAnswer']['id']) ?>" onclick="upVote(<?php echo($answer['QnaAnswer']['id']) ?>);" class="btn btn-primary">UPVOTE | <?php echo $answer['QnaAnswer']['upvotes'];?></a>
							</div>
							<div class="review">
								<strong><span>Is this insight helpful?</span> <a href="<?php echo($disp);?>">Schedule a free consultation with <?php echo($fname);?>.</a></strong>
							</div>
						</div>
					</div>
				</div>
				<!-- /box -->
				<?php }?>


				<div class="section-ans box-wrap" id="preview-wrap" style="display: none;">					
					  	<?php if(isset($userData['UserImage'][0]['image_name']) && $userData['UserImage'][0]['image_name'] !=''){
				            			$image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$userData['User']['id'].DS.$userData['UserImage'][0]['image_name'];
				               } else {
				            			$image_path = SITE_URL.'img/media/profile.png';
				        }?>
					<div class="user-img" id="a<?php echo($answer['QnaAnswer']['id']);?>">
						<a href="<?php echo(SITE_URL.strtolower($userData['User']['url_key']));?>" style="border: 0px;">
							<img src="<?php echo($image_path);?>" alt="<?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?>" title="<?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?>" data-pin-nopin="true">
						</a>
					</div>
					<div class="user-info">						
						<h2 class="title-medium">
							<a href="<?php echo(SITE_URL.strtolower($userData['User']['url_key']));?>" rel="author" title="<?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?>"><?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?></a>
						</h2>
						<p>
							<?php  echo(ucfirst($userData['UserReference']['linkedin_headline']));?>
							
						</p>
						
										 
						<?php $totalinsight = $answer['QnaAnswer']['ans_count'] + $answer['QnaAnswer']['insight_count'];
						$disp=SITE_URL.strtolower($answer['QnaAnswer']['url_key'])."#insights";
						if($totalinsight >1)
							echo($this->Html->link($totalinsight." insights",$disp,array('escape'=>false)));
						else
						echo($this->Html->link($totalinsight." insight",$disp,array('escape'=>false)));?>
					</div>
					<hr>
					<div class="description">
						<p id="answerPara">
						</p>
						<div class="box-btm ans-btn">
							<a href="javascript:void(0);" id="editAnswer" class="btn-cancle">Edit</a>										
							<a href="javascript:void(0);" class="btn btn-primary" id="submitButton">Submit</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


					<!-- Comment Section begin -->
					<div class="container">
						<div class="section-ans box-wrap box-editor" id="addanswer" style="display: none;">
							<div class="user-img">
								<?php if(isset($userData['UserImage'][0]['image_name']) && $userData['UserImage'][0]['image_name'] !=''){
									$image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$userData['User']['id'].DS.$userData['UserImage'][0]['image_name'];
								} else {
									$image_path = SITE_URL.'img/media/profile.png';
								}?>
								<a href="<?php echo(SITE_URL.strtolower($userData['User']['url_key']));?>">
									<img src="<?php echo($image_path);?>" alt="<?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?>" title="<?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?>"/>
								</a>
							</div>
							<form action="<?php echo SITE_URL ?>qna/add_answer" method="post" id="AddAnswerform">
								<div class="user-info">
									<h2 class="title-medium"><a href="<?php echo(SITE_URL.strtolower($userData['User']['url_key']));?>" title="<?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?>"><?php echo($userData['UserReference']['first_name']." ".$userData['UserReference']['last_name']);?></a></h2>
						<?php $totalinsight = $answer['QnaAnswer']['ans_count'] + $answer['QnaAnswer']['insight_count'];
						$disp=SITE_URL.strtolower($answer['QnaAnswer']['url_key'])."#insights";
						if($totalinsight >1)
							echo($this->Html->link($totalinsight." insights",$disp,array('escape'=>false)));
						else
						echo($this->Html->link($totalinsight." insight",$disp,array('escape'=>false)));?>
									<p>(<span id="noOfChar">2000</span> Characters Remaining)</p>
									<input type="hidden" name="data[QnaAnswer][id]" value="<?php echo($question['QnaQuestion']['id']);?>"/>
									<input type="hidden" name="data[QnaAnswer][answer_text]"  id="answeredtext"/>
								</div>
								<div class="description">
									<div class="nopadding">
										<!-- Create the toolbar container -->
										<!-- <div id="toolbar">
													<button class="ql-bold">Bold</button>
													<button class="ql-italic">Italic</button>
													<button class="ql-underline">Italic</button>
													<button class="ql-link">Italic</button>
													<button class="ql-list">Italic</button>
													<button class="ql-list">Italic</button>
													<button class="ql-clean">Italic</button>
										</div> -->
										<!-- Create the editor container -->
										<div id="editor" style="height: 200px">
										</div>
									</div>
									<div class="box-btm ans-btn">
										<?php  echo($this->Html->link("Cancel",SITE_URL.'users/my_account#insights',array('escape'=>false)));?>
										<a href="javascript:void(0);" class="btn btn-primary" id="answerpreviewButton">Preview</a>
									</div>
								</div>
							</form>
						</div>
						<div class="row">
							<div class="box-editor-btm">
								<div class="col-md-12">
									<h3>Similar questions</h3>
								</div>
								<?php foreach ($questionArray as $key => $qaValue) {?>
								<div class="col-md-12">
									<div class="box">
										<p>
											<a href="<?php echo(SITE_URL.'qna/question/'.$qaValue['QnaQuestion']['url_key'])?>"><?php echo($qaValue['QnaQuestion']['question_text'])?> </a>
											<?php echo($this->Html->link("Read more",SITE_URL.'qna/question/'.$qaValue['QnaQuestion']['url_key'],array('escape'=>false,'class'=>'btn','title'=>'View insights')));?>
										</p>
										<ul>
											<?php foreach ($qaValue['QnaQuestionCategory'] as $catValue) {
											$str = ($categories[$catValue['topic_id']]);
											if(false !== stripos(($categories[$catValue['topic_id']]),"&"))
											$str = str_replace("&","_and",($categories[$catValue['topic_id']]));
											if(false !== stripos(($categories[$catValue['topic_id']]),"/"))
											$str = str_replace("/","_slash",($categories[$catValue['topic_id']]));?>
											<li><?php echo($this->Html->link($categories[$catValue['topic_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false, 'title'=>$categories[$catValue['topic_id']])));?></li>
											<?php }?>
										</ul>
										<div class="box-btm">
											<?php $tCount = 0;?>
											<?php foreach ($qaValue['User'] as $uValue) {
											if($tCount<3){
											$tCount++;
													if(isset($uValue['user_image'])){
											$image_path = MENTORS_IMAGE_PATH.DS.$uValue['member_id'].DS.$uValue['user_image'];?>
											<a href="<?php echo(SITE_URL.strtolower($uValue['url_key']));?>">
												<img src="<?php echo(SITE_URL.'img/'.$image_path)?>"alt="<?php echo($uValue['first_name']." ".$uValue['last_name']);?>" title="<?php echo($uValue['first_name']." ".$uValue['last_name']);?>"/>
											</a>
											<?php } else {
											$image_path = 'profile.png';?>
											<a href="<?php echo(SITE_URL.strtolower($uValue['url_key']));?>">
												<img src="<?php echo(SITE_URL.'img/media/'.$image_path)?>" alt="<?php echo($uValue['first_name']." ".$uValue['last_name']);?>" title="<?php echo($uValue['first_name']." ".$uValue['last_name']);?>"/>
											</a>
											<?php }}}?>
											
										</div>
									</div>
								</div>
								<?php }?>
							</div>
							<div class="box-editor-btm ask_expert" id="askQuestion" style="display:none;">
								<div class="col-md-12">
									<h3>Ask an expert <sub>Free & anonymous advice for business leaders</sub></h3>
								</div>
								<div class="col-md-12">
									<form id="QuestionSubmitform" enctype="multipart/form-data" method="post" action="<?php echo SITE_URL ?>qna/question_preview">
										<div class="box askanexpert">
											<label>Your question <span> (<span id="noOfChar1">128</span> characters remaining)</span></label>
											<textarea id="questionTextBox" placeholder="Enter question e.g. How do I drive employee engagement?" name="data[QnaQuestion][question_text]"></textarea>
											<div id="questiondetail-box" class="inactive hide">
												<label>Provide details </label>
												<textarea id="questiondetailtextbox" placeholder="To get a useful response provide all relevant background, but avoid any personally identifiable information." name="data[QnaQuestion][question_context]"></textarea>
											</div>
											<a href="javascript:void(0);" id="asknowbutton" class="btn btn-primary">Ask now</a>
											<div id="previewbutton_area" style="display:none;">
												<a href="" id="cancelLink">Cancel</a>
												<a href="javascript:void(0);" id="previewbutton" class="btn btn-primary">Preview</a>
											</div>
										</div>
										<div>
											
										</div>
										
									</form>
								</div>
							</div>
						</div>
					</div>
					<!-- Comment Section close -->
					<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
					<!-- Include all compiled plugins (below), or include individual files as needed -->
					<script src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
					<!-- <script src="<?php echo(SITE_URL)?>yogesh_new1/js/editor.js"></script> -->
					<!-- Google Re-captcha Code -->
					<script src='https://www.google.com/recaptcha/api.js'></script>
					<!-- Include the Quill library -->
					<script src="https://cdn.quilljs.com/1.0.0/quill.js"></script>
					<!-- Include yogesh JS code -->
					<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
					<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
					<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
					<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
					<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
					<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>

						<script type="text/javascript">
	                     var loggedUsername;
	                     loggedUsername = "<?php echo ($loggedinuser['UserReference']['first_name']." ".$loggedinuser['UserReference']['last_name']);?>";
                         console.log(loggedUsername);
                         </script>
					<script>
					$('.social-button').on("click", function() {
					if ($(this).parent().parent().parent().find("ul.list-unstyled").hasClass("inactive")) {
					$(this).parent().parent().parent().find("ul.list-unstyled").removeClass("inactive").addClass("active");
					$(this).parent().parent().parent().find("ul.list-unstyled").removeClass("hide");
					} else {
					$(this).parent().parent().parent().find("ul.list-unstyled").addClass("inactive").removeClass("active");
					$(this).parent().parent().parent().find("ul.list-unstyled").addClass("hide");
					}
					});
					$("#questionTextBox").on("click",function(){
						if ($(this).parent().find("#questiondetail-box").hasClass("inactive")) {
								$("#questiondetail-box").removeClass("inactive").addClass("active");
					$("#questiondetail-box").removeClass("hide");
						}
						else
						{
							$("#questiondetail-box").addClass("inactive").removeClass("active");
					$("#questiondetail-box").addClass("hide");
						}
					});
					// mail
					$(".mail-btn img").mouseover(function() {
					$(this).attr("src","<?php echo(SITE_URL)?>yogesh_new1/images/mail_red.svg")
					});
					$(".mail-btn img").mouseout(function() {
					$(this).attr("src","<?php echo(SITE_URL)?>yogesh_new1/images/mail_dark.svg")
					});
					// share
					$(".share-btn img").mouseover(function() {
					$(this).attr("src","<?php echo(SITE_URL)?>yogesh_new1/images/share_red.svg")
					});
					$(".share-btn img").mouseout(function() {
					$(this).attr("src","<?php echo(SITE_URL)?>yogesh_new1/images/share_dark.svg")
					});
					// print
					$(".print-btn img").mouseover(function() {
					$(this).attr("src","<?php echo(SITE_URL)?>yogesh_new1/images/print_red.svg")
					});
					$(".print-btn img").mouseout(function() {
					$(this).attr("src","<?php echo(SITE_URL)?>yogesh_new1/images/print_dark.svg")
					});
					</script>
					<!-- Initialize Quill editor -->
					<script>
					var toolbarOptions = [
					['bold', 'italic', 'underline', 'link'],        // toggled buttons\
					[{ 'list': 'ordered'}, { 'list': 'bullet' }],
					['clean']                                         // remove formatting button
					];
					var editor = new Quill('#editor', {
					modules: { toolbar: toolbarOptions },
					theme: 'snow',
					placeholder: 'Enter your advice here. Please avoid direct solicitation.'
					});
					var currentanswerid;
					function upVote(id){
					
					jQuery.ajax({
								url:SITE_URL+'qna/upvote/'+id,
								type:'post',
								dataType:'json',
								success:function(res){
									if(res.value == -1){
										alert("Please first login or register to vote.");
									} else
										jQuery('#button'+id).val('Upvote | ' + res.value);
								}
								});
					}
					function ansshare(id){
						currentanswerid = id;

                                                                                       
						var text = 	jQuery("#anemaildiv"+currentanswerid).html();				 
											 
						$("#lineModalLabel").html("Share an Answer");
						$("#emailsubject").val(loggedUsername + ' has shared an answer on GUILD with you');
						$("#qemaildiv").html(text);
                        var frm = document.getElementById('Sharequestionform');
						frm.action = '<?php echo SITE_URL ?>qna/share_answer';
						$("#squarespaceModalQuestion").modal();

					}
					
					function quesshare(){
						

                                                                                       
						var text = 	jQuery("#qemaildiv1").html();				 
											 
						$("#lineModalLabel").html("Share a Question");
						$("#emailsubject").val(loggedUsername + ' has shared a Question on GUILD with you');
						$("#qemaildiv").html(text);
                        var frm = document.getElementById('Sharequestionform');
						frm.action = '<?php echo SITE_URL ?>qna/share_question';
						$("#squarespaceModalQuestion").modal();
					}
					
                       function nl2br (str, is_xhtml) {
                           var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br/>' : '<br>';
                           return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
                       }					

					</script>
					<script type="text/javascript">
					jQuery(document).ready(function(){
						if(memberId != ''){
							
							var s = document.getElementById('addanswer');
							var p = document.getElementById('addYourAnswer');
							
							s.style.display="block";
							p.style.display="block";
						
						} else {
							
							var s = document.getElementById('addQuestion');
							var p = document.getElementById('askQuestion');
							s.style.display="block";
							p.style.display="block"
						}
						
					   
					jQuery("#href_addques").click(function() {
							jQuery('html, body').animate({
						scrollTop: jQuery("#askQuestion").offset().top -(100)
						}, 1000);
						return false;
						});
					jQuery("#href_addans").click(function() {
							jQuery('html, body').animate({
						scrollTop: jQuery("#addanswer").offset().top -(100)
						}, 1000);
						return false;
						});
						function updateCount ()
					{
					var qText = jQuery("#editor").text().length;
					
					if(qText < 2000) {
					jQuery("#noOfChar").html(2000 - qText);
					} else {
					jQuery('#noOfChar').html(0);;
					}
					}
					jQuery("#editor").keyup(function () {
					updateCount();
					});
					jQuery("#editor").keypress(function () {
					updateCount();
					});
					jQuery("#editor").keydown(function () {
					updateCount();
					});
					function updateCount1 ()
					{
					var qText = jQuery("#questionTextBox").val();
					if(qText.length < 128) {
					jQuery("#noOfChar1").html(128 - qText.length);
					} else {
					jQuery("#noOfChar1").html(0);
					jQuery("#questionTextBox").val(qText.substring(0,128));
					}
					}
					jQuery("#questionTextBox").keyup(function () {
					updateCount1();
					});
					jQuery("#questionTextBox").keypress(function () {
					updateCount1();
					});
					jQuery("#questionTextBox").keydown(function () {
					updateCount1();
					});
					jQuery("#previewbutton").click(function(){
						var flag = 0;
						if(jQuery("#questionTextBox").val() == '') {
					
								jQuery('#questionTextBox').css('border-color','#F00');
								jQuery('#questionTextBox').focus();
								flag++;
							} else {
								jQuery('#questionTextBox').css('border-color','');
							}
						var context = jQuery("#questionContextBox").val();
						if(context == '') {
								jQuery('#questionContextBox').css('border-color','#F00');
								jQuery('#questionContextBox').focus();
								flag++;
							} else {
								jQuery('#questionContextBox').css('border-color','');
							}
							if(flag > 0) {
								return false;
							} else {
					
								jQuery("#QuestionSubmitform").submit();
								return true;
							}
							
					});
					jQuery("#questionTextBox").click(function(){
					jQuery("#previewbutton_area").show();
					jQuery("#asknowbutton").hide();
					
					});
					});
					</script>
					<script type="text/javascript">
					jQuery(document).ready(function(){
						setTimeout(	function focusQ(){
							var focus = jQuery('#focusJS').val();
							if(focus != null){
								document.getElementById(focus).scrollIntoView();
								jQuery('#' + focus).css({"margin-top":"110px"});
							}
						}, 600);

						jQuery("#answerpreviewButton").click(function(){
	                        var answerEntered = jQuery("#editor").html();

                            var result = answerEntered.split('<div class="ql-clipboard"');
                            answerEntered = result[0];

	                        answerEntered = answerEntered.replace(/  /gi, '&nbsp;&nbsp;');

	                        chars = jQuery("#editor").text().length;

	                        if(answerEntered == '' || chars > 2000){
		                    jQuery('#editor').css('border-color','#F00');
		                    jQuery('#editor').css('border-width','<?php echo(ERROR_BORDER);?>px');
                            jQuery('#editor').focus();
                            return false;
	                        } else {
		                    jQuery('#editor').css('border-color',''); 
                            jQuery('#editor').css('border-width','<?php echo(NORMAL_BORDER);?>px');  
	                        }



	                        jQuery("#answerPara").html(nl2br(answerEntered));
	                        jQuery("#addanswer").hide();
                                jQuery("#answeredtext").val(answerEntered);

	                        jQuery("#preview-wrap").show();
                                jQuery('#preview-wrap').scrollIntoView();
		                jQuery('#preview-wrap').css({"margin-top":"290px"});			
                     
						});
					
                       jQuery("#editAnswer").click(function(){

	                       jQuery("#addanswer").show();
	                       jQuery("#preview-wrap").hide();
                       });

                       jQuery("#submitButton").click(function(){

	                       jQuery("#AddAnswerform").submit();
	                       jQuery('#submitButton').attr("disabled", true);
                       });

                       jQuery("#sharequestionbutton").click(function(){
						   var qemailtext = jQuery("#qemaildiv").html();
						   jQuery("#qemailcontent").val(qemailtext);
						   var qemailcontent = jQuery("#qemailcontent").val();
						   var frm = document.getElementById('Sharequestionform');

						var flag = 0;
						if(jQuery("#firstname").val() == '') {
								jQuery('#firstname').css('border-color','#F00');
								jQuery('#firstname').focus();
								flag++;
							} else {
								jQuery('#fname').css('border-color','');
							}
						if(jQuery("#lastname").val() == '') {
					
								jQuery('#lastname').css('border-color','#F00');
								jQuery('#lastname').focus();
								flag++;
							} else {
								jQuery('#lastname').css('border-color','');
							}
						if(jQuery("#emailaddress").val() == '') {
					
								jQuery('#emailaddress').css('border-color','#F00');
								jQuery('#emailaddress').focus();
								flag++;
							} else {
								jQuery('#emailaddress').css('border-color','');
							}
						if(jQuery("#emailsubject").val() == '') {
					
								jQuery('#emailsubject').css('border-color','#F00');
								jQuery('#emailsubject').focus();
								flag++;
							} else {
								jQuery('#emailsubject').css('border-color','');
							}
						if(jQuery("#qemaildiv").html() == '') {
					
								jQuery('#qemaildiv').css('border-color','#F00');
								jQuery('#qemaildiv').focus();
								flag++;
							} else {
								jQuery('#qemaildiv').css('border-color','');
							}

                                                if (grecaptcha.getResponse() == ""){
                                                        var e = document.getElementById('captchaerror');
							e.style.display="block";
						        flag++;
                                                }

							if(flag > 0) {
								return false;
							} else {

	                                                jQuery("#Sharequestionform").submit();
                                                        return true;
                                                        }   
                       });

  
				   
					   
                       function nl2br (str, is_xhtml) {
                           var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br/>' : '<br>';
                           return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
                       }

					   							
					});
					</script>
