

<!-- banner section -->
<div id="banner-section" class="lenses-page s-banner case-banner" data-parallax="scroll" data-image-src="<?php echo(SITE_URL)?>yogesh_new1/images/ask_banner.png">
    <div class="col-md-12 pad0 text-center banner-content">
        <img src="<?php echo(SITE_URL)?>yogesh_new1/images/ask_icon.png" alt="" class="s-icon">
       <?php  if($this->Session->read('Auth.User.role_id')!=2) {?>
        <h1>Ask an expert</h1>
       <?php } else{?>
        <h1>Share your expertise</h1>
       <?php }?> 
        <p>Free & anonymous advice for business leaders</p>
        <button  class="banner-getstarted btn btn-default get-started" id="viewquestion">
        Read recent insights
        </button >
    </div>
</div>
<div class="container-fluid ask-expertPage" id="apply-section">
    <div class="container create-section">
        <div class="row">
            <div class="col-md-12">
                <?php  if($this->Session->read('Auth.User.role_id')!=2) {?>
                <div class="apply-heading">
                    <h5>Ask an expert</h5>
                    <label class="free-c"> (Free & anonymous advice for business leaders)</label>
                </div>
                <div class="apply-form">
                    <form id="Questionform" method="post" action="<?php echo SITE_URL ?>qna/question_preview">
                        <div class="form-group">
                            <label>Your question on "<?php echo($queryString);?>"</label>
                            <span class="pull-right char-limit">(<span id="noOfChar">128</span> characters remaining)</span>
                            <input type="text" class="form-control" placeholder="e.g. How do i drive employee engagement?" id="questionTitlebox" name="data[QnaQuestion][question_text]">
                        </div>
                        <button class="banner-getstarted btn btn-default get-started" id="askNowbutton">
                        Ask now
                        </button>
                        <div style="display: none;" id="questionContextDiv">
                            <div class="form-group">
                                <label>Provide details</label>
                                <textarea class="form-control" id="questionContextBox" name="data[QnaQuestion][question_context]" rows="8" placeholder="To get a useful response provide all relevant background, but avoid any personally identifiable information."></textarea>
                            </div>
                            <div class="e-button pull-right">
                                <label>
                                    <a href="#" id="cancelLink">
                                        Cancel
                                    </a>
                                </label>
                                <button class="banner-getstarted btn btn-default get-started" id="questionSubmit">
                                Preview
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <?php }else{?>
                <div class="apply-heading">
                    <h5>Share your expertise</h5>
                    <label class="free-c"> (Advice for business leaders)</label>
                </div>
                <div class="apply-form">
                    <form id="insightForm" method="post" action="<?php echo SITE_URL ?>insight/edit">
                        <div class="form-group">
                            <input type="text" id="insighturlbox" class="form-control" placeholder="Paste a URL..." name="data[Insight][title]">
                        </div>
                        <div class="e-button pull-right">
                            <label><a href="#" id="cancelLink">Cancel</a></label>
                            <button class="banner-getstarted btn btn-default get-started" id="insightSubmitbutton">Preview</button>
                        </div>
                    </form>
                </div>
                <?php }?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 tag-container">
                <div class="all-tags inactive" data-menu="all-tags">
                    <label>
                        All topics
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </label>
                </div>
                <div class="all-tags" id="popularTags">
                    <div class="tag-lists">
                        <span>Popular Topics : </span>
                        <ul class="list-unstyled">
                            <li>
                                <a href="<?php echo(SITE_URL)?>qna/category/Culture">
                                    Culture
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo(SITE_URL)?>qna/category/Performance-management">
                                    Performance management
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo(SITE_URL)?>qna/category/Leadership">
                                    Leadership
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo(SITE_URL)?>qna/category/Communication">
                                    Communication
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row t-listwrapper all-tags-section">
                    <div class="col-md-12">
                        <ul class="list-unstyled">
                            <?php sort($topiclist, SORT_NATURAL | SORT_FLAG_CASE);
                            foreach ($topiclist as $key=>$catValue) {
                            $str = ($catValue);
                            if(false !== stripos(($catValue),"&"))
                            $str = str_replace("&","_and",($catValue));
                            if(false !== stripos(($catValue),"/"))
                            $str = str_replace("/","_slash",($catValue));
                            $str = str_replace(" ","-",$str);?>
                            <li><a href="<?php echo (SITE_URL.'qna/category/'.$str);?>"><?php echo($catValue);?></a></li>
                            
                            <?php }?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="heading-acs" id="interested-section">
                    <span><?php echo($queryString);?></span>
                </div>
            </div>
        </div>
        <div class="row">            
            <div class="col-md-12">
                <div class="expert-list">
                    <div class="search-results qa-listing">
                        <?php foreach ($questionArray as $key => $qaValue) {?>
                            <div id="withoutimg-l" class="box1">
                                <div class="box-head">
                                    <div class="content-right-text">
                                        <h3><?php echo($qaValue['QnaQuestion']['question_text'])?> <?php echo($qaValue['QnaAnswer']['created'])?> <?php echo($this->Html ->link("Read more",SITE_URL.'qna/question/'.$qaValue['QnaQuestion']['url_key'],array('escape'=>false,'title'=>'View answers')));?></h3>
                                        <div class="expertise-header">
                                            <ul class="tag-list">
                                                <?php foreach ($qaValue['QnaQuestionCategory'] as $catValue) {
                                                $str = ($categories[$catValue['topic_id']]);
                                                if(false !== stripos(($categories[$catValue['topic_id']]),"&"))
                                                $str = str_replace("&","_and",($categories[$catValue['topic_id']]));
                                                if(false !== stripos(($categories[$catValue['topic_id']]),"/"))
                                                $str = str_replace("/","_slash",($categories[$catValue['topic_id']]));
                                                $str = str_replace(" ","-",$str);?>
                                                <li><?php echo($this->Html ->link($categories[$catValue['topic_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false,'title'=>$categories[$catValue['topic_id']]))); ?></li>
                                                <?php }?>
                                            </ul>
                                        </div>
                                        <div class="withimg-data">
                                            <?php $tCount = 0; foreach ($qaValue['User'] as $uValue) { if($tCount<3){ $tCount++; if(isset($uValue['user_image'])){ $image_path = MENTORS_IMAGE_PATH.DS.$uValue['member_id'].DS.$uValue['user_image'];?>
                                                <div class="content-left-img user-100">
                                                    <a href="<?php echo(SITE_URL.strtolower($uValue['url_key']));?>">
                                                        <img src="<?php echo(SITE_URL.'img/'.$image_path)?>"  alt="<?php echo($uValue['first_name']." ".$uValue['last_name']);?>" title="<?php echo($uValue['first_name']." ".$uValue['last_name']);?>"/>
                                                    </a>
                                                </div>
                                            <?php } else {
                                            $image_path = 'profile.png';?>
                                                <div class="content-left-img user-100">
                                                    <a href="<?php echo(SITE_URL.strtolower($uValue['url_key']));?>">
                                                        <img src="<?php echo(SITE_URL.'img/media/'.$image_path)?>" alt="<?php echo($uValue['first_name']." ".$uValue['last_name']);?>" title="<?php echo($uValue['first_name']." ".$uValue['last_name']);?>"/>
                                                    </a>
                                                </div>
                                            <?php }}}?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }?>

                        <?php foreach ($insights as $key => $insight) { ?>
                            <div class="box1" id="withimg-l">
                                <?php if (strpos($insight['Insight']['post_url'], "www")!=false){
                                    $pos = strpos($insight['Insight']['post_url'], '.');                    
                                    $pos1 = strpos($insight['Insight']['post_url'], '/');
                                    $pos2 = strpos($insight['Insight']['post_url'],'/', $pos1 + strlen('/'));
                                    $pos3 = strpos($insight['Insight']['post_url'],'/', $pos2 + strlen('/'));
                                    $final =$pos3 - $pos;
                                    $url = substr($insight['Insight']['post_url'],$pos+1,$final-1);
                                } else {
                                    $pos1 = strpos($insight['Insight']['post_url'], '/');
                                    $pos2 = strpos($insight['Insight']['post_url'],'/', $pos1 + strlen('/'));
                                    $pos3 = strpos($insight['Insight']['post_url'],'/', $pos2 + strlen('/'));
                                    $final =$pos3 - $pos2;
                                    $url = substr($insight['Insight']['post_url'],$pos2+1,$final-1);
                                } ?>
                                <?php if ($url == ''){
                                    $url = "guild.im";
                                }
                                if($insight['Insight']['feed_favicon'] == ''){
                                    $favicon = "https://test.guild.im/imgs/mg_monogram.png";
                                }
                                else{
                                    $favicon = $insight['Insight']['feed_favicon'];
                                }?>
                                <div class="box-head">
                                    <div class="fixed-head">
                                        <div class="content-left-img">
                                            <?php if(isset($insight['User']['user_image'])){ $image_path = MENTORS_IMAGE_PATH.DS.$insight['Insight']['user_id'].DS.$insight['User']['user_image'];?>
                                            <a href="<?php echo(SITE_URL.strtolower($insight['User']['url_key']));?>">
                                                <img src="<?php echo(SITE_URL.'img/'.$image_path)?>"  alt="<?php echo($insight['User']['first_name']." ".$insight['User']['last_name']);?>" title="<?php echo($insight['User']['first_name']." ".$insight['User']['last_name']);?>"/>
                                            </a>
                                            <?php } else { $image_path = 'profile.png';?>
                                            <a href="<?php echo(SITE_URL.strtolower($insight['User']['url_key']));?>">
                                                <img src="<?php echo(SITE_URL.'img/media/'.$image_path)?>"  alt="<?php echo($insight['User']['first_name']." ".$insight['User']['last_name']);?>" title="<?php echo($insight['User']['first_name']." ".$insight['User']['last_name']);?>"/>
                                            </a>
                                            <?php }?>
                                            
                                            <!-- <button class="banner-getstarted btn btn-default get-started upvote-button" id="upvote-btn" onclick="upVote(<?php echo($insight['Insight']['id']) ?>);">
                                            Upvote | <?php echo($insight['Insight']['upvotes']);?>
                                            </button> -->
                                        </div>
                                    </div>
                                    <div class="content-right-text">
                                        <h3><?php echo($insight['Insight']['title'])?> <?php  echo($this->Html ->link("Read more",SITE_URL.'insight/insight/'.$insight['Insight']['url_key'],array('escape'=>false,'class'=>'readMore','title'=>'View insight')));?></h3>
                                        <p class="user-name">
                                            <span><?php echo($this->Html ->link($insight['User']['first_name']." ".$insight['User']['last_name'],SITE_URL.$insight['User']['url_key']));?></span>
                                        </p>
                                        <!-- <h1><p><?php echo($insight['User']['city'].', '.$insight['User']['state']);?> | <small class="user-headline"><?php echo($insight['User']['headline']);?></small></p>
                                        </h1> -->
                                        <div class="mm-data">
                                            <?php
                                            $date = new DateTime($insight['Insight']['created']);
                                            $result = $date->format('Y-m-d');
                                            
                                            $path = explode("-", $result); // splitting the path
                                            $month = $path[1]; // get the value of the last element
                                            $monthName = date("F", mktime(0, 0, 0, $month, 10));
                                            ?>
                                            <ul class="list-unstyled">
                                                <li>
                                                    <label class="smallIcon">
                                                        <img src="<?php echo($favicon)?>">
                                                    </label>
                                                    <?php echo($url);?>
                                                </li>
                                                <li>
                                                    |
                                                </li>
                                                <li>
                                                    <?php echo($monthName);?> <?php echo($path[2]);?>, <?php echo($path[0]);?>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="text-content-box"></div>
                                        <div class="expertise-header">
                                            <ul class="tag-list">
                                                <?php foreach ($insight['Insight_category'] as $catValue) {
                                                $str = ($categories[$catValue['topic_id']]);
                                                if(false !== stripos(($categories[$catValue['topic_id']]),"&"))
                                                $str = str_replace("&","_and",($categories[$catValue['topic_id']]));
                                                if(false !== stripos(($categories[$catValue['topic_id']]),"/"))
                                                $str = str_replace("/","_slash",($categories[$catValue['topic_id']]));
                                                $str = str_replace(" ","-",$str);?>
                                                <li>
                                                    <?php echo($this->Html ->link($categories[$catValue['topic_id']],SITE_URL.'qna/category/'.$str,array('escape'=>false,'title'=>$categories[$catValue['topic_id']])));?>
                                                </li>
                                                <?php }?>
                                            </ul>
                                        </div>                                        

                                    </div>
                                </div>
                            </div>
                        <?php  }?>
                    </div>
                </div>
            </div>            
        </div>        
    </div>
</div>
<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>-->
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>-->

<script type="text/javascript">
$('.t-listwrapper').addClass('hide');
$('.all-tags').on("click", function(e) {
//e.preventDefault();
var menu = $(this).attr('data-menu');
$('.' + menu + '-section').toggleClass('hide');
});
</script>
<script type="text/javascript">
$(document).ready(function(){
$("#questionSubmit").click(function(){

var flag = 0;
if($.trim($("#questionTitlebox").val()) == '') {

$('#questionTitlebox').css('border-color','#F00');
$('#questionTitlebox').focus();
flag++;
} else {

$('#questionTitlebox').css('border-color','');
}
if($.trim($("#questionContextBox").val()) == '') {

$('#questionContextBox').css('border-color','#F00');
$('#questionContextBox').focus();
flag++;
} else {

$('#questionContextBox').css('border-color','');
}
if(flag == 0) {

$("#Questionform").submit();
return true;

} else {

return false;
}

});
$("#questionTitlebox").click(function(){
$("#questionContextDiv").show();
$("#askNowbutton").hide();

});

$("#askNowbutton").click(function(){

$("#questionContextDiv").show();
$("#askNowbutton").hide();
return false;
});
$("#cancelLink").click(function(){

$("#questionContextDiv").hide();
$("#askNowbutton").show();
$('#questionContextBox').css('border-color','');
$('#questionTitlebox').css('border-color','');
return false;
});
$("#insightSubmitbutton").click(function(){
var flag = 0;
if($.trim($("#insighturlbox").val()) == '') {
$('#insighturlbox').css('border-color','#F00');
$('#insighturlbox').focus();
flag++;
} else {
$('#insighturlbox').css('border-color','');
}
if(flag > 0) {
return false;
} else {
$("#insightForm").submit();
return true;
}

});
});
function updateCount ()
{
var qText = $("#questionTitlebox").val();
if(qText.length < 128) {
$("#noOfChar").html(128 - qText.length);
} else {
$("#noOfChar").html(0);
$("#questionTitlebox").val(qText.substring(0,128));
}
}
$("#questionTitlebox").keyup(function () {
updateCount();
});
$("#questionTitlebox").keypress(function () {
updateCount();
});
$("#questionTitlebox").keydown(function () {
updateCount();
});

</script>

