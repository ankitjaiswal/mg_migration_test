	<div class="adminrightinner">
		<?php echo($this->Form->create('MediaRecords', array('url' => array('controller' => 'qna', 'action' => 'admin_editmediarecord'), 'type'=>'file') ));?>
		<?php echo($this->Form->input('id'));?>
		<div class="tablewapper2 AdminForm">
			<table border="0" class="Admin2Table" width="100%">			   

				<tr>   <td valign="middle" class="Padleft26">Deadline</td>

					<td><?php echo($this->Form->input('MediaRecords.deadline', array('div'=>false, 'label'=>false, "class" => "TextBox5" , "style" => "width: 800px;")));?></td>
				</tr>
				<tr>   <td valign="middle" class="Padleft26">Reporter name</td>

					<td><?php echo($this->Form->input('MediaRecords.reporter_name', array('div'=>false, 'label'=>false, "class" => "TextBox5" , "style" => "width: 800px;")));?></td>
				</tr>
				<tr>   <td valign="middle" class="Padleft26">Reporter email</td>

					<td><?php echo($this->Form->input('MediaRecords.reporter_email', array('div'=>false, 'label'=>false, "class" => "TextBox5" , "style" => "width: 800px;")));?></td>
				</tr>
				<tr>
                                   <td valign="middle" class="Padleft26">Media outlet</td>
					<td><?php echo($this->Form->input('MediaRecords.media_outlet', array('div'=>false, 'label'=>false, "class" => "TextBox5" , "style" => "width: 800px;")));?></td>
				</tr>
				<tr>
                                   <td valign="middle" class="Padleft26">Subject</td>
					<td><?php echo($this->Form->input('MediaRecords.subject', array('div'=>false, 'label'=>false, "class" => "TextBox5" , "style" => "width: 800px;height:70px;")));?></td>
				</tr>
				<tr>
                                   <td valign="middle" class="Padleft26">Reporter query</td>
					<td><?php echo($this->Form->textarea('MediaRecords.reporter_query', array('rows'=>6,'div'=>true, 'label'=>false, 'id'=>'text-area' , "class" => "forminput", "style"=>"width:800px; height:140px;font-family: Ubuntu, arial, vardana; font-size: 14px;")));?></td>
				</tr>
				<tr>
                                   <td valign="middle" class="Padleft26">Requirement</td>
					<td><?php echo($this->Form->textarea('MediaRecords.requirement', array('rows'=>6,'div'=>true, 'label'=>false, 'id'=>'text-area' , "class" => "forminput", "style"=>"width:800px; height:140px;font-family: Ubuntu, arial, vardana; font-size: 14px;")));?></td>
				</tr>
                </table>
				
		</div>
		<div class="buttonwapper">
			<div><input type="submit" value="Submit" class="submit_button" /></div>
			<div class="cancel_button"><?php echo $this->Html->link("Cancel", "/admin/qna/media_record", array("title"=>"", "escape"=>false)); ?></div>
		</div>
		<?php  echo($this->Form->end());	?>	
		

		
	</div>
	
