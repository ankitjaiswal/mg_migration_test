<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<title>Guild</title>
	<link href="<?php echo(SITE_URL)?>jyotibora/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo(SITE_URL)?>jyotibora/css/style.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&amp;subset=greek" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
</head>
<body>
	<header>
		<nav class="navbar navbar-default navbar-fixed-top header-bg">
			<div class="container">
				<div class="navbar-header">
					<button aria-controls="navbar" aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button> <a class="navbar-brand" href="#"><img src="<?php echo(SITE_URL)?>jyotibora/images/logo.png"></a>
				</div>
				<div class="nav-main">
					<ul class="list-unstyled nav-top">
						<li>Call 1-866-511-1898</li>
						<li>
							<a href="https://www.guild.im/members/advantage">CONSULTANT MEMBERSHIP</a>
						</li>
					</ul>
					<div class="navbar-collapse collapse" id="navbar">
						<ul class="nav navbar-nav">
							<li>
								<a href="#">Search</a>
							</li>
							<li>
								<a href="#">Case Studies</a>
							</li>
							<li>
								<a href="#">Ask An Expert</a>
							</li>
							<li>
								<a href="#">Project Proposals</a>
							</li>
							<li>
								<a href="#"><img src="<?php echo(SITE_URL)?>jyotibora/images/nav-img.png"></a>
							</li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
			</div>
		</nav>
	</header><!-- header-section -->
	<div class="container">
		<div class="question-sec-bg">
			<h2>Question: How do we deal with competitors, who are more advanced (funding, experience, design, longer, larger team etc.)</h2>
			<p> We have launched a disruptive startup in the last mile logistics. We are 6 months on the project but have achieved significant traction.</p>
			<p>Our competitors recently launched global PR campaign. They are far more advanced. 2 yrs, 30+ team (we are 3), better product design, better funded. globally known names.</p>
			<p>We are not afraid but it seems they will outperform us. We have talked with them, but no cooperation options for now. We have chosen to focus on b2b (they are b2c). What else we can do?</p>
			<p>They are developing hardware. We also do but We want to focus on business development more. </p>
			<div class="question_btn_main">
				<div class="question_btn">
					<a href="">Innovation</a> <a href="">Small Business/Startup</a>
				</div><a class="answer" href="">Is this your area of expertise? <span>Add you answer</span></a>
			</div>
		</div>
		<div class="expert_answers">
			<h2>9 Expert answers</h2>
		</div>
		<div class="answer_bg">
			<div class="expert-sec">
				<div class="expert-img-sec"><img src="<?php echo(SITE_URL)?>jyotibora/images/expert-img.png"></div>
				<div class="expert-text-section">
					<h2>Tom Northup</h2>
					<p>Leadership, management and strategy coach developing</p>
					<h3>3 answers</h3>
				</div>
			</div>
			<div class="col-md-10 col-md-offset-1">
				<div class="answer-text-sec">
					<p>You have already focused on b2b as a competitive differentiator. Focus on your client / customer base. Don't look at what you do well but ask what do they want from your business category.</p>
					<ul>
						<li>-What is most important to them?</li>
						<li>-What will give them the most value?</li>
					</ul>
				</div>
				<div class="answer-text-sec1">
					<p>Then work to make these core competencies in your company. The stronger these core competencies the stronger your competitive advantage.</p>
					<p>As a startup you have many competing issues for your time. Be focused, clear and strong on what is most important for your client base.</p>
					
				</div>
				<div class="upvote-main-sec">
					<div class="upvote-btn">
						<a href="">UPVOTE | 12</a>
					</div>
					<ul>
						<li>
							<a href=""><img src="<?php echo(SITE_URL)?>jyotibora/images/icon.png"></a>
						</li>
						<li>
							<a href=""><img src="<?php echo(SITE_URL)?>jyotibora/images/icon1.png"></a>
						</li>
						<li>
							<a href=""><img src="<?php echo(SITE_URL)?>jyotibora/images/icon2.png"></a>
						</li>
					</ul>
				</div>
				<p class="consult-sec">Is this answer helpful? <span>Schedule a free consultation with Tom.</span></p>
			</div>
		</div>
		<div class="answer_bg">
			<div class="expert-sec">
				<div class="expert-img-sec"><img src="<?php echo(SITE_URL)?>jyotibora/images/expert-img1.png"></div>
				<div class="expert-text-section">
					<h2>Steve Johnson</h2>
					<p>The godfather of product management</p>
					<h3>8 answers</h3>
				</div>
			</div>
			<div class="col-md-10 col-md-offset-1">
				<div class="answer-text-sec">
					<p>When competing with a better funded company, you need to focus on "not". What are you that they are not? They are B2C; you're B2B. They are cheap; you are premium. It's a classic case of positioning yourself - and "slightly better" or "a little different" isn't enough. Where are you 10 times better? 100 times better?</p>
				</div>
				<div class="answer-text-sec1">
					<p>The answer is usually found in focus on a buyer type, a persona. What type of customer do you target? What are their challenges that are not being met by the competitors.</p>
				</div>
				<div class="upvote-main-sec">
					<div class="upvote-btn">
						<a href="">UPVOTE | 12</a>
					</div>
					<ul>
						<li>
							<a href=""><img src="<?php echo(SITE_URL)?>jyotibora/images/icon.png"></a>
						</li>
						<li>
							<a href=""><img src="<?php echo(SITE_URL)?>jyotibora/images/icon1.png"></a>
						</li>
						<li>
							<a href=""><img src="<?php echo(SITE_URL)?>jyotibora/images/icon2.png"></a>
						</li>
					</ul>
				</div>
				<p class="consult-sec">Is this answer helpful? <span>Schedule a free consultation with Tom.</span></p>
			</div>
		</div>
		<div class="answer_bg">
			<div class="expert-sec">
				<div class="expert-img-sec"><img src="<?php echo(SITE_URL)?>jyotibora/images/expert-img2.png"></div>
				<div class="expert-text-section">
					<h2>Jason Kitayama</h2>
					<p>Providing insights into the customer experience to create a xxxxxx</p>
					<h3>2 answers</h3>
				</div>
			</div>
			<div class="col-md-10 col-md-offset-1">
				<div class="answer-text-sec">
					<p>Consider prioritizing your BD efforts toward areas of deep expertise / product differentiation and developing a product roadmap that strategically grows your offering into a more competitive space</p>
					</div>
					
					<div class="answer-text-sec">
					<p>Understanding that you are CURRENTLY over-matched can be used to a strong advantage as you temporarily niche yourself with a vision to grow into the priority areas.</p>
					
				</div>
				<div class="answer-text-sec1">
					<p>Hopefully you have begun this process with a good enough understanding of your potential customers to develop a more targeted approach to meet their needs. The target groups would be those with specialized needs and industry influence. Use this as the "thin edge of your wedge" for successful entry, and use feedback from your competitor's customers to unravel emerging market needs.</p>
					
				</div>
				<div class="upvote-main-sec">
					<div class="upvote-btn">
						<a href="">UPVOTE | 8 </a>
					</div>
					<ul>
						<li>
							<a href=""><img src="<?php echo(SITE_URL)?>jyotibora/images/icon.png"></a>
						</li>
						<li>
							<a href=""><img src="<?php echo(SITE_URL)?>jyotibora/images/icon1.png"></a>
						</li>
						<li>
							<a href=""><img src="<?php echo(SITE_URL)?>jyotibora/images/icon2.png"></a>
						</li>
					</ul>
				</div>
				<p class="consult-sec">Is this answer helpful? <span>Schedule a free consultation with Tom.</span></p>
			</div>
		</div>
		<div class="answer_bg">
			<div class="expert-sec">
				<div class="expert-img-sec"><img src="<?php echo(SITE_URL)?>jyotibora/images/expert-img3.png"></div>
				<div class="expert-text-section">
					<h2>Natalie Jenkins</h2>
					<p>Director, Organizational Development | Innovation Guide</p>
					<h3>1 answer</h3>
				</div>
			</div>
			<div class="col-md-10 col-md-offset-1">
				<div class="answer-text-sec">
					<p>You've got some good tips here to jump start your thinking. Every business is under constant pressure to perform and deliver value. </p> </div>
					<div class="answer-text-sec">
					<p>Reading between the lines a bit it would seem that your number one challenge is business development. The fact that the competition you mentioned has a bigger budget, etc. is really just noise to you since you are targeting a different buyer. It'd be an interesting experiment for you to explore what might be all the things you would do if budget wasn't an issue. Once you explore that you could then ask yourselves, How to take action within the constraints of your budget.
</p>
				</div>
				<div class="answer-text-sec1">
					<p>There are variety of ways to creatively explore how you might grow your business. Turning your problems into questions could be your first step to finding viable solutions. This article from Fast Company came across my desk today. It seemed perfect fodder for you http://bit.ly/1Wd7PQI</p>
					
				</div>
				<div class="upvote-main-sec">
					<div class="upvote-btn">
						<a href="">UPVOTE | 3</a>
					</div>
					<ul>
						<li>
							<a href=""><img src="<?php echo(SITE_URL)?>jyotibora/images/icon.png"></a>
						</li>
						<li>
							<a href=""><img src="<?php echo(SITE_URL)?>jyotibora/images/icon1.png"></a>
						</li>
						<li>
							<a href=""><img src="<?php echo(SITE_URL)?>jyotibora/images/icon2.png"></a>
						</li>
					</ul>
				</div>
				<p class="consult-sec">Is this answer helpful? <span>Schedule a free consultation with Tom.</span></p>
			</div>
		</div>
	</div><!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">
	</script> <!-- Include all compiled plugins (below), or include individual files as needed -->
	 
	<script src="<?php echo(SITE_URL)?>jyotibora/js/bootstrap.min.js">
	</script>
</body>
</html>