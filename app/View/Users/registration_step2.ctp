<?php echo($this->Html->css(array('opentip'))); ?>
<?php echo($this->Html->script(array('opentip-jquery')));?>

<div class="content-wrap registration-step">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div id="inner-content-wrapper" class="rs-box">
                    <div id="inner-content-box" class="pagewidth">
                        <div id="user" class="registration-user">
                            <div id="pro-creation">
                                <h3>Profile Creation</h3>
                            </div>
                            <?php  echo($this->Form->create('User',array('url'=>array('controller'=>'users','action'=>'registration_step2'),'type'=>'file', 'id'=>'UserRegistrationStep2Form', "name"=>'UserRegistrationStep2Form'))); ?>
                            <?php  //e($this->Form->hidden('User.username'));?>
                            <?php  //e($this->Form->hidden('User.password2'));?>
                            <?php //e($this->Form->hidden('UserImage.image_temp_name'));?>
                            <?php //e($this->Form->hidden('User.hash',array('value'=>'temp'.time()."_mentee"))); ?>
                            <div id="name-box">
                                <div id="profileImage" class="profile-img">
                                    <?php
                                    $imageName = $this->General->FindUserImage($this->Session->read('Auth.User.id'));
                                    if(isset($imageName) && $imageName!='')
                                    {
                                      $image_path = MENTEES_IMAGE_PATH.DS.$this->Session->read('Auth.User.id').DS.$imageName;
                                      echo($this->Html->image($image_path,array('alt'=>$this->Session->read('Auth.UserReference.first_name')." ".$this->Session->read('Auth.UserReference.last_name'),'style' => 'padding-bottom:0px;')));
                                      echo($this->Form->hidden($imageName,array('value'=>$imageName,'id'=>'image_name')));
                                    }
                                    else
                                    {
                                    echo($this->Html->image('media/profile.png',array('alt'=>'user-image'))); 
                                    echo($this->Form->hidden('profile.png',array('value'=>'profile.png','id'=>'image_name')));
                                    } ?>
                                    <div class="registration-add-image">
                                        <?php echo($this->Element('Menteesuser/step2_add_image')); ?>
                                    </div>
                                </div>



                                <div class="registration-form">
                                    <div id="input-names">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <?php echo($this->Form->input('UserReference.first_name',array('maxlength'=>'20','div'=>false,'label'=>false,'class'=>'form-control','placeholder'=>'First Name','value'=>$this->Session->read('Auth.UserReference.first_name'))));?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <?php echo($this->Form->input('UserReference.last_name',array('maxlength'=>'20','div'=>false,'label'=>false,'class'=>'form-control','placeholder'=>'Last Name','value'=>$this->Session->read('Auth.UserReference.last_name'))));?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <?php echo($this->Form->input('UserReference.headline',array('div'=>false,'label'=>false,'class'=>'form-control','placeholder'=>'Professional Headline','value'=>$this->Session->read('Auth.UserReference.headline'))));?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <?php echo($this->Form->input('UserReference.zipcode',array('id'=>'register_zipcode','div'=>false,'label'=>false,'class'=>'form-control','placeholder'=>'ZIP (if US based) OR Country','value'=>$this->Session->read('Auth.UserReference.zipcode'))));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php echo($this->Form->input('UserReference.country',array('type'=>'hidden','div'=>false,'label'=>false,'class'=>'form-control','placeholder'=>'Country','value'=>'US','readonly'=>true)));?>

                                    <div id="country">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php /*     <?php echo($this->Form->input('UserReference.country',array('div'=>false,'label'=>false,'class'=>'form-control','placeholder'=>'Country','value'=>'US','readonly'=>true)));?>
                                                    <?php echo($this->Form->input('UserReference.zipcode',array('id'=>'zipcode','div'=>false,'maxlength'=>'5','label'=>false,'class'=>'form-control','placeholder'=>'ZIP (if US based) OR Country','value'=>$this->Session->read('Auth.UserReference.zipcode'))));?>
                                                    <?php //echo($this->Html->image('loading.gif',array('id'=>'loading','style'=>'float:right')));?>
                                                    <span id="ajax_response" style="margin-top:-20px"></span> */?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="upload-resume" class="upload-resume">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <h4>Background summary</h4>
                                                    <?php echo($this->Form->textarea('UserReference.background_summary',array('div'=>false,'label'=>false,'id'=>'text-area','class'=>'form-control','placeholder'=>'Background summary','value'=>$this->Session->read('Auth.UserReference.background_summary'))));?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="links" class="links">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <h4>Profile links</h4>
                                                    <div id="p_social">
                                                    <?php 
                                                    $ss=0;
                                                    $userLinks = $this->General->FindSocialLink($this->Session->read('Auth.User.id'));
                                                    //pr($userLinks); die;
                                                    if(!empty($userLinks) && count($userLinks)>0)
                                                    {
                                                        $cc = count($userLinks);
                                                        foreach($userLinks as $social):
                                                            if(trim($social['Social']['social_name'])!=''):
                                                                echo "<p>";
                                                                echo($this->Form->input('Social.'.$ss.'.social_name',array('onblur'=>'chkUrl(this)','div'=>false,'label'=>false,'placeholder'=>'Twitter','value'=>$social['Social']['social_name'],'class'=>'form-control')));
                                                                if($ss==0):
                                                                    echo "<span class='addmore' style='cursor:pointer' id='addSocial'>&nbsp[ + ]</span></p>";
                                                                else:
                                                                    echo "<span class='remSocial addmore' style='cursor:pointer'>&nbsp[ - ]</span></p>";
                                                                endif;
                                                                $ss++;
                                                            endif;
                                                        endforeach;
                                                    } 
                                                    else { 
                                                    ?>
                                                    <p class="rs-profile-link">
                                                        <?php  echo($this->Form->input('Social.0.social_name',array('onblur'=>'chkUrl(this)','div'=>false,'label'=>false,'class'=>'form-control','placeholder'=>'Twitter')));
                                                    echo '<span id="addSocial" class="addmore">[ + ]</span>';                 ?>
                                                    </p>
                                                    <?php } ?>
                                                    </div>
                                                    <div class="example-domain"><span>e.g. http://www.domain.com</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div id="app-button" class="row">
                                        <div class="col-md-12">
                                            <?php echo($this->Form->submit('Submit',array('id'=>'submit','onclick'=>'return validateMenteeRegister();','class'=>'btn btn-primary'))); ?>
                                        </div>
                                    </div>
                                </div>





                            </div>
                            <?php echo($this->Form->end()); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
<script src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrapValidator.min.js" type="text/javascript"></script> 
<script>
    jQuery(document).ready(function() {

        jQuery(function() {
            var socialDiv = jQuery('#p_social');
            var i = jQuery('#p_social p').size() + 1;

            jQuery(document).on('click', '#addSocial', function() {   
                if (i <= 5) {
                    var title = '<label>';
                    title += '<input onblur=chkUrl(this) type="text" id="Social' + i + 'SocialName" size="97" name="data[Social][' + i + '][social_name]" value="" placeholder="Twitter" />';
                    title += '</label>';
                    var remove = '<span class="remSocial addmore" style="cursor:pointer">&nbsp;[ - ]</span>';
                    jQuery('<p>' + title + remove + '</p>').appendTo(socialDiv);
                    i++;
                    return false;
                } else {
                    alert("You can enter up to 5 profile links");
                }
            });

             jQuery(document).on('click', '.remSocial', function() { 
                if (i > 2) {
                    jQuery(this).parents('p').remove();
                    i--;
                }
                return false;
            });

            jQuery('#UserRegistrationStep2Form').bootstrapValidator({
                trigger: 'blur',
                fields: {
                    'data[UserReference][first_name]': {
                        //enabled : false,
                        validators: {
                            notEmpty: {
                                message: 'Do not leave blank'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z ]+$/,
                                message: 'Only alphabets are allowed'
                            }
                        }
                    },
                    'data[UserReference][last_name]': {
                        validators: {
                            notEmpty: {
                                message: 'Do not leave blank'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z ]+$/,
                                message: 'Only alphabets are allowed'
                            }
                        }
                    },
                    'data[UserReference][headline]': {
                        validators: {
                            notEmpty: {
                                message: 'Do not leave blank'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z ]+$/,
                                message: 'Only alphabets are allowed'
                            }
                        }
                    },
                    'data[UserReference][zipcode]': {
                          validators: {
                              notEmpty: {
                                  message: 'Do not leave blank'
                              },
                            stringLength: { 
                            message: 'ZIP should be maximum 5 digits', 
                            max: function (value, validator, $field) { 
                            return 5 - (value.match(/\r/g) || []).length; 
                            } 
                            },                          
                              regexp: {
                                  regexp: /^\d+$/,
                                  message: 'ZIP should be 5 digits and numbers only'
                              }
                          }
                    },
                    'data[UserReference][background_summary]': {
                          validators: {
                              notEmpty: {
                                  message: 'Your background summary is required'
                              },
                            stringLength: { 
                            message: '1. Use 4-6 short paragraphs</br>2. Open with a 2-3 lines of overview</br>3. Focus on your past consulting work</br>4. Mention related experience, speaking engagements, publications</br>5. End with education, credentials, affiliations</br>(Minimum 500 characters)', 
                            min: function (value, validator, $field) { 
                            return 500 - (value.match(/\r/g) || []).length; 
                            } 
                            }

                          }
                    }
               },
            });
        });

    });

    function chkUrl(data) {
        /*var URLReg = new RegExp();
        URLReg.compile("^[A-Za-z]+://[A-Za-z0-9-_]+\\.[A-Za-z0-9-_%&\?\/.=]+$");*/
        flag = 0;
        var url_reg = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
        if (jQuery.trim(data.value) != '') {
            if (!url_reg.test(data.value)) {
                jQuery('#' + data.id).css('border-color', '#F00');
                flag++;
            } else
                jQuery('#' + data.id).css('border-color', '');
        } else
            jQuery('#' + data.id).css('border-color', '');

        if (flag > 0)
            return false;
    }


</script>