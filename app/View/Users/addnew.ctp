<div class="users form"  style="padding:40px; min-height: 200px;">
<?php echo $this->Form->create('User',array('url'=>array('controller'=>'users','action'=>'addnew'),'id'=>'loginForm')); ?>
    <fieldset>
        <legend><?php echo __('Add User'); ?></legend>
        <?php echo $this->Form->input('username');
        echo $this->Form->input('password');
        echo $this->Form->input('role', array(
            'options' => array('admin' => 'Admin', 'author' => 'Author')
        ));
    ?>
    </fieldset>
    <div style="float: right;">
<?php echo $this->Form->end(__('Submit')); ?>
</div>
</div>