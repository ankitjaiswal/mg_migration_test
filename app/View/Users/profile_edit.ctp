<script type="text/javascript">
Array.prototype.remove = function() {
    var what, a = arguments,
        L = a.length,
        ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};
console.log(1);
var topicVal = [-1, -1, -1, -1, -1];
var topicCount = 0;

function monkeyPatchAutocomplete() {

    jQuery.ui.autocomplete.prototype._renderItem = function(ul, item) {
        var re = new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + this.term + ")(?![^<>]*>)(?![^&;]+;)", "gi");
        var t = item.label.replace(re, "<span style='font-weight:bold;color:#434343;'>" +
            "$&" +
            "</span>");
        return jQuery("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + t + "</a>")
            .appendTo(ul);
    };
}

function getKeywords() {

    var allKeywords = <?php echo json_encode($allKeywords); ?>;

    return allKeywords;
}

function deleteThis(element) {
    var topic = jQuery('#noOftopics').html();
    topicVal[element] = -1;

    for (var i = 0; i < 5; i++) {

        if (topicVal[i] == -1) {
            jQuery('#div' + i).css('display', 'none');
            jQuery('#topic' + i).html('');
            jQuery("#noOftopics").html(parseInt(topic) + parseInt(1));
        } else {
            jQuery('#div' + i).css('display', 'inline-block');
            jQuery('#topic' + i).html(topicVal[i]);
            jQuery("#noOftopics").html(parseInt(topic) + parseInt(1));
        }
    }
}

jQuery(document).ready(function() {
    jQuery("#noOftopics").html(5);
    var hidVal = jQuery('#hiddenSelect').val();
    //alert(hidVal);
    if (hidVal != '') {
        var hidValArr = hidVal.split(",");
        var total = 0;
        for (var i = 0; i < hidValArr.length; i++) {
            topicVal[i] = hidValArr[i];
            jQuery('#div' + i).css('display', 'inline-block');
            jQuery('#topic' + i).html(hidValArr[i]);
            total++;
        }
        jQuery("#noOftopics").html(5 - total);
    }
    monkeyPatchAutocomplete();

    var CityKeyword = jQuery('#CityKeyword');

    CityKeyword.autocomplete({
        minLength: 1,
        source: getKeywords(),
        source: function(request, response) {

            var results = jQuery.ui.autocomplete.filter(getKeywords(), request.term);

            for (var x = 0; x < 5; x++) {
                if (topicVal[x] != -1)
                    results.remove(topicVal[x]);
            }

            results = results.slice(0, 5);
            results.push('Add <b>"' + jQuery("#CityKeyword").val() + '"</b>');
            
            response(results); //show 3 items.
        },
        focus: function(event, ui) {

            if (ui.item.value[0] == "A")
                return false;

        },
        select: function(event, ui) {
            //var t = jQuery("#noOftopics").val();
            var toAdd = ui.item.value;
            if (ui.item.value[1] == "d") {
                var total = toAdd.length;
                var final = total - 5;
                toAdd = toAdd.substring(8, final);
            }

            var changed = -1;

            for (var i = 0; i < 5; i++) {

                if (topicVal[i] == -1) {
                    changed = 1;

                    topicVal[i] = toAdd;
                    jQuery("#noOftopics").html(4 - i);
                    break;
                }
            }

            if (changed == -1)
                alert("You can select up to 5 topics");

            for (var i = 0; i < 5; i++) {

                if (topicVal[i] == -1) {
                    jQuery('#div' + i).css('display', 'none');
                    jQuery('#topic' + i).html('');

                } else {
                    jQuery('#div' + i).css('display', 'inline-block');
                    jQuery('#topic' + i).html(topicVal[i]);

                }

            }

            jQuery("#CityKeyword").val('');

            return false;
        }
    });
});


jQuery(function() {
    jQuery('select').change(function() {
        var old_type = this.id.substring(13);

        type = old_type.substring(0, 8);

        if (type == 'Industry') {

            var id = old_type.substring(8);

            var industry = this.value;

            var select_id = "#UserReferenceCategory" + id;

            var item = jQuery(select_id);

            item.find('option').remove();


            if (industry != -1) {

                var cats = document.getElementById(industry);
                var cat_array = cats.value.split("|");
                //alert(cat_array);
                var i = cat_array.indexOf("ALL CATEGORIES");
                if (i != -1) {
                    cat_array.splice(i, 1);
                }

                cat_array = cat_array.sort();
                cat_array.push('ALL CATEGORIES');


                for (i = 0; i < cat_array.length; i++) {
                    item.append('<option value="' + cat_array[i] + '">' + cat_array[i] + '</option>');
                }
            } else {
                item.append('<option value="-1"> </option>');
            }

        }
    });
});
</script>
<?php 

 $topicarray = array();

if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] != -1 && $this->data['MemberIndustryCategory']['topic4'] != -1 && $this->data['MemberIndustryCategory']['topic5'] != -1){
 $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']].",".$categories[$this->data['MemberIndustryCategory']['topic3']].",".$categories[$this->data['MemberIndustryCategory']['topic4']].",".$categories[$this->data['MemberIndustryCategory']['topic5']];

}
else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] != -1 && $this->data['MemberIndustryCategory']['topic4'] != -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
 $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']].",".$categories[$this->data['MemberIndustryCategory']['topic3']].",".$categories[$this->data['MemberIndustryCategory']['topic4']];

}
else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] != -1 && $this->data['MemberIndustryCategory']['topic4'] == -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
 $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']].",".$categories[$this->data['MemberIndustryCategory']['topic3']];

}
else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] == -1 && $this->data['MemberIndustryCategory']['topic4'] == -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
 $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']];

}
else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] == -1 && $this->data['MemberIndustryCategory']['topic3'] == -1 && $this->data['MemberIndustryCategory']['topic4'] == -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
 $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']];

}
else{
 $topicarray ="";

}
//pr($this->data['Social']);
//die();
?>
<?php
                    $socialCount=0;
                    $linkedinURL = '';
                                   $twitterURL ='';
                                   $googleURL ='';
                                   $facebookURL ='';
                                   $youtubeURL ='';
                    while(count($this->data['Social'])>$socialCount)
                    {
                    
                    if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'http'))!==false)
                        {
                            $link=$this->data['Social'][$socialCount]['social_name'];
                        }
                        else
                        {
                            $link="https://".$this->data['Social'][$socialCount]['social_name'];
                        }
                        
                        $icon = $this->General->getIcon($this->data['Social'][$socialCount]['social_name']);
                        if($icon == 'media_new/linkedin.svg')
                            $linkedinURL = $link;
                                          if($icon == 'media_new/svg/twitter.svg')
                            $twitterURL = $link;
                                          if($icon == 'media_new/svg/Google+.svg')
                            $googleURL = $link;
                                          if($icon == 'media_new/svg/facebook.svg')
                            $facebookURL = $link;
                        if($icon == 'media_new/youtube.svg')
                            $youtubeURL = $link;
                        $socialCount++;
                        }
                    ?>
    <?php 
$facecm = '';
$phonecm = '';
$videocm = '';
$facevalue;
$phonevalue;
$videovalue;
if(isset($this->data['Communication'][0])){
        if($this->data['Communication'][0]['mode_type'] == 'face_to_face'){
            $facecm = 'checked';
            $facevalue = $this->data['Communication'][0]['mode'];
        }else if($this->data['Communication'][0]['mode_type'] == 'phone'){
            $phonecm = 'checked';
            $phonevalue = $this->data['Communication'][0]['mode'];
        }else if($this->data['Communication'][0]['mode_type'] == 'video'){
            $videocm = 'checked';
            $videovalue = $this->data['Communication'][0]['mode'];
        }
}
if(isset($this->data['Communication'][1]['mode_type'])){
        if($this->data['Communication'][1]['mode_type'] == 'face_to_face'){
            $facecm = 'checked';
            $facevalue = $this->data['Communication'][1]['mode'];
        }else if($this->data['Communication'][1]['mode_type'] == 'phone'){
            $phonecm = 'checked';
            $phonevalue = $this->data['Communication'][1]['mode'];
        }else if($this->data['Communication'][1]['mode_type'] == 'video'){
            $videocm = 'checked';
            $videovalue = $this->data['Communication'][1]['mode'];
        }
}
if(isset($this->data['Communication'][2]['mode_type'])){
        if($this->data['Communication'][2]['mode_type'] == 'face_to_face'){
            $facecm = 'checked';
            $facevalue = $this->data['Communication'][2]['mode'];
        }else if($this->data['Communication'][2]['mode_type'] == 'phone'){
            $phonecm = 'checked';
            $phonevalue = $this->data['Communication'][2]['mode'];
        }else if($this->data['Communication'][2]['mode_type'] == 'video'){
            $videocm = 'checked';
            $videovalue = $this->data['Communication'][2]['mode'];
        }
}
if(isset($this->data['Communication'][0]['id'])){
    echo($this->Form->hidden('Communication.0.id'));
}
if(isset($this->data['Communication'][1]['id'])){
    echo($this->Form->hidden('Communication.1.id'));
}
if(isset($this->data['Communication'][2]['id'])){
    echo($this->Form->hidden('Communication.2.id'));
}
?>
    <?php

    $ioptions = array();
    $count = 0;
    $options[-1] = '';


    $industry_options = array();
    $category_options = array();
    $industry_options[-1] = '';
    
    foreach ($industry_categories as $value) {
        
        $ioptions[$value['IndustryCategory']['id']] = $value['IndustryCategory']['category'];
        
        if($value['IndustryCategory']['parent'] == 0) 
            $industry_options[$value['IndustryCategory']['category']] = $value['IndustryCategory']['category'];
    }
    
    foreach ($industry_categories as $value) {
        
        if($value['IndustryCategory']['parent'] > 0)  {
                    
            if($category_options[$ioptions[$value['IndustryCategory']['parent']]] == '')
                $category_options[$ioptions[$value['IndustryCategory']['parent']]] = $value['IndustryCategory']['category'];
            else
                $category_options[$ioptions[$value['IndustryCategory']['parent']]] = $category_options[$ioptions[$value['IndustryCategory']['parent']]].'|'.$value['IndustryCategory']['category'];
        }
    }
                        
    foreach ($category_options as $key => $value) {
            ?>
        <input type="hidden" name="<?php echo($key); ?>" value="<?php echo($value);?>" id="<?php echo($key); ?>" />
        <?php
    }
    $industry_options = array_unique($industry_options);
    
    $industry1 = '';
    $industry2 = '';
    $industry3 = '';
    $category1_options = '';
    $category1_options = '';
    $category1_options = '';
    
    if(isset($this->data['MemberIndustryCategory']) && isset($this->data['MemberIndustryCategory']['industry1']) ) {
            
        $industry1 = $ioptions[$this->data['MemberIndustryCategory']['industry1']];
        $industry2 = $ioptions[$this->data['MemberIndustryCategory']['industry2']];
        $industry3 = $ioptions[$this->data['MemberIndustryCategory']['industry3']];
                        
        $category1 = $ioptions[$this->data['MemberIndustryCategory']['category1']];
        $category2 = $ioptions[$this->data['MemberIndustryCategory']['category2']];
        $category3 = $ioptions[$this->data['MemberIndustryCategory']['category3']];
        
        $category11_options = explode("|",$category_options[$industry1]);
        $category22_options = explode("|",$category_options[$industry2]);
        $category33_options = explode("|",$category_options[$industry3]);
        
        foreach ($category11_options as $key => $value) {
            $category1_options[$value] = $value;
        }
        
        foreach ($category22_options as $key => $value) {
            $category2_options[$value] = $value;
        }
            
        foreach ($category33_options as $key => $value) {
            $category3_options[$value] = $value;
        }
    }
?>
            <div class="profile-member-content">
                <div class="profile-banner">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="profile-banner-box edit">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="banner-box">
                                                <div class="banner-box-img" id="profileImage">
                                                    <?php if(isset($this->data['UserImage'][0]['image_name']) && $this->data['UserImage'][0]['image_name'] !=''){
                                       if($this->data['User']['role_id']== 2){
                                        $image_path = MENTORS_IMAGE_PATH.DS.$this->data['User']['id'].DS.$this->data['UserImage'][0]['image_name'];
                                        echo($this->Html->image($image_path,array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])));   
                                       }else{
                                        $image_path = MENTEES_IMAGE_PATH.DS.$this->data['User']['id'].DS.$this->data['UserImage'][0]['image_name'];
                                        echo($this->Html->image($image_path,array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])));   
                                       }                            
                                      }else{
                                        echo($this->Html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'])));
                                      }?>
                                                    <div class="button-set edit">
                                                        <?php echo($this->Element('Mentorsuser/add_image'));?>
                                                    </div>
                                                </div>
                                                <div class="banner-box-content">
                                                    <div class="bb-title">
                                                        <h4><span id="viewfirstname"><?php echo(ucfirst($this->data['UserReference']['first_name']));?></span> <span id="viewlastname"><?php echo(ucfirst($this->data['UserReference']['last_name'])); ?></span></h4>
                                                        <?php if($this->data['UserReference']['guarantee'] == 'Y') {
                                             echo($this->Html->link($this->Html->image('media/gurantee_shield28.png',array('alt'=>'Client Satisfaction Guaranteed','title'=>'Client Satisfaction Guaranteed')),SITE_URL.'fronts/guarantee',array('escape'=>false)));
                                            } ?>
                                                        <a class="e-pro" href="javascript:;" data-toggle="modal" data-target="#edit_profile"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    </div>
                                                    <span>
                                        <?php if($city != '') {
                                         $citylink = str_replace(" ","-",$city);
                                         $keyword = "browse-all";?>
                                         <a href="<?php echo(SITE_URL.'browse/search_result/all-industries/all-categories/'.$keyword.'/'.$state.'/'.$citylink);?>"><?php echo($city) ;?>, <?php echo($state) ;?></a>
                                        <?php } else {
                                          echo($this->data['UserReference']['zipcode']);
                                        }?>
                                       <?php $disp="guild.im/".strtolower($this->data['User']['url_key']);
                                             $completelink=SITE_URL.strtolower($this->data['User']['url_key']);
                                             $displink=SITE_URL.strtolower($this->data['User']['url_key']);?>
                                                        
                                            <a href="#profile"><?php echo($disp); ?></a>
                                            <p><a href="#footer-logo">See how you're connected</a></p>
                                        </span>
                                                    <p id="viewprofileheadline">
                                                        <?php echo(ucfirst($this->data['UserReference']['linkedin_headline']));?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="profile-inner-content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="tab-content">
                                <!-- Profile Tab Begin -->
                                <div class="gpp-profile tab-pane active" id="profile">
                                    <div class="row">
                                        <div class="col-lg-9 col-md-8">
                                            <!-- Areas of expertise edit popup -->
                                            <div class="edit-aoe-box aoe-m">
                                                <div class="edit-aoe">
                                                    <div class="aoe-title">
                                                        <i class="fa fa-cogs" aria-hidden="true"></i>
                                                        <h3>Industry - Category</h3>
                                                    </div>
                                                    <a href="javascript:;" data-toggle="modal" data-target="#aoe_edit">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                                </div>
                                                <div class="aoe-box">
                                                    <?php if($this->data['MemberIndustryCategory']['industry1'] !='-1'){?>
                                                    <div class="form-group col-md-5 col-sm-6 col-xs-6 col-aoe-12">
                                                        <input type="text" class="form-control" name="" id="viewind1" value="<?php echo($indcategories[$this->data['MemberIndustryCategory']['industry1']]);?>" readonly>
                                                    </div>
                                                    <div class="form-group col-md-5 col-sm-6 col-xs-6 col-aoe-12">
                                                        <input type="text" class="form-control" name="" id="viewcat1" value="<?php echo($indcategories[$this->data['MemberIndustryCategory']['category1']]);?>" readonly>
                                                    </div>
                                                    <?php }?>
                                                    <?php if($this->data['MemberIndustryCategory']['industry2'] !='-1'){?>
                                                    <div class="form-group col-md-5 col-sm-6 col-xs-6 col-aoe-12">
                                                        <input type="text" class="form-control" name="" id="viewind2" value="<?php echo($indcategories[$this->data['MemberIndustryCategory']['industry2']]);?>" readonly>
                                                    </div>
                                                    <div class="form-group col-md-5 col-sm-6 col-xs-6 col-aoe-12">
                                                        <input type="text" class="form-control" name="" id="viewcat2" value="<?php echo($indcategories[$this->data['MemberIndustryCategory']['category2']]);?>" readonly>
                                                    </div>
                                                    <?php }?>
                                                    <?php if($this->data['MemberIndustryCategory']['industry3'] !='-1'){?>
                                                    <div class="form-group col-md-5 col-sm-6 col-xs-6 col-aoe-12">
                                                        <input type="text" class="form-control" name="" id="viewind3" value="<?php echo($indcategories[$this->data['MemberIndustryCategory']['industry3']]);?>" readonly>
                                                    </div>
                                                    <div class="form-group col-md-5 col-sm-6 col-xs-6 col-aoe-12">
                                                        <input type="text" class="form-control" name="" id="viewcat3" value="<?php echo($indcategories[$this->data['MemberIndustryCategory']['category3']]);?>" readonly>
                                                    </div>
                                                    <?php }?>
                                                </div>
                                            </div>
                                            <!-- Topics popup -->
                                            <div class="edit-aoe-box topics" id="viewtopic" style="display:block;">
                                                <div class="edit-aoe">
                                                    <div class="aoe-title">
                                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                                        <h3>Topics</h3>
                                                    </div>
                                                    <a href="javascript:;" data-toggle="modal" data-target="#aoe_topics">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                                </div>
                                                <div class="aoe-box">
                                                    <ul class="topics-tag">
                                                        <?php if($this->data['MemberIndustryCategory']['topic1'] !='-1'){?>
                                                        <li><span><?php echo($categories[$this->data['MemberIndustryCategory']['topic1']]);?></span></li>
                                                        <?php }?>
                                                        <?php if($this->data['MemberIndustryCategory']['topic2'] !='-1'){?>
                                                        <li><span><?php echo($categories[$this->data['MemberIndustryCategory']['topic2']]);?></span></li>
                                                        <?php }?>
                                                        <?php if($this->data['MemberIndustryCategory']['topic3'] !='-1'){?>
                                                        <li><span><?php echo($categories[$this->data['MemberIndustryCategory']['topic3']]);?></span></li>
                                                        <?php }?>
                                                        <?php if($this->data['MemberIndustryCategory']['topic4'] !='-1'){?>
                                                        <li><span><?php echo($categories[$this->data['MemberIndustryCategory']['topic4']]);?></span></li>
                                                        <?php }?>
                                                        <?php if($this->data['MemberIndustryCategory']['topic5'] !='-1'){?>
                                                        <li><span><?php echo($categories[$this->data['MemberIndustryCategory']['topic5']]);?></span></li>
                                                        <?php }?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- Topics popup -->
                                            <div class="edit-aoe-box topics" id="edittopic" style="display:none;">
                                                <div class="edit-aoe">
                                                    <div class="aoe-title">
                                                        <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                                                        <h3>Topics</h3>
                                                    </div>
                                                    <a href="javascript:;" data-toggle="modal" data-target="#aoe_topics">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                                </div>
                                                <div class="aoe-box">
                                                    <ul class="topics-tag">
                                                        <li><span id="vvtopic0" style="display:none;"><?php echo($categories[$this->data['MemberIndustryCategory']['topic1']]);?></span></li>
                                                        <li><span id="vvtopic1" style="display:none;"><?php echo($categories[$this->data['MemberIndustryCategory']['topic1']]);?></span></li>
                                                        <li><span id="vvtopic2" style="display:none;"><?php echo($categories[$this->data['MemberIndustryCategory']['topic1']]);?></span></li>
                                                        <li><span id="vvtopic3" style="display:none;"><?php echo($categories[$this->data['MemberIndustryCategory']['topic1']]);?></span></li>
                                                        <li><span id="vvtopic4" style="display:none;"><?php echo($categories[$this->data['MemberIndustryCategory']['topic1']]);?></span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- Professional summary popup -->
                                            <div class="edit-aoe-box topics">
                                                <div class="edit-aoe">
                                                    <div class="aoe-title">
                                                        <i class="fa fa-file-text" aria-hidden="true"></i>
                                                        <h3>Professional summary</h3>
                                                    </div>
                                                    <a href="javascript:;" data-toggle="modal" data-target="#pro_summary">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                                </div>
                                                <div class="gpp-profile-content" id="opensummary">
                                                    <?php          
                                                                $string  =  $this->data['UserReference']['background_summary'];
                                                                $first = substr($string, 0, 780);
                                                                $first = wordwrap($first, 780,"<br>\n");
                                                                 ?>
                                                    <p id="viewprofessionalsummary">
                                                        <?php echo(nl2br(strip_tags($first))); ?> <a class="read-more collapsed" href="javascript:void(0);" data-toggle="collapse" id="summaryreadmore">Read More</a></p>
                                                </div>
                                                <div class="gpp-profile-content" id="hiddensummary" style="display:none;">
                                                    <?php echo(nl2br($this->data['UserReference']['background_summary'])); ?>
                                                </div>
                                            </div>
                                            <!-- Engagement overview popup -->
                                            <div class="edit-aoe-box topics">
                                                <div class="edit-aoe">
                                                    <div class="aoe-title">
                                                        <i class="fa fa-cog" aria-hidden="true"></i>
                                                        <h3>Engagement overview</h3>
                                                    </div>
                                                    <a href="javascript:;" data-toggle="modal" data-target="#e_overview">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                                </div>
                                                <div class="gpp-profile-content">
                                                    <p id="viewengagementovierview">
                                                        <?php if($this->data['UserReference']['feeNSession']) {
                                 $result = str_replace('  ', '&nbsp;&nbsp;', $this->data['UserReference']['feeNSession']); 
                                 echo nl2br($result);           
                                 } else {?> Duration and cost of an engagement depends on the scope of work, identified during the initial consultation.
                                                        <?php }?>
                                                    </p>
                                                </div>
                                            </div>
                                            <!-- Clients popup -->
                                            <div class="edit-aoe-box topics">
                                                <div class="edit-aoe">
                                                    <div class="aoe-title">
                                                        <i class="fa fa-building" aria-hidden="true"></i>
                                                        <h3>Clients</h3>
                                                    </div>
                                                    <a href="javascript:;" data-toggle="modal" data-target="#e_clients">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                                </div>
                                                <div class="gpp-profile-content" id="openclientlist">
                                                    <p>
                                                        <?php echo $this->General->make_links(nl2br($this->data['UserReference']['past_clients'])); ?> 
                                                   </p>
                                                </div>
                                                <div class="gpp-profile-content" id="hiddenclientlist" style="display:none;">
                                                    <p>
                                                        <?php $clients = str_replace('  ', '&nbsp;&nbsp;', $this->data['UserReference']['past_clients']);
                                                             echo $this->General->make_links(nl2br($clients));?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 col-xs-012">
                                            <div class="edit-social">
                                                <ul class="ggp-social ess-social">
                                                    <?php if(($this->data['UserReference']['business_website']) !=''){
                     $link ='';
                     $url ='';
                       if(($pos =strpos($this->data['UserReference']['business_website'],'http'))!==false)
                        {
                            $link=$this->data['UserReference']['business_website'];
                        }
                        else
                        {
                            $link="http://".$this->data['UserReference']['business_website'];
                        }
                  ?>
                                                    <li><a href="<?php echo($link);?>" target="_blank" data-toggle="tooltip" data-placement="top" title="Business Website" id="busineswebsitelink"><i class="fa fa-globe" aria-hidden="true"></i></a></li>
                                                    <?php }?>
  

                   <?php
                    $url ='';
                    $socialCount=0;
                                    
$icon1 = '';
$icon2 = '';
$icon3 = '';
$icon4 = '';
$icon5 = '';


$link1 = '';
$link2 = '';
$link3 = '';
$link4 = '';
$link5 = '';

                               
                    
                    while(count($this->data['Social'])>$socialCount)
                    {
                    
                        if(($pos=strpos($this->data['Social'][$socialCount]['social_name'],'http'))!==false)
                        {
                            $link=$this->data['Social'][$socialCount]['social_name'];
                        }
                        else
                        {
                            $link="https://".$this->data['Social'][$socialCount]['social_name'];
                        }
                        
                        $icon = $this->General->getIcon($this->data['Social'][$socialCount]['social_name']);

                                          
                        if($socialCount == 0) {
                        
                            $icon1 = $icon;
                            $link1 = $link;
                            $keys = parse_url($icon1); // parse the url
                            $path = explode("/", $keys['path']); // splitting the path
                            $str = $path[2];
                            $path1 = explode(".", $str );
                            $final1 = $path1[0];                            
                        } else if($socialCount == 1) {
                            $icon2 = $icon;
                            $link2 = $link; 
                            $keys = parse_url($icon2); // parse the url
                            $path = explode("/", $keys['path']); // splitting the path
                            $str = $path[2];
                            $path1 = explode(".", $str );
                            $fina2 = $path1[0];                             
                        } else if($socialCount == 2) {
                            $icon3 = $icon;
                            $link3 = $link;
                            $keys = parse_url($icon3); // parse the url
                            $path = explode("/", $keys['path']); // splitting the path
                            $str = $path[2];
                            $path1 = explode(".", $str );
                            $final3 = $path1[0];                                
                        } else if($socialCount == 3) {
                            $icon4 = $icon;
                            $link4 = $link;
                            $keys = parse_url($icon4); // parse the url
                            $path = explode("/", $keys['path']); // splitting the path
                            $str = $path[2];
                            $path1 = explode(".", $str );
                            $final4 = $path1[0];                                
                        } else if($socialCount == 4) {
                            $icon5 = $icon;
                            $link5 = $link;
                            $keys = parse_url($icon5); // parse the url
                            $path = explode("/", $keys['path']); // splitting the path
                            $str = $path[2];
                            $path1 = explode(".", $str );
                            $final5 = $path1[0];                                
                        }
                         $socialCount++;}?> 
    


                                       


            
<li><?php echo($this->Html->link($this->Html->image($icon1,array('alt'=>$link1, 'class'=>$final1, 'id'=>'icon1')),$link1,array('escape'=>false , 'target'=>'_blank')));?></li>
<li><?php echo($this->Html->link($this->Html->image($icon2,array('alt'=>$link2, 'class'=>$final2, 'id'=>'icon2')),$link2,array('escape'=>false , 'target'=>'_blank')));?></li>
<li><?php echo($this->Html->link($this->Html->image($icon3,array('alt'=>$link3, 'class'=>$final3, 'id'=>'icon3')),$link3,array('escape'=>false , 'target'=>'_blank')));?></li>
<li><?php echo($this->Html->link($this->Html->image($icon4,array('alt'=>$link4, 'class'=>$final4, 'id'=>'icon4')),$link4,array('escape'=>false , 'target'=>'_blank')));?></li>
<li><?php echo($this->Html->link($this->Html->image($icon5,array('alt'=>$link5, 'class'=>$final5, 'id'=>'icon5')),$link5,array('escape'=>false , 'target'=>'_blank')));?></li>

</ul>
                                                    

                                    <a href="javascript:void(0);" class="esl-link" data-toggle="modal" data-target="#esl_link">Edit social links <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                </div>
                                <div class="ggp-sidebar edit-euh">
                                    <div class="form-group">
                                        <span>Business website</span>
                                        <input readonly type="text" class="form-control" id="viewbusinesswebsite" name="" value="<?php echo($this->data['UserReference']['business_website']);?>" placeholder="Enter URL here">
                                    </div>
                                    <div class="form-group">
                                        <span>Intro video (embed link)</span>
                                        <input readonly type="text" class="form-control" id="viewintrovideo" value='<?php echo($this->data['UserReference']['intro_link']);?>' placeholder="Enter embed link here">
                                    </div>
                                </div>                                
                                <div class="ggp-sidebar edit-availability">
                                    <div class="edit-aoe">
                                        <h5>Availability</h5>
                                        <a href="javascript:;" data-toggle="modal" data-target="#availability"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    </div>
                    <ul class="ggp-availability">
            <?php 
              $modeTypeFaceToFace=$modeTypePhone=$modeTypeVideo='FALSE' ;
              $modeValue1=$modeValue2=$modeValue3='';
              if(isset($this->data['Communication'][0]['mode_type']) && $this->data['Communication'][0]['mode_type'] != '')
              {
                    
                    if($this->data['Communication'][0]['mode_type']=='face_to_face')
                {
                        $modeTypeFaceToFace='TRUE';
                        $modeValue1=$this->data['Communication'][0]['mode'];
                    }
                    else if($this->data['Communication'][0]['mode_type']=='phone')
                        {
                          $modeTypePhone='TRUE';
                          $modeValue2=$this->data['Communication'][0]['mode'];
                        }
                        
                        else
                        {
                           $modeTypeVideo='TRUE';
                           $modeValue3=$this->data['Communication'][0]['mode'];
                     }
                    
    
                
            }
              if(isset($this->data['Communication'][1]['mode_type']) && $this->data['Communication'][1]['mode_type'] != '')
              {
                    
                    if($this->data['Communication'][1]['mode_type']=='face_to_face')
                {
                        $modeTypeFaceToFace='TRUE';
                        $modeValue1=$this->data['Communication'][1]['mode'];
                    }
                    else if($this->data['Communication'][1]['mode_type']=='phone')
                        {
                           $modeTypePhone='TRUE';
                           $modeValue2=$this->data['Communication'][1]['mode'];
                        }
                        
                        else
                        {
                           $modeTypeVideo='TRUE';
                           $modeValue3=$this->data['Communication'][1]['mode'];
                     }
                    
        
                
            }
             if(isset($this->data['Communication'][2]['mode_type']) && $this->data['Communication'][2]['mode_type'] != '')
              {
                    if($this->data['Communication'][2]['mode_type']=='face_to_face')
                    {
                        $modeTypeFaceToFace='TRUE';
                        $modeValue1=$this->data['Communication'][2]['mode'];
                    }
                        else if($this->data['Communication'][2]['mode_type']=='phone')
                        {
                           $modeTypePhone='TRUE';
                           $modeValue2=$this->data['Communication'][2]['mode'];
                        }
                        
                        else
                        {
                           $modeTypeVideo='TRUE';
                           $modeValue3=$this->data['Communication'][2]['mode'];
                     }
                    
                    
              }
            
            
                 ?>     
                     <?php if($modeTypeFaceToFace=='TRUE') {?>
                        <?php if(trim($modeValue1)!='') : ?>                 
                        <li><i class="fa fa-map-marker" aria-hidden="true"></i><span id="viewfacetoface"><?php echo $modeValue1; ?><?php endif; } ?></span></li>
                     <?php if($modeTypeVideo=='TRUE') {?>
                        <?php if(trim($modeValue3)!='') : ?>    
                        <li><i class="fa fa-laptop" aria-hidden="true"></i><span id="viewvideo"><?php echo $modeValue3; ?><?php endif; }?></span></li>
                        
                     <?php if($modeTypePhone=='TRUE' && ($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==2)) {
                     if(trim($modeValue2)!=''){  ?> 
                        <li><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:<?php echo $modeValue2; ?>"><span id="viewphone"><?php echo $modeValue2; ?><?php }?></span></a></li>
                     <?php }else{
                         if(trim($modeValue2)!='' && $this->data['User']['extension'] !='') { ?>                         
                        <li><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:1-866-511-1898 x<?php echo $this->data['User']['extension']; ?>"><span>1-866-511-1898 x<?php echo $this->data['User']['extension']; ?></span></a></li>
                     <?php }else{?> 
                        <li><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:1-866-511-1898 x1111"><span>1-866-511-1898 x1111</span></a></li>
                     <?php }}?>     
                        <li><i class="fa fa-clock-o" aria-hidden="true"></i><span id="viewavailablity"><?php echo($this->data['Availablity']['day_time']); ?></span></li>
                    </ul>
                                </div>
                                <div class="ggp-sidebar">
                                    <div class="edit-aoe">
                                        <h5>Fee</h5>
                                        <a href="javascript:;" data-toggle="modal" data-target="#s-fee"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="edit-fee">
                                       <?php $options = array('1' => 'Hour', '2' => 'Month', '3' => 'Engagement', '4' => 'Day'); ?>
                                        <span class="doller-sign">$</span>
                                        <span class="slash-sign"><span id="viewengfee"><?php echo($this->data['UserReference']['fee_regular_session']);?></span> / <span id="viewengper"><?php echo($options[$this->data['UserReference']['regular_session_per']]);?></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Profile Tab Close -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Edit Profile Modal -->
                            <div class="modal fade member-edit-popup" id="edit_profile" tabindex="-1" role="dialog" aria-labelledby="edit_profile">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <h3 class="modal-title" id="myModalLabel">Profile Information</h3>
                                        </div>
                                        <form id="frmedit-profile-m">
                                            <div class="modal-body edit-profile-m">
                                                <div class="aoe-box">
                                                    <div class="edit-member">
                                                        <form class="" action="" method="">
                                                            <div class="form-group col-md-6">
                                                                <input type="text" name="firstname" class="form-control" value="<?php echo(ucfirst($this->data['UserReference']['first_name']));?>" placeholder="First Name" id="firstname">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <input type="text" name="lastname" class="form-control" value="<?php echo(ucfirst($this->data['UserReference']['last_name']));?>" placeholder="Last Name" id="lastname">
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <input type="text" name="zipcode" class="form-control" value="<?php echo(ucfirst($this->data['UserReference']['zipcode']));?>" placeholder="ZIP" id="zipcodevalue">
                                                            </div>
                                                            <div class="form-group col-md-12">
                                                                <span class="pull-right char-limit">(<span id="headlinenoOfChars"><?php echo(140 - strlen($this->data['UserReference']['linkedin_headline']))?></span> characters remaining)</span>
                                                                <textarea name="profileheadline" class="form-control" rows="4" id="profileheadline" placeholder="Your professional headline"><?php echo(ucfirst($this->data['UserReference']['linkedin_headline']));?></textarea>
                                                                    
                                                                
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="button-set">
                                                    <a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
                                                    <a href="javascript:void(0);" onclick="javascript:saveprofileinfo();"class="btn btn-primary">Save</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- aoe_edit Modal -->
                            <div class="modal fade member-edit-popup" id="aoe_edit" tabindex="-1" role="dialog" aria-labelledby="aoe_edit">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <h3 class="modal-title" id="myModalLabel">Industry - Category</h3>
                                        </div>
                                        <div class="modal-body aoe-edit">
                                            <div class="aoe-box">
                                                <div class="form-group col-sm-6 col-xs-12">
                                                    <?php echo $this->Form->select('UserReference.industry1',$industry_options,array('div' =>false,'class'=>'form-control','empty' => false,'value'=>$industry1));?>
                                                </div>
                                                <div class="form-group col-sm-6 col-xs-12">
                                                    <?php echo $this->Form->select('UserReference.category1',$category1_options,array('div' =>false,'class'=>'form-control','empty' => false,'value'=>$category1));?>
                                                </div>
                                            </div>
                                            <div class="aoe-box">
                                                <div class="form-group col-sm-6 col-xs-12">
                                                    <?php echo $this->Form->select('UserReference.industry2',$industry_options,array('div' =>false,'class'=>'form-control','empty' => false,'value'=>$industry2));?>
                                                </div>
                                                <div class="form-group col-sm-6 col-xs-12">
                                                    <?php echo $this->Form->select('UserReference.category2',$category2_options,array('div' =>false,'class'=>'form-control','empty' => false,'value'=>$category2));?>
                                                </div>
                                            </div>
                                            <div class="aoe-box">
                                                <div class="form-group col-sm-6 col-xs-12">
                                                    <?php echo $this->Form->select('UserReference.industry3',$industry_options,array('div' =>false,'class'=>'form-control','empty' => false,'value'=>$industry3));?>
                                                </div>
                                                <div class="form-group col-sm-6 col-xs-12">
                                                    <?php echo $this->Form->select('UserReference.category3',$category3_options,array('div' =>false,'class'=>'form-control','empty' => false,'value'=>$category3));?>
                                                </div>
                                            </div>
                                            <div class="button-set col-md-12">
                                                <a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
                                                <a href="javascript:void(0);" onclick="javascript:saveindustrycategory();" class="btn btn-primary">Save</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Topics Modal -->
                            <div class="modal fade member-edit-popup" id="aoe_topics" tabindex="-1" role="dialog" aria-labelledby="aoe_topics">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <h3 class="modal-title" id="myModalLabel">Topics</h3>
                                        </div>
                                        <div class="modal-body topics">
                                            <div class="aoe-box e-topics">
                                                <div class="form-group">
                                                    <span class="pull-right char-limit">(<span id="noOftopics"></span> topics remaining)</span>
                                                    <input type="text" name="data[City][keyword]" value="" class="form-control" placeholder="Choose topics" id="CityKeyword">
                                                    <input type="hidden" name="data[UserReference][select]" value="<?php echo ($topicarray);?>" id="hiddenSelect">
                                                </div>
                                                <div class="e-topics-box" id="div0" style="display: none;">
                                                    <span id='close' onclick='deleteThis(0); return false;'>x</span>
                                                    <p id="topic0" style="display: inline-block;"></p>
                                                </div>
                                                <div class="e-topics-box" id="div1" style="display: none;">
                                                    <span id='close' onclick='deleteThis(1); return false;'>x</span>
                                                    <p id="topic1" style="display: inline-block;"></p>
                                                </div>
                                                <div class="e-topics-box" id="div2" style="display: none;">
                                                    <span id='close' onclick='deleteThis(2); return false;'>x</span>
                                                    <p id="topic2" style="display: inline-block;"></p>
                                                </div>
                                                <div class="e-topics-box" id="div3" style="display: none;">
                                                    <span id='close' onclick='deleteThis(3); return false;'>x</span>
                                                    <p id="topic3" style="display: inline-block;"></p>
                                                </div>
                                                <div class="e-topics-box" id="div4" style="display: none;">
                                                    <span id='close' onclick='deleteThis(4); return false;'>x</span>
                                                    <p id="topic4" style="display: inline-block;"></p>
                                                </div>
                                            </div>
                                            <div class="button-set">
                                                <a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
                                                <a href="javascript:void(0);" onclick="javascript:savetopics();" class="btn btn-primary">Save</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Professional summary Modal -->
                            <div class="modal fade member-edit-popup" id="pro_summary" tabindex="-1" role="dialog" aria-labelledby="pro_summary">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <h3 class="modal-title" id="myModalLabel">Professional summary</h3>
                                        </div>
                                        <form id="frmedit-professionalsummary">
                                            <div class="modal-body topics">
                                                <div class="aoe-box">
                                                    <div class="form-group">
                                                        <span class="pull-right char-limit">(<span id="psummarynoOfChars"><?php echo(3000 - strlen($this->data['UserReference']['background_summary']));?></span> characters remaining)</span>
                                                        <textarea class="form-control" name="professionalsummary" id="professionalsummary" placeholder="Suggested format: Start with an overview. Then, explain your process and list past clients. In the end, mention related work, training & publications (3,000 characters limit)" rows="14"><?php echo(strip_tags($this->data['UserReference']['background_summary'])); ?></textarea>
                                                            
                                                        
                                                    </div>
                                                </div>
                                                <div class="button-set">
                                                    <a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
                                                    <a href="javascript:void(0);" onclick="javascript:saveprofessionalsummary();" class="btn btn-primary">Save</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Engagement overview Modal -->
                            <div class="modal fade member-edit-popup" id="e_overview" tabindex="-1" role="dialog" aria-labelledby="e_overview">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <h3 class="modal-title" id="myModalLabel">Engagement overview</h3>
                                        </div>
                                        <form id="frmedit-engoverview">
                                            <div class="modal-body topics">
                                                <div class="aoe-box">
                                                    <div class="form-group">
                                                        <span class="pull-right char-limit">(<span id="eoverviewnoOfChar"><?php echo(1000 - strlen($this->data['UserReference']['feeNSession']))?></span> characters remaining)</span>
                                                        <textarea name="engagementovierview" class="form-control" placeholder="What's included in a typical engagement? Indicate duration, methodology and pricing" rows="6" id="engagementovierview"><?php if($this->data['UserReference']['feeNSession']) { echo strip_tags($this->data['UserReference']['feeNSession']);} else {?> Duration and cost of an engagement depends on the scope of work, identified during the initial consultation.<?php }?></textarea>
                                                        
                                                    </div>
                                                </div>
                                                <div class="button-set">
                                                    <a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
                                                    <a href="javascript:void(0);" onclick="javascript:saveengagementovierview();" class="btn btn-primary">Save</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Clients overview Modal -->
                            <div class="modal fade member-edit-popup" id="e_clients" tabindex="-1" role="dialog" aria-labelledby="e_clients">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <h3 class="modal-title" id="myModalLabel">Clients</h3>
                                        </div>
                                        <div class="modal-body topics">
                                            <div class="aoe-box">
                                                <div class="form-group">
                                                    <textarea class="form-control" placeholder="List your past and present clients here. If a client name is confidential, indicate industry and size" rows="6" id="pastclients"><?php echo($this->data['UserReference']['past_clients']);?></textarea>
                                                    
                                                </div>
                                            </div>
                                            <div class="button-set">
                                                <a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
                                                <a href="javascript:void(0);" onclick="javascript:savepastclients();" class="btn btn-primary">Save</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Edit Social Links Modal -->
                            <div class="modal fade member-edit-popup" id="esl_link" tabindex="-1" role="dialog" aria-labelledby="esl_link">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <h3 class="modal-title" id="myModalLabel">Edit Social Links</h3>
                                        </div>
                                        <div class="modal-body esl-links">
                                            <div class="aoe-box">
                                                <div class="form-group w100per">
                                                    <input type="text" class="form-control" id="businesswebsite" placeholder="Business website (URL here)" value="<?php echo($this->data['UserReference']['business_website']);?>" />
                                                </div>
                                            </div>
                                            <div class="aoe-box">
                                                <div class="form-group w100per">
                                                    <input type="text" class="form-control" id="introvideo" placeholder="Intro video (embed link here)" value='<?php echo($this->data['UserReference']['intro_link']);?>' />
                                                </div>
                                            </div>
                                            <div class="aoe-box">
                                                <?php
                            $val='';
                            if(isset($this->data['Social'][0]['social_name']))
                            {
                                $val=$this->data['Social'][0]['social_name'];
                            }
                        ?>
                                                    <div class="form-group w100per">
                                                        <input type="text" class="form-control" id="sociallink1" name="" value="<?php echo($val);?>" placeholder="www.linkedin.com">
                                                    </div>
                                            </div>
                                            <div class="aoe-box">
                                                <?php
                            $val='';
                            if(isset($this->data['Social'][1]['social_name']))
                            {
                                $val=$this->data['Social'][1]['social_name'];
                            }
                        ?>
                                                    <div class="form-group w100per">
                                                        <input type="text" class="form-control" id="sociallink2" name="" value="<?php echo($val);?>" placeholder="www.twitter.com">
                                                    </div>
                                            </div>
                                            <div class="aoe-box">
                                                <?php
                            $val='';
                            if(isset($this->data['Social'][2]['social_name']))
                            {
                                $val=$this->data['Social'][2]['social_name'];
                            }
                        ?>
                                                    <div class="form-group w100per">
                                                        <input type="text" class="form-control" id="sociallink3" name="" value="<?php echo($val);?>" placeholder="www.facebook.com">
                                                    </div>
                                            </div>
                                            <div class="aoe-box">
                                                <?php
                            $val='';
                            if(isset($this->data['Social'][3]['social_name']))
                            {
                                $val=$this->data['Social'][3]['social_name'];
                            }
                        ?>
                                                    <div class="form-group w100per">
                                                        <input type="text" class="form-control" id="sociallink4" name="" value="<?php echo($val);?>" placeholder="www.amazon.com">
                                                    </div>
                                            </div>
                                            <div class="aoe-box">
                                                <?php
                            $val='';
                            if(isset($this->data['Social'][4]['social_name']))
                            {
                                $val=$this->data['Social'][4]['social_name'];
                            }
                        ?>
                                                    <div class="form-group w100per">
                                                        <input type="text" class="form-control" id="sociallink5" name="" value="<?php echo($val);?>" placeholder="www.hbr.org">
                                                    </div>
                                            </div>
                                            <div class="button-set">
                                                <a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
                                                <a href="javascript:void(0);" onclick="javascript:savesociallinks();" class="btn btn-primary">Save</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Availability Links Modal -->
                            <div class="modal fade member-edit-popup" id="availability" tabindex="-1" role="dialog" aria-labelledby="availability">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <h3 class="modal-title" id="myModalLabel">Availability</h3>
                                        </div>
                                        <form id="frmeditavailability">
                                            <div class="modal-body availability">
                                                <div class="aoe-box">
                                                    <div class="form-group col-md-12">
                                                        <h5>Availability</h5>
                                                        <textarea class="form-control" rows="3" name="availablitytime" id="availablitytime" placeholder="e.g.Business hours (ET). By appointment"><?php echo($this->data['Availablity']['day_time']);?></textarea>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h5>Communication</h5>
                                                        <ul>
                                                            <li class="styled-form">
                                                                <div class="form-group">
                                                                    <input id="a1" class="group1" type="checkbox" name="communication1checkbox" value="face_to_face" <?php echo($facecm); ?> />
                                                                    <label for="a1">In person</label>
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" name="communication1" id="communication1" value="<?php echo ($facevalue)?>" placeholder="e.g. New York, NY">
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="styled-form">
                                                                <div class="form-group">
                                                                    <input id="a2" class="group1" type="checkbox" name="communication2checkbox" value="phone" <?php echo($phonecm); ?> />
                                                                    <label for="a2">Phone</label>
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" name="communication2" id="communication2" value="<?php echo ($phonevalue)?>" placeholder="Business phone number">
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li class="styled-form">
                                                                <div class="form-group">
                                                                    <input id="a3" class="group1" type="checkbox" name="communication3checkbox" value="video" <?php echo($videocm); ?> />
                                                                    <label for="a3">Online</label>
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control" name="communication3" id="communication3" value="<?php echo ($videovalue)?>" placeholder="e.g. Skype, G+ Hangout">
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="button-set">
                                                    <a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
                                                    <a href="javascript:void(0);" onclick="javascript:void(0);" id="btnsaveavailability" class="btn btn-primary">Save</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- Fee Modal -->
                            <div class="modal fade member-edit-popup" id="s-fee" tabindex="-1" role="dialog" aria-labelledby="s-fee">
                                <div class="modal-dialog s-fee" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            <h3 class="modal-title" id="myModalLabel">Fee</h3>
                                        </div>
                                        <form id="frmedit-fee">
                                            <div class="modal-body availability">
                                                <div class="aoe-box">
                                                    <div class="form-group col-md-12">
                                                        <h5>Fee</h5>
                                                        <div class="edit-fee">
                                                            <span class="doller-sign">$</span>
                                                            <input type="text" class="form-control" name="consultationfee" id="engfee" value="<?php echo($this->data['UserReference']['fee_regular_session']);?>" placeholder="Fee">
                                                            <span class="slash-sign">/</span>
                                                            <select class="form-control" value="<?php echo($this->data['UserReference']['regular_session_per']);?>" id="engper">
                                                                <option value="1">Hour</option>
                                                                <option value="2">Month</option>
                                                                <option value="3">Engagement</option>
                                                                <option value="4">Day</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="button-set">
                                                    <a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
                                                    <a href="javascript:void(0);" onclick="javascript:saveengfee();" class="btn btn-primary">Save</a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="editprofilesuccess" value="1" id="editprofilesuccess" />
                            <input type="hidden" name="editprofilesummarysuccess" value="1" id="editprofilesummarysuccess" />
                            <input type="hidden" name="editengoverviewsuccess" value="1" id="editengoverviewsuccess" />
                            <input type="hidden" name="editavailabilitysuccess" value="1" id="editavailabilitysuccess" />
                            <input type="hidden" name="editfeesuccess" value="1" id="editfeesuccess" />
                            <!-- js link -->
                            <!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
                            <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
                            <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>-->
                            <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
                            <!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>-->
                            <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>

                            <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/slick.min.js"></script>
                            <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap-notify.min.js"></script>
                            <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/quill.min.js"></script>
                            <script src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrapValidator.min.js" type="text/javascript"></script>
                            <script type="text/javascript" src="https://js.hscollectedforms.net/collectedforms.js"></script>
                            <script type="text/javascript" src="<?php echo(SITE_URL)?>sanchit/js/profileeditscript.js"></script>
                            <style>
                            .ui-autocomplete {
                                position: absolute;
                                cursor: default;
                                z-index: 9999999!important;
                            }

                            * html .ui-autocomplete {
                                width: 1px;
                            }

                            .ui-menu {
                                list-style: none;
                                padding: 2px;
                                margin: 0;
                                display: block;
                                float: left;
                                border: #dedede solid 1px;
                                background: #f8f8f8;
                                max-height: 265px;
                                overflow-x: hidden;
                            }

                            .ui-menu .ui-menu {
                                margin-top: -3px;
                            }

                            .ui-menu .ui-menu-item {
                                margin: 0;
                                padding: 0;
                                zoom: 1;
                                float: left;
                                clear: left;
                                width: 100%;
                            }

                            .ui-menu .ui-menu-item a {
                                text-decoration: none;
                                display: block;
                                padding: .2em .4em;
                                line-height: 1.5;
                                zoom: 1;
                                font-size: 15px;
                                margin: 0 0 2px 0;
                                color: #434343;
                            }

                            .ui-menu .ui-menu-item a.ui-state-hover,
                            .ui-menu .ui-menu-item a.ui-state-active {
                                font-weight: normal;
                            }

                            .ui-autocomplete-loading {
                                background: url(../img/ajax/circle_ball.gif)250px 5px no-repeat!important;
                            }
                            </style>