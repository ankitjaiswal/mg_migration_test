
<?php
$email=''; $pass=''; $checked='false';
if(isset($_SESSION['menteeActEmail']) && $_SESSION['menteeActEmail']!=''):
	$email = $_SESSION['menteeActEmail'];
	unset($_SESSION['menteeActEmail']);
endif;
if(isset($_COOKIE['CakeCookie']['email']))
{
	$email = $_COOKIE['CakeCookie']['email'];
	$checked = "true";
}
if(isset($_COOKIE['CakeCookie']['password']))
	$pass = $_COOKIE['CakeCookie']['password']; ?>
<?php echo($this->Html->css(array('regist_popup'))); ?>

<div id="registerationFm" style="width:100%;">
	<h1>Login</h1>
		<?php echo($this->Form->create('User',array('url'=>array('controller'=>'users','action'=>'login'),'id'=>'loginForm'))); ?>
		<?php
		if(isset($this->data['User']['mentor_id'])){
			//if mentee come here from apply for mentorship
			echo($this->Form->hidden('User.mentor_id'));		
		}?>		
		
		<?php
		if(isset($this->data['User']['question_url'])){
			//if mentee come here from apply for mentorship
			echo($this->Form->hidden('User.question_url', array('id'=>'question_url', 'value'=>$this->data['User']['question_url'])));		
		}?>				
		
		<table style="padding:0px;padding-top:10px;border:0px solid red;width:100%;">
			
			<tr>
				<td colspan="2">
				<div id="errorMessage" class="errormsg"></div>
				<div id="login-account">
				<?php echo($this->Form->input('username',array('class'=>'forminput','id'=>'loginEmail','label'=>false,'div'=>false,'placeholder'=>'Email','value'=>$email)));?>	
				<div id="LoginEmailErr"  class="errormsg"></div>
				</div>			
				</td>
			</tr>
			<tr>
				<td colspan="2">
				<div id="login-account">
                 	<?php echo($this->Form->password('password',array('class'=>'input','label'=>false,'div'=>false,'placeholder'=>'Password', 'value'=>$pass)));?>	
                	<div id="LoginPassErr"  class="errormsg"></div>             
     			</div>
				</td>
			</tr>
			<tr>
				<td>
				<?php echo($this->Form->submit('Login',array('class'=>'btn','onclick'=>'return validateLogin();')));?>
				 </td>  
				
			</tr>
			<tr>
				<td colspan="2" style="text-align:right;"><?php echo($this->Html->link('Forgot password?','javascript:forgotPopup();',array('class'=>'needAcc','style'=>'font-weight:normal;'))); ?></td>
			</tr>	
			<tr>
				<td colspan="2">
					<?php echo($this->Form->end()); ?>
			<div class="floatL"></div>
			<div class="line" style="margin-top:4px !important;border-top:1px solid #BEBEBE;clear:both;"></div>
			<?php
			if(isset($this->data['User']['question_url'])){?>
				<div class="floatL" style="width:100%;">&nbsp;&nbsp;&nbsp;<?php echo($this->Html->image('linkedin.jpg',array('alt'=>'Linked in',"onclick"=>"redirect_q();",'style'=>'cursor:pointer;'))); ?></div>
	
			<?php } else {?>
                       <div class="floatL" style="width:100%;">&nbsp;&nbsp;&nbsp;<?php echo($this->Html->image('linked-in.png',array('alt'=>'Linked in',"onclick"=>"linkedin();",'style'=>'cursor:pointer;'))); ?></div>
                       <?php }?>
			<div class="line1" style="border-bottom:1px solid #BEBEBE;clear:both;"></div>
		
		<div class="floatR" style="padding-top:25px;">Need an account? <?php echo($this->Html->link('Register here','javascript:register_popup();',array('class'=>'needAcc','style'=>'font-weight:normal;')));?></div>
				</td>
			</tr>
		</table>
		
</div>
<div style="clear:both;"></div>
<style type="text/css">
.submit{
	padding: 6px 0 !important;
}
</style>
<script type="text/javascript">
jQuery(document).ready(function(){

jQuery("#UserPassword").keyup(function(event){
    if(event.keyCode == 13){
        $("#btn").click();
    }
});

});
 </script>
<?php


?>