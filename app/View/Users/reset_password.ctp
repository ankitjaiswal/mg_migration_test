<?php //e($html->css(array('regist_popup'))); 
$error = ''; $succ='';
if(isset($_SESSION['forgot_error']))
{
	$error  = $_SESSION['forgot_error'];
	unset($_SESSION['forgot_error']);
}?>
<?php if($error!=''){ ?>
<!--<div id="registerationFm" style="width:37%; padding:20px; margin-left:280px;">
	<h1 style="margin-bottom:14px; padding-bottom:8px; color:#990000;"></h1>
</div>
--><div id="inner-content-wrapper" style="height: 400px">
    <div id="inner-content-box" class="pagewidth">
        <div id="user">
            <h1 style="color:#000;font-size:15px;">
                <?php $this->Layout->sessionFlash(); ?>
            </h1>
        </div>
        <div style="padding: 20px 50px;color:#000;font-size:15px;">
            <h1 style="color:#000;font-size:15px;">
                <?php echo $error; ?>
            </h1>
        </div>
        <div id ="app-button">		
            <?php echo($this->Form->create('User', array('url' => '/','method'=>'link')));?>     
                <input type="submit" value="Done" id="submit"/>
            <?php echo($this->Form->end()); ?>
        </div>
    </div>
</div>
<?php } 
if(isset($_SESSION['forgot_success']) && $_SESSION['forgot_success']=='success')
{ 
	$succ = 'forgot_succ';
	unset($_SESSION['forgot_success']);
?>
<div id="inner-content-wrapper" style="height: 400px">
    <div id="inner-content-box" class="pagewidth">
        <div id="user" style="margin-left:53px;">
            <h1>
                
            </h1>
        </div>
        <div style="padding: 20px 50px;color:#000;font-size:15px;">
            <h1 style="color:#000;font-size:15px;">
               Your password has been successfully changed.<br/><br/>
            </h1>
        </div>
        <div id ="app-button">		
            <?php echo($this->Form->create('User', array('url' => '/','method'=>'link')));?>     
                <input type="submit" value="Done" id="submit"/>
            <?php echo($this->Form->end()); ?>
        </div>
    </div>
</div>

<?php }
if($error=='' && $succ==''){ ?>
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div id="user">
            <div id="pro-creation">
                <h1>Set your password</h1>
            </div>
        <?php echo($this->Form->create('User',array('url'=>array('controller'=>'users','action'=>'reset_password'),'id'=>'resetPassForm'))); ?>   
		<?php echo $this->Form->hidden('id',array('value'=>$id));?> 
                <div id="name-box">  
				                
                    <div id="country">
                        <div class="floatL">
				<?php 	echo($this->Form->password('newpass',array('class'=>'forminput','maxlengh'=>20,'id'=>'newPass','label'=>false,'div'=>false,"class" => "forminput1",'placeholder'=>'New Password')));?>
						
						</div>
						
                        <div class="floatL mr-lt20">
                            <?php
							 echo($this->Form->password('confpass',array('class'=>'forminput','maxlengh'=>20,'id'=>'confPass','label'=>false,'div'=>false,"class" => "forminput",'placeholder'=>'Confirm password'))); ?>
							                       
						</div>
						<div id="errorMessage" class="errormsg" style="display:none;"></div> 
						<div class="clear"></div>
                    </div>
                    <div id ="app-button" style="padding-bottom: 45px;width: 700px;">						
                       <?php echo($this->Form->submit('Submit',array('class'=>'btn','onclick'=>'return validateReset();')));?>
                    </div>		
                </div>
            <?php echo($this->Form->end()); ?>
        </div>
    </div>
</div>
<?php } ?>
