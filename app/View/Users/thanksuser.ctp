<div id="inner-content-wrapper" style="height: 400px">
	<div id="inner-content-box" class="pagewidth">
		<div id="user">
	<?php 
		if(isset($MenteeData) && count($MenteeData)>0): ?>
			<div id="upload-resume">
				<h1 style="color:#000;font-size:15px;">
				<?php if($MenteeData['isLinkedin'] == 0){ ?>
                A verification mail has been sent to your account.
                <?php }else {?>
                A welcome mail has been sent to your account.
                <?php }?>
            	</h1>
			 <div style="padding: 20px 0px;color:grey;font-size:15px;">
          	 	<h1 style="font-size:15px;color:#000;">
             	   Thanks for your interest!
            	</h1>
       		 </div>	
				
			</div>
			<div id ="app-button">
			<?php
			echo ($this->Form->create('User',array('url'=>array('controller'=>'clients','action'=>'resend_email'))));?>
					<?php echo ($this->Form->hidden('id',array('value'=>$MenteeData['User']['id']))); ?>
					<?php if($MenteeData['isLinkedin'] == 0){ ?>
					<?php echo ($this->Form->submit('Resend email',array('id'=>'submit','style'=>'width:135px;'))); echo ($this->Form->end()); ?>
					<?php } else {?>
						  <input id="submit" type="button" value="Done" style="width:135px;" onclick="window.location.href='<?php echo SITE_URL ?>'; "></input>
					<?php }?>
			</div>
		<?php 
		else: ?>
			<div id="upload-resume">
				<p>
				Your application to become a client has been submitted
				</p>
				<p>Thanks for your interest!</p>
			</div>
			<div id ="app-button">
			<?php echo ($this->Form->create('Null',array('url'=>array('controller'=>'fronts','action'=>'index'))));?>
					<?php echo ($this->Form->submit('Done',array('id'=>'submit'))); echo ($this->Form->end()); 
				endif;
				?>
			</div	
		></div>
	</div>
</div>