<?php echo($this->Html->css(array('regist_popup'))); ?>
<div id="registerationFm" style="width:100%;">
	<h1 style="margin-bottom:14px; padding-bottom:8px;">Forgot your password?</h1>
	<span style="color:#888888;">Submit your email address and we'll send you a link to reset your password.</span>
		<?php echo($this->Form->create('User',array('url'=>array('controller'=>'users','action'=>'forgot'),'id'=>'forgotForm'))); ?>
		
		<table style="padding:0px;padding-top:10px;border:0px solid red;width:100%;">
			
			<tr>
				<td colspan="2">
				<div class="input_box">
				<?php 
				
				echo($this->Form->input('username',array('class'=>'forminput','id'=>'forgotEmail','label'=>false,'div'=>false,'placeholder'=>'Email')));
				?>	
				</div>	
				<div id="ForgotEmailErr" class="errormsg"></div>		
				</td>
			</tr>
			<tr>
				<td style="float:left;">
				<?php echo($this->Form->submit('Submit',array('class'=>'btn','onclick'=>'return validateforgot();')));?>
				
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<?php echo($this->Form->end()); ?>
			<div class="floatL"></div>
			</td>
			</tr>
		</table>
		
</div>
<div style="clear:both;"></div>