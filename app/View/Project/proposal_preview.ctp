<script type="text/javascript">



Array.prototype.remove = function() {
    var what, a = arguments,
        L = a.length,
        ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

var topicVal = [-1, -1, -1];
var topicCount = 0;

function monkeyPatchAutocomplete() {

    jQuery.ui.autocomplete.prototype._renderItem = function(ul, item) {
        var re = new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + this.term + ")(?![^<>]*>)(?![^&;]+;)", "gi");
        var t = item.label.replace(re, "<span style='font-weight:bold;color:#434343;'>" +
            "$&" +
            "</span>");
        return jQuery("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + t + "</a>")
            .appendTo(ul);
    };
}

function getKeywords() {

    var allKeywords = <?php echo json_encode($allKeywords); ?>;

    return allKeywords;
}

function deleteThis(element) {
    var topic = jQuery('#noOftopics').html();
    topicVal[element] = -1;

    for (var i = 0; i < 3; i++) {

        if (topicVal[i] == -1) {
            jQuery('#div' + i).css('display', 'none');
            jQuery('#topic' + i).html('');
            jQuery("#noOftopics").html(parseInt(topic) + parseInt(1));
        } else {
            jQuery('#div' + i).css('display', 'inline-block');
            jQuery('#topic' + i).html(topicVal[i]);
            jQuery("#noOftopics").html(parseInt(topic) + parseInt(1));
        }
    }
}

jQuery(document).ready(function() {
    jQuery("#noOftopics").html(3);
    var hidVal = jQuery('#hiddenSelect').val();
    if (hidVal != '') {
        var hidValArr = hidVal.split(",");
        var total = 0;
        for (var i = 0; i < hidValArr.length; i++) {
            topicVal[i] = hidValArr[i];
            jQuery('#div' + i).css('display', 'inline-block');
            jQuery('#topic' + i).html(hidValArr[i]);
            total++;
        }
        jQuery("#noOftopics").html(3 - total);
    }
    monkeyPatchAutocomplete();

    var CityKeyword = jQuery('#CityKeyword');

    CityKeyword.autocomplete({
        minLength: 1,
        source: getKeywords(),
        source: function(request, response) {

            var results = jQuery.ui.autocomplete.filter(getKeywords(), request.term);

            for (var x = 0; x < 3; x++) {
                if (topicVal[x] != -1)
                    results.remove(topicVal[x]);
            }

            results = results.slice(0, 3);
            results.push('Add <b>"' + jQuery("#CityKeyword").val() + '"</b>');
            response(results); //show 3 items.
        },
        focus: function(event, ui) {

            if (ui.item.value[0] == "A")
                return false;

        },
        select: function(event, ui) {
            //var t = jQuery("#noOftopics").val();
            var toAdd = ui.item.value;
            if (ui.item.value[1] == "d") {
                var total = toAdd.length;
                var final = total - 5;
                toAdd = toAdd.substring(8, final);
            }

            var changed = -1;

            for (var i = 0; i < 3; i++) {

                if (topicVal[i] == -1) {
                    changed = 1;

                    topicVal[i] = toAdd;
                    jQuery("#noOftopics").html(2 - i);
                    break;
                }
            }

            if (changed == -1)
                alert("You can select upto 3 topics for industry, function or skills");

            for (var i = 0; i < 3; i++) {

                if (topicVal[i] == -1) {
                    jQuery('#div' + i).css('display', 'none');
                    jQuery('#topic' + i).html('');

                } else {
                    jQuery('#div' + i).css('display', 'inline-block');
                    jQuery('#topic' + i).html(topicVal[i]);

                }

            }

            jQuery("#CityKeyword").val('');

            return false;
        }
    });
});

</script>
<?php if(isset($user['UserImage'][0]['image_name']) && $user['UserImage'][0]['image_name'] !=''){
      $image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$user['User']['id'].DS.$user['UserImage'][0]['image_name'];
 } else {
     $image_path = SITE_URL.'img/media/profile.png';
}?>
    <div id="proposal-page">
        <div class="container">
            <div class="col-md-12 pp-page">
                               <form action="<?php echo SITE_URL ?>project/finalize_proposal/<?php echo($proposal['Proposal']['id']);?>" method="post" id="Addform">
                               <?php echo($this->Form->hidden('Proposal.id'));?>
				<div class="pp-heading text-center">
					<h5>DRAFT PROPOSAL: <?php echo($proposal['Proposal']['proposal_title']);?></h5>
				</div>

                <div class="pp-profile">
                    <div class="pp-pic">
                        <a href="<?php echo(SITE_URL.strtolower($user['User']['url_key']));?>"><img src="<?php echo($image_path);?>" alt="<?php echo($user['UserReference']['first_name']." ".$user['UserReference']['last_name']);?>"></a>
                    </div>
                    <div class="pp-rightside">
                        <h5><a href="<?php echo(SITE_URL.strtolower($user['User']['url_key']));?>" title="<?php echo($user['UserReference']['first_name']." ".$user['UserReference']['last_name']);?>"><?php echo($user['UserReference']['first_name']." ".$user['UserReference']['last_name']);?></a>
                           <?php if($user['UserReference']['guarantee'] == 'Y') {?>

                           <span class="gg-img">
                                <a href="<?php echo(SITE_URL);?>/fronts/guarantee" target="_blank"><img src="<?php echo(SITE_URL)?>yogesh_new1/images/gurantee.png" alt="Client Satisfaction Guarantee">
                            </span>
                         <?php }?>

                        </h5>
                        <ul class="list-unstyled">
                            <li>

                                       <?php if($city != '') {
                                         $citylink = str_replace(" ","-",$city);
                                         $keyword = "browse-all";?>
					<a href="<?php echo(SITE_URL.'browse/search_result/all-industries/all-categories/'.$keyword.'/'.$state.'/'.$citylink);?>" ><?php echo($city) ;?>, <?php echo($state) ;?></a>
				<?php } else {
					echo($this->data['UserReference']['zipcode']);
				}?>

                            </li>
                            <li class="dashTo">|</li>
                         <?php
				$disp="guild.im/".strtolower($user['User']['url_key']);
				$completelink=SITE_URL.strtolower($user['User']['url_key']);
				?>
                            <li><a href = "<?php echo($completelink); ?>" ><?php echo($disp); ?></a></li>
                        </ul>
                        <label class="ll-data"><?php echo(ucfirst($user['UserReference']['linkedin_headline']));?></label>
                     </div>
                    </div>

				<div class="project-overview">

					<div class="pp-label">
						<h5>Client Impact</h5>
						<p><?php {
                                $editthis = '<div class="ql-editor" contenteditable="true"';
                                $editby= '<div class="ql-editor" contenteditable="false"';
                                $proposaldetails = str_replace($editthis, $editby, $proposal['Proposal']['proposal_details']);

                                $editthis1 = 'target="_blank" style="color: rgba(0, 0, 0, 0.8);"';
                                $editby1 = 'target="_blank" style="color: #D03135;"';
                                $proposaldetails = str_replace($editthis1, $editby1, $proposaldetails);
                                echo(nl2br(htmlspecialchars_decode($proposaldetails)));}?>
                                                </p>
					</div>
					<div class="pp-label">
						<h5>Engagement Overview</h5>
						<p><?php {

                                $editthis = '<div class="ql-editor" contenteditable="true"';
                                $editby= '<div class="ql-editor" contenteditable="false"';
                                $proposalapproach = str_replace($editthis, $editby, $proposal['Proposal']['proposal_approach']);

                                $editthis1 = 'target="_blank" style="color: rgba(0, 0, 0, 0.8);"';
                                $editby1 = 'target="_blank" style="color: #D03135;"';
                                $proposalapproach = str_replace($editthis1, $editby1, $proposalapproach);
                                echo(nl2br(htmlspecialchars_decode($proposalapproach)));}?>
                                              </p>
					</div>
					<div class="pp-label">
						<h5>Relevant Background</h5>
						<p><?php {

                                $editthis = '<div class="ql-editor" contenteditable="true"';
                                $editby= '<div class="ql-editor" contenteditable="false"';
                                $proposalsuccess = str_replace($editthis, $editby, $proposal['Proposal']['proposal_success']);

                                $editthis1 = 'target="_blank" style="color: rgba(0, 0, 0, 0.8);"';
                                $editby1 = 'target="_blank" style="color: #D03135;"';
                                $proposalsuccess = str_replace($editthis1, $editby1, $proposalsuccess);
                                echo(nl2br(htmlspecialchars_decode($proposalsuccess)));}?> 
                                              </p>
					</div>

			<div class="form-group" data-placement="right" data-toggle="DescPopover" data-container="body">
                        <h5>Add Topics</h5>
			<input type="text" id="CityKeyword" class="form-control" placeholder="Add topics for industry, function or skills" name="data[City][keyword]">

			</div>
	  		<div id="div0" class="form-group" style="display: none; border-bottom:1px solid #BEBEBE; background:#ccc; border-radius:5px;">
		        <span id='close' onclick='deleteThis(0); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
		        <p id="topic0" style="padding-top: 10px; padding-bottom: 0px;padding-left:8px; display: inline-block"></p>
			</div>
			<div id="div1" class="form-group" style="display: none; border-bottom:1px solid #BEBEBE;; background:#ccc; border-radius:5px;">
		        <span id='close' onclick='deleteThis(1); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
		        <p id="topic1" style="padding-top: 10px; padding-bottom: 0px;padding-left:8px; display: inline-block""></p>
			</div>
			<div id="div2" class="form-group" style="display: none; border-bottom:1px solid #BEBEBE; background:#ccc; border-radius:5px;">
		        <span id='close' onclick='deleteThis(2); return false;' style="float:right; display:inline-block; padding:2px 5px; background:#ccc; cursor: pointer; border-radius:5px;">x</span>
		        <p id="topic2" style="padding-top: 10px; padding-bottom: 0px;padding-left:8px; display: inline-block""></p>
			</div>

                   <?php 
                        $topic = array();

                       if($proposal['ProposalTopic'][0]['topic_id']!='' && $proposal['ProposalTopic'][1]['topic_id']!='' && $proposal['ProposalTopic'][2]['topic_id']!='') {
                            
                            $topic =  $categories[$proposal['ProposalTopic'][0]['topic_id']].",".$categories[$proposal['ProposalTopic'][1]['topic_id']].",".$categories[$proposal['ProposalTopic'][2]['topic_id']];

                       } else if($proposal['ProposalTopic'][0]['topic_id']!='' && $proposal['ProposalTopic'][1]['topic_id']!='' && $proposal['ProposalTopic'][2]['topic_id']=='') {

                           $topic =  $categories[$proposal['ProposalTopic'][0]['topic_id']].",".$categories[$proposal['ProposalTopic'][1]['topic_id']];

                       } else if($proposal['ProposalTopic'][0]['topic_id']!='' && $proposal['ProposalTopic'][1]['topic_id']=='' && $proposal['ProposalTopic'][2]['topic_id']=='') {
                          
                           $topic =  $categories[$proposal['ProposalTopic'][0]['topic_id']];

                       } else{
                       
                           $topic ='';
                        }
                            
                       ?>
                       
                     

                      
                    <?php echo($this->Form->hidden('Proposal.select',array('value'=>$topic, 'id'=>'hiddenSelect')));?>



			</div>
			</div>

                       

				<div class="e-button pull-right" style="margin-bottom:60px;">
					<label>
						<a href="<?php echo SITE_URL?>project/proposal_edit/<?php echo $proposal['Proposal']['id']; ?>" style="margin-right:20px;">
							Edit
						</a>
					</label>
					<button class="banner-getstarted btn btn-default get-started" id="mgButton">
						Post your proposal
					</button>
				</div>

                       </form>
			

		

	</div>
       </div>  
          
	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/script6.js"></script>	

<script type="text/javascript">
jQuery(document).ready(function(){
	jQuery("#mgButton").click(function(){


    	var flag = 0;

		var myValues;
	
		for (var i = 0 ; i < 3 ; i++) {
			
			if(topicVal[i] != "-1") {
				if( i == 0)
					myValues = topicVal[i];
				else
					myValues = myValues + "," + topicVal[i];
			}
			
		}
		
		jQuery('#hiddenSelect').val(myValues);

		if(flag > 0) {
			return false;
		} else {
			jQuery("#Addform").submit();
			return true;
		}
    });
});
</script>
<style>
.ql-editor {
padding:0px;
}
.form-group {
    margin-bottom: 50px;
    margin-left: 30px;
    margin-top: 25px;
}
a {
    color: #d03135;
    
}
</style>