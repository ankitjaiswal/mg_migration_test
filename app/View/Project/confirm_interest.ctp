<div class="comman-page-boby test7-page">
	<div class="content-wrap">
		<div class="container">
			<h2>Confirm</h2>
			<p>GUILD has developed consulting relationships with various Clients who require specialized expertise from time to time. GUILD converts these requirements into consulting Projects and engages independent contractors with relevant expertise to execute these Projects.</p>

			<div class="styled-form styled-form2">
				<input type="checkbox" class="" name="" id="test7-checkbox1" placeholder="">
				<label for="test7-checkbox1">I understand all discussion with GUILD and Clients will be confidential in nature. I agree to maintain confidentiality of these discussions whether or not I am engaged by GUILD on this specific Project.</label>
			</div>
			<div class="styled-form">
				<input type="checkbox" class="" name="" id="test7-checkbox2" placeholder="">
				<label for="atest7-checkbox2">I shall not agree, nor entice any Client to agree to any terms, not established by GUILD, without the prior written approval from GUILD.</label>
			</div>

			<div class="submitprofile-full">
				<div class="pagewidth">
					<a class="confirm_interest_btn submitProfile confirm-interest-btn" href="javascript:void(0);">
						<img src="<?php echo SITE_URL?>/yogesh_new1/images/linked-in.png" alt="Linked in" style="cursor:pointer; float: right;"> 
						<div class="submitprofile-box">
							<span>Click to complete confirmation process </span>
							<img class="down-a" src="<?php echo SITE_URL?>/yogesh_new1/images/arrow-r.png">
						</div>
					</a>
				</div>
				<div class="text-info">GUILD will use LinkedIn to validate your identity. GUILD will <b style="font-weight: bolder; font-family: Ubuntu,arial,vardana;">never</b> post to your LinkedIn account without your permission.</div>
			</div>
		</div>
	</div>

	<div class="modal fade claimmodal member-edit-popup confirm_interest_modal" id="confirm_interest_modal" tabindex="-1" role="dialog" aria-labelledby="claimmodal" style="display: none;">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<h3 class="modal-title" id="myModalLabel">Confirm</h3>
				</div>
				<form id="frmedit-profile-m">
					<div class="modal-body edit-profile-m">
						<div class="col-sm-12 form-group">
							<p>Please check the boxes to proceed</p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>




<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
	        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/script6.js"></script>

<script type="text/javascript">
	jQuery(document).ready(function() {


        jQuery(".confirm-interest-btn").click(function(){
        	if(jQuery('#test7-checkbox2').prop("checked") == false){
        		jQuery("#confirm_interest_modal").modal();	
        	}

        	if(jQuery('#test7-checkbox1').prop("checked") == false){
        		jQuery("#confirm_interest_modal").modal();	
        	}

                if(jQuery('#test7-checkbox2').prop("checked") == true && jQuery('#test7-checkbox1').prop("checked") == true){
                 window.location.href = SITE_URL+'linkedin/OAuth2.php';

                 }

        });
        
	});

</script>