<div class="page-content">
    <div class="container">
        <div class="row">
	        <div class="col-sm-12">
	        	<h2 class="title-h2">Invoice Create</h2>
	        </div>
            <div class="col-sm-8">

               <form id="Invoiceform" enctype="multipart/form-data" method="post" action="" accept-charset="utf-8">
                <div class="form-group">
                    <label>Project name</label>
                    <input class="form-control" type="hidden" id="invoiceno" name="data[ProjectInvoice][invoiceno]" value="<?php echo($invoiceno);?>"/>
                    <input class="form-control" type="hidden" id="projectid" name="data[ProjectInvoice][projectid]" value="<?php echo($projectid);?>"/>
                    <input class="form-control" type="text" id="inv_title" name="data[ProjectInvoice][inv_title]" value="<?php echo($project['Project']['title']);?>"/>
                </div>
                <div class="form-group">
                    <label>Invoice description</label>
                    <textarea class="form-control" rows="7" id="inv_description" name="data[ProjectInvoice][inv_description]"></textarea>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Client name</label>
                            <input class="form-control" type="name" id="inv_client" name="data[ProjectInvoice][client_name]" value="<?php echo($clientData['UserReference']['first_name']." ".$clientData['UserReference']['last_name']);?>"/>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Client email</label>
                            <input class="form-control" type="email" id="inv_clientemail" name="data[ProjectInvoice][client_email]" value="<?php echo($clientData['User']['username']);?>"/>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>From</label>
                            <input class="form-control" type="text" id="inv_from" name="data[ProjectInvoice][inv_from]" value="<?php echo($memberData['UserReference']['first_name']." ".$memberData['UserReference']['last_name']);?>"/>
                        </div>
                    </div>
                </div>
                <div class="text-right button-set right-btnset mt-30 hidden-xs">
                    <a href="<?php echo(SITE_URL)?>/project/index/<?php echo($project['Project']['project_url']);?>" class="reset" style="">Cancel</a>
                    <input id="projectid" value="<?php echo($project['Project']['id']);?>" type="hidden" />
                    <input class="btn btn-primary" value="Preview"  id="previewlink" type="submit" />
                </div>
            </div>
            <div class="col-sm-4">
                <div class="invoice-box">
                    <h5>New invoice</h5>
                    <ul>
                        <li>
                            <label>Amount</label>
                            <span><input type="text" name="data[ProjectInvoice][inv_amount]" value="" id="pricetxt"/></span>
                        </li>
                        <li>
                            <label>Discount</label>
                            <span><input type="text" value="" name="data[ProjectInvoice][inv_discount]" id="discount" onfocus="if (this.value == '0.00') {this.value = '';}"/></span>
                        </li>
                        <li>
                            <label>Subtotal</label>
                            <input type="hidden" value="$0.00" id="subtotalhidden" name="data[ProjectInvoice][inv_subtotal]" readonly/>
                            <span class="total" id="subtotal" >$0.00</span>
                        </li>
                        <li>
                            <label>Tax</label>
                            <span><input type="text" value="" name="data[ProjectInvoice][inv_tax]" id="tax" onfocus="if (this.value == '0.00') {this.value = '';}"/></span>
                        </li>
                        <li>
                            <label>Total</label>
                            <span class="total" id="total">$0.00</span>
                        </li>
                    </ul>
                </div>
                <div class="text-right button-set right-btnset mt-30 visible-xs">
                    <a href="<?php echo(SITE_URL)?>/project/index/<?php echo($project['Project']['project_url']);?>" class="reset" style="">Cancel</a>
                    <input id="projectid" value="<?php echo($project['Project']['id']);?>" type="hidden" />
                    <input class="btn btn-primary" value="Preview"  id="previewlink" type="submit" />
                </div>
             </form>
            </div>
        </div>
    </div>
</div>

            <?php 
            	if($this->Session->read('Auth.User.mentor_type')=='Founding Member'):
					$rate = Configure::read('FounderMentorCommission'); 
					$MGFee = ($price)*($rate)/100;
					$myshare = $price - $MGFee;
				elseif($this->Session->read('Auth.User.mentor_type')=='Premium Member'):
					$rate = Configure::read('PremiumMemberCommission');
					$MGFee = ($price)*($rate)/100;
					$myshare = $price - $MGFee;
				else:
					$rate = Configure::read('RegularrMentorCommission');	
					$MGFee = ($price)*($rate)/100;
					$myshare = $price - $MGFee;
				endif;
				echo $this->Form->hidden('ment_rate',array('value'=>$rate,'id'=>'rateMentor'));
				echo $this->Form->hidden('ment_price',array('value'=>$price,'id'=>'priceMentor'));
				echo $this->Form->hidden('ment_discount',array('value'=>0.00,'id'=>'discountMentor'));
				echo $this->Form->hidden('ment_tax',array('value'=>0.00,'id'=>'taxMentor'));
                echo $this->Form->hidden('mentee_id',array('value'=>$clientData['User']['id']));
            ?>

<!-- js link -->
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>



<script type="text/javascript">

jQuery(document).ready(function(){
 jQuery(function() {
         var goingID ='';
         var globalflag ='0';
         
        jQuery("#pricetxt").change(function () {
       	 var onlyNumbers = /^[0-9,]+(\.\d{0,2})?$/.test(jQuery.trim(jQuery("#discount").val()));
       	 var price = jQuery("#pricetxt").val();
       	   
                    
       	 var discount = jQuery("#discount").val();
       	 var tax = jQuery("#tax").val();


       	 	 if(jQuery.trim(discount)==''){ discount = 0.00; }
       	 	 if(jQuery.trim(tax)==''){ tax = 0.00; }
	        	 var subtotal = parseFloat(price)-parseFloat(discount);
	        	 var total = parseFloat(subtotal) + parseFloat(tax);
                         var amount = parseFloat(price);
                     

	        	 jQuery('#pricetxt').val(amount.toFixed(2));
	        	 jQuery("#discount").val(parseFloat(discount).toFixed(2));
                         jQuery("#tax").val(parseFloat(tax).toFixed(2));
	        	 jQuery('#subtotal').html(subtotal.toFixed(2)); 
                         jQuery('#subtotalhidden').val(subtotal.toFixed(2)); 
	        	 jQuery('#total').html(total.toFixed(2));
	        	 jQuery('#discount').css('border-color','');
                         jQuery('#pricetxt').css('border-color','');  

      	 
       });
        jQuery("#discount").change(function () {
        	 var onlyNumbers = /^[0-9,]+(\.\d{0,2})?$/.test(jQuery.trim(jQuery("#discount").val()));
        	 var price = jQuery("#pricetxt").val();
        	              
        	 var discount = jQuery("#discount").val();
        	 var tax = jQuery("#tax").val();

        	 	 if(jQuery.trim(discount)==''){ discount = 0.00; }
        	 	 if(jQuery.trim(tax)==''){ tax = 0.00; }
	        	 var subtotal = parseFloat(price)-parseFloat(discount);
	        	 var total = parseFloat(subtotal) + parseFloat(tax);
	        	 
	        	 jQuery("#discount").val(parseFloat(discount).toFixed(2));
                         jQuery("#tax").val(parseFloat(tax).toFixed(2));
	        	 jQuery('#subtotal').html(subtotal.toFixed(2)); 
                         jQuery('#subtotalhidden').val(subtotal.toFixed(2));
	        	 jQuery('#total').html(total.toFixed(2));
	        	 jQuery('#discount').css('border-color','');
                           
      	 
        });
        jQuery("#tax").change(function () {
        	 var onlyNumbers = /^[0-9,]+(\.\d{0,2})?$/.test(jQuery.trim(jQuery("#tax").val()));
                 var price = jQuery("#pricetxt").val();
             
        	 var discount = jQuery("#discount").val();
        	 var tax = jQuery("#tax").val();
 

        	 	 if(jQuery.trim(discount)==''){ discount = 0.00; }
        	 	 if(jQuery.trim(tax)==''){ tax = 0.00; }
	        	 var subtotal = parseFloat(price)-parseFloat(discount);
	        	 var total = parseFloat(subtotal) + parseFloat(tax);

	        	 
	        	 jQuery("#discount").val(parseFloat(discount).toFixed(2));
                        jQuery("#tax").val(parseFloat(tax).toFixed(2));
	        	 jQuery('#subtotal').val(subtotal.toFixed(2)); 
                         jQuery('#subtotalhidden').val(subtotal.toFixed(2));
	        	 jQuery('#total').html(total.toFixed(2));

	        	 jQuery('#tax').css('border-color','');  
        	       	 
        });


                jQuery("#inv_from").change(function () {
                 jQuery('#inv_from').css('border-color',''); 

                 });

                jQuery("#inv_from").change(function () {
                 jQuery('#inv_from').css('border-color',''); 

                 });


                jQuery("#inv_client").change(function () {
                 jQuery('#inv_client').css('border-color',''); 

                 });


                jQuery("#inv_clientemail").change(function () {
                 jQuery('#inv_clientemail').css('border-color',''); 

                 });

                jQuery("#inv_title").change(function () {
                 jQuery('#inv_title').css('border-color',''); 

                 });

                jQuery("#inv_description").change(function () {
                 jQuery('#inv_description').css('border-color',''); 

                 });


       

         jQuery('#previewlink').click(function (e) {   
                             
                e.preventDefault();
                var projectid = jQuery('#projectid').val();

                var url = 'https://test.guild.im/project/invoice_preview/';
                jQuery('#Invoiceform').attr('action', url);
                //jQuery('#Invoiceform').attr('target', '_blank');

                 var  goingID ='';
                 var onlyNumbers = /^[0-9]*$/;

                 var flag;
                 flag=0;
                 var priceValue = jQuery('#total').html();

                 var inv_from = jQuery('#inv_from').val();
                 if(jQuery.trim(inv_from)=='') {
                    jQuery('#inv_from').css('border-color','#F00');
                    if(goingID==''){ goingID = 'inv_from'; }
                    flag++;
                 } else { jQuery('#inv_from').css('border-color',''); goingID='';}

                 var inv_client = jQuery('#inv_client').val();
                 if(jQuery.trim(inv_client)=='') {
                    jQuery('#inv_client').css('border-color','#F00');
                    if(goingID==''){ goingID = 'inv_client'; }
                    flag++;
                 } else { jQuery('#inv_client').css('border-color',''); goingID='';}

                 var inv_clientemail = jQuery('#inv_clientemail').val();
                 if(jQuery.trim(inv_clientemail)=='') {
                    jQuery('#inv_clientemail').css('border-color','#F00');
                    if(goingID==''){ goingID = 'inv_clientemail'; }
                    flag++;
                 } else { jQuery('#inv_clientemail').css('border-color',''); goingID='';}

                 
                 var title = jQuery('#inv_title').val();
                 if(jQuery.trim(title)=='') {
                    jQuery('#inv_title').css('border-color','#F00');
                    if(goingID==''){ goingID = 'inv_title'; }
                    flag++;
                 } else { jQuery('#inv_title').css('border-color',''); goingID='';}

                var invoicedescription = jQuery('#inv_description').val();
                if(jQuery.trim(invoicedescription)=='') {
                    jQuery('#inv_description').css('border-color','#F00');
                    if(goingID==''){ goingID = 'inv_description'; }
                    flag++;
                } else { jQuery('#inv_description').css('border-color',''); goingID='';}
                if(priceValue == '$0.00') {
                    jQuery('#pricetxt').css('border-color','#F00');
                    if(goingID==''){ goingID = 'pricetxt'; }
                    flag++;
                } else { 
                    jQuery('#pricetxt').css('border-color',''); goingID='';
                }
             
             
                if(flag>0){ 

                    console.log(flag);
                    if(goingID != ""){
 
                        jQuery('#'+goingID).focus();
                    }
                     return false;
                } else {

                    jQuery("#Invoiceform").submit();
                }
          });
         
    });

jQuery( "#discount" ).mouseout(function() {

var discount = jQuery("#discount").val();
if(discount ==''){

  jQuery( "#discount" ).val( "0.00" );

  }
});

jQuery( "#tax" ).mouseout(function() {

var tax = jQuery("#tax").val();
if(tax ==''){

  jQuery( "#tax" ).val( "0.00" );

  }
});

});
</script>