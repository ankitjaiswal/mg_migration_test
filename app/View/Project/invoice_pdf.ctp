<?php 
ob_clean();
App::import('Vendor','tcpdf/tcpdf/tcpdf');
//App::uses('Vendor','tcpdf/xtcpdf');
//echo $html->css(array('style'));

class MYPDF extends TCPDF {





  public function AcceptPageBreak() {

         if (1 == $this->PageNo()) {
                $this->SetMargins($left_margin, $top_margin, $right_margin, true);
         }
        if ($this->num_columns > 1) {
            // multi column mode
            if ($this->current_column < ($this->num_columns - 1)) {
                // go to next column
                $this->selectColumn($this->current_column + 1);
            } elseif ($this->AutoPageBreak) {
                // add a new page
                $this->AddPage();
                // set first column
                $this->selectColumn(0);
            }
            // avoid page breaking from checkPageBreak()
            return false;
        }
        return $this->AutoPageBreak;
    }


}

$tcpdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$textfont = 'ubuntu';
$tcpdf->SetFont($textfont,'',10);

$tcpdf->SetAuthor("Julia Holland");
$tcpdf->SetAutoPageBreak(true);
 
$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(false);

$tcpdf->SetMargins(10, 5, 10, 5); // left = 2.5 cm, top = 4 cm, right = 2.5cm
$tcpdf->SetFooterMargin(1.5);  // the bottom margin has to be set with SetFooterMargin
 

$invNo = $data['Invoice']['inv_no'];
$mentorName = ucfirst(strtolower($data['ProjectInvoice']['inv_from'])); 
$menteeName = ucfirst(strtolower($data['ProjectInvoice']['inv_client_name'])); 
$invCreatedDate =  date('M d, Y', strtotime($data['ProjectInvoice']['created']));
$invDate =  date('M d, Y');
$invTitle = $data['ProjectInvoice']['title'];


$price = number_format((float)($data['ProjectInvoice']['amount']), 2, '.', ',');
$discount = ($data['ProjectInvoice']['discount']);
$subtotal = number_format((float)($data['ProjectInvoice']['amount']-$data['ProjectInvoice']['discount']), 2, '.', ',');
$tax = ($data['ProjectInvoice']['tax']);
$total = number_format((float)(($data['ProjectInvoice']['amount']-$data['ProjectInvoice']['discount']) + $data['ProjectInvoice']['tax']), 2, '.', ',');
$invno = $inv_to = $data['ProjectInvoice']['invoiceno'];
$inv_to = $data['ProjectInvoice']['inv_client_name'];
$inv_from = $data['ProjectInvoice']['inv_from'];
$inv_description = nl2br(strip_tags($data['ProjectInvoice']['inv_description']));


$imgPath = 'https://www.guild.im/img/media/GUILD-PDF-NEW-Logo.svg';

$tcpdf->AddPage();

if($data['ProjectInvoice']['invoicestatus']=="OPEN"){
	// create some HTML content
	$htmlcontent = <<<EOF
<br/><br/><br/><br/><br/>

<table border="0">

<tr>
<td width="330" colspan="3" style="border-bottom:1px solid #CCCCCC;height:45inchs;"><img src="$imgPath" width="180"><br/></td>
<td width="210" style="border-bottom:1px solid #CCCCCC;text-align:right;height:45inchs;font-size:11inchs;"><br/><br/>Manoa Innovation Center<br/>2800 Innovation Dr. Suite 100<br/>Honolulu, HI 96822<br/>Toll Free: (866) 511-1898</td>
</tr>

<br/>
<tr><td width="350" colspan="2" style="border-bottom:1px solid #CCCCCC;height:38inch;"><h1 style="font-size:34inch;">INVOICE</h1>
</td>
<td width="120" colspan="1" style="border-bottom:1px solid #CCCCCC;height:38inch;font-size:11inchs;text-align:right;"><br/>Invoice No:<br/>Invoice Date:<br/>Payment Terms:<br/>
</td>
<td width="70" colspan="1" style="border-bottom:1px solid #CCCCCC;height:38inch;font-size:11inchs;text-align:right;"><br/>$invno<br/>$invCreatedDate<br/>Net 7<br/>
</td>
<br/>
</tr>

<br/><br/>

<tr>

<td width="420" colspan="3" style="line-height:3px;font-size:12inch;">To,<h2 style="font-size:14inch;">$inv_to</h2><br/></td>
<td width="125"  style="line-height:3px;font-size:12inch;"></td>
</tr>


<tr style="border:1px solid #333333;">
<td  width="15" colspan="1" style="background-color:#333333;height:30px;"></td>
<td  width="435" colspan="2" style="background-color:#333333;height:30px;border-right: 1px solid #CCCCCC;line-height:10px;"><h2 style="color: #CCCCCC;">Description</h2></td>
<td  width="95" style="background-color:#333333;height:30px;text-align:center;line-height:10px;"><h2 style="color: #CCCCCC;">&nbsp;&nbsp;&nbsp;&nbsp;Amount</h2></td>
</tr>

<tr>
<td  width="15" colspan="1" style="background-color:#f9f9f9;border-bottom:1px solid #CCCCCC;"></td>
<td width="435" colspan="2" style="border-bottom:1px solid #CCCCCC;background-color:#f9f9f9;border-right: 1px solid #CCCCCC;font-size:12inch;line-height:4px;"><br/><br/>For the consulting services delivered by $inv_from:<br/><br/>$inv_description<br/></td>
<td  width="95" style="border-bottom:1px solid #CCCCCC;background-color:#f9f9f9;font-size:12inch;text-align:right;line-height:4px;"><br/><br/>$$price&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/></td>
</tr>
<br/>
<br/>


<tr>

<td width="450" colspan="3" style="font-size:12inch;line-height:4px;text-align:right;"><br/>Discount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td  width="95" style="font-size:12inch;text-align:center;line-height:4px;text-align:right;"><br/>$$discount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
</tr>

<tr>

<td width="450" colspan="3" style="font-size:12inch;line-height:4px;text-align:right;"><br/>Subtotal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td  width="95" style="font-size:12inch;text-align:center;line-height:4px;text-align:right;"><br/>$$subtotal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
</tr>

<tr>

<td width="450" colspan="3" style="font-size:12inch;line-height:4px;text-align:right;"><br/>Tax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td  width="95" style="font-size:12inch;text-align:center;line-height:4px;text-align:right;"><br/>$$tax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
</tr>
<br/>

<tr style="border:1px solid #333333;">
<td  width="400" colspan="2" style="background-color:#333333;height:30px;"></td>
<td  width="50" colspan="1" style="background-color:#333333;height:30px;border-right: 1px solid #CCCCCC;line-height:10px;"><h2 style="color: #fff;font-weight:bold;">Total</h2></td>
<td  width="95" style="background-color:#333333;height:30px;text-align:right;line-height:10px;"><h2 style="color: #fff;font-weight:bold;">$$total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h2></td>
</tr>

<br/>
<br/>
<tr>
<td colspan="4" style="text-align:center;color:#333333;font-size:16inch;"><h2>Thank You!</h2></td>
</tr>
<br/>
<tr>
<td colspan="4" style="color:#666666;font-size:10inch;text-align:center;">
<p>You can <a href="https://test.guild.im/project/invoice_detail/$invno" style="color:#CF3135;text-decoration:none;" target="_blank">pay this invoice here</a>. If you have a question, contact <a href="mailto:help@guild.im" style="color:#CF3135;text-decoration:none;">help@guild.im</a> or call 1 (808) 729-5850.</p>


</td></tr>



</table>
      
        
  
    
EOF;

}else{


	// create some HTML content
	$htmlcontent = <<<EOF
<br/><br/><br/><br/><br/>



        
<table border="0">

<tr>
<td width="330" colspan="3" style="border-bottom:1px solid #CCCCCC;height:45inchs;"><img src="$imgPath" width="180"><br/></td>
<td width="210" style="border-bottom:1px solid #CCCCCC;text-align:right;height:45inchs;font-size:11inchs;"><br/><br/>Manoa Innovation Center<br/>2800 Innovation Dr. Suite 100<br/>Honolulu, HI 96822<br/>Toll Free: (866) 511-1898</td>
</tr>

<br/>
<tr><td width="350" colspan="2" style="border-bottom:1px solid #CCCCCC;height:38inch;"><h1 style="font-size:34inch;">INVOICE</h1>
</td>
<td width="120" colspan="1" style="border-bottom:1px solid #CCCCCC;height:38inch;font-size:11inchs;text-align:right;"><br/>Invoice No:<br/>Invoice Date:<br/>Payment Terms:<br/>
</td>
<td width="70" colspan="1" style="border-bottom:1px solid #CCCCCC;height:38inch;font-size:11inchs;text-align:right;"><br/>$invno<br/>$invCreatedDate<br/>Net 7<br/>
</td>
<br/>
</tr>

<br/><br/>

<tr>

<td width="420" colspan="3" style="line-height:3px;font-size:12inch;">To,<h2 style="font-size:14inch;">$inv_to</h2><br/></td>
<td width="125"  style="line-height:3px;font-size:12inch;"></td>
</tr>


<tr style="border:1px solid #333333;">
<td  width="15" colspan="1" style="background-color:#333333;height:30px;"></td>
<td  width="435" colspan="2" style="background-color:#333333;height:30px;border-right: 1px solid #CCCCCC;line-height:10px;"><h2 style="color: #CCCCCC;">Description</h2></td>
<td  width="95" style="background-color:#333333;height:30px;text-align:center;line-height:10px;"><h2 style="color: #CCCCCC;">&nbsp;&nbsp;&nbsp;&nbsp;Amount</h2></td>
</tr>

<tr>
<td  width="15" colspan="1" style="background-color:#f9f9f9;border-bottom:1px solid #CCCCCC;"></td>
<td width="435" colspan="2" style="border-bottom:1px solid #CCCCCC;background-color:#f9f9f9;border-right: 1px solid #CCCCCC;font-size:12inch;line-height:4px;"><br/><br/>For the consulting services delivered by $inv_from:<br/><br/>$inv_description<br/></td>
<td  width="95" style="border-bottom:1px solid #CCCCCC;background-color:#f9f9f9;font-size:12inch;text-align:right;line-height:4px;"><br/><br/>$$price&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/></td>
</tr>
<br/>
<br/>


<tr>

<td width="450" colspan="3" style="font-size:12inch;line-height:4px;text-align:right;"><br/>Discount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td  width="95" style="font-size:12inch;text-align:center;line-height:4px;text-align:right;"><br/>$$discount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
</tr>

<tr>

<td width="450" colspan="3" style="font-size:12inch;line-height:4px;text-align:right;"><br/>Subtotal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td  width="95" style="font-size:12inch;text-align:center;line-height:4px;text-align:right;"><br/>$$subtotal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
</tr>

<tr>

<td width="450" colspan="3" style="font-size:12inch;line-height:4px;text-align:right;"><br/>Tax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td  width="95" style="font-size:12inch;text-align:center;line-height:4px;text-align:right;"><br/>$$tax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
</tr>
<br/>

<tr style="border:1px solid #333333;">
<td  width="400" colspan="2" style="background-color:#333333;height:30px;"></td>
<td  width="50" colspan="1" style="background-color:#333333;height:30px;border-right: 1px solid #CCCCCC;line-height:10px;"><h2 style="color: #fff;font-weight:bold;">Total</h2></td>
<td  width="95" style="background-color:#333333;height:30px;text-align:right;line-height:10px;"><h2 style="color: #fff;font-weight:bold;">$$total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h2></td>
</tr>

<br/>
<br/>
<tr>
<td colspan="4" style="text-align:center;color:#333333;font-size:16inch;"><h2>Thank You!</h2></td>
</tr>
<br/>
<tr>
<td colspan="4" style="color:#666666;font-size:10inch;text-align:center;">

<p>If you have a question, contact <a href="mailto:help@guild.im" style="color:#CF3135;text-decoration:none;">help@guild.im</a> or call 1 (808) 729-5850.</p>

</td></tr>


</table>

      
        
  
    
EOF;

}


// output the HTML content
$tcpdf->writeHTML($htmlcontent, true, 0, true, 0);
$filename = "GUILD_Invoice-".$invno.".pdf";
$tcpdf->Output($filename.".pdf", 'I'); ?>