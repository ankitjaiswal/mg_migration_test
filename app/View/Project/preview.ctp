  <link rel="stylesheet" href="<?php echo(SITE_URL)?>css/project.css">
  
    <div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div class="mentor_details"><!-- VL 22/12-->
			<div id="mentor-detail-left1">
				<form id="Appyform" enctype="multipart/form-data" method="post" action="<?php echo SITE_URL ?>project/postproject" accept-charset="utf-8">
				<div id="area-box">
					<div class="expertise" style="padding-top:15px;">
					
					<div style="display: block; height: 60px; margin-bottom: 20px; border-bottom: 1px solid rgb(188, 188, 188);">
					<div style="float: left;">
						<h1 style="font-size: 20px; border: 0px;">Post a project</h1>
						<sub class="MGSub">Free & confidential</sub> 
					</div>
					<div style="float: right;">
						<table class="step3">
						    <tr>
						        <td><span>1</span></td>
						        <td>Create</td>
						        <td><span>2</span></td>
						        <td>Preview</td>
						        <td><span style="background-color: #F13031;">3</span></td>
						        <td style="color: #F13031;">Post</td>
						    </tr>
						</table>
					</div>
					<br/>	
				</div>
						<?php echo($this->Form->hidden('Project.id'));?>
						
						<div class="expertise">
                        	<h2>Project title</h2>
                        	<p><?php echo($this->data['Project']['title']);?></p>
                   		</div>
                   		
                   		<div class="expertise">
                        	<h2>Describe your need</h2>
                        	<p>
                        		<?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['details']); 
								echo $this->General->make_links(nl2br($details));?>
							</p>
                   		</div>
                   		
                   		<div class="expertise">
                        	<h2>Company URL</h2>
                        	<p><?php echo($this->Html->link($this->data['Project']['url'],$this->General->addhttp($this->data['Project']['url']),array('escape'=>true , 'target'=>'_blank')));?></p>
                   		</div>
                   		
						<?php if(isset($this->data['Project']['success']) && $this->data['Project']['success'] != '' ) {?>
						<div class="expertise">
                        	<h2>How will you define success of the project?</h2>
                        	<p>
                        		<?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['success']); 
								echo $this->General->make_links(nl2br($details));?>
							</p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['budget']) && $this->data['Project']['budget'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Approximate Budget $ </h2>
                        	<p><?php echo($this->data['Project']['budget']);?></p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['location']) && $this->data['Project']['location'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Location preference</h2>
                        	<p><?php echo($this->data['Project']['location']);?></p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['duration']) && $this->data['Project']['duration'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Estimated duration</h2>
                        	<p><?php echo($this->data['Project']['duration']);?></p>
                   		</div>
                   		<?php }?>
                   		
                   		<?php if(isset($this->data['Project']['start_date']) && $this->data['Project']['start_date'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Target start date</h2>
                        	<p><?php echo($this->data['Project']['start_date']);?></p>
                   		</div>
                   		<?php }?>
                   		<?php if(isset($this->data['Project']['phone_number']) && $this->data['Project']['phone_number'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Phone number</h2>
                        	<p><?php echo($this->data['Project']['phone_number']);?></p>
                   		</div>
                   		<?php }?>
                   		<?php if(isset($this->data['Project']['filepath']) && $this->data['Project']['filepath'] != '' ) {?>
                   		<div class="expertise">
                        	<h2>Attachment</h2>
                        	<div class="upload">
                            	<?php 
                            	$img = SITE_URL."img/".MENTEES_PROJECT_PATH.DS.$this->data['Project']['id'].DS.$this->data['Project']['filepath']; ?>
                            	
                            	<p><span style="font-size:14px;"> <?php echo($this->Html->link($this->data['Project']['filepath'],$img,array('escape'=>true , 'target'=>'_blank'))); ?> </span></p>
					   		</div>
                   		</div>
                   		<?php }?>
                   		
                   		<div class="expertise" style="margin-top: 20px; margin-bottom: 20px;">
	                   		<div style="float: right;">
					            <?php  echo($this->Html->link("Edit project",SITE_URL.'project/create/'.$this->data['Project']['id'],array('escape'=>false)));?>
				            </div>
				        </div>
                   		
                   		<div class="expertise">
                        	<h2>Shortlisted consultants</h2>
                        	<div>
		                   		<?php foreach ($mentorsArray as $value) {
		                   			
			                   		if(isset($value['UserImage'][0]['image_name']) && $value['UserImage'][0]['image_name'] !=''){
			                   			$image_path = MENTORS_IMAGE_PATH.DS.$value['User']['id'].DS.$value['UserImage'][0]['image_name'];
			                   		} else {
										$image_path = 'media/profile.png';
			                   		}?>
                                                 <?php
						    $urlk = ($value['User']['url_key']);
							$displink=SITE_URL.strtolower($urlk);
				                  ?>
		                   			<p>
		            					<img src="<?php echo(SITE_URL.'img/'.$image_path)?>" width="30" height="30" alt="<?php echo($value['UserReference']['first_name']." ".$value['UserReference']['last_name']);?>" title="<?php echo($value['UserReference']['first_name']." ".$value['UserReference']['last_name']);?>"/>
		                   				<?php echo($this->Html->link(ucfirst($value['UserReference']['first_name']).' '.ucfirst($value['UserReference']['last_name']),$displink,array('escape'=>false, 'style'=>'margin-left: 5px;','target'=>'_blank'))); ?>
		                   			</p>
		                   		<?php echo($this->Form->hidden('Project.User.'.$value['User']['id'],array('value'=>$value['User']['id'])));
		                   		 }?>
	                   		</div>
                   		</div>
                   		
                   		<div class="expertise" style="margin-top: 20px; margin-bottom: 20px;">
	                   		<div style="float: right;">
					            <?php  echo($this->Html->link("Change consultants",SITE_URL.'project/invite/'.$this->data['Project']['id'],array('escape'=>false)));?>
				            </div>
			            </div>
					  	
					  	<div class="inputRELATIVE">
					  		<div style="margin-bottom: 20px; margin-top: 10px;">
                        		
                        		<div class="showHINT" style="height: 115px; top: -34px; right: -20px;">
	                        		<span></span><!--for left arrow show on hover-->
	                        		<p style="text-align: left; margin-top: 10px;">
										Your project details will be shared with the shortlisted consultants only (unless you choose to share with other consultants)</p>
	                        		</p>
	                    		</div>

                        	</div>
                   		</div>
                   		

                   		
						<div style="float: right;">
					    	<input style="float: none; margin-left: 15px; font-weight: normal;" value="Post" type="submit" class="mgButton" id="previewButton">
			            </div>
<br/>
<br/>
					</div>
				</div>
				</form>
    		</div>
    	</div>	

    </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function(){

});
</script>
