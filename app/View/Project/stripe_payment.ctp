<script src="https://checkout.stripe.com/checkout.js"></script>
<div class="stripe-payment-page comman-page-boby">
	<div class="container create-section pad0">
		<div style="">
			<div class="progresbar" style="margin: 0 0 30px;">
				<div class="col-sm-8 col-sm-offset-2">
					<ul>
						<li class="active">
							<span>1</span>
							<div class="title">
								<p>Project created</p>
							</div>
						</li>
						<li class="active">
							<span>2</span>
							<div class="title">
								<p>Project approved</p>
							</div>
						</li>
						<li>
							<span>3</span>
							<div class="title">
								<p>Proposal accepted</p>
							</div>
						</li>
						<li>
							<span>4</span>
							<div class="title">
								<p>Project started</p>
							</div>
						</li>
						<li>
							<span>5</span>
							<div class="title">
								<p>Project completed</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="stripe-payment-list">
					<h3>GUILD offers this summary as a quick reference to potential clients. This is not a complete outline, so please read the entire <a href="https://test.guild.im/fronts/terms">Terms of Service</a> very carefully.</h3>
					<ul>
						<li>You are responsible for all activity and content in your account.</li>
						<li>Do not do anything illegal.</li>
						<li>Do not misrepresent who you are, or falsify any records.</li>
						<li>Client-Client Partner-Consultant interactions are based on trust. Keep it professional and confidential.</li>
						<li>Do not share any intellectual property that does not belong to you.</li>
						<li>Do not make any arrangements to bypass GUILD and make separate arrangements with member Consultants outside the Service.</li>
						<li>For all other violations not listed above, e.g. minor transgressions against the spirit or letter of our Terms of Service, we have a 3-strike policy: the first reported violation will lead to a warning, the second to a 1-month ban from the Service, and the third to a permanent ban from the Service.</li>
					</ul>
				</div>
			</div>
			<div class="col-xs-12">
				<div class="stripe-payment-requirements">
					<center><h2 class="project-heading">I understand that I will be charged a $500 deposit that will either go towards my GUILD invoice or be refunded if I do not accept the GUILD proposal for my requirements.</h2></center>
					<input id="projectId" value="<?php echo($projectid);?>" type="hidden">
					<input id="emailaddress" value="<?php echo($this->Session->read('Auth.User.username'));?>" type="hidden">
					<center><button class="banner-getstarted btn btn-default get-started" id="customButton"  style="margin:20px 12px 10px 10px;">Pay Now</button></center>
				</div>
			</div>
		</div>
		
		<script type="text/javascript">
		var projectId = jQuery('#projectId').val();
		var email = jQuery('#emailaddress').val();
		var price = 1*100;
		console.log(projectId);
		var handler = StripeCheckout.configure({
		key: 'pk_live_vg2OVxfTe0Iw5FGSMhitLWE7',
		image: 'https://www.guild.im/imgs/guild_monogram.png',
		locale: 'auto',
		token: function(token) {
		// Use the token to create the charge with a server-side script.
		// You can access the token ID with `token.id`
		console.log("After payment Made");
		jQuery.ajax({
							url:SITE_URL+'users/stripecheckout',
							type:'post',
							dataType:'json',
							data: 'projectId='+projectId + '&price=' + price + '&email=' + email +'&token=' + token.id,
							success:function(projectId){
								window.location.href = SITE_URL+'project/thanks_for_payment';
							}
						});
		}
		});
		jQuery('#customButton').click(function(){
		console.log("Button Clicked");
		// Open Checkout with further options
		handler.open({
		name: 'GUILD',
		description: 'Project Deposit',
		amount: price,
		email: email
		});
		return false;
		e.preventDefault();
		});
		// Close Checkout on page navigation
		jQuery(window).on('popstate', function() {
		handler.close();
		});
		</script>
		<!-- js link -->
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
		<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
		
		<style>
		.stripe-payment-list ul
		{
			margin-left:1.4%;
		}
		.stripe-payment-list h3 {
    margin-bottom: 42px !important;
}
		</style>

	</div>
</div>