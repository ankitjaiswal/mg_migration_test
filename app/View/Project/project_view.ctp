	<div class="container-fluid" id="apply-section">
		<div class="container create-section pad0">
				<div class="progresbar" style="margin: 0 0 30px;">
                                      <div class="col-sm-8 pad40">
					<ul>
						<li class="active">
							<span>1</span>
							<div class="title">
								<p>Project created</p>
							</div>
						</li>
						<li>
							<span>2</span>
							<div class="title">
								<p>Project approved</p>
							</div>
						</li>
						<li>
							<span>3</span>
							<div class="title">
								<p>Proposal accepted</p>
							</div>
						</li>
						<li>
							<span>4</span>
							<div class="title">
								<p>Project started</p>
							</div>
						</li>
						<li>
							<span>5</span>
							<div class="title">
								<p>Project completed</p>
							</div>
						</li>
					</ul>
				</div>
                             </div> 
			<div class="col-md-8 pad40" id="projectnew-view">
                               <form id="Projectform" enctype="multipart/form-data" method="post"  action="<?php echo SITE_URL."project/client_approveproject/".$this->data['Project']['id']; ?>" accept-charset="utf-8">
				<div class="apply-heading">
					<h5>Approve your project</h5>
					<label class="free-c"> (Free & confidential)</label>
				</div>
				<?php echo($this->Form->hidden('Project.id'));?>
				<div class="project-overview">
					<div class="overview-desc">
						<h5>Project title</h5>
						<p><?php echo($this->data['Project']['title']);?></p>
					</div>


					<div class="overview-desc">
						<h5>Project details</h5>
						<p> <?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['details']); 
								echo $this->General->make_links(nl2br($details));?>
					    </p>
					</div>

                                      <?php if(isset($this->data['Project']['start_date']) && $this->data['Project']['start_date'] != '' ) {?>				 
					<div class="overview-desc">
						<h5>Start</h5>
						<p><?php echo($this->data['Project']['start_date']);?></p>
					</div>
				     <?php }?>
                                     <?php if(isset($this->data['Project']['budget']) && $this->data['Project']['budget'] != '' ) {?>					
					<div class="overview-desc">
						<h5>Budget</h5>
						<p><?php echo($this->data['Project']['budget']);?></p>
					</div>
				    <?php }?>
                                 <?php if(isset($this->data['Project']['success']) && $this->data['Project']['success'] != '' ) {?>					
					<div class="overview-desc">
						<h5>Desired qualification</h5>
						<p> <?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['success']); 
								echo $this->General->make_links(nl2br($details));?>
					    </p>
					</div>
			         <?php }?>
                                 <?php if(isset($this->data['Project']['industry']) && $this->data['Project']['industry'] != '' ) {?>
					<div class="overview-desc">
						<h5>Industry</h5>
						<p><?php echo($this->data['Project']['industry']);?> &#8212; <?php echo($this->data['Project']['category']);?></p>
					</div>
                                  <?php }?>
                                 <?php if(isset($this->data['Project']['business_need']) && $this->data['Project']['business_need'] != '' ) {?>
					<div class="overview-desc">
						<h5>Business need</h5>
						<p><?php echo($this->data['Project']['business_need']);?></p>
					</div>
                                 <?php }?>
                                 <?php if(isset($this->data['Project']['solution_type']) && $this->data['Project']['solution_type'] != '' ) {?>
					<div class="overview-desc">
						<h5>Solution type</h5>
						<p><?php echo($this->data['Project']['solution_type']);?></p>
					</div>
                                  <?php }?>

	
				 <?php if(isset($this->data['Project']['location']) && $this->data['Project']['location'] != '' ) {?> 
					<div class="overview-desc">
						<h5>Location preference</h5>
						<p><?php echo($this->data['Project']['location']);?></p>
					</div>
				 <?php }?>	
				 <?php if(isset($this->data['Project']['duration']) && $this->data['Project']['duration'] != '' ) {?>
					<div class="overview-desc">
						<h5>Estimated duration</h5>
						<p><?php echo($this->data['Project']['duration']);?></p>
					</div>
				 <?php }?>

                  <?php  if($this->Session->read('Auth.User.role_id')== 3 || $this->Session->read('Auth.User.role_id')== '1' || ($this->Session->read('Auth.User.role_id')==2 && $this->Session->read('Auth.User.id') == $this->data['Project']['user_id'])) {                		
                   		if(isset($this->data['Project']['phone_number']) && $this->data['Project']['phone_number'] != '' ) {?>
					<div class="overview-desc">
						<h5>Phone number</h5>
						<p><?php echo($this->data['Project']['phone_number']);?></p>
					</div>
				  <?php }}?>
                  <?php if(isset($this->data['Project']['filepath']) && $this->data['Project']['filepath'] != '' ) {?>				  
					<div class="overview-desc">
						<h5>Attachment</h5>
						                            	 
                            <?php $img = SITE_URL."img/".MENTEES_PROJECT_PATH.DS.$this->data['Project']['id'].DS.$this->data['Project']['filepath']; ?>
						<p class="attach-overview">
							<?php echo($this->Html->link($this->data['Project']['filepath'],$img,array('escape'=>true , 'target'=>'_blank'))); ?>
						</p>
					</div>
				  <?php }?>




					<div class="e-button pull-right">
						<label>
							<a href="<?php echo(SITE_URL);?>project/projectedit/<?php echo($this->data['Project']['id']);?>" class="hide-fields">
								Edit
							</a>
						</label>
						<button class="banner-getstarted btn btn-default get-started" onclick="window.location.href='<?php echo SITE_URL."project/client_approveproject/".$this->data['Project']['id']; ?>';">
							Approve
						</button>
					</div>
                                      
                      </form>
				</div>
			</div>
			<div class="col-md-4 clearboth create-right">
				<div class="right-apply">
					<div class="apply-heading">
					<h5>How does it work?</h5>	
					</div>
					<div class="apply-desc">
				<div class="cr-desc">
					<h5>A. Specify Business Need</h5>
					<p>Post your high-level requirement using our 1-page form.</p>
				</div>
				<div class="cr-desc">
					<h5>B. Review Proposals</h5>
					<p>A Client Partner will help you finalize your project needs. Then, in 2-3 days they will get back to you with proposals and recommend specialist(s) for the job.</p>
				</div>
				<div class="cr-desc">
					<h5>C. Get work done</h5>
					<p>Once you approve a proposal we will get to work. The Client Partner will ensure all deliverables are met on a timely basis.</p>
				</div>
					</div>
					<div class="apply-heading">
						<h5>Not ready to post yet?</h5>
					</div>
					<div class="apply-desc">
						<div class="cr-desc mt10">
							<p>Call us to discuss your business needs. </br>Call 1-866-511-1898.</p>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>
	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/script6.js"></script>	

<script type="text/javascript">
jQuery(document).ready(function(){


    	jQuery("#loggedinsubmit").click(function(){

           jQuery("#Projectform").submit();
    });


	jQuery("#visitorformopen").click(function(){
           
         document.getElementById('visitorform').style.display = 'inline-block';
         document.getElementById('visitorbutton').style.display = 'none';
    	 jQuery('html, body').animate({
    		 scrollTop: jQuery("#visitorform").offset().top
         }, 2000);
         return false;
         
    });

	jQuery("#linkedinbuttonclick").click(function(){

        var projectId = jQuery('#ProjectId').val();
             
	window.location.href = SITE_URL+'linkedin/OAuth2.php';
    });

    		jQuery("#visitorsubmit").click(function() {
                            
                var flag = 0;
				
		      if(jQuery.trim(jQuery("#userfname").val()) == '') {
                    jQuery('#userfname').css('border-color','#F00');
                    jQuery('#userfname').focus();                
                    flag++;
                    }else {
                    jQuery('#userfname').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#userlname").val()) == '') {
                    jQuery('#userlname').css('border-color','#F00');
                    jQuery('#userlname').focus();                
                    flag++;
                    }else {
                    jQuery('#userlname').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#useremail").val()) == '') {
                    jQuery('#useremail').css('border-color','#F00');
                    jQuery('#useremail').focus();                
                    flag++;
                    }else {
                    jQuery('#useremail').css('border-color','');                 
                    }
		      if(jQuery.trim(jQuery("#userphone").val()) == '') {
                    jQuery('#userphone').css('border-color','#F00');
                    jQuery('#userphone').focus();                
                    flag++;
                    }else {
                    jQuery('#userphone').css('border-color','');                 
                    }
    			if(flag > 0) {
    				return false;
    			} else {

				var answer = jQuery.trim(jQuery("#phone_number").val());
				jQuery("#phonenumber").val(answer);

				jQuery("#newmentorshipForm").submit();
                     }
    		});


 });   		
    		
</script>
