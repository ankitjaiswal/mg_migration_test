	
	<div class="fieldset" style="display: block;">
    <div class="adminrightinner" style="padding:0px;">

            <div class="tablewapper2">
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Admin2Table">	
                    <tr class="head">
                        <td width="5%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Id</td>
                        <td  width="40%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Proposal Title</td>
                        <td  width="15%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Proposal Status</td>


                         <td width="25%" align="center" valign="middle" class="Bdrrightbot Padtopbot6">Proposal Author</td>
                        <td width="15%" align="center" valign="middle" class="Bdrrightbot Padtopbot6">Action</td>
                    </tr>
                    <?php 
                    if(!empty($proposals) && count($proposals)>0)
                    { 
                        foreach($proposals as $msg)
                        {    
                     $proposal_url = $msg['Proposal']['proposal_url'];
 
                        ?>
                        <tr>
                            <td width="5%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;"><?php echo $msg['Proposal']['id']; ?></td>

                            <td  width="40%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><?php echo($this->Html->link($msg['Proposal']['proposal_title'],SITE_URL.'project/proposal/'.$proposal_url,array('escape'=>true, ))); ?></td>
                           <?php if($msg['Proposal']['haspublished'] == 'Y'){?>
                             <td  width="15%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Published</td>

                            <?php }else{?>
                              <td  width="15%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6">Draft</td>
                             <?php }?>
                              <?php 
                              $menu = ClassRegistry::init('UserReference');  //for class load in view
                              $author = $menu->find('first', array('conditions' => array('UserReference.user_id' => $msg['Proposal']['user_id'])));
                              $authorname = $author['UserReference']['first_name'].' '.$author['UserReference']['last_name'];
                              ?>
                             <td  width="20%"  align="center" valign="middle" class="Bdrrightbot Padtopbot6"><a href="<?php echo(SITE_URL);?><?php echo($msg['User']['url_key']);?>"><?php echo($authorname);?></a></td>
                                                   
                            <td width="15%" align="center" valign="middle" class="Bdrbot ActionIcon">
                            <?php  if($msg['Proposal']['ispublic'] == 'Y') {
    echo("Public");
   }else{
   echo("Private");
   }
 ?>
                             <?php echo($this->Admin->getActionImage(array('edit' => array('controller' => 'project', 'action' => 'proposal_adminedit')),$msg['Proposal']['id'])); ?>
                              <?php echo($this->Admin->getActionImage(array('delete' => array('controller' => 'project', 'action' => 'admin_deleteproposal', 'token' => 'delete')), $msg['Proposal']['id'])); ?>

                              <a href="<?php echo(SITE_URL);?>project/admin_changeproposalstatus/<?php echo($msg['Proposal']['id']);?>"><img src="<?php echo(SITE_URL);?>img/admin/change_password.jpg" class="viewstatusimg1" title="change proposal status" alt="change proposal status" height="18" width="18"></a>

                          </td>

                           </td>
                            
                        </tr>	
                   <?php
                        }
                    }else{ ?>
                   <tr>
                        <td colspan="3" width="15%" align="left" valign="middle" class="Bdrrightbot" style="padding-left:9px;">Record not found</td>           
                    </tr>
                   <?php } ?>   
                </table>
            </div>
            <?php
     //   }
        ?>

    </div>
</div>
<div class="clr"></div>
<?php //echo $this->element('admin/admin_paging', array("paging_model_name" => "NewsLetter", "total_title" => "NewsLetter")); ?>	 