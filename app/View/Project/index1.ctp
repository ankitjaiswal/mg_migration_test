<div class="container-fluid projectCreated" id="apply-section">
	<div class="container create-section pad0">
                                <?php foreach ($mentorsArray as $value) {
                                   if($this->Session->read('Auth.User.id') == $value['User']['id']){
                                    $consultantisthere = "yes";
                                    }

                                        if($value['User']['id'] == $this->Session->read('Auth.User.id')){         
					$consultant = ClassRegistry::init('Project_member_confirm');
					$show_interest = $consultant->find('first', array('conditions' => array('AND' =>array('Project_member_confirm.project_id ' => $this->data['Project']['id'] ,'Project_member_confirm.member_id' => $value['User']['id']))));
                                        if($show_interest['Project_member_confirm']['status'] == '1'){
                                        $consultantselected = "yes";
                                        }
                                        }

                                 }?>





		<?php if($this->data['Project']['status'] == 3 || $this->data['Project']['status'] == 4 || $this->data['Project']['status'] == 5 || $this->data['Project']['status'] == 6 || $this->data['Project']['status'] == 7 || $this->data['Project']['status'] == 8 || $this->data['Project']['status'] == 9) {?>
		<div class="progresbar" style="margin: 40px 0 30px;">
			<div class="col-sm-8 pad40">
				<ul>
					<li class="active">
						<span>1</span>
						<div class="title">
							<p>Project created</p>
						</div>
					</li>
					<?php if($this->data['Project']['status'] == 4 || $this->data['Project']['status'] == 5 || $this->data['Project']['status'] == 6 || $this->data['Project']['status'] == 7 || $this->data['Project']['status'] == 8) {?>
					<li class="active">
						<span>2</span>
						<div class="title">
							<p>Project approved</p>
						</div>
					</li>
					<?php }else{?>
					<li>
						<span>2</span>
						<div class="title">
							<p>Project approved</p>
						</div>
					</li>
					<?php }?>
					<?php if($this->data['Project']['status'] == 7 || $this->data['Project']['status'] == 8 || $this->data['Project']['status'] == 9) {?>
					<li class="active">
						<span>3</span>
						<div class="title">
							<p>Proposal accepted</p>
						</div>
					</li>
					<?php }else{?>
					<li>
						<span>3</span>
						<div class="title">
							<p>Proposal accepted</p>
						</div>
					</li>
					<?php }?>
					<?php if($this->data['Project']['status'] == 8 || $this->data['Project']['status'] == 9) {?>
					<li class="active">
						<span>4</span>
						<div class="title">
							<p>Project started</p>
						</div>
					</li>
					<?php }else{?>
					<li>
						<span>4</span>
						<div class="title">
							<p>Project started</p>
						</div>
					</li>
					<?php }?>
					<?php if($this->data['Project']['status'] == 9) {?>
					<li class="active">
						<span>5</span>
						<div class="title">
							<p>Project completed</p>
						</div>
					</li>
					<?php }else{?>
					<li>
						<span>5</span>
						<div class="title">
							<p>Project completed</p>
						</div>
					</li>
					<?php }?>
				</ul>
			</div>
		</div>
		<?php }?>




		<div class="col-md-4 col-sm-12 col-xs-12 pull-right clearboth create-right">
			<div class="right-apply">
				<div class="project-overview">
					<div class="overview-desc created-by">
						<h5>GUILD Client Partner
						</h5>
						<div class="created-person-img">
							<img src="<?php echo(SITE_URL)?>press_material/Iqbal-Ashraf-GUILD-Client-Partner.jpg">
						</div>
                                                <br/>
                                                <h5>Iqbal Ashraf</h5>
                                                <h5>Phone: <a href="tel:1-808-729-5850">1-808-729-5850</a></h5>
                                                <h5>Email: <a href="mailto:iqbal@guild.im">iqbal@guild.im</a></h5>
					</div>
				</div>
			</div>
		</div>

		
		<div class="col-md-8 pull-left pad40" id="projectnew">

			<div class="project-overview">
				<?php echo($this->Form->hidden('Project.id'));?>
				<div class="overview-desc">
					<h5>Project title</h5>
					<p><?php echo($this->data['Project']['title']);?></p>
				</div>
				<div class="overview-desc">
					<h5>Project details</h5>
					<p> <?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['details']);
						echo $this->General->make_links(nl2br($details));?>
					</p>
				</div>
				<?php if(isset($this->data['Project']['start_date']) && $this->data['Project']['start_date'] != '' ) {?>
				<div class="overview-desc">
					<h5>Start</h5>
					<p><?php echo($this->data['Project']['start_date']);?></p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['budget']) && $this->data['Project']['budget'] != '' ) {?>
				<div class="overview-desc">
					<h5>Budget</h5>
					<p><?php echo($this->data['Project']['budget']);?></p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['success']) && $this->data['Project']['success'] != '' ) {?>
				<div class="overview-desc">
					<h5>Desired qualification</h5>
					<p> <?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['success']);
						echo $this->General->make_links(nl2br($details));?>
					</p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['industry']) && $this->data['Project']['industry'] != '' ) {?>
				<div class="overview-desc">
					<h5>Industry</h5>
					<p><?php echo($this->data['Project']['industry']);?> &#8212; <?php echo($this->data['Project']['category']);?></p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['business_need']) && $this->data['Project']['business_need'] != '' ) {?>
				<div class="overview-desc">
					<h5>Business need</h5>
					<p><?php echo($this->data['Project']['business_need']);?></p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['solution_type']) && $this->data['Project']['solution_type'] != '' ) {?>
				<div class="overview-desc">
					<h5>Solution type</h5>
					<p><?php echo($this->data['Project']['solution_type']);?></p>
				</div>
				<?php }?>
				
				<?php if(isset($this->data['Project']['location']) && $this->data['Project']['location'] != '' ) {?>
				<div class="overview-desc">
					<h5>Location preference</h5>
					<p><?php echo($this->data['Project']['location']);?></p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['duration']) && $this->data['Project']['duration'] != '' ) {?>
				<div class="overview-desc">
					<h5>Estimated duration</h5>
					<p><?php echo($this->data['Project']['duration']);?></p>
				</div>
				<?php }?>
				<?php  if($this->Session->read('Auth.User.role_id')== 3 || $this->Session->read('Auth.User.role_id')== '1' || ($this->Session->read('Auth.User.role_id')==2 && $this->Session->read('Auth.User.id') == $this->data['Project']['user_id'])) {
				if(isset($this->data['Project']['phone_number']) && $this->data['Project']['phone_number'] != '' ) {?>
				<div class="overview-desc">
					<h5>Phone number</h5>
					<p><?php echo($this->data['Project']['phone_number']);?></p>
				</div>
				<?php }}?>
				<?php if(isset($this->data['Project']['filepath']) && $this->data['Project']['filepath'] != '' ) {?>
				<div class="overview-desc">
					<h5>Attachment</h5>
					
					<?php $img = SITE_URL."img/".MENTEES_PROJECT_PATH.DS.$this->data['Project']['id'].DS.$this->data['Project']['filepath']; ?>
					<p class="attach-overview">
						<?php echo($this->Html->link($this->data['Project']['filepath'],$img,array('escape'=>true , 'target'=>'_blank'))); ?>
					</p>
				</div>
				<?php }?>
				
				


				


				
				
				

				
			</div>





			<!-- Consultant -->

                           
                                    

				<?php if(($consultantisthere == "yes") && ($this->Session->read('Auth.User.role_id')==2 && $this->Session->read('Auth.User.id') != $this->data['Project']['user_id'])) {?>
                                
				<?php if(!empty($mentorsArray)){?>
			<div class="project-overview">
				<div class="overview-desc account-info">
					<h3>Project proposal</h3>
					<ul class="proposal-list">


					<?php foreach ($mentorsArray as $value) {
                                        if($value['User']['id'] == $this->Session->read('Auth.User.id')){         
					$consultant = ClassRegistry::init('Project_member_confirm');
					$show_interest = $consultant->find('first', array('conditions' => array('AND' =>array('Project_member_confirm.project_id ' => $this->data['Project']['id'] ,'Project_member_confirm.member_id' => $value['User']['id']))));
					$proposal = ClassRegistry::init('Proposal');
					$proposal_create = $proposal->find('first', array('conditions' => array('AND' =>array('Proposal.projectid ' => $this->data['Project']['id'] ,'Proposal.user_id' => $value['User']['id']))));
					$proposallink = SITE_URL."project/proposal/".$proposal_create['Proposal']['proposal_url'];
					if(($show_interest['Project_member_confirm']['status'] == '0' || $show_interest['Project_member_confirm']['status'] == '1' || $show_interest['Project_member_confirm']['status'] == '-1' )){
						if(isset($value['UserImage'][0]['image_name']) && $value['UserImage'][0]['image_name'] !=''){
							$image_path = MENTORS_IMAGE_PATH.DS.$value['User']['id'].DS.$value['UserImage'][0]['image_name'];
						} else {
											$image_path = 'media/profile.png';
					}?>
                                                 
						<li>
							<img src="<?php echo(SITE_URL.'img/'.$image_path)?>" alt=""  style="width:40px;height:40px;"/>
							<span><?php echo (ucfirst($value['UserReference']['first_name']).' '.ucfirst($value['UserReference']['last_name'])); ?></span>
							<div class="button-set">
                                                               <?php if($proposal_create['Proposal']['proposal_url'] !=''){?>
                                                                <?php if($proposal_create['Proposal']['haspublished'] == 'N'){?>
                                                                <span>Draft</span>
                                                                <?php } else if($proposal_create['Proposal']['haspublished']== 'Y' && $show_interest['Project_member_confirm']['status'] == '0'){?>
                                                                <span>Submitted</span>
                                                                <?php } else if($proposal_create['Proposal']['haspublished']== 'Y' && $show_interest['Project_member_confirm']['status'] == '-1'){?>
                                                                <span>Not accepted by client</span>
                                                                <?php }else{?>
                                                                <span> Accepted</span>
                                                                <?php }?>
                                                                <a href="<?php echo ($proposallink);?>" class="reset" target="_blank">View proposal</a>
                                                                <?php }else if($proposal_create['Proposal']['proposal_url'] =='' && $show_interest['Project_member_confirm']['status'] == '0'){?>
                                                                <span>Applied</span>
                                                                <?php }else if($proposal_create['Proposal']['proposal_url'] =='' && $show_interest['Project_member_confirm']['status'] == '1'){?>
                                                                <span>Accepted</span>
                                                                <?php }?>
                                                                
								
							</div>
						</li>
                                                   
					<?php echo($this->Form->hidden('Project.User.'.$value['User']['id'],array('value'=>$value['User']['id'])));}}}?>
					

					</ul>
				</div>
			</div>
                       <?php }}?>






			<div class="e-button pull-right btn-inline">				

				<button class="banner-getstarted btn btn-default get-started" onclick="window.location.href='<?php echo SITE_URL."project/confirm_interest/".$this->data['Project']['id']; ?>';">
				Click to confirm your interest in this project
				</button>
			</div>



		</div>
		
		
	</div>
</div>



<!-- js link -->
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>




