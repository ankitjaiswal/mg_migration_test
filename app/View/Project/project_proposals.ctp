<div id="banner-section" class="lenses-page site-banner" data-parallax="scroll" data-image-src="<?php echo(SITE_URL)?>yogesh_new1/images/project_proposal.png">
    <div class="col-md-12 text-center banner-content">
        <img src="<?php echo(SITE_URL)?>yogesh_new1/images/pp_img.png" alt="" class="site-icon">
        <h1>Project proposals</h1>
        <p>Here is a sample of our public project proposals. These are high-impact projects that we can deliver to your organization.</p>
<?php if($this->Session->read('Auth.User.id') != '' && $this->Session->read('Auth.User.role_id')== "2"){?>
        <a target="_blank" class="banner-getstarted btn btn-default get-started" onclick="window.location.href='<?php echo SITE_URL?>project/proposal_create'">
        Create a project proposal
        </a>
<?php }else{?>
        <a target="_blank" class="banner-getstarted btn btn-default get-started" onclick="window.location.href='<?php echo SITE_URL?>project/create'">
        Post your project
        </a>
<?php }?>
    </div>
</div>

<div class="project-proposal-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tag-container">
                    <div class="all-tags" data-menu="all-tags" id="tagsdropdown1">
                        <label >
                        All topics
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </label>
                    </div>
                    <div class="all-tags" id="popularTags">
                        <div class="tag-lists">
                            <span>Popular topics : </span>
                            <ul class="list-unstyled">
                                <li>
                                    <a href="<?php echo (SITE_URL.'project/project_proposals/'."Organizational-Change-Management");?>">
                                    Organizational change management
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo (SITE_URL.'project/project_proposals/'."Business-process-improvement");?>">
                                    Business process improvement
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo (SITE_URL.'project/project_proposals/'."Business-transformation");?>">
                                    Business transformation
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo (SITE_URL.'project/project_proposals/'."Succession-planning");?>">
                                    Succession planning
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="row t-listwrapper all-tags-section" id="topicbox1">
                        <div class="col-md-12">
                            <ul class="list-unstyled">
                                <?php sort($topiclist, SORT_NATURAL | SORT_FLAG_CASE);
                                    foreach ($topiclist as $key=>$catValue) {
                                              $str = ($catValue);
                                              if(false !== stripos(($catValue),"&"))
                                              $str = str_replace("&","_and",($catValue));
                                              if(false !== stripos(($catValue),"/"))
                                              $str = str_replace("/","_slash",($catValue));
                                              $str = str_replace(" ","-",$str);?>
                                <li>
                                    <a href="<?php echo (SITE_URL.'project/project_proposals/'.$str);?>"><?php echo($catValue);?></a>
                                </li>
                                <?php }?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="heading-acs">
                    <span>All project proposals</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="expert-list">
                    <div class="search-results qa-listing">
                        <?php 
                            $qCount = count($proposals); 
                            if($qCount > 0){
                            $countA = 1; 
                            foreach ($proposals as $key => $proposal) {?>
                        <div class="box1">
                            <div class="box-head">
                                <div class="fixed-head">
                                    <div class="content-left-img">
                                        <?php 
                                            if(isset($proposal['User']['user_image'])){
                                                $image_path = MENTORS_IMAGE_PATH.DS.$proposal['Proposal']['user_id'].DS.$proposal['User']['user_image'];?>
                                        <a href="<?php echo(SITE_URL.strtolower($proposal['User']['url_key']));?>">
                                        <img src="<?php echo(SITE_URL.'img/'.$image_path)?>" width="100" alt="<?php echo($proposal['User']['first_name']." ".$proposal['User']['last_name']);?>" title="<?php echo($proposal['User']['first_name']." ".$proposal['User']['last_name']);?>"/>
                                        </a>
                                        <?php } else {
                                            $image_path = 'profile.png';?>
                                        <a href="<?php echo(SITE_URL.strtolower($proposal['User']['url_key']));?>">
                                        <img src="<?php echo(SITE_URL.'img/media/'.$image_path)?>" width="100" alt="<?php echo($proposal['User']['first_name']." ".$proposal['User']['last_name']);?>" title="<?php echo($proposal['User']['first_name']." ".$proposal['User']['last_name']);?>"/>
                                        </a>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="content-right-text">
                                    <h3><?php echo($proposal['Proposal']['proposal_title'])?> <?php  echo($this->Html ->link("Read more",SITE_URL.'project/proposal/'.$proposal['Proposal']['proposal_url'],array('title'=>'View proposal')));?></h3>
                                    <p class="user-name"><span><?php echo($this->Html ->link($proposal['User']['first_name']." ".$proposal['User']['last_name'],SITE_URL.$proposal['User']['url_key']));?></span></p>
                                    <div class="text-content-box"></div>
                                    <div class="expertise-header">
                                        <ul class="tag-list">
                                            <?php foreach ($proposal['ProposalTopic'] as $catValue) {
                                                $str = ($categories[$catValue['topic_id']]);
                                                if(false !== stripos(($categories[$catValue['topic_id']]),"&"))
                                                $str = str_replace("&","_and",($categories[$catValue['topic_id']]));
                                                if(false !== stripos(($categories[$catValue['topic_id']]),"/"))
                                                $str = str_replace("/","_slash",($categories[$catValue['topic_id']]));
                                                $str = str_replace(" ","-",$str);?>
                                            <li>
                                                <a href="<?php echo(SITE_URL);?>project/project_proposals/<?php echo($str);?>">
                                                <?php echo($categories[$catValue['topic_id']]);?>
                                                </a>
                                            </li>
                                            <?php }?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $countA++; }}?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>-->
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>

<script type="text/javascript">
    // parallax
    if($(window).width() >991){
    	$('#banner-section').parallax();
    }
    
           $('.t-listwrapper').addClass('hide');
           $('.all-tags').on("click", function(e) {
               //e.preventDefault();
               $('.t-listwrapper').addClass('hide');
               var menu = $(this).attr('data-menu');
               $('.' + menu + '-section').removeClass('hide');
           });
</script>
<script type="text/javascript">
    var box_isvisible = 0;
    
    jQuery(document).ready(function(){
    
    
    var container = jQuery('#topicbox1');
    
    jQuery(document).mouseup(function (e)
    {
    
    
        if (!container.is(e.target) // if the target of the click isn't the container...
            && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
            container.hide();
        }
    });
    
    
    jQuery("#tagsdropdown1").click(function(){
    
        if(box_isvisible==1){
         box_isvisible = 0;
        
        document.getElementById("topicbox1").style.display="none";
        }else{
         
          box_isvisible = 1;
       document.getElementById("topicbox1").style.display="block";
       }
    });
    
    });
     
</script>