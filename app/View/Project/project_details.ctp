<div class="container-fluid projectCreated" id="apply-section">
	<div class="container create-section pad0">
		<?php if($this->data['Project']['status'] == 3 || $this->data['Project']['status'] == 4 || $this->data['Project']['status'] == 5 || $this->data['Project']['status'] == 6 || $this->data['Project']['status'] == 7 || $this->data['Project']['status'] == 8) {?>
		<div class="progresbar" style="margin: 40px 0 30px;">
			<div class="col-sm-8 pad40">
				<ul>
					<li class="active">
						<span>1</span>
						<div class="title">
							<p>Project Created</p>
						</div>
					</li>
					<?php if($this->data['Project']['status'] == 4 || $this->data['Project']['status'] == 5 || $this->data['Project']['status'] == 6 || $this->data['Project']['status'] == 7 || $this->data['Project']['status'] == 8) {?>
					<li class="active">
						<span>2</span>
						<div class="title">
							<p>Project approved</p>
						</div>
					</li>
					<?php }else{?>
					<li>
						<span>2</span>
						<div class="title">
							<p>Project approved</p>
						</div>
					</li>
					<?php }?>
					<?php if($this->data['Project']['status'] == 6 || $this->data['Project']['status'] == 7 || $this->data['Project']['status'] == 8) {?>
					<li class="active">
						<span>3</span>
						<div class="title">
							<p>Proposal accepted</p>
						</div>
					</li>
					<?php }else{?>
					<li>
						<span>3</span>
						<div class="title">
							<p>Proposal accepted</p>
						</div>
					</li>
					<?php }?>
					<?php if($this->data['Project']['status'] == 7 || $this->data['Project']['status'] == 8) {?>
					<li class="active">
						<span>4</span>
						<div class="title">
							<p>Project started</p>
						</div>
					</li>
					<?php }else{?>
					<li>
						<span>4</span>
						<div class="title">
							<p>Project started</p>
						</div>
					</li>
					<?php }?>
					<?php if($this->data['Project']['status'] == 8) {?>
					<li class="active">
						<span>5</span>
						<div class="title">
							<p>Project completed</p>
						</div>
					</li>
					<?php }else{?>
					<li>
						<span>5</span>
						<div class="title">
							<p>Project completed</p>
						</div>
					</li>
					<?php }?>
				</ul>
			</div>
		</div>
		<?php }?>

		<?php  $for_creator = ClassRegistry::init('User');
		$creator = $for_creator->find('first', array('conditions' => array('User.id' => $this->data['Project']['user_id'])));
		if($creator['User']['role_id'] == 2 && $this->data['Project']['status'] != '4'){?>
		<div class="col-md-4 col-sm-12 col-xs-12 pull-right clearboth create-right">
			<div class="right-apply">
				<div class="project-overview">
					<div class="overview-desc created-by">
						
						<?php $displink1=SITE_URL.strtolower($createdBy['User']['url_key']);?>
						
						<h5>Created By:
						<?php  echo($this->Html->link($createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'],$displink1,array('escape'=>false, 'target'=>'_blank')));?>
						</h5>
						<div class="created-person-img">
							<?php
								if(isset($createdBy['UserImage'][0]['image_name']) && $createdBy['UserImage'][0]['image_name'] !=''){
										$image_path = MENTORS_IMAGE_PATH.DS.$createdBy['User']['id'].DS.$createdBy['UserImage'][0]['image_name'];
										echo($this->Html->link($this->Html->image($image_path, array('class'=>'desaturate','alt' => $createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'])),$displink1,array('escape'=>false, 'target'=>'_blank','style'=>'border: 1px solid #DCDCDC; display:block; ')));
								}else{
										echo($this->Html->link($this->Html->image('media/profile.png', array('class'=>'desaturate','alt' => $createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'])),$displink1,array('escape'=>false, 'target'=>'_blank','style'=>'border: 1px solid #DCDCDC; display:block;')));
							}?>
							<?php  $mentorshipData = $this->General->getMentorshipSessionsWithClient($createdBy['User']['id']);
							if (empty($mentorshipData) == false && $mentorshipData['Mentorship']['applicationType'] != 1) { ?>
							<button class="btn btn-default get-started" onclick="window.location.href='<?php echo SITE_URL."invoice/invoice_create/".$mentorshipData['Mentorship']['id']."/".$createdBy['User']['id']; ?>';">
							Send Invoice
							</button>
							<?php }?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php } elseif($creator['User']['id'] == $this->Session->read('Auth.User.id') ){?>
		<div class="col-md-4 col-sm-12 col-xs-12 pull-right clearboth create-right">
			
			<?php $displink1=SITE_URL."clients/my_account/".strtolower($createdBy['User']['url_key']); ?>
			
			<div class="right-apply">
				<div class="project-overview">
					<div class="overview-desc created-by">
						<h5>Created By:
						<?php  echo($this->Html->link($createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'],$displink1,array('escape'=>false,'target'=>'_blank')));?>
						</h5>
						<div class="created-person-img">
							<?php
							if(isset($createdBy['UserImage'][0]['image_name']) && $createdBy['UserImage'][0]['image_name'] !=''){
									$image_path = MENTEES_IMAGE_PATH.DS.$createdBy['User']['id'].DS.$createdBy['UserImage'][0]['image_name'];
									echo($this->Html->link($this->Html->image($image_path, array('class'=>'desaturate','alt' => $createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank','style'=>'border: 1px solid #DCDCDC; display:block;')));
							}else{
									echo($this->Html->link($this->Html->image('media/profile.png', array('class'=>'desaturate','alt' => $createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank','style'=>'border: 1px solid #DCDCDC; display:block; ')));
							}?>
						</div>
					</div>
				</div>
			</div>
		</div>		
		<?php } elseif($creator['User']['role_id'] == 3 && ($this->data['Project']['status'] == '6' || $this->data['Project']['status'] == '7' || $this->data['Project']['status'] == '8')){?>
		<div class="col-md-4 col-sm-12 col-xs-12 pull-right clearboth create-right">
			
			<?php $displink1=SITE_URL."clients/my_account/".strtolower($createdBy['User']['url_key']); ?>
			
			<div class="right-apply">
				<div class="project-overview">
					<div class="overview-desc created-by">
						<h5>Created By:
						<?php  echo($this->Html->link($createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'],$displink1,array('escape'=>false,'target'=>'_blank')));?>
						</h5>
						<div class="created-person-img">
							<?php
							if(isset($createdBy['UserImage'][0]['image_name']) && $createdBy['UserImage'][0]['image_name'] !=''){
									$image_path = MENTEES_IMAGE_PATH.DS.$createdBy['User']['id'].DS.$createdBy['UserImage'][0]['image_name'];
									echo($this->Html->link($this->Html->image($image_path, array('class'=>'desaturate','alt' => $createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank','style'=>'border: 1px solid #DCDCDC; display:block;')));
							}else{
									echo($this->Html->link($this->Html->image('media/profile.png', array('class'=>'desaturate','alt' => $createdBy['UserReference']['first_name']." ".$createdBy['UserReference']['last_name'])),$displink1,array('escape'=>false,'target'=>'_blank','style'=>'border: 1px solid #DCDCDC; display:block; ')));
							}?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php  } else{?>
		<div class="col-md-4 col-sm-12 col-xs-12 pull-right clearboth create-right">
			<div class="right-apply">
				<div class="project-overview">
					<div class="overview-desc created-by">
						<h5>Created By:
						GUILD Client Partner
						</h5>
						<div class="created-person-img">
							<img src="<?php echo(SITE_URL)?>press_material/GUILD CLIENT PARTNER.jpg">
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php }?>
		
		<div class="col-md-8 pull-left pad40" id="projectnew">
			<?php  if($this->Session->read('Auth.User.role_id')== 1) {
			if($this->data['Project']['status'] == 0) {
			echo("Project created by Client");
			}else if($this->data['Project']['status'] == 1){
			echo("Project posted by Client");
			}else if($this->data['Project']['status'] == 2){
			echo("Client Partner call scheduled");
			}else if($this->data['Project']['status'] == 3){
			echo("Project submitted for Client approval");
			}else if($this->data['Project']['status'] == 4){
			echo("Project approved by Client");
			}else if($this->data['Project']['status'] == 5){
			echo("Proposal submitted");
			}else if($this->data['Project']['status'] == 6){
			echo("Proposal approved by Client");
			}else if($this->data['Project']['status'] == 7){
			echo("Project started");
			}else{
			echo("Project completed");
			}
			}?>
			<div class="project-overview">
				<?php echo($this->Form->hidden('Project.id'));?>
				<div class="overview-desc">
					<h5>Project title</h5>
					<p><?php echo($this->data['Project']['title']);?></p>
				</div>
				<div class="overview-desc">
					<h5>Project details</h5>
					<p> <?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['details']);
						echo $this->General->make_links(nl2br($details));?>
					</p>
				</div>
				<?php if(isset($this->data['Project']['start_date']) && $this->data['Project']['start_date'] != '' ) {?>
				<div class="overview-desc">
					<h5>Start</h5>
					<p><?php echo($this->data['Project']['start_date']);?></p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['budget']) && $this->data['Project']['budget'] != '' ) {?>
				<div class="overview-desc">
					<h5>Budget</h5>
					<p><?php echo($this->data['Project']['budget']);?></p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['success']) && $this->data['Project']['success'] != '' ) {?>
				<div class="overview-desc">
					<h5>Desired qualification</h5>
					<p> <?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['success']);
						echo $this->General->make_links(nl2br($details));?>
					</p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['industry']) && $this->data['Project']['industry'] != '' ) {?>
				<div class="overview-desc">
					<h5>Industry</h5>
					<p><?php echo($this->data['Project']['industry']);?> &#8212; <?php echo($this->data['Project']['category']);?></p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['business_need']) && $this->data['Project']['business_need'] != '' ) {?>
				<div class="overview-desc">
					<h5>Business need</h5>
					<p><?php echo($this->data['Project']['business_need']);?></p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['solution_type']) && $this->data['Project']['solution_type'] != '' ) {?>
				<div class="overview-desc">
					<h5>Solution type</h5>
					<p><?php echo($this->data['Project']['solution_type']);?></p>
				</div>
				<?php }?>
				
				<?php if(isset($this->data['Project']['location']) && $this->data['Project']['location'] != '' ) {?>
				<div class="overview-desc">
					<h5>Location preference</h5>
					<p><?php echo($this->data['Project']['location']);?></p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['duration']) && $this->data['Project']['duration'] != '' ) {?>
				<div class="overview-desc">
					<h5>Estimated duration</h5>
					<p><?php echo($this->data['Project']['duration']);?></p>
				</div>
				<?php }?>
				<?php  if($this->Session->read('Auth.User.role_id')== 3 || $this->Session->read('Auth.User.role_id')== '1' || ($this->Session->read('Auth.User.role_id')==2 && $this->Session->read('Auth.User.id') == $this->data['Project']['user_id'])) {
				if(isset($this->data['Project']['phone_number']) && $this->data['Project']['phone_number'] != '' ) {?>
				<div class="overview-desc">
					<h5>Phone number</h5>
					<p><?php echo($this->data['Project']['phone_number']);?></p>
				</div>
				<?php }}?>
				<?php if(isset($this->data['Project']['filepath']) && $this->data['Project']['filepath'] != '' ) {?>
				<div class="overview-desc">
					<h5>Attachment</h5>
					
					<?php $img = SITE_URL."img/".MENTEES_PROJECT_PATH.DS.$this->data['Project']['id'].DS.$this->data['Project']['filepath']; ?>
					<p class="attach-overview">
						<?php echo($this->Html->link($this->data['Project']['filepath'],$img,array('escape'=>true , 'target'=>'_blank'))); ?>
					</p>
				</div>
				<?php }?>
				
				


				


				
				
				

				
			</div>

			<!-- Client -->

			<div class="e-button pull-right">
				<button class="banner-getstarted btn btn-default get-started" onclick="window.open('<?php echo SITE_URL."project/projectproposal/".$this->data['Project']['id']; ?>');">
				Create a proposal
				</button>
			</div>





		</div>
		
		
	</div>
</div>

<div id="accept-modal" class="modal fade member-edit-popup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h3 class="modal-title">Project</h3>
			</div>
			<form id="frmedit-profile-m" class="bv-form">
				<div class="modal-body edit-profile-m">
					<div class="aoe-box">
						<div class="col-md-12">
							<p class="text-center">Are you sure?</p>
						</div>
					</div>
					<div class="button-set">
						<a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
						<a href="javascript:void(0);" class="btn btn-primary">Save</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- js link -->
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>


