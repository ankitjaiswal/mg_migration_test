<script src="https://checkout.stripe.com/checkout.js"></script>
<div class="page-content">
    <div class="container">
    	<div class="row">
    		<div class="col-sm-10 col-sm-offset-1">

                <div class="invoice-content">
                    <h4>Invoice</h4>
                    <div class="invoice-info">
                        <ul>
                            <li>
                                <label>Invoice No:</label>
                                <span><?php echo($invoicedata['ProjectInvoice']['invoiceno']);?></span>
                            </li>
                            <li>
                                <label>Invoice Date:</label>
                                <span><?php echo date('F d, Y', strtotime($invoicedata['ProjectInvoice']['created']));?></span>
                            </li>

                            <li>
                                <label>Payment Terms:</label>
                                <span>Net 7</span>
                            </li>
                        </ul>
                    </div>
                    <div class="invoice-from-to">
                    	 <p>To, <strong><?php echo($invoicedata['ProjectInvoice']['inv_client_name']);?></strong></p>
                    </div>
                    <div class="invoice-table">
                		<table class="table table-striped">
                    	    <thead>
	            	      		<tr>
	                	        	<th>Description</th>
	                	        	<th>Amount</th>
	                	      	</tr>
            	    		</thead>
                	    	<tbody>
                    	      	<tr>
                    	        	<td>For the consulting services delivered by <?php echo($invoicedata['ProjectInvoice']['inv_from']);?>:</br><?php echo($invoicedata['ProjectInvoice']['inv_description']);?></td>
                    	        	<td>$<?php echo($invoicedata['ProjectInvoice']['amount']);?></td>
                    	      	</tr>
                	    	</tbody>
                    	</table>
                    </div>
                    <div class="invoice-total-section">
                    	<div class="invoice-total-box">
                    		<ul>
                    			<li>
                    				<label>Discount</label>
                    				<span>$<?php echo($invoicedata['ProjectInvoice']['discount']);?></span>
                    			</li>
                    			<li>
                    				<label>Sub-total</label>
                    				<span>$<?php echo($invoicedata['ProjectInvoice']['subtotal']);?></span>
                    			</li>

                    			<li>
                    				<label>Tax</label>
                    				<span>$<?php echo($invoicedata['ProjectInvoice']['tax']);?></span>
                    			</li>
                    		</ul>
                    	</div>
                    	<div class="invoice-total">
                    		<ul>
                    			<li>
                    				<label>Total</label>
                    				<span>$<?php echo($invoicedata['ProjectInvoice']['total']);?></span>
                    			</li>
                    		</ul>
                    	</div>
                    </div>
                    <?php if(($invoicedata['ProjectInvoice']['inv_client_id'] == $this->Session->read('Auth.User.id')) && $invoicedata['ProjectInvoice']['invoicestatus']=="OPEN") {?>
			            <div class="text-right button-set right-btnset mt-30">
			                <input class="btn btn-primary" value="Pay Invoice" type="submit" id="customButton" />
			            </div>
	                <?php }?>
                    <div class="thanks-bar">
                    	<p class="thanks">Thank You!</span></p>
                    	<p>If you have a question, contact <a href="mailto:help@guild.im">help@guild.im</a> or call 1 (808) 729-5850.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="Language" value="<?php echo($invoicedata['ProjectInvoice']['id']);?>" id="invoiceId"> 
<input id="emailaddress" value="<?php echo($this->Session->read('Auth.User.username'));?>" type="hidden"> 
<input id="invtotal" value="<?php echo($invoicedata['ProjectInvoice']['total']);?>" type="hidden">


<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>


<script type="text/javascript">
	var invoiceId = jQuery('#invoiceId').val();
	var price = jQuery('#invtotal').val() *100;
        price  = Math.round(price);
        console.log(price);
	var email = jQuery('#emailaddress').val();
	console.log(invoiceId);
  	var handler = StripeCheckout.configure({
	    key: 'pk_live_vg2OVxfTe0Iw5FGSMhitLWE7',
	    image: 'https://www.guild.im/imgs/guild_monogram.png',
	    locale: 'auto',
	    token: function(token) {
			console.log("After payment Made");
		 	jQuery.ajax({
				url:SITE_URL+'project/stripecheckout',
				type:'post',
				dataType:'json',
				data: 'invoiceId='+invoiceId + '&price=' + price + '&email=' + email +'&token=' + token.id,
				success:function(invoiceId){
					window.location.href = SITE_URL+'project/thanks_for_invoicepayment';
				}
			});
    	}
  	});

  	jQuery('#customButton').click(function(){
		console.log("Button Clicked");
	    // Open Checkout with further options
	    handler.open({
      		name: 'GUILD',
	      	description: 'Invoice Payment',
	      	amount: price,
	      	email: email
	    });
		return false;
    	e.preventDefault();
  	});

  	// Close Checkout on page navigation
  	jQuery(window).on('popstate', function() {
    	handler.close();
  	});
</script>

<script type="text/javascript">
jQuery(document).ready(function() {
if(loggedUserId ==''){
jQuery('#signin-Modal').modal();
} else{
				//jQuery('#signin-Modal').modal();
			}
});
</script>

<!-- js link -->