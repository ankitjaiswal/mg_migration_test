<!--<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>guild</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="style.css">
  </head> 
  <body>-->
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
  


  <div class="row">
    <div class="container-fluid">
      <div class="col-md-12">
        <div class="panel-outer">
        	<div class="panel-title project">
                        <span>1</span><p>Project created</p>
                    </div>
                    <div class="panel-title project1">
                        <span>2</span><p>Project approved</p>
                    </div>
                    <div class="panel-title">
                        <span>3</span><p>Project accepted</p>
                    </div>
                    <div class="panel-title">
                        <span>4</span><p>Project started</p>
                    </div>
                    <div class="panel-title panel-title-1">
                        <span>5</span><p>Project completed</p>
            </div>            
                       
        </div>
    </div>
  </div>

  <div class="row">
    <div class="container">
      <div class="col-md-12">

      	<div class="col-md-8 pull-left pad40" id="projectnew">
<div class="project-overview">

 <div class="overview-desc">
<h5>Project title</h5>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut.</p>
</div>
<div class="overview-desc">
<h5>Project details</h5>
<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  </p>
</div>
<div class="overview-desc">
<h5>Start</h5>
<p>Within 1 month</p>
</div>
<div class="overview-desc">
<h5>Budget</h5>
<p>More than $50K</p>
</div>
<div class="overview-desc">
<h5>Desired qualification</h5>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
</div>
<div class="overview-desc">
<h5>industry</h5>
<p>Lorem ipsum dolor sit amet </p>
</div>
<div class="overview-desc">
<h5>Business need</h5>
<p>Lorem ipsum dolor sit amet </p>
</div>
<div class="overview-desc">
<h5>Solution type</h5>
<p>Lorem ipsum dolor sit amet </p>
</div>
<div class="overview-desc">
<h5>Location prefrence</h5>
<p>Lorem ipsum dolor sit amet </p>
</div>
<div class="overview-desc">
<h5>Estimated duration</h5>
<p>Lorem ipsum dolor sit amet </p>
</div>
</div>



</div>

      	<div class="col-md-4 col-sm-12 col-xs-12 pull-right clearboth create-right">
<div class="right-apply">
<div class="project-overview">
<div class="overview-desc created-by  part-2">
<h5>Client Partner</h5>
<div class="created-person-img">
<img src="https://test.guild.im/press_material/Iqbal-Ashraf-GUILD-Client-Partner.jpg">
</div>
<br>
<h5 style="color:#d03135;">Iqbal Ashraf</h5>
<h5><i class="fa fa-phone" aria-hidden="true"></i>
 <a href="tel:1-808-729-5850">1-808-729-5850</a></h5>
<h5><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:iqbal@guild.im">iqbal@guild.im</a></h5>
</div>
</div>
<div class="project-overview">
<div class="overview-desc created-by  part-3">
<h5>Consultants</h5>
<div class="created-person-img last">
<img src="images/2019-06-23.png">
</div>
<h5 style="color:#d03135;border:none;"><p class="davis">Robert Davis</p><img src="images/View Proposal icon.png" style="
    width: 15%;padding-left: 15px;"></h5>
<button type="button" class="btn btn-link" style="font-size: 18px;color: #d03135;">Ignore</button>
<button type="button" class="btn btn-danger">Accept</button>
</div>

<div class="created-person-img last1">
<img src="images/2019-06-23 (1).png">
</div>

<h5 style="color:#d03135;border:none;"><p class="davis1">Mike Dorman</p></h5>
<button type="button" class="btn btn-link" style="font-size: 18px;color: #d03135;">Ignore</button>
<button type="button" class="btn btn-danger">Accept</button>
</div>
</div>
</div>


       
  </div>

 
  <!--  </body>
</html>-->