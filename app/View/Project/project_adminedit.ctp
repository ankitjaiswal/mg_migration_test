<script type="text/javascript">

	
jQuery(document).ready(function(){
jQuery(function() {

	jQuery('select').change(function() {
		
		
		var old_type = this.id.substring(7);

		type = old_type.substring(0,8);
		
		if(type == 'Industry') {
                   console.log(1);
			var id = old_type.substring(8);
			
			var industry = this.value;
			
			var select_id = "#ProjectCategory"+id;
			
			var item = jQuery(select_id);
		
			item.find('option').remove();
				
							
			if(industry != -1) {
				 console.log(2);
				var cats = document.getElementById(industry); 
				
				var cat_array = cats.value.split("|");
				    //alert(cat_array);
                                var i = cat_array.indexOf("ALL CATEGORIES");
                                if(i != -1) {
	                        cat_array.splice(i, 1);
                                 }
                                 
                                     cat_array =  cat_array.sort();
                                     cat_array.push('ALL CATEGORIES');


				for (i = 0; i < cat_array.length; i++) {
					item.append('<option value="' + cat_array[i] + '">' + cat_array[i] + '</option>');
		        }
	      } else {
	      	item.append('<option value="-1"> </option>');	
	      }
	          
        }          
	});
});
});
</script>

<?php

    $ioptions = array();
    $count = 0;
    $options[-1] = '';


	$industry_options = array();
	$category_options = array();

	

    foreach ($industry_categories as $value) {
        
        $ioptions[$value['IndustryCategory']['id']] = $value['IndustryCategory']['category'];
		
		if($value['IndustryCategory']['parent'] == 0) 
			$industry_options[$value['IndustryCategory']['category']] = $value['IndustryCategory']['category'];
    }
    
	foreach ($industry_categories as $value) {
		
		if($value['IndustryCategory']['parent'] > 0)  {
					
			if($category_options[$ioptions[$value['IndustryCategory']['parent']]] == '')
				$category_options[$ioptions[$value['IndustryCategory']['parent']]] = $value['IndustryCategory']['category'];
			else
				$category_options[$ioptions[$value['IndustryCategory']['parent']]] = $category_options[$ioptions[$value['IndustryCategory']['parent']]].'|'.$value['IndustryCategory']['category'];
		}
    }
						
	foreach ($category_options as $key => $value) {
			?>
		<input type="hidden" name="<?php echo($key); ?>" value="<?php echo($value);?>" id="<?php echo($key); ?>" />
		<?php
	}
	$industry_options = array_unique($industry_options);
	
	$industry1 = '';
        $category1 ='';
	$category1_options = array();

        $category11_options = array();
       
        $category = $this->data['Project']['category'];



	if(isset($this->data['Project']) && isset($this->data['Project']['industry'])!= '') {

		$industry1 = $this->data['Project']['industry'];

						
		$category1 = $this->data['Project']['category'];

		$category11_options = explode("|",$category_options[$industry1]);


		foreach ($category11_options as $key => $value) {
			$category1_options[$value] = $value;
		}
		
	}

	
?>

<div class="content-wrap post-project" id="apply-section">
	<div class="container create-section">
		<div class="row">
			<div class="col-sm-8">
				<div class="progresbar" style="margin: 0 0 30px;">
					<ul>
						<li>
							<span>1</span>
							<div class="title">
								<p>Project Created</p>
							</div>
						</li>
						<li>
							<span>2</span>
							<div class="title">
								<p>Project approved</p>
							</div>
						</li>
						<li>
							<span>3</span>
							<div class="title">
								<p>Proposal accepted</p>
							</div>
						</li>
						<li>
							<span>4</span>
							<div class="title">
								<p>Project started</p>
							</div>
						</li>
						<li>
							<span>5</span>
							<div class="title">
								<p>Project completed</p>
							</div>
						</li>
					</ul>
				</div>
				<div class="apply-heading">
					<h5>Post a project</h5>
					<label class="free-c"> (Free & confidential)</label>
				</div>
				<div class="project-form">
					<form id="Projectform" enctype="multipart/form-data" method="post" action="<?php echo SITE_URL ?>project/project_adminedit" accept-charset="utf-8">
                                          <?php echo($this->Form->hidden('Project.id'));?>
					<div class="form-group">
						<label>Project Title</label>
						<span class="pull-right char-limit"><span id="noOfCharTitle"><?php echo(128 - strlen($this->data['Project']['title']))?></span> characters remaining</span>
						<input type="text" name="data[Project][title]" placeholder="Design and implement an employee incentive plan." class="form-control" id="project_title" value="<?php echo($this->data['Project']['title']);?>" data-toggle="tooltip" data-placement="right" title="Be specific" />
					</div>
					<div class="form-group">
						<label>Describe your need</label>
						<span class="pull-right char-limit"><span id="noOfCharDetails"><?php echo(5000 - strlen($this->data['Project']['details']))?></span> characters remaining</span>
						<textarea rows="8" name="data[Project][details]" placeholder="Please provide all relevant details about your project." class="form-control" id="project_details" data-toggle="tooltip" data-placement="right" title="What is the key business challenge or opportunity?"><?php echo($this->data['Project']['details']);?></textarea>
					</div>

					<div class="row">
						<div class="col-sm-6">
							<select class="form-control active" name="data[Project][start_date]" id="project_start_date" value="<?php echo($this->data['Project']['start_date']);?>">
								
                                                                <?php if($this->data['Project']['start_date'] ==''){?>
                                                                <option disabled selected>Start</option>
                                                                <?php }else{?>
                                                                <option disabled>Start</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['start_date'] =='Immediate'){?>
								<option selected>Immediate</option>
                                                                <?php }else {?> 
                                                                <option>Immediate</option>
                                                                <?php }?>

                                                                <?php if($this->data['Project']['start_date'] =='Within 1 month'){?>
								<option selected>Within 1 month</option>
                                                                <?php }else {?> 
                                                                <option>Within 1 month</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['start_date'] =='Other/ Not sure'){?>
								<option selected>Other/ Not sure</option>
                                                                <?php }else {?> 
                                                                <option>Other/ Not sure</option>
                                                                <?php }?>



							</select>
						</div>
						<div class="col-sm-6">
							<select class="form-control active" name="data[Project][budget]" id="project_budget" value="<?php echo($this->data['Project']['budget']);?>">
                                                                <?php if($this->data['Project']['budget'] ==''){?>
                                                                <option disabled selected>Budget</option>
                                                                <?php }else{?>
                                                                <option disabled>Budget</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['budget'] =='Less than $10K'){?>
								<option selected>Less than $10K</option>
                                                                <?php }else {?> 
                                                                <option>Less than $10K</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['budget'] =='$10K to $50K'){?>
								<option selected>$10K to $50K</option>
                                                                <?php }else {?> 
                                                                <option>$10K to $50K</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['budget'] =='More than $50K'){?>
								<option selected>More than $50K</option>
                                                                <?php }else {?> 
                                                                <option>More than $50K</option>
                                                                <?php }?>
								

							</select>
						</div>
					</div>
				</div>
				<div class="project-form extra-fields">
					<div class="form-group">
						<label>Describe the ideal professional for your project</label>
						<span class="pull-right char-limit"><span id="noOfCharSuccess"><?php echo(5000 - strlen($this->data['Project']['success']))?></span> characters remaining</span>
						<textarea rows="8" placeholder="Specify industry experience, key expertise, etc." name="data[Project][success]" class="form-control" data-toggle="tooltip" data-placement="right" title="What combination of industry and functional experience is most relevant?" id="project_success"><?php echo($this->data['Project']['success']);?></textarea>
					</div>

					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<select class="form-control active" value="<?php echo($industry1);?>" id="ProjectIndustry" name="data[Project][industry]">
									<option disabled="" selected="">Select Industry...</option>
                                                              <?php if($this->data['Project']['industry'] !=''){?>
                                                                  
                                                              <?php foreach ($industry_options as $key=>$industry) {?>
                                                                                     
                                                              <?php if($industry == $this->data['Project']['industry']){?>
								<option selected><?php echo($industry);?></option>
                                                              <?php }else{?>
                                                                  <option ><?php echo($industry);?></option>
                                                              <?php }?>
                                                                  <?php }}else{?>
                                                              <?php foreach ($industry_options as $key=>$industry) {?>
                                                                   

								<option><?php echo($industry);?></option>
                                                             <?php }}?>
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<select class="form-control active" value="<?php echo($category1);?>" id="ProjectCategory" name="data[Project][category]">
									<option disabled="" selected="">Select Category...</option>
                                                              <?php if($this->data['Project']['category'] !=''){?>
                                                                  
                                                              <?php foreach ($category1_options as $key=>$category) {?>
                                                                                     
                                                              <?php if($category == $this->data['Project']['category']){?>
								<option selected><?php echo($category);?></option>
                                                              <?php }else{?>
                                                                  <option ><?php echo($category);?></option>
                                                              <?php }?>
                                                                  <?php }}else{?>
                                                              <?php foreach ($category1_options as $key=>$category) {?>
                                                                   

								<option><?php echo($category);?></option>
                                                             <?php }}?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<select class="form-control active" id="business_need" name="data[Project][business_need]" value="<?php echo($this->data['Project']['business_need']);?>">

                                                                <?php if($this->data['Project']['business_need'] ==''){?>
                                                                <option disabled selected>My business need...</option>
                                                                <?php }else{?>
                                                                <option disabled>My business need...</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['business_need'] =='Strategic planning'){?>
								<option selected>Strategic planning</option>
                                                                <?php }else {?> 
                                                                <option>Strategic planning</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['business_need'] =='Connect with customers'){?>  
								<option selected>Connect with customers</option>
                                                                <?php }else {?> 
                                                                <option>Connect with customers</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['business_need'] =='Develop employees & culture'){?> 
								<option selected>Develop employees & culture</option>
                                                                <?php }else {?> 
                                                                <option>Develop employees & culture</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['business_need'] =='Improve a process'){?> 
                                                                <option selected>Improve a process</option>
                                                                <?php }else {?> 
                                                                <option>Improve a process</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['business_need'] =='Access industry best practices'){?> 
                                                                <option selected>Access industry best practices</option>
                                                                <?php }else {?> 
                                                                <option>Access industry best practices</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['business_need'] =='Drive reorganization'){?> 
                                                                <option selected>Drive reorganization</option>
                                                                <?php }else {?> 
                                                                <option>Drive reorganization</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['business_need'] =='Project support / Other'){?> 
                                                                <option selected>Project support / Other</option>
                                                                <?php }else {?> 
                                                                <option>Project support / Other</option>
                                                                <?php }?>
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<select class="form-control active" id="solution_type" name="data[Project][solution_type]" value="<?php echo($this->data['Project']['solution_type']);?>">
                                                                <?php if($this->data['Project']['solution_type'] ==''){?>
                                                                <option disabled selected>Solution type...</option>
                                                                <?php }else{?>
                                                                <option disabled>Solution type...</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['solution_type'] =='Advisory support'){?>
								<option selected>Advisory support</option>
                                                                <?php }else{?>                   
								<option >Advisory support</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['solution_type'] =='Market research report'){?>
								<option selected>Market research report</option>
                                                                <?php }else{?>
								<option>Market research report</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['solution_type'] =='Executive coaching'){?>
								<option selected>Executive coaching</option>
                                                                <?php }else{?>
								<option>Executive coaching</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['solution_type'] =='Consulting project'){?>
                                                                <option selected>Consulting project</option>
                                                                <?php }else{?>
                                                                <option>Consulting project</option>
                                                                <?php }?>
                                                                <?php if($this->data['Project']['solution_type'] =='Interim management'){?>
                                                                <option selected>Interim management</option>
                                                                <?php }else{?>
                                                                <option>Interim management</option>
                                                                <?php }?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<select class="form-control active" name="data[Project][duration]" value="<?php echo($this->data['Project']['duration']);?>">
                                                                        <?php if($this->data['Project']['duration'] ==''){?>
                                                                        <option disabled selected>Estimated duration...</option>
                                                                        <?php }else{?>
                                                                        <option disabled>Estimated duration...</option>
                                                                        <?php }?>
                                                                        <?php if($this->data['Project']['duration'] =='Less than 1 Month'){?>
									<option>Less than 1 Month</option>
                                                                        <?php }else{?>
									<option>Less than 1 Month</option>
                                                                        <?php }?>
                                                                        <?php if($this->data['Project']['duration'] =='1 to 3 Months'){?>
									<option selected>1 to 3 Months</option>
                                                                        <?php }else{?>
									<option>1 to 3 Months</option>
                                                                        <?php }?>
                                                                        <?php if($this->data['Project']['duration'] =='3 to 6 Months'){?>
									<option selected>3 to 6 Months</option>
                                                                        <?php } else if($this->data['Project']['duration'] ==''){?>
									<option >3 to 6 Months</option>
                                                                        <?php }else{?>
									<option >3 to 6 Months</option>
                                                                        <?php }?>
                                                                        <?php if($this->data['Project']['duration'] =='Greater than 6 Months'){?>
									<option selected>Greater than 6 Months</option>
                                                                        <?php }else{?>
									<option>Greater than 6 Months</option>
                                                                        <?php }?>
                                                                        <?php if($this->data['Project']['duration'] =='Flexible'){?>
									<option selected>Flexible</option>
                                                                        <?php }else{?>
									<option>Flexible</option>
                                                                        <?php }?>
							       </select>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group">
								<input type="text" placeholder="Location Preference" class="form-control" name="data[Project][location]" id="project_location" value="<?php echo($this->data['Project']['location']);?>" />
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="form-attachment">
							<input type="file" id="exampleInputFile" name="data[Resume1][resume_location][name]"  value="<?php echo($this->data['Project']['filepath']);?>" data-toggle="tooltip" data-placement="right" title="Upload your RFP or Subcontractor Agreement(PDF, 2MB limit)" />
							                                  <span id="resume_old"  class="errormsg"></span>
                   		                                      <?php if(!empty($this->data['Project']) && isset($this->data['Project']['filepath']) && $this->data['Project']['filepath'] != ''){?>
                    			                             <?php 
                            	                                     $img = SITE_URL."img/".MENTEES_PROJECT_PATH.DS.$this->data['Project']['id'].DS.$this->data['Project']['filepath']; ?>
                            	
                            	                                     <p></br>Attached:  <?php echo($this->Html->link($this->data['Project']['filepath'],$img,array('escape'=>true , 'target'=>'_blank'))); ?></p>
                    			                          <?php }?>
								<div id="popover-content3" class="hide">
									<p>Upload your RFP or Subcontractor Agreement (PDF, 2MB limit)</p>
                                                                        
 								</div>
						</div>
					</div>
				</div>
				<div class="e-button pull-right">
					<label>
						<a href="javascript:;" class="show-fields hide">Hide optional fields</a>
						<a href="javascript:;" class="hide-fields ">Show optional fields</a>
					</label>
					<button class="banner-getstarted btn btn-default get-started" id="projectsubmit">Save and preview</button>
				</div>
			</div>
                       </form>
			<div class="col-sm-4">
				<div class="apply-heading space-top">
					<h5>How does it work?</h5>
				</div>
				<div class="cr-desc">
					<h5>A. Specify Business Need</h5>
					<p>Post your high-level requirement using our 1-page form.</p>
				</div>
				<div class="cr-desc">
					<h5>B. Review Proposals</h5>
					<p>A Client Partner will help you finalize your project needs. Then, in 2-3 days they will get back to you with proposals and recommend specialist(s) for the job.</p>
				</div>
				<div class="cr-desc">
					<h5>C. Get work done</h5>
					<p>Once you approve a proposal we will get to work. The Client Partner will ensure all deliverables are met on a timely basis.</p>
				</div>

				<div class="apply-heading">
					<h5>Not ready to post yet?</h5>
				</div>
				<div class="cr-desc mt0">
					<p>Call us to discuss your business needs.</p>
					<p>Call <a href="tel:1-866-511-1898">1-866-511-1898</a>.</p>
				</div>
			</div>


		</div>
	</div>
</div>

			<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/script12.js"></script>			
			<script type="text/javascript">
				jQuery(document).ready(function(){
					jQuery(function() {
						jQuery('select').change(function() {
							var old_type = this.id.substring(7);
							type = old_type.substring(0,8);							
							if(type == 'Industry') {
			                   	console.log(1);
								var id = old_type.substring(8);								
								var industry = this.value;								
								var select_id = "#ProjectCategory"+id;								
								var item = jQuery(select_id);							
								item.find('option').remove();
								if(industry != -1) {
									 console.log(2);
									var cats = document.getElementById(industry);
									var cat_array = cats.value.split("|");
									    //alert(cat_array);
		                                var i = cat_array.indexOf("ALL CATEGORIES");
					                    if(i != -1) {
				                        	cat_array.splice(i, 1);
	                                 	}					                                 
                                     	cat_array =  cat_array.sort();
                                     	cat_array.push('ALL CATEGORIES');
									for (i = 0; i < cat_array.length; i++) {
										item.append('<option value="' + cat_array[i] + '">' + cat_array[i] + '</option>');
							        }
						      } else {
						      	item.append('<option value="-1"> </option>');	
						      }						          
					        }          
						});
					});
					jQuery(".show-fields").click(function () {
			            jQuery(".extra-fields").hide("slow");
			            jQuery(".hide-fields").removeClass("hide");
			            jQuery(".show-fields").addClass("hide");
			        });
					jQuery(".hide-fields").click(function () {
			            jQuery(".extra-fields").show("slow");
			            jQuery(".hide-fields").addClass("hide");
			            jQuery(".show-fields").removeClass("hide");
			        });

			        jQuery(function () {
					  jQuery('[data-toggle="tooltip"]').tooltip()
					})
				});
			</script>

<script type="text/javascript">
jQuery(document).ready(function(){

	function updateCountTitle ()
    {
        var qText = jQuery("#project_title").val();

       if(qText.length < 128) {
           jQuery("#noOfCharTitle").html(128 - qText.length);
       } else {
           jQuery("#noOfCharTitle").html(0);
           jQuery("#project_title").val(qText.substring(0,128));
       }
    }

    jQuery("#project_title").keyup(function () {
    	updateCountTitle();
    });
    jQuery("#project_title").keypress(function () {
    	updateCountTitle();
    });
    jQuery("#project_title").keydown(function () {
    	updateCountTitle();
    });

	function updateCountDetails ()
    {
        var qText = jQuery("#project_details").val();

       if(qText.length < 5000) {
           jQuery("#noOfCharDetails").html(5000 - qText.length);
       } else {
           jQuery("#noOfCharDetails").html(0);
           jQuery("#project_details").val(qText.substring(0,5000));
       }
    }

    jQuery("#project_details").keyup(function () {
    	updateCountDetails();
    });
    jQuery("#project_details").keypress(function () {
    	updateCountDetails();
    });
    jQuery("#project_details").keydown(function () {
    	updateCountDetails();
    });


	function updateCountSuccess ()
    {
        var qText = jQuery("#project_success").val();

       if(qText.length < 5000) {
           jQuery("#noOfCharSuccess").html(5000 - qText.length);
       } else {
           jQuery("#noOfCharSuccess").html(0);
           jQuery("#project_success").val(qText.substring(0,5000));
       }
    }

    jQuery("#project_success").keyup(function () {
    	updateCountSuccess();
    });
    jQuery("#project_success").keypress(function () {
    	updateCountSuccess();
    });
    jQuery("#project_success").keydown(function () {
    	updateCountSuccess();
    });

    
	var flag1 = 0;
	jQuery("#exampleInputFile").change(function () {

		var uploadedFile=jQuery("#exampleInputFile").val();
		if(jQuery.trim(uploadedFile!='')) {	
		var resumeArr = uploadedFile.split(".");			
		var ext = ["pdf" ]; // Creating Extension array 
			if(jQuery.inArray(resumeArr[1], ext)==-1){ // Checking value of extension
				jQuery("#exampleInputFile").css('border','1px solid #F00');
				jQuery("#resume_old").css('color','#F00');
				jQuery("#resume_old").html("Upload only "+ext);	
				flag1++;
			}else{
				jQuery('#exampleInputFile').css('border-color', '');
				jQuery("#resume_old").html('');
				var iSize = (jQuery("#exampleInputFile")[0].files[0].size / 1024);				
				var res = ValidateFileSize(iSize,uploadedFile);
				if(!res){
					flag1++;
				}else{
					flag1 = 0;
				}
			}
		}else{
			flag1 = 0;
		}
								
	});

	function ValidateFileSize(iSize,Filename){
		var size=Math.round((iSize / 1024) * 100) / 100;
		if (size <2)
		{
					
			jQuery("#resume_old").html('');
			jQuery('#exampleInputFile').css('border-color', '');
			return true;
		}else{
			var ErrorMessage = "2MB size limit";
			
			jQuery("#resume_old").html(ErrorMessage);
			jQuery("#resume_old").css('color','#F00')
			jQuery("#exampleInputFile").css('border','1px solid #F00');
			return false;
		}

	}
	
	

	jQuery("#projectsubmit").click(function(){


	   var title = jQuery("#project_title").val();
	   var details = jQuery("#project_details").val();
           var project_start_date = jQuery("#project_start_date").val();
           var project_budget = jQuery("#project_budget").val();


	   var flag = 0;
   		if(jQuery.trim(title) == '') {
			jQuery('#project_title').css('border-color','#F00');
			jQuery('#project_title').focus();
			flag++;
		} else {
			jQuery('#project_title').css('border-color',''); 
		}

   		if(jQuery.trim(details) == '') {
			jQuery('#project_details').css('border-color','#F00');
			jQuery('#project_details').focus();
			flag++;
		} else {
			jQuery('#project_details').css('border-color','');
		}


   		if(jQuery.trim(project_start_date) == '') {
			jQuery('#project_start_date').css('border-color','#F00');
			jQuery('#project_start_date').focus();
			flag++;
		} else {
			jQuery('#project_start_date').css('border-color','');
		}
   		if(jQuery.trim(project_budget) == '') {
			jQuery('#project_budget').css('border-color','#F00');
			jQuery('#project_budget').focus();
			flag++;
		} else {
			jQuery('#project_budget').css('border-color','');
		}
		if(flag > 0) {
			return false;
		} else {
		      flag = 0;
		}
    });

});
</script>
<style type="text/css" media="screen">
.progresbar{
    display: inline-block;
    width: 100%;
    float: left;
}
.progresbar ul{
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
	display: flex;
	position: relative
}
.progresbar ul li:after{
	position: absolute;
	content: '';
	left: 30px;
	right: 30px;
	top: 20px;
	height: 2px;
	background: #DDDDDD;
	z-index: 0;
}
.progresbar ul li.active:nth-child(2):after {
    background: #CA2E32;
    right: 70%;
    z-index: 1;
}
.progresbar ul li.active:nth-child(3):after {
    background: #CA2E32;
    right: 50%;
    z-index: 1;
}
.progresbar ul li.active:nth-child(4):after {
    background: #CA2E32;
    right: 30%;
    z-index: 1;
}
.progresbar ul li.active:nth-child(5):after {
    background: #CA2E32;
    right: 30px;
    z-index: 1;
}
.progresbar ul li.active span{
	background: #CA2E32;
	color: #fff;
}
/*.progresbar ul li.active span:after {
    content: '';
    display: block;
    width: 10px;
    height: 20px;
    border: solid #fff;
    border-width: 0 4px 4px 0;
    transform: rotate(45deg);
    top: 9px;
    left: 0;
    position: absolute;
    right: 0;
    margin: 0 auto;
}*/
.progresbar ul li span{
	background: #DDDDDD;
	height: 40px;
	width: 40px;
	display: block;
	margin: 0 auto;
	border-radius: 50%;
	line-height: 40px;
	font-size: 14px;
	color: #000;
	text-align: center;
	z-index:2;
	position: relative
}
.progresbar ul li .title{
	display: inline-block;
	width: 100%;
	float: left;
	margin: 10px 0 0;
}
.progresbar ul li .title p{
	float: left;
	width: 100%;
	text-align: center;
	display: inline-block;
	max-width: 80px;
	font-size: 12px;
}

@media screen and (max-width: 640px){
	.progresbar ul li span{
		height: 30px;
		width:30px;
		line-height: 30px;
	}
	.progresbar ul li:after{
		top: 15px;
	}
	.progresbar ul li.active span:after{
		width: 8px;
		top: 7px;
    	height: 15px;
	}
}
</style>