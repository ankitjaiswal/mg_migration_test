<div class="container-fluid projectCreated" id="apply-section">
	<div class="container create-section pad0">
		<div class="col-md-12">
			<div class="p-members-table">
				<h5>My Projects</h5>
				<table class="table table-striped smartresponsive-table">
					<thead>
						<tr>
							<th>Created by</th>
							<th>Date</th>
							<th>Project title</th>
                                                        <th>Project status</th>
                                                        <th></th>
						</tr>
					</thead>
					<tbody>
						<?php if(!empty($data) && count($data)>0)
						{
						$i=1;
						foreach($data as $project){
						
					        $consultant = ClassRegistry::init('Project_member_confirm');
					        $show_interest = $consultant->find('first', array('conditions' => array('AND' =>array('Project_member_confirm.project_id ' => $project['Project']['id'] ,'Project_member_confirm.member_id' => $this->Session->read('Auth.User.id')))));
					        $proposal = ClassRegistry::init('Proposal');
					        $proposal_create = $proposal->find('first', array('conditions' => array('AND' =>array('Proposal.projectid ' => $project['Project']['id'] ,'Proposal.user_id' => $this->Session->read('Auth.User.id')))));
						
                                                $for_creator = ClassRegistry::init('User');
						$creator = $for_creator->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));
						if($creator['User']['role_id'] == 2)
						$displink1=SITE_URL.strtolower($creator['User']['url_key']);
						else
						$displink1=SITE_URL."clients/my_account/".strtolower($creator['User']['url_key']);
						?>
                                                <?php if($show_interest['Project_member_confirm']['status'] != '-1'){?>
						<tr>
                                                        <?php if(($project['Project']['status'] == 8 || $project['Project']['status'] == 9) && $creator['User']['id'] != $this->Session->read('Auth.User.id')){?>
							<td data-th="Created by"><?php  echo($this->Html->link(ucfirst($creator['UserReference']['first_name'])." ".ucfirst($creator['UserReference']['last_name']),$displink1,array('escape'=>false,'target'=>'_blank')));?></td>
                                                        <?php }else if($creator['User']['id'] == $this->Session->read('Auth.User.id')){?>
							<td data-th="Created by"><?php  echo($this->Html->link(ucfirst($creator['UserReference']['first_name'])." ".ucfirst($creator['UserReference']['last_name']),$displink1,array('escape'=>false,'target'=>'_blank')));?></td>
                                                        <?php }else{?>
							<td data-th="Created by">GUILD Client Partner</td>
                                                        <?php }?>


							<td data-th="Date"><?php echo date("m-d-Y", strtotime($project['Project']['created']));?></td>
							<td data-th="Project title"><a href="<?php echo (SITE_URL);?>project/index/<?php echo($project['Project']['project_url']);?>"><?php echo $project['Project']['title']; ?></a></td>

									<?php if($project['Project']['status'] == 0) {?>
									<td data-th="Action"><?php echo($this->Html->link('Create Project',array('controller'=>'project','action'=>'view/'.$project['Project']['id']),array('class'=>'delete','title'=>'View project')));?></td>

									<?php } else if($project['Project']['status'] == 1) { ?>
									<td data-th="Action"><?php echo($this->Html->link('Schedule call with Client Partner',array('controller'=>'project','action'=>'schedule_call/'.$project['Project']['id']),array('class'=>'delete','title'=>'Schedule call with Client Partner')));?></td>

									<?php } else if($project['Project']['status'] == 2) { ?>
									<td data-th="Action">Waiting for Client Partner review</td>

									<?php } else if($project['Project']['status'] == 3) { ?>
									<td data-th="Action"><?php echo($this->Html->link('Approve Project',array('controller'=>'project','action'=>'index/'.$project['Project']['project_url']),array('class'=>'delete','title'=>'Approve Project'))); ?></td>

									<?php } else if($project['Project']['status'] == 4) { ?>
									<td data-th="Action"><?php echo($this->Html->link('Pay Deposit to submit Project',array('controller'=>'project','action'=>'stripe_payment/'.$project['Project']['id']),array('class'=>'delete','title'=>'Pay Deposit to submit Project')));?></td>

									<?php } else if($project['Project']['status'] == 5 && $project['Project']['user_id'] == $this->Session->read('Auth.User.id')) { ?>
									<td data-th="Action">Waiting for Proposals</td>

									<?php } else if($project['Project']['status'] == 5 && $project['Project']['user_id'] != $this->Session->read('Auth.User.id') && ($proposal_create['Proposal']['proposal_url'] =='' && $show_interest['Project_member_confirm']['status'] == '0')) { ?>
									<td data-th="Action">Interest confirmed.</br><?php echo($this->Html->link('Add proposal',array('controller'=>'project','action'=>'projectproposal/'.$project['Project']['id']),array('class'=>'delete','title'=>'Add proposal'))); ?>.</td>

									<?php } else if($project['Project']['status'] == 5 && $project['Project']['user_id'] != $this->Session->read('Auth.User.id') && ($proposal_create['Proposal']['proposal_url'] !='' && $show_interest['Project_member_confirm']['status'] == '0')) { ?>
									<td data-th="Action">Proposal sent.</br>Waiting for response.</td>

									<?php } else if($project['Project']['status'] == 6 && $project['Project']['user_id'] == $this->Session->read('Auth.User.id')) { ?>
									<td data-th="Action"><?php echo($this->Html->link('Review Proposal',array('controller'=>'project','action'=>'index/'.$project['Project']['project_url']),array('class'=>'delete','title'=>'Review Proposal'))); ?></td>

									<?php } else if($project['Project']['status'] == 6 && $project['Project']['user_id'] != $this->Session->read('Auth.User.id') && ($proposal_create['Proposal']['proposal_url'] =='' && $show_interest['Project_member_confirm']['status'] == '0')) { ?>
									<td data-th="Action">Interest confirmed.</br><?php echo($this->Html->link('Add proposal',array('controller'=>'project','action'=>'projectproposal/'.$project['Project']['id']),array('class'=>'delete','title'=>'Add proposal'))); ?>.</td>

									<?php } else if($project['Project']['status'] == 6 && $project['Project']['user_id'] != $this->Session->read('Auth.User.id') && ($proposal_create['Proposal']['proposal_url'] !='' && $show_interest['Project_member_confirm']['status'] == '0')) { ?>
									<td data-th="Action">Proposal sent.</br>Waiting for response.</td>

									<?php } else if($project['Project']['status'] == 7 && $project['Project']['user_id'] == $this->Session->read('Auth.User.id')) { ?>
                                                                        <div id="<?php echo($project['Project']['id']); ?>">
									<td data-th="Action"><a style="cursor:pointer;" data-toggle="modal" data-target="#project-start-modal" class="btnxyz" id="accept<?php echo($project['Project']['id']); ?>">Start project</a><input type="hidden"  class="txtabc" value="<?php echo($project['Project']['id']); ?>"></td>
                                                                         </div>
									<?php } else if($project['Project']['status'] == 7 && $project['Project']['user_id'] != $this->Session->read('Auth.User.id') && $show_interest['Project_member_confirm']['status'] == '1') { ?>
                                                                        <div id="<?php echo($project['Project']['id']); ?>">
									<td data-th="Action">Client confirmed. Waiting to start.</td>
                                                                        
                                                                        </div>
									<?php } else if($project['Project']['status'] == 8 && $project['Project']['user_id'] == $this->Session->read('Auth.User.id')) { ?>
                                                                        <div id="<?php echo($project['Project']['id']); ?>">
									<td data-th="Action"><a style="cursor:pointer;" data-toggle="modal" data-target="#project-close-modal" class="btnxyz" id="accept<?php echo($project['Project']['id']); ?>">Close project</a><input type="hidden"  class="txtabc" value="<?php echo($project['Project']['id']); ?>"></td>
                                                                        </div>
									<?php } else if($project['Project']['status'] == 8 && $project['Project']['user_id'] != $this->Session->read('Auth.User.id')) { ?>
                                                                        <div id="<?php echo($project['Project']['id']); ?>">
									<td data-th="Action">Project in-progress.</td>
                                                                        </div>

									<?php } else if($project['Project']['status'] == 9) { ?>
									<td data-th="Action">Project completed</td>
                  
                                                              

									<?php }?>
                                                        <?php if($creator['User']['url_key'] != $this->Session->read('Auth.User.url_key') && $project['Project']['status'] == 8 && $show_interest['Project_member_confirm']['status'] == '1'){?>

                                                        <td data-th="Send Invoice"><a href="javascript:void(0);" onclick="window.location.href='<?php echo SITE_URL."project/invoice_create/".$project['Project']['id']; ?>';" class="delete" title="Send Invoice">Send Invoice</a></td>
                                                        <?php }else{?>

                                                        <td data-th="Send Invoice"></td>

                                                        <?php }?>
						</tr>
						<?php  $i++;  }}?>
						
						
						<?php
						} else{?>
						<tr style="text-align: center;">
							<td colspan="7">No record found</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- js link -->
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>bala/js/jssor.slider-22.0.15.mini.js"></script>
