<script defer type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
    <div id="banner-section" class="lenses-page hPage" data-parallax="scroll" data-image-src="<?php echo(SITE_URL)?>yogesh_new1/images/projecthome.png">
        <div class="col-md-12 pad0 text-center projectHome banner-content">
            <img src="<?php echo(SITE_URL)?>yogesh_new1/images/bg_icon.png" alt="" class="bg-icon">
            <h1>Business Expertise. On Demand.</h1>
            <p>Access relevant expertise in areas of coaching, financial management, revenue growth, M&A, and more.</p>
            <!-- <div class="business-step">
                <ul class="list-unstyled">
                    <li>
                        <label>1</label>
                        <span>Post Your Project</span>
                    </li>
                    <li>
                        <label>2</label>
                        <span>Review Candidates for free</span>
                    </li>
                    <li>
                        <label>3</label>
                        <span>Get Work Done</span>
                    </li>
                </ul>
            </div> -->
            <a onclick="window.location.href='<?php echo SITE_URL?>project/create'" target="_blank" class="banner-getstarted btn btn-default get-started">
                GET STARTED NOW
            </a>
        </div>
    </div>
    <div class="trusted-section">
        <div class="trusted-heading">
            <h6>OUR CONSULTANTS ARE TRUSTED BY</h6>
        </div>
        <div class="col-md-12 trusted-logo">
            <ul class="list-unstyled">
                <li>
                    <img src="<?php echo(SITE_URL)?>yogesh_new1/images/projects/1.png">
                </li>
                <li>
                    <img src="<?php echo(SITE_URL)?>yogesh_new1/images/projects/2.png">
                </li>
                <li>
                    <img src="<?php echo(SITE_URL)?>yogesh_new1/images/projects/3.png">
                </li>
                <li>
                    <img src="<?php echo(SITE_URL)?>yogesh_new1/images/projects/4.png">
                </li>
                <li>
                    <img src="<?php echo(SITE_URL)?>yogesh_new1/images/projects/5.png">
                </li>
                <li>
                    <img src="<?php echo(SITE_URL)?>yogesh_new1/images/projects/6.png">
                </li>
                <li>
                    <img src="<?php echo(SITE_URL)?>yogesh_new1/images/projects/7.png">
                </li>
                <li>
                    <img src="<?php echo(SITE_URL)?>yogesh_new1/images/projects/8.png">
                </li>
                <li>
                    <img src="<?php echo(SITE_URL)?>yogesh_new1/images/projects/9.png">
                </li>
                <li>
                    <img src="<?php echo(SITE_URL)?>yogesh_new1/images/projects/10.png">
                </li>
                <li>
                    <img src="<?php echo(SITE_URL)?>yogesh_new1/images/projects/11.png">
                </li>
            </ul>
        </div>
        <label>And Scores of small and Mid Sized Organizations.</label>
    </div>
    <div class="col-md-12 why-guild">
        <div class="w-heading text-center">
            <h5>Why GUILD?</h5>
        </div>
        <div class="container why-container">
            <div class="col-md-3 nn-img">
                <img src="<?php echo(SITE_URL)?>yogesh_new1/images/projects/vetted.svg">
                <label>VETTED</label>
                <span>Engage nation's leading Coaches, Functional and Industry experts.</span>
            </div>
            <div class="col-md-3 nn-img">
                <img src="<?php echo(SITE_URL)?>yogesh_new1/images/projects/cost-effective3.svg">
                <label>COST EFFECTIVE</label>
                <span>Eliminate the sacrifice between Cost and Quality through innovation.</span>
            </div>
            <div class="col-md-3 nn-img">
                <img src="<?php echo(SITE_URL)?>yogesh_new1/images/projects/ondemand.svg">
                <label>ON DEMAND</label>
                <span>Instantly receive relevant applicants to your posted projects. No bottlenecks.</span>
            </div>
            <div class="col-md-3 nn-img">
                <img src="<?php echo(SITE_URL)?>yogesh_new1/images/projects/project_service_gurantee_final.svg">
                <label>GUARANTEE</label>
                <span>We get our skin in the game with our unique <a href="<?php echo(SITE_URL)?>fronts/guarantee">Service Guarantee</a>.</span>
            </div>
        </div>
    </div>
    <div class="col-md-12 member-wrapper">
        <div class="w-heading text-center">
            <h5>GUILD Members</h5>
            <p>Representatives profiles from a community of 8,000 Top Business Experts:</p>
        </div>
        <div class="container">
            <div class="memberData">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12"> 
                        <div class="featured-profile-box-home featured-profile-box expert">
                             <div class="box-head"></div>
                             <img class="box-bg-img" src="<?php echo SITE_URL?>/yogesh_new1/images/homepage_image/box-top.png">
                             <div class="meta-box">
                                 <img src="<?php echo(SITE_URL)?>imgs/Michael-Stratford-Guild.jpg" style="width:85px;">
                                 <div class="meta-info">
                                    <h6>Michael Stratford</h6>
                                    <p>Laguna Hills, CA</p>
                                 </div>
                             </div>
                             <div class="pro-post">
                                <p>Executive Coach</p>
                             </div>
                             <div class="pro-info">
                                 <p>Michael is a champion of uniqueness. It’s been said he’s a blend of Robin Williams, and the Dalai lama. That doesn’t include the business savvy, range and depth of experience garnered from 56 different jobs, his clearly provocative nature...</p>
                             </div>
                             <div class="box-bottom">
                                <a href="<?php echo(SITE_URL)?>michael.stratford">View Profile</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="featured-profile-box-home featured-profile-box expert">
                             <div class="box-head"></div>
                             <img class="box-bg-img" src="<?php echo SITE_URL?>/yogesh_new1/images/homepage_image/box-top.png">
                             <div class="meta-box">
                                 <img src="<?php echo(SITE_URL)?>imgs/Drumm-McNaughton-Guild.jpg" style="width:85px;">
                                 <div class="meta-info">
                                    <h6>Drumm McNaughton</h6>
                                    <p>Fallbrook, CA</p>
                                 </div>
                             </div>
                             <div class="pro-post">
                                <p>Strategy Implementation</p>
                             </div>
                             <div class="pro-info">
                                 <p>Dr. Drumm McNaughton is a recognized thought leader in change management and leadership, and one of a select group with senior leadership experience in academe, nonprofits, and business. His 21st century approach...</p>
                             </div>
                             <div class="box-bottom">
                                <a href="<?php echo(SITE_URL)?>drumm.mcnaughton">View Profile</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="featured-profile-box-home featured-profile-box expert">
                             <div class="box-head"></div>
                             <img class="box-bg-img" src="<?php echo SITE_URL?>/yogesh_new1/images/homepage_image/box-top.png">
                             <div class="meta-box">
                                 <img src="<?php echo(SITE_URL)?>imgs/Bruce-LaFetra-Guild.jpg" style="width:85px;">
                                 <div class="meta-info">
                                    <h6>Bruce LaFetra</h6>
                                    <p>Sunnyvale, CA</p>
                                 </div>
                             </div>
                             <div class="pro-post">
                                <p>Marketing Strategy</p>
                             </div>
                             <div class="pro-info">
                                 <p>My job is to draw out insights and craft a marketing strategy that reflects a firm's specific situation, available resources, and firm culture. I start by talking to your clients. Focusing on why your clients see your firm as the best choice...</p>
                             </div>
                            <div class="box-bottom">
                                <a href="<?php echo(SITE_URL)?>Bruce.LaFetra">View Profile</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 post-section">
        <div class="w-heading text-center">
            <p>Get immediate assistance from relevant experts! It's FREE to post:</p>
            <a onclick="window.location.href='<?php echo SITE_URL?>project/create'" target="_blank" class="banner-getstarted btn btn-default get-started">
                POST YOUR PROJECT
            </a>
        </div>
    </div>
    <div class="col-md-12 linkedin-section">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center">
            <script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
             <script type="IN/CompanyInsider" data-id="2966858"></script>
        </div>
    </div>
    <div class="col-md-12 member-wrapper media-wrapper">
        <!--<div class="w-heading text-center md-heading">
            <h5>Media</h5>
        </div>-->
        <div class="container memberData mdData">
            <div class="media-section">
                <div class="col-md-3 col-xs-12 col-sm-4 text-center">
                    <a href="http://www.bizjournals.com/pacific/print-edition/2015/03/06/how-i-connect-business-owners-and-executives-with.html" target="_blank">
                        <img src="<?php echo(SITE_URL)?>yogesh_new1/images/pacific.png">
                    </a>
                </div>
                <div class="col-md-3 col-xs-12 col-sm-4 text-center">
                    <a href="http://www.forbes.com/sites/fotschcase/2016/03/01/sales-flat-try-the-hawaiian-approach/#3ca7ebb3662a" target="_blank">
                        <img src="<?php echo(SITE_URL)?>yogesh_new1/images/forbes-logo.png">
                    </a>
                </div>
                <div class="col-md-3 col-xs-12 col-sm-4 text-center">
                    <a href="http://www.inc.com/laura-garnett/10-innovative-gift-ideas-to-help-someone-s-performance-at-work.html" target="_blank">
                        <img src="<?php echo(SITE_URL)?>yogesh_new1/images/inc-logo.png">
                    </a>
                </div>
                <div class="col-md-3 col-xs-12 col-sm-4 text-center">
                    <a href="http://fi.co/posts/16151" target="_blank">
                        <img src="<?php echo(SITE_URL)?>yogesh_new1/images/FI-logo.png">
                    </a>
                </div>
            </div>
        </div>
    </div>

<!--    <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
    <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
    <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>-->
    
    <script type="text/javascript">
    // parallax
   // if($(window).width() >991){
     //   $('#banner-section').parallax();
    //}
    </script>