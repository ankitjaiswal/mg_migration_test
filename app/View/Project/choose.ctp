<style type="text/css">

input[type="checkbox"] { display:none; }

input[type="checkbox"] + label {
    color:#333333;
    font-family:Arial, sans-serif;
    font-size:14px;
}

input[type="checkbox"] + label span {
    display:inline-block;
    width:25px; height:25px;
    margin:-1px 4px 0 0;
    vertical-align:middle;
    background:url(../../img/media/check.png) left top no-repeat;
    cursor:pointer;
}
input[type="checkbox"]:checked + label span {
    background:url(../../img/media/check.png) -26px top no-repeat;
}
</style>

<link rel="stylesheet" href="<?php echo(SITE_URL)?>js/chosen/chosen.css">
  <link rel="stylesheet" href="<?php echo(SITE_URL)?>css/project.css">
    
  <div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth" style="margin-top:60px; margin-bottom:30px;">
    	<div style="display: block; height: 60px; margin-bottom: 20px; border-bottom: 1px solid rgb(188, 188, 188);">
			<div style="float: left;">
				<h1 style="font-size: 20px; border: 0px;">Post a project</h1>
				<sub class="MGSub">Free & confidential</sub> 
			</div>
			<div style="float: right;">
				<table class="step3">
				    <tr>
				        <td><span>1</span></td>
				        <td>Create</td>
				        <td><span>2</span></td>
				        <td>Preview</td>
				        <td><span style="background-color: #F13031;">3</span></td>
				        <td style="color: #F13031;">Post</td>
				    </tr>
				</table>
			</div>
			<br/>	
		</div>
    
     <?php if(isset($data) && $data != ''){?>
      <div id="" style="background:url('<?php echo(SITE_URL)?>img/media/home-banner4A.jpg') 0 0 no-repeat;border:0px;">
     <?php }else{?>
      <div id="" style="background:url('<?php echo(SITE_URL)?>img/media/home-banner4A.jpg') 0 0 no-repeat;border:0px; min-height: 500px;">
     <?php }?>
        <form id="Appyform" enctype="multipart/form-data" method="post" action="<?php echo SITE_URL ?>project/choose/<?php echo($projectId);?>" accept-charset="utf-8">
        	<?php echo($this->Form->hidden('Project.id',array('value'=>$projectId)));?>
            <div id="spotlight1" class="pagewidth1">
                <div id="find-mentor1">
                <h1>Consult leading business experts.</h1>
                <div id="text-box1">
                    <div id="enter-keywords">
                    	<div id="enter-text1">
	                        <select id="Multi-select" data-placeholder="Enter key areas of expertise" multiple class="chosen-select" style="width:664px; min-height: 300px;" tabindex="18">
					            <option value=""></option>
								<?php foreach ($keywords as $key) {
										
										if(isset($keyword[$key['Topic']['id']])){;?>
											<option value="<?php echo($key['Topic']['autocomplete_text'])?>" selected="selected"><?php echo($key['Topic']['autocomplete_text'])?></option>
										<?php } else {?>
											<option value="<?php echo($key['Topic']['autocomplete_text'])?>"><?php echo($key['Topic']['autocomplete_text'])?></option>
								<?php }}?>
					          </select>
				          </div>
				          
                    </div>
                    <?php echo($this->Form->hidden('Project.select',array('value'=>'', 'id'=>'hiddenSelect')));?>
                    <div id="search">
                        <input class="searchSubmit" type="submit" value="Search" id="apply"/>
                    </div>       
                </div>
                </div>  
            </div>
        </form>
        	<div style="margin-top: 10px; width: 70%; float: left">
	        	<span style="font-weight: bold;"> Shortlist, up to 5 relevant consultants, to post your project requirements to...</span>
	        </div>
	        <?php if(isset($data) && $data != ''){?>
	        <div style="height: 40px; margin: 10px 0px; font-weight: bold;">
		        <p>
					<span style="float: right;">
						Showing <?php echo(count($data));?> consultants
					</span>
				</p>
	      	</div>
	      	<?php }?>
        </div>
        <?php if(isset($data) && $data != ''){?>
		
      <div id="mentors-content">
<?php
	$pageLimit = 5;
	$chosenCount = 0;
	$data_flag = false;
	if(!empty($data)){
		$data_flag = true;
?>
		
<form id="Appyform" enctype="multipart/form-data" method="post" action="<?php echo SITE_URL ?>project/preview/<?php echo($projectId);?>" accept-charset="utf-8">
<?php echo($this->Form->hidden('Project.id',array('value'=>$projectId)));?>
	<div style="width: 100%; height: 60px;">
      	<div id="Apply" style="float: right;">
			<input id="applyNext1" class="mgButton" type="submit" value="Next" disabled="disabled">
		</div>
	</div>
	<table id="results" style="width:100% !important;">
	<?php	
	$key = 0;
	foreach($data as $value){

			if($key%$pageLimit == 0){
				$num = floor($key/$pageLimit);
			}
			?>
			<tr class="set_<?php echo($num); ?>">
				<td>
				 	<div class="box1" id="box_<?php echo($value['User']['id'])?>"style="padding: 10px 0px 10px 10px; margin-bottom: 20px; border-bottom: 0px solid #c7c7c7; border-top: 0px solid #c7c7c7;" >
					  <div class="content-left-img" style="width: 180px;"> 
					  <?php 
					  	$urlk1 = ($value['User']['url_key']);
						$displink1=SITE_URL.strtolower($urlk1); ?>
					 <?php 
					 if(!empty($value['UserImage'])){
						if(file_exists(WWW_ROOT . 'img' . '/' .MENTORS_IMAGE_PATH.'/'.$value['User']['id'].'/'.$value['UserImage'][0]['image_name'])){ 
							echo($this->Html->link($this->Html->image(MENTORS_IMAGE_PATH.'/'.$value['User']['id'].'/'.$value['UserImage'][0]['image_name'], array('class'=>'desaturate','alt'=>$value['UserReference']['first_name']." ".$value['UserReference']['last_name'],'style'=>'width:150px; height:150px;')),$displink1,array('escape'=>false,'target'=>'_blank', 'style'=>'border: 0px;')));                       					
						}else{				
							echo($this->Html->link($this->Html->image('media/profile.png',array('alt'=>$value['UserReference']['first_name']." ".$value['UserReference']['last_name'],'style'=>'width:150px; height:150px;')),$displink1,array('escape'=>false,'target'=>'_blank', 'style'=>'border: 0px;')));  
						}
						
					}else{
						echo($this->Html->link($this->Html->image('media/profile.png',array('alt'=>$value['UserReference']['first_name']." ".$value['UserReference']['last_name'],'style'=>'width:150px; height:150px;')),$displink,array('escape'=>false, 'style'=>'border: 0px;','target'=>'_blank')));  

					}
					?>
						<div class="apply-button-search" style="margin-top: 5px; float: left;font-weight:normal;">
							<input id="applyView" style="width: 150px;" style="font-weight:normal;" class="mgButton" type="button"  value="View Profile" onclick="window.open('<?php echo SITE_URL.strtolower($value['User']['url_key']); ?>')">
						</div>
					</div>
                     <?php
						    $urlk = ($value['User']['url_key']);
							$displink=SITE_URL.strtolower($urlk);
				          ?>
					  <div class="content-right-text" style="width: 80%;">
						<h1><span><?php echo($this->Html->link(ucwords($value['UserReference']['first_name']).' '.$value['UserReference']['last_name'],$displink,array('escape'=>false,'target'=>'_blank'))); ?></span> - <?php echo($value['UserReference']['City']['city_name'].', '.$value['UserReference']['City']['state']);?>
							<span style="float: right;">
								<input type="checkbox" id="check_<?php echo($value['User']['id'])?>" name="catCheck[]" class="group1" value="<?php echo($value['User']['id']);?>" onclick="changeBackground(<?php echo($value['User']['id']);?>);"/>
						        <label for="check_<?php echo($value['User']['id'])?>"><span></span></label>
							</span>
						</h1>
						<div class="text-content-box">
						  <p> 
						  <?php
							$strdata=strlen ($value['UserReference']['background_summary']);
							if($strdata < 700){
							echo $this->Text->truncate( $value['UserReference']['background_summary'], 400, array('ending' => '...', 'exact' => false));
							}else{
								echo $this->Text->truncate( $value['UserReference']['background_summary'], 400, array('ending' => '...', 'exact' => false));
							}
						  ?>&nbsp;&nbsp;
						  
						 
				          <?php echo($this->Html->link('read more',$displink,array('escape'=>false,'target'=>'_blank'))); ?>

						  </p>
							
						</div>
						<div class="expertise-header">
						  <h1>Areas of expertise</h1>
						  <p><?php echo($value['UserReference']['area_of_expertise']);?></p>
						</div>
						
					  </div>
					</div>		
				</td>
			</tr>			
		<?php
		$key++;
	}?>
	</table>
	<div id="Apply" style="float: right; margin-bottom:30px; margin-top:45px;" role="Apply">
		<input id="applyNext2" class="mgButton" type="submit" value="Next" disabled="disabled">
	</div>
</form>
    <div style="width:81%; float:right; padding-bottom:30px; text-align:center">
	<?php
		/*if($num >0){
			$style ="background: none repeat scroll 0 0 #F32C33; color: #FFFFFF; cursor: pointer; font-weight: bold; height: 14px; text-align: center; margin:auto; padding: 10px; width:50%;";
			if($this->Session->read('Auth.User.id') ==''){
			?>	
				<div id="openRegisterNav" class="more-button-search">
		        <input type="button" value="MORE MEMBERS"></div>
			<?php
			}else{
			?>
				<div id="pageNavPosition" class="more-button-search">
		        <input type="button" value="MORE MEMBERS"></div>
			<?php
			}
		}*/
	?>
	</div>
	<?php

}else{?>
	 <div class="box1">
	 <div class="content-right-text">
		<div class="expertise-header">
		<p><span style="text-decoration:none">No Records Found. Please try browsing or change location.</span></p>
		</div>
	 
	 </div>
	 </div>
	<?php 
} ?>
<?php 
$chose = 5 - $chosenCount;
echo($this->Form->hidden('numInvite',array('value'=>($chose),'id'=>'numInvite'))); ?>
<?php
if($data_flag){?>	
	<?php // echo($this->Form->hidden('num',array('value'=>$num,'id'=>'num'))); ?>
	<?php // echo($this->Form->hidden('countShow',array('id'=>'countShow'))); ?>
	<?php
}else{?>
	<?php // echo($this->Form->hidden('num',array('value'=>'NOT','id'=>'num'))); ?>
	<?php
}?>
<script type="text/javascript">
	function apply(url,mentor_id,ClickType){
		var role_id = "<?php echo $this->Session->read('Auth.User.role_id'); ?>";
		var email_send = "<?php echo $this->Session->read('Auth.User.email_send'); ?>";
		var is_approved = "<?php echo $this->Session->read('Auth.User.is_approved'); ?>";
		if(role_id==''){
			//applyloginpopup(mentor_id);
			applyregisterpopup();
			
		}else if(role_id==2){
			alert("Sorry, mentors are currently not allowed to apply for mentorship from other mentors.");
		}else{
		   if(is_approved=='0')
			{
				location.href = SITE_URL+"users/logout";
			}
			else
			{
				if(ClickType == "request"){
					requestMentorship(mentor_id);
				}
				else{
					location.href = url+mentor_id;
				}
			}
		}
	
	}
	// more mentor show functionality
	//history.navigationMode = 'compatible';
	jQuery(document).ready(function(){

		var numInvite = parseInt(jQuery("#numInvite").val(),10);
		jQuery("#noOfInvite").html(numInvite);
		
		/*var num = jQuery("#num").val();	
		
		if(num !='NOT'){			
			jQuery(".set_0").show();
			jQuery("#countShow").val(0);	
		}
		var old_countShow = readCookie('cookie_countShow')
		if (old_countShow) {
			for(showCnt=0;showCnt<old_countShow;showCnt++){
				jQuery("#countShow").val(showCnt);
				paging_hack();		
			}
			
		}	
		eraseCookie('cookie_countShow');*/
	});

	/* jQuery("#openRegisterNav").click(function(){
		//register_popup();
		var num = parseInt(jQuery("#num").val(),10);	
		var countShow = parseInt(jQuery("#countShow").val(),10) + 1;
		//createCookie('cookie_countShow',countShow,0);
		
		if(num > countShow){			
			jQuery(".set_"+countShow).show();
			jQuery("#countShow").val(countShow);	
		}else if(num == countShow){
			jQuery(".set_"+countShow).show();
			jQuery("#countShow").val(countShow);
			jQuery("#pageNavPosition").hide();	
			jQuery("#openRegisterNav").hide();
		}
	});
	
	
	jQuery("#pageNavPosition").click(function(){
		var num = parseInt(jQuery("#num").val(),10);	
		var countShow = parseInt(jQuery("#countShow").val(),10) + 1;
		//createCookie('cookie_countShow',countShow,0);
		
		if(num > countShow){			
			jQuery(".set_"+countShow).show();
			jQuery("#countShow").val(countShow);	
		}else if(num == countShow){
			jQuery(".set_"+countShow).show();
			jQuery("#countShow").val(countShow);
			jQuery("#pageNavPosition").hide();	
			jQuery("#openRegisterNav").hide();
		}
	});*/

	/*jQuery("#applyNext").click(function(){

		var numInvite = parseInt(jQuery("#numInvite").val(),10);

		if(numInvite == 5) {

			alert("Choose atleast one consultants");
			return false;
		}
	});*/
	
	
	function changeBackground(u_id) {

		var numInvite = parseInt(jQuery("#numInvite").val(),10);
		if(document.getElementById('box_'+u_id).style.backgroundColor){
			document.getElementById('box_'+u_id).style.backgroundColor="";
			document.getElementById('box_'+u_id).style.borderWidth="0";
			
			numInvite = numInvite + 1;

			if(numInvite >= 5){
				document.getElementById('applyNext1').disabled = true;
				document.getElementById('applyNext2').disabled = true;
			}
		}
		else {
			if(numInvite - 1 < 0) {
				alert("You can send invitations to a maximum of 5 consultants for a project. \n\nHowever, you can make your project visible to other consultants, and they will contact you if interested.");
				jQuery("#check_"+u_id).prop('checked', false).uniform(); 
				return false;
			}
			else {
				numInvite = numInvite - 1;
				document.getElementById('box_'+u_id).style.backgroundColor="#f2f1f1";
				document.getElementById('box_'+u_id).style.borderWidth="1px 0 1px 0";
				document.getElementById('applyNext1').disabled = false;
				document.getElementById('applyNext2').disabled = false;
			}

		}

		jQuery("#noOfInvite").html(numInvite);
		jQuery("#numInvite").val(numInvite);
	}
	
</script>
      
      </div>
      <?php }?>
    </div>
  </div> 
<?php 
if(empty($data)){?>
 <script type="text/javascript">
	jQuery(window).bind('load resize',function(){
		if(jQuery("body").height() < jQuery(window).height()){
			jQuery("#footer-wrapper").css({'position':'fixed'})
		}
		
	});
 </script>
 <?php
}?>

<script type="text/javascript">
 	var URL = '<?php echo(SITE_URL); ?>fronts/search';

 	jQuery(document).ready(function(){

 		var username = jQuery('#username');

 		username.autocomplete({
	 		minLength    : 1,
	 		source        : URL
 		});

 	});
 
  </script>
  
  <script src="<?php echo(SITE_URL);?>js/chosen/chosen.jquery.js" type="text/javascript"></script>
  <script src="<?php echo(SITE_URL);?>js/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">

     jQuery('#Multi-select').chosen();

     jQuery('#apply').click(function(){
     var myValues = jQuery('#Multi-select').chosen().val();

     jQuery('#hiddenSelect').val(myValues);

     jQuery('#Appyform').submit();
     
 	});

  </script>
  
  
  <style>
/** //file:app/webroot/css/autocomplete.css **/
/** search container **/
.users{
	width:640px;
	margin:0 auto;
	max-height:180px;
	overflow:auto;
}
#content p{
	width:300px;
	margin:0 auto;
	text-align:center;
	max-height:180px;
	overflow:auto;
}
#username{
	border:1px solid silver;
	clear:none;
	width:400px;
	max-height:180px;
	overflow:auto;
}
.input{
	clear:none;
	float:left;
}
.submit{
	clear:none;
	float:right;
	padding:20px 0;
}
/* Styling the markup generated by the autocomplete jQuery UI widget */

ul.ui-autocomplete{
	width:200px;
	background-color:white;
	border:1px solid gray;
	margin-left:-3px !important;
	margin-right:1px !important;
	margin-top:-4px;
	font-family:Helvetica, Arial,sans-serif;max-height:180px;
	overflow:hidden;
	max-height:160px;
}
ul.ui-autocomplete li{
	list-style:none;
	border-top:1px solid white;
	border-left:1px solid white;
	margin:0;
	max-height:180px;
	overflow:auto;
}
ul.ui-autocomplete li:first-child{
	border-top:none;
}
ul.ui-autocomplete li:last-child{
	border-bottom:none;
}
ul.ui-autocomplete li a{
	border:none !important;
	text-decoration:none !important;
	padding:2px;
	display:block;
	color:black;
}
ul.ui-autocomplete li img{
	margin-right:4px;
}
ul.ui-autocomplete li span{
}
#ui-active-menuitem{
	background-color:#efefef;
	cursor:pointer;
}
</style>
  
  
<script type="text/javascript" src="http://j.maxmind.com/app/geoip.js"></script>
<script type="text/javascript">
	var conntry_code 		= geoip_country_code();
	var geoip_city 	 		= geoip_city();	
	var geoip_region_name 	= geoip_region_name();	
	jQuery(document).ready(function(){
		jQuery.ajax({
			url:SITE_URL+'fronts/getStateCode',
			type:'post',
			dataType:'json',
			data:'state_name='+geoip_region_name,
			success:function(res){
				if(res.value !=''){
					state_name = res.value;	
					if(conntry_code =='US'){
						jQuery(".newyork").val(geoip_city+', '+state_name);
					}else{
						jQuery(".newyork").val('International');
					}					
				}else{
					if(conntry_code =='US'){
						jQuery(".newyork").val(geoip_city+', '+geoip_region_name);
					}else{
						jQuery(".newyork").val('International');
					}
				}
			}
		});
		
	});
</script>
  