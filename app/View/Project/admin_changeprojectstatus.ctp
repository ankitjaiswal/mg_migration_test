		<div class="adminrightinner">
                        <form id="Projectform" enctype="multipart/form-data" method="post" action="<?php echo SITE_URL ?>project/admin_changeprojectstatus/<?php echo($this->data['Project']['id']);?>" accept-charset="utf-8">
			   <?php echo($this->Form->hidden('Project.id'));?>
			<div class="tablewapper2 AdminForm">	
				<table border="0" class="Admin2Table" width="100%">		
					<tr>

              
                                              <td>Project Current Status</td> 
						<td>



<?php  

    if($this->data['Project']['status'] == 0) {
    echo("Project created by Client");
   }else if($this->data['Project']['status'] == 1){
    echo("Project posted by Client");
   }else if($this->data['Project']['status'] == 2){
    echo("Client Partner call scheduled");
   }else if($this->data['Project']['status'] == 3){
    echo("Project submitted for Client approval");
   }else if($this->data['Project']['status'] == 4){
    echo("Project approved by Client");
   }else if($this->data['Project']['status'] == 5){
    echo("$500 Payment Done");
   }else if($this->data['Project']['status'] == 6){
    echo("Proposal submitted");
   }else if($this->data['Project']['status'] == 7){
    echo("Proposal approved by Client");
   }else if($this->data['Project']['status'] == 8){
    echo("Project started");
   }else{
   echo("Project completed");
   }
 ?>



                                             </td>		
					</tr>
					<tr>
						<td valign="middle" class="Padleft26">Change Project Status</td>
						
                                                <td>


								<select class="form-control active" name="data[Project][status]" id="project_start_date" value="<?php echo($this->data['Project']['status']);?>">
									
									<?php if($this->data['Project']['status'] =='0'){?>
									<option disabled selected>0. Project created by Client</option>
									<?php }else{?>
									<option >0. Project created by Client</option>
									<?php }?>
									<?php if($this->data['Project']['status'] =='1'){?>
									<option disabled selected>1. Project posted by Client</option>
									<?php }else{?>
									<option >1. Project posted by Client</option>
									<?php }?>
									<?php if($this->data['Project']['status'] =='2'){?>
									<option disabled selected>2. Client Partner call scheduled</option>
									<?php }else{?>
									<option >2. Client Partner call scheduled</option>
									<?php }?>
									<?php if($this->data['Project']['status'] =='3'){?>
									<option disabled selected>3.Project submitted for Client approval</option>
									<?php }else{?>
									<option >3. Project submitted for Client approval</option>
									<?php }?>
									<?php if($this->data['Project']['status'] =='4'){?>
									<option disabled selected>4. Project approved by Client</option>
									<?php }else{?>
									<option >4. Project approved by Client</option>
									<?php }?>
									<?php if($this->data['Project']['status'] =='5'){?>
									<option disabled selected>5.$500 Deposit Done</option>
									<?php }else{?>
									<option >5. $500 Payment Done</option>
									<?php }?>
									<?php if($this->data['Project']['status'] =='6'){?>
									<option disabled selected>6. Proposal submitted</option>
									<?php }else{?>
									<option >6. Proposal submitted</option>
									<?php }?>
									<?php if($this->data['Project']['status'] =='7'){?>
									<option disabled selected>7. Proposal approved by Client</option>
									<?php }else{?>
									<option >7. Proposal approved by Client</option>
									<?php }?>
									<?php if($this->data['Project']['status'] =='8'){?>
									<option disabled selected>8. Project started</option>
									<?php }else{?>
									<option >8. Project started</option>
									<?php }?>
									<?php if($this->data['Project']['status'] =='9'){?>
									<option disabled selected>9. Project completed</option>
									<?php }else{?>
									<option >9. Project completed</option>
									<?php }?>

								</select>

                                                   </td>
                                                 
					</tr>

                                         


				</table>
			</div>
			<div class="buttonwapper">
				<div><input type="submit" value="Submit" class="submit_button" /></div>
				<div class="cancel_button"><?php echo $this->Html->link("Cancel", "/admin/project/review/", array("title"=>"", "escape"=>false)); ?></div>
			</div>
			</form>	
		</div>