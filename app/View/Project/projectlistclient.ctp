<div class="container-fluid projectCreated" id="apply-section">
	<div class="container create-section pad0">
		<div class="col-md-12">
			<div class="p-members-table">
				<h5>My Projects</h5>
				<table class="table table-striped smartresponsive-table">
					<thead>
						<tr>
							<th>Created by</th>
							<th>Date</th>
							<th>Project title</th>
                                                        <th>Project Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php if(!empty($data) && count($data)>0)
						{
						$i=1;
						foreach($data as $project){
						
							$for_creator = ClassRegistry::init('User');
						$creator = $for_creator->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));
						if($creator['User']['role_id'] == 2)
						$displink1=SITE_URL.strtolower($creator['User']['url_key']);
						else
						$displink1=SITE_URL."clients/my_account/".strtolower($creator['User']['url_key']);
						?>
						<tr>
							<td data-th="Created by"><?php  echo($this->Html->link(ucfirst($creator['UserReference']['first_name'])." ".ucfirst($creator['UserReference']['last_name']),$displink1,array('escape'=>false,'target'=>'_blank')));?></td>
							<td data-th="Date"><?php echo date("m-d-Y", strtotime($project['Project']['created']));?></td>
							<td data-th="Project title"><?php echo $project['Project']['title']; ?></td>

                   <?php if($project['Project']['status'] == 1) {   ?> 
                           <td data-th="Action">Project saved as draft</td>
                           <td data-th="Action"><?php echo($this->Html->link('Complete now',array('controller'=>'project','action'=>'create/'.$project['Project']['id']),array('class'=>'delete','title'=>'Complete project'))); ?></td>
                    <?php } 
                         else if($project['Project']['status'] == 2) {?>
                           <td data-th="Action">Project created. To be posted</td>
                           <td data-th="Action"><?php echo($this->Html->link('Post now',array('controller'=>'project','action'=>'view/'.$project['Project']['id']),array('class'=>'delete','title'=>'Edit and post project')));?></td>
                    <?php } else if($project['Project']['status'] == 3) { ?>
                           <td data-th="Action">Project posted. Pending approval</td>
                           <td data-th="Action"><?php echo($this->Html->link('View',array('controller'=>'project','action'=>'index/'.$project['Project']['project_url']),array('class'=>'delete','title'=>'Edit and post project'))); ?></td>
                    <?php } else if($project['Project']['status'] == 5) { ?>
                           <td data-th="Action">Project not approved. Please edit and repost</td>
                           <td data-th="Action"><?php echo($this->Html->link('Edit and repost',array('controller'=>'project','action'=>'view/'.$project['Project']['id']),array('class'=>'delete','title'=>'Edit and post project'))); ?></td>
                    <?php } else {?>
                    	   <td data-th="Action">Project approved</td>
                    	   <td data-th="Action"><?php echo($this->Html->link('View',array('controller'=>'project','action'=>'index/'.$project['Project']['project_url']),array('class'=>'delete','title'=>'View project'))); ?></td>
                    <?php }?>

						</tr>
						<?php  $i++;  }?>
						
						
						<?php
						} else{?>
						<tr style="text-align: center;">
							<td colspan="7">No record found</td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
<!-- <script src="<?php echo(SITE_URL)?>yogesh_new1/js/editor.js"></script> -->


<!-- Include yogesh_new1 JS code -->
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/script12.js"></script>