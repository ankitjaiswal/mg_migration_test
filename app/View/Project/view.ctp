<?php
if (time() - $_SESSION['myapp']['user_start'] < 700) { 
$visitoremail = $this->Session->read('visitoremail');
}?>
<div class="container-fluid" id="apply-section">
	<div class="container create-section pad0">
		<?php if($this->data['Project']['status'] == 3 || $this->data['Project']['status'] == 4 || $this->data['Project']['status'] == 5 || $this->data['Project']['status'] == 6 || $this->data['Project']['status'] == 7 || $this->data['Project']['status'] == 8) {?>
		<div class="progresbar" style="margin: 0 0 30px;">
			<div class="col-sm-8 pad40">
				<ul>
					<li>
						<span>1</span>
						<div class="title">
							<p>Project created</p>
						</div>
					</li>
					<li>
						<span>2</span>
						<div class="title">
							<p>Project approved</p>
						</div>
					</li>
					<li>
						<span>3</span>
						<div class="title">
							<p>Proposal accepted</p>
						</div>
					</li>
					<li>
						<span>4</span>
						<div class="title">
							<p>Project started</p>
						</div>
					</li>
					<li>
						<span>5</span>
						<div class="title">
							<p>Project completed</p>
						</div>
					</li>
				</ul>
			</div>			
		</div>
		<?php }?>
		<div class="col-md-8 pad40" id="projectnew-view">
			<form id="Projectform" enctype="multipart/form-data" method="post" action="<?php echo SITE_URL ?>project/finalise" accept-charset="utf-8">
				<div class="apply-heading">
					<h5>Post a project</h5>
					<label class="free-c"> (Free & confidential)</label>
				</div>
				<?php echo($this->Form->hidden('Project.id'));?>
				<div class="project-overview">
					<div class="overview-desc">
						<h5>Project title</h5>
						<p><?php echo($this->data['Project']['title']);?></p>
					</div>
					<div class="overview-desc">
						<h5>Describe your need</h5>
						<p>    <?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['details']);
							echo $this->General->make_links(nl2br($details));?>
						</p>
					</div>
					<?php if(isset($this->data['Project']['start_date']) && $this->data['Project']['start_date'] != '' ) {?>
					<div class="overview-desc">
						<h5>Start</h5>
						<p><?php echo($this->data['Project']['start_date']);?></p>
					</div>
					<?php }?>
					<?php if(isset($this->data['Project']['budget']) && $this->data['Project']['budget'] != '' ) {?>
					<div class="overview-desc">
						<h5>Budget</h5>
						<p><?php echo($this->data['Project']['budget']);?></p>
					</div>
					<?php }?>
					<?php if(isset($this->data['Project']['success']) && $this->data['Project']['success'] != '' ) {?>
					<div class="overview-desc">
						<h5>Describe the ideal professional for your project</h5>
						<p><?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['success']);
						echo $this->General->make_links(nl2br($details));?></p>
					</div>
					<?php }?>
					<?php if(isset($this->data['Project']['industry']) && $this->data['Project']['industry'] != '' ) {?>
					<div class="overview-desc">
						<h5>Industry</h5>
						<p><?php echo($this->data['Project']['industry']);?> &#8212; <?php echo($this->data['Project']['category']);?></p>
					</div>
					<?php }?>
					<?php if(isset($this->data['Project']['business_need']) && $this->data['Project']['business_need'] != '' ) {?>
					<div class="overview-desc">
						<h5>Business need</h5>
						<p><?php echo($this->data['Project']['business_need']);?></p>
					</div>
					<?php }?>
					<?php if(isset($this->data['Project']['solution_type']) && $this->data['Project']['solution_type'] != '' ) {?>
					<div class="overview-desc">
						<h5>Solution type</h5>
						<p><?php echo($this->data['Project']['solution_type']);?></p>
					</div>
					<?php }?>
					<?php if(isset($this->data['Project']['duration']) && $this->data['Project']['duration'] != '' ) {?>
					<div class="overview-desc">
						<h5>Estimated duration</h5>
						<p><?php echo($this->data['Project']['duration']);?></p>
					</div>
					<?php }?>
					<?php if(isset($this->data['Project']['location']) && $this->data['Project']['location'] != '' ) {?>
					<div class="overview-desc">
						<h5>Location preference</h5>
						<p><?php echo($this->data['Project']['location']);?></p>
					</div>
					<?php }?>
					<?php if(isset($this->data['Project']['filepath']) && $this->data['Project']['filepath'] != '' ) {?>
					<div class="overview-desc">
						
						<?php $img = SITE_URL."img/".MENTEES_PROJECT_PATH.DS.$this->data['Project']['id'].DS.$this->data['Project']['filepath']; ?>
						<h5>Attachment</h5>
						<p class="attach-overview">
							<?php echo($this->Html->link($this->data['Project']['filepath'],$img,array('escape'=>true , 'target'=>'_blank')));?>
						</p>
					</div>
					<?php }?>
					<?php if($this->Session->read('Auth.User.id') == '') {?>
					<div class="e-button pull-right" id="visitorbutton">
						<label>
							<a href="<?php echo(SITE_URL);?>project/create/<?php echo($this->data['Project']['id']);?>" class="hide-fields">
								Edit
							</a>
						</label>
						<button class="banner-getstarted btn btn-default get-started" id="visitorformopen">
						Post your requirement
						</button>
					</div>
					<?php } else{?>
					<div class="e-button pull-right">
						<label>
							<a href="<?php echo(SITE_URL);?>project/create/<?php echo($this->data['Project']['id']);?>" class="hide-fields">
								Edit
							</a>
						</label>
						<button class="banner-getstarted btn btn-default get-started" id="loggedinsubmit">
						Post your requirement
						</button>
					</div>
					<?php } ?>
				</form>
			</div>
		</div>
		<div class="col-md-4 clearboth create-right">
			<div class="right-apply">
				<div class="apply-heading">
					<h5>How does it work?</h5>
				</div>
				<div class="cr-desc">
					<h5>A. Specify business Need</h5>
					<p>Post your high-level requirement using our 1-page form.</p>
				</div>
				<div class="cr-desc">
					<h5>B. Review proposals</h5>
					<p>A dedicated Client Partner will help you finalize your project needs. Then, in 2-3 days we will get back to you with proposals for getting the job done.</p>
				</div>
				<div class="cr-desc">
					<h5>C. Get work done</h5>
					<p>Once you approve, we will get to work. The Client Partner will ensure all deliverables are met on a timely basis.</p>
				</div>
				<div class="apply-heading">
					<h5>Not ready to post yet?</h5>
				</div>
				<div id="visitorform_here"></div>
				<div class="cr-desc mt0">
					<p>Call us to discuss your business needs.</p>
					<p>Call <a href="tel:1-866-511-1898">1-866-511-1898</a>.</p>
				</div>
			</div>
		</div>
		<div class="col-md-8 guest-signup" id="visitorform" style="display:none;">
			<div class="g-heading text-center">
				<!--<img src="<?php echo(SITE_URL)?>yogesh_new1/images/arrow.png">-->
				<label id="almost-done">Almost done!  Leave your name and email so a Client Partner can get in touch.</label></br>
				<label>Already have a GUILD account? <a href="javascript:void(0);" data-toggle="modal" data-target="#signin-Modal" title="Login" style="font-weight:bold;">Please login here</a></label>
			</div>
			<div class="col-md-12" style="height: 190px;">
			<div class="col-md-7 pad0 text-center" id="guest-container" style="display: inline-block;margin: 0 auto;position: absolute;left: 0;right: 0;">
				<div class="col-md-12 text-center">
					
					<div class="apply-form"> 
						<form method="post" action="<?php echo (SITE_URL); ?>fronts/new_project_request" id="newmentorshipForm">
							<div class="form-group width49 " style="width:50% ;float:left;padding-right:5px">
								<input type="text" name="data[Mentorship][firstname]" id="userfname" class="form-control" placeholder="First Name">
							</div>
							<div class="form-group width49 "  style="width:50%;float:left;padding-left:5px">
								<input type="text" name="data[Mentorship][lastname]" id="userlname"  class="form-control"  placeholder="Last Name">
							</div>
							<div class="form-group width49">
								<input type="email" name="data[Mentorship][email]" id="useremail" class="form-control " value="<?php echo($visitoremail);?>" placeholder="email@yourcompany.com">
								<span id="Emailalready" class="errormsg" style="display:inline; float:left; display:none;"><?php echo $error; ?></span>
							</div>

							<input name="data[Project][phone_number]" type="hidden" value="" id="phonenumber">
							<?php echo($this->Form->hidden('Project.id'));?>
						</form>
					</div>
				</div>
				</div>
				<div style="clear:both"></div>
				<!--<div class="col-md-6 mpad0">
				<div class="g-linkedin">
						<a href="#" id="linkedinbuttonclick">
							<img src="<?php echo(SITE_URL)?>yogesh_new1/images/linkedin.png">
						</a>
						<p>GUILD will never post to your LinkedIn account without your permission.</p>
					</div>
				</div>-->
			</div>
			<div style="clear:both"></div>
			<div class="e-button text-center" style="margin-top:0;margin-left:18%">
				<label>
					<a href="<?php echo(SITE_URL);?>project/create/<?php echo($this->data['Project']['id']);?>" class="hide-fields">
						Edit
					</a>
				</label>
				<button style="float:none" class="banner-getstarted btn btn-default get-started" id="visitorsubmit">
				Post your requirement
				</button>
			</div>
		</div>
	</div>
</div>
<style>
.g-heading label {
    font-size: 16px;
    //font-family: ubuntum;
    color: #000;
}

.g-linkedin {
    padding: 100px 0px;
    border-left: 1px solid #ddd !important;
	border-right:0 !important;
}
.g-linkedin::before {

    content: 'OR';
    position: absolute;
    left: 4px;
    top: 45%;
	background: #fff;
	font-weight:bold;
}
.g-linkedin a
{
	margin-left:13% !important;
}
#guest-container .apply-form
{
	margin-top:13px !important;
}
@media only screen and (max-width: 600px) {
	
.g-linkedin a
{
	margin-left:0% !important;
}
.g-linkedin
{
	border-left:0 !important;
	border-bottom:0 !important;
	border-top:1px solid #ddd !important;
}
}
label#almost-done:before {
    content: 'te';
    width: 100px;
    height: 100px;
    background-image: url(https://test.guild.im/yogesh_new1/images/arrow.png);
    color: transparent;
    background-size: contain;
    background-repeat: no-repeat;
	background-position: center;
}
</style>
<!-- js link -->

<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?php echo(SITE_URL)?>sanchit/js/projectviewscript1.js"></script>
