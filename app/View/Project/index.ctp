                           <?php foreach ($mentorsArray as $value) {
                                   if($this->Session->read('Auth.User.id') == $value['User']['id']){
                                    $consultantisthere = "yes";
                                    }

                                        if($value['User']['id'] == $this->Session->read('Auth.User.id')){         
					                    $consultant = ClassRegistry::init('Project_member_confirm');
					                    $show_interest = $consultant->find('first', array('conditions' => array('AND' =>array('Project_member_confirm.project_id ' => $this->data['Project']['id'] ,'Project_member_confirm.member_id' => $value['User']['id']))));
                                        if($show_interest['Project_member_confirm']['status'] == '1'){
                                        $consultantselected = "yes";
                                        }
                                        }

                                 }?>

<div class="row">
<div class="row">
<div class="container">
   <!--<div class="col-md-12">-->
      <div class="col-md-8 pull-left pad40" id="projectnew">
         <div class="container-fluid main-container">

       <?php if($this->data['Project']['status'] == 3 || $this->data['Project']['status'] == 4 || $this->data['Project']['status'] == 5 || $this->data['Project']['status'] == 6 || $this->data['Project']['status'] == 7 || $this->data['Project']['status'] == 8 || $this->data['Project']['status'] == 9) {?>
            <div class="col-md-12 progresbar" style="padding: 0;">
               <ul style="-webkit-justify-content: space-between;display: flex;-moz-justify-content: space-between;">
                  <li class="active">
                     <span>1</span>
                     <div class="title">
                        <p>Project created</p>
                     </div>
                  </li>

        <?php if($this->data['Project']['status'] == 4 || $this->data['Project']['status'] == 5 || $this->data['Project']['status'] == 6 || $this->data['Project']['status'] == 7 || $this->data['Project']['status'] == 8 || $this->data['Project']['status'] == 9) {?>
                  <li class="active">
                     <span>2</span>
                     <div class="title">
                        <p>Project approved</p>
                     </div>
                  </li>
         <?php }else{?>
                  <li>
                     <span>2</span>
                     <div class="title">
                        <p>Project approved</p>
                     </div>
                  </li>
           <?php }?>

          <?php if($this->data['Project']['status'] == 7 || $this->data['Project']['status'] == 8 || $this->data['Project']['status'] == 9) {?>
                  <li class="active">
                     <span>3</span>
                     <div class="title">
                        <p>Proposal accepted</p>
                     </div>
                  </li>
           <?php }else{?>
                  <li>
                     <span>3</span>
                     <div class="title">
                        <p>Proposal accepted</p>
                     </div>
                  </li>
            <?php }?>

          <?php if($this->data['Project']['status'] == 8 || $this->data['Project']['status'] == 9) {?>
                  <li class="active">
                     <span>4</span>
                     <div class="title">
                        <p>Project started</p>
                     </div>
                  </li>
           <?php }else{?>
                  <li>
                     <span>4</span>
                     <div class="title">
                        <p>Project started</p>
                     </div>
                  </li>
            <?php }?>
            <?php if($this->data['Project']['status'] == 9) {?>
                  <li class="active">
                     <span>5</span>
                     <div class="title">
                        <p>Project completed</p>
                     </div>
                  </li>
             <?php }else{?>
                  <li>
                     <span>5</span>
                     <div class="title">
                        <p>Project completed</p>
                     </div>
                  </li>
              <?php }?>
               </ul>
            </div>
          <?php }?>

         </div>

         <div class="project-overview" style="margin-bottom: 30px;">

                              <div class="overview-desc">
                              <h5>Project title</h5>
                              <p><?php echo($this->data['Project']['title']);?></p>
                              </div>
                              <div class="overview-desc">
                              <h5>Project details</h5>
                              <p> <?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['details']);
		                  echo $this->General->make_links(nl2br($details));?></p>
                               </div>

				<?php if(isset($this->data['Project']['start_date']) && $this->data['Project']['start_date'] != '' ) {?>
				<div class="overview-desc">
					<h5>Start</h5>
					<p><?php echo($this->data['Project']['start_date']);?></p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['budget']) && $this->data['Project']['budget'] != '' ) {?>
				<div class="overview-desc">
					<h5>Budget</h5>
					<p><?php echo($this->data['Project']['budget']);?></p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['success']) && $this->data['Project']['success'] != '' ) {?>
				<div class="overview-desc">
					<h5>Desired qualification</h5>
					<p> <?php $details = str_replace('  ', '&nbsp;&nbsp;', $this->data['Project']['success']);
						echo $this->General->make_links(nl2br($details));?>
					</p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['industry']) && $this->data['Project']['industry'] != '' ) {?>
				<div class="overview-desc">
					<h5>Industry</h5>
					<p><?php echo($this->data['Project']['industry']);?> &#8212; <?php echo($this->data['Project']['category']);?></p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['business_need']) && $this->data['Project']['business_need'] != '' ) {?>
				<div class="overview-desc">
					<h5>Business need</h5>
					<p><?php echo($this->data['Project']['business_need']);?></p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['solution_type']) && $this->data['Project']['solution_type'] != '' ) {?>
				<div class="overview-desc">
					<h5>Solution type</h5>
					<p><?php echo($this->data['Project']['solution_type']);?></p>
				</div>
				<?php }?>
				
				<?php if(isset($this->data['Project']['location']) && $this->data['Project']['location'] != '' ) {?>
				<div class="overview-desc">
					<h5>Location preference</h5>
					<p><?php echo($this->data['Project']['location']);?></p>
				</div>
				<?php }?>
				<?php if(isset($this->data['Project']['duration']) && $this->data['Project']['duration'] != '' ) {?>
				<div class="overview-desc">
					<h5>Estimated duration</h5>
					<p><?php echo($this->data['Project']['duration']);?></p>
				</div>
				<?php }?>
				<?php  if($this->Session->read('Auth.User.role_id')== 3 || $this->Session->read('Auth.User.role_id')== '1' || ($this->Session->read('Auth.User.role_id')==2 && $this->Session->read('Auth.User.id') == $this->data['Project']['user_id'])) {
				if(isset($this->data['Project']['phone_number']) && $this->data['Project']['phone_number'] != '' ) {?>
				<div class="overview-desc">
					<h5>Phone number</h5>
					<p><?php echo($this->data['Project']['phone_number']);?></p>
				</div>
				<?php }}?>
				<?php if(isset($this->data['Project']['filepath']) && $this->data['Project']['filepath'] != '' ) {?>
				<div class="overview-desc">
					<h5>Attachment</h5>
					
					<?php $img = SITE_URL."img/".MENTEES_PROJECT_PATH.DS.$this->data['Project']['id'].DS.$this->data['Project']['filepath']; ?>
					<p class="attach-overview">
						<?php echo($this->Html->link($this->data['Project']['filepath'],$img,array('escape'=>true , 'target'=>'_blank'))); ?>
					</p>
				</div>
				<?php }?>


         </div>




                        <?php if(($this->Session->read('Auth.User.id') == $this->data['Project']['user_id']) && $invoicedata !='') {?>
			<div class="project-overview project_invoice_main_div">
				<div class="overview-desc account-info p-members-table ">
					<h3>Project invoices</h3>
					<table class="table table-striped smartresponsive-table">
						<thead>
							<tr>
								<th>Invoice no</th>
								<th>Date</th>
								<th>Amount</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
                                                 
						<tbody>
                                                      <?php foreach ($invoicedata  as $value) {?>
							<tr>
                                                               
								<td data-th="Invoice No"><?php echo($value['ProjectInvoice']['invoiceno']);?></td>
								<td data-th="Date"><?php echo date("m-d-Y", strtotime($value['ProjectInvoice']['created']));?></td>
								<td data-th="Price">$<?php echo($value['ProjectInvoice']['total']);?></td>
								<td data-th="Status"><?php echo($value['ProjectInvoice']['invoicestatus']);?></td>
                                                                 <?php if($value['ProjectInvoice']['invoicestatus'] =="OPEN"){?>
								<td data-th="Print"><a href="<?php echo SITE_URL."project/invoice_detail/".$value['ProjectInvoice']['invoiceno']; ?>" class="delete" title="Pay Now">Pay Now</a></td>
                                                                <?php }else{?>
								<td data-th="Print"><a href="<?php echo SITE_URL."project/invoice_detail/".$value['ProjectInvoice']['invoiceno']; ?>" class="delete" title="View invoice">View</a></td>
                                                               <?php }?>
							</tr>
                                                        <?php }?> 
						</tbody>
					</table>
				</div>
			</div>
                        <?php }?>




                        <?php if(($this->Session->read('Auth.User.role_id')==2 && $this->Session->read('Auth.User.id') != $this->data['Project']['user_id']) && ($consultantisthere == "yes" && ($this->data['Project']['status'] == '8' ||  $this->data['Project']['status'] == '9' ) && $consultantselected == "yes") || ($this->data['Project']['status'] == '8' && $this->Session->read('Auth.User.role_id')==1)) {?>
			<div class="project-overview">
				<div class="overview-desc account-info p-members-table ">
					<h3>Project invoices</h3>
					<table class="table table-striped smartresponsive-table">
						<thead>
							<tr>
								<th>Invoice no</th>
								<th>Date</th>
								<th>Amount</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
                                                      <?php foreach ($invoicedata  as $value) {?>
							<tr>
								<td data-th="Invoice No"><?php echo($value['ProjectInvoice']['invoiceno']);?></td>
								<td data-th="Date"><?php echo date("m-d-Y", strtotime($value['ProjectInvoice']['created']));?></td>
								<td data-th="Price">$<?php echo($value['ProjectInvoice']['total']);?></td>
								<td data-th="Status"><?php echo($value['ProjectInvoice']['invoicestatus']);?></td>
								<td data-th="Print"><a href="<?php echo SITE_URL."project/invoice_detail/".$value['ProjectInvoice']['invoiceno']; ?>" class="delete" title="View invoice">View</a></td>
							</tr>
                                                        <?php }?> 
						</tbody>
					</table>
                                        <?php if($this->data['Project']['status'] == '8'){?>
					<div class="button-set">
						<input class="btn btn-primary" value="Create Invoice" type="submit" onclick="window.location.href='<?php echo SITE_URL."project/invoice_create/".$this->data['Project']['id']; ?>';"/>
					</div>
                                        <?php }?>
				</div>
			</div>
                        <?php }?>



         <!--<div class="project-overview" style="margin-bottom: 10px;">
            <div class="overview-desc">
               <h5>Consultants</h5>
               <p><span style="float:left;"><img style="width:70px !important" src="<?php echo(SITE_URL)?>sanchit/images/2019-06-23.png"><span style="color:#CF3135">Robert Davis</span></span><span><span style="float:right"><button type="button" class="btn btn-danger" style="margin-top:30px;margin-left:20px">Lorem Ipsum</button></span></p>
            </div> 
         </div>-->

		 <!--Show interest btn proposal for consultant-->
		<?php if($this->Session->read('Auth.User.role_id')==2 && $this->Session->read('Auth.User.id') != $this->data['Project']['user_id']) {?>
		<?php if (empty($mentorsArray)){?>

		 <div class="show_interest"  >
		 <a class="modal-link show_interest_ignore"  style="" data-toggle="modal" data-target="#ignore-modal">Ignore</a>
                     <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#interest-modal">Show interest in this project</button>
		 </div>

	        <?php }else if($count == 0){?>
		 <div  class="show_interest">
		 <a class="modal-link show_interest_ignore" data-toggle="modal" data-target="#ignore-modal">Ignore</a>
                     <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#interest-modal">Show interest in this project</button>
		 </div>
                <?php }else {?>
				
		 <div  class="show_interest">
		 </div>
                <?php }?>

	      <?php } else if($this->data['Project']['status'] == '1' && $this->Session->read('Auth.User.id') == $this->data['Project']['user_id']) {?>
                <div  class="show_interest">
                 <button type="button" class="btn btn-danger" onclick="window.location.href='<?php echo SITE_URL."project/schedule_call/".$this->data['Project']['id']; ?>';">Schedule Client Partner Call</button>
                </div>
              <?php } else if($this->data['Project']['status'] == '3' && $this->Session->read('Auth.User.id') == $this->data['Project']['user_id']) {?>
		 <div  class="show_interest">
		<?php echo($this->Html->link('Edit',SITE_URL."/project/projectedit/".$this->data['Project']['id'],array('escape'=>false, 'style'=>'margin-right:20px;margin-top:10px;line-height:48px;font-size:16px'))); ?>
                     <button type="button" class="btn btn-danger" onclick="window.location.href='<?php echo SITE_URL."project/client_approveproject/".$this->data['Project']['id']; ?>';">Approve</button>
		 </div>      
               <?php } else if($this->data['Project']['status'] == '4' && $this->Session->read('Auth.User.id') == $this->data['Project']['user_id']) {?>
                <div  class="show_interest">
                 <button type="button" class="btn btn-danger" onclick="window.location.href='<?php echo SITE_URL."project/stripe_payment/".$this->data['Project']['id']; ?>';">Pay project deposit</button>
                </div>
               <?php } else if($this->data['Project']['status'] == '7' && $this->Session->read('Auth.User.id') == $this->data['Project']['user_id']) {?>
                <div  class="show_interest" id="proposal<?php echo($this->data['Project']['id']); ?>">
                 <button type="button" class="btn btn-danger btnxyz" data-toggle="modal" data-target="#project-start-modal" id="accept<?php echo($this->data['Project']['id']); ?>">Start project</button>
                 <input type="hidden"  class="txtabc" value="<?php echo($this->data['Project']['id']);?>">
                </div>
               <?php } else if($this->data['Project']['status'] == '8' && $this->Session->read('Auth.User.id') == $this->data['Project']['user_id']) {?>
                <div  class="show_interest" id="proposal<?php echo($this->data['Project']['id']); ?>">
                 <button type="button" class="btn btn-danger btnxyz" data-toggle="modal" data-target="#project-close-modal" id="accept<?php echo($this->data['Project']['id']); ?>">Close project</button>
                 <input type="hidden"  class="txtabc" value="<?php echo($this->data['Project']['id']); ?>">
                </div>
               <?php }?>  



</div>
   <!--</div>-->
   <div class="col-md-4 col-sm-12 col-xs-12 pull-right clearboth create-right">
      <div class="right-apply">

         <div class="project-overview">
            <div class="overview-desc created-by  part-2">
               <h5>Client Partner</h5>
               <div class="created-person-img">
                  <img src="<?php echo(SITE_URL)?>press_material/Iqbal-Ashraf-GUILD-Client-Partner.jpg">
               </div>
               <br>
               <h5 class="client_details_name" style="border:none;">Iqbal Ashraf</h5>
               <h5 class="client_details_phone"><i class="fa fa-phone" aria-hidden="true"></i>
                  <a href="tel:1-808-729-5850">1-808-729-5850</a>
               </h5>
               <h5 class="client_details_mail"><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:iqbal@guild.im">iqbal@guild.im</a></h5>
            </div>
         </div>


<!-- Client -->
	<?php if(($this->Session->read('Auth.User.role_id')==3 || ($this->Session->read('Auth.User.role_id')==2 && $this->Session->read('Auth.User.id') == $this->data['Project']['user_id'])) && ($this->data['Project']['status'] == '6' || $this->data['Project']['status'] == '7' || $this->data['Project']['status'] == '8' || $this->data['Project']['status'] == '9')) {?>
          <?php if(!empty($mentorsArray)){?>
         <div class="project-overview" style="margin-top:0">
            <div class="overview-desc created-by  part-3">
               <h5 >Consultant</h5>
               <ul>
			   <!--Consultant who has submitted the proposal-->

					<?php foreach ($mentorsArray as $value) {
					$consultant = ClassRegistry::init('Project_member_confirm');
					$show_interest = $consultant->find('first', array('conditions' => array('AND' =>array('Project_member_confirm.project_id ' => $this->data['Project']['id'] ,'Project_member_confirm.member_id' => $value['User']['id']))));
					$proposal = ClassRegistry::init('Proposal');
					$proposal_create = $proposal->find('first', array('conditions' => array('AND' =>array('Proposal.projectid ' => $this->data['Project']['id'] ,'Proposal.user_id' => $value['User']['id']))));
					$proposallink = SITE_URL."project/proposal/".$proposal_create['Proposal']['proposal_url'];
					if(($show_interest['Project_member_confirm']['status'] == '0' || $show_interest['Project_member_confirm']['status'] == '1') && $proposal_create['Proposal']['haspublished']=='Y'){
						if(isset($value['UserImage'][0]['image_name']) && $value['UserImage'][0]['image_name'] !=''){
							$image_path = MENTORS_IMAGE_PATH.DS.$value['User']['id'].DS.$value['UserImage'][0]['image_name'];
						} else {
											$image_path = 'media/profile.png';
					}?>
                  <li id="proposalid<?php echo($show_interest['Project_member_confirm']['id']); ?>">
                     <div class="created-person-img last1" style="width: 70px !important;">
                        <img src="<?php echo(SITE_URL.'img/'.$image_path)?>" alt=""  style="width:70px;height:70px;"/>
                     </div>
                     <h5 style="color:#d03135;border:none;padding-bottom: 5px;" id="proposal<?php echo($show_interest['Project_member_confirm']['id']); ?>">
                        <a href="<?php echo (SITE_URL);?><?php echo($value['User']['url_key']);?>" class="reset" target="_blank"><?php echo (ucfirst($value['UserReference']['first_name']).' '.ucfirst($value['UserReference']['last_name'])); ?></a>
                        <a href="<?php echo ($proposallink);?>" class="reset proposal_icon" target="_blank"><img src="<?php echo(SITE_URL)?>sanchit/images/View Proposal icon.png" class="proposal_icon"></a>
                     </h5>
                      <div class="button-set" id="proposal<?php echo($show_interest['Project_member_confirm']['id']); ?>">
					 <!--Ignore accept btn-->
                     <?php if($show_interest['Project_member_confirm']['status'] != '1'){?>
                     <a  style="" data-toggle="modal" data-target="#proposal-ignore-modal" class="btnxyz ignore_btn" id="accept<?php echo($show_interest['Project_member_confirm']['id']); ?>">Ignore</a>
                     <input data-toggle="modal" data-target="#accept-modal" id="accept<?php echo($show_interest['Project_member_confirm']['id']); ?>" class="btn btn-primary btnxyz" value="Accept" type="submit"/>
                     <input type="hidden"  class="txtabc" value="<?php echo($show_interest['Project_member_confirm']['id']); ?>">   
                  <?php }?>
                     </div>
                  </li>


					<?php echo($this->Form->hidden('Project.User.'.$value['User']['id'],array('value'=>$value['User']['id'])));
                                         }else if(($show_interest['Project_member_confirm']['status'] == '0' || $show_interest['Project_member_confirm']['status'] == '1')  && $proposal_create['Proposal']['proposal_url'] !=''){


						if(isset($value['UserImage'][0]['image_name']) && $value['UserImage'][0]['image_name'] !=''){
							$image_path = MENTORS_IMAGE_PATH.DS.$value['User']['id'].DS.$value['UserImage'][0]['image_name'];
						} else {
											$image_path = 'media/profile.png';
                                                }
                                               ?>

                  <li id="proposalid<?php echo($show_interest['Project_member_confirm']['id']); ?>">
                     <div class="created-person-img last1" style="width: 70px !important;">
                        <a href="<?php echo (SITE_URL);?><?php echo($value['User']['url_key']);?>" class="reset" target="_blank"><img src="<?php echo(SITE_URL.'img/'.$image_path)?>" alt=""  style="width:70px;height:70px;"/></a>
                     </div>
                     <h5 style="color:#d03135;border:none;padding-bottom: 5px;" id="proposal<?php echo($show_interest['Project_member_confirm']['id']); ?>">
                        <a href="<?php echo (SITE_URL);?><?php echo($value['User']['url_key']);?>" class="reset" target="_blank"><?php echo (ucfirst($value['UserReference']['first_name']).' '.ucfirst($value['UserReference']['last_name'])); ?></a>
                     </h5>
		     <div class="button-set" id="proposal<?php echo($show_interest['Project_member_confirm']['id']); ?>">			 
                     <!--Ignore accept btn-->
                     <?php if($show_interest['Project_member_confirm']['status'] != '1'){?>
                     <a   data-toggle="modal" data-target="#proposal-ignore-modal" class="btnxyz ignore_btn" id="accept<?php echo($show_interest['Project_member_confirm']['id']); ?>">Ignore</a>
                     <input data-toggle="modal" data-target="#accept-modal" id="accept<?php echo($show_interest['Project_member_confirm']['id']); ?>" class="btn btn-primary btnxyz" value="Accept" type="submit"/>
                     <input type="hidden"  class="txtabc" value="<?php echo($show_interest['Project_member_confirm']['id']); ?>">   
                     </div>
                  <?php }?>
                  </li>
                    

					<?php echo($this->Form->hidden('Project.User.'.$value['User']['id'],array('value'=>$value['User']['id'])));
                                         }else if($show_interest['Project_member_confirm']['status'] == '0' || $show_interest['Project_member_confirm']['status'] == '1'){


						if(isset($value['UserImage'][0]['image_name']) && $value['UserImage'][0]['image_name'] !=''){
							$image_path = MENTORS_IMAGE_PATH.DS.$value['User']['id'].DS.$value['UserImage'][0]['image_name'];
						} else {
											$image_path = 'media/profile.png';
                                                }
                                                if($value['User']['access_specifier']=="publish")
                                                 $profilekey = SITE_URL.$value['User']['url_key'];
                                                else
                                                 $profilekey = $value['UserReference']['client_linkedinurl'];
                                               ?>

                  <li id="proposalid<?php echo($show_interest['Project_member_confirm']['id']); ?>">
                     <div class="created-person-img last1" style="width: 70px !important;">
                        <a href="<?php echo ($profilekey);?>" class="reset" target="_blank"><img src="<?php echo(SITE_URL.'img/'.$image_path)?>" alt=""  style="width:70px;height:70px;"/></a>
                     </div>
                     <h5 style="color:#d03135;border:none;padding-bottom: 5px;" id="proposal<?php echo($show_interest['Project_member_confirm']['id']); ?>">
                        <a href="<?php echo ($profilekey);?>" class="reset" target="_blank"><?php echo (ucfirst($value['UserReference']['first_name']).' '.ucfirst($value['UserReference']['last_name'])); ?></a>
                     </h5>
					 <!--Ignore accept btn-->
                     <div class="button-set" id="proposal<?php echo($show_interest['Project_member_confirm']['id']); ?>">
                     <?php if($show_interest['Project_member_confirm']['status'] != '1'){?>
                     <a   data-toggle="modal" data-target="#proposal-ignore-modal" class="btnxyz ignore_btn" id="accept<?php echo($show_interest['Project_member_confirm']['id']); ?>">Ignore</a>
                     <input data-toggle="modal" data-target="#accept-modal" id="accept<?php echo($show_interest['Project_member_confirm']['id']); ?>" class="btn btn-primary btnxyz" value="Accept" type="submit"/>
                     <input type="hidden"  class="txtabc" value="<?php echo($show_interest['Project_member_confirm']['id']); ?>">   
                  <?php }?>
                     </div>
                  </li>
                    <?php echo($this->Form->hidden('Project.User.'.$value['User']['id'],array('value'=>$value['User']['id'])));}}?>

               </ul>
            </div>
         </div>
        <?php }}?>





<!-- client end-->


			<!-- Consultant -->

                           
                                    

				<?php if(($consultantisthere == "yes") && ($this->Session->read('Auth.User.role_id')==2 && $this->Session->read('Auth.User.id') != $this->data['Project']['user_id'])) {?>
                                
				<?php if(!empty($mentorsArray)){?>
         <div class="project-overview" style="margin-top:0">
            <div class="overview-desc created-by  part-3">
					<h5>Consultant</h5>
					<ul class="proposal-list">
					<?php foreach ($mentorsArray as $value) {
                                        if($value['User']['id'] == $this->Session->read('Auth.User.id')){         
					$consultant = ClassRegistry::init('Project_member_confirm');
					$show_interest = $consultant->find('first', array('conditions' => array('AND' =>array('Project_member_confirm.project_id ' => $this->data['Project']['id'] ,'Project_member_confirm.member_id' => $value['User']['id']))));
					$proposal = ClassRegistry::init('Proposal');
					$proposal_create = $proposal->find('first', array('conditions' => array('AND' =>array('Proposal.projectid ' => $this->data['Project']['id'] ,'Proposal.user_id' => $value['User']['id']))));
					$proposallink = SITE_URL."project/proposal/".$proposal_create['Proposal']['proposal_url'];
					if(($show_interest['Project_member_confirm']['status'] == '0' || $show_interest['Project_member_confirm']['status'] == '1' || $show_interest['Project_member_confirm']['status'] == '-1' )){
						if(isset($value['UserImage'][0]['image_name']) && $value['UserImage'][0]['image_name'] !=''){
							$image_path = MENTORS_IMAGE_PATH.DS.$value['User']['id'].DS.$value['UserImage'][0]['image_name'];
						} else {
											$image_path = 'media/profile.png';
					}?>

                                                 
                  <li>
                     <div class="created-person-img last1" style="width: 70px !important;">
                        <img src="<?php echo(SITE_URL.'img/'.$image_path)?>" alt=""  style="width:70px;height:70px;"/>
                     </div>
                     <h5 style="color:#d03135;border:none;" id="proposal<?php echo($show_interest['Project_member_confirm']['id']); ?>">
                        <a href="<?php echo (SITE_URL);?><?php echo($value['User']['url_key']);?>" class="reset" target="_blank"><?php echo (ucfirst($value['UserReference']['first_name']).' '.ucfirst($value['UserReference']['last_name'])); ?></a>

                       <?php if($proposal_create['Proposal']['proposal_url'] !=''){?>
                        <a href="<?php echo ($proposallink);?>" class="reset" target="_blank"><img src="<?php echo(SITE_URL)?>sanchit/images/View Proposal icon.png" class="proposal_icon"></a>
                        <?php }?>
                        </h5>

                                                                <?php if($proposal_create['Proposal']['proposal_url'] =='' && $show_interest['Project_member_confirm']['status'] == '0'){?>
                                                                  <button type="button" class="btn btn-danger" onclick="window.open('<?php echo SITE_URL."project/projectproposal/".$this->data['Project']['id']; ?>');">Create a proposal</button>  

                                                                <?php }?>
                  </li>
                                                   
					<?php echo($this->Form->hidden('Project.User.'.$value['User']['id'],array('value'=>$value['User']['id'])));}}}?>
					

					</ul>
				</div>
			</div>
                       <?php }}?>

                  <!-- Consultant end -->


      </div>
   </div>


</div>

<div id="accept-modal" class="modal fade member-edit-popup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h3 class="modal-title">Confirm interest</h3>
			</div>
			<form id="frmedit-profile-m" class="bv-form">
				<div class="modal-body edit-profile-m">
					<div class="aoe-box">
						<div class="col-md-12">
							<p class="text-left">Do you want to accept this proposal?</p>
						</div>
					</div>
					<div class="button-set">
						<a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
						<a href="javascript:void(0);" class="btn btn-primary continue" data-dismiss="modal" onclick="javascript:proposal_accept(this.id); ">Accept</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="proposal-ignore-modal" class="modal fade member-edit-popup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h3 class="modal-title">Ignore consultant</h3>
			</div>
			<form id="frmedit-profile-m" class="bv-form">
				<div class="modal-body edit-profile-m">
					<div class="aoe-box">
						<div class="col-md-12">
							<p class="text-left">Do you want to ignore this proposal?</p>
						</div>
					</div>
					<div class="button-set">
						<a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
						<a href="javascript:void(0);" class="btn btn-primary continue" data-dismiss="modal" onclick="javascript:proposal_ignore(this.id); ">Ignore</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="interest-modal" class="modal fade member-edit-popup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h3 class="modal-title">Confirm interest</h3>
			</div>
			<div class="modal-body" style="padding-left: 20px;">
				<p>Are you interested in this project?</p>
				<div class="button-set">
					<a href="javascript:;" class="reset" data-dismiss="modal" aria-label="Close">No</a>
					<a href="javascript:;" class="btn btn-primary continue" data-dismiss="modal" onclick="javascript:show_interest(<?php echo($this->data['Project']['id']); ?>);">Yes</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="ignore-modal" class="modal fade member-edit-popup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h3 class="modal-title">Ignore this project</h3>
			</div>
			<div class="modal-body" style="padding-left: 20px;">
				<p>Are you not interested in this project?</p>
				<div class="button-set">
					<a href="javascript:;" class="reset" data-dismiss="modal" aria-label="Close">No</a>
					<a href="javascript:;" class="btn btn-primary continue" data-dismiss="modal" onclick="window.open('<?php echo (SITE_URL); ?>');">Yes</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="project-start-modal" class="modal fade member-edit-popup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h3 class="modal-title">Start project</h3>
			</div>
			<form id="frmedit-profile-m" class="bv-form">
				<div class="modal-body edit-profile-m">
					<div class="aoe-box">
						<div class="col-md-12">
							<p class="text-left">Do you want to start this project?</p>
						</div>
					</div>
					<div class="button-set">
						<a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
						<a href="javascript:void(0);" class="btn btn-primary continue" data-dismiss="modal" onclick="javascript:project_start(this.id);">Confirm</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="project-close-modal" class="modal fade member-edit-popup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h3 class="modal-title">Close project</h3>
			</div>
			<form id="frmedit-profile-m" class="bv-form">
				<div class="modal-body edit-profile-m">
					<div class="aoe-box">
						<div class="col-md-12">
							<p class="text-left">Do you want to close this project?</p>
						</div>
					</div>
					<div class="button-set">
						<a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
						<a href="javascript:void(0);" class="btn btn-primary continue" data-dismiss="modal" onclick="javascript:project_close(this.id); ">Confirm</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>


	<!-- signup modal -->
	<div class="modal loginModal fade" id="projectsignup-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title Register-title" id="myModalLabel">
						Register as a client
					</h4>
				</div>
				<div class="modal-body">
					<!-- client signup-form -->
					<div class="login-form signup-form">
						<form id="signupProceess" method="post" action="<?php echo SITE_URL;?>pricing/pricing_auth_popup">
			                      <?php if(isset($this->data['User']['id']) && isset($this->data['UserReference']['id']) && $id!=0){
				                    echo($this->Form->hidden('User.id',array('id'=>'popupId','div'=>false,'label'=>false)));
				                    echo($this->Form->hidden('UserReference.id',array('div'=>false,'label'=>false)));
			
			                      }?>
							<div class="login-with text-center">
								<a href="javascript:void(0); javascript:linkedin();">
									<img src="<?php echo(SITE_URL)?>yogesh_new1/images/linkedin.png">
								</a>
							</div>
							<label class="or">Or Register using email id</label>

							<div class="form-group">
								<input type="name" class="form-control" name="data[UserReference][first_name]" value="<?php echo($firstname);?>" id="fname" placeholder="First Name">
                                                                <span class="errormsg" id="firstNameErr"></span>
							</div>
							<div class="form-group">
								<input type="name" class="form-control" name="data[UserReference][last_name]" value="<?php echo($lastname);?>" id="lname" placeholder="Last Name">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="data[User][username]" value="<?php echo($useremail);?>" id="email" placeholder="Email">
                                                                <div id="UserEmailErr" class="errormsg"></div>
							</div>
							<div class="form-group">
								<input type="password" name="data[User][password2]" id="password" class="form-control password_icon" placeholder="Password">
                                                                <div id="PassLengtherr" class="errormsg"></div>
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="data[UserReference][zipcode]" id="zipcode" placeholder="Zip Code">
                                                                <div id="zipError" class="errormsg"></div>
							</div>
							<div class="text-center">
							<button style="margin-bottom:10px" class="banner-getstarted btn btn-default get-started" id="registerbutton">
								REGISTER
							</button>
							</div>
							<div class="need-accounts loginHere text-center">
								<label>
									Already have an account?
									<span class="mymodal" data-dismiss="modal" data-toggle="modal" data-target="#signin-Modal">Login here</span>
								</label>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- signup process -->



<!-- js link -->
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>

<script type="text/javascript">
	var username = "<?php echo $username; ?>";
        var prospect = "<?php echo $prospect; ?>";
       </script>

<script type="text/javascript">
jQuery(document).ready(function() {
if(username != '' && loggedUserId =='' && prospect =='register'){

jQuery('#projectsignup-modal').modal();

}else if(username != '' && loggedUserId =='' && prospect =='login'){

jQuery('#signin-Modal').modal();
}else{
				//jQuery('#signin-Modal').modal();
			}
});
</script>




<script type="text/javascript">
$(".btnxyz").click(function(){
	var txtabc = $(this).parent("div").find(".txtabc").val();
	console.log(txtabc);
        $(".continue").attr("id",txtabc);

});


function show_interest(confirm_id){

		var ignore_id = 'Ignore'+confirm_id
		jQuery(document).ready(function(){
				jQuery.ajax({
					url:SITE_URL+'project/show_interest',
					type:'post',
					dataType:'json',
					data:'confirm_id='+confirm_id,
					success:function(){
						
                                                location.reload();
					}
					});
});
	
}


function proposal_accept(confirm_id){

		jQuery(document).ready(function(){
				jQuery.ajax({
					url:SITE_URL+'project/proposal_accept_ajax',
					type:'post',
					dataType:'json',
					data:'confirm_id='+confirm_id,
					success:function(id){


						
                                                  location.reload();
}
});
});
}

function proposal_ignore(confirm_id){

		jQuery(document).ready(function(){
				jQuery.ajax({
					url:SITE_URL+'project/proposal_ignore_ajax',
					type:'post',
					dataType:'json',
					data:'confirm_id='+confirm_id,
					success:function(id){


						
                                                  location.reload();
}
});
});
}

function project_start(confirm_id){

		var ignore_id = 'Ignore'+confirm_id
		jQuery(document).ready(function(){
				jQuery.ajax({
					url:SITE_URL+'project/project_start',
					type:'post',
					dataType:'json',
					data:'confirm_id='+confirm_id,
					success:function(link){
                                                location.reload();
                                                
					}
					});
});
	
}


function project_close(confirm_id){

		var ignore_id = 'Ignore'+confirm_id
		jQuery(document).ready(function(){
				jQuery.ajax({
					url:SITE_URL+'project/project_close',
					type:'post',
					dataType:'json',
					data:'confirm_id='+confirm_id,
					success:function(link){
                                                location.reload();
                                                
					}
					});
});
	
}

$(document).ready(function(){
	$('.close').click(function(){
		var url      = window.location.href;
		var urlarray = url.split("/");
		if(urlarray.length == 8)
		{
			window.location.replace(window.location.origin);
		}
	});
});

</script>


<style>
.main-container
{
margin-top: 112px;
padding:0;
}
.project_invoice_main_div
{
margin-bottom:60px
}
.show_interest
{
margin-bottom: 10px;
text-align:right
}
.show_interest_ignore
{
margin-right:5px;
}
.client_details_phone
{
border:none;
padding-bottom:0;
}
.clients_details_mail
{
border:none;
padding-top:0;
}
.proposal_icon
{
width: 12%;
padding-left: 15px;
margin-top: -20px;
}
.ignore_btn
{
font-size: 16px;
color: #d03135;
cursor:pointer;
margin-right: 7%;
}
   body {
   font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
   font-size: 14px;
   line-height: 1.42857143;
   color: #333;
   background-color: #efefef;
   }
   .panel-title span {
   height: 40px;
   width: 40px;
   display: block;
   margin: 0 auto;
   border-radius: 50%;
   line-height: 40px;
   font-size: 14px;
   color: #000;
   text-align: center;
   z-index: 2;
   position: relative;
   background-color: #ddd;
   }
   .panel-title {
   width: 20%;
   float: left;
   }
   .panel-title p {
   text-align: center;
   margin-top: 10px;
   font-size: 13px;
   }
   .panel-title span::after {
   position: absolute;
   content: '';
   left: 40px;
   right: 30px;
   top: 20px;
   height: 2px;
   background: #DDDDDD;
   z-index: 0;
   width: 124px;
   }
   .panel-title-1 span::after {
   content: '';
   background: transparent!important;
   }
   .project span {
   background: #888;
   }
   .project1 span {
   background: #888;
   }
   .project span::after {
   background: #888;
   }
   /*.title h5 {
   border-bottom: 1px solid #ddd;
   padding-bottom: 10px;
   font-size: 16px;
   color: #000;
   }*/
   #projectnew .overview-desc, #projectnew-view .overview-desc {
   margin-bottom: 0;
   padding: 20px;
   padding-bottom: 0;
   }
   .overview-desc h5 {
   font-family: opensansregular,sans-serif;
   font-size: 16px;
   color: #000;
   border-bottom: 1px solid #ddd;
   padding-bottom: 10px;
   font-weight: bold;
   }
   .overview-desc p {
   font-family: opensansregular,sans-serif;
   font-size: 16px;
   color: #333;
   line-height: 24px;
   margin-bottom: 20px;
   }
   #projectnew .project-overview {
   margin-top: 30px;
   padding-bottom: 20px;
   background-color: #fff;
   width: 100%;
   display: inline-block;
   }
   @media only screen and (min-width: 601px) {
   .project-overview {
   margin-top: 217px;
   }
   }
   @media only screen and (max-width:600px)
   {
   .panel-title span::after {
   position: absolute;
   content: '';
   left: 40px;
   right: 30px;
   top: 20px;
   height: 2px;
   background: #DDDDDD;
   z-index: 0;
   width: 56px;
   }
   }
   .part-2 a {
   color: #d03135!important;
   }
   .part-2 h5 i {
   padding-right: 15px;
   }
   .davis {
   float: left;
   margin-bottom: 0;
   color: #d03135!important;
   }
   button.btn.btn-link:hover {
   text-decoration: none;
   border: 1px solid;
   border-radius: 4px;
   padding: 2px 10px;
   }
   .created-person-img {
   width: 120px !important;
   }
   .btn-danger {
   color: #fff;
   background-color: #d03135;
   border-color: #d03135;
   font-size: 18px;
   line-height:28px;
   }
   .btn-danger:hover {
   color: #fff;
   background-color: #000;
   border-color: #000;
   font-size: 18px;
   line-height:28px;
   }
   button.btn.btn-link {
   padding-left: 0
   }
   .last1
   {
   margin-top: 30px;
   }
   .davis1 {
   font-size: 16px;
   font-weight: bold;
   }
   /*.part-3 {
   padding-top: 35px;
   }*/
   .panel-title .stepcenter
   {
   margin:0 auto;
   }
   .panel-title .step
   {
   margin:0 !important;
   }
   .modal-link
   {
	   font-size: 16px;
	   color: #CF3135;
	   cursor:pointer;
   }
   .client_details_name
   { 
   }
   @media only screen and (max-width:600px)
   {
	   
#footer-section {
   background-color: #222222;
   padding: 40px 30px;
   clear: both;
}

.col-md-8.pull-left {
    width: 100%;
}
.col-md-8.pull-left .btn.btn-danger {
    float: left;
    margin-left: 20px;
}
.right-apply {
padding-left: 20px;
}
   } 
   #projectnew {

    margin-bottom: 42px;

}
   
</style>

