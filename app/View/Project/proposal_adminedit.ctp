	<div class="container-fluid" id="apply-section">
		<div class="container create-section pad0">
			<div class="col-md-8 pad40">
				<div class="apply-heading">
					<h5>Create a project proposal</h5>
					<label class="free-c"> (Pitch your solution)</label>
				</div>
				<div class="apply-form">
					<form id="Applyform" enctype="multipart/form-data" method="post" action="<?php echo SITE_URL ?>project/proposal_adminedit" accept-charset="utf-8">
                                            <?php echo($this->Form->hidden('Proposal.id'));?>
						<div class="form-group" data-placement="right" data-toggle="DescPopover1" data-container="body">
                                                   <?php if(isset($this->data['Proposal']['proposal_title']) && $this->data['Proposal']['proposal_title'] != ''){?>
							<label>Proposal title</label>
                                                   <?php }else{?>
                                                        <label>Title your proposal</label>
                                                   <?php }?>
							<span class="pull-right char-limit">(<span id="noOfCharTitle"><?php echo(128 - strlen($title))?></span> characters remaining)</span>
							<input type="text" name="data[Proposal][proposal_title]" class="form-control" placeholder="Implement Lean Six Sigma in your Service Business" id="proposal_title" value="<?php echo($title);?>">
							<div id="popover-content1" class="hide">
								<p>Be specific about ideal client and value proposition</p>
							</div>
						</div>
						<div class="form-group" data-placement="right" data-toggle="DescPopover2" data-container="body">
							<label>Client Impact</label>
							<span class="pull-right char-limit">(<span id="noOfCharDetails"><?php echo(1000 - strlen($details))?></span> characters remaining)</span>
                                                        <input type="hidden" name="data[Proposal][proposal_details]"  id="proposaldetailstext"  value="<?php echo(htmlspecialchars($details));?>"/>
							<div id="editor-container"></div>
							<div id="popover-content2" class="hide">
								<p>What specific benefits will the client see?</p>
							</div>
						</div>
						<div class="form-group" data-placement="right" data-toggle="DescPopover3" data-container="body">
							<label>Engagement Overview</label>
							<span class="pull-right char-limit">(<span id="noOfCharApproach"><?php echo(1000 - strlen($approach))?></span> characters remaining)</span>
                                                        <input type="hidden" name="data[Proposal][proposal_approach]"  id="proposalapproachtext"  value="<?php echo(htmlspecialchars($approach));?>"/>
							<div id="editor-container1"></div>
							<div id="popover-content3" class="hide">
								<p>1. What steps will you take to create that value?</p>
								<p>2. What is the estimated duration and scope?</p>
								<p>3. What will the cost be?</p>
							</div>
						</div>
						<div class="form-group" data-placement="right" data-toggle="DescPopover4" data-container="body">
							<label>Relevant Background</label>
							<span class="pull-right char-limit">(<span id="noOfCharSuccess"><?php echo(500 - strlen($success))?></span> characters remaining)</span>
                                                        <input type="hidden" name="data[Proposal][proposal_success]"  id="proposalsuccesstext"  value="<?php echo(htmlspecialchars($success));?>"/>
							<div id="editor-container2"></div>
							<div id="popover-content4" class="hide">
								<p>Mention directly relevant background/ proof points</p>
							</div>
						</div>
						<div class="e-button pull-right">
							<label>
								<?php  echo($this->Html->link("Cancel",SITE_URL.'project/admin_proposalreview'));?>

							</label>
							<button class="banner-getstarted btn btn-default get-started" id="submitProfile">
								Preview
							</button>
						</div>
					</form>
				</div>
			</div>
            <div class="col-md-4 clearboth create-right">
				<div class="right-apply">
					<div class="apply-heading">
						<h5>What can you do for an ideal client?</h5>
					</div>
					<div class="apply-desc">
						<div class="cr-desc">
							<h5>1. Create your proposal.</h5>
							<p>Create your project proposal using our simple, 1-page form.</p>
						</div>
						<div class="cr-desc">
							<h5>2. Promote your proposal.</h5>
							<p>Use social media to promote your proposal, and we will do the same.</p>
						</div>
						<div class="cr-desc">
							<h5>3. Respond to inquiries.</h5>
							<p>Be prompt in responding to prospect inquiries.</p>
						</div>
					</div>
					<div class="apply-heading">
						<h5>Not ready to post yet?</h5>
					</div>
					<div class="apply-desc">
						<div class="cr-desc mt10">
							<p>Call us to discuss your proposal goals. </br>Call <a href='tel:+18665111898'>1-866-511-1898</a>.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/script12.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/quill.min.js"></script>

	<script type="text/javascript">
		$(function() {
			$( ".data-picker" ).datepicker();
		});
		$(".hide-fields").on("click",function (e) {
			e.preventDefault();
			$(".optional-fields").addClass("hide");
			$(this).addClass("hide");
			$(".show-fields").removeClass("hide");
		});
		$(".show-fields").on("click",function (e) {
			e.preventDefault();
			$(".optional-fields").toggleClass("hide");
			$(this).addClass("hide");
			$(".hide-fields").removeClass("hide");
		});
		if($(window).width() > 991){
			$("[data-toggle=DescPopover1]").popover({
				html: true, 
				trigger : 'hover',
				content: function() {
					return $('#popover-content1').html();
				}
			});
			$("[data-toggle=DescPopover2]").popover({
				html: true, 
				trigger : 'hover',
				content: function() {
					return $('#popover-content2').html();
				}
			});
			$("[data-toggle=DescPopover3]").popover({
				html: true, 
				trigger : 'hover',
				content: function() {
					return $('#popover-content3').html();
				}
			});
			$("[data-toggle=DescPopover4]").popover({
				html: true, 
				trigger : 'hover',
				content: function() {
					return $('#popover-content4').html();
				}
			});
		}


        var limit = 1000;

        var limit1 = 500;

        var proposaldetailseditmode= jQuery("#proposaldetailstext").val();
        var proposalapproacheditmode= jQuery("#proposalapproachtext").val();
        var proposalsuccesseditmode= jQuery("#proposalsuccesstext").val();

    if(proposaldetailseditmode !=''){

    jQuery("#editor-container").html(proposaldetailseditmode);

     var quillproposaldetails = new Quill('#editor-container', {
    	modules: {
    	toolbar: [
      ['bold', 'italic', 'underline','link'], 
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      ['clean']
    	]
    	},
    	theme: 'snow' 
    });

     quillproposaldetails.on('text-change', function (delta, old, source) {

    if (quillproposaldetails.getLength() > limit) {
     quillproposaldetails.deleteText(limit, quillproposaldetails.getLength());
     }
    });

  }else{

      var quillproposaldetails= new Quill('#editor-container', {
      modules: {
    	toolbar: [
      ['bold', 'italic', 'underline','link'], 
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      ['clean']
      ]
      },
      theme: 'snow'
     });
     


     quillproposaldetails.on('text-change', function (delta, old, source) {

     if (quillproposaldetails.getLength() > limit) {
     quillproposaldetails.deleteText(limit, quillproposaldetails.getLength());
     }
    });


  }

   if(proposalapproacheditmode !=''){

     jQuery("#editor-container1").html(proposalapproacheditmode);

    var quillproposalapproach = new Quill('#editor-container1', {
    	modules: {
    	toolbar: [
      ['bold', 'italic', 'underline','link'], 
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      ['clean']
      ]
    	},
    	theme: 'snow' 
    });

     quillproposalapproach.on('text-change', function (delta, old, source) {

     if (quillproposalapproach.getLength() > limit) {
     quillproposalapproach.deleteText(limit, quillproposalapproach.getLength());
    }
    });

  }else{ 

      var quillproposalapproach = new Quill('#editor-container1', {
      modules: {
    	toolbar: [
      ['bold', 'italic', 'underline','link'], 
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      ['clean']
      ]
      },
      theme: 'snow'
     });
     


     quillproposalapproach.on('text-change', function (delta, old, source) {

     if (quillproposalapproach.getLength() > limit) {
     quillproposalapproach.deleteText(limit, quillproposalapproach.getLength());
     }
    });
 
 }

    if(proposalsuccesseditmode !=''){

     jQuery("#editor-container2").html(proposalsuccesseditmode);

    var quillproposalsuccess = new Quill('#editor-container2', {
    	modules: {
    	toolbar: [
      ['bold', 'italic', 'underline','link'], 
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      ['clean']
       ]
    	},
    	theme: 'snow' 
    });

     quillproposalsuccess.on('text-change', function (delta, old, source) {

     if (quillproposalsuccess.getLength() > limit1) {
     quillproposalsuccess.deleteText(limit1, quillproposalsuccess.getLength());
    }
    });

     }else{

      var quillproposalsuccess = new Quill('#editor-container2', {
      modules: {
    	toolbar: [
      ['bold', 'italic', 'underline','link'], 
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      ['clean']
       ]
      },
      theme: 'snow'
     });
     


     quillproposalsuccess.on('text-change', function (delta, old, source) {

     if (quillproposalsuccess.getLength() > limit1) {
     quillproposalsuccess.deleteText(limit1, quillproposalsuccess.getLength());
     }
    });

   }


</script>

<script type="text/javascript">
jQuery(document).ready(function(){
    window.onload=updateCountTitle();
});
	function updateCountTitle ()
    {
        var qText = jQuery("#proposal_title").val();

       if(qText.length < 128) {
           jQuery("#noOfCharTitle").html(128 - qText.length);
       } else {
           jQuery("#noOfCharTitle").html(0);
           jQuery("#proposal_title").val(qText.substring(0,128));
       }
    }

</script>
<script type="text/javascript">
jQuery(document).ready(function(){
    window.onload=updateCountDetails();
});
	function updateCountDetails ()
    {
        var qText = jQuery("#editor-container").text().length;

       if(qText< 1000) {
           jQuery("#noOfCharDetails").html(1000 - qText);
       } else {
           jQuery("#noOfCharDetails").html(0);

       }
    }


</script>
<script type="text/javascript">
jQuery(document).ready(function(){
    window.onload=updateCountApproach();
});
	function updateCountApproach ()
    {
        var qText = jQuery("#editor-container1").text().length;

       if(qText < 1000) {
           jQuery("#noOfCharApproach").html(1000 - qText);
       } else {
           jQuery("#noOfCharApproach").html(0);

       }
    }


</script>
<script type="text/javascript">
jQuery(document).ready(function(){
    window.onload=updateCountSuccess();
});
	function updateCountSuccess ()
    {  
        var qText = jQuery("#editor-container2").text().length;

       if(qText < 500) {
           jQuery("#noOfCharSuccess").html(500 - qText);
       } else {
           jQuery("#noOfCharSuccess").html(0);

       }
    }


</script>

<script type="text/javascript">
jQuery(document).ready(function(){


    jQuery("#proposal_title").keyup(function () {
    	updateCountTitle();
    });
    jQuery("#proposal_title").keypress(function () {
    	updateCountTitle();
    });
    jQuery("#proposal_title").keydown(function () {
    	updateCountTitle();
    });

	function updateCountDetails ()
    {
        var qText = jQuery("#editor-container").text().length;

       if(qText < 1000) {
           jQuery("#noOfCharDetails").html(1000 - qText);
       } else {
           jQuery("#noOfCharDetails").html(0);

       }
    }



    jQuery("#editor-container").keyup(function () {
    	updateCountDetails();
    });
    jQuery("#editor-container").keypress(function () {
    	updateCountDetails();
    });
    jQuery("#editor-container").keydown(function () {
    	updateCountDetails();
    });





    jQuery("#editor-container1").keyup(function () {
    	updateCountApproach();
    });
    jQuery("#editor-container1").keypress(function () {
    	updateCountApproach();
    });
    jQuery("#editor-container1").keydown(function () {
    	updateCountApproach();
    });

    



    jQuery("#editor-container2").keyup(function () {
    	updateCountSuccess();
    });
    jQuery("#editor-container2").keypress(function () {
    	updateCountSuccess();
    });
    jQuery("#editor-container2").keydown(function () {
    	updateCountSuccess();
    });
	
	jQuery("#submitProfile").click(function(){

	var proposaldetailsEntered = jQuery("#editor-container").html();

        var result1 = proposaldetailsEntered.split('<div class="ql-clipboard"');
        proposaldetailsEntered = result1[0];
	proposaldetailsEntered = proposaldetailsEntered.replace(/  /gi, '&nbsp;&nbsp;');
        
        jQuery("#proposaldetailstext").val(proposaldetailsEntered);

	var proposalapproachEntered = jQuery("#editor-container1").html();

        var result1 = proposalapproachEntered.split('<div class="ql-clipboard"');
        proposalapproachEntered = result1[0];
	proposalapproachEntered = proposalapproachEntered.replace(/  /gi, '&nbsp;&nbsp;');
        
        jQuery("#proposalapproachtext").val(proposalapproachEntered);

	var proposalsuccessEntered = jQuery("#editor-container2").html();

        var result1 = proposalsuccessEntered.split('<div class="ql-clipboard"');
        proposalsuccessEntered = result1[0];
	proposalsuccessEntered = proposalsuccessEntered.replace(/  /gi, '&nbsp;&nbsp;');
        
        jQuery("#proposalsuccesstext").val(proposalsuccessEntered);



	   var flag = 0;
   		if(jQuery.trim(title) == '') {
			jQuery('#proposal_title').css('border-color','#F00');
			jQuery('#proposal_title').focus();
			flag++;
		} else {
			jQuery('#proposal_title').css('border-color',''); 
		}

   		if(jQuery.trim(details) == '') {
			jQuery('#editor-container').css('border-color','#F00');
			jQuery('#editor-container').focus();
			flag++;
		} else {
			jQuery('#editor-container').css('border-color','');
		}

   		if(jQuery.trim(success) == '') {
			jQuery('#editor-container2').css('border-color','#F00');
			jQuery('#editor-container2').focus();
			flag++;
		} else {
			jQuery('#editor-container2').css('border-color','');
		}

		if(flag > 0) {
			return false;
		} else {

                       jQuery("#Applyform").submit();

		}
    });



	
});
</script>
