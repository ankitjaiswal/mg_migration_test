

<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div id="content-submit" class="thanksCENTER">
        	<h5>PAYMENT CONFIRMATION!</h5>
        	<p>Your payment has been received.</p>
        	<br/>
        	<div class="button-set">
                         <?php if($this->Session->read('Auth.User.role_id') == 3){?>
			<input type="submit" class="LTSpreview" value="Back to invoices" onclick="window.location.href='<?php echo SITE_URL."invoice/invoicelistclient"; ?>'; ">
			<?php }else{?>
                          <input type="submit" class="LTSpreview" value="Back to invoices" onclick="window.location.href='<?php echo SITE_URL."invoice/invoicelistmember"; ?>'; "> 
                         <?php }?>
                     </div>
	    </div>
    </div>
</div>