<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
        <div class="Profile_page_container">
        	<div id="pro-creation">
                <h1>Invoice creation
                <?php if($this->Session->read('Auth.User.role_id')=='2'){?>
                    <a title="Back" href="<?php echo SITE_URL."users/my_account";?>" style="float:right; font-size:14px;">&lt;&lt;Back to profile</a>
           <?php } ?>
            <?php if($this->Session->read('Auth.User.role_id')=='3'){?>
                        <a title="Back" href="<?php echo SITE_URL."clients/my_account";?>" style="float:right; font-size:14px;">&lt;&lt;Back to profile</a>
             <?php } ?> </h1>
            </div>
           <?php echo($this->Form->create('Invoice',array('url'=>array('controller'=>'invoice','action'=>'invoice_create'),'id'=>'invoiceForm')));  ?>
          <div class="figure-left align-left-botttom "> 
		  
		  <div id="profileImage" class="profileimg">
			<?php 
			$MentorData = $this->General->getUserInfo($this->Session->read('Auth.User.id'));
			
			if($MentorData['UserImage']['image_name'] !=''){
				$image_path = '/img/members/profile_images/'.$this->Session->read('Auth.User.id').'/'.$MentorData['UserImage']['image_name'];
			}else{
				$image_path = '/img/media/profile.png';
			}?>        
            <?php echo($this->Html->image($image_path,array('alt'=>$MentorData['UserReference']['first_name'].' '.$MentorData['UserReference']['last_name'],'class'=>'desaturate')));
			 ?>	
			</div>
			
			
			
            <div class="figcaption">
 			<h1><span><?php echo(ucfirst(strtolower($MentorData['UserReference']['first_name'])).' '.ucfirst(strtolower($MentorData['UserReference']['last_name']))); ?></span></h1>
				<p><?php echo $MentorData['location']['city_name'];?>, <?php echo $MentorData['location']['state'];?></p>	
            </div>
            <div id="ApplyPreview" style="float:right;">
				<input id="applyPreview" value="Preview" class="submitProfile" type="submit" style="margin-bottom:10px; margin-top: 170px;">
			</div>
          </div>
          
		  <div class="mentor_details mr-bt20" style="margin-top:0px; float:none;">
			<div id="mentor-detail-left1">
				<div id="area-box">
					<div class="invoce-creation">
						<h1></h1>
						<div class="formrow"></div>
						<table>
							<tbody>
								<tr>
									<td style="width: 30%;">
										From:
									</td>
									<td style="width: 70%;">
										<?php echo($this->Form->input('inv_from', array('div'=>false,'id'=>"inv_from", 'label'=>false, "class" => "forminput1" ,'style'=>'width:460px;', "placeholder" => "Name/Company of member", "value"=> $memberData['UserReference']['first_name']." ".$memberData['UserReference']['last_name'])));?>
									</td>
								</tr>
								<tr>
									<td>
										To:
									</td>
									<td>
										<?php echo($this->Form->input('inv_to', array('div'=>false,'id'=>"inv_to", 'label'=>false, "class" => "forminput1" ,'style'=>'width:460px;', "placeholder" => "Name/Company of client", "value"=> $clientData['UserReference']['first_name']." ".$clientData['UserReference']['last_name'])));?>
									</td>
								</tr>
								<tr>
									<td>
										Title:
									</td>
									<td>
										<?php echo($this->Form->input('title', array('div'=>false,'id'=>"inv_title", 'label'=>false, "class" => "forminput1" ,'style'=>'width:460px;', "placeholder" => "Title for invoice", "value"=>"Consulting Services")));?>
									</td>
								</tr>
								<tr>
									<td>
										Description:
									</td>
									<td>
										<?php echo($this->Form->input('inv_description', array('div'=>false,'id'=>"inv_description", 'label'=>false, "class" => "Testbox" ,'style'=>'width:480px;')));?>
									</td>
								</tr>
							</tbody>
						</table>
						<?php echo($this->Form->hidden('mentorship_id',array('value'=>$mentorship_id,'id'=>'mentorship_id'))); ?>
						<?php echo($this->Form->hidden('pay_in_advance',array('value'=>2,'id'=>'pay_in_advance'))); ?>
                        <div class="formrow"></div>
                      <div id="Apply" role="Apply" style="float:right;">
							<input id="apply" value="Submit" class="submitProfile" type="submit" style="margin-bottom:10px; margin-top: 20px;">
				      </div>
					</div>
				</div>
				
			</div>
			
			<div id="invoce-right">
			<div class="invoice-detail">
            <h1 style="color:#000000;"><?php 
            $invNo = 'New invoice';            
            echo $invNo; ?></h1>
            <?php 
			$price = $memberData['UserReference']['fee_regular_session'];				
            ?>
            <div class="invoice-price">
            <div class="price-row">
            <div class="left-price">Price</div>
            <div class="right-price">
            $<?php echo($this->Form->input('pricetxt', array('div'=>false,'value'=> number_format($price,2), 'label'=>false,'id'=>"pricetxt", "class" => "price-textbox", "style"=>"margin-bottom:5px;" , "placeholder" => "0.00")));?>
            </div>
            </div>
            <div class="price-row">
            <div class="left-price">Discount </div>
            <div class="right-price">
            	$<?php echo($this->Form->input('discounts', array('div'=>false,'value'=>'0.00', 'label'=>false,'id'=>"discount", "class" => "price-textbox", "style"=>"margin-bottom:5px;" , "placeholder" => "0.00")));?>
            </div>
            <div class="price-row">
            <div class="left-price"><strong>Subtotal</strong> </div>
            <div class="right-price">$<strong id="subtotal"><?php echo number_format($price,2); ?></strong></div>
            </div>
            <div class="price-row">
            <div class="left-price">Tax</div>
            <div class="right-price">
                $<?php echo($this->Form->input('taxes', array('div'=>false,'id'=>"tax",'value'=>'0.00','label'=>false, "class" => "price-textbox", "style"=>"margin-bottom:5px;" , "placeholder" => "0.00")));?>
            </div>
            </div>
            <div class="price-total">
            <div class="left-price"><strong>Total</strong></div>
            <div class="right-price">$<strong id="total"><?php echo number_format($price,2); ?></strong></div>
            </div>
            </div>
            </div>
            <div class="invoice-cal">
            <?php 
            	if($this->Session->read('Auth.User.mentor_type')=='Founding Member'):
					$rate = Configure::read('FounderMentorCommission'); 
					$MGFee = ($price)*($rate)/100;
					$myshare = $price - $MGFee;
				elseif($this->Session->read('Auth.User.mentor_type')=='Premium Member'):
					$rate = Configure::read('PremiumMemberCommission');
					$MGFee = ($price)*($rate)/100;
					$myshare = $price - $MGFee;
				else:
					$rate = Configure::read('RegularrMentorCommission');	
					$MGFee = ($price)*($rate)/100;
					$myshare = $price - $MGFee;
				endif;
				echo $this->Form->hidden('ment_rate',array('value'=>$rate,'id'=>'rateMentor'));
				echo $this->Form->hidden('ment_price',array('value'=>$price,'id'=>'priceMentor'));
				echo $this->Form->hidden('ment_discount',array('value'=>0.00,'id'=>'discountMentor'));
				echo $this->Form->hidden('ment_tax',array('value'=>0.00,'id'=>'taxMentor'));
                echo $this->Form->hidden('mentee_id',array('value'=>$clientData['User']['id']));
            ?>
            <h1 style="padding-top: 40px;">Earnings Calculator</h1>
            <div class="invoice-calbox">
            <div class="price-row">
            <div class="left-price"><strong>My Share </strong></div>
            <div class="right-price">$<strong id="myShare"><?php echo number_format($myshare,2);?></strong></div>
            </div>
            <div class="price-row">
            <div class="left-price">GUILD fee </div>
            <div class="right-price">$<strong id="MGFee"><?php echo number_format($MGFee,2);?></strong></div>
            </div>
            </div>
            </div>
			</div>
		  </div>
        </div>
        <?php echo($this->Form->end());?>
      
    </div>
  </div>
 </div>
<script type="text/javascript">
 jQuery(function() {
        
        jQuery("#pricetxt").change(function () {
       	 var onlyNumbers = /^[0-9,]+(\.\d{0,2})?$/.test(jQuery.trim(jQuery("#discount").val()));
       	 var price = jQuery("#pricetxt").val();
       	              
       	 var discount = jQuery("#discount").val();
       	 var tax = jQuery("#tax").val();
       	 var rate = jQuery('#rateMentor').val();
       	 if(!onlyNumbers || discount>price)
       	 {
       	 	jQuery('#discount').css('border-color','#F00');	
       	 	jQuery('#discount').foucs();
       	 	if(goingID==''){ goingID = 'discount'; }
       	 	globalflag++;
       	 	return false;
       	 }
       	 else
       	 {
       	 	 if(jQuery.trim(discount)==''){ discount = 0.00; }
       	 	 if(jQuery.trim(tax)==''){ tax = 0.00; }
	        	 var subtotal = parseFloat(price)-parseFloat(discount);
	        	 var total = parseFloat(subtotal) + parseFloat(tax);
	        	 var MGFee = (parseFloat(total) * parseFloat(rate)) / 100;
	        	 var myshare = total - MGFee;
	        	 goingID = '';
	        	 
	        	 jQuery("#discount").val(parseFloat(discount).toFixed(2));
                jQuery("#tax").val(parseFloat(tax).toFixed(2));
	        	 jQuery('#subtotal').html(subtotal.toFixed(2)); 
	        	 jQuery('#total').html(total.toFixed(2));
	        	 jQuery('#myShare').html(myshare.toFixed(2));
	        	 jQuery('#MGFee').html(MGFee.toFixed(2));
	        	 jQuery('#discountMentor').val(discount);
	        	 jQuery('#priceMentor').val(price);
	        	 jQuery('#taxMentor').val(tax);
	        	 jQuery('#discount').css('border-color','');  
	        	 globalflag--;
       	}        	 
       });
        jQuery("#discount").change(function () {
        	 var onlyNumbers = /^[0-9,]+(\.\d{0,2})?$/.test(jQuery.trim(jQuery("#discount").val()));
        	 var price = jQuery("#pricetxt").val();
        	              
        	 var discount = jQuery("#discount").val();
        	 var tax = jQuery("#tax").val();
        	 var rate = jQuery('#rateMentor').val();
        	 if(!onlyNumbers || discount>price)
        	 {
        	 	jQuery('#discount').css('border-color','#F00');	
        	 	jQuery('#discount').foucs();
        	 	if(goingID==''){ goingID = 'discount'; }
        	 	globalflag++;
        	 	return false;
        	 }
        	 else
        	 {
        	 	 if(jQuery.trim(discount)==''){ discount = 0.00; }
        	 	 if(jQuery.trim(tax)==''){ tax = 0.00; }
	        	 var subtotal = parseFloat(price)-parseFloat(discount);
	        	 var total = parseFloat(subtotal) + parseFloat(tax);
	        	 var MGFee = (parseFloat(total) * parseFloat(rate)) / 100;
	        	 var myshare = total - MGFee;
	        	 goingID = '';
	        	 
	        	 jQuery("#discount").val(parseFloat(discount).toFixed(2));
                 jQuery("#tax").val(parseFloat(tax).toFixed(2));
	        	 jQuery('#subtotal').html(subtotal.toFixed(2)); 
	        	 jQuery('#total').html(total.toFixed(2));
	        	 jQuery('#myShare').html(myshare.toFixed(2));
	        	 jQuery('#MGFee').html(MGFee.toFixed(2));
	        	 jQuery('#discountMentor').val(discount);
	        	 jQuery('#priceMentor').val(price);
	        	 jQuery('#taxMentor').val(tax);
	        	 jQuery('#discount').css('border-color','');  
	        	 globalflag--;
        	}        	 
        });
        jQuery("#tax").change(function () {
        	 var onlyNumbers = /^[0-9,]+(\.\d{0,2})?$/.test(jQuery.trim(jQuery("#tax").val()));
             var price = jQuery("#pricetxt").val();
             
        	 var discount = jQuery("#discount").val();
        	 var tax = jQuery("#tax").val();
        	 var rate = jQuery('#rateMentor').val();
        	 if(jQuery.trim(tax)=='' || !onlyNumbers)
        	 {
        	 	jQuery('#tax').css('border-color','#F00');	
        	 	jQuery('#tax').foucs();
        	 	return false;
        	 }
        	 else
        	 {
        	 	 if(jQuery.trim(discount)==''){ discount = 0.00; }
        	 	 if(jQuery.trim(tax)==''){ tax = 0.00; }
	        	 var subtotal = parseFloat(price)-parseFloat(discount);
	        	 var total = parseFloat(subtotal) + parseFloat(tax);
	        	 var MGFee = (parseFloat(total) * parseFloat(rate)) / 100;
	        	 var myshare = total - MGFee;
	        	 
	        	 jQuery("#discount").val(parseFloat(discount).toFixed(2));
                 jQuery("#tax").val(parseFloat(tax).toFixed(2));
	        	 jQuery('#subtotal').html(subtotal.toFixed(2)); 
	        	 jQuery('#total').html(total.toFixed(2));
	        	 jQuery('#myShare').html(myshare.toFixed(2));
	        	 jQuery('#MGFee').html(MGFee.toFixed(2));
	        	 jQuery('#discountMentor').val(discount);
	        	 jQuery('#priceMentor').val(price);
	        	 jQuery('#taxMentor').val(tax);
	        	 jQuery('#tax').css('border-color','');  
        	}        	 
        });
        
         jQuery("#apply").click(function () {

        	this.form.action = SITE_URL+'invoice/invoice_create/';
         	this.form.target = "";
            
            var onlyNumbers = /^[0-9]*$/;

            var flag = 0;
            var priceValue = jQuery('#total').html();

            var inv_from = jQuery('#inv_from').val();
            if(jQuery.trim(inv_from)=='')
            {
               jQuery('#inv_from').css('border-color','#F00');
               if(goingID==''){ goingID = 'inv_from'; }
               flag++;
            } else
            { jQuery('#inv_from').css('border-color',''); goingID='';}

            var inv_to = jQuery('#inv_to').val();
            if(jQuery.trim(inv_to)=='')
            {
               jQuery('#inv_to').css('border-color','#F00');
               if(goingID==''){ goingID = 'inv_to'; }
               flag++;
            } else
            { jQuery('#inv_to').css('border-color',''); goingID='';}

            
            var title = jQuery('#inv_title').val();
            if(jQuery.trim(title)=='')
            {
               jQuery('#inv_title').css('border-color','#F00');
               if(goingID==''){ goingID = 'inv_title'; }
               flag++;
            } else
            { jQuery('#inv_title').css('border-color',''); goingID='';}

            if(priceValue == '0.00')
            {
            	jQuery('#pricetxt').css('border-color','#F00');
                if(goingID==''){ goingID = 'pricetxt'; }
                flag++;
            } else
            { jQuery('#pricetxt').css('border-color',''); goingID='';}
            
            
            if(flag>0){ 
                        if(goingID != ""){
                            jQuery('#'+goingID).focus();
                        }
                        return false;
                      }
            else{ 
                    if(globalflag>0)
                    { 
                            if(goingID != ""){
                            jQuery('#'+goingID).focus();
                             } return false; 
                    }
                    else 
                    {   jQuery("#invoiceForm").submit();
                        jQuery("#apply").css('disabled',true);
                     } 
               }
         });

         jQuery("#applyPreview").click(function () {

        	 this.form.action = SITE_URL+'invoice/member_invoice_preview/';
        	 this.form.target="_blank";
        	 
             var onlyNumbers = /^[0-9]*$/;

             var flag = 0;
             var priceValue = jQuery('#total').html();

             var inv_from = jQuery('#inv_from').val();
             if(jQuery.trim(inv_from)=='')
             {
                jQuery('#inv_from').css('border-color','#F00');
                if(goingID==''){ goingID = 'inv_from'; }
                flag++;
             } else
             { jQuery('#inv_from').css('border-color',''); goingID='';}

             var inv_to = jQuery('#inv_to').val();
             if(jQuery.trim(inv_to)=='')
             {
                jQuery('#inv_to').css('border-color','#F00');
                if(goingID==''){ goingID = 'inv_to'; }
                flag++;
             } else
             { jQuery('#inv_to').css('border-color',''); goingID='';}

             
             var title = jQuery('#inv_title').val();
             if(jQuery.trim(title)=='')
             {
                jQuery('#inv_title').css('border-color','#F00');
                if(goingID==''){ goingID = 'inv_title'; }
                flag++;
             } else
             { jQuery('#inv_title').css('border-color',''); goingID='';}

             if(priceValue == '0.00')
             {
             	jQuery('#pricetxt').css('border-color','#F00');
                 if(goingID==''){ goingID = 'pricetxt'; }
                 flag++;
             } else
             { jQuery('#pricetxt').css('border-color',''); goingID='';}
             
             
             if(flag>0){ 
                         if(goingID != ""){
                             jQuery('#'+goingID).focus();
                         }
                         return false;
                       }
             else{ 
                     if(globalflag>0)
                     { 
                             if(goingID != ""){
                             jQuery('#'+goingID).focus();
                              } return false; 
                     }
                     else 
                     {   jQuery("#invoiceForm").submit();
                         jQuery("#apply").css('disabled',true);
                      } 
                }
          });
         
    });
</script>