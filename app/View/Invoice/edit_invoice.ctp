<?php echo($this->Html->css(array('jquery/jquery-ui'))); ?>
<?php echo($this->Html->script(array('caljs/jquery-1.9.1','caljs/jquery-ui')));?> 
<script type="text/javascript">
var globalflag = 0;
var goingID = '';
jQuery(document).ready(function(){
    window.onload=noBack();
});
</script>
<script type="text/javascript">
    jQuery(document).ready(function(){  

    // select element styling
            jQuery('select.select').each(function(){
                var title = jQuery(this).attr('title');
                if( jQuery('option:selected', this).val() != ''  ) title = jQuery('option:selected',this).text();
                jQuery(this)
                    .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
                    .after('<span class="select">' + title + '</span>')
                    .change(function(){
                        val = jQuery('option:selected',this).text();
                        jQuery(this).next().text(val);
                        })
            });
            for(i=0;i<=2;i++)
            {
                id='';
                if(i!=0)
                {id = i}
                jQuery("#time_hh"+id).keypress(function(e) {
                  if(!isNumberKey(e))
                  {e.preventDefault();}
                });
                jQuery("#time_mm"+id).keypress(function(e) {
                  if(!isNumberKey(e))
                  {e.preventDefault();}
                });
                
            }

            changeEditInvoice(); 
            return false;
            
    });
</script>
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
     
        <div class="Profile_page_container">
        <div id="pro-creation">
                <h1>Propose an appointment</h1>
            </div>
          <?php echo($this->Form->create('Invoice',array('url'=>array('controller'=>'invoice','action'=>'edit_invoice'),'id'=>'invoiceForm')));  ?>
          <div class="figure-left align-left-botttom "> 
          
          <div id="profileImage" class="profileimg">
            <?php 
            $MentorData = $this->General->getUserInfo($data['Mentorship']['mentor_id']);
            $timezoneValue = $this->General->gettimezone($this->Session->read('Auth.User.id'));
            if($MentorData['UserImage']['image_name'] !=''){
                $image_path = '/img/members/profile_images/'.$data['Mentorship']['mentor_id'].'/'.$MentorData['UserImage']['image_name'];
            }else{
                $image_path = '/img/media/profile.png';
            }?>        
            <?php echo($this->Html->image($image_path,array('alt'=>$MentorData['UserReference']['first_name'].' '.$MentorData['UserReference']['last_name'],'class'=>'desaturate')));
             ?> 
            </div>
            
            
            
            <div class="figcaption">
            <h1><span><?php echo(ucfirst(strtolower($MentorData['UserReference']['first_name'])).' '.ucfirst(strtolower($MentorData['UserReference']['last_name']))); ?></span></h1>
                <p><?php echo $MentorData['location']['city_name'];?>, <?php echo $MentorData['location']['state'];?></p> 
            </div>
            <div id="ApplyPreview" style="float:right;">
				<input id="applyPreview" value="Preview" class="submitProfile" type="submit" style="margin-bottom:10px; margin-top: 170px; 
				<?php if($data['Invoice']['pay_in_advance'] == 0){?>
				display: none
				<?php }?>
				">
			</div>
          </div>
          
          <div class="mentor_details mr-bt20" style="margin-top:0px; float:none;">
            <div id="mentor-detail-left1">
                <div id="area-box">
                    <div class="invoce-creation">
                        <h1></h1>
                      <?php /*<div class="formrow">
                        <p><b>Session Tiltle</b></p>
                        <?php echo($this->Form->input('title', array('div'=>false,'maxlength'=>70,'id'=>"title",'value'=>$data['Invoice']['title'], 'label'=>false, "class" => "forminput1" ,'style'=>'width:200px;', "placeholder" => "Enter session title")));?>
                      </div>*/ ?>
                      <div class="formrow">
                      	<div class="column3">
	                        <p><b>Title</b></p>
	                        <?php echo($this->Form->input('title', array('style'=>'width:620px;', 'div'=>false,'id'=>"inv_title", 'label'=>false, "class" => "forminput1", "placeholder" => "Title", 'value'=>$data['Invoice']['title'])));?>
	                      </div>
                      </div>
                      <div class="formrow">
                        <div class="column1">
                        <p><b>Date</b></p>
                        <?php 
                            $invDate = date('m/d/Y',$data['Invoice']['inv_date']);
                        echo($this->Form->input('inv_datex', array('disabled'=>'disabled','div'=>false,'id'=>"inv_date",'value'=>$invDate,'label'=>false, "class" => "forminput1" ,'style'=>'width:200px;',"placeholder" => "MM/DD/YYYY")));?>
                       </div>
                       <div class="column2">
                        <p><b>Time</b></p>
                        <?php 
                        $invDate1='';$invDate2='';$time1_0 ='';$time1_1 ='';$time1_2 ='';$time2_0 ='';$time2_1 ='';$time2_2 ='';
                        $timeData = explode(":",$data['Invoice']['inv_time']);
                        
                        if($data['Invoice']['inv_date1']!=0)
                            $invDate1 = date('m/d/Y',$data['Invoice']['inv_date1']);
                        if($data['Invoice']['inv_date2']!=0)
                            $invDate2 = date('m/d/Y',$data['Invoice']['inv_date2']);
                        
                        if($data['Invoice']['inv_time1']!='')
                        {
                          $timeData1 = explode(":",$data['Invoice']['inv_time1']);
                          $time1_0 = $timeData1[0];
                          $time1_1 = $timeData1[1];
                          $time1_2 = $timeData1[2]; 
                        }
                        if($data['Invoice']['inv_time2']!='')
                        { 
                            $timeData2 = explode(":",$data['Invoice']['inv_time2']);
                            $time2_0 = $timeData2[0];
                            $time2_1 = $timeData2[1];
                            $time2_2 = $timeData2[2];
                        }
                        ?>
                        <?php echo($this->Form->input('time_hh', array('div'=>false,'value'=>$timeData[0],"onblur"=>"appendzero(this);",'id'=>"time_hh",'maxlength'=>'2', 'label'=>false, "class" => "forminput1", "style"=>"width:50px; text-align:center; margin-right:10px;" , "placeholder" => "HH")));?>
                        <?php echo($this->Form->input('time_mm', array('div'=>false,'value'=>$timeData[1],"onblur"=>"appendzero(this);",'id'=>"time_mm",'maxlength'=>'2', 'label'=>false, "class" => "forminput1", "style"=>"width:50px; text-align:center; margin-right:10px;" , "placeholder" => "MM")));?>
                        <?php $options = array('AM'=>'AM','PM'=>'PM'); ?>
                        <div class="timzoneselectbox_cont">
                            <?php echo($this->Form->input('time_AM_PM', array('disabled'=>'disabled','class'=>'select','div'=>false,'label'=>false,'type'=>'select','id'=>"time_AM_PM", "style"=>"width:71px;",'options'=>$options,'selected'=>$timeData[2])));?>
                        </div>
                        <div class="selectbox_cont">
                            <?php echo($this->Form->input('timezone', array('disabled'=>'disabled','class'=>'select','div'=>false,'label'=>false,'type'=>'select','id'=>"time_zone", "style"=>"width:130px;color:#292929; margin-left:10px;",'options'=>$timezoneData,'selected'=>$timezoneValue)));?>
                        </div>
                        </div>
                        </div>
                        <div class="formrow">
                        <div class="column1">
                            <p><b>Option 2</b></p>
                            <?php echo($this->Form->input('inv_datey', array('disabled'=>'disabled','div'=>false,'id'=>"inv_date1",'value'=>$invDate1,'label'=>false, "class" => "forminput1",'style'=>'width:200px;', "placeholder" => "MM/DD/YYYY"))); ?>
                        </div>
                        <div class="column2">
                        <p><b>&nbsp;</b></p>
                            <?php echo($this->Form->input('time_hh1', array('div'=>false,'id'=>"time_hh1",'value'=>$time1_0,'maxlength'=>'2',"onblur"=>"appendzero(this);", 'label'=>false, "class" => "forminput1", "style"=>"width:50px; text-align:center; margin-right:10px;" , "placeholder" => "HH")));?>
                            <?php echo($this->Form->input('time_mm1', array('div'=>false,'id'=>"time_mm1",'value'=>$time1_1,'maxlength'=>'2',"onblur"=>"appendzero(this);", 'label'=>false, "class" => "forminput1", "style"=>"width:50px; text-align:center; margin-right:10px;" , "placeholder" => "MM")));?>
                            <?php $options = array('AM'=>'AM','PM'=>'PM'); ?>
                            <div class="timzoneselectbox_cont">
                                <?php echo($this->Form->input('time_AM_PM1', array('disabled'=>'disabled','class'=>'select','div'=>false,'label'=>false,'type'=>'select','id'=>"time_AM_PM1", "style"=>"width:71px;",'options'=>$options,'selected'=>$time1_2)));?>
                            </div>
                         </div>
                        </div> 
                        <div class="formrow">
                        <div class="column1">
                            <p><b>Option 3</b></p>
                            <?php echo($this->Form->input('inv_datez', array('disabled'=>'disabled','div'=>false,'id'=>"inv_date2",'value'=>$invDate2,'label'=>false, "class" => "forminput1" ,'style'=>'width:200px;', "placeholder" => "MM/DD/YYYY"))); ?>
                        </div>
                         <div class="column2">
                        <p><b>&nbsp;</b></p>
                            <?php echo($this->Form->input('time_hh2', array('div'=>false,'id'=>"time_hh2",'value'=>$time2_0,'maxlength'=>'2',"onblur"=>"appendzero(this);", 'label'=>false, "class" => "forminput1", "style"=>"width:50px; text-align:center; margin-right:10px;" , "placeholder" => "HH")));?>
                            <?php echo($this->Form->input('time_mm2', array('div'=>false,'id'=>"time_mm2",'value'=>$time2_1,'maxlength'=>'2',"onblur"=>"appendzero(this);", 'label'=>false, "class" => "forminput1", "style"=>"width:50px; text-align:center; margin-right:10px;" , "placeholder" => "MM")));?>
                            <?php $options = array('AM'=>'AM','PM'=>'PM'); ?>
                             <div class="timzoneselectbox_cont"> 
                            <?php echo($this->Form->input('time_AM_PM2', array('disabled'=>'disabled','class'=>'select','div'=>false,'label'=>false,'type'=>'select','id'=>"time_AM_PM2", "style"=>"width:71px;",'options'=>$options,'selected'=>$time2_2)));?>
                            </div>
                         </div>
                        </div> 
                        
                     <div class="formrow">
                     	<p><b>Duration</b></p>
                      <div class="column4">
                        <?php $options = array('00'=>'00','01'=>'01'); ?>
                       	<table>
                       		<tbody>
                       			<tr>
                       				<td>
                       					<div class="timzoneselectbox_cont">
				                        <?php echo($this->Form->input('duration_hours', array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"duration_hours", "style"=>"width:50px;",'options'=>$options,'value'=>$data['Invoice']['duration_hours'])));?>
				                        </div> 
				                    </td>
				                    <td>
				                    	Hours
				                    </td>
                       			</tr>
                       		</tbody>
                       	</table>
                        </div>
                      <div class="column4">
                        <?php $options2 = array('00'=>'00','15'=>'15','30'=>'30','45'=>'45'); ?>
                        <table>
                       		<tbody>
                       			<tr>
                       				<td>
                       					<div class="timzoneselectbox_cont">
				                        <?php echo($this->Form->input('duration_minutes', array('div'=>false,'label'=>false,'class'=>'select','type'=>'select','id'=>"duration_minutes", "style"=>"width:50px;",'options'=>$options2,'value'=>$data['Invoice']['duration_minutes'])));?>
				                        </div>
				                    </td>
				                    <td>
				                    	Minutes
				                    </td>
                       			</tr>
                       		</tbody>
                       	</table>
                        </div>
                        </div>
                        
                        
                      <div class="formrow" style="padding-top: 10px;">
                        <div id="mode">
        <p><b>Mode</b></p>
    </div>
  <?php   $face='';$online='';$phone='';
                 if(isset($data['Invoice']['inv_mode']) && $data['Invoice']['inv_mode']==1): 
                            $face='checked="checked"';
                            $txtmargin = "4px";
                            $placetext = "Enter address and click on Generate map";
                 elseif(isset($data['Invoice']['inv_mode']) && $data['Invoice']['inv_mode']==2):
                         $online='checked="checked"';
                         $txtmargin = "50px";
                         $placetext = "Enter Skype ID, etc"; 
                     elseif(isset($data['Invoice']['inv_mode']) && $data['Invoice']['inv_mode']==3):
                         $phone='checked="checked"';
                         $txtmargin = "108px";
                         $placetext = "Enter 10 digit phone number";  
                     else:
                         $face='checked="checked"';
                         $txtmargin = "4px";
                         $placetext = "Enter address and click on Generate map";
                     endif; ?>
   <div id="face-box">
        <p class="styled-form">
            <input type="radio" name="data[Invoice][inv_mode]" <?php echo $face; ?> value="1" id="a1" disabled>
            <label for="a1" style="font-family:'Ubuntu';">In person</label>
        </p>
        <p class="styled-form">
            <input type="radio" name="data[Invoice][inv_mode]" <?php echo $online; ?> value="2" id="a2" disabled>
            <label for="a2" style="font-family:'Ubuntu';">Online</label>
        </p>
        <p class="styled-form">
            <input type="radio" name="data[Invoice][inv_mode]" <?php echo $phone; ?>  value="3" id="a3" disabled>
            <label for="a3" style="font-family:'Ubuntu';">Phone</label>
        </p>
    </div>
    <div id="face-text" style="padding-top:4px; margin-top: <?php echo $txtmargin; ?>;">
        <?php echo($this->Form->input('mode_text', array('div'=>false,'id'=>"mode_text",'maxlength'=>'250','value'=>$data['Invoice']['mode_text'], 'label'=>false, "class" => "forminput1", "style"=>"margin-top:6px; width:378px;", "placeholder" => $placetext)));?>
        <?php if($data['Invoice']['inv_mode']==1){?>
        <div id="generateButton" style="margin-top:10px;">
                <a href="javascript:void(0);" onclick="genMap();">Generate map</a>&nbsp;<a id="hideMapText" style="display: none;" href="javascript:void(0);" onclick="hideMap();">|&nbsp;Hide map</a>
                <img src="<?php echo SITE_URL?>img/spinner.gif" id="preload" style="display:none;" />
       </div>
       <div style="float: inherit; margin-top: 20px;" id="mapDiv">  
          <?php echo  "<iframe width='400' height='200' frameborder='0' scrolling='no'  marginheight='0' marginwidth='0' src='https://maps.google.com/maps?&amp;q=".urlencode($data['Invoice']['mode_text'])."&amp;output=embed&amp;iwloc=near'></iframe>"; ?>
       </div>
      <?php }?> 
    </div>
                        
                      </div>
                      <div class="formrow">
                        <p><strong>Comments</strong></p>
                        <?php echo($this->Form->textarea('comment', array('rows'=>6,'cols'=>'30','value'=>$data['Invoice']['comment'], 'label'=>false, 'id'=>'text-area', "style"=>"width:580px;padding:10px;" , "placeholder" => "Enter your comment here")));?>
                      </div>
                      <div id="Apply" role="Apply" style="float: right; margin-right:70px;">
                            <input id="resendButton" value="Resend" class="submitProfile" type="submit" style="margin-bottom:10px; margin-left: 10px;">
                      </div>
                    </div>
                </div>
                
            </div>
            <?php /**
            <div id="invoce-right">
            	<h2> </h2>
			    <div>
			    	<p style="margin-top: 10px;">	Does client need to pay in advance?</p>
			    	<?php if($data['Invoice']['pay_in_advance'] == 0){?>
					<select onchange="sel(this.value)" style="font-size: 14px; font-family: 'proximanova semibold',Ubuntu; color: #000; font-weight: bold;">
						<option value="No" selected="selected">No</option>
						<option value="Yes">Yes</option>
					</select>			
					<?php } else {?>
					<select onchange="sel(this.value)" style="font-size: 14px; font-family: 'proximanova semibold',Ubuntu; color: #000; font-weight: bold;">
						<option value="No">No</option>
						<option value="Yes" selected="selected">Yes</option>
					</select>	
					<?php }?>
				</div>**/?>
				<?php echo($this->Form->hidden('pay_in_advance',array('value'=>$data['Invoice']['pay_in_advance'],'id'=>'pay_in_advance'))); ?>
				
            <div class="invoice-detail" id="invoice_details" 
            <?php if($data['Invoice']['pay_in_advance'] == 0){?>
            style="display: none"
            <?php }?>>
            <h1 style="color: #000000;">Invoice: <?php echo $data['Invoice']['inv_no']; ?></h1>
            <?php 
            $price = $data['Invoice']['price'];
            $discount = $data['Invoice']['discount'];
            $subTotal = $data['Invoice']['price']-$data['Invoice']['discount'];
            $tax = $data['Invoice']['tax'];
            $total = $subTotal + $tax;
            ?>
            <div class="invoice-price">
            <div class="price-row">
            <div class="left-price">Price</div>
            <div class="right-price">
            $<?php echo($this->Form->input('pricetxt', array('div'=>false,'value'=> number_format($price,2), 'label'=>false,'id'=>"pricetxt", "class" => "price-textbox", "style"=>"margin-bottom:5px;" , "placeholder" => "0.00")));?>
            </div>
            </div>
            <div class="price-row">
            <div class="left-price">Discount </div>
            <div class="right-price">
                $<?php echo($this->Form->input('discounts', array('div'=>false,'value'=> $discount, 'label'=>false,'id'=>"discount", "class" => "price-textbox", "style"=>"margin-bottom:5px;" , "placeholder" => "0.00")));?>
            </div>
            <div class="price-row">
            <div class="left-price"><strong>Subtotal</strong> </div>
            <div class="right-price">$<strong id="subtotal"><?php echo number_format($subTotal,2); ?></strong></div>
            </div>
            <div class="price-row">
            <div class="left-price">Tax</div>
            <div class="right-price">
                $<?php echo($this->Form->input('taxes', array('div'=>false,'id'=>"tax",'value'=> $tax,'label'=>false, "class" => "price-textbox", "style"=>"margin-bottom:5px;" , "placeholder" => "0.00")));?>
            </div>
            </div>
            <div class="price-total">
            <div class="left-price"><strong>Total</strong></div>
            <div class="right-price">$<strong id="total"><?php echo number_format($total,2); ?></strong></div>
            </div>
            </div>
            </div>
            <div class="invoice-cal">
              <?php 
                if($this->Session->read('Auth.User.mentor_type')=='Founding Member'):
                    $rate = Configure::read('FounderMentorCommission'); 
                    $MGFee = ($total)*($rate)/100;
                    $myshare = $total - $MGFee;
                elseif($this->Session->read('Auth.User.mentor_type')=='Premium Member'):
	                $rate = Configure::read('PremiumMemberCommission');
	                $MGFee = ($total)*($rate)/100;
	                $myshare = $total - $MGFee;
                else:
                    $rate = Configure::read('RegularrMentorCommission');    
                    $MGFee = ($total)*($rate)/100;
                    $myshare = $total - $MGFee;
                endif;
                echo $this->Form->hidden('ment_rate',array('value'=>$rate,'id'=>'rateMentor'));
                echo $this->Form->hidden('ment_price',array('value'=>$price,'id'=>'priceMentor'));
                echo $this->Form->hidden('ment_discounts',array('value'=>$discount,'id'=>'discountMentor'));
                echo $this->Form->hidden('ment_taxes',array('value'=>$tax,'id'=>'taxMentor'));
                echo $this->Form->hidden('mentorship_id',array('value'=>$data['Invoice']['mentorship_id']));
                echo $this->Form->hidden('id',array('value'=>$data['Invoice']['id']));
                echo $this->Form->hidden('clickValue',array('value'=>'deny'));
                echo $this->Form->hidden('mentee_id',array('value'=>$data['Mentorship']['mentee_id']));
           ?>
            <h1 style="padding-top: 40px;">Earnings Calculator</h1>
            <div class="invoice-calbox">
            <div class="price-row">
            <div class="left-price"><strong>My Share </strong></div>
            <div class="right-price">$<strong id="myShare"><?php echo number_format($myshare,2);?></strong></div>
            </div>
            <div class="price-row">
            <div class="left-price">GUILD fee </div>
            <div class="right-price">$<strong id="MGFee"><?php echo number_format($MGFee,2);?></strong></div>
            </div>
            </div>
            </div>
            </div>
          </div>
        </div>
        <?php echo($this->Form->end());?>
      
    </div>
  </div>
<script type="text/javascript">

function sel(value)
{
	if(value=="Yes")
	{
		document.getElementById('invoice_details').style.display='block';
		document.getElementById('pay_in_advance').value = 1;
		document.getElementById('applyPreview').style.display='block';
	}
	else if(value=="No"){
		document.getElementById('invoice_details').style.display='none';
		document.getElementById('pay_in_advance').value = 0;
		document.getElementById('applyPreview').style.display='none';
	}
}

 jQuery(function() {
         jQuery("form").bind("keypress", function(e) {
                if (e.keyCode == 13) {
                    return false;
                }
            });
        jQuery( "#inv_date" ).datepicker({minDate:0});
        jQuery( "#inv_date1" ).datepicker({minDate:0});
        jQuery( "#inv_date2" ).datepicker({minDate:0});

        jQuery("#pricetxt").change(function () {
         	 var onlyNumbers = /^[0-9,]+(\.\d{0,2})?$/.test(jQuery.trim(jQuery("#discount").val()));
         	 var price = jQuery("#pricetxt").val();
         	              
         	 var discount = jQuery("#discount").val();
         	 var tax = jQuery("#tax").val();
         	 var rate = jQuery('#rateMentor').val();
         	 if(!onlyNumbers || discount>price)
         	 {
         	 	jQuery('#discount').css('border-color','#F00');	
         	 	jQuery('#discount').foucs();
         	 	if(goingID==''){ goingID = 'discount'; }
         	 	globalflag++;
         	 	return false;
         	 }
         	 else
         	 {
         	 	 if(jQuery.trim(discount)==''){ discount = 0.00; }
         	 	 if(jQuery.trim(tax)==''){ tax = 0.00; }
  	        	 var subtotal = parseFloat(price)-parseFloat(discount);
  	        	 var total = parseFloat(subtotal) + parseFloat(tax);
  	        	 var MGFee = (parseFloat(total) * parseFloat(rate)) / 100;
  	        	 var myshare = total - MGFee;
  	        	 goingID = '';
  	        	 
  	        	 jQuery("#discount").val(parseFloat(discount).toFixed(2));
                  jQuery("#tax").val(parseFloat(tax).toFixed(2));
  	        	 jQuery('#subtotal').html(subtotal.toFixed(2)); 
  	        	 jQuery('#total').html(total.toFixed(2));
  	        	 jQuery('#myShare').html(myshare.toFixed(2));
  	        	 jQuery('#MGFee').html(MGFee.toFixed(2));
  	        	 jQuery('#discountMentor').val(discount);
  	        	 jQuery('#priceMentor').val(price);
  	        	 jQuery('#taxMentor').val(tax);
  	        	 jQuery('#discount').css('border-color','');  
  	        	 globalflag--;
         	}        	 
         });
        jQuery("#discount").change(function () {
             var onlyNumbers = /^[0-9,]+(\.\d{0,2})?$/.test(jQuery.trim(jQuery("#discount").val()));
             var price = jQuery("#pricetxt").val();
             
             var discount = jQuery("#discount").val();
             var tax = jQuery("#tax").val();
             var rate = jQuery('#rateMentor').val();
             if(!onlyNumbers || discount>price)
             {
                jQuery('#discount').css('border-color','#F00');
                jQuery('#discount').focus(); 
                goingID = 'discount';
                globalflag++;
                return false;
             }
             else
             {
                 if(jQuery.trim(discount)==''){ discount = 0.00; }
                 if(jQuery.trim(tax)==''){ tax = 0.00; }
                 var subtotal = parseFloat(price)-parseFloat(discount);
                 var total = parseFloat(subtotal) + parseFloat(tax);
                 var MGFee = (parseFloat(total) * parseFloat(rate)) / 100;
                 var myshare = total - MGFee;
                 goingID = '';
                 jQuery("#discount").val(parseFloat(discount).toFixed(2));
                 jQuery("#tax").val(parseFloat(tax).toFixed(2));
                 jQuery('#subtotal').html(subtotal.toFixed(2)); 
                 jQuery('#total').html(total.toFixed(2));
                 jQuery('#myShare').html(myshare.toFixed(2));
                 jQuery('#MGFee').html(MGFee.toFixed(2));
                 jQuery('#discountMentor').val(discount);
                 jQuery('#priceMentor').val(price);
                 jQuery('#taxMentor').val(tax);
                 jQuery('#discount').css('border-color','');
                 globalflag--;
            }            
        });
        jQuery("#tax").change(function () {
             var onlyNumbers = /^[0-9,]+(\.\d{0,2})?$/.test(jQuery.trim(jQuery("#tax").val()));
             var price = jQuery("#pricetxt").val();
             
             var discount = jQuery("#discount").val();
             var tax = jQuery("#tax").val();
             var rate = jQuery('#rateMentor').val();
             if(jQuery.trim(tax)=='' || !onlyNumbers)
             {
                jQuery('#tax').css('border-color','#F00');
                jQuery('#tax').focus();
                if(goingID==''){ goingID = 'tax'; }
               // globalflag++;
                return false;
             }
             else
             {
                 if(jQuery.trim(discount)==''){ discount = 0.00; }
                 if(jQuery.trim(tax)==''){ tax = 0.00; }
                 var subtotal = parseFloat(price)-parseFloat(discount);
                 var total = parseFloat(subtotal) + parseFloat(tax);
                 var MGFee = (parseFloat(total) * parseFloat(rate)) / 100;
                 var myshare = total - MGFee;
                 
                 jQuery("#discount").val(parseFloat(discount).toFixed(2));
                 jQuery("#tax").val(parseFloat(tax).toFixed(2));
                 jQuery('#subtotal').html(subtotal.toFixed(2)); 
                 jQuery('#total').html(total.toFixed(2));
                 jQuery('#myShare').html(myshare.toFixed(2));
                 jQuery('#MGFee').html(MGFee.toFixed(2));
                 jQuery('#discountMentor').val(discount);
                 jQuery('#priceMentor').val(price);
                 jQuery('#taxMentor').val(tax);
                 jQuery('#tax').css('border-color','');
            }            
        });
        
        /*jQuery(".denybutton").click(function() {
            jQuery("#InvoiceClickValue").val('deny');
            jQuery("#invoiceForm").submit();
        });*/
        jQuery("#editButton").click(function () {
            changeEditInvoice(); 
            jQuery('#Apply').html('<input id="resendButton" value="Resend" class="submitProfile" type="submit" style="margin-bottom:10px; margin-left: 10px;">');
            jQuery('#inv_date').focus();
            return false;
        });
        $("#Apply").on("click", "#resendButton", function(){

        	this.form.action = SITE_URL+'invoice/edit_invoice/';
          	this.form.target="";
          	 
            var actionValue = jQuery(this).val();
            if(actionValue=='Resend')
            {   
                jQuery("#InvoiceClickValue").val('edit');
            }
            var onlyNumbersHH = /^[0-9]*$/.test(jQuery.trim(jQuery("#time_hh").val()));
            var onlyNumbersMM = /^[0-9]*$/.test(jQuery.trim(jQuery("#time_mm").val()));
            var onlyNumbers = /^[0-9]*$/;
            var invDate = jQuery('#inv_date').val();
            var invHH   = jQuery('#time_hh').val();
            var invMM   = jQuery('#time_mm').val();
            
            var invDate1 = jQuery('#inv_date1').val();
            var invDate2 = jQuery('#inv_date2').val();
            var invHH1  = jQuery('#time_hh1').val();
            var invMM1  = jQuery('#time_mm1').val();
            var invHH2  = jQuery('#time_hh2').val();
            var invMM2  = jQuery('#time_mm2').val();
            
            var invMode = jQuery('#mode_text').val();
            var invComment = jQuery('#text-area').val();
          //  var title = jQuery('#title').val();
            var flag = 0;
            var gotoid = '';
            var priceValue = jQuery('#total').html();
            /*if(jQuery.trim(title)=='')
            {
               jQuery('#title').css('border-color','#F00');
               flag++;
            }
            else{ jQuery('#title').css('border-color',''); }*/

            var title = jQuery('#inv_title').val();
            if(jQuery.trim(title)=='')
            {
               jQuery('#inv_title').css('border-color','#F00');
               if(gotoid==''){gotoid = 'inv_title';}
               flag++;
            } else
            { jQuery('#inv_title').css('border-color','');}
            
            if(jQuery.trim(invDate)=='')
            {
               jQuery('#inv_date').css('border-color','#F00');
               if(gotoid==''){gotoid = 'inv_date';}
               flag++;
            }
            else{ jQuery('#inv_date').css('border-color','');}
            
            if(jQuery.trim(invHH)=='' || !onlyNumbersHH || invHH>12)
            {
               jQuery('#time_hh').css('border-color','#F00');
               if(gotoid==''){gotoid = 'time_hh';}
               flag++;
            }
            else{ jQuery('#time_hh').css('border-color','');}
            
            if(jQuery.trim(invMM)=='' || !onlyNumbersMM || invMM>59)
            {
               jQuery('#time_mm').css('border-color','#F00');
               if(gotoid==''){gotoid = 'time_mm';}
               flag++;
            }
            else{ jQuery('#time_mm').css('border-color','');}
            
           
            if(jQuery.trim(invDate1)!='' || jQuery.trim(invHH1)!='' || jQuery.trim(invMM1)!='')
            {
                if(jQuery.trim(invDate1)=='')
                {
                   jQuery('#inv_date1').css('border-color','#F00');
                   if(gotoid==''){gotoid = 'inv_date1';}
                   flag++;
                }
                else{ jQuery('#inv_date1').css('border-color','');}
                
                if(jQuery.trim(invHH1)=='' || !onlyNumbers.test(invHH1) || invHH1>12)
                {
                   jQuery('#time_hh1').css('border-color','#F00');
                   if(gotoid==''){gotoid = 'time_hh1';}
                   flag++;
                }
                else{ jQuery('#time_hh1').css('border-color','');}  
                
                if(jQuery.trim(invMM1)=='' || !onlyNumbers.test(invMM1) || invMM1>59)
                {
                   jQuery('#time_mm1').css('border-color','#F00');
                   if(gotoid==''){gotoid = 'time_mm1';}
                   flag++;
                }
                else{ jQuery('#time_mm1').css('border-color',''); }
            }
            else
            {
                jQuery('#time_hh1').css('border-color','');
                jQuery('#time_mm1').css('border-color','');
                jQuery('#inv_date1').css('border-color','');
            }
            if(jQuery.trim(invDate2)!='' || jQuery.trim(invHH2)!='' || jQuery.trim(invMM2)!='')
            {
                if(jQuery.trim(invDate2)=='')
                {
                   jQuery('#inv_date2').css('border-color','#F00');
                   if(gotoid==''){gotoid = 'inv_date2';}
                   flag++;
                }
                else{ jQuery('#inv_date2').css('border-color','');}
                
                if(jQuery.trim(invHH2)=='' || !onlyNumbers.test(invHH2) || invHH1>12)
                {
                   jQuery('#time_hh2').css('border-color','#F00');
                   if(gotoid==''){gotoid = 'time_hh2';}
                   flag++;
                }
                else{ jQuery('#time_hh2').css('border-color',''); } 
                
                if(jQuery.trim(invMM2)=='' || !onlyNumbers.test(invMM2) || invMM2>59)
                {
                   jQuery('#time_mm2').css('border-color','#F00');
                   if(gotoid==''){gotoid = 'time_mm2';}
                   flag++;
                }
                else{ jQuery('#time_mm2').css('border-color','');} 
            }
            else
            {
                jQuery('#time_hh2').css('border-color','');
                jQuery('#time_mm2').css('border-color','');
                jQuery('#inv_date2').css('border-color','');
            }
            
            var duration_hours = jQuery('#duration_hours').val();
            var duration_minutes = jQuery('#duration_minutes').val();

            if(duration_hours == 0 && duration_minutes == 0) {
            	jQuery('#duration_hours').css('border-color','#F00');
            	jQuery('#duration_minutes').css('border-color','#F00');
            	if(gotoid==''){gotoid = 'duration_hours';}
                flag++;
            }
            else{ 
                jQuery('#duration_hours').css('border-color','');
                jQuery('#duration_minutes').css('border-color',''); 
            }
            
            if(jQuery.trim(invMode)=='')
            {
               jQuery('#mode_text').css('border-color','#F00');
               if(gotoid==''){gotoid = 'mode_text';}
               flag++;
            }
            else{ jQuery('#mode_text').css('border-color','');}
            
            if(flag>0){ 
                        if(goingID != ""){
                            jQuery('#'+goingID).focus();
                            
                        }else if(gotoid != ""){
                            jQuery('#'+gotoid).focus();
                        }
                        return false;
                      }
            else{ 
                    if(globalflag>0)
                    { 
                            if(goingID != ""){
                            jQuery('#'+goingID).focus();
                             } return false; 
                    }
                    else 
                    {   jQuery("#invoiceForm").submit();
                        jQuery("#apply").css('disabled',true);
                     } 
               }
         });

        jQuery("#applyPreview").click(function () {

       	 this.form.action = SITE_URL+'invoice/member_invoice_preview/';
       	 this.form.target="_blank";
            
            var onlyNumbersHH = /^[0-9]*$/.test(jQuery.trim(jQuery("#time_hh").val()));
            var onlyNumbersMM = /^[0-9]*$/.test(jQuery.trim(jQuery("#time_mm").val()));
            var onlyNumbers = /^[0-9]*$/;
            var invDate = jQuery('#inv_date').val();
            var invHH   = jQuery('#time_hh').val();
            var invMM   = jQuery('#time_mm').val();
            
            var invDate1 = jQuery('#inv_date1').val();
            var invDate2 = jQuery('#inv_date2').val();
            var invHH1  = jQuery('#time_hh1').val();
            var invMM1  = jQuery('#time_mm1').val();
            var invHH2  = jQuery('#time_hh2').val();
            var invMM2  = jQuery('#time_mm2').val();
            
            var invMode = jQuery('#mode_text').val();
            var invComment = jQuery('#text-area').val();
          //  var title = jQuery('#title').val();
            var flag = 0;
            var gotoid = '';
            var priceValue = jQuery('#total').html();
            /*if(jQuery.trim(title)=='')
            {
               jQuery('#title').css('border-color','#F00');
               flag++;
            }
            else{ jQuery('#title').css('border-color',''); }*/

            var title = jQuery('#inv_title').val();
            if(jQuery.trim(title)=='')
            {
               jQuery('#inv_title').css('border-color','#F00');
               if(gotoid==''){gotoid = 'inv_title';}
               flag++;
            } else
            { jQuery('#inv_title').css('border-color','');}
            
            if(jQuery.trim(invDate)=='')
            {
               jQuery('#inv_date').css('border-color','#F00');
               if(gotoid==''){gotoid = 'inv_date';}
               flag++;
            }
            else{ jQuery('#inv_date').css('border-color','');}
            
            if(jQuery.trim(invHH)=='' || !onlyNumbersHH || invHH>12)
            {
               jQuery('#time_hh').css('border-color','#F00');
               if(gotoid==''){gotoid = 'time_hh';}
               flag++;
            }
            else{ jQuery('#time_hh').css('border-color','');}
            
            if(jQuery.trim(invMM)=='' || !onlyNumbersMM || invMM>59)
            {
               jQuery('#time_mm').css('border-color','#F00');
               if(gotoid==''){gotoid = 'time_mm';}
               flag++;
            }
            else{ jQuery('#time_mm').css('border-color','');}
            
           
            if(jQuery.trim(invDate1)!='' || jQuery.trim(invHH1)!='' || jQuery.trim(invMM1)!='')
            {
                if(jQuery.trim(invDate1)=='')
                {
                   jQuery('#inv_date1').css('border-color','#F00');
                   if(gotoid==''){gotoid = 'inv_date1';}
                   flag++;
                }
                else{ jQuery('#inv_date1').css('border-color','');}
                
                if(jQuery.trim(invHH1)=='' || !onlyNumbers.test(invHH1) || invHH1>12)
                {
                   jQuery('#time_hh1').css('border-color','#F00');
                   if(gotoid==''){gotoid = 'time_hh1';}
                   flag++;
                }
                else{ jQuery('#time_hh1').css('border-color','');}  
                
                if(jQuery.trim(invMM1)=='' || !onlyNumbers.test(invMM1) || invMM1>59)
                {
                   jQuery('#time_mm1').css('border-color','#F00');
                   if(gotoid==''){gotoid = 'time_mm1';}
                   flag++;
                }
                else{ jQuery('#time_mm1').css('border-color',''); }
            }
            else
            {
                jQuery('#time_hh1').css('border-color','');
                jQuery('#time_mm1').css('border-color','');
                jQuery('#inv_date1').css('border-color','');
            }
            if(jQuery.trim(invDate2)!='' || jQuery.trim(invHH2)!='' || jQuery.trim(invMM2)!='')
            {
                if(jQuery.trim(invDate2)=='')
                {
                   jQuery('#inv_date2').css('border-color','#F00');
                   if(gotoid==''){gotoid = 'inv_date2';}
                   flag++;
                }
                else{ jQuery('#inv_date2').css('border-color','');}
                
                if(jQuery.trim(invHH2)=='' || !onlyNumbers.test(invHH2) || invHH1>12)
                {
                   jQuery('#time_hh2').css('border-color','#F00');
                   if(gotoid==''){gotoid = 'time_hh2';}
                   flag++;
                }
                else{ jQuery('#time_hh2').css('border-color',''); } 
                
                if(jQuery.trim(invMM2)=='' || !onlyNumbers.test(invMM2) || invMM2>59)
                {
                   jQuery('#time_mm2').css('border-color','#F00');
                   if(gotoid==''){gotoid = 'time_mm2';}
                   flag++;
                }
                else{ jQuery('#time_mm2').css('border-color','');} 
            }
            else
            {
                jQuery('#time_hh2').css('border-color','');
                jQuery('#time_mm2').css('border-color','');
                jQuery('#inv_date2').css('border-color','');
            }
            
            var duration_hours = jQuery('#duration_hours').val();
            var duration_minutes = jQuery('#duration_minutes').val();

            if(duration_hours == 0 && duration_minutes == 0) {
            	jQuery('#duration_hours').css('border-color','#F00');
            	jQuery('#duration_minutes').css('border-color','#F00');
            	if(gotoid==''){gotoid = 'duration_hours';}
                flag++;
            }
            else{ 
                jQuery('#duration_hours').css('border-color','');
                jQuery('#duration_minutes').css('border-color',''); 
            }
            
            if(jQuery.trim(invMode)=='')
            {
               jQuery('#mode_text').css('border-color','#F00');
               if(gotoid==''){gotoid = 'mode_text';}
               flag++;
            }
            else{ jQuery('#mode_text').css('border-color','');}
            
            if(flag>0){ 
                        if(goingID != ""){
                            jQuery('#'+goingID).focus();
                            
                        }else if(gotoid != ""){
                            jQuery('#'+gotoid).focus();
                        }
                        return false;
                      }
            else{ 
                    if(globalflag>0)
                    { 
                            if(goingID != ""){
                            jQuery('#'+goingID).focus();
                             } return false; 
                    }
                    else 
                    {   jQuery("#invoiceForm").submit();
                        jQuery("#apply").css('disabled',true);
                     } 
               }
         });

        
       jQuery(".styled-form input[type='radio']").click(function () {
            var modeValue =jQuery(".styled-form input[type='radio']:checked").attr('value'); 
            if(modeValue==1)
            {
                jQuery('#generateButton').show();
                jQuery('#mode_text').attr('value','');
                jQuery('#mode_text').attr('placeholder','Enter address and click on Generate map');
                jQuery('#face-text').css('margin-top','4px'); 
            } 
            else
            {
                if(modeValue==2)
                {
                  jQuery('#mode_text').attr('value','');  
                  jQuery('#mode_text').attr('placeholder','Enter Skype ID, etc'); 
                  jQuery('#face-text').css('margin-top','50px');  
                }
                if(modeValue==3)
                {
                  jQuery('#mode_text').attr('value','');  
                  jQuery('#mode_text').attr('placeholder','Enter 10 digit phone number'); 
                  jQuery('#face-text').css('margin-top','108px');  
                }
                jQuery('#generateButton').hide();
                jQuery('#mapDiv').hide();
            }   
           });   
         
    });
</script>