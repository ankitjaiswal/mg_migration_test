<?php
$event=$invoiceData['Invoice']['inv_no']; 
$title='Guild '.$invoiceData['Invoice']['title'];
$start_date = date('Ymd',$invoiceData['Invoice']['inv_date']);
$end_date   = date('Ymd',$invoiceData['Invoice']['inv_date']);
$description = 'GUILD '.ucwords($invoiceData['Invoice']['title']).' Scheduled '.ucfirst($invoiceData['Mentor']['first_name']).' '.ucfirst($invoiceData['Mentor']['last_name']).' & '.ucfirst($invoiceData['Mentee']['first_name']).' '.ucfirst($invoiceData['Mentee']['last_name']);
$start_time = '';
$end_time = '';
$timeData = $this->General->timeformatchange($invoiceData['Invoice']['inv_time'], $invoiceData['Invoice']['duration_hours'], $invoiceData['Invoice']['duration_minutes']);
$durationInSeconds = ((($invoiceData['Invoice']['duration_hours'] * 60 ) + $invoiceData['Invoice']['duration_minutes']) * 60);
$timeValue = explode("-",$timeData);
$timeZoneValue = $this->General->getTimeZoneById($invoiceData['Invoice']['inv_timezone']);

if($timeZoneValue!="")
{
//    $zoneValue = $timeZoneValue."/Honolulu";
    switch($timeZoneValue)
    {
        case "Hawaii Time":
                $zoneValue = "Pacific/Honolulu";
                break;                
        case "Alaska Time":
                $zoneValue = "America/Anchorage";
                break;
        case "Pacific Time":
                $zoneValue = "America/Los_Angeles";
                break;
        case "Mountain Time":
                $zoneValue = "America/Phoenix";
                break;
        case "Central Time":
                $zoneValue = "America/Chicago";
                break;
        case "Eastern Time":
                $zoneValue = "America/New_York";
                break;               
    }
}else 
    $zoneValue = 'Hawaii Time(HST)/Honolulu';
//pr($timeValue);
/*$firstTime  = $invoiceData['Invoice']['inv_date'] + strtotime(substr(trim($timeValue[0]),0,5)); 
$secondTime = $invoiceData['Invoice']['inv_date']  + strtotime(substr(trim($timeValue[1]),0,5));*/


//$ff = strtotime("2013-04-16 15:30:00");
//$ss = strtotime("2013-04-16 16:30:00"); 

$ff = strtotime(date("Y-m-d",$invoiceData['Invoice']['inv_date'])." ".substr(trim($timeValue[0]),0,5).":00".substr(trim($timeValue[0]),6,8));
$ss = $ff + $durationInSeconds;


//echo strtotime(substr(trim($timeValue[1]),0,5)); die;
//echo trim($timeValue[0])."<br/>";
//echo trim($timeValue[1]); die;

$file_name = microtime(true) .'.ical';
$start_time = date("Ymd", $ff) .'T'. date("H", $ff) . date("i", $ff) .'00';

if($end_date != false){
$end_time = date("Ymd", $ss) .'T'. date("H", $ss) . date("i", $ss) .'00';
}
$this->Ical->create('GUILD', $title, $zoneValue,$start_time);
//$url2=Configure::read('Site.Url')."event_detail/".$event."/";   
$this->Ical->addEvent($start_time, $end_time, $title, $description, array('UID'=>$event, 'URL'=>'', 'location'=>$zoneValue ,'attach'=>''),$zoneValue);
$this->Ical->render();
?>