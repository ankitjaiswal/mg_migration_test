<script type="text/javascript">
jQuery(document).ready(function(){
    window.onload=noBack1();
});
function noBack1() {
        window.history.forward(); }
</script>

<script src="https://checkout.stripe.com/checkout.js"></script>
<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
       <input type="hidden" name="Language" value="<?php echo($data['VisitorInvoice']['id']);?>" id="invoiceId">     
        <div class="Profile_page_container">
            
        <div id="pro-creation">
                <h1>Order confirmation
                <?php if($this->Session->read('Auth.User.role_id')=='2'){?>
                    <a title="Back" href="<?php echo SITE_URL."users/my_account";?>" style="float:right; font-size:14px;">&lt;&lt;Back to profile</a>
           <?php } ?>
            <?php if($this->Session->read('Auth.User.role_id')=='3'){?>
                        <a title="Back" href="<?php echo SITE_URL."clients/my_account";?>" style="float:right; font-size:14px;">&lt;&lt;Back to profile</a>
             <?php } ?> </h1>
            </div>
            <?php
            echo $this->Layout->sessionFlash();
        ?>
          <div class="orderconformation">
     <?php //echo($this->Form->create('Invoice',array('url'=>array('controller'=>'invoice','action'=>'invoice_payment'),'id'=>'invoiceForm')));  ?>  
          <div class="mentor_details mr-bt20" style="margin-top:0px; float:none;">
            <div id="mentor-detail-left1">
                <div id="area-box">
                    <div class="invoce-creation">
                    <div class="orderdetail">
                    
                      	<?php 
                        $price = $data['VisitorInvoice']['price'];
                        $discount = $data['VisitorInvoice']['discount'];
                        $subTotal = $data['VisitorInvoice']['price']-$data['VisitorInvoice']['discount'];
                        $tax = $data['VisitorInvoice']['tax'];
                        $total = $subTotal + $tax; ?> 
                        

                    <h1>Title </h1>
                    <div class="content">
                    <h2><?php echo ucfirst($data['VisitorInvoice']['title']); ?>: <strong>
                        <?php echo ucfirst(strtolower($memberData['UserReference']['first_name'])).' '.ucfirst(strtolower($memberData['UserReference']['last_name'])); ?> - <?php  echo ucfirst(strtolower($data['VisitorInvoice']['inv_client_name'])); ?></strong></h2>
                    </div>
                 		<div class="orderdetail">
	                 		<table style="width: 100%;">
                 				<tbody>
                 					<tr> 
                 						<td style="width: 30%;"><strong>Title: </strong></td><td><?php echo $data['VisitorInvoice']['title']?></td>
                 					</tr>
                 					<tr>
                 						<td><strong>To: </strong></td><td><?php echo $data['VisitorInvoice']['inv_to']?></td>
                 					</tr>
                 					<tr>
                 						<td><strong>From: </strong></td><td><?php echo $data['VisitorInvoice']['inv_from']?></td>
                 					</tr>
                 					<tr>
                 						<td><strong>Description: </strong></td><td><?php echo $data['VisitorInvoice']['inv_description']?></td>
                 					</tr>
                 				</tbody>

                 			</table>
                 		</div>
                 	
                    
 
			     <div id="Apply" role="Apply" style="float: right;">
                               <input id="customButton" value="Pay Invoice" class="submitProfile" type="submit" style="margin:20px 12px 10px 10px;">

	                       	</div>

                      
                      <div class="note">
                      <div class="formrow">
                        <p><b>Note:</b></p>
                        <p> You should pay fees (if any) only if all the terms are acceptable.</p>
                      </div>
                      </div>
                    </div>
                </div>
              </div>  
            </div>

          

                      <div id="invoce-right">
                        <div class="invoice-detail">
                        <h1 style="color: #000000; ">Invoice: <?php echo $data['VisitorInvoice']['inv_no'];?></h1>
                        <div class="invoice-price">
                        <div class="price-row">
                        <div class="left-price">Price</div>
                        <div class="right-price">$<?php echo number_format($price,2); ?></div>
                        </div>
                        <div class="price-row">
                        <div class="left-price">Discount </div>
                        <div class="right-price">$<?php echo number_format($discount,2); ?></div>
                        </div>
                        <div class="price-row">
                        <div class="left-price"><strong>Subtotal</strong> </div>
                        <div class="right-price"><strong>$<?php echo number_format($subTotal,2); ?></strong></div>
                        </div>
                        <div class="price-row">
                        <div class="left-price">Tax</div>
                        <div class="right-price">$<?php echo number_format($tax,2); ?></div>
                        </div>
                        <div class="price-total">
                        <div class="left-price"><strong>Total</strong></div>
                        <div class="right-price">$<strong id="totalprice"><?php echo number_format($total,2); ?></strong></div>
                        </div>
                        </div>
                        </div>
                    </div>          

          </div>
        </div>
      
    </div>
  </div>

<script type="text/javascript">
var invoiceId = jQuery('#invoiceId').val();
var price = jQuery('#totalprice').html() *100;
var visitor = 1;
console.log(invoiceId);
  var handler = StripeCheckout.configure({
    key: 'pk_live_vg2OVxfTe0Iw5FGSMhitLWE7',
    image: 'https://www.guild.im/imgs/guild_monogram.png',
    locale: 'auto',
    token: function(token) {
      // Use the token to create the charge with a server-side script.
      // You can access the token ID with `token.id`
console.log("After payment Made");

 jQuery.ajax({
					url:SITE_URL+'invoice/stripecheckout',
					type:'post',
					dataType:'json',
					data: 'invoiceId='+invoiceId + '&price=' + price + '&token=' + token.id + '&visitor=' + visitor,
					success:function(invoiceId){
						window.location.href = SITE_URL+'invoice/thanks';
					}
				});
    }
  });

  jQuery('#customButton').live('click', function(e) {



console.log("Button Clicked");

    // Open Checkout with further options
    handler.open({
      name: 'GUILD',
      description: 'Invoice Payment',
      amount: price
    });
return false;
    e.preventDefault();
  });

  // Close Checkout on page navigation
  jQuery(window).on('popstate', function() {
    handler.close();
  });
</script>
<style>
    #flashMessage{
        margin-top:5px !important;
    }
</style>