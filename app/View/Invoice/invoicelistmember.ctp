	<div class="container-fluid projectCreated" id="apply-section">
		<div class="container create-section pad0">
			<div class="col-md-12">
				<div class="p-members-table">
					<h5>My Invoices</h5>
					<table class="table table-striped smartresponsive-table">
						<thead>
							<tr>
								<th>Invoice No</th>
								<th>Date</th>
								<th>Client Name</th>
								<th>Session title</th>
								<th>Price</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
			           		<?php if(!empty($data) && count($data)>0) { 
                                                       foreach($data as $invoice){?>
								<tr>
									<td data-th="Invoice No">
										<?php echo($this->Html->link($invoice['Invoice']['inv_no'],array('controller'=>'invoice','action'=>'invoice_view/'.$invoice['Invoice']['id']),array('class'=>'delete','title'=>'View invoice'))); ?>
									</td>
									<td data-th="Date"><?php echo $this->Time->format('d-m-Y',$invoice['Invoice']['created']); ?></td>
                                    <td data-th="Client Name"><?php echo ucfirst(strtolower($invoice['Mentee']['first_name']))." ".ucfirst(strtolower($invoice['Mentee']['last_name'])); ?></td>
									<td data-th="Session title"><?php echo $invoice['Invoice']['title']; ?></td>
					                <?php if($invoice['Invoice']['total']==0)
					                   		$total = 'FREE';
					                   else
					                   		$total =  '$'.number_format($invoice['Invoice']['total'],2); 
					                ?>
									<td data-th="Price"><?php echo $total; ?></td>
					                <?php if($invoice['Invoice']['inv_create_status']=='1'){?>
					                    <td data-th="Status">PAID</td>
					         	<?php }else{?>
					                    <td data-th="Status"><?php echo($this->Html->link('OPEN',array('controller'=>'invoice','action'=>'order_confirm/'.$invoice['Invoice']['id']),array('class'=>'delete','title'=>'Pay invoice'))); ?></td>
					             	<?php }?>
									
									<td data-th="Print">
										<?php echo($this->Html->link('Print',array('controller'=>'invoice','action'=>'view_pdf/'.$invoice['Invoice']['id']),array('class'=>'delete','title'=>'Print invoice'))); ?>
									</td>
								</tr>
			             	<?php } } else{?>
			                   	<tr><td colspan="7" style="text-align:center;">No record found</td></tr>                    
			              	<?php }?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/script12.js"></script>