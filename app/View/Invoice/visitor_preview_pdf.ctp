<?php 
ob_clean();
App::import('Vendor','tcpdf/tcpdf/tcpdf');
//App::uses('Vendor','tcpdf/xtcpdf');
//echo $html->css(array('style'));

class MYPDF extends TCPDF {





  public function AcceptPageBreak() {

         if (1 == $this->PageNo()) {
                $this->SetMargins($left_margin, $top_margin, $right_margin, true);
         }
        if ($this->num_columns > 1) {
            // multi column mode
            if ($this->current_column < ($this->num_columns - 1)) {
                // go to next column
                $this->selectColumn($this->current_column + 1);
            } elseif ($this->AutoPageBreak) {
                // add a new page
                $this->AddPage();
                // set first column
                $this->selectColumn(0);
            }
            // avoid page breaking from checkPageBreak()
            return false;
        }
        return $this->AutoPageBreak;
    }


}

$tcpdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$textfont = 'ubuntu';
$tcpdf->SetFont($textfont,'',10);

$tcpdf->SetAuthor("Julia Holland");
$tcpdf->SetAutoPageBreak(true);
 
$tcpdf->setPrintHeader(false);
$tcpdf->setPrintFooter(false);

$tcpdf->SetMargins(10, 5, 10, true); // left = 2.5 cm, top = 4 cm, right = 2.5cm
$tcpdf->SetFooterMargin(1.5);  // the bottom margin has to be set with SetFooterMargin
 

$invNo = $data['VisitorInvoice']['inv_no'];
$mentorName = ucfirst(strtolower($data['Mentor']['first_name'])).' '.ucfirst(strtolower($data['Mentor']['last_name'])); 
$menteeName = ucfirst(strtolower($data['Mentee']['name'])); 
$invCreatedDate =  date('M d, Y');
$invDate =  date('M d, Y');
$invTitle = $data['VisitorInvoice']['title'];


$price = number_format((float)($data['VisitorInvoice']['ment_price']), 2, '.', ',');
$discount = ($data['VisitorInvoice']['discounts']);
$subtotal = number_format((float)($data['VisitorInvoice']['ment_price']-$data['VisitorInvoice']['discounts']), 2, '.', ',');
$tax = ($data['VisitorInvoice']['taxes']);
$total = number_format((float)(($data['VisitorInvoice']['ment_price']-$data['VisitorInvoice']['discounts']) + $data['VisitorInvoice']['taxes']), 2, '.', ',');

$inv_to = $data['VisitorInvoice']['inv_to'];
$inv_from = $data['VisitorInvoice']['inv_from'];
$inv_description = nl2br(strip_tags($data['VisitorInvoice']['inv_description']));

//$imgPath = '/img/media/guild-headerlogo.png';
$imgPath = 'https://www.guild.im/img/media/Guild-Headerlogo.svg';
//$imgPath = 'https://test.guild.im/press_material/Guild.ai';
$tcpdf->AddPage();


	// create some HTML content
	$htmlcontent = <<<EOF
<br/><br/><br/>

        <table border="0">

    <tr><td style="border-bottom:1px solid #CCCCCC;height:65px;"><img src="$imgPath" width="180"></td>

<td style="border-bottom:1px solid #CCCCCC;"></td>
<td style="border-bottom:1px solid #CCCCCC;"></td>
         <td style="border-bottom:1px solid #CCCCCC;text-align:right;"><br/><b style="font-size:11inchs;">Manoa Innovation Center</b><br/>2800 Innovation Dr. Suite 100<br/>Honolulu, HI 96822<br/>Toll Free: (866) 511-1898</td>

</tr>

<br/>
<tr><td style="border-bottom:1px solid #CCCCCC;height:45px;"><h1 style="font-size:32inch;font-weight:200%;">INVOICE</h1></td>
<td style="border-bottom:1px solid #CCCCCC;"></td>
<td style="border-bottom:1px solid #CCCCCC;"></td>
<td style="border-bottom:1px solid #CCCCCC;"><br/><br/>Invoice No:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$invNo<br/>
Invoice Date:&nbsp;&nbsp;&nbsp;$invCreatedDate
</td></tr>

<br/>
<tr><td colspan="2" style="border-bottom:1px solid #CCCCCC;">For the consulting services delivered by<br/>$inv_from<br/><br/></td>

<td style="border-bottom:1px solid #CCCCCC;"><br/><br/></td>
<td style="border-bottom:1px solid #CCCCCC; font-family: 'Open Sans', sans-serif;font-size:10inch;">To,<br/><span style="font-size:36px;font-weight:bold;">$menteeName</span>
<br/><br/></td></tr>
<tr>
<td width="435" colspan="2" style="border-bottom:1px solid #CCCCCC;height:20px;"><h2>Description</h2></td>
<td  width="50" style="border-bottom:1px solid #CCCCCC;height:20px;text-align:right;"><h2>Price</h2></td>
<td  width="55" style="border-bottom:1px solid #CCCCCC;height:20px;"></td>
</tr>
<br/>
<tr>
<td width="410" colspan="2" style="border-bottom:1px solid #CCCCCC; font-family: 'Open Sans', sans-serif;font-size:10inch;">$inv_description<br/></td>
<td  width="50" style="border-bottom:1px solid #CCCCCC;height:20px;text-align:right;"><h2>$</h2></td>
<td  width="80" style="border-bottom:1px solid #CCCCCC;height:20px;text-align:right;"><h2>$price</h2></td>
</tr>
<br/>
<tr>
<td width="310" colspan="2" style="border-bottom:1px solid #CCCCCC;"></td>
<td  width="150" style="border-bottom:1px solid #CCCCCC;text-align:right;"><h2>Sub-total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$
<br/>
&nbsp;&nbsp;Discount&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$
<br/>
&nbsp;&nbsp;&nbsp;&nbsp;Tax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$
<br/></h2></td>
<td width="80" style="border-bottom:1px solid #CCCCCC;text-align:right;"><h2>$subtotal<br/>
$discount<br/>
$tax</h2></td>
</tr>

<tr style="border:1px solid #333333;">
<td width="323" colspan="2" style="background-color:#666666;height:20px;margin-top:10px;"></td>
<td width="138" style="background-color:#666666;text-align:right;height:20px;margin-top:10px;"><h2 style="color:#fff;">Total Payable&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$</h2></td>
<td width="85" style="background-color:#666666;border-bottom:1px solid #CCCCCC;text-align:right;"><h2 style="color:#fff;">$total&nbsp;&nbsp;</h2></td>
</tr>
<br/>
<tr>
<td colspan="4" style="text-align:center;color:#666666;"><h2>THANK YOU!</h2></td>
</tr>
<br/>
<tr>
<td colspan="4" style="color:#666666;font-size:20px;">
<p>Please read our <a href="https://test.guild.im/fronts/terms" style="color:#CF3135;" target="_blank">Terms of Service</a>. Contact <a href="mailto:help@guild.im" style="color:#CF3135;">help@guild.im</a> or call <span style="font-weight:bold;font-size:22px;">1 (808) 729-5850</span> for any questions or clarifications.</p>


</td></tr>
</table>
      
        
  
    
EOF;

// output the HTML content
$tcpdf->writeHTML($htmlcontent, true, 0, true, 0);
$filename = "GUILD Invoice_".date("m-d-Y");
$tcpdf->Output($filename.".pdf", 'I'); ?>