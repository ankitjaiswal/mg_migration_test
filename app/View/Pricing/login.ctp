<?php
$email=''; $pass=''; $checked='false';
if(isset($_SESSION['menteeActEmail']) && $_SESSION['menteeActEmail']!=''):
	$email = $_SESSION['menteeActEmail'];
	unset($_SESSION['menteeActEmail']);
endif;
if(isset($_COOKIE['CakeCookie']['email']))
{
	$email = $_COOKIE['CakeCookie']['email'];
	$checked = "true";
}
if(isset($_COOKIE['CakeCookie']['password']))
	$pass = $_COOKIE['CakeCookie']['password']; ?>
<?php echo($this->Html->css(array('regist_popup'))); ?>

<div id="registerationFm" style="width:100%;">
	<h1>Login</h1>
		<?php echo($this->Form->create('User',array('url'=>array('controller'=>'pricing','action'=>'login'),'id'=>'loginForm'))); ?>
		<?php
		if(isset($this->data['User']['plan_id_for_login'])){
			echo($this->Form->hidden('User.plan_id_for_login', array('id'=>'plan_id_for_login', 'value'=>$this->data['User']['plan_id_for_login'])));		
		}?>				
		
		<table style="padding:0px;padding-top:10px;border:0px solid red;width:100%;">
			
			<tr>
				<td colspan="2">
				<div id="errorMessage" class="errormsg"></div>
				<div id="login-account">
				<?php echo($this->Form->input('username',array('class'=>'forminput','id'=>'loginEmail','label'=>false,'div'=>false,'placeholder'=>'Email','value'=>$email)));?>	
				<div id="LoginEmailErr"  class="errormsg"></div>
				</div>			
				</td>
			</tr>
			<tr>
				<td colspan="2">
				<div id="login-account">
                 	<?php echo($this->Form->password('password',array('class'=>'input','label'=>false,'div'=>false,'placeholder'=>'Password', 'value'=>$pass)));?>	
                	<div id="LoginPassErr"  class="errormsg"></div>             
     			</div>
				</td>
			</tr>
			<tr>
	
				<td>
                            <?php echo($this->Form->submit('Login',array('class'=>'btn','onclick'=>'return validateLogin();')));?>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:right;"><?php echo($this->Html->link('Forgot password?','javascript:forgotPopup();',array('class'=>'needAcc','style'=>'font-weight:normal;'))); ?></td>
			</tr>	
			<tr>
				<td colspan="2">
					<?php echo($this->Form->end()); ?>
		
			<div class="line" style="margin-top:4px !important;border-top:1px solid #BEBEBE;clear:both;"></div>
          		
 <?php
			if(isset($this->data['User']['plan_id_for_login'])){?>
				<div class="floatL" style="width:100%;padding-left:20px"><?php echo($this->Html->image('linked-in.png',array('alt'=>'Linkedin',"onclick"=>'linkedin("pricingAuthPopUp","'.$this->data['User']['plan_id_for_login'].'");','style'=>'cursor:pointer;'))); ?></div>
			<?php }?>
			<div class="line1" style="border-bottom:1px solid #BEBEBE;clear:both;"></div>
		
		<div class="floatR" style="padding-top:25px;">Need an account? <?php echo($this->Html->link('Register here','javascript:pricingAuthPopSettingPopup();',array('class'=>'needAcc','style'=>'font-weight:normal;')));?> </div>
				</td>
			</tr>
		</table>
		
</div>
<div style="clear:both;"></div>
<style type="text/css">
.submit{
	padding: 6px 0 !important;
}
</style>

