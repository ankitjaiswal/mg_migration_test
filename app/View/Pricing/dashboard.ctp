 <div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
      <div id="user-account">
        <div class="account-form">
			<div class="heading">
              <h1>Plan Dashboard</h1>
            </div>
            <div>
            	<p><span  style="font-weight: bold;">Plan Type</span>: <?php echo($plan['Plan']['plan_type'])?> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;<a href="<?php echo(SITE_URL.'pricing');?>">Change Plan</a> </p>
        
            </div>
          	<div class="column first">
            	<div class="clear"></div>
            	<div></div>
            	<div class="clear"> </div>
            	<div>
					<span id="passwordError"></span>
                	<div class="clear"> </div>              
            	</div>
				<div class="clear"> </div>
	            <div class="subheading">
	              <h2>Plan Users</h2>
	            </div>
	            <div class="clear"> </div>
	            <div>
	            	<div>
		            	<table>
						<?php $count = 0 ; foreach($plan_users as $pUsers) { ?>
							<tr> <?php if ($pUsers['Plan_user']['activation_key'] == '-1'){?>
                                	<td width="40%"> <?php echo($pUsers['Plan_user']['first_name'])?>  <?php echo($pUsers['Plan_user']['last_name'])?></td>
                                 <?php } else{?>
                                    <td width="40%"> <?php echo($pUsers['Plan_user']['username'])?> </td>
                                 <?php }?>
					             <td width="20%">
								<?php if($pUsers['Plan_user']['user_id'] == $this->Session->read('Auth.User.id')){?> 
									<span> Admin </span>
								<?php }else if (($pUsers['Plan_user']['activation_key'] != '-1') || ($pUsers['Plan_user']['is_approved'] == 0) || ($pUsers['Plan_user']['access_specifier'] == 'draft')){?>
									<a href="#">Invited</a>
								<?php } else{?>
									<a href="<?php SITE_URL?>make_admin/<?php echo($pUsers['Plan_user']['user_id']);?>">Make Admin</a>
								<?php }?>
								</td>
								<td width="20%">
									<?php if($pUsers['Plan_user']['user_id'] != $this->Session->read('Auth.User.id')){?> 
										<a href="<?php SITE_URL?>remove_user/<?php echo($pUsers['Plan_user']['id']);?>">Remove</a>
									<?php }?>
								</td>
							</tr>
						<?php $count++;}?>  
						</table>  
					</div>
					<p>
						<a href="javascript:addUsers('<?php echo($plan['Plan']['plan_type'])?>','<?php echo($count)?>');">Add users to your plan</a> 
					</p>   
	            </div>
	            <div class="clear"> </div>
	         </div>
          	<div class="column last">
	            <div class="clear"> </div>
	            <div class="subheading">
	              <h2>Activities</h2>
	            </div>
	            <div class="clear"> </div>
	            <div class="styled-form">               
            		<p><a href="<?php SITE_URL?>past_invoices">View Past Invoices</a></p>
            		<p><a href="http://www.paypal.com" target="_blank">Update Payment Info on Paypal</a></p>
            		<p><a href="<?php SITE_URL?>round_table">Request Executive Roundtable</a></p>
	            </div>
	            
	            
	            <div class="clear"> </div>
	          </div>
	        </div>
	      </div>
	    </div>
	  </div>
  <script type="text/javascript">
	function addUsers(plan_type, noOfUsers) {
		var msg= '';
		var done = false;
		if(plan_type == 'Pro' && noOfUsers >= 3) {
			msg = "A Pro plan can have only three members including admin"
			done = true;
		}else if(plan_type == 'Enterprise' && noOfUsers >= 7) {
			msg = "An enterprise plan can have only seven members including admin"
			done = true;
		}
		if(done) {
			alert(msg);
			return;
		}else { //Open popup
			plan_invitation_popup();
		}
	}

	function plan_invitation_popup() {
		var path=SITE_URL+'/pricing/plan_invitation/';
		TINY.box.show({url:path,width:595,height:580});
	}
  </script>

  
  