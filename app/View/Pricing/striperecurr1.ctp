<div class="content-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="inner-content-wrapper" class="premium-website">
                    <div id="inner-content-box" class="pagewidth">
                        <div class="inner_wrapper">
                            <div class="premium-title">
                                <h3>Upgrade to master membership</h3>
                            </div>
                            <div class="premium-user-info">
                                <?php 
                                    $urlk1 = ($this->data['User']['url_key']);
                                    $displink=SITE_URL.strtolower($urlk1); 
                                          ?>
                                <div class="premium-user-img">
                                    <?php
                                    $flag = 0; $defaultQuestion=0;
                                    if (isset($this->data['UserImage'][0]['image_name']) && $this->data['UserImage'][0]['image_name'] != '') {
                                        $file = file_exists(WWW_ROOT . 'img' . '/' . MENTORS_IMAGE_PATH . '/' . $this->data['User']['id'] . '/' . $this->data['UserImage'][0]['image_name']);
                                        if ($file == 0) {
                                            $flag++;
                                        }
                                    } else {
                                        $flag++;
                                    }
                                    if ($flag == 0) {
                                        echo($this->Html->link($this->Html->image(MENTORS_IMAGE_PATH . '/' . $this->data['User']['id'] . '/' . $this->data['UserImage'][0]['image_name'],array('alt' => $this->data['UserReference']['first_name'],'class'=>'thumb')),$displink,array('escape'=>false,'style'=>'border: 0px;')));
                                  } else {
                                        echo($this->Html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name'],'class'=>'thumb')));
                                    }
                                  ?>
                                </div>
                                <div class="premium-user-detail">  
                                    <h2><?php echo($this->Html->link(ucwords($this->data['UserReference']['first_name']).' '.($this->data['UserReference']['last_name']),$displink,array('escape'=>false)));?></h2>
                                    <?php
                                        $disp="guild.im/".strtolower($this->data['User']['url_key']);
                                        $completelink=SITE_URL.strtolower($this->data['User']['url_key']);
                                        $displink=SITE_URL.strtolower($this->data['User']['url_key']);
                                        ?>
                                        <a href="<?php echo($displink);?>"><?php echo($disp); ?></a>
                                    <ul>
                                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Stand out among your peers</li>
                                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Extend your reach, and</li>
                                        <li><i class="fa fa-check-square-o" aria-hidden="true"></i>Shorten your sales cycle</li>
                                    </ul>
                                    <p><i class="fa fa-check-square-o" aria-hidden="true"></i>Master membership is <span>$999 per month</span>. You can cancel any time.</p>
                                </div>
                                <div class="premium-user-btm">
                                    <span class="premium-btn"><?php echo($this->Html->link('Cancel',SITE_URL.'members/master',array('class'=>'reset','style'=>""))); ?></span>
                                    <div class="pull-right">
                                        <form action="stripe" method="POST">
                                            <input type="hidden" name="FEE" value="<?php echo($centFee);?>">
			  		<script
			    			src="https://checkout.stripe.com/checkout.js" class="stripe-button"
			    			data-key="pk_live_vg2OVxfTe0Iw5FGSMhitLWE7"
			    			data-amount="99900"
			    			data-name="GUILD"
			    			data-description="GUILD MASTER"
			    			data-image="/img/default_user_image.png">
			 		 </script>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/script6.js"></script>