<?php echo($this->Html->css(array('regist_popup')));
if(isset($plan_id)):
	$id = $plan_id;
else:
	$id = 0;
endif;
?>
<div id="registerationFm" style="height: 512px; width: 512px; background-image: none;">
	<h1>Register as a client</h1>
	<div class="infobar"><!-- VL 27/12--><!-- VL 2/1/2013-->
		<p style="float:left; padding-left:20px;"><?php echo($this->Html->image('linked-in.png',array('alt'=>'Linked in',"onclick"=>'linkedin("pricingAuthPopUp","'.$id.'");','style'=>'cursor:pointer;'))); ?></p>
	</div><!-- VL 27/12-->
	<div class="infobar noborder"><!-- VL 2/1/2013-->
		<span>Or register using email id</span>
	</div>
	
<div class="clear">	
	<?php echo($this->Form->create('User',array('url'=>array('controller'=>'pricing','action'=>'pricing_auth_popup'),'id'=>'passwordSettingPopupForm'))); ?>	
	<div>
		<div class="floatL">
			<?php 
			
			if(isset($this->data['User']['id']) && isset($this->data['UserReference']['id']) && $id!=0){
				echo($this->Form->hidden('User.id',array('id'=>'popupId','div'=>false,'label'=>false)));
				echo($this->Form->hidden('UserReference.id',array('div'=>false,'label'=>false)));
			
			}
			echo($this->Form->input('UserReference.first_name',array('id'=>'fname','class'=>'forminput1','size'=>30,'label'=>false,'div'=>false,'placeholder'=>'First Name')));
			?>		
			<span class="errormsg" id="firstNameErr"></span>
		</div>
		<div class="floatL mr-lt20">
			<?php 
			echo($this->Form->input('UserReference.last_name',array('id'=>'lname','class'=>'forminput1','size'=>30,'label'=>false,'div'=>false,'placeholder'=>'Last Name')));
			?>
		</div>
		<div class="clear"></div>
	</div>
	<div>
		<?php 
		echo($this->Form->input('User.username',array('id'=>'email','class'=>'forminput Big','label'=>false,'div'=>false,'placeholder'=>'Email')));
		?>
		<div id="UserEmailErr" class="errormsg"></div>
	</div>
	<div>
	<?php 
	echo($this->Form->password('User.password2',array('class'=>'forminput Big','label'=>false,'div'=>false,'placeholder'=>'Password','style' => 'font-family:verdana;')));
	?>	
	<div id="PassLengtherr" class="errormsg"></div>			
	</div>
	<div>
	<?php 
	echo($this->Form->input('UserReference.zipcode',array('id'=>'zipcode','maxlength'=>'5','class'=>'forminput Big','label'=>false,'div'=>false,'placeholder'=>'ZIP')));
	?>
	<div id="zipError" class="errormsg"></div>	
	</div>	
	<div class="floatR" style="padding:8px 12px 0 0 !important;">
	<?php echo($this->Form->submit('Register',array('class'=>'btn','onclick'=>'return passwordSettingPopupValidate();'))); ?>
	</div>
       
	<div class="submit_bar" style="margin-right:13px;margin-top:-20px;">
		<div class="submit_bar_left" style="float: right;">
			<p style="font-weight:bold;">Already have an account? <?php echo($this->Html->link('Login here',array('controller'=>'pricing','action'=>'loginfromregister'))); ?></p>
		</div>
	</div>
	<?php echo($this->Form->end()); ?>	
	
</div>
</div>

