<?php
$error = '';
if(isset($_SESSION['pass_error']))
{
	$error  = $_SESSION['pass_error'];
	unset($_SESSION['pass_error']);
}?>
<?php 
echo($this->Html->css(array('jquery/jquery.alerts')));
echo($this->Html->script(array('jquery/jquery.min','jquery/jquery.alerts')));
?>

<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<?php echo($this->Form->create('User', array('url' => array('controller' => 'clients', 'action' => 'account_setting'),'id'=>'accountSettingForm')));?>
	  			<?php echo($this->Form->hidden('UserReference.id',array('value'=>$userData['UserReference']['id']))); ?>

	  			<ul class="left-tab">
					<li data-tab="tab-account" class="active"><a href="javascript:void(0);">Account type</a></li>				    
				    <li data-tab="tab-notification"><a href="javascript:void(0);">Preferences</a></li>				    
				    <li data-tab="tab-setting"><a href="javascript:void(0);">Change Password</a></li>
				</ul>
				<div class="left-tab-content">
					<div class="account-info active" id="tab-account">
						<h3>Account Type</h3>
						<?php if($this->Session->read('Auth.User.id')!='' && $this->Session->read('Auth.User.role_id')=='3' && $this->Session->read('Auth.User.isPlanAdmin') =='1'){ ?>
				          	<p><?php echo $this->Session->read('Auth.User.plan_type');?> Client (Admin)</p>
						<?php } else{?>
							<p><?php echo $this->Session->read('Auth.User.plan_type');?> Client</p>
						<?php } ?>
						<div class="buttons">
							<a class="btn btn-primary-trans" href="<?php echo(SITE_URL);?>invoice/invoicelistclient"><i class="fa fa-file-text-o"></i>My invoices</a>
							<a class="btn btn-primary-trans" href="<?php echo(SITE_URL);?>project/projectlistclient"><i class="fa fa-folder-open-o"></i>My Projects</a>
							<a href="javascript:delAccount();" class="link"><i class="fa fa-trash-o"></i>Delete my account</a>
						</div>
					</div>
					<div class="account-info" id="tab-notification">
						<h3>Email Notifications</h3>
						<ul>
						    <li class="styled-form">
							    <?php  $options = array('W'=>'Weekly');
								  	echo($this->Form->radio('UserReference.email_notification',$options,array('legend'=>false,'div'=>false,'label'=>true,'default'=>$userData['UserReference']['email_notification'])));
								?>
							</li>
							<li class="styled-form">
								<?php  $options = array('N'=>'None');
								  	echo($this->Form->radio('UserReference.email_notification',$options,array('legend'=>false,'div'=>false,'label'=>true,'default'=>$userData['UserReference']['email_notification'])));
								?>
							</li>
						</ul>
						<div class="button-set">
			          		<?php echo($this->Html->link('Cancel','javascript:void(0);',array('class'=>'reset','style'=>""))); ?>
			              	<input name="" type="submit" id="submitAccount" onclick="return validAccountSetting();"  class="btn btn-primary" value="Save">
						</div>
					</div>
					<div class="account-info" id="tab-setting">
						<h3>Change Password</h3>
						<span id="passwordError"></span>		                
						<ul>
						    <li class="full">
		                 		<?php echo($this->Form->password('oldpass', array('maxlength'=>20,'div'=>false, 'label'=>false, "class" => "form-control", "placeholder" => "Old password")));?>
							  	<span id="OldNotMatch" class="errormsg" style="display:inline; float:left; display:none;"><?php echo $error; ?></span>
		                  	</li>
		                  	<li class="full">
							  	<?php echo($this->Form->password('newpass', array('maxlength'=>20,'div'=>false, 'label'=>false, "class" => "form-control", "placeholder" => "New password")));?>
						  	</li>
						  	<li class="full">		                  
		                 		<?php echo($this->Form->password('confpass', array('maxlength'=>20,'div'=>false, 'label'=>false, "class" => "form-control", "placeholder" => "Confirm password"))); ?>
							  	<span id="commonerror" class="errormsg" style="display:inline; float:left; display:none;"></span>
							</li>
						</ul>

						<div class="button-set">
			          		<?php echo($this->Html->link('Cancel','javascript:void(0);',array('class'=>'reset','style'=>""))); ?>
			              	<input name="" type="submit" id="submitAccount" onclick="return validAccountSetting();"  class="btn btn-primary" value="Save">
						</div>
					</div>
				</div>

				<?php echo($this->Form->end()); ?>

				<script type="text/javascript">
					$(document).ready(function(){						
						$('ul.left-tab li').click(function(){
							var tab_id = $(this).attr('data-tab');
							$('ul.left-tab li').removeClass('active');
							$('.account-info').removeClass('active');
							$(this).addClass('active');
							$("#"+tab_id).addClass('active');
						});
					});
				</script>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#OldNotMatch').css('display','none');
		<?php if($error!=''):?>
			jQuery('#UserOldpass').css('border-color','#F00');
			jQuery('#OldNotMatch').css('display','block');
		<?php endif; ?>

		//validation cancel
		jQuery(".reset").click(function(){
			document.location.href= SiteUrl;
		});
	});
</script>

	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/script6.js"></script>
  
  