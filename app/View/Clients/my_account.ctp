<div class="content-wrap">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="left-tab">
					<li data-tab="tab-profile" class="active header-profile" ><a href="javascript:void(0);">Profile</a></li>
                                        <?php if($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==3){?>
					<li data-tab="tab-password" class="header-password"><a href="javascript:void(0);">Change Password</a></li>


					<li data-tab="tab-projects" class="header-projects" ><a href="javascript:void(0);">My Projects</a></li>
                                        <?php if(!empty($invoicedata) && count($invoicedata)>0) {?>
					<li data-tab="tab-invoices" class="header-invoices"><a href="javascript:void(0);">My Invoices</a></li>
                                        <?php }}?>
				</ul>
				<div class="left-tab-content ">
					<div class="account-info active " id="tab-profile">
						<h3>Client Profile</h3>
						<div class="grey-box">
							<?php if($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==3){?>
							<a href="javascript:void(0);" class="edit-btn" data-toggle="modal" data-target="#edit_profile"><i class="fa fa-pencil"></i></a>
							<?php }?>
							<div class="profile-edit">
								<div class="profile-edit-img">
									<?php if(isset($this->data['UserImage'][0]['image_name']) && $this->data['UserImage'][0]['image_name'] !=''){
											$image_path = MENTEES_IMAGE_PATH.DS.$this->data['User']['id'].DS.$this->data['UserImage'][0]['image_name'];
																			echo($this->Html->image($image_path,array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'],'id'=>'profileImage1')));
										
										
									} else {
										echo($this->Html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'],'id'=>'profileImage2')));
									}?>
								</div>
								<div class="profile-edit-info">
									<h4><span id="viewfirstname"><?php echo($this->data['UserReference']['first_name']);?></span> <span id="viewlastname"?><?php echo($this->data['UserReference']['last_name']); ?></span></h4>
									<p>
										<a id="viewprofileheadline"><?php if($this->data['UserReference']['client_companydesignation']!=''){?>
										<?php echo($this->data['UserReference']['client_companydesignation']);?>
										<?php }else{?>
										Your Designation
										<?php }?></a> 
                                                                               <a id="viewlocation"><?php
										if($city != '') {
										echo($city) ;?>, <?php echo($state) ;
										} else {?>
										ZIP Code<?php }?></a>
                                                                               
									</p>
                                                                        <?php if($this->data['UserReference']['phone']!=''){?>
									<p id="phoneshow1"><a href="javascript:void(0);" id="viewphonenumber"><?php echo($this->data['UserReference']['phone']);?></a>(will be kept private)</p>
                                                                        <?php }?>
                                                                        <p id="phoneshow2" style="display:none;"><a href="javascript:void(0);" id="viewphonenumber1"><?php echo($this->data['UserReference']['phone']);?></a>(will be kept private)</p>
                                                                         
                                                                         <?php if($this->data['UserReference']['client_linkedinurl']!=''){
					                                        if(($pos =strpos($this->data['UserReference']['client_linkedinurl'],'http'))!==false)
						                                 {
							                           $link=$this->data['UserReference']['client_linkedinurl'];
						                                  }
						                                     else
						                                  {
							                           $link="http://".$this->data['UserReference']['client_linkedinurl'];
						                                   }
                                                                                ?>
									         <ul class="social-links" id="linkicon">
										<li><a href="<?php echo($link);?>" id="viewlinkedinurl"><i class="fa fa-linkedin"></i></a></li>
                                                                                </ul>
									
                                                                        <?php }?>
                                                                                <ul class="social-links" style="display:none;" id="linkicon1">
                                                                                <li><a href="" id="viewlinkedinurl1"><i class="fa fa-linkedin"></i></a></li>
                                                                                </ul>
                                                                                
								</div>
							</div>
						</div>
						<div class="grey-box">
							<h5>Company Details</h5>
							<?php if($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==3){?>
							<a href="javascript:void(0);" class="edit-btn" data-toggle="modal" data-target="#edit_company"><i class="fa fa-pencil"></i></a>
							<?php }?>
							<div class="white-box">
								<div class="company-detail">
									<div class="cmp-logo">
										<?php if(isset($this->data['UserReference']['client_companylogourl']) && $this->data['UserReference']['client_companylogourl'] !=''){
												$image_path = MENTEES_COMPANYLOGO_PATH.DS.$this->data['User']['id'].DS."uploaded/".$this->data['UserReference']['client_companylogourl'];
																				echo($this->Html->image($image_path,array('alt'=>'Company Logo','style'=>'width:80px;height:80px;','id'=>'companylogo1')));
											
											
										} else {?>
										<img src="https://test.guild.im/yogesh_new1/images/company-logo.png" alt="Company Logo" style="width:80px;height:80px;" id="companylogo2"/>
										<?php }?>
										
									</div>
									<div class="cmp-detail">
										<p id="viewcompanyname">
											<?php if($this->data['UserReference']['client_companyname'] !=''){
											echo($this->data['UserReference']['client_companyname']);
											}else{?>
											Company Name
										<?php }?></p>
										<p>
											<?php if($this->data['UserReference']['client_companydomain'] !=''){
					                                                     if(($pos =strpos($this->data['UserReference']['client_companydomain'],'http'))!==false)
						                                               {
							                                      $clink=$this->data['UserReference']['client_companydomain'];
						                                                }
						                                              else
						                                               {
							                                       $clink="http://".$this->data['UserReference']['client_companydomain'];
						                                                }

                                                                                         ?>
											<a href="<?php echo($clink);?>" target="_blank" id="viewcompanydomain"><?php echo($this->data['UserReference']['client_companydomain']);?></a>
											<?php }else{?>
											<a href="javascript:void(0);" id="viewcompanydomain">Company Website URL</a>
											<?php }?>
										</p>
										<p id="viewcompanydetails">
											<?php if($this->data['UserReference']['client_companydetails'] !=''){
											echo($this->data['UserReference']['client_companydetails']);
											}else{?>
											Company Description
										<?php }?></p>
									</div>
								</div>
							</div>
						</div>
						<div class="grey-box">
							<h5>Company Contact</h5>
							<?php if($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==3){?>
							<a href="javascript:void(0);" class="edit-btn" data-toggle="modal" data-target="#edit_company_contact"><i class="fa fa-pencil"></i></a>
							<?php }?>
							<div class="white-box">
								<div class="cmp-row">
									<label>Contact name</label>
									<div class="cmp-row-info">
										<p id="viewcompanycontactname"><?php echo($this->data['UserReference']['client_companycontactname']); ?></p>
									</div>
								</div>
								<div class="cmp-row">
									<label>Email</label>
									<div class="cmp-row-info">
										<p id="viewcompanycontactaddress"><?php echo($this->data['UserReference']['client_companyaddress']);?></p>
									</div>
								</div>
								<div class="cmp-row">
									<label>Phone</label>
									<div class="cmp-row-info">
										<p id="viewcompanycontactnumber"><?php echo($this->data['UserReference']['client_companycontact']);?></p>
									</div>
								</div>
							</div>
						</div>
                                                 <?php if($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==3){?>
						<p class="ac-info-row">This is a <span>Client</span> account. <?php echo($this->Html->link('Close this account','javascript:delAccount();',array('class'=>'delete'))); ?></p>
                                                 <?php }?>
					</div>
					<?php echo($this->Form->create('User', array('url' => array('controller' => 'clients', 'action' => 'account_setting'),'id'=>'accountSettingForm')));?>
					<?php echo($this->Form->hidden('UserReference.id',array('value'=>$this->data['UserReference']['id']))); ?>
					<div class="account-info" id="tab-password">
						<h3>Change Password</h3>
						<ul>
							<li class="full">
								<div class="fom-group">
									<label>Email</label>
									<input class="form-control" id="hiddenusername" value="<?php echo($this->data['User']['username']);?>" type="hidden" />
									<input class="form-control" name="data[User][username]" id="username" value="<?php echo($this->data['User']['username']);?>" type="text" onchange="return ChangeUsername();"/>
									<span id="Emailalready" class="errormsg" style="display:inline; float:left; display:none;"><?php echo $error; ?></span>
								</div>
							</li>
							<li class="full">
								<div class="fom-group">
									<label>Old Password</label>
                                                                        <input class="form-control" id="hiddenoldpassword" value="<?php echo($this->data['User']['password2']);?>" type="hidden" />
									<?php echo($this->Form->password('oldpass', array('maxlength'=>20,'div'=>false, 'label'=>false, "class" => "input form-control","onchange"=>"return Checkoldpassword();", "placeholder" => "Old password")));?>
									<span id="OldNotMatch" class="errormsg" style="display:inline; float:left; display:none;"><?php echo $error; ?></span>
								</div>
							</li>
							<li class="full">
								<div class="fom-group">
									<label>New password</label>
									<?php echo($this->Form->password('newpass', array('maxlength'=>20,'div'=>false, 'label'=>false, "class" => "input form-control", "placeholder" => "New password")));?>
								</div>
							</li>
							<li class="full">
								<div class="fom-group">
									<label>Confirm password</label>
									<?php echo($this->Form->password('confpass', array('maxlength'=>20,'div'=>false, 'label'=>false, "class" => "input form-control", "placeholder" => "Confirm password"))); ?>
									<span id="commonerror" class="errormsg" style="display:inline; float:left; display:none;"></span>
								</div>
							</li>
						</ul>
						<div class="button-set">
							<a href="javascript:void(0);" class="reset">Cancel</a>
							<input name="" type="submit" onclick="return validAccountSetting();" class="btn btn-primary" value="Save" />
						</div>
					</div>
					<?php echo($this->Form->end()); ?>
					<div class="account-info p-members-table" id="tab-invoices">
						<h3>My Invoices</h3>
						<table class="table table-striped smartresponsive-table">
							<thead>
								<tr>
									<th>Invoice No</th>
									<th>Date</th>
									<th>Project title</th>
									<th>Price</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								
								<?php if(!empty($invoicedata) && count($invoicedata)>0) {
								foreach($invoicedata as $invoice){?>
								<tr>
									<td data-th="Invoice No">
										<?php echo($this->Html->link($invoice['ProjectInvoice']['invoiceno'],array('controller'=>'project','action'=>'invoice_detail/'.$invoice['ProjectInvoice']['invoiceno']),array('class'=>'delete','title'=>'View invoice'))); ?>
									</td>
									<td data-th="Date"><?php echo $this->Time->format('d-m-Y',$invoice['ProjectInvoice']['created']); ?></td>
									<td data-th="Session title"><?php echo $invoice['ProjectInvoice']['title']; ?></td>
									<?php 
											$total =  '$'.number_format($invoice['ProjectInvoice']['total'],2);
									?>
									<td data-th="Price"><?php echo $total; ?></td>
									<?php if($invoice['ProjectInvoice']['invoicestatus']=='PAID'){?>
									<td data-th="Status"><?php echo($invoice['ProjectInvoice']['invoicestatus']);?></td>
									<?php }else{?>
									<td data-th="Status"><?php echo($this->Html->link('Pay Now',array('controller'=>'project','action'=>'invoice_detail/'.$invoice['ProjectInvoice']['invoiceno']),array('class'=>'delete','title'=>'Pay Now'))); ?></td>
									<?php }?>
									<td data-th="Print">
										
                                                                                <?php echo($this->Html->link('Print',array('controller'=>'project','action'=>'invoice_pdf/'.$invoice['ProjectInvoice']['invoiceno']),array('class'=>'delete','title'=>'Print','target'=>'_blank'))); ?>
									</td>
								</tr>
								<?php } } else{?>
								<tr><td colspan="7" style="text-align:center;">No record found</td></tr>
								<?php }?>
							</tbody>
						</table>
					</div>
					<div class="account-info p-members-table" id="tab-projects">
						<h3>My Projects</h3>
						<table class="table table-striped smartresponsive-table">
							<thead>
								<tr>
									<th>Created by</th>
									<th>Date</th>
									<th>Project title</th>
									<th>Project Status</th>
								</tr>
							</thead>
							<tbody>
								
								<?php if(!empty($projectdata) && count($projectdata)>0)
								{
								$i=1;
								foreach($projectdata as $project){
								
									$for_creator = ClassRegistry::init('User');
								$creator = $for_creator->find('first', array('conditions' => array('User.id' => $project['Project']['user_id'])));
								if($creator['User']['role_id'] == 2)
								$displink1=SITE_URL.strtolower($creator['User']['url_key']);
								else
								$displink1=SITE_URL."clients/my_account/".strtolower($creator['User']['url_key']);
								?>
								<tr>
									<td data-th="Created by"><?php  echo($this->Html->link(ucfirst($creator['UserReference']['first_name'])." ".ucfirst($creator['UserReference']['last_name']),$displink1,array('escape'=>false,'target'=>'_blank')));?></td>
									<td data-th="Date"><?php echo date("m-d-Y", strtotime($project['Project']['created']));?></td>
									<td data-th="Project title"><a href="<?php echo (SITE_URL);?>project/index/<?php echo($project['Project']['project_url']);?>"><?php echo $project['Project']['title']; ?></a></td>

									<?php if($project['Project']['status'] == 0) {?>
									<td data-th="Action"><?php echo($this->Html->link('Create project',array('controller'=>'project','action'=>'view/'.$project['Project']['id']),array('class'=>'delete','title'=>'View project')));?></td>

									<?php } else if($project['Project']['status'] == 1) { ?>
									<td data-th="Action"><?php echo($this->Html->link('Schedule call with client Partner',array('controller'=>'project','action'=>'schedule_call/'.$project['Project']['id']),array('class'=>'delete','title'=>'Schedule call with Client Partner')));?></td>

									<?php } else if($project['Project']['status'] == 2) { ?>
									<td data-th="Action">Waiting for Client Partner review</td>

									<?php } else if($project['Project']['status'] == 3) { ?>
									<td data-th="Action"><?php echo($this->Html->link('Approve project',array('controller'=>'project','action'=>'index/'.$project['Project']['project_url']),array('class'=>'delete','title'=>'Approve project'))); ?></td>

									<?php } else if($project['Project']['status'] == 4) { ?>
									<td data-th="Action"><?php echo($this->Html->link('Pay project deposit',array('controller'=>'project','action'=>'stripe_payment/'.$project['Project']['id']),array('class'=>'delete','title'=>'Pay project deposit')));?></td>

									<?php } else if($project['Project']['status'] == 5) { ?>
									<td data-th="Action">Waiting for Proposals</td>

									<?php } else if($project['Project']['status'] == 6) { ?>
									<td data-th="Action"><?php echo($this->Html->link('Review proposal',array('controller'=>'project','action'=>'index/'.$project['Project']['project_url']),array('class'=>'delete','title'=>'Review proposal'))); ?></td>

									<?php } else if($project['Project']['status'] == 7) { ?>
                                                                        <div id="<?php echo($project['Project']['id']); ?>">
									<td data-th="Action"><a style="cursor:pointer;" data-toggle="modal" data-target="#project-start-modal" class="btnxyz" id="accept<?php echo($project['Project']['id']); ?>">Start project</a><input type="hidden"  class="txtabc" value="<?php echo($project['Project']['id']); ?>"></td>
                                                                        
                                                                        </div>
									<?php } else if($project['Project']['status'] == 8) { ?>

									<td data-th="Action"><a style="cursor:pointer;" data-toggle="modal" data-target="#project-close-modal" class="btnxyz" id="accept<?php echo($project['Project']['id']); ?>">Close project</a><input type="hidden"  class="txtabc" value="<?php echo($project['Project']['id']); ?>"></td>
                                                                        

									<?php } else if($project['Project']['status'] == 9) { ?>
									<td data-th="Action">Project completed</td>
                  
                                                              

									<?php }?>
								</tr>
								<?php  $i++;  }?>
								
								
								<?php
								} else{?>
								<tr style="text-align: center;">
									<td colspan="7">No record found</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
				
				<script>
				$(window).bind('hashchange', function() {
				var link = location.hash;
				if(link.includes("projectlist"))
				{ 
				$(".header-profile").removeClass('active');
				$(".header-projects").addClass('active');
				$("#tab-profile").removeClass('active');
				$("#tab-projects").addClass('active');
				}
				});
				$('document').ready(function(){
				var link2 = window.location.href;
				if(link2.includes("#projectlist"))
				{
				$(".header-profile").removeClass('active');
				$(".header-projects").addClass('active');
				$("#tab-profile").removeClass('active');
				$("#tab-projects").addClass('active');
				}
				});
				$('.dropdown-menu li:nth-child(2)').click(function(){
				var link2 = window.location.href;
				if(link2.includes("#projectlist"))
				{
				$(".header-profile").removeClass('active');
				$(".header-projects").addClass('active');
				$("#tab-profile").removeClass('active');
				$("#tab-projects").addClass('active');
				}
				});
				
				</script>
				<!-- Edit Profile Modal -->
				<div class="modal fade member-edit-popup" id="edit_profile" tabindex="-1" role="dialog" aria-labelledby="edit_profile">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
								<h3 class="modal-title" id="myModalLabel">Profile Information</h3>
							</div>
							<form id="frmedit-profile-m">
								<div class="modal-body edit-profile-m">
									<div class="aoe-box">
										<div class="edit-member">
											<form class="" action="" method="">
												<div class="col-md-4 col-xs-12 form-group">
													<div class="profile-edit-img">
														<?php
														if(isset($this->data['UserImage'][0]['image_name']) && $this->data['UserImage'][0]['image_name'] !=''){
																$image_path = MENTEES_IMAGE_PATH.DS.$this->data['User']['id'].DS.$this->data['UserImage'][0]['image_name'];
															
																echo($this->Html->image($image_path,array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'],'id'=>'profileImage')));
																
															echo($this->Form->hidden($this->data['UserImage'][0]['image_name'],array('value'=>$this->data['UserImage'][0]['image_name'],'id'=>'image_name')));
														} else {?>
														
														<img src="/yogesh_new1/images/personal-user.jpg" alt="Default Image" id="profileImage">
														<?php echo($this->Form->hidden('profile.png',array('value'=>'profile.png','id'=>'image_name')));}?>
														<div><?php echo($this->Element('Menteesuser/step2_add_image')); ?></div>
														
													</div>
												</div>
												<div class="col-md-8 col-xs-12">
													<div class="row">
														<div class="form-group col-md-6 col-xs-12">
															<input type="text" class="form-control" placeholder="First Name" id="firstname" value="<?php echo($this->data['UserReference']['first_name']); ?>"/>
														</div>
														<div class="form-group col-md-6 col-xs-12">
															<input type="text" class="form-control" placeholder="Last Name" id="lastname" value="<?php echo($this->data['UserReference']['last_name']); ?>" />
														</div>
														<div class="form-group col-md-6 col-xs-12">
															<input type="text" class="form-control" placeholder="Phone Number" id="phonenumber" value="<?php echo($this->data['UserReference']['phone']); ?>"/>
														</div>
														<div class="form-group col-md-6 col-xs-12">
															<input type="text" class="form-control" placeholder="ZIP Code" id="zipcodenumber" value="<?php echo($this->data['UserReference']['zipcode']); ?>"/>
														</div>
														<div class="form-group col-md-12 col-xs-12">
															<textarea class="form-control" rows="1" placeholder="Your Designation" id="pheadline" style="height:40px;"><?php echo($this->data['UserReference']['client_companydesignation']); ?></textarea>
														</div>
														<div class="form-group col-md-12 col-xs-12">
															<input type="text" class="form-control" placeholder="LinkedIn URL" id="linkedinurl" value="<?php echo($this->data['UserReference']['client_linkedinurl']); ?>"/>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
									<div class="button-set">
										<a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
										<a href="javascript:void(0);" onclick="javascript:saveprofileinfo();" class="btn btn-primary">Save</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				
				<!-- Edit Company Modal -->
				<div class="modal fade member-edit-popup" id="edit_company" tabindex="-1" role="dialog" aria-labelledby="edit_company">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
								<h3 class="modal-title" id="myModalLabel">Company Details</h3>
							</div>
							<form id="frmedit-profile-m">
								<div class="modal-body edit-profile-m">
									<div class="aoe-box">
										<div class="edit-member">
											<form class="" action="" method="">
												<div class="col-md-4 col-xs-12 form-group">
													<div class="profile-edit-img">
														<?php
														if(isset($this->data['UserReference']['client_companylogourl']) && $this->data['UserReference']['client_companylogourl'] !=''){
																$image_path = MENTEES_COMPANYLOGO_PATH.DS.$this->data['User']['id'].DS."uploaded/".$this->data['UserReference']['client_companylogourl'];
															
																echo($this->Html->image($image_path,array('alt'=>$this->data['UserReference']['client_companyname'],'id'=>'companylogo')));
																
															echo($this->Form->hidden($this->data['UserReference']['client_companylogourl'],array('value'=>$this->data['UserReference']['client_companylogourl'],'id'=>'company_logo')));
														} else {?>
														
														<img src="/yogesh_new1/images/company-logo.png" alt="Company Logo" id="companylogo">
														<?php echo($this->Form->hidden('company-logo.png',array('value'=>'company-logo.png','id'=>'company_logo')));}?>
														<div><?php echo($this->Element('Menteesuser/company_logoupload')); ?></div>
													</div>
												</div>
												<div class="col-md-8 col-xs-12">
													<div class="row">
														<div class="form-group col-md-6 col-xs-12">
															<input type="text" class="form-control" placeholder="Company Name" id="companyname" value="<?php echo($this->data['UserReference']['client_companyname']); ?>" />
														</div>
														<div class="form-group col-md-6 col-xs-12">
															<input type="text" class="form-control" placeholder="Company Website" id="companydomain" value="<?php echo($this->data['UserReference']['client_companydomain']); ?>"/>
														</div>
														<div class="form-group col-md-12 col-xs-12">
															<textarea class="form-control" rows="4" placeholder="Brief description" id="companydetails" ><?php echo($this->data['UserReference']['client_companydetails']); ?></textarea>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>
									<div class="button-set">
										<a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
										<a href="javascript:void(0);" onclick="javascript:savecompanydetails();" class="btn btn-primary">Save</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<!-- Edit Company Contact Modal -->

				<div class="modal fade member-edit-popup" id="edit_company_contact" tabindex="-1" role="dialog" aria-labelledby="edit_company_contact">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
								<h3 class="modal-title" id="myModalLabel">Company Contact</h3>
							</div>
							<form id="frmedit-profile-m">
								<div class="modal-body edit-profile-m">
									<div class="aoe-box">
										<div class="edit-member">
											<form class="" action="" method="">
												<div class="form-group col-md-6 col-xs-12">
													<input type="text" class="form-control" placeholder="Contact Name" id="companycontactname" value="<?php echo($this->data['UserReference']['client_companycontactname']); ?>"/>
												</div>
												<div class="form-group col-md-6 col-xs-12">
													<input type="text" class="form-control" placeholder="Phone Number" id="companycontactnumber" value="<?php echo($this->data['UserReference']['client_companycontact']); ?>"/>
												</div>
												<div class="form-group col-md-12 col-xs-12">
													<input type="text" class="form-control" placeholder="Email" id="companycontactaddress" value="<?php echo($this->data['UserReference']['client_companyaddress']); ?>"/>
												</div>
											</form>
										</div>
									</div>
									<div class="button-set">
										<a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
										<a href="javascript:void(0);" onclick="javascript:savecompanycontact();" class="btn btn-primary">Save</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>


<div id="project-start-modal" class="modal fade member-edit-popup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h3 class="modal-title">Start project</h3>
			</div>
			<form id="frmedit-profile-m" class="bv-form">
				<div class="modal-body edit-profile-m">
					<div class="aoe-box">
						<div class="col-md-12">
							<p class="text-left">Do you want to start this project?</p>
						</div>
					</div>
					<div class="button-set">
						<a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
						<a href="javascript:void(0);" class="btn btn-primary continue" data-dismiss="modal" onclick="javascript:project_start(this.id);">Confirm</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="project-close-modal" class="modal fade member-edit-popup">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h3 class="modal-title">Close project</h3>
			</div>
			<form id="frmedit-profile-m" class="bv-form">
				<div class="modal-body edit-profile-m">
					<div class="aoe-box">
						<div class="col-md-12">
							<p class="text-left">Do you want to close this project?</p>
						</div>
					</div>
					<div class="button-set">
						<a href="javascript:void(0);" class="reset" data-dismiss="modal" aria-label="Close">Cancel</a>
						<a href="javascript:void(0);" class="btn btn-primary continue" data-dismiss="modal" onclick="javascript:project_close(this.id); ">Confirm</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

				<input type="hidden" name="editprofilesuccess" value="1" id="editprofilesuccess" />
				<input type="hidden" name="editcompanydetails" value="1" id="editcompanydetails" />
				<input type="hidden" name="editcompanycontact" value="1" id="editcompanycontact" />
				<!-- js link -->
				<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
				<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
				<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>-->
				<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
				<!--<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>-->
				<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
				<script type="text/javascript" src="<?php echo(SITE_URL)?>sanchit/js/clientmyaccountscript.js"></script>

<script type="text/javascript">
$(".btnxyz").click(function(){
	var txtabc = $(this).parent("td").find(".txtabc").val();
	console.log(txtabc);
        $(".continue").attr("id",txtabc);

});


function project_start(confirm_id){

		var ignore_id = 'Ignore'+confirm_id
		jQuery(document).ready(function(){
				jQuery.ajax({
					url:SITE_URL+'project/project_start',
					type:'post',
					dataType:'json',
					data:'confirm_id='+confirm_id,
					success:function(link){
                                                location.reload();
                                                window.location = link;
					}
					});
});
	
}


function project_close(confirm_id){

		var ignore_id = 'Ignore'+confirm_id
		jQuery(document).ready(function(){
				jQuery.ajax({
					url:SITE_URL+'project/project_close',
					type:'post',
					dataType:'json',
					data:'confirm_id='+confirm_id,
					success:function(link){
                                                location.reload();
                                                window.location = link;
					}
					});
});
	
}

</script>				
				
			</div>
		</div>
	</div>
</div>