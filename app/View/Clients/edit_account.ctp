
<script>

	function act(id) {		
		var mutli_education = document.EditAccountform.elements["link[]"];
		mutli_education[id].value="";
		
	} 

</script>

<div class="content-wrap">
	<div class="container">
		<div class="box-wrap edit-profile">

			<?php echo($this->Form->create('User', array('url' => array('controller' => 'clients', 'action' => 'edit_account'),'type'=>'file','id'=>'EditAccountform', "name"=>'EditAccountform')));?>
				<h2>Edit Profile</h2>
				<div class="row">
					<div class="col-sm-3">
						<div id="profileImage" class="profile-img">
							<?php if(isset($this->data['UserImage'][0]['image_name']) && $this->data['UserImage'][0]['image_name'] !=''){ if($this->data['User']['role_id']==Configure::read('App.Role.Mentor')) {
									$image_path = MENTORS_IMAGE_PATH.DS.$this->data['User']['id'].DS.$this->data['UserImage'][0]['image_name'];
							?>
								<?php echo($this->Html->image($image_path,array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'], 'height'=>'200','width'=>'200')));?>
							<?php } else {
								$image_path = MENTEES_IMAGE_PATH.DS.$this->data['User']['id'].DS.$this->data['UserImage'][0]['image_name']; ?>
								<?php echo($this->Html->image($image_path,array('alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'] ,'height'=>'200', 'width'=>'200')));	?>
							<?php }
								echo($this->Form->hidden($this->data['UserImage'][0]['image_name'],array('value'=>$this->data['UserImage'][0]['image_name'],'id'=>'image_name')));
							} else {
								echo($this->Html->image('media/profile.png',array('width'=>'200px','alt'=>$this->data['UserReference']['first_name']." ".$this->data['UserReference']['last_name'],'style' => '')));
								echo($this->Form->hidden('profile.png',array('value'=>'profile.png','id'=>'image_name')));
							}?>
							<div class="imgedit-btn"><?php echo($this->Element('Menteesuser/step2_add_image')); ?></div>
						</div>
						
					</div>
					<div class="col-sm-9">
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
								<?php echo($this->Form->input('UserReference.first_name', array('div'=>false, 'label'=>false, "class" => "form-control", "placeholder" => "First Name")));?>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
								<?php echo($this->Form->input('UserReference.last_name', array('div'=>false, 'label'=>false, "class" => "form-control", "placeholder" => "Last Name")));?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
								<?php echo($this->Form->input('UserReference.headline', array('div'=>false, 'label'=>false, "class" => "form-control", "placeholder" => "Professional headline")));?>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
								<?php echo($this->Form->input('UserReference.zipcode', array('div'=>false, 'label'=>false, "class" => "form-control", "placeholder" => "ZIP (if US based) OR Country")));?>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<ul class="social-links">
									<?php $socialCount=0;
										while(count($this->data['Social'])>$socialCount) {
											if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'http'))!==false) {
												$link=$this->data['Social'][$socialCount]['social_name'];
											} else {
												$link="http://".$this->data['Social'][$socialCount]['social_name'];
											} if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'linkedin'))!==false) {
											 	echo "<li><a href='" .$link . "'><i class='fa fa-linkedin'></i></a></li>";
											} else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'twitter'))!==false) {
											 	echo "<li><a href='" .$link . "'><i class='fa fa-twitter'></i></a></li>";
											} else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'facebook'))!==false) {
											 	echo "<li><a href='" .$link . "'><i class='fa fa-facebook'></i></a></li>";
											} else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'google'))!==false) {
											 	echo "<li><a href='" .$link . "'><i class='fa fa-google-plus'></i></a></li>";
											} else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'blogspot'))!==false) {
												echo "<li><a href='" .$link . "'><i class='fa fa-spotify'></i></a></li>";
											} else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'wordpress'))!==false) {
											 	echo "<li><a href='" .$link . "'><i class='fa fa-wordpress'></i></a></li>";
											} else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'youtube'))!==false) {
											 	echo "<li><a href='" .$link . "'><i class='fa fa-youtube-play'></i></a></li>";
											} else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'yahoo'))!==false) {
											 	echo "<li><a href='" .$link . "'><i class='fa fa-yahoo'></i></a></li>";
											} else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'delicious'))!==false) {
											 	echo "<li><a href='" .$link . "'><i class='fa fa-delicious'></i></a></li>";
											} else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'digg'))!==false) {
												echo "<li><a href='" .$link . "'><i class='fa fa-digg'></i></a></li>";
											} else if(($pos =strpos($this->data['Social'][$socialCount]['social_name'],'orkut'))!==false) {
												echo "<li><a href='" .$link . "'><i class='fa fa-circle-o'></i></a></li>";
											} else {
												echo "<li><a href='" .$link . "'><i class='fa fa-globe'></i></a></li>";
											}
											$socialCount++;
										}
									?>
								</ul>
								<a id='edit_btn'>edit links</a>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<div id="edit_box" style="display:none;">
									<div class="row">
										<div class="col-sm-4">
											<?php $val=''; if(isset($this->data['Social'][0]['social_name'])) { $val=$this->data['Social'][0]['social_name']; } ?>
											<?php echo($this->Form->input('',array('label'=>false,'div'=>false , "name"=>'link[]' , "id"=>'link[]', "class"=>'form-control', "value"=>$val))); ?>
											<?php echo($this->Html->link($this->Html->image('media/minus1.png',array('alt'=>'skype',"class"=>"clear-text")),'javascript:act(0);',array('escape'=>false)));?>
										</div>
										<div class="col-sm-4">
											<?php $val=''; if(isset($this->data['Social'][1]['social_name'])) { $val=$this->data['Social'][1]['social_name']; } ?>
											<?php echo($this->Form->input('',array('label'=>false,'div'=>false , "name"=>'link[]' , "id"=>'link[]', "class"=>'form-control', "value"=>$val))); ?>
											<?php echo($this->Html->link($this->Html->image('media/minus1.png',array('alt'=>'skype',"class"=>"clear-text")),'javascript:act(1);',array('escape'=>false)));?>
										</div>
										<div class="col-sm-4">
											<?php $val=''; if(isset($this->data['Social'][2]['social_name'])) { $val=$this->data['Social'][2]['social_name']; } ?>
											<?php echo($this->Form->input('',array('label'=>false,'div'=>false , "name"=>'link[]' , "id"=>'link[]', "class"=>'form-control', "value"=>$val))); ?>
											<?php echo($this->Html->link($this->Html->image('media/minus1.png',array('alt'=>'skype',"class"=>"clear-text")),'javascript:act(2);',array('escape'=>false)));?>
										</div>
									</div>
									<div class="row">										
										<div class="col-sm-4">
											<?php $val=''; if(isset($this->data['Social'][3]['social_name'])) { $val=$this->data['Social'][3]['social_name']; } ?>
											<?php echo($this->Form->input('',array('label'=>false,'div'=>false , "name"=>'link[]' , "id"=>'link[]', "class"=>'form-control', "value"=>$val))); ?>											
											<?php echo($this->Html->link($this->Html->image('media/minus1.png',array('alt'=>'skype',"class"=>"clear-text")),'javascript:act(3);',array('escape'=>false)));?>
										</div>
										<div class="col-sm-4">
											<?php $val=''; if(isset($this->data['Social'][4]['social_name'])) { $val=$this->data['Social'][4]['social_name']; } ?>
											<?php echo($this->Form->input('',array('label'=>false,'div'=>false , "name"=>'link[]' , "id"=>'link[]', "class"=>'form-control', "value"=>$val))); ?>
											<?php echo($this->Html->link($this->Html->image('media/minus1.png',array('alt'=>'skype',"class"=>"clear-text" )),'javascript:act(4);',array('escape'=>false)));?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div id="area-box" class="area-box">
							<div class="expertise">
								<div class="form-group">
								<h4>Professional summary</h4>
								<p><?php echo($this->Form->textarea('UserReference.background_summary', array('rows'=>6,'div'=>true, 'label'=>false, 'id'=>'text-area' , "class" => "form-control", "placeholder" => "Professional summary")));?></p>
								</div>
							</div>
						</div>
						<?php if($this->Session->read('Auth.User.id') == $this->data['User']['id'] && $this->data['User']['role_id']==3) { ?>
							<div id="area-box" class="area-box">
								<div class="invoce-creation">
									<?php echo ($this->Element('Menteesuser/mentee_dash_edit')); ?>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="button-set submitProfile">
					<a href="javascript:void(0);" class="sp-cancle" style="">Cancel</a>
					<input id="apply" type="submit" value="Save" class="btn btn-primary" />
				</div>
			<?php echo($this->Form->end()); ?>

		</div>
	</div>
</div>

 	<!-- js link -->
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrap.min.js"></script>
	<script src="<?php echo(SITE_URL)?>yogesh_new1/js/bootstrapValidator.min.js" type="text/javascript"></script> 
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>
	<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/script12.js"></script> 


<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery('#EditAccountform').bootstrapValidator({
				    trigger: 'blur',
				    fields: {
				        'data[UserReference][first_name]': {
				        	//enabled : false,
				            validators: {
				                notEmpty: {
				                    message: 'Do not leave blank'
				                },
				                regexp: {
				                    regexp: /^[a-zA-Z ]+$/,
				                    message: 'Only alphabets are allowed'
				                }
				            }
				        },
				        'data[UserReference][last_name]': {
				            validators: {
				                notEmpty: {
				                    message: 'Do not leave blank'
				                },
				                regexp: {
				                    regexp: /^[a-zA-Z ]+$/,
				                    message: 'Only alphabets are allowed'
				                }
				            }
				        },
				        'data[UserReference][headline]': {
				            validators: {
				                notEmpty: {
				                    message: 'Do not leave blank'
				                },
				                regexp: {
				                    regexp: /^[a-zA-Z ]+$/,
				                    message: 'Only alphabets are allowed'
				                }
				            }
				        },
		          		'data[UserReference][zipcode]': {
		                      validators: {
		                          notEmpty: {
		                              message: 'Do not leave blank'
		                          },
		          				stringLength: { 
		          				message: 'ZIP should be maximum 5 digits', 
		          				max: function (value, validator, $field) { 
		          				return 5 - (value.match(/\r/g) || []).length; 
		          				} 
		          				},							
		                          regexp: {
		                              regexp: /^\d+$/,
		                              message: 'ZIP should be 5 digits and numbers only'
		                          }
		                      }
		                  },
                  		'data[UserReference][background_summary]': {
                              validators: {
                                  notEmpty: {
                                      message: 'Your background summary is required'
                                  },
                  				stringLength: { 
                  				message: '1. Use 4-6 short paragraphs</br>2. Open with a 2-3 lines of overview</br>3. Focus on your past consulting work</br>4. Mention related experience, speaking engagements, publications</br>5. End with education, credentials, affiliations</br>(Minimum 500 characters)', 
                  				min: function (value, validator, $field) { 
                  				return 500 - (value.match(/\r/g) || []).length; 
                  				} 
                  				}

                              }
                          }
					
				   },
	       
				})
			var i = 0;	
			jQuery("#edit_btn").click(function(){
				
				if(i == 0){
					$("#edit_box").css('display','block');	
					i=1;
				}else{
					$("#edit_box").css('display','none');	
					i = 0;
				}
				
			});
			});

</script>			

