		<div class="adminrightinner">
			<?php echo($this->Form->create('User', array('url' => array('controller' => 'clients', 'action' => 'changepassword'))));?>     
			<div class="tablewapper2 AdminForm">	
				<table border="0" class="Admin2Table" width="100%">		
					<tr>
						<td colspan="2"><?php echo($this->Form->input('id'));?></td>		
					</tr>
					<tr>
						<td valign="middle" class="Padleft26">New Password<span class="input_required">*</span></td>
						<td><?php echo($this->Form->input('User.password2', array('type'=>'password', 'div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
					</tr>
					<tr>
						<td valign="middle" class="Padleft26">Confirm Password<span class="input_required">*</span></td>
						<td><?php echo($this->Form->input('User.confirm_password', array('type'=>'password', 'div'=>false, 'label'=>false, "class" => "Testbox5")));?></td>
					</tr>
				</table>
			</div>
			<div class="buttonwapper">
				<div><input type="submit" value="Submit" class="submit_button" /></div>
				<div class="cancel_button"><?php echo $this->Html->link("Cancel", "/admin/clients/index/", array("title"=>"", "escape"=>false)); ?></div>
			</div>
			<?php echo($this->Form->end()); ?>	
		</div>