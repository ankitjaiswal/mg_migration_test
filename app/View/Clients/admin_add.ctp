<?php echo($this->Html->script(array('autocomplete/jquery.coolautosuggest')));?>
<?php echo($this->Html->css(array('autocomplete/jquery.coolautosuggest')));?>
<div class="adminrightinner">
	<?php echo($this->Form->create('User', array('url' => array('controller' => 'clients', 'action' => 'admin_add'),'type'=>'file')));?>     
	<div class="tablewapper2 AdminForm">
		<h3 class="legend1">Add Client</h3>
		<?php echo($this->Element('Menteesuser/form'));?>
	</div>
	<div class="buttonwapper">
		<div><input type="submit" value="Submit" class="submit_button" /></div>
		<div class="cancel_button"><?php echo $this->Html->link("Cancel", "/admin/clients/index/", array("title"=>"", "escape"=>false)); ?>
		</div>
	</div>
	<?php echo($this->Form->end()); ?>	
</div>

