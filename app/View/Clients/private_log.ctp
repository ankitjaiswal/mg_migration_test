<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
     <?php echo($this->Form->create('Message', array('url' => array('controller' => 'clients', 'action' => 'private_log'),'id'=>'mentorshipForm' , "name"=>'mentorshipForm')));?>     
        <div class="Profile_page_container">
        <div id="pro-creation">
                <h1>Private Log<span class="mentorname"><strong><?php echo ucfirst(strtolower($mentorship['Mentor']['first_name']))." ".ucfirst(strtolower($mentorship['Mentor']['last_name'])); ?></strong> - <?php echo ucfirst($mentorship['Mentee']['first_name']." ".$mentorship['Mentee']['last_name']); ?></span></h1>
            </div>
		
		 <div class="logsearch">
<div class="formrow">
       <p><b>Shared documents : </b>
           <?php $lnkId = ''; $lnkTxt = '';$disp='';
                 if(isset($linkData['MessageLink']['id']))
                 {
                     $lnkId = $linkData['MessageLink']['id'];
                     if($linkData['MessageLink']['link_text']!="")
                        $lnkTxt = $this->General->addhttp($linkData['MessageLink']['link_text']);
                     $disp = "display:none;";
                 }?>
           <?php echo($this->Form->hidden('MessageLink.id',array('value'=>$lnkId))); ?>
           <?php if($this->Session->read('Auth.User.role_id')=='2')
                    {   
                        echo($this->Form->input('MessageLink.link_text', array('value'=>$lnkTxt,'maxlength'=>'70', 'label'=>false,'div'=>false, "style"=>"$disp;margin-left:22px;" , "placeholder" => "Enter link to Google drive, Dropbox, etc"))); 
                        if(isset($linkData['MessageLink']['id']))
                 		{?>
                        	<label id="addlinklink" style="padding-left:20px; display:none;"><a href="javascript:formSubmit();" title="add link">Add link</a></label>
                        <?php } else {?>
                        	<label id="addlinklink" style="padding-left:20px;"><a href="javascript:formSubmit();" title="add link">Add link</a></label>
                        <?php }?>
                        <?php if($lnkTxt==''): ?>
                        <div><span style="float: left; margin-left: 152px; color:#888888;">e.g. http://www.domain.com</span></div>
                        
                        <?php else: ?>
                        <label id="showtxt" style="padding-left:20px;"><a href="<?php echo $lnkTxt;?>" target="_blank"><?php echo $lnkTxt; ?></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:changetext();" title="edit link">Edit link</a></label>
                        <?php endif; ?>
           <?php }else
                 {
                     if($lnkTxt==""): ?>
                        <label style="padding-left:20px;">No shared documents link here</label>
            <?php    else:
                ?>
                        <label style="padding-left:20px;"><a href="<?php echo $lnkTxt; ?>" target="_blank"><?php echo $lnkTxt; ?></a></label>
            <?php   endif;                     
                 }   
           if($this->Session->read('Auth.User.role_id')=='2'){?>
                    <a title="Back" href="<?php echo SITE_URL."users/my_account";?>" style="float:right;">&lt;&lt;Back to profile</a>                    
           <?php } ?>
            <?php if($this->Session->read('Auth.User.role_id')=='3'){?>
                        <a title="Back" href="<?php echo SITE_URL."clients/my_account";?>" style="float:right;">&lt;&lt;Back to profile</a>
             <?php } ?>                       
                </p>
            
          </div>
</div>
<div id="memberlog">
<div class="content">
 <?php 
 echo($this->Form->hidden('log_id',array('value'=>$mentorship['Mentorship']['id'])));
 echo($this->Form->hidden('mentor_id',array('value'=>$mentorship['Mentorship']['mentor_id'])));
 echo($this->Form->hidden('mentee_id',array('value'=>$mentorship['Mentorship']['mentee_id'])));
 if(count($messages)>0 && !empty($messages))
 { 
	foreach($messages as $message)
	{
	    if($message['Message']['sent_by']=='3')
            $senderName = ucfirst($mentorship['Mentee']['first_name']." ".ucfirst($mentorship['Mentee']['last_name']));
        else 
            $senderName = ucfirst($mentorship['Mentor']['first_name']." ".ucfirst($mentorship['Mentor']['last_name']));
	?>
		<div class="row">
		<div class="date1" style="float:left;width: 28%!important; padding:none;">
        <?php echo $senderName; ?>
        </div>
		<div class="description" style="width:54%  !important;padding-right:5% !important;">
		<?php 	if($message['Message']['sent_by']=='3'):
					echo "<p>".nl2br(strip_tags($message['Message']['message']))."</p>"; ?>
		<?php 	else:
					echo "<strong>".nl2br(strip_tags($message['Message']['message']))."</strong>"; ?>
		<?php 	endif; ?>
		</div>
		<div class="date" style="float:right!important;width: auto!important;text-align:right;padding-right:10px; !important; " >
        <?php 
//             $currDate = date("Y-m-d");
//             $msgDate = date("Y-m-d",strtotime($message['Message']['created']));
//             if($currDate==$msgDate){
//               echo date('h:i A',strtotime($message['Message']['created'])); 
//             }
//             else
//             {
//                echo date('D, m/d/Y',strtotime($message['Message']['created']));  
//             }
            
		$timestamp = strtotime($message['Message']['created']);
		$time = (time() - $timestamp);
		if($time < 60) {
		echo "Less than 1 min ago";
		} else {
		
			$minutes = floor($time/60);
			if($minutes < 60) {
				echo $minutes.' mins ago';
			} else {
			
				$hour = floor($minutes/60);
				if($hour < 24) {
					echo $hour.' hours ago';
				} else {
	
					$days = floor($hour/24);
					if($days < 30) {
						echo $days.' days ago';
					} else {
		
						$months = floor($days/30);
						if($months < 12) {
							echo $months.' months ago';
						} else {
			
								echo 'Over a year ago';
								}
							}
						}
					}
				}
		
        ?></div>
		<div class="clear"></div>
		</div>
<?php }
}
?>	
</div>
<div class="row">
<?php echo($this->Form->textarea('message', array('rows'=>6,'div'=>true, 'label'=>false, 'id'=>'text-area' , "class" => "forminput", "style"=>"width:906px; padding:10px;" , "placeholder" => "Enter your comment here")));?>
	<div role="Apply" id="Apply" style="float:right">
		<input type="submit" class="submitMessage" onclick="return chkUrl(this);" style="margin-top:10px;" value="Send Message" id="apply">
	</div>
<div class="clear"></div>
</div>
</div>
        </div>
      
    </div>
  </div>
  <script type="text/javascript">
      function changetext()
      {
          jQuery('#showtxt').hide();
          jQuery('#MessageLinkLinkText').show();
          jQuery('#addlinklink').show();
      }
  </script>
    <script type="text/javascript">
      function formSubmit()
      {
          jQuery('#mentorshipForm').submit();
      }
  </script>
  
  