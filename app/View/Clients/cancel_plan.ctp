<div id="inner-content-wrapper">
<div id="inner-content-box" class="pagewidth">
  <div class="inner_wrapper">
    <div class="underline">
      <h1 style="font-size: 20px;">Cancel your Business plan</h1>
    </div>
    <div class="">
        <?php 
				$urlk1 = ($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($urlk1); 
              ?>
      <div class="figure-left align-left-botttom ">
        <?php
        $flag = 0; $defaultQuestion=0;
        if (isset($this->data['UserImage'][0]['image_name']) && $this->data['UserImage'][0]['image_name'] != '') {
            $file = file_exists(WWW_ROOT . 'img' . '/' . MENTEES_IMAGE_PATH . '/' . $this->data['User']['id'] . '/' . $this->data['UserImage'][0]['image_name']);
            if ($file == 0) {
                $flag++;
            }
        } else {
            $flag++;
        }
        if ($flag == 0) {
            echo($this->Html->link($this->Html->image(MENTEES_IMAGE_PATH . '/' . $this->data['User']['id'] . '/' . $this->data['UserImage'][0]['image_name'],array('alt' => $this->data['UserReference']['first_name'],'class'=>'thumb')),$displink,array('escape'=>false,'style'=>'border: 0px;')));
      } else {
            echo($this->Html->image('media/profile.png',array('alt'=>$this->data['UserReference']['first_name'],'class'=>'thumb')));
        }
      ?>      
        <div class="figcaption" style="margin-top: 114px; position: static;">        
         <h2><span><?php echo($this->Html->link(ucwords($this->data['UserReference']['first_name']).' '.($this->data['UserReference']['last_name']),$displink,array('escape'=>false)));?></span></h2>
            
             <?php
				$disp="guild.im/".strtolower($this->data['User']['url_key']);
				$completelink=SITE_URL.strtolower($this->data['User']['url_key']);
				$displink=SITE_URL.strtolower($this->data['User']['url_key']);
				?>
				
        </div>
      </div>
      <div>
                <span>All the benefits of a Basic plan, plus</span>
</br></br>
        <ul>

          <li>Get a part-time Project Manager</li>
          <li>Simplify administration with one master contract</li>
          <li>Add 3 complimentary hours for a new project & quarterly roundtables</li>

   
        </ul>
        </br>
	 
        <p>Business plan is <span style="font-weight: bold;">$495 per month</span>. There is a 15-day trial, and you can cancel anytime during or after the trial period.</p>
        <br></br>

        <div style="float: right; margin-bottom:80px;" >
		<span style="float:left;padding:11px 18px;"><?php echo($this->Html->link('Cancel my business plan',SITE_URL.'clients/striperecurrcancel',array('class'=>'reset','style'=>""))); ?></span>
	      <div class="apply-button-search" style="float: right; margin: auto; ">	
              <input class="profilebtn" type="button" style="width:200px !important; margin-top: 1px;" value="Keep my business Plan" onclick="window.location.href='<?php echo SITE_URL."fronts/index" ?>';">
		</div>
		</div>	
    </div>
  </div>
</div>
</div>
