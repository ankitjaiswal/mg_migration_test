<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $title_for_layout; ?> &raquo; <?php echo Configure::read('Site.title');?></title>    
	<?php if(isset($meta_description) || isset($meta_keywords)){?>
	<meta name="description" content="<?php echo($meta_description);?>" />
    <meta name="keywords" content="<?php echo($meta_keywords);?>" /> 
     <?php } else{  //echo $layout->meta();  
     }		
     echo($this->Html->meta('icon')); 
     echo($this->Html->css(array('admin', 'jquery/jquery.alerts')));
     ?>
     <script type="text/javascript">var SiteUrl = "<?php echo SITE_URL; ?>"; </script>
     <?php	  
	 echo $this->Html->script(array(
					//'prototype',            		
					'jquery/jquery.min', 
					'ddaccordion',
					'jquery/jquery.cookie',			
					'jquery/jquery.collapsor',
					'jquery/jquery.alerts',                   
					'admin',
					'checkboxes_operation',
					
        ));
     echo($scripts_for_layout);
    ?>
	
	<script type="text/javascript">
	 jQuery.noConflict();
	 var SITE_URL = "<?php echo SITE_URL; ?>";
	 var SITE_TITLE = "<?php echo Configure::read('Site.title'); ?>";
	</script>
	<!--[if lte IE 6]><style>
        img { behavior: url("<?php echo SITE_URL; ?>css/iepngfix.htc") }
	</style><![endif]-->
</head>
<body>
<div id="wrapper">
	<div id="AdminSidebar">
		<div class="AdminSidebarWrapper">
			<div class="logo">
				<?php  echo($this->Html->link($this->Html->image('media/logo.png', array('width'=>240,'height'=>50,'alt'=>Configure::read('Site.title').' Logo', 'title'=>Configure::read('Site.title').' Logo')), array(), array('escape'=>false)));?>
			</div>
			<div class="UserMenu">
				<ul>
					<li>
						<span>Welcome,<span style="font-weight:bold;"> <?php echo($this->Html->link(ucfirst(strtolower('admin')),array('controller'=>'users', 'action'=>'dashboard')));?></span> &nbsp;your Control Panel</span> 
					</li>				  
				</ul>
			</div>
			<div id="left"><?php echo($this->Element("Admin/navigation")); ?></div>
		</div>
	</div>
	<div id="right">
		<div class="AdminAction">
			
			<?php echo $this->Html->link("logout", array('controller'=>'users', 'action'=>'logout'), array("class"=>"GreenBtn")); ?>
		
		</div>
		<H2><?php  echo  $title_for_layout;?></H2>
		<?php
			$this->Layout->sessionFlash();
			echo($content_for_layout);
		?>
	</div>
<?php //echo($this->Element('sql_dump'));?>
</div>
</body>
</html>