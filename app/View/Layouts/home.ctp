<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="GOOGLEBOT" content="index,archive,follow">
        <meta name="google-site-verification" content="y3TwsghV-3TWMTz7Kni0ezXPt2EQoAu7s80Bso4pVxs" />

        <title>
            <?php echo $title_for_layout; ?> | <?php echo Configure::read('Site.title'); ?>
        </title>
        <link rel="icon" type="image/x-icon" href="<?php echo $this->webroot; ?>favicon.ico" />
        <link rel="shortcut icon" href="<?php echo $this->webroot; ?>favicon.ico" type="image/x-icon" />

        <?php
        if (isset($meta_description) || isset($meta_keywords)) {
            if (isset($meta_description) && !empty($meta_description)) {
                ?>
                <meta name="description" content="<?php echo($meta_description); ?>" />
                <?php
            }
            if (isset($meta_keywords) && !empty($meta_keywords)) {
                ?>
                <meta name="keywords" content="<?php echo($meta_keywords); ?>" /> 
                <?php
            }
        } else {
            echo $layout->meta();
        }
        ?>  
        <?php
        echo($this->Html->css(array('front1')));
        echo($this->Html->meta('icon'));
        echo($this->Html->css(array('jquery/jquery.alerts')));
        ?>
        <?php
        echo($javascript->link(array(
                    'jquery/jquery',
                    'jquery/jquery.alerts',
                    'ddaccordion',
                    'front'
                )));
        ?>
        <?php echo($this->Html->script(array('fancybox/jquery.fancybox-1.3.4.pack', 'fancybox/jquery.mousewheel-3.0.4.pack'))); ?>
        <?php echo($this->Html->css(array('jquery.fancybox-1.3.4')), false); ?>
        <script type="text/javascript">
            jQuery.noConflict();
            var SITE_URL = "<?php echo SITE_URL; ?>";	
            var SiteUrl = "<?php echo SITE_URL; ?>";
        </script>
        <!--[if lte IE 6]><style>
                img { behavior: url("<?php echo SITE_URL; ?>/css/iepngfix.htc") }
        </style><![endif]-->
        <?php echo($scripts_for_layout); ?>
        <?php //echo $this->Html->css("/popup/css/default_theme"); ?>
    </head>
    <body>
        <div id="Wraper">
            <div id="Header">
                <?php echo($this->Element('Front/header')); ?>
            </div>
            <div id="Middle">
                <div class="MiddleConts">
                    <div class="MidTop">
                        <div class="MidTopImgs"><?php echo($this->Html->image('images/mid_pics.png', array('alt' => 'Img'))); ?></div>
                        <div class="MidTopSrh">
                            <?php echo($this->Element('Front/form_normal_search')); ?>				
                        </div>
                    </div>
                    <div class="MidBtm">			
                        <div class="MidTopBg">
                            <h2 class="InnerHd"><?php echo($this->Element('inner_head_title')); ?></h2>
                            <div class="InnerMidCnt">
                                <?php echo($this->Element('left')); ?>
                                <?php echo($content_for_layout); ?>
                            </div>
                        </div>
                        <div class="MidBtmBg"></div>			
                    </div>
                    <div class="Clear"></div>			
                </div>
            </div>
            <div id="Footer">
                <?php echo($this->Element('Front/footer')); ?>
            </div>
        </div>
        <?php //echo($this->Element('sql_dump')); ?>
    </body>
</html>