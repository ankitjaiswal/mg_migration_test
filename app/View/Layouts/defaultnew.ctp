<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" lang="en-US">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="GOOGLEBOT" content="index,archive,follow">
      <meta name="msvalidate.01" content="67494CAA039573E002087689C5077B6C" />
      <meta name="google-site-verification" content="y3TwsghV-3TWMTz7Kni0ezXPt2EQoAu7s80Bso4pVxs" />
      <meta http-equiv="Cache-Control" content="max-age=200" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <meta name="format-detection" content="telephone=no">
      <link rel="icon" type="image/x-icon" href="<?php echo $this->webroot; ?>favicon.ico?v=3"  />
      <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->webroot; ?>favicon.ico?v=3"  />
 
     <?php echo($this->Html->script(array('jquery/jquery')));?>


        <?php 
		
	      $url = $this->request->url;


               if(strpos($url, ".")!== false && strpos($url, "fronts/consultation_request")!==false){//meta tags for consultant consultation request page

               $city = ClassRegistry::init('City');
               $dataZip=$city->find('first',array('conditions'=>array('City.zip_code'=>trim($this->data['UserReference']['zipcode']))));

               //Get Member Profile Topic

               $topic = ClassRegistry::init('Topic');  
		       $result = $topic->find('all');
		       $categories = array();
		       foreach($result as $value){
			      $categories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
		       }

               $topicarray = '';
                if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] != -1 && $this->data['MemberIndustryCategory']['topic4'] != -1 && $this->data['MemberIndustryCategory']['topic5'] != -1){
                $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']].",".$categories[$this->data['MemberIndustryCategory']['topic3']].",".$categories[$this->data['MemberIndustryCategory']['topic4']].",".$categories[$this->data['MemberIndustryCategory']['topic5']];
                }
                else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] != -1 && $this->data['MemberIndustryCategory']['topic4'] != -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
                $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']].",".$categories[$this->data['MemberIndustryCategory']['topic3']].",".$categories[$this->data['MemberIndustryCategory']['topic4']];
                }
                else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] != -1 && $this->data['MemberIndustryCategory']['topic4'] == -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
                $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']].",".$categories[$this->data['MemberIndustryCategory']['topic3']];
                }
                else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] == -1 && $this->data['MemberIndustryCategory']['topic4'] == -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
                $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']];
                }
                else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] == -1 && $this->data['MemberIndustryCategory']['topic3'] == -1 && $this->data['MemberIndustryCategory']['topic4'] == -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
                $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']];
                }
				
                $menu2 = ClassRegistry::init('UserImage');  //for class load in view
                $Image = $menu2->find('first', array('conditions' => array('UserImage.user_id' => $this->data['User']['id'])));
                $image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$Answer['QnaAnswer']['member_id'].DS.$Image['UserImage']['image_name'];				
             ?>
                 <meta property="og:image" content="<?php echo($image_path);?>" />		
		 <meta property="og:title" content="<?php echo(ucfirst($this->data['UserReference']['first_name']));?> <?php echo(ucfirst($this->data['UserReference']['last_name'])); ?> - <?php echo(ucfirst($this->data['UserReference']['linkedin_headline'])); ?> - guild.im" />
                 <meta property="og:jobtitle" content="Consultant at GUILD">
                 <meta name="keywords" content="Expert in <?php echo($topicarray); ?>" />
	         <meta name="description"  property="og:description" content="Send a free consultation request to <?php echo(ucfirst($this->data['UserReference']['first_name']));?> <?php echo(ucfirst($this->data['UserReference']['last_name'])); ?> (<?php echo($dataZip['City']['city_name']) ;?>, <?php echo($dataZip['City']['state']) ;?>)." />

        <?php }elseif(strpos($url, ".")!== false){//meta tags for consultant profile
		

               $city = ClassRegistry::init('City');
              
               $dataZip=$city->find('first',array('conditions'=>array('City.zip_code'=>trim($this->data['UserReference']['zipcode']))));


		       //Get Member Profile Topic

                        $topic = ClassRegistry::init('Topic');  
		        $result = $topic->find('all');
		        $categories = array();
		        foreach($result as $value){
			     $categories[$value['Topic']['id']] = $value['Topic']['autocomplete_text'];
		        }

                $topicarray = '';
                if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] != -1 && $this->data['MemberIndustryCategory']['topic4'] != -1 && $this->data['MemberIndustryCategory']['topic5'] != -1){
                $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']].",".$categories[$this->data['MemberIndustryCategory']['topic3']].",".$categories[$this->data['MemberIndustryCategory']['topic4']].",".$categories[$this->data['MemberIndustryCategory']['topic5']];
                }
                else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] != -1 && $this->data['MemberIndustryCategory']['topic4'] != -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
                $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']].",".$categories[$this->data['MemberIndustryCategory']['topic3']].",".$categories[$this->data['MemberIndustryCategory']['topic4']];
                }
                else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] != -1 && $this->data['MemberIndustryCategory']['topic4'] == -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
                $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']].",".$categories[$this->data['MemberIndustryCategory']['topic3']];
                }
                else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] != -1 && $this->data['MemberIndustryCategory']['topic3'] == -1 && $this->data['MemberIndustryCategory']['topic4'] == -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
                $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']].",".$categories[$this->data['MemberIndustryCategory']['topic2']];
                }
                else if($this->data['MemberIndustryCategory']['topic1'] != -1 && $this->data['MemberIndustryCategory']['topic2'] == -1 && $this->data['MemberIndustryCategory']['topic3'] == -1 && $this->data['MemberIndustryCategory']['topic4'] == -1 && $this->data['MemberIndustryCategory']['topic5'] == -1){
                $topicarray = $categories[$this->data['MemberIndustryCategory']['topic1']];
                }
				
                $menu2 = ClassRegistry::init('UserImage');  //for class load in view
                $Image = $menu2->find('first', array('conditions' => array('UserImage.user_id' => $this->data['User']['id'])));
                $image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$Answer['QnaAnswer']['member_id'].DS.$Image['UserImage']['image_name'];				
              ?>
                <meta property="og:image" content="<?php echo($image_path);?>" />		
		<meta property="og:title" content="<?php echo(ucfirst($this->data['UserReference']['first_name']));?> <?php echo(ucfirst($this->data['UserReference']['last_name'])); ?> - <?php echo(ucfirst($this->data['UserReference']['linkedin_headline'])); ?> - guild.im" />
                <meta property="og:jobtitle" content="Consultant at GUILD">
                <meta name="keywords" content="Expert in <?php echo($topicarray) ?>" />
	        <meta name="description"  property="og:description" content="<?php echo(ucfirst($this->data['UserReference']['first_name']));?> <?php echo(ucfirst($this->data['UserReference']['last_name'])); ?> is a leading expert in <?php echo($topicarray); ?> (<?php echo($dataZip['City']['city_name']) ;?>, <?php echo($dataZip['City']['state']) ;?>). Click to view <?php echo(ucfirst($this->data['UserReference']['first_name']));?>'s bio or request a free consultation." />
              

        <?php }else if(strpos($url, "advantage")!== false){?>

		<meta property="og:title" content="GUILD Advantage | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD advantage, GUILD features, GUILD membership, executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants,management consultant" />
	        <meta name="description"  property="og:description" content="Learn about GUILD's features for consultants-the free GUILD profile, targeted advertising to generate leads, marketing and business tools to offer to your clients." />

        <?php }else if(strpos($url, "member_register")!== false){?>

		<meta property="og:title" content="Member Register | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Advantage,Member Register, GUILD features, GUILD membership, executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants,management consultant" />
	        <meta name="description"  property="og:description" content="Learn about GUILD's features for consultants-the free GUILD profile, targeted advertising to generate leads, marketing and business tools to offer to your clients." />

        <?php }else if(strpos($url, "websites")!== false){?>

		<meta property="og:title" content="GUILD Websites | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Websites, featured websites, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD develops Mobile-friendly websites for business consultants to connect them with their audience." />


        <?php }else if(strpos($url, "master")!== false){?>

		<meta property="og:title" content="GUILD Master | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Masters, featured works, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD strategizes in marketing best-practices for experienced business consultants." />


        <?php }else if(strpos($url, "members/eligibility")!== false){?>

		<meta property="og:title" content="Members Eligibility | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Members Eligibility, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD Membership is open to seasoned business professionals who meet the eligibility criteria." />

        <?php }else if(strpos($url, "members/apply")!== false){?>

		<meta property="og:title" content="Apply to be a member | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Membership Apply, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD Membership helps great consultants get more business." />

        <?php }else if(strpos($url, "members/flevy")!== false){?>

		<meta property="og:title" content="Business Documents by Flevy | Business Consulting | GUILD" />
                <meta name="keywords" content="Business Documents by Flevy at GUILD, Business frameworks & methodologies, presentation templates, financial models" />
	        <meta name="description"  property="og:description" content="GUILD offers Flevy's Business frameworks & methodologies, presentation templates, financial models, and more." />

        <?php }else if(strpos($url, "members/ninelenses")!== false){?>

		<meta property="og:title" content="Smarter Way to Engage Clients | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD's Smarter Way to Engage Clients, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD provides consultants a smarter way to engage clients." />

        <?php }else if(strpos($url, "fronts/privacy")!== false){?>

		<meta property="og:title" content="Privacy Policy | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Privacy Policy, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD offers business leaders proven expertise to solve hard business problems, as a cost-effective alternative to internal staffing or traditional consulting." />

        <?php }else if(strpos($url, "fronts/terms")!== false){?>

		<meta property="og:title" content="Terms of Service | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Terms of Service, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD offers business leaders proven expertise to solve hard business problems, as a cost-effective alternative to internal staffing or traditional consulting." />

        <?php }else if(strpos($url, "fronts/guarantee")!== false){?>

		<meta property="og:title" content="Client Satisfaction Guarantee | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Client Satisfaction Guarantee, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD offers business leaders proven expertise to solve hard business problems, as a cost-effective alternative to internal staffing or traditional consulting." />

        <?php }else if(strpos($url, "project/create")!== false){?>

		<meta property="og:title" content="Post A Project | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Post A Project, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="Specify your requirement using GUILD 1-page form." />

	 <?php } elseif(strpos($url,'question') !== false  && strpos($url,'/a') !== false){//meta tags for page to give answer of a question 
       
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $str = $path[3];
            $answerid = substr($str, 1);

            $last = $path[2]; // get the value of the last element 
            $last = str_replace("--","-",$last);
            $last = str_replace("-"," ",$last);
            $keys = parse_url($last);
            $text = explode(" ", $keys['path']);
            $id = end($text);
            $menu = ClassRegistry::init('QnaQuestion');  //for class load in view
            $Question = $menu->find('first', array('conditions' => array('QnaQuestion.id' => $id))); 
            $combined = $Question['QnaQuestion']['question_context']; 
            $qtitle  = $Question['QnaQuestion']['question_text'];

	        if(false !== stripos($qtitle,"'")){
        	$qtitle = str_replace("'"," ",$qtitle);
            }
	        if(false !== stripos($qtitle,'"')){
        	$qtitle = str_replace('"'," ",$qtitle);
            }
	        if(false !== stripos($qtitle,'&')){
        	$qtitle = str_replace('&',"and",$qtitle);
            }


	        if(false !== stripos($combined,"'")){
        	$combined = str_replace("'"," ",$combined);
            }
	        if(false !== stripos($combined,'"')){
        	$combined = str_replace('"'," ",$combined);
            }
	        if(false !== stripos($combined,'&')){
        	$combined = str_replace('&',"and",$combined);
            } 
            $length = strlen($combined);
            if($length >= 160)
            $result = preg_replace('/\s+?(\S+)?$/', '', substr($combined, 0, 160));         
            else
            $result = $combined;

            $menu1 = ClassRegistry::init('QnaAnswer');  //for class load in view
            $Answer = $menu1->find('first', array('conditions' => array('QnaAnswer.id' => $answerid)));
            $menu2 = ClassRegistry::init('UserImage');  //for class load in view
            $Image = $menu2->find('first', array('conditions' => array('UserImage.user_id' => $Answer['QnaAnswer']['member_id'])));
            $image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$Answer['QnaAnswer']['member_id'].DS.$Image['UserImage']['image_name'];		
	

        ?>  
             <meta property="og:image" content="<?php echo($image_path);?>"/>		
             <meta property="og:title" content="<?php echo($qtitle);?>" />
             <meta name="keywords" content="top question & answers, executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, GUILD, management consultant" />
	     <meta name="description"  property="og:description" content="<?php echo($result);?>..." />

	    <?php } elseif(strpos($url,'question') !== false){//meta tags for each question & answer page
       
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $last = $path[2]; // get the value of the last element 
            $last = str_replace("--","-",$last);
            $last = str_replace("-"," ",$last);
            $keys = parse_url($last);
            $text = explode(" ", $keys['path']);
            $id = end($text);
            $menu = ClassRegistry::init('QnaQuestion');  //for class load in view
            $Question = $menu->find('first', array('conditions' => array('QnaQuestion.id' => $id))); 
            $combined = $Question['QnaQuestion']['question_context']; 
	        if(false !== stripos($combined,"'")){
        	$combined = str_replace("'"," ",$combined);
            }
	        if(false !== stripos($combined,'"')){
        	$combined = str_replace('"'," ",$combined);
            }
	        if(false !== stripos($combined,'&')){
        	$combined = str_replace('&',"and",$combined);
            }  
            $length = strlen($combined);
            if($length >= 160)
            $result = preg_replace('/\s+?(\S+)?$/', '', substr($combined, 0, 160));         
            else
            $result = $combined;

            $menu1 = ClassRegistry::init('QnaAnswer');  //for class load in view
            $Answer = $menu1->find('first', array('conditions' => array('QnaAnswer.question_id' => $id)));
			
            $menu2 = ClassRegistry::init('UserImage');  //for class load in view
            $Image = $menu2->find('first', array('conditions' => array('UserImage.user_id' => $Answer['QnaAnswer']['member_id'])));
            $image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$Answer['QnaAnswer']['member_id'].DS.$Image['UserImage']['image_name'];

        ?>    
              <meta property="og:image" content="<?php echo($image_path);?>" />		
              <meta property="og:title" content="<?php echo($Question['QnaQuestion']['question_text']);?>" />
              <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, GUILD, management consultant" />
	      <meta name="description"  property="og:description" content="<?php echo($result);?>..." />

           <?php } elseif(strpos($url,'/proposal/') !== false){//meta tags for proposal page
             
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $last = $path[2]; // get the value of the last element 
            $last = str_replace("--","-",$last);
            $last = str_replace("-"," ",$last);
            $keys = parse_url($last);
            $text = explode(" ", $keys['path']);
            $id = end($text);
            $menu = ClassRegistry::init('Proposal');  //for class load in view 
            $proposal = $menu->find('first', array('conditions' => array('Proposal.id' => $id))); 
            $combined = strip_tags($proposal['Proposal']['proposal_details']);
	        if(false !== stripos($combined,"'")){
        	$combined = str_replace("'"," ",$combined);
            }
	        if(false !== stripos($combined,'"')){
        	$combined = str_replace('"'," ",$combined);
            }
	        if(false !== stripos($combined,'&')){
        	$combined = str_replace('&',"and",$combined);
            } 

            $length = strlen($combined);
            if($length >= 160)
            $result = preg_replace('/\s+?(\S+)?$/', '', substr($combined, 0, 160));         
            else
            $result = $combined;
			
            $menu2 = ClassRegistry::init('UserImage');  //for class load in view
            $Image = $menu2->find('first', array('conditions' => array('UserImage.user_id' => $proposal['Proposal']['user_id'])));
            $image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$Answer['QnaAnswer']['member_id'].DS.$Image['UserImage']['image_name'];			
        ?>
           
               <meta property="og:image" content="<?php echo($image_path);?>" />
	       <meta property="og:title" content="PROJECT PROPOSAL: <?php echo($proposal['Proposal']['proposal_title']);?>" />
               <meta name="keywords" content="project proposal, executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, GUILD, management consultant" />
	       <meta name="description"  property="og:description" content="<?php echo($result);?>..." />	  

	  
        <?php } elseif(strpos($url,'/casestudy/') !== false){//meta tags for casestudy page
             
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $last = $path[2]; // get the value of the last element 
            $last = str_replace("--","-",$last);
            $last = str_replace("-"," ",$last);
            $keys = parse_url($last);
            $text = explode(" ", $keys['path']);
            $id = end($text);
            $menu = ClassRegistry::init('CaseStudy');  //for class load in view 
            $casestudy = $menu->find('first', array('conditions' => array('CaseStudy.id' => $id))); 
            $combined = strip_tags($casestudy['CaseStudy']['situation']);
	        if(false !== stripos($combined,"'")){
        	$combined = str_replace("'"," ",$combined);
            }
	        if(false !== stripos($combined,'"')){
        	$combined = str_replace('"'," ",$combined);
            }
	        if(false !== stripos($combined,'&')){
        	$combined = str_replace('&',"and",$combined);
            } 

            $length = strlen($combined);
            if($length >= 160)
            $result = preg_replace('/\s+?(\S+)?$/', '', substr($combined, 0, 160));         
            else
            $result = $combined;
			
            $menu2 = ClassRegistry::init('UserImage');  //for class load in view
            $Image = $menu2->find('first', array('conditions' => array('UserImage.user_id' => $casestudy['CaseStudy']['user_id'])));
            $image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$Answer['QnaAnswer']['member_id'].DS.$Image['UserImage']['image_name'];				
        ?>
              <meta property="og:image" content="<?php echo($image_path);?>" />           
              <meta property="og:title" content="Case Study: <?php echo($casestudy['CaseStudy']['title']);?>" />
              <meta name="keywords" content="casestudy, executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, GUILD, management consultant" />
	      <meta name="description"  property="og:description" content="<?php echo($result);?>..." />


        <?php }else{?>

               <meta property="og:title" content="Business Consulting | GUILD" />
               <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, GUILD, management consultant" />
	       <meta name="description"  property="og:description" content="GUILD offers business leaders proven expertise to solve hard business problems, as a cost-effective alternative to internal staffing or traditional consulting." />

        <?php }?>
	


        <meta property="og:image" content="<?php echo(SITE_URL);?>img/media/guild-finallogo.png" />	
	<meta property="og:url" content="<?php echo(SITE_URL);?><?php echo($this->request->url);?>" />
        <meta property="og:site_name" content="GUILD"/>
	<meta property="og:type" content="Website" />   
        <meta name="language" content="English"/>

        <meta name="DC.Title" content="Business Consulting | GUILD" />
        <meta name="DC.Creator" content="GUILD" />
        <meta name="DC.Subject" content="Business Consulting" />
        <meta name="DC.Description" content="GUILD offers business leaders proven expertise to solve hard business problems, as a cost-effective alternative to internal staffing or traditional consulting." />
        <meta name="DC.Publisher" content="GUILD" />
        <meta name="DC.Date" content="2015-04-22" />
        <meta name="DC.Type" content="Text" />
        <meta name="DC.Language" content="en-US" />
        <meta name="DC.Rights" content="GUILD" />  

     
	     <title><?php if(strpos($this->request->url, "qna/question")!== false && $title_for_layout != '')
                        { echo $title_for_layout; ?> | GUILD
                  <?php }else if ($title_for_layout == 'GUILD: Expertise On Demand'){
            			echo $title_for_layout; 
                  }else if ($title_for_layout != ''){
            			echo $title_for_layout; ?> | <?php echo Configure::read('Site.title'); 
            	    }else {
				echo Configure::read('Site.title');}?></title>
		
        <link rel="stylesheet" type="text/css" href="<?php echo(SITE_URL)?>yogesh_new1/css/fonts.css">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo(SITE_URL)?>yogesh_new1/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo(SITE_URL)?>yogesh_new1/css/slick.css">
        <link rel="stylesheet" type="text/css" href="<?php echo(SITE_URL)?>yogesh_new1/css/jquery-ui.css">
        <link type="text/css" href="<?php echo(SITE_URL)?>yogesh_new1/css/style.css?v=6.195" rel="stylesheet">
        



        

	  

        <script type="text/javascript">
            jQuery.noConflict();
            var SiteUrl = "<?php echo SITE_URL; ?>";
            var SITE_URL = "<?php echo SITE_URL; ?>";	
			var loggedUserId = '';
			var memberId = '';
			var mentee_url = '';
        </script>



<script type="text/javascript">
//IPInfoDB javascript JSON query example
//Tested with FF 3.5, Opera 10, Chome 5 and IE 8
//Geolocation data is stored as serialized JSON in a cookie
//Bug reports : http://forum.ipinfodb.com/viewforum.php?f=7
function geolocate(timezone, cityPrecision, objectVar) {
 
  var api = (cityPrecision) ? "ip-city" : "ip-country";
  var domain = 'api.ipinfodb.com';
  var url = "https://" + domain + "/v3/" + api + "/?key=75c52efd0ce60f4a2da885d0696500e98f6db4c72ae8eee3ef05277d661d715a&format=json" + "&callback=" + objectVar + ".setGeoCookie";
  var geodata;
  var callbackFunc;
  var JSON = JSON || {};
 
  // implement JSON.stringify serialization
  JSON.stringify = JSON.stringify || function (obj) {
    var t = typeof (obj);
    if (t != "object" || obj === null) {
      // simple data type
      if (t == "string") obj = '"'+obj+'"';
        return String(obj);
    } else {
    // recurse array or object
      var n, v, json = [], arr = (obj && obj.constructor == Array);
      for (n in obj) {
        v = obj[n]; t = typeof(v);
        if (t == "string") v = '"'+v+'"';
        else if (t == "object" && v !== null) v = JSON.stringify(v);
        json.push((arr ? "" : '"' + n + '":') + String(v));
      }
      return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
  };
 
  // implement JSON.parse de-serialization
  JSON.parse = JSON.parse || function (str) {
    if (str === "") str = '""';
      eval("var p=" + str + ";");
      return p;
  };
 
  //Check if cookie already exist. If not, query IPInfoDB
  this.checkcookie = function(callback) {
    geolocationCookie = getCookie('geolocation');
    callbackFunc = callback;
    if (!geolocationCookie) {
      getGeolocation();
    } else {
      geodata = JSON.parse(geolocationCookie);
      callbackFunc();
    }
  }
 
  //API callback function that sets the cookie with the serialized JSON answer
  this.setGeoCookie = function(answer) {
    if (answer['statusCode'] == 'OK') {
      JSONString = JSON.stringify(answer);
      setCookie('geolocation', JSONString, 365);
      geodata = answer;
      callbackFunc();
    }
  }
 
  //Return a geolocation field
  this.getField = function(field) {
    try {
      return geodata[field];
    } catch(err) {}
  }
 
  //Request to IPInfoDB
  function getGeolocation() {
    try {
      script = document.createElement('script');
      script.src = url;
      document.body.appendChild(script);
    } catch(err) {}
  }
 
  //Set the cookie
  function setCookie(c_name, value, expire) {
    var exdate=new Date();
    exdate.setDate(exdate.getDate()+expire);
    document.cookie = c_name+ "=" +escape(value) + ((expire==null) ? "" : ";expires="+exdate.toGMTString());
  }
 
  //Get the cookie content
  function getCookie(c_name) {
    if (document.cookie.length > 0 ) {
      c_start=document.cookie.indexOf(c_name + "=");
      if (c_start != -1){
        c_start=c_start + c_name.length+1;
        c_end=document.cookie.indexOf(";",c_start);
        if (c_end == -1) {
          c_end=document.cookie.length;
        }
        return unescape(document.cookie.substring(c_start,c_end));
      }
    }
    return '';
  }
}
</script>




<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KDV5VZT');</script>
<!-- End Google Tag Manager -->

  

<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-2.1.4.min.js"></script>
<script defer type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jquery_validate_min_js.js"></script>
<script defer type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/parallax.min.js"></script>
<script defer type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/jssor.slider-22.0.15.mini.js"></script>


<script defer type="text/javascript" src="<?php echo(SITE_URL)?>yogesh_new1/js/slick.min.js"></script>


    </head>
    <body>



	<div id="main-container">
		<div id="header-wrapper">
		   <?php 

			echo($this->Element('Front/headernew10'));  ?> 			
		
		</div>
	</div>



 
        <?php echo($content_for_layout); ?>
        <?php echo($this->Element("Front/footernew")); ?> 
	<!-- js link -->




    </body>
</html>