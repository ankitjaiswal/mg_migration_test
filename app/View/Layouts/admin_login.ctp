<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo $title_for_layout; ?> &raquo; <?php echo Configure::read('Site.title');?></title>    
	<?php
	  if(isset($meta_description) || isset($meta_keywords)){
	?>
	<meta name="description" content="<?php echo($meta_description);?>" />
    <meta name="keywords" content="<?php echo($meta_keywords);?>" /> 
     <?php
	 }
	 else{
        //echo $layout->meta();
       }		
     echo($this->Html->meta('icon'));     	 
	 echo($this->Html->css(array('admin_login')));
     echo($scripts_for_layout);
    ?>
    <!--[if lte IE 6]><style>
        img { behavior: url("<?php echo SITE_URL; ?>css/iepngfix.htc") }
		</style><![endif]-->
</head>
<body>
<div id="LoginWrpapper">
	<div id="header">
		<h1 class="logo">
			<?php  echo($this->Html->link($this->Html->image('media/logo.png', array('alt'=>Configure::read('Site.title').' Logo', 'title'=>Configure::read('Site.title').' Logo')), array(), array('escape'=>false)));?>

		</h1>
	</div>
	<div class="LoginForm" id="login">
		<?php	 
		 echo($content_for_layout); 
		?>
	</div>
</div>
<?php  // echo($this->element('sql_dump'));?>
</body>
</html>