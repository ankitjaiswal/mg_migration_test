<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml" lang="en-US">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="GOOGLEBOT" content="index,archive,follow">
      <meta name="msvalidate.01" content="67494CAA039573E002087689C5077B6C" />
      <meta name="google-site-verification" content="y3TwsghV-3TWMTz7Kni0ezXPt2EQoAu7s80Bso4pVxs" />
      <meta http-equiv="Cache-Control" content="max-age=200" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
      <meta name="format-detection" content="telephone=no">
      <link rel="icon" type="image/x-icon" href="<?php echo $this->webroot; ?>favicon.ico?v=2"  />
      <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->webroot; ?>favicon.ico?v=2"  />
     <?php echo($this->Html->script(array('jquery/jquery')));?>


        <?php 
		
	      $url = $this->request->url;
              if(strpos($url, "advantage")!== false){?>

		<meta property="og:title" content="GUILD Advantage | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD advantage, GUILD features, GUILD membership, executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants,management consultant" />
	        <meta name="description"  property="og:description" content="Learn about GUILD's features for consultants-the free GUILD profile, targeted advertising to generate leads, marketing and business tools to offer to your clients." />

        <?php }else if(strpos($url, "member_register")!== false){?>

		<meta property="og:title" content="Member Register | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Advantage,Member Register, GUILD features, GUILD membership, executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants,management consultant" />
	        <meta name="description"  property="og:description" content="Learn about GUILD's features for consultants-the free GUILD profile, targeted advertising to generate leads, marketing and business tools to offer to your clients." />

        <?php }else if(strpos($url, "websites")!== false){?>

		<meta property="og:title" content="GUILD Websites | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Websites, featured websites, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD develops Mobile-friendly websites for business consultants to connect them with their audience." />


        <?php }else if(strpos($url, "master")!== false){?>

		<meta property="og:title" content="GUILD Master | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Masters, featured works, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD strategizes in marketing best-practices for experienced business consultants." />


        <?php }else if(strpos($url, "members/eligibility")!== false){?>

		<meta property="og:title" content="Members Eligibility | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Members Eligibility, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD Membership is open to seasoned business professionals who meet the eligibility criteria." />

        <?php }else if(strpos($url, "members/apply")!== false){?>

		<meta property="og:title" content="Apply to be a member | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Membership Apply, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD Membership helps great consultants get more business." />

        <?php }else if(strpos($url, "members/flevy")!== false){?>

		<meta property="og:title" content="Business Documents by Flevy | Business Consulting | GUILD" />
                <meta name="keywords" content="Business Documents by Flevy at GUILD, Business frameworks & methodologies, presentation templates, financial models" />
	        <meta name="description"  property="og:description" content="GUILD offers Flevy's Business frameworks & methodologies, presentation templates, financial models, and more." />

        <?php }else if(strpos($url, "members/ninelenses")!== false){?>

		<meta property="og:title" content="Smarter Way to Engage Clients | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD's Smarter Way to Engage Clients, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD provides consultants a smarter way to engage clients." />

        <?php }else if(strpos($url, "fronts/privacy")!== false){?>

		<meta property="og:title" content="Privacy Policy | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Privacy Policy, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD offers business leaders proven expertise to solve hard business problems, as a cost-effective alternative to internal staffing or traditional consulting." />

        <?php }else if(strpos($url, "fronts/terms")!== false){?>

		<meta property="og:title" content="Terms of Service | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Terms of Service, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD offers business leaders proven expertise to solve hard business problems, as a cost-effective alternative to internal staffing or traditional consulting." />

        <?php }else if(strpos($url, "fronts/guarantee")!== false){?>

		<meta property="og:title" content="Client Satisfaction Guarantee | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Client Satisfaction Guarantee, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="GUILD offers business leaders proven expertise to solve hard business problems, as a cost-effective alternative to internal staffing or traditional consulting." />

        <?php }else if(strpos($url, "project/create")!== false){?>

		<meta property="og:title" content="Post A Project | Business Consulting | GUILD" />
                <meta name="keywords" content="GUILD Post A Project, GUILD membership, executive coach, business consulting" />
	        <meta name="description"  property="og:description" content="Specify your requirement using GUILD 1-page form." />

	 <?php } elseif(strpos($url,'question') !== false  && strpos($url,'/a') !== false){//meta tags for page to give answer of a question 
       
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $str = $path[3];
            $answerid = substr($str, 1);

            $last = $path[2]; // get the value of the last element 
            $last = str_replace("--","-",$last);
            $last = str_replace("-"," ",$last);
            $keys = parse_url($last);
            $text = explode(" ", $keys['path']);
            $id = end($text);
            $menu = ClassRegistry::init('QnaQuestion');  //for class load in view
            $Question = $menu->find('first', array('conditions' => array('QnaQuestion.id' => $id))); 
            $combined = $Question['QnaQuestion']['question_context']; 
            $qtitle  = $Question['QnaQuestion']['question_text'];

	        if(false !== stripos($qtitle,"'")){
        	$qtitle = str_replace("'"," ",$qtitle);
            }
	        if(false !== stripos($qtitle,'"')){
        	$qtitle = str_replace('"'," ",$qtitle);
            }
	        if(false !== stripos($qtitle,'&')){
        	$qtitle = str_replace('&',"and",$qtitle);
            }


	        if(false !== stripos($combined,"'")){
        	$combined = str_replace("'"," ",$combined);
            }
	        if(false !== stripos($combined,'"')){
        	$combined = str_replace('"'," ",$combined);
            }
	        if(false !== stripos($combined,'&')){
        	$combined = str_replace('&',"and",$combined);
            } 
            $length = strlen($combined);
            if($length >= 160)
            $result = preg_replace('/\s+?(\S+)?$/', '', substr($combined, 0, 160));         
            else
            $result = $combined;	

            $menu1 = ClassRegistry::init('QnaAnswer');  //for class load in view
            $Answer = $menu1->find('first', array('conditions' => array('QnaAnswer.id' => $answerid)));
            $menu2 = ClassRegistry::init('UserImage');  //for class load in view
            $Image = $menu2->find('first', array('conditions' => array('UserImage.user_id' => $Answer['QnaAnswer']['member_id'])));
            $image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$Answer['QnaAnswer']['member_id'].DS.$Image['UserImage']['image_name'];		

        ?>  
             <meta property="og:image" content="<?php echo($image_path);?>"/>		
             <meta property="og:title" content="<?php echo($qtitle);?>" />
             <meta name="keywords" content="top question & answers, executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, GUILD, management consultant" />
	     <meta name="description"  property="og:description" content="<?php echo($result);?>..." />

	    <?php } elseif(strpos($url,'question') !== false){//meta tags for each question & answer page
       
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $last = $path[2]; // get the value of the last element 
            $last = str_replace("--","-",$last);
            $last = str_replace("-"," ",$last);
            $keys = parse_url($last);
            $text = explode(" ", $keys['path']);
            $id = end($text);
            $menu = ClassRegistry::init('QnaQuestion');  //for class load in view
            $Question = $menu->find('first', array('conditions' => array('QnaQuestion.id' => $id))); 
            $combined = $Question['QnaQuestion']['question_context']; 
	        if(false !== stripos($combined,"'")){
        	$combined = str_replace("'"," ",$combined);
            }
	        if(false !== stripos($combined,'"')){
        	$combined = str_replace('"'," ",$combined);
            }
	        if(false !== stripos($combined,'&')){
        	$combined = str_replace('&',"and",$combined);
            }  
            $length = strlen($combined);
            if($length >= 160)
            $result = preg_replace('/\s+?(\S+)?$/', '', substr($combined, 0, 160));         
            else
            $result = $combined;

            $menu1 = ClassRegistry::init('QnaAnswer');  //for class load in view
            $Answer = $menu1->find('first', array('conditions' => array('QnaAnswer.question_id' => $id)));
			
            $menu2 = ClassRegistry::init('UserImage');  //for class load in view
            $Image = $menu2->find('first', array('conditions' => array('UserImage.user_id' => $Answer['QnaAnswer']['member_id'])));
            $image_path = SITE_URL.'img/'.MENTORS_IMAGE_PATH.DS.$Answer['QnaAnswer']['member_id'].DS.$Image['UserImage']['image_name'];

        ?>    
              <meta property="og:image" content="<?php echo($image_path);?>" />		
              <meta property="og:title" content="<?php echo($Question['QnaQuestion']['question_text']);?>" />
              <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, GUILD, management consultant" />
	      <meta name="description"  property="og:description" content="<?php echo($result);?>..." />

        <?php } elseif(strpos($url,'insight') !== false){//meta tags for insights
             
            $keys = parse_url($url); // parse the url
            $path = explode("/", $keys['path']); // splitting the path
            $last = $path[2]; // get the value of the last element 
            $last = str_replace("--","-",$last);
            $last = str_replace("-"," ",$last);
            $keys = parse_url($last);
            $text = explode(" ", $keys['path']);
            $id = end($text);
            $menu = ClassRegistry::init('Insight');  //for class load in view 
            $insight = $menu->find('first', array('conditions' => array('Insight.id' => $id))); 
            $combined = $insight['Insight']['insight'];
	        if(false !== stripos($combined,"'")){
        	$combined = str_replace("'"," ",$combined);
            }
	        if(false !== stripos($combined,'"')){
        	$combined = str_replace('"'," ",$combined);
            }
	        if(false !== stripos($combined,'&')){
        	$combined = str_replace('&',"and",$combined);
            } 

            $length = strlen($combined);
            if($length >= 160)
            $result = preg_replace('/\s+?(\S+)?$/', '', substr($combined, 0, 160));         
            else
            $result = $combined;
			
        ?>
              <meta property="og:image" content="<?php echo($insight['Insight']['image']);?>" />		
              <meta property="og:title" content="<?php echo($insight['Insight']['title']);?>" />
              <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, GUILD, management consultant" />
	      <meta name="description"  property="og:description" content="<?php echo($result);?>..." />


        <?php }else{?>

               <meta property="og:title" content="Business Consulting | GUILD" />
               <meta name="keywords" content="executive coach, business consulting, CEO coach, management consulting, top coaches, top consultants, GUILD, management consultant" />
	       <meta name="description"  property="og:description" content="GUILD offers business leaders proven expertise to solve hard business problems, as a cost-effective alternative to internal staffing or traditional consulting." />

        <?php }?>
	


        <meta property="og:image" content="<?php echo(SITE_URL);?>img/media/guild-finallogo.png" />	
	<meta property="og:url" content="<?php echo(SITE_URL);?><?php echo($this->request->url);?>" />
        <meta property="og:site_name" content="GUILD"/>
	<meta property="og:type" content="Website" />   
        <meta name="language" content="English"/>

        <meta name="DC.Title" content="Business Consulting | GUILD" />
        <meta name="DC.Creator" content="GUILD" />
        <meta name="DC.Subject" content="Business Consulting" />
        <meta name="DC.Description" content="GUILD offers business leaders proven expertise to solve hard business problems, as a cost-effective alternative to internal staffing or traditional consulting." />
        <meta name="DC.Publisher" content="GUILD" />
        <meta name="DC.Date" content="2015-04-22" />
        <meta name="DC.Type" content="Text" />
        <meta name="DC.Language" content="en-US" />
        <meta name="DC.Rights" content="GUILD" />  

     
	     <title><?php if(strpos($this->request->url, "qna/question")!== false && $title_for_layout != '')
                        { echo $title_for_layout; ?> | GUILD
                  <?php }else if ($title_for_layout == 'GUILD: Expertise On Demand'){
            			echo $title_for_layout; 
                  }else if ($title_for_layout != ''){
            			echo $title_for_layout; ?> | <?php echo Configure::read('Site.title'); 
            	    }else {
				echo Configure::read('Site.title');}?></title>
		
        <link rel="icon" href="<?php echo $this->webroot; ?>favicon.ico?v=2" type="image/x-icon" />
        <link rel="shortcut icon" href="<?php echo $this->webroot; ?>favicon.ico?v=2" type="image/x-icon" />



	<link rel="stylesheet" type="text/css" href="<?php echo(SITE_URL)?>yogesh_new1/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo(SITE_URL)?>yogesh_new1/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="<?php echo(SITE_URL)?>yogesh_new1/css/fonts.css">
    <link rel="stylesheet" type="text/css" href="<?php echo(SITE_URL)?>yogesh_new1/css/jquery-ui.css">
    <link type="text/css" href="<?php echo(SITE_URL)?>yogesh_new1/css/style.css?v=3.40" rel="stylesheet">
        <script type="text/javascript">
            jQuery.noConflict();
            var SiteUrl = "<?php echo SITE_URL; ?>";
            var SITE_URL = "<?php echo SITE_URL; ?>";	
			var loggedUserId = '';
			var memberId = '';
			var mentee_url = '';
        </script>



  




    </head>
    <body>
	<div id="main-container">
		<div id="header-wrapper">
		  <?php 

			echo($this->Element('Front/headernew10'));  ?> 	
		
		</div>
	</div>



 
        <?php echo($content_for_layout); ?>
        <?php echo($this->Element("Front/footernew")); ?>





    </body>
</html>
