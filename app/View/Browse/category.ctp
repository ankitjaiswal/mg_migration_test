<script type="text/javascript">
mixpanel.track("Practice Area Search", {"URL": document.URL});
</script>

<div id="inner-content-wrapper">
    <div id="inner-content-box" class="pagewidth">
      <div id="financial-management">
        <p>
		<span><?php 
		$memCount = count($data);
		$dirCount = count($dUsersFinal);
		$totCount = $memCount + $dirCount;
		if($keyword == ''){
		echo "Browse all";
		}else{
		echo $keyword;	
		}
		?></span> 
		<span style="float: right; font-weight: normal;"> <?php echo($totCount." experts");?></span>
		</p>
      </div>
      <div id="mentors-content">
		<?php
			echo($this->Element('Front/search'));
		?>
      </div>
    </div>
  </div> 
<?php 
if(empty($data)){?>
 <script type="text/javascript">
	jQuery(window).bind('load resize',function(){
		if(jQuery("body").height() < jQuery(window).height()){
			jQuery("#footer-wrapper").css({'position':'fixed'})
		}
		
	});
 </script>
 <?php
}?>
