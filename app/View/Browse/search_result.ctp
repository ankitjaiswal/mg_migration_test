<div id="inner-content-wrapper" class="content-wrap">
    <div id="inner-content-box" class="container">
        <div class="row">
            <div class="col-sm-12">
                <div id="financial-management">
                    <p>
                        <span><?php
                            $memCount = count($data);
                            $dirCount = count($dUsersFinal);
                            $totCount = $memCount + $dirCount;
                            
                            $topictable= ClassRegistry::init('Topic');  //for class load in view
                            $topictabledata = $topictable->find('first', array('conditions' => array('Topic.autocomplete_text	' => $keyword))); 
                            if($topictabledata['Topic']['isName'] == 1)
                            {
                             $name = "yes";
                            }else{
                             $name = "no";
                            }
                            if($adIndustry == 'all industries' && $adSubIndustry == 'all categories' && $keyword != '' && $keyword == 'lean six sigma' && $name == 'no'){
                            echo ('Experts in '.ucwords($keyword));
                            }
                            else if($adIndustry == 'all industries' && $adSubIndustry == 'all categories' && $keyword != '' && $keyword != 'browse all' && $name == 'no'){
                            echo ('Experts in '.ucfirst($keyword));
                            }
                            else if($adIndustry == 'all industries' && $adSubIndustry == 'all categories' && $keyword != '' && $keyword != 'browse all' && $name == 'yes'){
                            echo ('Search results for "'.ucwords($keyword).'"');
                            }
                            else if($adIndustry == 'all industries' && $adSubIndustry == 'all categories' && $keyword == 'browse all'){
                            echo ( 'Experts near you');
                            }
                            else if($adIndustry != 'all industries' && $adSubIndustry == 'all categories' && $keyword == 'browse all'){
                            echo ( 'Experts in '.ucfirst($adIndustry));
                            }
                            else if($adIndustry != 'all industries' && $adSubIndustry == 'all categories' && $keyword != '' && $keyword != 'browse all' && $name == 'no'){
                            echo (ucfirst($keyword).' experts in '.ucfirst($adIndustry));
                            }
                            else if($adIndustry != 'all industries' && $adSubIndustry == 'all categories' && $keyword != '' && $keyword != 'browse all' && $name == 'yes'){
                            echo ('Search results for "'.ucwords($keyword).'"');
                            }
                            else if($adIndustry != 'all industries' && $adSubIndustry != 'all categories' && $keyword == 'browse all'){
                            echo (ucfirst($adSubIndustry).' experts in '.ucfirst($adIndustry));
                            }
                            else if($adIndustry != 'all industries' && $adSubIndustry != 'all categories' && $keyword != '' && $keyword != 'browse all' && $name == 'no'){
                            echo (ucfirst($keyword).' experts in '.ucfirst($adIndustry));
                            }
                            else if($adIndustry != 'all industries' && $adSubIndustry != 'all categories' && $keyword != '' && $keyword != 'browse all' && $name == 'yes'){
                            echo ('Search results for "'.ucwords($keyword).'"');
                            }
                            else{
                            echo ('Experts near you');
                            }
                        ?></span>
                        <span style="float: right; font-weight: normal;"> <?php echo($totCount." experts");?></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="expert-list">
                    <div id="mentors-content">
                        <?php echo($this->Element('Front/search')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if(empty($data)){?>
    <script type="text/javascript">
        jQuery(window).bind('load resize',function(){
            if(jQuery("body").height() < jQuery(window).height()){
                jQuery("#footer-wrapper").css({'position':'fixed'})
            }
            
        });
    </script>
<?php }?>