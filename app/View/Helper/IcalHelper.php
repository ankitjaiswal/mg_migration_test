<?php 
require_once('../Vendor/ical/vcalendar.class.php');

//App::uses("vcalendar", "Vendor/ical");
//App::import("Vendor", "ical/vcalendar.class.php"); 
/*$path = '/';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);*/
	
//include "ical/vcalendar.class.php";
//App::import('Vendor', 'ical', array('file' => 'vcalendar.class.php'));
class IcalHelper extends Helper 
{
    var $name = 'ICalHelper';
    var $errorCode = null;
    var $errorMessage = null;
    
    var $calendar;
            
    function create($name, $description='', $tz='US/Eastern',$start)
    {
        $v = new vcalendar();
	 $v->setConfig('unique_id', $name.'.com');
        $v->setProperty('method', 'PUBLISH');
        $v->setProperty('x-wr-calname', $name.' Calendar');
        $v->setProperty("X-WR-CALDESC", $description);
        $v->setProperty("X-WR-TIMEZONE", $tz);
       
        //$config = array( "unique_id" => "kigkonsult.se", "TZID" => "Europe/Stockholm" ); 
		
		//$standard->setComponent($standard);
		//$vtimezone->setComponent($vtimezone);
        $this->calendar = $v;
    }
    
    function addEvent($start, $end=false, $summary, $description='', $extra=false,$tz='US/Eastern')
    {
    	switch($tz)
    	{
			case 'Pacific/Honolulu':
					$zoneSet = 'HAST';
					$offsetFrom = "-0900";
					$offsetTo   = "-1000";
					break;
			case 'America/Anchorage':
					$zoneSet = 'AKST';
					$offsetFrom = "-0800";
					$offsetTo   = "-0900";
					break;
			case 'America/Los_Angeles':
					$zoneSet = 'PST';
					$offsetFrom = "-0700";
					$offsetTo   = "-0800";
					break;
			case 'America/Phoenix':
					$zoneSet = 'MST';
					$offsetFrom = "-0600";
					$offsetTo   = "-0700";
					break;
			case 'America/Chicago':
					$zoneSet = 'CST';
					$offsetFrom = "-0500";
					$offsetTo   = "-0600";
					break;
			case 'America/New_York':
					$zoneSet = 'EST';
					$offsetFrom = "-0400";
					$offsetTo   = "-0500";
					break;
    	}
        $vtimezone = new vtimezone();
		$vtimezone->setProperty('tzid',$tz);		
		$vtimezone->setProperty("X-LIC-LOCATION", $tz);
		
		$standard = new vtimezone("standard");
		$standard->setProperty("tzoffsetfrom", $offsetFrom);
        $standard->setProperty("tzoffsetto", $offsetTo);
		$standard->setProperty("tzname", $zoneSet);
        $standard->setProperty("dtstart","19701101T020000");		
        $this->calendar->setComponent($vtimezone);
		$standard->setProperty("rrule", array("FREQ"=>"YEARLY","BYMONTH"=>"11","BYDAY"=>"1SU"));	
        $this->calendar->setComponent($standard);
        $vevent = new vevent();
		/* right code */
		
		$vevent->setProperty('dtstart',$start,array( 'TZID' => $extra['location'] ));
        $vevent->setProperty('dtend',$end,array( 'TZID' => $extra['location'] ));
		
        $vevent->setProperty('summary', $summary);
        $vevent->setProperty('description', $description);
		
		$this->calendar->setComponent($vevent);
		/* right code */        
    }
		
	
	function addEventFull($extra1, $extra=false)
    {
        $vevent = new vevent();
		
        if(is_array($extra1))
        {
            foreach($extra1 as $data)
            { 
	            //$vevent->setProperty('uid', $data['uid']);
			    $vevent->setProperty('dtstart', $data['dtstart']);
 			    $vevent->setProperty('dtend', $data['dtend']);
				$vevent->setProperty('summary', $data['summary']);
 			    $vevent->setProperty('description', $data['description']);
				//$vevent->getProperty('uid',$data['uid']);
				//pr($vevent); die;
				$uid = $vevent->setProperty('uid',$this->_makeuid($data['dtstart']));
				$this->calendar->setComponent($vevent);
			}
        }
        
    }
    
    function getCalendar()
    {
        return $this->calendar;
    }
    
    function render()
    {
        $this->calendar->returnCalendar();
		
    }
	
	function _makeUid($stdate) 
	{
		$date   = date('Ymd',$stdate);
		$unique = substr(microtime(), 2, 4);
		$base   = 'aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPrRsStTuUvVxXuUvVwWzZ1234567890';
		$start  = 0;
		$end    = strlen( $base ) - 1;
		$length = 6;
		$str    = null;
		for( $p = 0; $p < $length; $p++ )
		  $unique .= $base{mt_rand( $start, $end )};
		$this->uid = array( 'params' => null );
		return $date.'-'.$unique.'@guild.im';
  	}
}