<?php
class QnaQuestion extends AppModel{

	var $name = "QnaQuestion";
	/**
     * Behaviors used by the Model
     *
     * @var array
     * @access public
     */
	
	var $hasMany = array(
			'QnaQuestionCategory' => array(
					'className' => 'QnaQuestionCategory',
					'foriegnKey' => 'id',
					'dependent' => true
			),
	);
	
	function createUrlKey($id = null){
	
		$this->recursive = 0;
		$this->data = $this->findById($id);
		if(isset($this->data['QnaQuestion']['question_text'])){

			
			$urlKey = preg_replace('/\PL/u', '-', $this->data['QnaQuestion']['question_text']);
			$urlKey .= '-'.$id;
				
			$this->updateAll(array('QnaQuestion.url_key'=>"'".$urlKey."'"),array('QnaQuestion.id'=>$id));
		}
	}
   
}
?>