<?php

class Social extends AppModel {

    var $name = 'Social';
    /**
     * Behaviors used by the Model
     *
     * @var array
     * @access public
     */
    var $actsAs = array('Multivalidatable');
    var $belongsTo = array('User');
    var $validationSets = array(
        'front' => array(
            'social_name' => array(
                'rule' => 'checkLink',
                'message' => "Valid link required",
            )
        )
    );
    function checkLink($subject) { //pr($subject); exit;
        return preg_match('/^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-‌​\.\?\,\'\/\\\+&amp;%\$#_]*)?$/', $subject['social_name']) == false ? false : true;
    }

}

?>