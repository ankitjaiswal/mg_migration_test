<?php
class UserExpertise extends AppModel {

	var $name = 'UserExpertise';	
	var $actsAs = array('Multivalidatable');
	var $belongsTo=array('User');
	var $validationSets = array(	
		'admin'=>array(
			'cat_id' => array(
				'notEmpty'=> array(
					'rule' => 'notEmpty',  
	        		'message' => 'First name is required.'
				)	
			)
		),
		'user'=>array(
			'category_id' => array(
				'notEmpty'=> array(
					'rule' => 'notEmpty',  
	        		'message' => 'Please select a value in the drop down.'
				)	
			),
			'short_desc' => array(
				'notEmpty'=> array(
					'rule' => 'notEmpty',  
	        		'message' => 'This is required.'
				)	
			),
			'long_desc' => array(
				'notEmpty'=> array(
					'rule' => 'notEmpty',  
	        		'message' => 'This is required.'
				)	
			),
			'degrees' => array(
				'notEmpty'=> array(
					'rule' => 'notEmpty',  
	        		'message' => 'This is required.'
				)	
			),
			'experience' => array(
				'notEmpty'=> array(
					'rule' => 'notEmpty',  
	        		'message' => 'This is required.'
				)	
			),
			
			
			
		),
		
		
	);	
}
?>