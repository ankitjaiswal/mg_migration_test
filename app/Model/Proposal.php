<?php

App::uses('AppModel', 'Model');
class Proposal extends AppModel{

	var $name = "Proposal";


    var $actsAs = array('Multivalidatable');
    var $belongsTo = array('User');

    var $hasMany = array(
			'ProposalTopic' => array(
					'className' => 'ProposalTopic',
					'foriegnKey' => 'id',
					'dependent' => true
			),
	);

}
?>