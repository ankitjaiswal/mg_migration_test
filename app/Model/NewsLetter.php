<?php
/**
 * Specialty
 *
 * PHP version 5
 *
 * @category Model 
 * 
 */
class NewsLetter extends AppModel{
	/**
	 * Model name
	 *
	 * @var string
	 * @access public
	 */
	var $name = 'NewsLetter';
	
	/**
	 * Behaviors used by the Model
	 *
	 * @var array
	 * @access public
	 */
    var $actsAs = array(        
        'Multivalidatable'
    );
	
	/**
     * Custom validation rulesets
     */	
	var $validationSets = array(
		'admin'	=>	array(			
			'email'=>array(
				'isUnique'	=>	array(
					'rule'	=>	array('checkEmail','email'),
					'message'	=>	'Email is already exists.'
				),
				'notEmpty' => array(
					'rule' 		=> 'notEmpty',
					'message' 	=>	'Email is required'
				)
			)
	
		)	
	);	
	function checkEmail($data = null, $field=null)
	{
		//prd($this->data[$this->name][$field]);
		if(!empty($field)){
			if(!empty($this->data[$this->name][$field])){				
				if(isset($this->data['NewsLetter']['id'])){
					$condition = $this->hasAny(array('NewsLetter.id !='=>$this->data[$this->name]['id'],'NewsLetter.email' => $this->data[$this->name][$field]));
				}else{
					$condition = $this->hasAny(array('NewsLetter.email' => $this->data[$this->name][$field]));
				}
				if($condition){
					return false;
				}else{
					return true;
				}
			}
		}
	}	
	/*function getTemplate($slug = null){
		if(!empty($slug)){
			$data = $this->find('first', array('conditions'=>array('NewsLetter.slug'=>$slug)));
			return $data;
		}
	}*/
	
}
?>