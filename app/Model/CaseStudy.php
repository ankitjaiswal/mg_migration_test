<?php
App::uses('AppModel', 'Model');
class CaseStudy extends AppModel {

    var $name = 'CaseStudy';

    var $actsAs = array('Multivalidatable');
    var $belongsTo = array('User');


    var $hasMany = array(
			'CasestudyTopic' => array(
					'className' => 'CasestudyTopic',
					'foriegnKey' => 'id',
					'dependent' => true
			),
	);
    
    function createUrlKey($id = null){
    
    	$this->recursive = 0;
    	$this->data = $this->findById($id);
    	if(isset($this->data['CaseStudy']['title'])){
    
    			
    		$urlKey = preg_replace('/\PL/u', '-', $this->data['CaseStudy']['title']);
    		$urlKey .= '-'.$id;
    
    		$this->updateAll(array('CaseStudy.url_key'=>"'".$urlKey."'"),array('CaseStudy.id'=>$id));
    	}
    }
    
}

?>