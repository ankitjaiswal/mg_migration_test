<?php
App::uses('AppModel', 'Model');
class UserReference extends AppModel {

    var $name = 'UserReference';
    /**
     * Behaviors used by the Model
     *
     * @var array
     * @access public
     */
    var $actsAs = array('Multivalidatable');
    var $belongsTo = array('User');
    var $validationSets = array(
        'admin' => array(
            'first_name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'First name is required.'
                )
            ),
            'last_name' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Last name is required.'
                )
            ),
/*             'zipcode' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Zipcode is required.'
                )
            ), */
            'Phone' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Phone is required.'
                )
            ),
            'background_summary' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Background summary is required.'
                )
            ),
            'fee_first_hour' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'This is required.'
                )
            ),
            'fee_regular_session' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'This is required.'
                )
            )
        ),
        'front' => array(
            'first_name' => array(
                'rule' => 'notEmpty',
                'message' => 'First name is required.',
            ),
            'last_name' => array(
                'rule' => 'notEmpty',
                'message' => 'Last name is required.',
            ),
            'background_summary' => array(
                'rule' => 'notEmpty',
                'message' => 'Background summary is required.',
            )
        )
    );
	
}

?>