<?php
class QnaQuestionCategory extends AppModel{

	var $name = "QnaQuestionCategory";
	/**
     * Behaviors used by the Model
     *
     * @var array
     * @access public
     */
	var $belongsTo = array('QnaQuestion');
}
?>