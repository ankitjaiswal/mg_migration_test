<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel {
    
    public $name = 'User';
    /**
     * Behaviors used by the Model
     *
     * @var array
     * @access public
     */
    var $actsAs = array(
        'Multivalidatable'
    );
    var $hasOne = array(
        'UserReference' => array(
            'className' => 'UserReference',
            'foriegnKey' => 'user_id',
            'dependent' => true
        ),
        'Availablity' => array(
            'className' => 'Availablity',
            'foriegnKey' => 'user_id',
            'dependent' => true
        ),
       'ShowcaseClient' => array(
    		'className' => 'ShowcaseClient',
    		'foriegnKey' => 'user_id',
    		'dependent' => true
    	),
    	'MemberIndustryCategory' => array(
    		'className' => 'MemberIndustryCategory',
    		'foriegnKey' => 'user_id',
    		'dependent' => true
    	)
    );
    var $hasMany = array(
        'UserImage' => array(
            'className' => 'UserImage',
            'foriegnKey' => 'user_id',
            'dependent' => true
        ),

        'Social' => array(
            'className' => 'Social',
            'foriegnKey' => 'user_id',
            'dependent' => true
        ),

        'Communication' => array(
            'className' => 'Communication',
            'foriegnKey' => 'user_id',
            'dependent' => true
        ),
    	'CaseStudy' => array(
    		'className' => 'CaseStudy',
    		'foriegnKey' => 'user_id',
    		'dependent' => true
    	),
       'Feed' => array(
    		'className' => 'Feed',
    		'foriegnKey' => 'user_id',
    		'dependent' => true
    	), 
       'Proposal' => array(
    		'className' => 'Proposal',
    		'foriegnKey' => 'user_id',
    		'dependent' => true
    	), 
       'Testimonial' => array(
    		'className' => 'Testimonial',
    		'foriegnKey' => 'user_id',
    		'dependent' => true
    	)


         
    );
    /**

     * Custom validation rulesets
     */
    var $validationSets = array(
        'admin' => array(
            'username' => array(
                'isUnique' => array(
                    'rule' => array('checkEmail', 'username'),
                    'message' => 'Email already exists.'
                ),
                'email' => array(
                    'rule' => 'email',
                    'message' => 'Please provide a valid email address.'
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Email is required'
                )
            ),
            'confirm_password' => array(
                'identicalFieldValues' => array(
                    'rule' => array('identicalFieldValues', 'password2'),
                    'message' => 'Do not match confirm password please re enter password.'
                ),
                'R1' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Confirm password is required.'
                )
            )
        ),
        'front' => array(
            'username' => array(
                'isUnique' => array(
                    'rule' => array('checkEmail', 'username'),
                    'message' => 'Email already exists.'
                ),
                'email' => array(
                    'rule' => 'email',
                    'message' => 'Please provide a valid email address.'
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Email is required'
                )
            )
        ),
        'change_password' => array(
            'password2' => array(
                'minlength' => array(
                    'rule' => array('minLength', 6),
                    'message' => 'Password should be atleast 6 characters long.'
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Password is required'
                )
            ),
            'confirm_password' => array(
                'identicalFieldValues' => array(
                    'rule' => array('identicalFieldValues', 'password2'),
                    'message' => 'Do not match confirm password please re enter password.'
                ),
                'R1' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Confirm password is required.'
                )
            )
        ),
        'forget_password' => array(
            'email' => array(
                'email' => array(
                    'rule' => 'email',
                    'message' => 'Email is invalid.'
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Email is required'
                )
            )
        )
    );
    function checkEmail($data = null, $field=null) {

        if (!empty($field)) {
            if (!empty($this->data[$this->name][$field])) {
                if (isset($this->data['User']['id'])) {
                    $condition = $this->hasAny(array('User.id !=' => $this->data['User']['id'], 'User.username' => $this->data[$this->name][$field], 'User.role_id' => $this->data[$this->name]['role_id']));
                } else {
                    $condition = $this->hasAny(array('User.username' => $this->data[$this->name][$field], 'User.role_id' => $this->data[$this->name]['role_id']));
                }
                if ($condition) {
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    /*  */

    function identicalFieldValues($field=array(), $compare_field=null) {

        foreach ($field as $key => $value) {
            $v1 = $value;
            $v2 = $this->data[$this->name][$compare_field];

            if ($v1 !== $v2) {
                return false;
            } else {
                continue;
            }
        }
        return true;
    }

    function identicalEmail($field=array(), $compare_field=null) {

        foreach ($field as $key => $value) {
            $v1 = $value;
            $v2 = $this->data[$this->name][$compare_field];

            if ($v1 !== $v2) {
                return false;
            } else {
                continue;
            }
        }
        return true;
    }

    function checkOldPassword($field = array(), $password = null) {
        App::import('Component', 'Session');
        $Session = new SessionComponent();
        $userId = $Session->read('Auth.User.id'); //User or Admin or 
        $count = $this->find('count', array('conditions' => array(
                        'User.password' => $this->data[$this->name][$password],
                        'User.id' => $userId
                        )));
        if ($count == 1) {
            return true;
        } else {
            return false;
        }
    }

    /* return username */

    function getUserName($id = null) {
        if ($id) {
            $this->unbindModel(array('hasMany' => array('UserImage')), false);
            $data = $this->read(array('UserReference.first_name', 'UserReference.last_name', 'User.email'), $id);
            if (!empty($data)) {
                if (!empty($data['UserReference']['first_name'])) {
                    return ucfirst($data['UserReference']['first_name']) . ' ' . ucfirst($data['UserReference']['last_name']);
                }
                return $data['User']['email'];
            }
        }
        return false;
    }
	//make unque url link
	function afterSave(){
		
		if(isset($this->data['User']['username']) && isset($this->data['UserReference']['first_name'])){
			$urlKey = trim($this->data['UserReference']['first_name']).'.'.trim($this->data['UserReference']['last_name']);
			$i = 1;
			do{
				$count = $this->find('count',array('conditions'=>array('User.username !='=>$this->data['User']['username'],'User.url_key'=>$urlKey)));
				//echo $count;die;
				if($count<=0){					
					$this->updateAll(array('User.url_key'=>"'".$urlKey."'"),array('User.username'=>$this->data['User']['username']));
					break;
				}
				$i++;
				$urlKey .=$i;
				
			
			}while(1);
		}
		
	}
    
    function createUrlKey($id = null){
        
        $this->recursive = 0;
        $this->data = $this->findById($id);
        if(isset($this->data['User']['username']) && isset($this->data['UserReference']['first_name'])){
            $urlKey = trim($this->data['UserReference']['first_name']).'.'.trim($this->data['UserReference']['last_name']);
            
			$count = $this->find('count',array('conditions'=>array('User.username !='=>$this->data['User']['username'],'User.url_key'=>$urlKey)));
			
			if($count > 0) {
				$urlKey .= '-'.$id;
			}
			
			$this->updateAll(array('User.url_key'=>"'".$urlKey."'"),array('User.id'=>$id));
        }
    }
	
	public function beforeSave($options = array()) {
    	if (isset($this->data[$this->alias]['password'])) {
	        $passwordHasher = new BlowfishPasswordHasher();
	        $this->data[$this->alias]['password'] = $passwordHasher->hash(
	            $this->data[$this->alias]['password']
	        );
	    }
	    return true;
	}
}

?>