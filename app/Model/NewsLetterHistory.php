<?php
/**
 * Specialty
 *
 * PHP version 5
 *
 * @category Model 
 * 
 */
class NewsLetterHistory extends AppModel{
	/**
	 * Model name
	 *
	 * @var string
	 * @access public
	 */
	var $name = 'NewsLetterHistory';
	
	/**
	 * Behaviors used by the Model
	 *
	 * @var array
	 * @access public
	 */
    var $actsAs = array(        
        'Multivalidatable'
    );
	
	/**
     * Custom validation rulesets
     */	
	var $validationSets = array(
		'admin'	=>	array(			
			'to'=>array(
				'notEmpty' => array(
					'rule' 		=> 'notEmpty',
					'message' 	=>	'To is required'
				)
			),
			'subject'=>array(			
				'notEmpty' => array(
					'rule' 		=> 'notEmpty',
					'message' 	=>	'Subject is required'
				)
			),		
			'description'=>array(				
				'notEmpty' => array(
					'rule' 		=> 'notEmpty',
					'message' 	=>	'Description is required'
				)
			)	
		)	
	);	
	

	
}
?>