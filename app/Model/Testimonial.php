<?php
App::uses('AppModel', 'Model');
class Testimonial extends AppModel {

    var $name = 'Testimonial';

    var $actsAs = array('Multivalidatable');
    var $belongsTo = array('User');
    
}

?>