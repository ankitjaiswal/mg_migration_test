<?php
class Insight extends AppModel{

	var $name = "Insight";
	/**
     * Behaviors used by the Model
     *
     * @var array
     * @access public
     */
	
	var $hasMany = array(
			'Insight_category' => array(
					'className' => 'Insight_category',
					'foriegnKey' => 'id',
					'dependent' => true
			),
	);
	
	function createUrlKey($id = null){
	
		$this->recursive = 0;
		$this->data = $this->findById($id);
		if(isset($this->data['Insight']['title'])){

			
			$urlKey = preg_replace('/\PL/u', '-', $this->data['Insight']['title']);
			$urlKey .= '-'.$id;
				
			$this->updateAll(array('Insight.url_key'=>"'".$urlKey."'"),array('Insight.id'=>$id));
		}
	}
   
}
?>