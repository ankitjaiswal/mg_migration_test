<?php
class InsightCategory extends AppModel{

	var $name = "InsightCategory";
	/**
     * Behaviors used by the Model
     *
     * @var array
     * @access public
     */
	var $belongsTo = array('Insight');
}
?>