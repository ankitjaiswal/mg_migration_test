<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	//Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */

  Router::parseExtensions('xml');

    Router::connect('/mentors/56KOv57lNjSR9yI6q5Ps', array('controller' => 'members', 'action' => 'general','56KOv57lNjSR9yI6q5Ps'));
    Router::connect('/members/56KOv57lNjSR9yI6q5Ps', array('controller' => 'members', 'action' => 'general','56KOv57lNjSR9yI6q5Ps'));
    Router::connect('/application_submitted', array('controller' => 'staticPages', 'action' => 'thanks'));
	Router::connect('/home', array('controller' => 'fronts', 'action' => 'index','home'));
	Router::connect('/', array('controller' => 'fronts', 'action' => 'index','home'));
	Router::connect('/index.html', array('controller' => 'fronts', 'action' => 'index'));
	Router::connect('/index.php', array('controller' => 'fronts', 'action' => 'index'));
	Router::connect('/fronts', array('controller' => 'fronts', 'action' => 'index'));
	Router::connect('/fronts/index', array('controller' => 'fronts', 'action' => 'index'));
	Router::connect('/users', array('controller' => 'fronts', 'action' => 'index'));
	Router::connect('/users/index', array('controller' => 'fronts', 'action' => 'index'));
	Router::connect('/admin/login', array('controller' => 'users', 'action' => 'admin_login'));
	Router::connect('/admin/dashboard', array('controller' => 'users', 'action' => 'admin_dashboard'));
	Router::connect('/clients/process', array('controller' => 'clients', 'action' => 'admin_process'));
	Router::connect('/browse/admin_indexes', array('controller' => 'browse', 'action' => 'admin_indexes'));
	Router::connect('/admin/project/review', array('controller' => 'project', 'action' => 'admin_review'));
       Router::connect('/qna/member_categories', array('controller' => 'qna', 'action' => 'admin_member_categories'));
       Router::connect('/qna/categories', array('controller' => 'qna', 'action' => 'admin_categories'));
       Router::connect('/qna/question_review', array('controller' => 'qna', 'action' => 'admin_question_review'));
       Router::connect('/qna/media_add', array('controller' => 'qna', 'action' => 'admin_media_add'));
       Router::connect('/qna/media_record', array('controller' => 'qna', 'action' => 'admin_media_record'));
       Router::connect('/admin/members/add', array('controller' => 'members', 'action' => 'admin_add'));
       Router::connect('/admin/members', array('controller' => 'members', 'action' => 'admin_index'));
       //Router::connect('/admin/members/featured', array('controller' => 'members', 'action' => 'admin_featured'));
       Router::connect('/admin/members/pending', array('controller' => 'members', 'action' => 'admin_pending'));
       Router::connect('/admin/members/directory_index', array('controller' => 'members', 'action' => 'admin_directory_index'));
       Router::connect('/admin/members/directory_add', array('controller' => 'members', 'action' => 'admin_directory_add'));
      Router::connect('/members/admin_directory_clean', array('controller' => 'members', 'action' => 'admin_directory_clean'));
       Router::connect('/members/edit/:username',
	 array('controller' => 'members', 'action' => 'admin_edit'),
	 array(
	 'pass' => array('username')
	 )
	 );
       Router::connect('/members/directory_edit/:username',
	 array('controller' => 'members', 'action' => 'admin_directory_edit'),
	 array(
	 'pass' => array('username')
	 )
	 );
       Router::connect('/members/directory_delete/:username',
	 array('controller' => 'members', 'action' => 'admin_directory_delete'),
	 array(
	 'pass' => array('username')
	 )
	 );
       Router::connect('/members/delete/:username',
	 array('controller' => 'members', 'action' => 'admin_delete'),
	 array(
	 'pass' => array('username')
	 )
	 );
       Router::connect('/members/changepassword/:username',
	 array('controller' => 'members', 'action' => 'admin_changepassword'),
	 array(
	 'pass' => array('username')
	 )
	 );
       Router::connect('/members/switch_user/:username',
	 array('controller' => 'members', 'action' => 'switch_user'),
	 array(
	 'pass' => array('username')
	 )
	 );
       Router::connect('/members/activateMentor/:username',
	 array('controller' => 'members', 'action' => 'admin_activateMentor'),
	 array(
	 'pass' => array('username')
	 )
	 );
       Router::connect('/members/declineMentor/:username',
	 array('controller' => 'members', 'action' => 'admin_declineMentor'),
	 array(
	 'pass' => array('username')
	 )
	 );
       Router::connect('/members/detail/:username',
	 array('controller' => 'members', 'action' => 'admin_detail'),
	 array(
	 'pass' => array('username')
	 )
	 );
       Router::connect('/members/specifierChange/:username/:status',
	 array('controller' => 'members', 'action' => 'admin_specifierChange'),
	 array(
	 'pass' => array('username', 'status')
	 )
	 );
       Router::connect('/members/featured/:username',
	 array('controller' => 'members', 'action' => 'admin_featured'),
	 array(
	 'pass' => array('username')
	 )
	 );
       Router::connect('/members/deactivate_featured/:username',
	 array('controller' => 'members', 'action' => 'admin_deactivate_featured'),
	 array(
	 'pass' => array('username')
	 )
	 );
       Router::connect('/admin/clients', array('controller' => 'clients', 'action' => 'admin_index'));
       Router::connect('/admin/clients/add', array('controller' => 'clients', 'action' => 'admin_add'));
       Router::connect('/admin/fronts/prospective', array('controller' => 'fronts', 'action' => 'admin_prospective'));
       Router::connect('/admin/fronts/takeaway_prospective', array('controller' => 'fronts', 'action' => 'admin_takeaway_prospective'));
       Router::connect('/admin/qna/category_member_search', array('controller' => 'qna', 'action' => 'admin_category_member_search'));
       Router::connect('/settings/admin_index', array('controller' => 'settings', 'action' => 'admin_index'));
       Router::connect('/settings/edit/:username',
	 array('controller' => 'settings', 'action' => 'admin_edit'),
	 array(
	 'pass' => array('username')
	 )
	 );
       Router::connect('/settings/admin_add', array('controller' => 'settings', 'action' => 'admin_add'));
       Router::connect('/static_pages/admin_index', array('controller' => 'static_pages', 'action' => 'admin_index'));
       Router::connect('/static_pages/edit/:username',
	 array('controller' => 'static_pages', 'action' => 'admin_edit'),
	 array(
	 'pass' => array('username')
	 )
	 );
       Router::connect('/static_pages/admin_index', array('controller' => 'static_pages', 'action' => 'admin_index'));
	Router::connect('/admin/qna/editquestion/:username',
	 array('controller' => 'qna', 'action' => 'admin_editquestion'),
	 array(
	 'pass' => array('username')
	 )
	 );
	Router::connect('/admin/qna/admin_deletequestion/:username',
	 array('controller' => 'qna', 'action' => 'admin_deletequestion'),
	 array(
	 'pass' => array('username')
	 )
	 );
	Router::connect('/clients/delete/:username',
	 array('controller' => 'clients', 'action' => 'admin_delete'),
	 array(
	 'pass' => array('username')
	 )
	 );
	Router::connect('/clients/edit/:username',
	 array('controller' => 'clients', 'action' => 'admin_edit'),
	 array(
	 'pass' => array('username')
	 )
	 );
	Router::connect('/clients/changepassword/:username',
	 array('controller' => 'clients', 'action' => 'admin_changepassword'),
	 array(
	 'pass' => array('username')
	 )
	 );
	Router::connect('/fronts/admin_prospective_approve/:username',
	 array('controller' => 'fronts', 'action' => 'admin_prospective_approve'),
	 array(
	 'pass' => array('username')
	 )
	 );
	Router::connect('/fronts/admin_prospective_delete/:username',
	 array('controller' => 'fronts', 'action' => 'admin_prospective_delete'),
	 array(
	 'pass' => array('username')
	 )
	 );
	Router::connect('/fronts/admin_takeaway_prospective_approve/:username',
	 array('controller' => 'fronts', 'action' => 'admin_takeaway_prospective_approve'),
	 array(
	 'pass' => array('username')
	 )
	 );
	Router::connect('/fronts/admin_takeaway_prospective_delete/:username',
	 array('controller' => 'fronts', 'action' => 'admin_takeaway_prospective_delete'),
	 array(
	 'pass' => array('username')
	 )
	 );
	Router::connect('/members/search', array('controller' => 'members', 'action' => 'admin_search'));
    Router::connect('/thanks', array('controller' => 'staticPages', 'action' => 'thanks'));
    
	//App::import('Lib', 'routes/SharingFileRoute');
	//Router::connect('/:slug', array('controller' => 'fronts', 'action' => 'downloadfile'), array('routeClass' => 'SharingFileRoute'));
	Router::connect('/pages/*', array('controller' => 'static_pages', 'action' => 'page'));
	Router::connect('/contact_us', array('controller'=>'static_pages','action'=>'contact_us'));	 	   
 	Router::connect('/reset_mail_sent', array('controller'=>'users','action'=>'forgot_thanks'));	
 	Router::connect('/qna', array('controller' => 'qna', 'action' => 'index'));
        Router::connect('/insights', array('controller' => 'insight', 'action' => 'index'));
 	Router::connect('/qanda', array('controller' => 'qna', 'action' => 'index'));
 	Router::connect('/qna/index', array('controller' => 'qna', 'action' => 'index'));
       Router::connect('/qna/answers', array('controller' => 'qna', 'action' => 'index'));
	Router::connect('/pricing', array('controller' => 'pricing', 'action' => 'pricing'));
       Router::connect('/roundtable', array('controller' => 'roundtable', 'action' => 'roundtable'));
       Router::connect('/browse/category', array('controller' => 'browse', 'action' => 'category'));
       Router::connect('/reports/admin_userlist', array('controller' => 'reports', 'action' => 'admin_userlist'));
       Router::connect('/:username',
		array('controller' => 'users', 'action' => 'my_account'),
		  array(
			  'pass' => array('username')
		  )
		);
	 Router::connect('qna/question/:username',
	 array('controller' => 'qna', 'action' => 'question'),
	 array(
	 'pass' => array('username')
	 )
	 );
	 Router::connect('/admin/project/approveproject/:username',
	 array('controller' => 'project', 'action' => 'admin_approveproject'),
	 array(
	 'pass' => array('username')
	 )
	 );
	 Router::connect('/admin/project/disapproveproject/:username',
	 array('controller' => 'project', 'action' => 'admin_disapproveproject'),
	 array(
	 'pass' => array('username')
	 )
	 );
	 Router::connect('/project/deleteproject/:username',
	 array('controller' => 'project', 'action' => 'admin_deleteproject'),
	 array(
	 'pass' => array('username')
	 )
	 );
	 Router::connect('/reports/editautocompletetext/:username',
	 array('controller' => 'reports', 'action' => 'admin_editautocompletetext'),
	 array(
	 'pass' => array('username')
	 )
	 );
	 Router::connect('/reports/deleteautocompletetext/:username',
	 array('controller' => 'reports', 'action' => 'admin_deleteautocompletetext'),
	 array(
	 'pass' => array('username')
	 )
	 );
      

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
