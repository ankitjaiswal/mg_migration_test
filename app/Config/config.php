<?php
$config['Site.title']     					=	'Mentorsguild';
$siteFolder               					=	dirname(dirname(dirname($_SERVER['SCRIPT_NAME'])));
$config['App.AdminMail']  					=	"info@mentorsguild.com";
$config['App.PageLimit']  					=	'20';
$config['App.AdminPageLimit'] 				=	'20';
$config['App.Status.inactive'] 				=	'0';
$config['App.Status.active'] 				=	'1';
$config['App.Status.delete'] 				=	'2'; 

$config['App.MaxFileSize'] 					=	'1048576';
$config['Status']         					= 	array('1'=>'Active','0'=>'Inactive');
$config['EmailNotification']          = array('N'=>'None','W'=>'Weekly');
$config['AcceptApplication']          = array('N'=>'No','Y'=>'Yes');
$config['ApplicationFee']          = 5;






$config['App.Role.Admin'] = 1;
$config['App.Role.Mentor'] = 2;
$config['App.Role.Mentee'] = 3;
$config['App.Role.Visitor'] = 4;


/* $config['Authorized.login']					=  	"9bJ323XkA6a6" ;
$config['Authorized.key'] 					=  	"772bsNSVnZ49Hv8x" ; */

/* * @define SITE_URL - Not defined */
define('SITE_URL', 'http://' . $_SERVER['HTTP_HOST']);
define('ERROR_BORDER', '2');
define('NORMAL_BORDER', '1');

define('MENTORS_IMAGE_PATH','members'.DS.'profile_images');
define('MENTORS_RESUME_PATH','members'.DS.'resumes');
define('MENTORS_RESOURCE_PATH','members'.DS.'resources');

define('MENTEES_IMAGE_PATH','clients'.DS.'profile_images');
define('MENTEES_RESUME_PATH','clients'.DS.'resumes');
define('MENTEES_RESOURCE_PATH','clients'.DS.'resources');
define('MENTEES_PROJECT_PATH','clients'.DS.'projects');
//define('IMAGE_USER_FOLDER_NAME','user_images');
define('GENERAL_KEY','56KOv57lNjSR9yI6q5Ps');


?>