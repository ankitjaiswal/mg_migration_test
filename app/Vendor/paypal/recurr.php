<?php
class Paypal {

   protected $_errors = array();

   //Sandbox
   protected $_credentials = array(
      'USER' => 'iqbal-facilitator_api1.mentorsguild.com',
      'PWD' => '3JS7LMP7FQ3D6D4A',
      'SIGNATURE' => 'AsuM.IYM9qExGGicBAmmc-DK325AA5tjBGCo8coalstm5PtKJ37JBSvT',
   );
   
   //Live
   /*protected $_credentials = array(
   		'USER' => 'iqbal_api1.mentorsguild.com',
   		'PWD' => 'GBA7LL9FGDKCC49Q',
   		'SIGNATURE' => 'AfSdJ4DAXZtNjp9JAMuyacYFw89lAfBK6ocQdNiz5ZXrv9HLLgx0dcyf',
   );*/

   //Sandbox
   protected $_endPoint = 'https://api-3t.sandbox.paypal.com/nvp';

   //Live
   //protected $_endPoint = 'https://api-3t.paypal.com/nvp';
   
   protected $_version = '74.0';

   public function request($method,$params = array()) {
      $this -> _errors = array();
      if( empty($method) ) { 
         $this -> _errors = array('API method is missing');
         return false;
      }

      $requestParams = array(
         'METHOD' => $method,
         'VERSION' => $this -> _version
      ) + $this -> _credentials;


      $request = http_build_query($requestParams + $params);

      //Sandbox
      $http_header = array(
        'X-PAYPAL-SECURITY-USERID' => 'iqbal-facilitator_api1.mentorsguild.com',
        'X-PAYPAL-SECURITY-PASSWORD' => '3JS7LMP7FQ3D6D4A',
        'X-PAYPAL-SECURITY-SIGNATURE' => 'AsuM.IYM9qExGGicBAmmc-DK325AA5tjBGCo8coalstm5PtKJ37JBSvT',
        'X-PAYPAL-REQUEST-DATA-FORMAT' => 'JSON',
        'X-PAYPAL-RESPONSE-DATA-FORMAT' => 'JSON'
      );
      
      //Live
      /*$http_header = array(
      		'X-PAYPAL-SECURITY-USERID' => 'iqbal_api1.mentorsguild.com',
      		'X-PAYPAL-SECURITY-PASSWORD' => 'GBA7LL9FGDKCC49Q',
      		'X-PAYPAL-SECURITY-SIGNATURE' => 'AfSdJ4DAXZtNjp9JAMuyacYFw89lAfBK6ocQdNiz5ZXrv9HLLgx0dcyf',
      		'X-PAYPAL-REQUEST-DATA-FORMAT' => 'JSON',
      		'X-PAYPAL-RESPONSE-DATA-FORMAT' => 'JSON'
      );*/


      $curlOptions = array (
        CURLOPT_HTTPHEADER => $http_header,
        CURLOPT_URL => $this -> _endPoint,
        CURLOPT_VERBOSE => 1,
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_SSL_VERIFYHOST => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => $request
      );

      $ch = curl_init();
      curl_setopt_array($ch,$curlOptions);


      $response = curl_exec($ch);

      if (curl_errno($ch)) {
      	prd(curl_error($ch));
         $this -> _errors = curl_error($ch);
         curl_close($ch);
         return false;

      } else  {
         curl_close($ch);
         $responseArray = array();
         parse_str($response,$responseArray); 
         return $responseArray;
      }
   }
}
?>