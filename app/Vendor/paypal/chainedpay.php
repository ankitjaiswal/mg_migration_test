<?php
/*THIS IS STRICTLY EXAMPLE SOURCE CODE. IT IS ONLY MEANT TO 
QUICKLY DEMONSTRATE THE CONCEPT AND THE USAGE OF THE ADAPTIVE 
PAYMENTS API. PLEASE NOTE THAT THIS IS *NOT* PRODUCTION-QUALITY 
CODE AND SHOULD NOT BE USED AS SUCH.

THIS EXAMPLE CODE IS PROVIDED TO YOU ONLY ON AN "AS IS" 
BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, EITHER 
EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY WARRANTIES 
OR CONDITIONS OF TITLE, NON-INFRINGEMENT, MERCHANTABILITY OR 
FITNESS FOR A PARTICULAR PURPOSE. PAYPAL MAKES NO WARRANTY THAT 
THE SOFTWARE OR DOCUMENTATION WILL BE ERROR-FREE. IN NO EVENT 
SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF 
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
OF SUCH DAMAGE.
*/


/* TODO
 * Using PHP SOAP 5.3
 * Modifiy/add other fields to the class structure based on payment types/API requirements
 * the http headers and request body contains sample data, please replace the data with valid data.
*/

//turn php errors on
ini_set("track_errors", true);

$Env = "sandbox";

if ($Env == "sandbox")
{
	//set PayPal Endpoint to sandbox
	$api_endpoint = trim("https://svcs.sandbox.paypal.com/AdaptivePayments/Pay");
	$wsdl = trim("https://svcs.sandbox.paypal.com/AdaptivePayments?wsdl");
} else {
	$api_endpoint = trim("https://svcs.paypal.com/AdaptivePayments/Pay");
	$wsdl = trim("https://svcs.paypal.com/AdaptivePayments?wsdl");
	
}

/*
*******************************************************************
PayPal API Credentials
Replace <API_USERNAME> with your API Username
Replace <API_PASSWORD> with your API Password
Replace <API_SIGNATURE> with your Signature
*******************************************************************
*/

//PayPal API Credentials





class ReceiverList {
public $receiver;
}
class Receiver {
public $amount;
public $email;
public $primary; 
}

class RequestEnvelope {
    public $detailLevel;
    public $errorLanguage;
}


class PayRequest { 
public $requestEnvelope;  
public $actionType; 
public $cancelUrl;
public $currencyCode;  
public $memo;  
public $receiverList;  
public $returnUrl;
}  
?>


